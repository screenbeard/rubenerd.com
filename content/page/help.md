---
layout: page
title: "Help"
---
*Help* is a Beatles song! It's also what I hope you'll get from this page. [Contact me] if you need more info, I'd be happy to point you in the right direction.


### Contents

* [How to read the site](#how-to-read-the-site)
* [Subscribing to posts](#subscribing-to-posts)
* [Subscribing to podcast episodes](#subscribing-to-podcast-episodes)


### How to read the site

*Rubenerd* is a weblog, consisting of entries called *posts* that are sorted in chronological order. You can think of it as a personal journal, but meant to be read by the public:

* The [home page] always contains the ten most recent posts. You can use the pagination links at the bottom to go back and forward in time, but reading that one page is usually sufficient to keep up to date.

* Each post has an arrow alongside its heading on the home page, which links to each post individually. They too have pagination links at the bottom that let you read the next or previous post in the series. For example, [check out this post] about how to identify grilled cheese sandwiches.

* You can use the [archive page] if you're looking for a post on a specific topic, writing location, or year. Choosing one of these categories will give you a list of posts that match.

* The [omake page] lists other content published on *Rubenerd* that doesn't fit anywhere else.

[contact me]: https://rubenerd.com/about/#contact
[home page]: https://rubenerd.com/
[archive page]: https://rubenerd.com/archives/
[check out this post]: https://rubenerd.com/p3932/
[this feed link]: https://rubenerd.com/feed/
[omake page]: https://rubenerd.com/omake/
[Feedbro for Firefox]: https://addons.mozilla.org/en-GB/firefox/addon/feedbroreader/
[Feedly]: https://feedly.com 


### Subscribing to posts

If you read a lot of blogs, you can subscribe to them in a blog reader. I recommend [The Old Reader](https://theoldreader.com/), but there are lots of others. They make keeping up to date with people you care about that much easier.

You can subscribe to *Rubenerd* using the following services:

* [Bloglines](https://www.bloglines.com/sub/https://rubenerd.com/feed/)
* [Feedbin](https://feedbin.com/?subscribe=rubenerd.com/feed/)
* [Feedly](https://feedly.com/i/subscription/feed/rubenerd.com/feed/)
* [My Yahoo!](http://add.my.yahoo.com/rss?url=rubenerd.com/feed/)
* [NetNewsWire](feed://rubenerd.com/feed/)
* [Netvibes](http://www.netvibes.com/subscribe.php?url=https%3A%2F%2Ffrubenerd.com%2Ffeed%2F)
* [The Old Reader](http://theoldreader.com/feeds/subscribe?url=rubenerd.com/feed/)

Or you can paste `rubenerd.com/feed/` into your blog reader.


### Subscribing to podcast episodes

The [Rubenerd Show](https://rubenerd.com/show/) is my silly online radio show. You can subscribe to it with your favourite podcast client or service:

* [Apple iTunes](https://itunes.apple.com/au/podcast/rubenerd-show/id1003680071)
* [Internet Archive](https://archive.org/details/rubenerdshow)
* [Juice Receiver](podcast://rubenerd.com/show/feed/)
* [Overcast](https://overcast.fm/itunes1003680071/rubenerd-show)
* [Pocket Casts](http://pca.st/ybXl)
* [Podnova](http://podnova.com/add.html#url=http%3A%2F%2Frubenerd.com%2Fshow%2Ffeed%2F)

Or you can paste `showfeed.rubenerd.com` into your podcast client.

