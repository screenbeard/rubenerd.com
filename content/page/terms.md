---
layout: page
title: Terms
---
I went sixteen years without this page, but it's probably necessary now that some of my posts are being shared in high-profile places.

To [paraphrase](https://twitter.com/mwlauthor/status/1307719213067448320)  technical writer Chris Sanders:

> It's my [site] and I can do what I want here.

This blog has always been a labor of love project for me. I find writing cathardic, and a practical way to remember thoughts, news, and processes. This ethos informs the rest of this page.

<h3 id="contents">Contents</h3>

* [General disclaimer](#general-disclaimer)
* [Privacy policy](#privacy-policy)
* [Comments](#comments)
* [Terms of use](#terms-of-use)
* [Conclusion](#conclusion)


<h3 id="general-disclaimer">General disclaimer</h3>

I'm not motivated to deceive or harm. That said, everything written here is provided "AS IS" with absolutely no warranties, expressed or implied. You acknowledge your responsibility to perform due diligence after reading anything here. This includes&mdash;but is not limited to&mdash;technical, financial, medical, and legal information.

You are also expected to engage in critical thinking here, as you should everywhere! That includes knowing when something is embellished or joked about. I know you know what that means.


<h3 id="privacy-policy">Privacy policy</h3>

This site doesn't host first or third-party trackers, or store cookies on your machine. I respect your privacy as a matter of principle.

The web server software stores your IP addresses, destination page, and referring page for a period not exceeding 24 hours, to assist in dealing with abuse. Europeans under the <abbr title="General Data Protection Regulation">GDPR</abbr>, or anyone else, can request these logs pertaining to your visit by contacting me.

Servers hosting this site and its backups are hosted by me in Australia, Singapore, and the United States.


<h3 id="comments">Comments</h3>

I accept comments through [email](https://rubenerd.com/about/#contact) and [Twitter](https://twitter.com/Rubenerd). I'm usually unreasonably happy to get them! I may even quote you if I think there's something useful for everyone.

But while you can disagree with what I've written, I don't owe you an explanation or engagement, nor can you demand them of me (see above). I specifically ignore comments that:

<ol>
<li id="spam">are spam;</li>
<li id="dishonest">are dishonest;</li>
<li id="nitpick">only serve to nitpick;</li>
<li id="bad-faith">are written in bad faith;</li>
<li id="attack">engage in character attacks;</li>
<li id="ignored">miss/ignore points, willfully or otherwise;</li>
<li id="comprehension">demonstrate a lack of comprehension; or</li>
<li id="discretion">any other reason I want (see above)!</li>
</ol>

If you don't agree, I wish you the very best browsing other sites.


<h3 id="terms-of-use">Terms of use</h3>

All my text and podcast episodes are licenced under [Creative Commons 3.0 Attribution](https://creativecommons.org/licenses/by/3.0/), and the [3-Clause BSD licence](https://github.com/rubenerd/rubenerd.com/blob/master/LICENSE.md). I'm happy for you to quote and use my words, provided you give attribution and link back to the original page.

Third-party text and images used for the purposes of review, reporting, and parody are presumed in good faith to be protected under Australian Fair dealing laws and/or American Fair Use. [Contact me](/about/#contact) to request removal.


<h3 id="conclusion">Conclusion</h3>

Is a word with ten letters. Thanks for reading!

