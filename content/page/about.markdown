---
layout: page
title: "About"
---
<img src="https://rubenerd.com/files/2017/me.jpg" alt="Me!" style="float:right; margin:0 0 1em 2em; height:140px; width:140px; border-radius:128px;" />

* [Me](#me)
* [The site](#site)
* [Rubi the mascot](#mascot)
* [Geek code](#geekcode)
* [Contact](#contact)


<h3 id="me">$(whoami)</h3>

My name is Ruben Schade, though I've been called Reuben Shade, Robbin Shuffle, and Reneé Schultz. **HOW!?** I'm into retrocomputing, writing in coffee shops,  anime, and reading stuff about things. My beautiful girlfriend Clara and I live for travelling, bubble tea, and drinking bubble tea while travelling.

I make a living as a technical writer and solution architect for an [IaaS company](https://www.orionvm.com/) in Sydney and San Francisco, but I grew up in Singapore where I developed a taste for well-funded infrastructure, lah. I'm also working up the guts to do more speaking and attend more industry events, especially for the fine BSD operating system family.

*Rubenerd* has been my unfortunate Internet handle since I was 13; at the time I thought it was a clever concatenation. Now I think it's delightfully self-deprecating, like my [last name](https://en.wiktionary.org/wiki/schade#Adjective) in German. *Bagus!* Wait.

[Clara]: http://kiri.sasara.moe/
[podcast]: https://rubenerd.com/show/


<h3 id="site">The site</h3>

This is my weblog and [podcast] of things that have interested me since high school in 2004. I publish an [RSS feed](/feed/) for blog posts, and [just for the podcast](http://showfeed.rubenerd.com/) you can subscribe to. I have an [FAQ] and [Omake] with more details.

It's powered by [Hugo], [FreeBSD], and [OrionVM], and written using a keyboard and, usually, my brain. I also used to maintain a [separate anime blog], which one day I'll piece back together here.

I'm not sure what I'd fill spare time with if it weren't for this site... maybe something productive. Sounds terrifying.

[archives]: /archives/
[FAQ]: /faqs/
[Hugo]: https://gohugo.io/
[omake]: /omake.opml
[has become]: https://rubenerd.com/modernwebbloat-js/ "Blog post on ModernWebBloat.js"
[FreeBSD]: https://www.freebsd.org/
[OrionVM]: https://www.orionvm.com/
[separate anime blog]: https://rubenerd.com/anime-restore-my-blog/


<h3 id="mascot">Rubi the mascot</h3>

<img src="https://rubenerd.com/rubi@2x.jpg" alt="" style="width:128px; float:right; margin:0 0 1em 1em" />

Rubi is the site's mascot, conceived by Clara. Her mismatching, Miku-esque boots allude to my predilection for not wearing matching socks.

*(Funny story, I was chided for wearing orange and red socks during a high school work experience day, with the implication that I wasn't taking the assignment seriously. I've refused to ever since).*

We agreed that Rubi was born in Sapporo on her birthday. Her favourite foods are shiroi koibito biscuits and curry udon, washed down with matcha and copious amounts of black coffee. These are burned off by trips to indoor pools and nature hiking in the snowy Hokkaido wilderness. She enjoys reading, cosplaying, and slice-of-life anime that may or may not have robots. Her dream is to start a line of self-heating swimsuits and towels, and tactical cargo skirts with pockets that can actually fit modern smartphones.


<h3 id="geek-code">Geek code</h3>

    -----BEGIN GEEK CODE BLOCK-----
    Version: 3.1
    GIT/TW d+ s+:-- a C+++ UBL++++$ P+++>++++ L+ E--- 
    W+++ N+ !o? K-? w--$ O+ M++$ V PS+ PE- Y+ PGP+ t++ 
    5+ X+ R tv b++ DI-- D G+ e++ h r++>+++ y+
    ------END GEEK CODE BLOCK------ 


<h3 id="contact">Contact me</h3>

Thanks for taking an interest in my stuff! You can message me on [Mastodon], [Twitter], or email:

    printf "%s@%s.%s\n" me rubenschade com

If you're responding to a post, [please be civil](/faqs/#comments) and read it first. Thanks :).

[comment policy]: /omake/terms/#comments
[Mastodon]: https://bsd.network/@rubenerd
[Twitter]: https://twitter.com/rubenerd
[PGP public key]: https://pgp.mit.edu/pks/lookup?op=vindex&search=0x9CFC8AEBBD528543
[Pinboard]: https://pinboard.in/u:Rubenerd
[GitLab]: https://gitlab.com/users/rubenerd/projects
[Flickr]: https://flickr.com/photos/rubenerd
[Instagram]: https://instagram.com/RubenSchade
[Wikipedia]: https://en.wikipedia.org/wiki/User:RubenSchade
[YouTube]: https://www.youtube.com/user/rubenerd
[Vimeo]: https://vimeo.com/rubenerd

