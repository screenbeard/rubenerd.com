---
layout: omake
title: "Emacs notes"
---
<img src="https://rubenerd.com/files/2020/construction.gif" style="width:108px; float:right; margin:0 0 2em 2em;" />

These are my rough notes for a Vim user who's trying Emacs. It's very much a work in progress, though I don't have one of those handy Web 1.0 *Under Construction* banners handy. Oh, wait!

### Installing packages
Add to your `~/.emacs/init.el`:

    (require 'package)
    (package-initialize)
    (add-to-list 'package-archives
                 '("melpa" . "https://melpa.org/packages/") t)

### Packages used from ELPA
* [auctex](https://elpa.gnu.org/packages/auctex.html)
* [org](http://elpa.gnu.org/packages/org.html)

### Packages used from MELPA
* [centaur-tabs](https://melpa.org/#/centaur-tabs)
* [dired-sidebar](https://melpa.org/#/dired-sidebar)
* [markdown-mode](https://melpa.org/#/markdown-mode)
* [nyan-mode](https://melpa.org/#/nyan-mode)
* [spacemacs-theme](https://melpa.org/#/spacemacs-theme)
* [treemacs](https://melpa.org/#/treemacs)
* [yaml-mode](https://melpa.org/#/yaml-mode)
