---
draft: true
layout: omake
title: "Ruben’s Retro Computer Corner"
---
This [omake page](/omake/) details the nook in Clara's and my apartment where I tinker with my favourite vintage computers: the Pentium 1 machine I first built when I was a kid in the late 1990s, and my Commodore 128 from Josh Nunn!

I hope this page will be useful or fun for those of you intereted in this fascinating era of tech. You can also find blog posts about these machine with the tags [MMX machine](/tag/mmx-machine/) and [Commodore 128](/tag/commodore-128/). 

Like all good pages that belong in the 1990s, this is perenially *under construction!*

<p><img src="https://rubenerd.com/files/2021/retro-computer-corner@1x.jpg" srcset="https://rubenerd.com/files/2021/retro-computer-corner@1x.jpg 1x, https://rubenerd.com/files/2021/retro-computer-corner@2x.jpg 2x" alt="" style="width:500px; height:375px;" /></p>

<hr style="display:block !important; visibility:visible !important; background:#2b9c27; border:0; margin:3em 0 2em 0; height:1px;" />



<h3 id="contents">Contents</h3>

* **[Ami, my childhood Pentium 1]()**
    * [Specifications]()
    * [POST output]()
* **[Aino, the Commodore 128]()**

<hr style="display:block !important; visibility:visible !important; background:#2b9c27; border:0; margin:3em 0 2em 0; height:1px;" />



### Ami, the Pentium

This was the first computer I built as a kid, with the winnings from a primary school writing contest if you can believe it! She's been through dozens of international house moves and has more than her fair share of dings and scratches, but in 23 years she still runs.


#### Specifications

<table style="width:100%">
<th>Part</th>
<th>Version</th>
<th>Origin</th>
</tr>
</thead>
<tbody>
<tr>
<th>Motherboard</th>
<td>Octek Rhino 12+</td>
<td>Original</td>
</tr>
<tr>
<th>CPU</th>
<td>Pentium 200MHz “with MMX technology”</td>
<td>Original</td>
</tr>
<tr>
<th>Memory</th>
<td>48 MiB PC-100 (32, 16 DIMMs)</td>
<td>Replaced</td>
</tr>
<tr>
</tr>
</tbody>
</table>


#### POST output

<pre style="font-size:11px !important; line-height:11px;">
                             Award Software, Inc
                            System Configurations
╔═════════════════════════════════════════════════════════════════════════════╗
║ CPU Type          : PENTIUM-MMX         Base Memory       :    640K         ║
║ Co-Processor      : Installed           Extended Memory   :  48128K         ║
║ CPU Clock         : 200MHz              Cache Memory      :    512K         ║
╟─────────────────────────────────────────────────────────────────────────────╢
║ Diskette Drive  A : 1.2M , 5.25 in.     Display Type      : EGA/VGA         ║
║ Diskette Drive  B : 1.44M, 3.5 in.      Serial Port(s)    : 3FA 2F8         ║
║ Pri. Master  Disk : LBA ,UDMA 7,32021MB Parallel Port(s)  : None            ║
║ Pri. Slave   Disk : LBA ,UDMA 4,16014MB Bank  0           : Sync. DRAM      ║
║ Sec. Master  Disk : CDROM,Mode 4        Bank  1           : Sync. DRAM      ║
║ Sec. Slave   Disk : None                Bank  2           : None            ║
╚═════════════════════════════════════════════════════════════════════════════╝
  L2 Cache  Type    : Pipeline
    
Verifying DMI Pool Data .......

</pre>

4094.7

### Installers and Drivers

* **Drivers**
    * [Creative SoundBlaster AWE64](https://archive.org/download/CreativeLabsSoundBlasterAWE64DriverCDA64V-CDOEM-E4-E6)
* **Windows NT 3.51**
    * [Installer](https://archive.org/details/ntwks351_iso)
    * [Service Pack 5](https://archive.org/details/X05-93539)
    * [Resource Kit](https://archive.org/details/mswinntrskt351a_201901)

