---
layout: omake
title: "Coffee"
---
<p><img src="https://rubenerd.com/files/2021/japan-sanfran-singapore-japan-cawfee@1x.jpg" srcset="https://rubenerd.com/files/2021/japan-sanfran-singapore-japan-cawfee@1x.jpg 1x, https://rubenerd.com/files/2021/japan-sanfran-singapore-japan-cawfee@2x.jpg 2x" alt="Some of the coffee shops featured below!" style="width:500px" /></p>

I live to write and work from coffee shops. There's something special and oddly motivational about good coffee and atmosphere. Almost all the posts on this blog (Covid times notwithstanding) have been written from them.

This evolving page is dedicated to <a href="https://jamesg.blog/2021/05/05/edinburgh-coffee-shops">James' Coffee Blog</a>, which inspired me to update this page after an extended absense. One day Clara and I will meet up with him and he'll show us the best ones around Edinburgh!


### Contents

* [Singapore](#singapore)
* [Kuala Lumpur, Malaysia](#kuala-lumpur-malaysia)
* [Sydney and surrounds, Australia](#sydney-and-surrounds-australia)
* [Adelaide, Australia](#adelaide-australia)
* [Melbourne, Australia](#melbourne-australia)
* [Canberra, Australia](#canberra-australia)
* [Hong Kong](#hong-kong)
* [Kyoto, Japan](#kyoto-japan)
* [Osaka, Japan](#osaka-japan)
* [Tokyo, Japan](#tokyo-japan)
* [New York, USA](#new-york-usa)
* [San Francisco and Oakland, USA](#san-francisco-and-oakland-usa)
* [Los Angeles, USA](#los-angeles-usa)


### Singapore

Where I grew up, still consider home, and where I hope to move back to one day. It's a fascinating melting pot of Western, Straits Chinese, and Malay influences when it comes to coffee culture.

* [% Arabica](https://arabica.coffee/en/locations/singapore/): Best coffee I've had in Japan, and now they have a branch in the Lion City that's just as good. Get an iced brew coffee without milk or sugar; you won't need them :).

* [Coffee Bean and Tea Leaf](https://www.coffeebean.com.sg): Including in this list because the South-East Asian franchises are better than the parent company in California! I got through my high school exams thanks to sitting at their branches having Americanos, so I feel I owe them some love here.

* [Toastbox](https://toastbox.com.sg): The most accessible way to get a traditional Singaporean breakfast and coffee. It's the #1 thing I miss living back in Australia. ♡

* [Yahava Coffee Works](https://www.yahava.sg/): A deceptively-small terrace house that opens out to a long coffee shop and roastery. Lovely coffee and tea.


### Kuala Lumpur, Malaysia

I lived here with family for a couple of years in-between Singapore. It was a weird time in my life in many ways, so I didn't venture out much. When I did, convenience trumped exploring. I'm sure there's more special stuff there waiting to be discovered.

* [San Francisco Coffee](http://sfcoffee.com/): The best chain coffee in the city and surrounds. I basically lived in their KLCC outlet for a year!


### Sydney and surrounds, Australia

This is where I currently live, so it's where I'm most likely to find new stuff!

* [Apothecary Coffee](https://business.google.com/website/apothecary-coffee/): Best coffee on the North Shore and Chatswood, and the closest to our apartment. Their batch brew is mellow, light, fruity, and delicious.

* [Boatshed](https://www.teagardensboatshed.com/): Favourite coffee on the Central Coast in Tea Gardens, when I go up to visit my dad. Set right on the water, so the ducks and swans can visit you while you have your long black or espresso.

* [Cassiopeia](https://cassiopeia.com.au): Cute little roastery and coffee shop tucked away on the street leading to the main nature reserves in the Blue Mountains. Clara and I use it as a base when we're escaping Sydney for some relaxation.

* [Coffee Alchemy](https://coffeealchemy.com.au/): Best coffee in Australia. You sit on wooden chruch pews a metre or two away from the industrial roasting machines. There are no cakes or snacks, just coffee. The smells, sounds, sights and tastes make this entirely unique and special.

* [Devon Café](): Japanese fusion cuisine and unreasonably-good coffee. Their original branch was in Surry Hills, but Clara and I love their North Sydney location the best.

* [Industry Beans](https://industrybeans.com/): The chain from Melbourne that looks vaguely like a Californian Blue Bottle, and tastes just as good!

* [L'Expresso House](https://www.yelp.com/biz/l-expresso-house-woolloomooloo): Best coffee in the Sydney CBD, down the road from the world-famous Harry's Café de Wheels. I may or may not use it as a second-base to work from instead of the office nearby *cough*.


### Adelaide, Australia

I studied here for a few years in the 2000s.

* [Cibo Espresso](https://www.ciboespresso.com.au/): Best chance of good coffee from a chain. I used to sit at their shop in Rundle Mall to read.

* [The Boatdeck Cafe](http://theboatdeck.com.au/): Much of this site in the mid-2000s was written from this place. They don't do the *Betty Blue Sea of Espresso* any more, but still the best coffee and service in northern Adelaide!


### Melbourne, Australia

Melbourne bills itself as the cultural capital of Australia. I don't think its coffee is any better than other parts of Australia, but it's undeniable they take it seriously.

* [Cartel Coffee Roasters](https://www.coffeecartel.com.au/): I'd bought their beans online but hadn't had in their coffee shop until I went in 2019. 

* [The Kettle Black](https://darlinggroup.com.au/venues/thekettleblack/): The converted building makes for such a great setting for awesome coffee in South Melbourne. The food is exceptional as well.


### Canberra, Australia

A city best avoided (fighting words?) but they have some good brews.

* [Penny University Coffee Roasters](http://pennyuniversitycafe.com/): I came for the name, and stayed for the coffee. I'd had some awful, burnt stuff earlier that day, and the cup from here was a slice of heaven.


### Hong Kong

I only got to go there for a few days in transit in 2018, but I love all the indie, fun stuff behind all regular chains you're famila with.

* [Candies Cafe](https://www.yelp.com/biz/candies-cafe-%E9%A6%99%E6%B8%AF): A bohemian little corner store, run by friendly people. Their takeaway cold drip comes in an adorable little glass jar with lid, and saved me from summer heat in more ways than one.


### Kyoto, Japan

My favourite city in the world. I would learn Japanese just to retire there. 

* [% Arabica](https://arabica.coffee/en/locations/japan): So good, they're listed here twice! Arashiyama is the most beautiful place in the world, and the most serene I've ever felt. Sitting at the edge of those waterfalls and the wooden Togetsu-kyo bridge at this coffee shop in winter with Clara was just about the best experience of my life. 

* [Blue Bottle](https://bluebottlecoffee.com/): We stumbled across a branch of the Californian roaster while wandering. They refurbished a traditional-style Japanese wooden building with all their modern gear, but kept so much of its original charm. The coffee was exquisite, though be prepared to wait a while, it's shockingly popular!

* [Maeda Coffee](https://www.maedacoffee.com/en/) (前田珈琲): Excellent service, and your choice of filter single origin or their amazing house blend. They don't do espresso. They have a few branches, but definitely go to the one near Kiyomizu-dera, it's jaw-droppingly beautiful.

* [MOS Cafe](https://www.yelp.com/biz/%E3%83%A2%E3%82%B9%E3%82%AB%E3%83%95%E3%82%A7-%E7%83%8F%E4%B8%B8%E5%85%AD%E8%A7%92%E5%BA%97-%E4%BA%AC%E9%83%BD%E5%B8%82): The coffee isn't anything special, but the decor and atmosphere are beautiful, and it's great for light Western breakfasts and to chill when you miss the taste of home.


### Osaka, Japan

Kansai represent! I travel to Tokyo for conferences, but I much prefer travelling around Osaka.

* [CAFE BREAK ~To The Forest~](https://www.yelp.com/biz/%E3%82%AB%E3%83%95%E3%82%A7%E3%83%96%E3%83%AC%E3%83%BC%E3%82%AF-%E5%A4%A7%E9%98%AA%E5%B8%82): Coffee was decent, but the atmosphere was amazing. Such a welcome reprieve from excessive walking and exploring on a summer's day.

* [DEAN & DELUCA](https://www.deandeluca.co.jp/ddshop/5004/): Some of the best coffee in Osaka. You can tell how much pride they have in their craft by how meticulously polished and clean their equipment is kept. We found it entirely by accident after taking the Nozomi Shinkansen.


### Tokyo, Japan

* [Coffee Sakan Shu](https://foursquare.com/v/%E7%8F%88%E7%90%B2%E8%8C%B6%E9%A4%A8-%E9%9B%86/56358479498ef83081412e5b) (珈琲茶館 集): Down a small flight in Toshima is one of the most opulent, classy coffee shops Clara and I have ever been to! She thought the waiters were cute in their butler outfits. I might have agreed, but was too busy enjoying the beautiful coffee.


### San Francisco and Oakland, USA

I got to work from here for a few months in 2019. Be prepared to spend a *lot* of money for coffee and food here, but most of it is worth it.

* [Blue Bottle](https://bluebottlecoffee.com/): You have to visit their original store in Oakland, but all their branches deliver exceptional brews. They also have a branch in Kyoto!

* [Cafe Réveille](https://www.reveillecoffee.com/): Beautiful high ceilings and decor match the great coffee and friendly staff in Mission Bay. If you get there early enough, you can watch them roasting in-store.

* [SPRO Coffeelab](https://foursquare.com/v/spro-coffeelab/593475aa4c954c63db8a1c37): A cute food truck coffee shop with friendly staff and amazing brews. Ask them to choose for you :).


### New York, USA

We spent more time exploring and going to museums than looking for coffee, but one day I'll add all the happy surprises we stumbled across!

* [L'imprimerie](http://limprimerie.nyc/): Beautiful little hole-in-the-wall down from the [L/M Myrtle-Wyckoff Avenues](https://en.wikipedia.org/wiki/Myrtle%E2%80%93Wyckoff_Avenues_station) subway in Brooklyn. Best coffee in New York, and the people are super friendly.


### Los Angeles, USA

I'm not especially a fan of LA, but Clara was there for an anime convention, so I came down from San Francisco to visit for a few days. 

* [Cafe Demitasse](https://cafedemitasse.com/): Great coffee with a focus on sustainability and ethically-sourced beans.

