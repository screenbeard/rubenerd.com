---
title: "Missing music"
date: "2020-10-12T21:34:46+11:00"
abstract: "With a quote from a recent Big Issue by Steven Oliver."
year: "2020"
category: Media
tag:
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) is more of a commentary post. Pardon, a commentary "piece". I think I just gagged a bit!

I've talked about my disconnection from music [on the show](https://rubenerd.com/show/) a bunch of times. Music was hugely important to my parents growing up, which they passed on to me in spades and other horticultural instruments loaded with tapes. But for the last half a decade or so I've listened to *so much less* of it, and found difficult to dive back in again. 

Australian musician and comedian Steven Oliver wrote a great article in a recent Big Issue, emphasis added:

> Music is a powerful thing. We have songs that represent our footy clubs, our political ideologies and even our nations. People sing praises to their gods. We have a song to sing for our birthdays, and we use songs to sell products. Yet despite being inundated with songs from everywhere about everything, a few years back I **found myself missing music**. I know, right? How could I live in a world that is so saturated with songs, and be missing it?

This doesn't so much hit the nail on the head, as much as it smashes it repeatedly into billions of tiny splinters, like so many failed self tapping screws. That was for you Jim! *Turns out* I was missing music too, in part due to being inundated with it from a sea of open-ended streaming services and its ubiquity in public places. Music is disposable now.

Dick Clark said music is the soundtrack of our lives. I think it's evolved into the background music of our lives. It's not something to be enjoyed as an art form in its own right, it's what you play when doing something else. That includes everything from walking through food courts with their tinny speakers, to watching over-produced YouTube videos. Music is just there to fill silence.

This has even affected how music is written. Writers always wanted to create catchy tunes, but today's pop music is specifically engineered with autotune and looping hooks to make them stand out as much as possible in a sea of other fleeting moments and algorithmically-determined playlists. [Melody has been replaced](https://www.youtube.com/watch?v=K0Vn9V-tRCo "YouTube: Inside the Score: The Death of Melody") with tone colour and heavy beats that verge on the militaristic. I don't hate music from the last decade, as much as I'm just bored with it.

And I don't even blame musicians for doing this. The music industry's response to peer-to-peer downloading was streaming services, a business model in which musicians had no say. It's fine if you're a major act, but otherwise you're getting a pittance. I find it hard to see this as the victory over "piracy" that so many are willing to say.

