---
title: "Homeless people in media"
date: "2020-10-09T15:38:58+11:00"
abstract: "When did people become so okay with being this blasé?"
year: "2020"
category: Media
tag:
- human-rights
- philosophy
location: Sydney
---
I'm not sure if this is a cultural thing, but I'm struck with how cavalier so much American media treats the homeless. [News reports](https://www.youtube.com/watch?v=5LFxrPhNGAk "The Creepy NYC Subway System in 1990"), podcasts, and YouTube channels seemingly do nothing to conceal their contempt or indifference, even shows with hosts I otherwise respect. They're discussed in subhuman terms, as you would an animal or a pest to be avoided with their acrid smells and dangerous outbursts. It's even the source of jokes.

Whenever I see this I ask... *why are they homeless? Why isn't anyone helping them?* And, most of all, *why aren't they even talking about it?*

I don't want to assume it's because people don't care, or that they want to sweep it under the rug. Maybe for some people it is, but surely not those who otherwise live ethical, honest lives. It could be anything from projection to bad personal experiences.

I'm probably missing some context here, but all I could think of was that it's become so normalised, people unintentionally tune it out as a concern. Maybe it's a coping mechanism; people don't want to confront it given how they're but a few paycheques away from the same fate, or an expensive hospital visit in a country without universal healthcare. Or at the top end of town, higher income earners don't want to think about the opportunity cost of their latest tax cut, and for whom that pittance could have represented a second chance at life.

As for jokes, dark humour lets us discuss taboo subjects by shining a light and being candid and honest. It makes us uncomfortable, but that's the point.

Still, I can't help but cringe when someone on a show tallies the number of homeless people hunkering in a warm train carriage, without any further comment. Where's the empathy? Even a remark that they feel for them? I'd bet most, if not all of them riding on those plastic seats for days at a time wouldn't be there by choice.

We have enough food in the world to fill every tummy, enough housing to shelter everyone, and enough resources to provide mental health services to those doing it tough. Every single homeless person is someone society has failed. The least we can do&mdash;literally&mdash;is offer them some respect.

