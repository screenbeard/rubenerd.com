---
title: "NetBSD 9.1, FreeBSD 12.2-R, OpenBSD 6.8"
date: "2020-10-31T08:24:57+11:00"
abstract: "Lots of cool stuff to try this weekend!"
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2020"
category: Software
tag:
- bsd
- freebsd
- netbsd
- openbsd
- zfs
location: Sydney
---
<p><img src="https://rubenerd.com/files/2020/beastie@1x.png" srcset="https://rubenerd.com/files/2020/beastie@1x.png 1x, https://rubenerd.com/files/2020/beastie@2x.png 2x" alt="The BSD Daemon" style="width:192px; float:right; margin:0 0 1em 2em;" /></p>

Holiday presents have come early this year! We saw updates for the three biggest BSDs this month, all with something interesting to try. Emphasis added on what my weekend will be spent checking out:

[NetBSD 9.1](https://www.netbsd.org/releases/formal-9/NetBSD-9.1.html)\:

> The NetBSD Project is pleased to announce NetBSD 9.1, the first
feature and stability update for the netbsd-9 release branch.
> 
> Over the last months many changes have been made to the NetBSD 9 stable
branch. As a stable branch the release engineering team and the NetBSD
developers are conservative with changes to this branch and many users
rely on the binaries from our regular auto-builds for production use.
> 
> The new release features (among various other changes) many bug fixes,
a few performance enhancements, **stability improvements for ZFS** and LFS
and support for USB security keys in a mode easily usable in Firefox
and other applications.

[FreeBSD 12.2-RELEASE](https://lists.freebsd.org/pipermail/freebsd-announce/2020-October/001993.html)\:

> The FreeBSD Release Engineering Team is pleased to announce the
> availability of FreeBSD 12.2-RELEASE. This is the third release of the
> stable/12 branch.
> 
> Some of the highlights:
>
> * Updates to the wireless networking stack and various drivers have
>   been introduced to provide better 802.11n and **802.11ac support**.
> 
> * The ice(4) driver has been added, supporting Intel(R) 100Gb ethernet
>   cards.
>
> * The jail(8) utility has been updated to allow **running Linux(R) in a 
>   jailed environment**.
> 
> * OpenSSL has been updated to version 1.1.1h.
>
> * OpenSSH has been updated to version 7.9p1.
>
> * The clang, llvm, lld, lldb, compiler-rt utilities and libc++ have
    been updated to version 10.0.1.

I don't run OpenBSD anywhere, but I keep being given good reasons to finally try it again. [OpenBSD 6.8](https://www.openbsd.org/68.html), via the [OpenBSD Journal](https://www.undeadly.org/cgi?action=article;sid=20201018175726)\:

> On its 25th birthday, the OpenBSD project has released OpenBSD 6.8, the 49th release.
> 
> The new release comes with a large number of improvements and debuts a new architecture, OpenBSD/powerpc64, running on the POWER9 family of processors. The full list of changes can be found in the [announcement](https://marc.info/?l=openbsd-announce&m=160303500224235&w=2) and on the [release page](https://www.openbsd.org/68.html).

