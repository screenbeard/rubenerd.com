---
title: "Toodles and Michael Tsai on OLED iPhones"
date: "2020-12-15T09:19:00+11:00"
abstract: "I'm glad to see more attention being drawn to these nasty displays!"
year: "2020"
category: Hardware
tag:
- accessibility
- iphone
- oled
location: Sydney
---
Michael Tsai [reviewed the iPhone 12 Mini](https://mjtsai.com/blog/2020/12/10/upgrading-from-an-iphone-xr-to-an-iphone-12-mini/). He loved the form factor and camera, but added this caveat:

> I don’t like the OLED display. I’m not running into the accessibility issues that some are, but I just don’t like the way text looks. Black and gray text has a colored halo reminiscent of ClearType, which I never saw on iPhones with LCD displays.

I'm **so happy and relieved** to see this being called out by someone respected and widely-read in Apple circles. I'll continue to post about it because even normally-critical Apple writers and podcasters never discuss how nasty, shimmery, and grainy OLED screens look, and how they're a *serious* accessibility regression. Yet talk about a headphone jack or missing chargers and suddenly everyone is up in arms. It's bizarre.

Michael [links to this comment by Toodles](https://mjtsai.com/blog/2020/10/20/iphone-12-reviews/#comment-3343039) on his blog back in October:

> The only thing I am interested in knowing about iPhone 12 is how good or bad the PWM is on the all OLED lineup. I had to return an iPhone 11 Pro to get the iPhone 11 with LCD because the PWM on the Pro OLED was making my eyes and head hurt so bad that it was intolerable to use.
>
> This is a clear Accessibility issue and given that PWM negatively impacts something like 27% of people, I would expect this to be discussed

And [Erik said](https://mjtsai.com/blog/2020/10/20/iphone-12-reviews/#comment-3344327) in the same thread:

> Is Apple, the king of accessibility, ignoring a not insignificant user base to please OLED crazy reviewers? Reviewers said the XR wouldn't do well with its "subpar" display, but it was the best selling iPhone at the time.

Either Apple have calculated that the market for people with photo-sensitivity to OLEDs is too small to care about, or they haven't done accessibility testing and market research. I *highly* doubt the latter given the company's size. 

My earlier posts:

* [@Spycrowsoft on OLED sensitivity](https://rubenerd.com/spycrowsoft-on-oled-sensitivity/), <time>2020-11-17</time>
* [OLED-sensitive people left out from the iPhone 12](https://rubenerd.com/no-lcd-option-on-the-new-iphone-12/), <time>2020-10-15</time>
* [Why OLED phone screens suck for some of us](https://rubenerd.com/why-oled-phone-screens-suck-for-some-of-us/), <time>2019-01-03</time>
* [OLEDs suck, for me](https://rubenerd.com/oleds-suck/), <time>2017-10-08</time>
