---
title: "Unlimited web services aren’t"
date: "2020-11-13T09:23:56+11:00"
abstract: "Though it should be easy to see why people are perturbed."
year: "2020"
category: Thoughts
tag:
- business
- economics
location: Sydney
---
A well-known search engine has announced they're no longer honouring  unlimited photo uploads and storage. Reactions are split between anger, and people explaining how businesses work. All of it misses the mark.

The core technical issue should be obvious: there's no such thing as unlimited storage (yet). There's *practically* unlimited, there's *unlimited until our top 5% of customers reach a certain threshold and we kick them off*, and there's *unlimited but we enforce it with rate limiting etc*. But even the so-called hyperscalers can't store truly unlimited data for everyone. And even if they could, could they *forever?* People rarely talk about it, but ongoing maintenance is a far larger burden than initial deployment; it's the reason why "five minute installers" are so disingenuous. But that's a topic for another post.

When you throw in profit margins, justifications to shareholders, and shifting business priorities, it's an understandable and expected shift for companies to grandfather, revoke, or cancel unlimited storage.

But that leads us to the crux of the issue, and what nobody seems to be discussing: why then do businesses routinely offer unlimited storage in the first place? I can think of two reasons that may also flow into each other:

1. They think they can honour it. This sounds like a folly to a bivalent engineer who lives in material reality, but maybe some can.

2. They think they can get away with revoking it once they've bait-and-switched enough users, or locked them into their platform. This strikes me as *fundamentally* dishonest.

It doesn't&mdash;or shouldn't&mdash;take much empathy to understand why users and customers are perturbed when a core feature like unlimited storage is revoked, even if it was offered by a company infamous for killing services and selling customer data. You can justify the business and technical reasons, or point out the number of times it's happened before, but it doesn't negate the fact a business said they'd do one thing, but did something else. In any other industry this would be called out for the false advertising that it is.

Don't believe unlimited storage claims. The business might not be being dishonest, at least not initially, but the end result *will* be the same. Hubris in this industry is all that's unlimited!

