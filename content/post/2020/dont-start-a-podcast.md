---
title: "“Don’t start a podcast”"
date: "2020-11-29T12:07:35+11:00"
abstract: "Government broadcaster to the unwashed masses!"
year: "2020"
category: Media
tag:
- abc
- independent-media
- podcasting
location: Sydney
---
Happy Sunday! I've done my own [silly, sporadic podcast](https://rubenerd.com/show/) for the last fifteen years or so, and all my favourite shows are done by independent producers. I love that I hear people's personalities and thoughts, not a PR department or an over-produced commercial radio show with laser sounds and bumpers. With one notable exception, they've all turned out to be great people too, some of whom I've had the pleasure and honour of meeting in person years later in the US.

So when I heard that the Australian Broadcasting Corporation had done a satirical self-help video about [why you shouldn't start a podcast](https://www.youtube.com/watch?v=hN0njKIeK5M) back in May, I smiled at their irrelevancy and moved on. But I saw it in my bookmarks yesterday and decided *why not?*

Good heavens! I'm sure it wasn't *supposed* to be a patronising ramble of incoherent nonsense, but here we are. It opens with black and white shots of whom are I assume are various Australian celebrities among ABC staff. The setup for the joke dragged like a conference call on a late Friday afternoon, but eventually they get to the point:

> Don't start a podcast. Just... don't do it. You might feel like it's a productive use of your time right now. You probably already got a USB mic and a spare room ready to go. And your mate Dave's got some interesting opinions. But we're here to tell you, he doesn't. He really doesn't. He's a fuckwit.

Australians take the piss like it's a national sport, but what's this gate-keeping nonsense? Was watching this video a productive use of our time? And so what if Dave, isolated from friends and family, finds solace in having a chat and publishing it for people to share and comment on? Where do we sign up to gain approval from you fine people?

I've never understood this attitude. It's the open web; if you don't like something, don't listen to it!

> We don't need your "true crime exclusive" which is just you reading Wikipedia articles to your housemates. We don't need your minute by minute breakdown of every episode of Parks and Rec.

The edgiest stuff they could sling was a show that aired half a decade ago, and a Wikipedia reference comedians were using in the 2000s. Okay fine they're out of touch, but I couldn't help but notice a few of these people *started their own podcasts this year*, some of which deal with TV! I... what?!

These people need a mirror... for me to break over the heads to ensure them seven years of bad luck. Is that how that works? 

> What are you going to do, send everyone in your contact list a newsletter? And force them to mark you as spam instead of unsubscribing? Because they're scared to hurt your feelings? We're in the middle of a pandemic!

Okay I'm taking the mirror back, we don't need the projection!

&#x1f332; &#x1f332; &#x1f332;

Until this point it'd been funny, in a *Sideshow Bob stepping on rakes to the face* way. But then things take a sinister turn:

> Look, it's pretty straight forward. Just don't star a podcast. Make some pasta from scratch, like everyone else, **and shut up**. The greatest gift you can give your fellow human beings right now, is to **shut the fuck up. Shut the fuck up.** Come on. Do it. Don't start a podcast.

That sent a chill up my spine. Having a government-owned broadcaster tell you repeatedly with monochromatic video and haunting piano music to sit down, cook, and "shut the fuck up" would be some next-level *Orwell meets Menzies* if their lack of self-awareness and piss-weak delivery weren't so unintentionally hilarious.

But let's take a step back, being careful to avoid the shards of glass and the tatters of their dignity and self-respect. Wow, I'm not pulling punches today! My interest in videos like this isn't to critique bad acting or failed punchlines; I want to understand the rationale behind them. *What was the ABC's objective here?*

Occam's Razor would suggest it's merely a failed comedy sketch to drum up interest in the ABC, given the quality of the source material and the delivery. But there's a subtext here, and it's the same one the newspapers peddled about the Internet in the 1990s, and the RIAA about independent music. The unwashed masses are producing their own material which, thanks to modern computer equipment and streaming platforms, is competitive on content, quality, and/or reach. They can't respond by upping their game, so they delegitimise independent media instead, stereotyping it as the "[un]interesting" ramblings of "your mate Dave".

It reminds me of mid 2010s trolls who'd say awful or stupid things, then laugh and say it was a joke when being called out for it. Eventually Poe's Law takes effect and you wonder whether they really are joking, or if they're using it as cover. Most of these actors and TV anchors are so clueless I suspect they had no idea they were being used in this way for this surreal video. But I'll bet at least some of them did. Sit down, shut up, and become the passive consumers we tell you to be!

Am I over-analysing what was just a failed joke? Yes. Does that mean I'm equally lacking in self-awareness? Probably! But I'm not claiming to be the ultimate arbiter of interest and taste. I want you to write, start a podcast, paint, draw, code, make music, do what you love. Don't let these, to use their term, "fuckwits", guilt you away from it, *especially* during these pandemic times. Art is one of the things saving us right now.

Check out [Fireside.fm](https://fireside.fm/) if you'd like to start your own podcast. Dan Benjamin is a gentleman, and it looks like a great service.
