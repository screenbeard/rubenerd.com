---
title: "A celebratory New York cheesecake"
date: "2020-11-08T21:20:00+11:00"
abstract: "We were happy just to enjoy the news this afternoon, for once."
thumb: "https://rubenerd.com/files/2020/harris-biden-keeki@1x.jpg"
year: "2020"
category: Thoughts
tag:
- politics
- united-states
location: Sydney
---
The temptation is already there to [start worrying again](https://twitter.com/GrogsGamut/status/1325227447600644096). But for one of the few times this year, we were happy just to [enjoy the news](https://twitter.com/hughriminton/status/1325309841326268416).

<p><img src="https://rubenerd.com/files/2020/harris-biden-keeki@1x.jpg" srcset="https://rubenerd.com/files/2020/harris-biden-keeki@1x.jpg 1x, https://rubenerd.com/files/2020/harris-biden-keeki@2x.jpg 2x" alt="We called our cakes Harris and Biden." style="width:500px; height:333px;" /></p>

