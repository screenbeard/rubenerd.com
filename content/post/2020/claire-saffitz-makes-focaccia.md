---
title: "Claire Saffitz makes focaccia"
date: "2020-12-05T22:01:00+11:00"
abstract: "She's back! And she made my favourite break!"
year: "2020"
category: Media
tag:
- cooking
- claire-saffitz
- food
- video
- youtube
location: Sydney
---
Claire [is back](https://rubenerd.com/the-end-of-the-bon-appetit-test-kitchen/ "Goodbye, Bon Appétit test kitchen?"), with [her own channel](https://www.youtube.com/channel/UCvw6Y1kr_8bp6B5m1dqNyiw), and she made my favourite bread of all time! Clara and I are so happy.

<p><a href="https://www.youtube.com/watch?v=NGnMrM9qDtE" title="Play Soft & Crispy Focaccia | Claire Saffitz | Dessert Person"><img src="https://rubenerd.com/files/2020/yt-NGnMrM9qDtE@1x.jpg" srcset="https://rubenerd.com/files/2020/yt-NGnMrM9qDtE@1x.jpg 1x, https://rubenerd.com/files/2020/yt-NGnMrM9qDtE@2x.jpg 2x" alt="Play Soft & Crispy Focaccia | Claire Saffitz | Dessert Person" style="width:500px;height:281px;" /></a></p>
