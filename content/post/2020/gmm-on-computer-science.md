---
title: "GMM on computer science"
date: "2020-11-03T12:39:17+11:00"
abstract: "It’s not much of a matrix when there’s only one thing."
thumb: "gmm-on-computer-science@1x.jpg"
year: "2020"
category: Software
tag:
- good-mythical-morning
- programming
location: Sydney
---
From *Good Mythical Morning’s* episode about a [century of crisps](https://www.youtube.com/watch?v=ampZv-pi3ew).

<p><img src="https://rubenerd.com/files/2020/gmm-on-computer-science@1x.jpg" srcset="https://rubenerd.com/files/2020/gmm-on-computer-science@1x.jpg 1x, https://rubenerd.com/files/2020/gmm-on-computer-science@2x.jpg 2x" style="width:500px" alt="Link: It's not much of a matrix when there's only one thing." /></p>

    #!/usr/bin/env perl
    use strict;
    my @comestibles = ( ['crisp'] );
    print $comestibles[0][0];

Okay, he's probably right.

