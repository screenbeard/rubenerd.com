---
title: "That Outlook for Mac sloth"
date: "2020-10-15T08:15:10+11:00"
abstract: "Insert obligatory comment about how modern software has to do more, but...!"
year: "2020"
category: Software
tag:
- email
- microsoft
- outlook
location: Sydney
---
I've been giving Electron a lot of justifiable flack recently, but that's not to say native desktop software can't also feel like you're trudging through molasses.

The one that continues to amaze me is Outlook for Mac, which for `$REASONS` I still have to use in a specific capacity. Typing a new email is usually tolerable, but sometimes it can take upwards of half a second or more for each character to appear. I feel as though I need [mosh](https://mosh.org "Mosh: The Mobile Shell") for email, which is just silly.

Insert obligatory comment about how modern software is expected to do more, but I can't help remembering how much smoother Eudora, Apple Mail, and Mozilla Mail/Thunderbird felt with orders of magnitude less memory and CPU time than what Outlook has here. My Thunderbird install with two decades of mailing list messages and personal email still manages to index and run just fine with no noticeable performance impact when editing mail. Even Microsoft's own Entourage back in the day felt more performant.

And that's the rub. In the days of scarce system resources, developers seemed to grok that any real-time actions engaged by an operator needed to be fast; or at least, appear fast. We perceive graphical performance issues above all else, which is another reason why the compromises Electron apps introduce are such a monumental step backwards. If it takes indexing mail longer&mdash;I'm just hypothesising that's what causes Outlook to crawl&mdash;that's more tolerable if the UI itself is responsive.

The dream would be to go back to (Re-)(Al)Pine for mail, which just leaves the open question about shared Exchange calendars. Are there third party GUI clients that can handle these well, ideally for macOS and/or \*nix desktops? I'd pay good money for them not to have to see Outlook again.

