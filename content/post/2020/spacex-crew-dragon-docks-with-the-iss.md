---
title: "SpaceX Crew Dragon docks with the ISS"
date: "2020-06-01T00:41:23+10:00"
abstract: "I had some late night work to do this evening. Witnessing such a momentus occasion as I typed away back here on Earth was worth it :)."
thumb: "https://rubenerd.com/files/2020/EZWlxSfXQAIbdqJ.jpg"
year: "2020"
category: Thoughts
tag:
- international-space-station
- nasa
- space
- spacex
location: Sydney
---
I had some late night work to do this evening. Witnessing such a momentus occasion as I typed away back here on Earth was worth it :).

<p><img src="https://rubenerd.com/files/2020/EZWlxSfXQAIbdqJ.jpg" style="width:500px" alt="Screenshot from the NASA feed showing the SpaceX Crew Dragon docking with the International Space Station." ><br /><img src="https://rubenerd.com/files/2020/EZWlxTdXgAA25zi.jpg" style="width:500px" alt="Screenshot showing control screens for the dock procedure." /></p>

Screenshots from the NASA feed via [@LanceUlanoff](https://twitter.com/LanceUlanoff/status/1267101141277556739).
