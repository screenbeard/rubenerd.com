---
title: "The Scorpions: Wind of Change"
date: "2020-10-05T10:33:08+11:00"
abstract: "To mark the 30th anniversary of German reunification"
thumb: "https://rubenerd.com/files/2020/yt-n4RjJKxsamQ@1x.jpg"
year: "2020"
category: Media
tag:
- germany
- music
- music-monday
- politics
location: Sydney
---
Last Saturday marked the [thirtieth anniversary of German reunification](https://rubenerd.com/thirty-years-since-german-reunification/), and the end of the first Cold War. So I thought it was only fitting to share my dad's favourite tune of all time to mark the occasion.

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=n4RjJKxsamQ" title="Play Scorpions - Wind Of Change (Official Music Video)"><img src="https://rubenerd.com/files/2020/yt-n4RjJKxsamQ@1x.jpg" srcset="https://rubenerd.com/files/2020/yt-n4RjJKxsamQ@1x.jpg 1x, https://rubenerd.com/files/2020/yt-n4RjJKxsamQ@2x.jpg 2x" alt="Play Scorpions - Wind Of Change (Official Music Video)" style="width:500px;height:281px;" /></a></p>

What a fucking powerful song. And yet I fear the West has only become more authoritarian to fill the void.

