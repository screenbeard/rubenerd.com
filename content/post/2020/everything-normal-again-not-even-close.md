---
title: "Everything normal again? Sort of, maybe, kinda"
date: "2020-11-02T09:02:49+11:00"
abstract: "Back at the office full time in a country that largely handled COVID well. But then the air of unreality swoops in again."
year: "2020"
category: Thoughts
tag:
- australia
- coffee
- covid-19
location: Sydney
---
Today's my first day back at the office full time. I'm right back at the regular cafe I used to sit at before work, doing the same email and calendar prep, having the same iced long black, doing the same RSS catchup. I don't subject myself to Twitter first thing in the morning anymore, but the entire circumstance could otherwise be transplanted to January.

Wait, no it couldn't, Australia was on fire back then. Okay, late last year.

<p><img src="https://rubenerd.com/files/2020/cafe-near-office@1x.jpg" srcset="https://rubenerd.com/files/2020/cafe-near-office@1x.jpg 1x, https://rubenerd.com/files/2020/cafe-near-office@2x.jpg 2x" alt="Morning view inside the cafe." style="width:500px; height:333px;" /></p>

But that's where the air of 2020 unreality blows in. Most of the rest of the world hasn't handled Covid well enough for regular chumps like me to mostly go about their normal lives. Victoria only got to where the rest of us are thanks to a gruelling lockdown that had the talking heads in a froth. It's a similar story across in New Zealand.

I unintentionally captured it in [two operative words on Friday](https://rubenerd.com/tech-in-the-two-speed-covid-economy/)\: it feels in equal parts unreal and undeserved. I also can't shake the feeling that it's also temporary somehow. Misinformation and lies spread Covid more effectively than any bomb, but so does complacency. Let's not let it win.

