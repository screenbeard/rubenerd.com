---
title: "Turns out, not a brush with Covid"
date: "2020-12-03T09:12:00+11:00"
abstract: "Almost thought I got an email about it, and went through a whole range of thoughts."
year: "2020"
category: Thoughts
tag:
- covid-19
- personal
location: Sydney
---
I emphasise with Manton Reece's *tyranny of the blog title*; sometimes nothing really fits.

Save for a blip in Victoria which has since been flattened as well, we're *extraordinarily* lucky in Australia and New Zealand with our Covid cases. Contact tracing and home isolation has kept our new cases at zero or in the single digits for a while. I'm not sure whether our proximity to Asia made us more aware and serious earlier, or the fact we're massive islands, or strict quarantines, or that our state premiers filled the competency vacuum left by Australia's PM. I imagine we'll be analysing this for years.

I still worry that complacency will breed another spike. People here are largely living life as they did before now, albeit with government stimulus spending, check-ins everywhere, and the bulk of workers staying at home. I take the train into work a couple of days a week now, and it's still a far cry from regular morning peak hour. But shops and restaurants are full again.

It's those retail QR check-ins that led me to think something was serious in my email this morning:

> Rapid COVID-19 test

... uh oh. I went through the motions: where had I been, who had I come into contact with, where did I slip up, did my hand washing and mask reduce the chances, who do I need to call right now?!

Back in July I got news that a potential Covid case had been linked to a coffee shop I'd been to the same day. I went into isolation and ordered a mail in testing kit. It turned out to be a false alarm; the person didn't test positive, and I left the coffee shop before they arrived.

This email was also a false alarm:

> As this unforgettable year draws to a close, I’d like to take a moment to thank all of our volunteers who so generously contributed their time to the University [of Technology, Sydney]. Whilst 2020 has been full of challenges, our remarkable community has continually come together to innovate, research, mentor and support UTS philanthropic causes.

It was an alumni newsletter, with **that as the subject line**. How did the person in charge of sending that blast-out think it'd be received? Can you imagine getting that subject line flash on your phone during a meeting, and pretending to be composed while you wrack your brains with the above questions?

Me thinks that some of that innovative research should be spent on reading the room.
