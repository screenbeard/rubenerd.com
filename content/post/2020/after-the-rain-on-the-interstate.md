---
title: "After the rain on the interstate"
date: "2020-12-01T19:02:00+11:00"
abstract: "It started raining while I heard this Paul Simon song."
thumb: "https://rubenerd.com/files/2020/rainy-evening@1x.jpg"
year: "2020"
category: Thoughts
tag:
- microbreaks
- music
- paul-simon
- personal
location: Sydney
---
This would have been a great Music Monday had it been written on Monday and consisted primarily of music discussion. Scratch that, it would have made a *lousy* Music Monday now that I coach it in those terms.

I was working on the balcony this evening as I've been during since Covid started. It was another scorcher today but there was a pleasant breeze for much of the afternoon. Out of the grey I could smell that telltale ozone, then [Paul Simon chirped](https://www.youtube.com/watch?v=HD409HP-4h8 "Listen to Paul Simon's How the Heart Approaches What it Yearns") about the rain on the interstate. Then it started bucketing down.

<img src="https://rubenerd.com/files/2020/rainy-evening@1x.jpg" alt="Photo of the dark sky this evening" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2020/rainy-evening@2x.jpg 2x" />

Frank Nora of The Overnightscape would refer to this as a synchronicity, though he has more interesting and crazy ones that I seem to! But that doesn't stop me appreciating those fun little coincidences where the world lined up in a specific way that, had I not been paying attention, would have completely slipped by.

These brushes with reality make me think how many other things I'm missing out on; not by deliberate choice, but because I'm blissfully unaware. Not just the bigger picture ideas that are consequential and important to the IT industry, or the environment, or health, or anything else I care about. But the small things.

I sit out here doing work, glued to the company chat, my email client, conference calls, and my text editors. Then I pop in headphones to listen to a podcast. Heck, I was listening to this specific song to keep my energy levels up while emailing a client about the finer points of specific software licences. But then I had a [microbreak](https://rubenerd.com/microbreaks) of watching the sky and listening that ended up lasting more than a minute, and I got something great out of it.

I'd been feeling a bit melancholic all day, but feeling the rain lash the sleeves of my shirt as I sit out here under a dark sky, cowering at my laptop and finishing the last of my cold coffee, I couldn't help but smile. That silly little alignment of circumstances *absolutely* made my day, where otherwide I'd have been irritated to be wet, or at having another video conference call be interrupted. What felt like a cage out here is now something I'm enjoying. I might stay out here a bit longer.

> *The old weary traveller waits by the side of the road. [Where's he going](https://www.youtube.com/watch?v=HD409HP-4h8 "Listen to Paul Simon's How the Heart Approaches What it Yearns")?*
