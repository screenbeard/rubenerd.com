---
title: "Clickbait"
date: "2020-11-18T11:52:11+11:00"
abstract: "“The most popular painkiller on the planet is doing blah!”"
year: "2020"
category: Internet
tag:
- clickbait
- journalism
- spam
- writing
location: Sydney
---
I came across a website called BGR a few weeks ago, which I assume is pronounced *bigger*, or *site goes bgrrrrrrr*. These were the trending stories listed, all but one of which could be answered with a single phrase.

1. The most popular painkiller on the planet has been poisoning people **(Paracetamol, in high doses)**

2. This is one of the most dangerous jobs to have during the coronavirus pandemic – and now there’s proof **(Supermarket worker)**

3. I can’t stop watching this Netflix original that’s unlike anything you’ve ever seen – or heard **(Barbarians)**

4. These promising coronavirus vaccines could be approved sooner than expected – but not in the US

5. CDC study says tons of people catch COVID-19 in the one place that’s supposed to be safe **(Home)**

*[Saved You a Click](https://www.reddit.com/r/savedyouaclick/)* has done more to expose these transparent shenanigans than any other outlet. Some sites are even more brazen, or don't answer what the headline purports at all. And like my attempts at humour on this site, it's only getting worse.

But while we can point it out, the core issue remains that sites do it for ad revenue. Clicks and increasingly-problematic tracking are all that are sustaining sites like these. I don't know how we dig our way out, but the market will continue to reward this behaviour while this incentive structure exists.

*You won't believe what I'll blog about next!* By which I mean, you probably will. On average. Depeche Mode. Statistically speaking.

