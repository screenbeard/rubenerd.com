---
title: "John Naughton on the Internet-of-Things"
date: "2020-10-04T12:00:08+11:00"
abstract: "“The totally secure networked device has yet to be invented.”"
year: "2020"
category: Internet
tag:
- privacy
- security
location: Sydney
---
John wrote this at the end of an *excellent* article about a [specific piece of home surveillance](https://www.theguardian.com/commentisfree/2020/oct/03/why-amazons-home-security-drone-should-set-off-alarm-bells) tech, but it could easily apply everywhere.

> This kind of thing is par for the course. The totally secure networked device has yet to be invented. And the standard response of the industry is always to shift the blame to users who have failed to take appropriate security precautions. When “smart” devices render their hapless users vulnerable, somehow it’s always the customer’s, rather than the vendor’s, fault. So here’s a useful motto when tangling with this stuff in future: for “smart” read untrustworthy.

I keep quoting George Neville-Neil saying the "S in IoT stands for Security". I'm realising the P in it stands for Privacy, too.

There are ways to use some of this tech responsibly. Disabling features, feeding garbage data, putting on isolated networks, obsessively tracking updates. But we're kidding ourselves if we think non-technical people would know they need to do this, and how.

