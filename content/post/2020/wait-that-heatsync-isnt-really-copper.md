---
title: "Wait, that heatsync isn’t really copper?"
date: "2020-10-26T09:18:20+11:00"
abstract: "So dodgy!"
thumb: "https://rubenerd.com/files/2020/fake-copper@1x.jpg"
year: "2020"
category: Hardware
tag:
- networking
- shopping
location: Sydney
---
With summertime approaching in Sydney&mdash;though you wouldn't know it looking given the cold we've had over the weekend&mdash;I've been looking to better protect some of my homelab gear. Ideally it'd all be co-located in a climate-controlled data centre, but where's the fun in that? And besides, our FreeBSD bhyve PleX OpenZFS NAS now also runs Minecraft locally.

We all have CPU and GPU cooling sorted, which leaves integrated circuits on network cards. These beasts can easily run hot enough to compromise their performance, especially when you start pushing lots of traffic in summer months. The chips on my InfiniBand and Ethernet cards routinely hit 60°C, making them the hottest-running components outside the CPU.

My ultimate plan is to address airflow shortcomings with a proper new server build and case, but for now I just wanted some heatsyncs to alleviate some of the problem. Nobody (that I know) sells cooling devices specifically for NICs, but ones designed for GPU DIMMs seem to be the right size.

<p><img src="https://rubenerd.com/files/2020/fake-copper@1x.jpg" srcset="https://rubenerd.com/files/2020/fake-copper@1x.jpg 1x, https://rubenerd.com/files/2020/fake-copper@2x.jpg 2x" alt="Photo from the seller of the heatsyncs which look an awful lot like copper." style="width:500px" /></p>

So when I saw the thumbnail for the above copper ones I was ready to hit buy. Until I looked closer at the specifications:

> Color: Copper Tone

Wha? Sure enough, right there in the description:

> Copper Tone Aluminum Heatsink

I have many questions. Wouldn't painting the metal reduce its thermal efficiency? What would be the point of saving a few dollars to presumably impress people with your fancy PC, if you've spent thousands on the other components? It's like those people in Mercs or BMWs waiting for the ERP to end in Singapore.

The real reason is they know that a certain percentage of their buyers will mistake it for real copper, and will buy because they sorted by price with the keyword *copper*. Those buyers will either spend their days unaware that their expensive devices aren't being protected as much as they thought, or likely wouldn't bother going through a returns policy for a few dollars once they've realised they've been duped.

The seller could argue they included the words *Copper Tone*, thereby complying with the letter of the law and relevant terms of service, if not the spirit. In the indelible words of John Siracusa, being technically correct is necessary but not sufficient.

