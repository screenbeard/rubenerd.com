---
title: "My new-ish 16-inch MacBook Pro"
date: "2020-10-02T22:00:36+10:00"
abstract: "I’m back to my lightweight FreeBSD laptop and Mac desktop replacement arrangement that worked so well before."
thumb: "https://rubenerd.com/files/2020/photo-macbookpros@1x.jpg"
year: "2020"
category: Hardware
tag:
- apple
- macbook-pro
- macos
- nostalgia
location: Sydney
---
This is the first blog post written on my *Apple Certified Refurbished* 16-inch MacBook Pro!

The 15-inch machine on the right was the first Intel Mac laptop Apple ever released. She managed to merge the portability of my polycarbonate iBook G3, with the performance of my PowerMac G5. I was [at uni in Adelaide at the time](https://rubenerd.com/universal-binaries-for-mozilla-software/), but had to regularly fly back to Malaysia and Singapore to help during my mum's last years, among other tough family reasons. She was the right machine at the right time for my new life living out of a suitcase.

<p><img src="https://rubenerd.com/files/2020/photo-macbookpros@1x.jpg" srcset="https://rubenerd.com/files/2020/photo-macbookpros@1x.jpg 1x, https://rubenerd.com/files/2020/photo-macbookpros@2x.jpg 2x" alt="The refurbished 16-inch from 2019 alongside my 2006 15-inch MacBook Pro." style="width:500px" /></p>

I've had a few 13-inch MacBook Pros and a Retina iMac in the intervening years, but none matched the versatility of *MacTheKnife*. She had a nice keyboard, her big screen meant I didn't miss external ones, her trackpad is still more sensitive and accurate than any PC, and her Intel CPU let me run various x86 OSs in VMs for experimentation, homework, and games. I still have the disk image from my FreeBSD 6.3 VM with Xfce and a lighttpd web server in a nostalgia archive.

I kept a tiny ThinkPad X40 as my on-call carry, and often left the Mac as my desktop replacement at home. I tried merging the two together in a 13-inch for much of the 2010s to save money and effort, but it never worked well for me. That form factor was too small to use remote for long stretches, and too heavy to always be in my bag.

The tide started to turn again when Clara and I were back in Akihabara last year... remember travel? I caught a glimpse of this *gorgeous* 775g Panasonic Let's Note laptop with a high-resolution screen and full FreeBSD hardware support. Much of my writing and SSHing is done on it now, just as I did with my old ThinkPad ultra-portables.

So when it finally came time to upgrade my old MacBook Pro with its awful keyboard and awkward&mdash;for me&mdash;form factor, I kept the money in savings and set up watch alerts on Apple's refurbished hardware page for my 16-inch grail machine. It took a few months, but she popped up last week and I pounced!

As everyone said last year, her keyboard is fantastic, and she still has a better display than almost every other machine in the industry. She renders my static Hugo blog in 9 seconds compared to 20 on my 13-inch. And the *RAM!* Electron "software" made me forget what spare RAM feels like.

I took her to a coffee shop this morning, and was suddenly sent back in time to that Coffee Bean and Tea Leaf I used to sit with my mum at in KL after her chemo. I'd be sitting there typing away, and she'd be there with a Woodhouse book quoting me silly passages. All my machines have since been giving weeby anime hostnames, but this one has definitely earned the moniker *MacTheKnifeII.*

🌲 🌲 🌲 

Do I regret not waiting for an Apple Silicon Mac? Not really. I couldn't wait much longer to replace my other machine, and whatever Apple releases will undoubtedly have waiting lists and shipping times even after they announce specifics. The best computer is the one you have.

Having an Intel CPU is also *feature* for me, at least at this stage. She runs all the software I want and need to. I paid the extra for the 8 GiB GPU option, and even with the overhead of Parallels she runs the few games I care about almost as well as the dedicated game machine I rarely use and will now be selling for money and space.

As that first MacBook Pro was, this is the best machine for me right now. Maybe in a few years when Intel macOS ceases I'll review my options. But who knows, by then I might have even decided to move entirely off the Mac. I've been in the ecosystem since Classic MacOS in the 1990s, but I've always had one foot in the lifeboat in case things go pear-shaped.

And dare I say it, it's fun having the very first and very last Intel laptop. It feels like I have the bookends for this era of computing.

