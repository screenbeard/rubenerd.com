---
title: "Mac Dock persisting in Full Screen mode"
date: "2020-10-11T16:49:31+11:00"
abstract: "And insert obligatory comment about OPENSTEP and GNUstep having a better dock than a billion dollar company."
year: "2020"
category: Software
tag:
- apple
- bugs
- gnustep
- macos
- troubleshooting
location: Sydney
---
My new-ish 16-inch MacBook Pro has this bizarre UI quirk where the Dock will persist on the screen even when entering Full Screen mode. This is especially problematic when you align it to the correct, left-side of your screen, because it's guaranteed to have text on it:

<p><img src="https://rubenerd.com/files/2020/screenshot-dock-in-full-screen.png" style="width:500px; height:200px;" alt="Screenshot showing the aforementioned issue." /></p>

Force-quitting the Dock stops this happening:

    $ killall Dock

But the behaviour returns after a restart. Hopefully the next Catalina update resolves this, or I may resort to clearing my user account and starting again... it's *that* annoying.

Insert obligatory comment that the NeXTSTEP/OPENSTEP dock was better than the Mac one, despite Apple having had two decades to fix and improve it. Stacks are about all the features I can think of that Apple have added in this entire time.

I should do a FreeBSD GNUstep follow-up post at some point.

