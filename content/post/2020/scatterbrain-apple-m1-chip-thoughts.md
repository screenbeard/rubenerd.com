---
title: "Scatterbrain Apple M1 chip thoughts"
date: "2020-11-13T10:12:39+11:00"
abstract: "My first-gen MacBook Pro looked the same as a PowerBook G4."
year: "2020"
category: Hardware
tag:
- apple
- arm
location: Sydney
---
Apple's new CPU and hardware announcement was full of interesting tidbits, though I think I've talked about the broader implications enough here over the last few months. These are just some final closing thoughts for now, in no particular order:

* I'm always happy to see an end to monocultures. Macs used to be more interesting *because* they ran a different CPU architecture, and diversity benefits people who don't even use it.

* Benchmarks on somewhat comparable A14 hardware look promising, with certain emulated amd64 instructions outperforming native Intel CPUs.

* People whinging that the hardware looked the same clearly hadn't studied Apple's hardware transition strategies. My first-generation MacBook Pro looked the same as a PowerBook G4.

* Microsoft have announced x86-on-ARM emulation, which could signal a future Boot Camp scenario, and an out for those of us who bought Intel MacBooks for this compatibility.

* I keep thinking of the Singaporean telco when reading these headlines. Given their old branding had a sun, something something heat and cooling and something something SPARC.

* I'm still undecided if I'll stay on Macs long term. I've been an Apple user since the classic Mac days of the 1990s, but the Venn diagram of what my FreeBSD laptops and what my Macs can do is closing faster than I expected. [Antranig Vartanian](https://antranigv.am/weblog_en/posts/freebsd-signal-cli-scli/) ([RSS feed here](https://antranigv.am/weblog_en/index.xml)) has been especially inspiring in this regard.

