---
title: "OpenSSL 3.0 /dev/crypto issues on FreeBSD"
date: "2020-10-28T14:57:59+11:00"
abstract: "John-Mark Gurney tweeted a thread from freebsd-hackers."
year: "2020"
category: Software
tag:
- bsd
- freebsd
- openssl
- security
location: Sydney
---
John-Mark Gurney [shared this worrying mailing list find](https://twitter.com/encthenet/status/1321162232378486784?s=20)\:

> So, just learned that the OpenSSL devs decided to break /dev/crypto on FreeBSD.

Benjamin Kaduk [posted this output](https://docs.freebsd.org/cgi/getmsg.cgi?fetch=97291+0+current/freebsd-hackers):

    $ openssl speed -evp aes-128-cbc -engine devcrypto    
        
    82677: openat(AT_FDCWD,"/dev/crypto",O_RDWR,00)  = 3 (0x3)
    82677: ioctl(3,CIOCGSESSION,0x7fffffffde70)      ERR#22 'Invalid argument'
    82677: ioctl(3,CIOCGSESSION,0x7fffffffde70)      ERR#22 'Invalid argument'
    82677: ioctl(3,CIOCGSESSION,0x7fffffffde70)      ERR#22 'Invalid argument'
    82677: ioctl(3,CIOCGSESSION,0x7fffffffde70)      ERR#22 'Invalid argument'
    82677: ioctl(3,CIOCGSESSION,0x7fffffffde70)      ERR#22 'Invalid argument'

John raised using LibreSSL in FreeBSD if non-Linux compatibility isn't a priority anymore. Michael Warren Lucas responded in the Twitter thread with something I wasn't aware of:

> The support cycle on LibreSSL is shorter than a FreeBSD release's lifetime, which means they won't switch.

This is the manifestation of what I [just talked about](https://rubenerd.com/antranig-vartanian-on-rss-macos-apple-silicon/ "Antranig Vartanian on RSS, HiDPI, Apple Silicon"). I didn't think I'd get a specific example again so quickly.

> As a FreeBSD guy as well as a Mac user, I agree that among the biggest challenges today are Linux-first/only development, as opposed to thinking about the underlying architecture.

We all realised how perilously under-resourced and staffed the OpenSSL project was during the [Heartbleed days](https://rubenerd.com/the-openssl-heart-bleeds/). Is this another manifestation of that, or are we just witnessing yet another project that preferences Linux above \*nix?

