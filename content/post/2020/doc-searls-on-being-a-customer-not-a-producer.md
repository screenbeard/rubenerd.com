---
title: "Being a customer, not a producer"
date: "2020-11-06T13:26:11+11:00"
abstract: "Doc Searls discussed online video conferencing, which I think applies to chat apps and incentives in general."
year: "2020"
category: Internet
tag:
- doc-searls
- open-web
- standards
- video
location: Sydney
---
Doc Searls ([RSS feed here](https://blogs.harvard.edu/doc/feed/)) compared the current state of video conferencing tools to Microsoft's web browser in the 1990s, in a post titled [The smell of boiling frog](https://blogs.harvard.edu/doc/2020/09/08/frog/)\:

> What saved the personal computing world from being absorbed into Microsoft was the Internet—and the Web, running on the Internet. The Internet, based on a profoundly generative protocol, supported all kinds of hardware and software at an infinitude of end points. And the Web, based on an equally generative protocol, manifested on browsers that ran on Mac and Linux computers, as well as Windows ones.
> 
> But video conferencing is different. Yes, all the popular video conferencing systems run in apps that work on multiple operating systems, and on the two main mobile device OSes as well. And yes, they are substitutable. [..] But all of them have a critical dependency through their codecs [..] While there are some open source codecs, all the systems I just named use proprietary (patent-based) codecs. 

It's a similar situation to chat apps. We used to have Jabber and IRC. Now we have Slack, Discord, and the like. And as Doc says, this has turned us from being producers to customers. This isn't an accident, the companies behind these tools explicitly use the latter term.

It reminds me of the first trip I took on a train in Sydney, when the automated PA said "Good evening *customers*". It's haunted me since why they don't call us *passengers*. A reader of this blog back in the day&mdash;apologies, I forgot which one of you lovely people it was&mdash;said it was a way to control messaging and set expectations. Passengers use trains to get to a destination. A customer has merely paid to get through a turnstile. I think there's something to that.

But back to video conferencing, I think this even goes beyond codecs. Like online advertising and tracking, I keep coming back to incentive structures, or what push and pull factors motivate businesses to behave the way they do. A closed chat app is easy to sell as a product&mdash;another term I'm beginning to distrust&mdash;to customers, which venture capitalists and IPOs reward. VC-funded projects with open protocols exist, but it's a much tougher sell.

Just as I've said in the context of FreeBSD versus Linux, open protocols do exist, but they can't hope to match the marketing budgets of these million dollar companies. Until these pervasive incentives can be tweaked, I doubt we'll see this change. But Doc leaves us with another thought:

> Do we have to [lose out here]? I mean, it’s still early. The digital world is how old? Decades, at most. And how long will it last? At the very least, more than that. Centuries or millennia, probably. So there’s hope.

And another point to consider: what has survived all these fads and changes in tools? The Internet itself.

