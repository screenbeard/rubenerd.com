---
title: "Unlearning unsaved unbuffers"
date: "2020-11-29T12:20:41+11:00"
abstract: "I wasn't expecting Emacs to help me in this new way either."
year: "2020"
category: Software
tag:
- emacs
- workflows
location: Sydney
---
The title was supposed to be *Unlearning unsaved buffers*, but I couldn't resist adding the same prefix to the last word, even though it makes no sense. Or *especially* because it makes no sense. Or cents. Or pounds. Pounding this joke, or flogging it, despite it not being funny. Or *especially* because it isn't funny. Oh no, I've internalised [that ABC skit](https://rubenerd.com/dont-start-a-podcast/)!

I've made the switch to [org-mode](https://orgmode.org/) in [Emacs](https://www.gnu.org/software/emacs/) from [nvALT](https://brettterpstra.com/projects/nvalt/) to capture, store, and search for notes. I plan to write a proper series of posts about it; no one plugin or program has mapped with how my mind works so well in years. It's also one of the last Mac-specific tools I've been able to replace with a cross-platform one.

But in doing this I discovered another weird habit that, with hindsight, made absolutely no sense. For certain types of notes I was opening MacVim instead, and immediately typing in a new buffer. Then I'd inevitably need to [context switch](https://rubenerd.com/context-switching/) a few times, which necessitated opening even more buffers for even more disconnected thoughts. Suddenly I'd be staring down the end of the day, with all these unsaved buffers hovering around my mind and computer screen, as if mocking me for being so scatterbrain.

Which leads me to how Emacs has helped me in another way I didn't expect. You're presented with a splash screen and scratch space when you open it by default, but you either need to provide it a filename with `C-x C-f` to start writing, or invoke Emacs with a file path. This flew in the face of the above habit which initially frustrated me, but now I start each note and file with a deliberate subject and folder! Also telling, I rarely need to revise the name either, showing that the filename itself is useful for framing what I'm about to write, and setting the scope. Now I reach the end of the day, and I have more org-mode nodes, and well structured text files.

Tools can be used to enforce bad behaviors or learn new ones. The trick is figuring out which is which.
