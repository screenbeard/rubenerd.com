---
title: "Equinix data centre advice"
date: "2020-11-01T10:48:54+11:00"
abstract: "In the event of protests or other activity related to the U.S. election..."
year: "2020"
category: Hardware
tag:
- data-centres
- politics
- united-states
location: Sydney
---
Aiyo. All I can think to say.

> Dear Equinix Customer,
>
> In the event of protests or other activity related to the U.S. election is proximate to our IBX locations, please be advised Equinix has business continuity plans in place to ensure our IBXs remain fully operational and to safeguard access to our data centers or office locations.
>
> If protests or related road closures or curfews occur, we will make customers aware of the situation and any impacts to IBX Access via an IBX Advisory - as is our standard practice.
>
> Our sites remain fully operational, and Equinix is prepared to maintain the necessary on-site staffing levels required to support continuous operations, which include Smart Hands services should customers opt for remote services rather than scheduling a visit to the IBX.
>
> In keeping with our current COVID-19 health and safety protocols, appointments are required.

