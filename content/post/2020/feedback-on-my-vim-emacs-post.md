---
title: "Feedback on my Vim post"
date: "2020-11-12T14:21:14+11:00"
abstract: "Thanks to Jim Kloss, Jonathan H., Rebecca Hales, and everyone else who emailed comments."
year: "2020"
category: Software
tag:
- editors
- emacs
- vim
location: Sydney
---
[My Vim post on Monday](https://rubenerd.com/thinking-out-loud-about-vim/) generated more positive comments than I expected. Most of you were either Vim users who understood why it's not a fit for everyone, and non-Vim users making suggestions. I'm not sure how I'm going to react if you all keep being this civil.

Whole Wheat Radio alumni, NOCHANGE BBS maintainer, and XCHANGE author Jim Kloss [weighs in](https://twitter.com/JKloss4/status/1325960447938490369) by scaring me about the responsibility he's bestowing upon me:

> I look forward to reading more from @Rubenerd about his editor re-think. Great analogy about drums v. pianos.  I take Ruben's software suggestions very seriously because they often save me weeks of learning/testing.

Jonathan H. referred me to [this helpful Hacker News comment](https://news.ycombinator.com/item?id=25000709) by adimitrov about moving to Emacs, given I already use it for org-mode:

> I started using Vim in mid/late 2000s, and successfully switched to Emacs. Without knowing your specific gripes, it's hard no know what'll end up helping you, but here are my two cents:
> 
> - use emacsclient and have aliases for emacsclient -c and and emacsclient -n for popping up a new frame or using the console, respectively. I even have a window manager binding to open a new Emacs client window
> 
> - Rainer König is the best at getting across org mode workflows. if you like watching nerdy videos, go watch him.
> 
> - keep vim around, I still use it, sometimes, but with no or veery minimal config.
> 
> - centaur tabs and the new tab stuff can help vim people who like tabs. I just got used to buffers.
> 
> - M-x is really Emacs' primary UI. don't try to think of a million and one key bindings up front, just bind what you find yourself using M-x a lot for. You just need a nice completing read like ivy, helm or so, but doom has that.
> 
> - use magit. While many claim that org-mode is the Emacs killer feature, I'd say magit is even more important if you code. There simply is no better git interface, nothing comes close. You think git the new porcelain is cool? Magit is a git jacuzzi. 

A gentleman, name withheld, emailed saying I should try [Spacemacs](https://www.spacemacs.org/) if I wanted a more intuitive editor. I still think I want to give vanilla [GNU Emacs](https://www.gnu.org/software/emacs/) the old college try, but I'll keep that in mind :).

And Rebecca Hales, who keeps wanting to [dress us in cute things](https://rubenerd.com/rebecca-hales-on-manga-fitness/) to make us exercise:

> Emacs? Repent! But seriously... use what's best for you.

There's a wider lesson there. But point taken. `C-x C-c`.

