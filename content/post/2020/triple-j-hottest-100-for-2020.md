---
title: "Triple J’s Hottest 100 for 2020"
date: "2021-01-30T09:34:58+11:00"
abstract: "Tame Impala, Lime Cordiale, Glass Animals, and Dan Andrews."
thumb: "https://rubenerd.com/files/2021/yt-yw5xaVFol9M@1x.jpg"
year: "2021"
category: Media
tag:
- australia
- covid-19
- glass-animals
- music
- tame-impala
- victoria
location: Sydney
---
Australia's national music countdown doesn't usually have the genres or acts I'm interested in, but music from last <del>beer</del> year had a few fun <del>cases of beer</del> surprises.

I *love* Tame Impala, and his "[Is It True](https://www.youtube.com/watch?v=qLGwIHjhboA)" made it to #17. But I can see why "Lost in Yesterday" made it to number #5.

<p><a href="https://www.youtube.com/watch?v=C7VlC0QjdHU" title="Play Tame Impala - Lost in Yesterday (Official Audio)"><img src="https://rubenerd.com/files/2021/yt-C7VlC0QjdHU@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-C7VlC0QjdHU@1x.jpg 1x, https://rubenerd.com/files/2021/yt-C7VlC0QjdHU@2x.jpg 2x" alt="Play Tame Impala - Lost in Yesterday (Official Audio)" style="width:500px;height:281px;" /></a></p>

The Australian state of Victoria had its premier Dan Andrews' [Covid press conferences](https://www.youtube.com/watch?v=T0NFqOHFJvw), and an off-handed comment about *getting on the beers*, spun into this song by Mashd n Kutcher and released after the Melbourne lockdowns. I was fully expecting it to chart, but appearing at #12 demonstrated just how much this has been on everyone's mind. I think we all needed a bit of silly levity... and the man himself [took it well](https://twitter.com/DanielAndrewsMP/status/1352890342698172416?s=20)!

*(It reminded me of [Pendulum's Australian ABC News theme remix](https://www.youtube.com/watch?v=3iysSJ51sRc). The fact it didn't chart on the Hottest 100 of the Decade was a travesty).*

<p><a href="https://www.youtube.com/watch?v=yw5xaVFol9M" title="Play Get on the Beers (feat. Dan Andrews)"><img src="https://rubenerd.com/files/2021/yt-yw5xaVFol9M@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-yw5xaVFol9M@1x.jpg 1x, https://rubenerd.com/files/2021/yt-yw5xaVFol9M@2x.jpg 2x" alt="Play Get on the Beers (feat. Dan Andrews)" style="width:500px;height:281px;" /></a></p>

I'd thought Lime Cordiale's "Addicted to the Sunshine" was such a chill song, but I completely missed the environmental subtext until seeing the music video while writing this disjointed blog post... wow.

<p><a href="https://www.youtube.com/watch?v=iUeubIPHH6o" title="Play Lime Cordiale - Addicted to the Sunshine (Official Video)"><img src="https://rubenerd.com/files/2021/yt-iUeubIPHH6o@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-iUeubIPHH6o@1x.jpg 1x, https://rubenerd.com/files/2021/yt-iUeubIPHH6o@2x.jpg 2x" alt="Play Lime Cordiale - Addicted to the Sunshine (Official Video)" style="width:500px;height:281px;" /></a></p>

But I was most surprised to see the British group Glass Animals take out number 1 with "Heat Waves". Their *How To Be a Human Being* album was what my 2018 sounded like, but I didn't even know they had a new album last year. It's a great track with that subtle electronic weirdness they're known for.

<p><a href="https://www.youtube.com/watch?v=WgiWUnJqy3k" title="Play Glass Animals - Heat Waves"><img src="https://rubenerd.com/files/2021/yt-WgiWUnJqy3k@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-WgiWUnJqy3k@1x.jpg 1x, https://rubenerd.com/files/2021/yt-WgiWUnJqy3k@2x.jpg 2x" alt="Play Glass Animals - Heat Waves" style="width:500px;height:281px;" /></a></p>

