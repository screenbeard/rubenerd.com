---
title: "OLED-sensitive people left out from the iPhone 12"
date: "2020-10-15T08:36:53+11:00"
abstract: "That leaves the iPhone SE as the last current one we can use."
year: "2020"
category: Hardware
tag:
- apple
- iphone
- oled
- lcd
location: Sydney
---
The iPhone 12 is the first model in the phone's history not to ship with an LCD option. This small detail has been lost in the regular fray of frantic iPhone 12 coverage, but it's a big deal for those of us [photosensitive to OLED screens](https://rubenerd.com/why-oled-phone-screens-suck-for-some-of-us/).

If you haven't seen my earlier posts about this, OLED screens flicker uncomfortably for some of us, especially in low light and when being moved. This is amplified when holding a phone that *literally* moves in your field of vision as a function of its regular operation. The visual sensation can cause headaches even after a short time; I get them because I find focusing difficult, which irritates my eyes and mimics the unsettling colour shimmer I get at the onset of a migraine.

The iPhone X, XS, and 11 shipped with LCDs on their low-end models, which I attributed to Apple acknowledging this health issue for a subset of their customers. It also helped them differentiate the product line by selling the OLED's bolder colours and better contrast as premium features to people with normal peepers.

The iPhone 11 is still being sold, but the 12 signals the end of the line for LCDs in their mainstream phones. I can see why we're unimportant in a business sense, but it still makes me a bit sad.

It seems Apple's answer for us now is the iPhone SE, the current model of which still ships with an LCD. This works for me, as it's the same size as my current iPhone 8, and weighs less than the house bricks of the larger models. It's also cheaper by Apple standards, and still has Touch ID which I prefer because it can be unlocked as I take it out of my pocket. But I can see&mdash;hah!&mdash;how mobile enthusiasts who want the latest camera optics and goodies would be disappointed that they're locked out now.

I'm also keeping an eye&mdash;hah!&mdash;out for Android product lines that still feature LCDs as escape plan, should Apple abandon LCDs entirely. Android has always been a basketcase, but at least it has multiple vendors going for it. I've always been partial to mobile Sony hardware going back to the Clié; is there a modern LCD Xperia? Or I wonder if there's a nice Blackberry-style one? 

