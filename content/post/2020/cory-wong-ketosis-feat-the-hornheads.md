---
title: "Cory Wong: Ketosis (feat. The Hornheads)"
date: "2020-12-14T14:25:00+11:00"
abstract: "Everything is better with brass!"
year: "2020"
category: Media
tag:
- brass
- cory-wong
- funk
- independent-music
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday) is proof of Rubenerd's Second Law: *Everything is better with brass!* Cory Wong is fantastic; you should definitely [support him on Bandcamp](https://corywong.bandcamp.com/).

<p><a href="https://www.youtube.com/watch?v=VVM8bDWHfOE" title="Play Cory Wong // Ketosis (feat. The Hornheads)"><img src="https://rubenerd.com/files/2020/yt-VVM8bDWHfOE@1x.jpg" srcset="https://rubenerd.com/files/2020/yt-VVM8bDWHfOE@1x.jpg 1x, https://rubenerd.com/files/2020/yt-VVM8bDWHfOE@2x.jpg 2x" alt="Play Cory Wong // Ketosis (feat. The Hornheads)" style="width:500px;height:281px;" /></a></p>
