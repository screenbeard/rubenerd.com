---
title: "Antranig moving from macOS to FreeBSD"
date: "2020-12-08T08:18:00+11:00"
abstract: "Antranig Vartanian writes about his frustrations with the direction of the Mac, and I'm starting to agree."
year: "2020"
category: Software
tag:
- apple
- freebsd
- macos
location: Sydney
---
Antranig Vartanian [wrote about his jump from macOS to a FreeBSD ThinkPad](https://antranigv.am/weblog_en/posts/macos_to_freebsd/) at the end of November. It comes after a year or so of tweets and comments to my blog here discussing his increasing frustration at where the Mac platform is headed, and I find it hard to fault him.

His main reasons for the switch came down to macOS becoming less Unix-y, with services he doesn't need and an outdated userland; Big Sur's frustrating new interface; Linux and Windows having their own problems; and his working knowledge of FreeBSD on the server making the switch easier. I've noted the same trends, and have similar views.

Mac users like us have felt the ebbs and flows of the platform over the years, and the announcement of the M1 chip has reaffirmed the company's interest in the Mac after a lost decade chasing iOS instead. My current Intel 16-inch MacBook Pro is a great machine, and all the benchmarks suggest the M1 based Macs absolutely *smoke* any PC in their class. Almost all PC laptops [have crappy screens](https://rubenerd.com/foss-laptops-and-subpar-displays/) with bad colour, poor viewing angles, and low resolutions; and their trackpads don't come close to matching the sensitivity and precision of what Apple had a decade ago. 

*(Font rendering is another sticking point. Ironically, \*nix desktops still have better looking fonts than Windows. It's another thing you become accustomed to on macOS, and the standardisation of their Retina screens only makes the difference more stark when you go back to PCs).*

But software is another story, and I'm tending to lean in the direction of Antranig here from a usability perspective. Every <a href="https://www.apple.com/macos/big-sur/">Big Sur screenshot</a> I've seen has reminded me of how I blanched seeing Windows Aero in Vista/7 and Metro in 8/10. We're by no means in the minority here; prominent Mac developers have voiced their own frustrations at the seeming iOS 7-ificiation of the platform and the throwing away of their own user interface guidelines.

Apple have their reasons for doing this, but it shows their priorities for the platform are drifting further away from what users like me buy their kit for: a portable Unix workstation where I spend my time working and playing a few games rather than constantly tinkering with window managers and drivers. This wasn't a coincidence or convenient alignment of our priorities, Apple used to heavily advertise macOS's certified Unix pedigree. They're the only ones left doing this after SGI and Sun.

I've got it to the point where I can configure a new Mac, save for updates, within an hour. Homebrew Cask lets me install graphical packages, and I lean heavily on NetBSD's pkgsrc tooling for everything else. I've somehow avoided the headaches others in the community have had with VPN configs, firewall rules, OpenSSH, and others. Maybe I've just internalised so much of it from two decades of use.

That said, I've been careful to only use cross-platform software where I can, save for a few work-specific tools. Emacs, of all things, removed some of the last Mac-specific tools I used for years for note taking, reading RSS, using IRC, and expanding snippets. My Venn diagram of what my on-call Panasonic Let's Note laptop with FreeBSD can do, and what my Mac can, overlap more than ever before.

I'm glad to see gentleman like Antranig are able to productively move onto a FreeBSD desktop that works better for them. I'm sticking with the Mac for now, but it sounds like all I need is some more compelling hardware. I sure wouldn't mind more upgradeable ones!
