---
title: "Band names in console output"
date: "2020-10-20T09:08:01+11:00"
abstract: "oksh: cd: bad substitution"
year: "2020"
category: Software
tag:
- bsd
- freebsd
- macos
- shell-scripts
location: Sydney
---
The coffee hadn't taken effect when I tried doing this:

    $ cd ./script.sh
    ==> oksh: cd: bad substitution

Ladies and gentleman, it's *Bad Substitution* with their new hit, "Out of Bounds". It's Savage Garden with a bit of Lewburger.

I'm at the point in my life where I hear songs when I hear error messages, then wonder in which dream I first heard it. Is that concerning?

