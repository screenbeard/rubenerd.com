---
title: "An online place of record"
date: "2020-11-25T16:12:00+11:00"
abstract: "We don't need the blessing of large commercial sites to validate our ideas."
year: "2020"
category: Internet
tag:
- dave-winer
- indie-publishing
- weblog
location: Sydney
---
It doesn't seem all that long ago when everyone and their caninies were writing for Medium, the pseudo-blog that anyone could publish on. I chose my words carefully there; you're a writer *for* Medium, and it's not a true blog given you surrender full control and ownership of your writing. This is what sets all major social networks apart from blogs, even if they still present thoughts in a chronological order.

*(There's debate whether sites that aren't chronoligically ordered can still be classified as a blog, or whether it's a personal site or wiki. I would have hung my hat on the above definition before, but now I think it's far more useful to talk about them in the context of control. Which is great: no one company or entity owns the term or its definition)*.

Dave Winer [noticed a trend yesterday](http://scripting.com/2020/11/23.html#a154801), though it's not a step forwards. Emphasis added:

> Very quietly Medium is no longer the online place-of-record, which is good, because as a money-losing tech startup they have **no obligation to maintain a record**.The bad news is that the new incumbent is yet another tech startup. If we want the net to work, we need some institutions that are not-for-profit. archive.org is a good backstop. But the web imho deserves more.

What Dave implies here is what we've see time and time again: sites that absorb all this creative energy from people are shut down at the behest of their owners. Because the owners aren't us, and their motives lie elsewhere. I don't worry if Medium will be shut down, I'm counting the days.

But I think it goes even beyond whether something is for profit or not. Look at the major feature that every social network has been pushing hard lately: the idea of the ephemeral story. Snapchat were the first major platform to popularise it, and Twitter have just added it. There's something almost Bhuddist about the idea of living in the present moment with these works and throwing them away, though I doubt sharing your exploits with the world minute by minute to chase a dopamine hit is! What it's *really* doing is conditioning those of us outside traditional media that our material is disposable. We saw the current landscape, and somehow managed to peddle backwards even faster.

I happen to think everyone has something interesting to say, and there's a larger social good and necessity for records after our time has passed. But in the 2010s and early 2020s we're stuck in this rut that we need the blessing of a large commercial website to validate and publish what we think. I agree with Dave; the people who make up the web deserve more.
