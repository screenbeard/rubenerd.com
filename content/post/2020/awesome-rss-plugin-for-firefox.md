---
title: "Using RSS: Awesome RSS plugin for Firefox"
date: "2020-11-17T09:01:00+11:00"
abstract: "Put the web feed icon back in your toolbar."
thumb: "https://rubenerd.com/files/2020/icon-firefox@2x.png"
year: "2020"
category: Software
tag:
- extensions
- firefox
- plugins
- rss
location: Sydney
---
I've been extolling the virtues, utility, and freedom of RSS of late, but not so much *how* to use it. I'm addressing this in a new series of posts, some of which I may end up collecting into a set of links for my [help page](https://rubenerd.com/subscribe/).

For a few years browsers included icons for RSS, both to raise awareness of the protocol, and so you could easily add them to your aggregators. Even Internet Explorer! Felicia Day's *[Awesome RSS](https://addons.mozilla.org/en-GB/firefox/addon/awesome-rss/)* for Firefox returns that functionality, and with no extra bells and whistles. It's great not having to mess around in HTML headers again for something that should have an easy link.

Mozilla should make this a mandatory or pre-installed plugin if they're as strong an advocate for the open web as they say they are. Or they should return the functionality to the core web browser. Compared to *so much* they've added and changed lately, this would be a drop in the feed bucket.

