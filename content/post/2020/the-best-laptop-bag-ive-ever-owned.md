---
title: "The best laptop bag I've ever owned"
date: "2021-01-10T09:17:00+11:00"
abstract: "I would not have expected this!"
thumb: "https://rubenerd.com/files/2021/arena-backpack@1x.jpg"
year: "2021"
category: Travel
tag:
- bags
- clothing
- design
---
Laptop bags have a special place in my life and memory. They've always been there during tough times, when I was studying overseas, when travelling, going to work, or even just making my daily trips to coffee shops to write. I haven't had that many bags, given my odd attachment to the ones I end up getting.

The first bag I ever remember carrying daily was a beautiful brown and white wool backpack my dad brought back from India. I didn't have a laptop at the time&mdash;those were still unusual and expensive in the 1990s!&mdash;but it hauled my books, tapes, Little Bear, my monochrome Palm III PDA, and my Australian Geographic all-in-one gadget with a ruler, magnifying glass, and kitchen sink. It was usurped by my official school bag when I started year 7 and needed to carry textbooks all the time.

I used a large orange Crumpler shoulderbag after graduating, because it could fit my then-new 15-inch MacBook Pro. It was durable and copped a lot of abuse, especially when I started studying in Adelaide and basically lived out of it. There are probably photos of me from 2006-ish here with that bag on a Starbucks table, or an airport waiting room. My right shoulder twitches at the thought of how much weight I used to subject it to, but having it slung on one side did mean I could easily grab stuff out of it while I had it it.

The bag I've probably used the most is a smaller High Sierra backpack I got on special, right before Clara's and my first trip to Japan. It was a boring black and grey, but it was solidly built and has withstood being stuffed to the point of bursting. I still have it for smaller laptops.

<p><img src="https://rubenerd.com/files/2021/arena-backpack@1x.jpg" srcset="https://rubenerd.com/files/2021/arena-backpack@1x.jpg 1x, https://rubenerd.com/files/2021/arena-backpack@2x.jpg 2x" alt="Photo of my new laptop bag! Not the green shopping bag next to it, mind." style="width:500px" /></p>

Which leads me to my current *Arena Tote Backpack* I bought specifically for Covid times. I wanted a bag that could tolerate being regularly washed when I have to take the train, just as I do with my clothes. This one was *heavily* discounted owing to being discontinued, and the fact it was designed for the rigours of chlorine water boded well for my very specific use case.

It's already my favourite laptop bag I've ever used!

It's hard to tell from photos, but it's made of squishy wetsuit material, like a swimsuit but thicker. The lower quarter has an additional layer for repelling water and not absorbing nasties, which will be good for sitting on train seats. You can also use the straps that hang from the front to use it as a tote bag. I also love that it's a bit more of an unusual design, which makes up for its otherwise boring colours.

But the most important consideration: it's *super* comfortable. I mean it, it feels like I'm being hugged as I walk around. It starts out flat, unlike my High Sierra bag which is still chunky when empty. The shoulder straps are thick and well-padded with plenty of slack for tightening around your back. It sits flat against my back with a 16-inch MacBook Pro or a ThinkPad T550 stashed in what I'm using as a laptop compartment, and it has a few other waterproof pockets elsewhere for my little Ricoh GR III camera and my daily-carry pouch of adaptors.

Thus far my only complaint is there isn't much padding on the bottom, so you have to be careful putting it down if you have a laptop in it. I'm thinking of adding a bit of my own inside just in case.

It's a shame Arena don't make this bag anymore, all their new ones that I can see just look like regular, boring, non-descript chunky backpacks. But this experience has taught me to venture out a bit more when looking for things like this; I wouldn't have expected in a million years for my favourite backpack to come from a swimming store! Who knows, maybe the next one will be from a Korean barbecue chain, or a place where you get couches reupholstered.
