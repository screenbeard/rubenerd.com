---
title: "Rhett on opening coconuts"
date: "2020-10-09T21:21:14+11:00"
abstract: "He learned how to smile and work at the same time."
thumb: "https://rubenerd.com/files/2020/gmm-coconut-1@1x.jpg"
year: "2020"
category: Media
tag:
- good-mythical-morning
- video
- youtube
location: Sydney
---
From the [Coconut Opening Kit](https://www.youtube.com/watch?v=IR2rXypB_Dk) episode of Good Mythical More:

<p><img src="https://rubenerd.com/files/2020/gmm-coconut-1@1x.jpg" srcset="https://rubenerd.com/files/2020/gmm-coconut-1@1x.jpg 1x, https://rubenerd.com/files/2020/gmm-coconut-1@2x.jpg 2x" alt="(Rhett punching a hole in a coconut)." style="width:500px" /><br /><img src="https://rubenerd.com/files/2020/gmm-coconut-2@1x.jpg" srcset="https://rubenerd.com/files/2020/gmm-coconut-2@1x.jpg 1x, https://rubenerd.com/files/2020/gmm-coconut-2@2x.jpg 2x" alt="Rhett: I've got to have a new face. Because it's tropical, people are listening to music, people are playing vollyeball with strangers." style="width:500px" /><br /><img src="https://rubenerd.com/files/2020/gmm-coconut-3@1x.jpg" srcset="https://rubenerd.com/files/2020/gmm-coconut-3@1x.jpg 1x, https://rubenerd.com/files/2020/gmm-coconut-3@2x.jpg 2x" alt="I've got to learn how to smile and work at the same time." style="width:500px" /><br /><img src="https://rubenerd.com/files/2020/gmm-coconut-4@1x.jpg" srcset="https://rubenerd.com/files/2020/gmm-coconut-4@1x.jpg 1x, https://rubenerd.com/files/2020/gmm-coconut-4@2x.jpg 2x" alt="Rhett punching a hole in a coconut... with a grin)." style="width:500px" /></p>

