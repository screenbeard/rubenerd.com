---
title: "Trying an alternative to RSS topic folders"
date: "2020-11-08T10:45:08+11:00"
abstract: "Using folders for people, wire services, and advisories helps to surface things I'd have otherwise missed."
year: "2020"
category: Internet
tags:
- bloglines
- guides
- rss
- web-feeds
location: Sydney
---
It's natural when you've accumulated enough RSS feeds in your aggregator of choice to want to organise or sort them into folders. They're like Twitter lists, only infinitely more flexible, usable, and open! But what should they be sorted by?

Topics are the most obvious. It's how news sites organise their articles, and prominent blog aggregators like Feedly suggest topic collections to people with new accounts. I can't remember if mine started the same way on Bloglines back in the day, but I've carried over the same rough topic folders since I started with RSS:

* Apple
* Art
* Australia
* Friends
* Infocomm News
* Infocomm People
* Multifariousness *(a catchall, basically)*
* News
* Nix
* Otaku
* Photography
* Science
* Singapore

This *works*, but like the Tower Bridge when a boat approaches, it has a few glaring drawbacks. That didn't make sense.

I don't know if you've noticed this on your own aggregators, but blog networks and news sites routinely drown out anything else that's in a folder. A few sites like Metafilter and ZDNet dominate my *Infocomm News* folder, often leading to me missing tidbits from others. The unread count on the general *News* folder is in the thousands and realistically will never be caught up on.

What each folder *contains* has also evolved as blogs have dropped off and my interests have changed. For example, *Art* ended up including Hi-Fi gear, because that's where music news ended up. Should that still be there? Probably not, especially considering *photography* gets its own category. Or what about an anime blogger who's also a Postgres DBA? Do they go in *Otaku*, or *Nix*, or *Infocomm People?*

*(I already can't remember which self-anointed arbiter of blogging etiquette first advocated these arbitrary publishing edicts, but one stated you should limit your blog to one topic. This is, of course, bollocks. You should write what *you* want and what *you're* interested in. I've mentioned before that people talking about disparate topics is a feature, not a bug. I love subscribing to a blog based on common interests, and being exposed to something else I've never seen before).*

I've realised that *who's* blogging, and how important they are to me, matters *way* more than topic. I'm okay missing a few days of Nikkei Asia or the BBC, but I absolutely don't want to miss a post on [The Geekorium](https://www.the.geekorium.com.au/). So I've ditched the topics entirely, and rearranged my subscriptions around this:

* **Advisories.** No analysis or commentary, just raw bulletins about new packages, updates, and security patches from projects like FreeBSD and Debian. They're actionable.

* **Art.** Photos from space agencies, specific tags on anime image boards, profiles on sites like Tumblr, Wikimedia Commons featured pictures, etc. It's an ephemeral photo magazine for browsing, or for choosing new desktop backgrounds.

* **People.** Individual bloggers I *always* want to read. I don't need to sort by topic; I know that I follow them for stuff I'm interested in already.

* **Wire.** Aka, wire services. News outlets and large blog networks that regularly crank out updates. I'd turn off unread post counts here if I could, and use like Dave Winer's River system, or how I read Twitter now.

I have all the same feeds I had before, but this structure has surfaced so much more stuff I missed before.

