---
title: "Being worthy of social security"
date: "2020-10-22T22:27:51+10:00"
abstract: "Nobody deserves to starve. It’s that simple."
year: "2020"
category: Thoughts
tag:
- economics
- philosophy
location: Sydney
---
Do you assess people's worthiness of being rescued from a burning building? Whether you call the fire department, try to find water, or risk your own life to brave the flames, people with empathy and a concience will be *compelled* to assist.

It sounds so obvious. Yet turn the coversation to social security, and suddenly the press want to demonise recipents on the basis that they're slack, lazy, and unmotivated. We're asked why our hard-earned tax dollars should fund people who only sit on their couch and contribute nothing to society. But people who stash trillions in offshore accounts to avoid tax are just using legal loopholes, dontchaknow?

The alternatives are&mdash;conveniently&mdash;never mentioned: [homelessness](https://rubenerd.com/homeless-people-in-media/), broken families, crime, malnutrition, death. Our entire society is build around cold calculations like this, and prendending we don't.

I was [watching Simon Sinek](https://www.youtube.com/watch?v=IJyNoJCAuzA) do a talk about management, and he said something that's stuck in my head since: 

> Empathy is about the concern for that human being, not just their output. [..] Maybe they've had a hard day, maybe they're just a bastard! *We don't know.*

We can talk about how unemployment will never be 0%, or how disabilities and sickness can affect job performance and prospects. But that falls into that basic trap: we're judging people based on their economic output, not out of concern or empathy for their well being.

We're told that we can't help people, or they won't be motivated to work. Leave aside how people are supposed to go to a job interview without clean clothes, or apply for a loan without an address, or attend classes if they can't eat: do we really think whipping people to improve morale actually works? Advocates for this need to just come clean and admit it: it's entirely punitive.

I've only mentioned this once in fifteen years here, but I was a full-blown *hikikomori* for at least half a year after my mum died. I lost all motivation to work, study, or see people. It was soul-crushing; I felt like I was worthless. It took a lot of personal reflection and gentle patience from people who cared about me to coax me back to reality. Most people aren't so lucky, and the unemployed are somehow grappling with bills, commitments, and fear *on top* of this. And with Covid uncertainty too!? I can't begin to imagine how they do it; which is just how the press wants it. How it *needs* it to sell this bullshit, zero-sum game.

We have no idea how people got to where they are. Instead, we've been cynically conditioned to accept that the punishment for "laziness" is death. Because make no mistake, that *is* what the alternative is. And I don't accept it. *Nobody* deserves to starve.

I also don't buy the argument that we can't afford to give people dignity, shelter, and food. Of course we can; it's all about priorities.

