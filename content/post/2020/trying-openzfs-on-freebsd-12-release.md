---
title: "Trying OpenZFS 2 on FreeBSD 12.2-RELEASE"
date: "2020-12-17T08:24:00+11:00"
abstract: "Installing the latest version from ports"
year: "2020"
category: Software
tag: 
- bsd
- freebsd
- openzfs
- storage
location: Sydney
---
OpenZFS 2 is a huge achievement, and makes me bullish about the long term prospects for the world's most trustworthy and nicest to use storage system. You can even use try it today on FreeBSD 12.2-RELEASE, though I recommend tracking -CURRENT for these sorts of features.

The rule of thumb for packages with drivers or kernel extensions is to see when they were built. IIRC at the time of writing the current [openzfs](https://www.freshports.org/sysutils/openzfs) and [openzfs-kmod](https://www.freshports.org/sysutils/openzfs-kmod) packages were built for 12.1-RELEASE, so you won't be able to boot with them with 12.2. But they're easy to install from ports.

First make sure you have [base source](https://www.freebsd.org/doc/handbook/svn.html) for your release:

	# svnlite checkout https://svn.FreeBSD.org/base/releng/12.2 /usr/src 

Then download the latest ports tree, either with portsnap or subversion:

	# svnlite checkout https://svn.FreeBSD.org/ports/head /usr/ports

You can [build the ports](https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/ports-using.html) with the usual `make install clean`, but I still use [portmaster(8)]() after all these years:

	# portmaster sysutils/openzfs
	# portmaster sysutils/openzfs-kmod

Now enable, assuming this has been set to `YES`:

	# sed -i '.backup' 's/zfs_enable/openzfs_enable/' '/boot/loader.conf'

Then update your path to use the new userland tools in `/usr/local/sbin`, and reboot. You may need to reimport your pools created with these tools each reboot, or use [this rc.d script](https://svnweb.freebsd.org/base/head/libexec/rc/rc.d/zpool?view=markup) that [PMc raised](https://forums.freebsd.org/threads/trying-openzfs-on-freebsd-12-1-release-p10-zpool-not-imported.77073/post-484485) in the FreeBSD forums. That caught me out the first time. 

The only other caveats are to be careful when trying new features if you intend to share pools with other -RELEASE machines. ZFS does a good job with backwards compatibility, but older versions won't recognise an encrypted OpenZFS pool, for example. 12.2-R also doesn't support encrypted zroot boot volumes. By reading this post you acknowledge this [responsibility](https://rubenerd.com/omake/terms/#general-disclaimer)!

Refer to the [OpenZFS](https://openzfs.org/) website and [FreeBSD Handbook](https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/ports-using.html) for more information about the process, and for the canonical documentation.
