---
title: "Experience of flying back to the US"
date: "2020-12-08T10:03:00+11:00"
abstract: "“Both the US and Australia are responding to the same pandemic but you would hardly know it.”"
year: "2020"
category: Thoughts
tag:
- covid-19
- united-states
location: Sydney
---
Abby Bloom's [article for the Guardian Australia](https://www.theguardian.com/commentisfree/2020/dec/07/arriving-in-the-us-from-australia-during-covid-was-like-walking-through-the-looking-glass) on Monday put the difference more starkly than any I've read for a while:

> Flying back to Sydney on US election day was momentous. It also felt like whiplash. The Australian governments swung into action even before we stepped off the plane in Sydney. Once in the terminal it was full-on with precise coordination across jurisdictions and levels of government – immigration, biosecurity, state health authorities, police, army, air force. It embarrassed me that an air force officer was pushing my baggage trolley as he escorted me to my [mandatory quarantine hotel] room. Of course this wasn’t a courtesy: he was there to make sure that I was securely locked in my room without a key, open window or balcony for escape.
> 
> Both the US and Australia are responding to the same pandemic but you would hardly know it. In the US magical thinking and the elevation of individual freedom above the public good has squandered precious time. The number of deaths each day in the US quadrupled in just the four weeks after I landed in New York. Today it is up 30% in the past 14 days. Hospitals are reaching capacity and beyond. In a little more than two months my mother will have completed an entire year in self-quarantine, isolated from loved ones except for outdoor visits while the weather permitted. She’ll probably turn 107 before both of us are vaccinated and can once again embrace. She has never met her first and only great-grandchild, born during the pandemic, and probably never will.

A colleague of mine recently had to spend Thanksgiving with his American wife in Australia, because they couldn't go back home to see family. It's the same story in so many places around the world, but the psychological impact of seeing the world's most powerful country reduced to this is hard to overstate.

At least vaccine deniers complicit in the further spread of this disease, and who conflate freedom with freedom from responsibility, have gone from saying it's mind control, to claiming the elites are hoarding it. Why would they if it doesn't work? Whoever planted that brilliant seed might save more lives than anyone else.
