---
title: "Lessons learned from the 2016 Australian census?"
date: "2020-11-20T09:24:03+11:00"
abstract: "When it came to cybersecurity, the ABS had established “partly appropriate measures”"
year: "2020"
category: Internet
tag:
- australia
- politics
location: Sydney
---
*The irony isn't lost on me that the permalink for this post is completely wrong. But then again, my silly blog isn't hosting a national census. Though... maybe running it on OrionVM and FreeBSD would improve things, not that I'm biased!*

Like my dancing ability and coriander leaves, the 2016 Australian census is remembered in infocomm circles for all the wrong reasons. It was the first conducted where a majority of the population were encouraged to file online, but the submission site was plagued with issues from launch. I [quoted former PM Malcolm Turnbull admitting](https://rubenerd.com/mr-turnbull-on-nbn-and-censusfail/) in 2016:

> “That was a failure that was compounded by some failures in hardware – technical hardware failures – and inadequate redundancy.”

I had third-hand knowledge from people working in government at the time that none of this was unexpected. Responsible people had been ringing alarm bells for months in the leadup to the launch, much of which wasn't prioritised with funding or time.

It wasn't surprising then that many of us, including the Greens, Nick Xenophon, and Jacqui Lambie also didn't trust the [onerous extension](https://www.theguardian.com/technology/2016/jul/25/census-2016-australians-who-dont-complete-form-over-privacy-concerns-face-fines) of private data retention to four years. These systems had done nothing to earn our trust, and many of us subsequently avoided submission altogether using legal loopholes despite threats of fines.

So have we learned anything for the 2021 census? The Guardian's Katharine Murphy [tempered our expectations](https://www.theguardian.com/australia-news/2020/nov/20/cybersecurity-measures-for-2021-census-not-yet-fully-implemented-auditor-general-finds) this morning:

> A worrying new assessment by the Australian National Audit Office has found planning for the next census is only “partly effective” and the ABS has “not put in place arrangements to ensure that improvements to its architecture framework, change management processes and cybersecurity measures will be implemented ahead of the 2021 census”.
>
> The auditor general made seven recommendations to the ABS covering planning, efficiency, IT systems and data, risk controls and implementing external review recommendations.

This is what raised my eyebrows:

> When it came to cybersecurity, the ABS had established “partly appropriate measures”. The ANAO said the high-level measures and controls in the ABS’ cybersecurity strategy were “sound – however, the strategy has not been fully implemented”.

Chasing absolute security is a fool's errand, but if they have what they consider reasonable recommendations that have "not been fully implemented", that's a worry. The reason we have clichés about weakest links is because they're true.
