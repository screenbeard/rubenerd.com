---
title: "Antranig Vartanian on RSS, HiDPI, Apple Silicon"
date: "2020-10-28T08:29:22+11:00"
abstract: "Finally getting around to some more reader feedback."
year: "2020"
category: Hardware
tag:
- apple
- arm
- comments
- rss
location: Sydney
---
Antranig had a great email for me a few weeks ago pontificating the state of a few things which I'm just getting around to responding to. *Me, taking forever to answer something!? Never!*

> But first thing first, do you have a blog-post about what you use? e.g. hardware, software for home and work? I'd really like to know what you use for RSS. I use Reeder (and that's how I follow you) but I'm not able to add new feeds via Reeder because I use Fever-compatible API.

All the stuff I use to run the site here is on the [Engine Room](https://rubenerd.com/omake/engine-room/) page, but general stuff is on [Software](https://rubenerd.com/omake/software/). Between that and Ansible playbooks it's how I keep track of things, especially console tools that would be a pain to install manually on each new machine!

I haven't written about it yet, but for the last few weeks I've been trying [NetNewsWire](https://ranchero.com/netnewswire/) again. I forgot how nice proper desktop software can be! It's so fast and slick, though I haven't looked into how to sync it again. Before then I was exclusively using [TinyTinyRSS](https://tt-rss.org) on a FreeBSD cloud VM, and before that [Thunderbird](https://support.mozilla.org/en-US/kb/how-subscribe-news-feeds-and-blogs) which was surprisingly capable.

> Basically I feel like we have a similar setup and have the same feel about Apple (a unified experience. wanted to get an X1. I'm either stuck with 1080p in 2020 OR the software doesn't understand QHD :( )

This was in response to my earlier post about why [most PC makers still ship crappy displays](https://rubenerd.com/why-do-most-pc-laptops-have-awful-screens/), and a more recent post about [crappy laptops](https://rubenerd.com/foss-laptops-and-subpar-displays/), when Apple hardware has had Retina/HiDPI for almost a decade. It's definitely as much a software issue as hardware; even until recently FreeBSD and Linux desktops were still rougher than they needed to be with HiDPI and QHD.

I don't even fault F/OSS software for the subpar state of affairs; graphics seems to be one of the last bastions of binary blob obtuseness.

> Okay, other than that. How do you feel about this whole Apple Silicon thing? Will you move when they announce it?

I talked a bit about it when getting my [refurbished 16-inch MacBook Pro]()&mdash;with an x64 CPU!&mdash;but it sounds like I'm on the same fence as Antranig.

It's hard not to see the future as ARM, even just from a performance and energy-efficiency perspective. An ARM-Based Mac with all day battery life for business trips&mdash;remember those?&mdash;or when you're on call would be great. But for my main workstation an Intel CPU is a feature; if only for the elephant-sized desktop OS in the room that we need to run for specific software.

One thing that hasn't been discussed as much is just how splintered ARM is, too. We're used to a modicum of compatibility on x86, or POWER, or SPARC back in the day, but check out the FreeBSD mirrors or any Linux distro and see all the dozens of different ARM-based boards that need to be supported with their own distros. There are so many SKUs all implementing vastly different feature subsets. I've been told it's getting better, but saying something "runs on ARM" is still generic enough to be meaningless.

> I'm having a hard time, I'm sure it will work for day-to-day applications, but not sure about Unix programs, we(the tech industry)'re not even able to write portable code between Linux, macOS and \*BSD, how will we guarantee that software will work on different archs? :D

Admittedly I'm *slightly* more bullish about a mixed architecture world. Apple have put in effort to help some of the more popular F/OSS packages running on macOS, which they definitely didn't need to do. As a FreeBSD guy as well as a Mac user, I agree that among the biggest challenges today are Linux-first/only development, as opposed to thinking about the underlying architecture. It's inevitable when the OS has taken the lion's share of the \*nix world, but it's still disappointing.

> P.S. I know you love to blog about your interactions, yes, you may use the content of the email for a blogposting (I should do the same!)

Phew! Now I just need to remember to respond and discuss them every now and then, too. I know of at least two people with Hales in their name who have submitted great stuff, and there's still Hacker News follow-up. I appreciate the feedback, please don't let my backlog dissuade from sending more.
