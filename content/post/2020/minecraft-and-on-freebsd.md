---
title: "Minecraft; and on FreeBSD!"
date: "2020-10-18T09:18:35+11:00"
abstract: "Everyone told me so, but it only took me a decade to try."
thumb: "https://rubenerd.com/files/2020/minecraft-my-island@1x.jpg"
year: "2020"
category: Software
tag:
- freebsd
- games
- minecraft
location: Sydney
---
Clara and I have been playing this newfangled game called *Minecraft*. Just like Superliminal which I have a draft post about, we jumped in after watching the [Hololive EN crew play it](https://www.youtube.com/watch?v=NS3_sNTwIRw), and see all the cool stuff they'd build together.

<p style="font-style:italic;">(As an aside, I'm finally seeing the appeal of video game streamers. They do wonders to reduce the barrier to entry for games by giving you an introduction, and seeing them play in real time is better than any review. I'm also self-aware enough to realise that I'm saying this on a review, whoops).</p>

<p><a href="https://www.youtube.com/watch?v=oLfPxPzDt5M" title="Play [MINECRAFT] ADVENTURE!! #GAWRGURA #HololiveEnglish"><img src="https://rubenerd.com/files/2020/yt-oLfPxPzDt5M@1x.jpg" srcset="https://rubenerd.com/files/2020/yt-oLfPxPzDt5M@1x.jpg 1x, https://rubenerd.com/files/2020/yt-oLfPxPzDt5M@2x.jpg 2x" alt="Play [MINECRAFT] ADVENTURE!! #GAWRGURA #HololiveEnglish" style="width:500px;height:281px;" /></a></p>

In one of the biggest collective cases of *I told you so*, Minecraft is just as fascinating, addictive, and fun as everyone has said for the last decade. It marries the aesthetic of *Commander Keen* and the creative freedom and resource management of *SimCity*&mdash;my two favourite games of all time&mdash;into an open-world simulation you can explore. It's terrifyingly well-suited to my tastes.

You mine to find resources and uncover the beautiful, procedurally-generate caverns with flowing water and lava. You build your own houses, tunnels, bridges, boats, and *powered railways*. You craft clothes and glass, smelt and polish materials, and trade with villagers. You even encounter creatures, some of whom are even happy to see you. And the sunsets are quite pretty.

I imagine that in an alternative universe where Maxis wasn't bought by EA, the company came out with Cities Skylines and Minecraft. It imbues the same open-ended spirit of those games: it's a gigantic, multi-levelled puzzle without a pre-defined ending or path you have to follow. This is what computing was supposed to be!

<p><img src="https://rubenerd.com/files/2020/minecraft-my-island@1x.jpg" srcset="https://rubenerd.com/files/2020/minecraft-my-island@1x.jpg 1x, https://rubenerd.com/files/2020/minecraft-my-island@2x.jpg 2x" alt="Sunset over my little island" style="width:500px;" /></p>

Clara and I have already learned a lot in this last week. We were separated while running away from monsters and had to make our own makeshift shelters in disparate places, wondering if we'd ever see each other again. Now I'm in the process of building an MTR tunnel between the two with the F3 coordinates and a compass, and she's floating away to explore and fill in more of our maps.

This shows how green I was, but I didn't even realise the original Minecraft ran on Java, and the [FreeBSD Foundation](https://freebsdfoundation.org/)&mdash;for which I'm a proud regular donor&mdash;published an excellent [getting stared guide](https://freebsdfoundation.org/freebsd-project/resources/easy-minecraft-server-on-freebsd/ "FreeBSD Foundation: Easy Minecraft Server on FreeBSD"). Thanks to Jonathan Price and the committers for maintaining the [minecraft-server](https://www.freshports.org/games/minecraft-server/ "FreshPorts page on the minecraft-server port") port, it was unreasonably easy.

There's also always someone who comments on posts like this saying that I've offended their delicate sensibilities having only just discovered something everyone has known about for years. Let's just cut that feedback loop at the source and pretend you already sent it: that way you retain your smug satisfaction and I don't have to read it. Better yet, spend the effort paying Minecraft!

