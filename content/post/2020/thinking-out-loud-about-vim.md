---
title: "Thinking out loud about Vim"
date: "2020-11-09T16:07:33+11:00"
abstract: "After fourteen years, admitting that maybe it isn’t the right editor for me."
year: "2020"
category: Software
tag:
- editors
- vim
location: Sydney
---
I use the Vim text editor, every day. I've written millions of words in it, in HTML, Textile, Markdown, LaTeX, Docbook, and plain text. Every post on this blog since at least 2006 has, at some point, been composed or edited in it. Vim is where I change configuration files, code scripts, list todo items, and make meeting notes.

I navigate text from the home row not with cursor keys. I have a fleshed out `.vimrc` file for configuration. I have opinions about plugins. Sit me in front of nvi on a BSD, or vanilla Vim in compatibility mode, and I can be almost as productive. I instinctively reach for Vim keybindings in other contexts where it makes no sense!

Here comes the but. I haven't ever been able to shake this uncomfortable, lingering feeling of self-doubt that it's not the tool for me. In fourteen years I truthfully haven't progressed beyond an intermediate user. Having jumped in the shallow end and surrounded myself with a comfortable subset of features to give myself a cross-platform alternative to Mac-only editors, each additional step into the deep end has felt equally cold.

This is largely on me. I concede that if I took the time to learn all the features properly, I'd realise that promised, untapped potential for additional productivity. It's all there, waiting to be discovered, learned, and strategically deployed. But the reason I qualify that statement is because I still feel as though I'm the one seeding ground each time, rather than my editor working with me. Vim holds all the cards, to abuse another metaphor.

Text editors inhabit a weird space in computing. What could be simpler than transcribing the ramblings of an operator into characters on a blank screen? And yet there are dozens, possibly hundreds, of different ways to do it, all targeting different use cases, writing styles, feature sets, abilities, and trade-offs. There's a reason new ones still come out, despite the industry existing for decades.

Text editors aren't hammers, they're instruments. And I think I'm a drummer who's forced himself to use a piano. It taught me valuable theory, and I can appreciate how admins, writers, and developers can be productive using it. But ultimately it's not the best way to express myself. I guess I'm starting to accept that I'm fine admitting it.
 
The final salvo came from [Adrien Lucas Ecoffet](https://www.quora.com/Why-is-Vim-the-programmers-favorite-editor/answer/Adrien-Lucas-Ecoffet), via [Unixsheikh's great article](https://unixsheikh.com/articles/vim-i-hate-to-love-you.html)\:

> Vim users (I am not one of them) will tell you something along the lines of "it is hard to use at first but when you really learn it, you become super productive".
> 
> However, the second part of this sentence applies to just about every good editor out there: if you really learn Sublime Text, you will become super productive. If you really learn Emacs, you will become super productive. If you really learn Visual Studio... you get the idea.

So now I'm wondering where to go from here. If I'm going to take the time and learn an editor properly, I think I'm telling myself that I'd rather do it in an editor better suited to how my mind works. I already use Emacs as a glorified org-mode interpreter, having realised it clicked for me faster than any Vim-based wiki. Maybe I should deep dive there.

*(To confirm for those inclined to skim posts and leave abusive comments: I'm not saying Vim is a bad editor, nor am I judging anyone for using it. If anything, I'm envious of people who can use it, just as I am about those who can play piano. But in the words of myself at the end of this post, it's just not for me... I think).*

