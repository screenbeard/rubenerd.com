---
title: "Hacktoberfest needs to stop"
date: "2020-10-04T11:16:28+11:00"
abstract: "Ned Flanders couldn’t live in good intentions"
year: "2020"
category: Software
tag:
- economics
- foss
- incentives
location: Sydney
---
Ned Flanders telling Marj he "can't live in good intentions" remains one of my favourite Simpsons moments. It applies to *so* many situations, and especially within IT.

I've watched with dismay and a complete lack of surprise at Hactoberfest, again. Each year, a large hosting company rewards people contributing to open source software with free swag. It measures this by counting the number of pull requests you make to open source projects on GitHub.

*(For non-developers, a pull request is a formal way to merge your changes or improvements you've made to a codebase. You clone/fork the original code repository, make your changes, and issue a pull or change request).*

If you haven't been following the news, can you see a problem with this?  If it sounds like this would just lead to pull request spam, you have more foresight than this hosting company did.

Former Australian PM Paul Keating once commented that markets shift behavior, and we're seeing this play out here. Align financial incentives with your pull request count not quality, and this is the inevitable outcome. There are a few other angles this year specifically, including an infamous YouTube video explaining exactly how to game the system. But it couldn't have happened if the system didn't enable it.

It's good that we have companies wanting to encourage open source software development. I'd go as far as to say anyone who benefits from it has a responsibility to; not in the letter of the law or licence, but certainly in the spirit of them. Where I draw the line is shifting the burden of a gamified system with pretty graphics and lots of PR onto overworked, underpaid software developers to trawl through hundreds of pull requests every day. It causes burnout and resentment, and wastes a month of productivity.

You help open source by funding it, and helping aspiring contributors with training and mentorship. Everything else is window dressing, and it hurts the community. Please don't do this.

