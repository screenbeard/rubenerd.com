---
title: "@Spycrowsoft on OLED sensitivity"
date: "2020-11-17T08:59:31+11:00"
abstract: "Suggests it may have something to do with refresh rates."
year: "2020"
category: Hardware
tag:
- health
- oled
- phones
location: Sydney
---
@Spycrowsoft on Twitter direct messaged me about my latest [OLED phone post](https://rubenerd.com/no-lcd-option-on-the-new-iphone-12/ "OLED-sensitive people left out from the iPhone 12")\:

> Spot on! I'm one of those people sensitive to OLED's. However I've noticed that it really depends on the type of panel used. Generally, when the refresh-rates get above 150 Hz, there is no problem for me anymore
> 
> However, those displays are rare and only found in the expensive Samsung TV's

I'm keen to try screens with higher refresh rates now. The pulse width modulation that's used to adjust the perceived screen brightness is what flickers and causes me eyestrain and headaches. Maybe I need to source a Samsung TV and try.


