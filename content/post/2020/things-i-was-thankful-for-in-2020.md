---
title: "Things I was thankful for in 2020"
date: "2020-12-31T08:57:00+11:00"
abstract: "Ganbatte kudasai! Let's make 2021 better."
year: "2020"
category: Thoughts
tag:
- bsd
- blogging
- covid-19
- freebsd
- health
- hololive-en
- netbsd
location: Sydney
---
This has been a hard year, and today from a personal perspective has been the cherry on top I fully expected *(inb4 confirmation bias).* But I'm going to let it get away with sending us off like this, so I'm writing an (incomplete!) list of things I was thankful for this year, written to the sounds of Mori Calliope telling the year [what it can do](https://www.youtube.com/watch?v=5y3xh8gs24c), and reminding us it [won't bring us down](https://www.youtube.com/watch?v=3zdcd08XWRs).

**Clara, and health**
: ♡ <p></p>

**Dad didn't lose his life or home in the Australian bushfires**
: This was only in January, I already can't believe it. Helping him pack some essentials and evacuating on advice of the Rural Fire Service was... I can't summon the words right now. But he and his home survived, and hopefully we'll have a postponed Xmas again soon!<p></p>

**Reconnecting with friends**
: Dealing with anxiety over the last few years led me to distance from people I care about. I reconnected with so many of them again this year, from high school to now, and they're all awesome... and understanding. Thanks everyone.<p></p>

<p><img src="https://rubenerd.com/files/2020/freebsd-lca-itsame@2x.jpg" srcset="https://rubenerd.com/files/2020/freebsd-lca-itsame@1x.jpg 1x, https://rubenerd.com/files/2020/freebsd-lca-itsame@2x.jpg 2x" alt="Me awkwardly presenting my talk on FreeBSD at OrionVM" style="width:500px; height:375px;" /></p>

**Becoming more involved with FreeBSD and NetBSD**
: I [spoke at the FreeBSD miniconf](https://rubenerd.com/the-first-freebsd-conference-in-australia/) at Linux.conf.au 2020 in January, met some more members of the Australian BSD community, and I was mentioned on the BSD Now podcast! I still have impostor syndrome something fierce, but people have helped coax me out of my /bin/sh.<p></p>

**Hololive EN**
: People acting anime characters in realtime while playing games like Minecraft sounds niche but not groundbreaking. But their English-speaking generation launched this year have brought us so much joy. Clara and I will always be *Investimigators*, but Gura and Ina have become *de facto* councillors given the heavy questions they answer with care and good humour. I only half joke that they've probably saved lives during these depressing times.<p></p>

<img src="https://rubenerd.com/files/2020/our-minecraft-2020@1x.png" alt="Screenshot of one of our Minecraft villages" srcset="https://rubenerd.com/files/2020/our-minecraft-2020@2x.png 2x" />

**Minecraft**
: I blame Hololive-EN for getting me into this time sink game that I'd so successfully avoided for a decade. WOW it's fun. You could do worse than spending your evenings building out villages and exploring with your girlfriend if you can't travel or go outside.<p></p>

**OpenZFS 2.0**
: I use and advocate for ZFS everywhere I can, professionally and personally. Merging the disparate branches of open source ZFS into one tree was a huge technical and community achievement, and most importantly signalled the stability, long-term support, and viability of the world's most trustworthy file system and volume manager now that Oracle holds the other keys. I run it today, and can't wait for it to be in FreeBSD BASE next year.<p></p>

**Emacs**
: I like Vim and have used it for years, but this whole time I didn't realise that Emacs interfaces to my brain better: read into that as much as you want. Finding all these great tools that can run it has been too much fun.<p></p>

**This blog**
: I added 490 posts this year, and reached 7,200. Writing each one was a cathartic exercise to write, and even got mentioned on some high-profile news sites, aggregators, and mailing lists. I've had so many great emails, comments, and feedback. You've even taken time to read my site or RSS feed, and the post I'm writing now. Thank you.

Happy New Year. *Ganbatte kudasai!* Let's do our best to make 2021 better.
