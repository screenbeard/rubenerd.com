---
title: "Recdiffs on why people are the way they are"
date: "2020-11-27T16:49:00+11:00"
abstract: "Struggling with intentions and motivation."
year: "2020"
category: Thoughts
tag:
- merlin-mann
- philosophy
- rec-diffs
location: Sydney
---
Earlier this week I wrote about the discomfort of sharing ideas I agree with, from people I otherwise don't. It's part of a broader thought I've been trying to reconcile that I can't and won't ever understand all of someone's intentions or circumstances, but that it doesn't change the fact they're abusive to me or do things I take moral issue with.

Then Merlin Mann [dropped this truth bomb](https://www.relay.fm/rd/144) on the latest episode:

> When I try to image why somebody is the way they are, I'm projecting my best idea as to why they are the way they are. In the most blinkered way we think about these things we say, to quote the Renoir film, "the terrible thing in life is everyone has their reasons". Just because we all agree that everyone we disagree with aren't nuts, doesn't mean we agree on any part of why that is.
>
> So when people like me go "aw man, what must it be like to be Rudy Giuliani?" [..] I don't know. But the truth is, I *can't* know. And more's the pity. Because as much as I try to analyse&mdash;as in, break into pieces and make sense of&mdash;I don't know how much closer I am to understanding another person if I'm mostly using the model of me and the people around me to try and understand why they are the way they are.
>
> I would like to think&mdash;I have to think&mdash;that's a big problem with our various disconnects. It's not just the president assuming everyone's like him, in as much as he's the most special boy in the world, it's also because we're always trying to discover why someone does that nefarious thing based on what their "disability" is. What's *their* model for psychosis?

Many, many times this. I've been writing about my own struggle to [understand intentions](bryan-hughes-on-intentions) for a while. I can't find the other post, but I mentioned a couple of years ago that I wanted to spend more time assuming good intentions, or at least not ascribing the worst. Rationally, we don't know people's circumstances; someone being rude in public could be a dick, or just lost their job. But being empathetic is hard when the other actor either doesn't care to reciprocate, or worse, is threatening you or people you care about.

I also realised I spend so much of my mental energy making excuses for people and trying to understand them which, even if based in good intentions myself, are unknowable. Or at least to me in that moment in time.

[Reconcilable Differences](https://www.relay.fm/rd) is a fantastic show. If you're new to podcasts or burned out by the current crop, please consider it.

