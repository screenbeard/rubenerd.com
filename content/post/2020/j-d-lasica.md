---
title: "Music Monday: J.D. Lasica"
date: "2020-11-02T09:33:11+11:00"
abstract: "Darknet: Hollywood's War Against the Digital Generation was among the most important influential books on me growing up."
thumb: "https://rubenerd.com/files/2020/darknet@2x.jpg"
year: "2020"
category: Media
tag:
- books
- copyright
- jd-lasica
- music
- music-monday
location: Sydney
---
This is a slightly unconventional chapter in my long-running [Music Monday](https://rubenerd.com/tag/music-monday/) series, but I was reminded of this while updating my RSS feeds this morning.

J.D. Lasica's *Darknet: Hollywood's War Against the Digital Generation* was among the most important and influential books on my thinking growing up. I reread it at least a few times, but I have vivid memories first reading it with earnest on the MRT in Singapore on the way to my first job out of high school. Its warnings and lessons are as prescient now as they were when it was published in 2005.

<p><img src="https://rubenerd.com/files/2020/darknet@1x.jpg" srcset="https://rubenerd.com/files/2020/darknet@1x.jpg 1x, https://rubenerd.com/files/2020/darknet@2x.jpg 2x" alt="Cover of Darknet." style="width:162px; height:247px; float:right; margin:0 0 1em 2em;" /></p>

J.D. raises the issue of online remix culture that had its roots in the origins of the World Wide Web and BBSs, but by the early 2000s included video and audio. Large studios had begun to take notice, with the RIAA and MPAA most famously suing average people for copyright infringement. He demonstrated this behaviour was counterproductive and harmful to all parties involved, and advocated for entirely-sensible reforms.

It was eye opening for a naive person like me who grew up putting Simspons and Sailor Moon GIFs on GeoCities pages that this would even be a problem. It was clear the legal frameworks for copyright were outdated and disproportionately favoured large content producers. The fact the famous "You wouldn't steal a car..." advertisement had used music from an independent producer without permission laid bare this fundamental power imbalance. This was never about protecting content creators, it was a shameless attempt to retain power that had been so successfully consolidated during the twentieth century.

I still remember one of the more amusing rebukes by a large newspaper claiming the book was biased, and didn't do enough to advocate for large media companies. Billions of dollars spent on lawsuits, lobbying to extend copyright durations, and advertising campaigns were fine, but letting an independent publisher make an alternative case? Heavens, we can't have that! That was the final in a series of lightbulb moments when I realised: *this is what we're up against.*

YouTube content matching, and Tumblr's on again/off again filters are an evolution of the same fundamental challenges J.D. warned about. Large online streaming services have been touted as solving piracy, but still do nothing to help independent or small producers, and they continue to stymie remix culture by locking up copyright and making it just as inaccessible.

The ultimate irony is the industry saw copyright as a zero sum game, rather than a new way to connect with their fans, and ultimately their customers. We&mdash;and they&mdash;are still paying the price for this ham-fisted attitude now, though independent podcasts, books, and video channels are slowly but surely becoming more viable thanks to distribution channels in which these traditional media companies have less influence.

This book, Whole Wheat Radio's advocacy for independent music, and Lawrence Lessig's Creative Commons efforts were hugely valuable at countering the large media narrative for me, something I haven't done enough work in since to help advocate for.

I accidentally left my hard copy of *Darknet* years ago at a coffee shop in Kuala Lumpur, but I'm tempted to source another one and give it a re-read. I love ebooks, but I keep really important ones on the shelf. This deserves a place.

J.D. writes about [independent media](https://www.jdlasica.com/blog/), which you should [subscribe to](https://www.jdlasica.com/feed/). He's also released some new novels I need to check out.

