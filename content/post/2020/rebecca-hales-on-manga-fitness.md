---
title: "Rebecca Hales on more manga fitness"
date: "2020-11-03T15:12:36+11:00"
abstract: "I suspect her sending us cute fitness-related paraphernalia to buy is a surreptitious, mischievous, and entirely effective ploy to get us to exercise more."
thumb: "https://rubenerd.com/files/2020/arena-mizu@1x.jpg"
year: "2020"
category: Anime
tag:
- arena
- fitness
- swimming
location: Sydney
---
Speaking of friendly people with a [specific last name](https://rubenerd.com/hales-warns-us-of-greymarket-electronics/ "Hales warns us of greymarket electronics"), Rebecca Hales has been reading and commenting on this blog for more than a decade. Lately she's taken a keen interest in our fitness since I mentioned that we were [swimming again last year](https://rubenerd.com/we-almost-got-arena-manga-swimsuits/ "We almost got Arena manga swimsuits"), and has given Clara and I advice which has been hugely helpful. Clara and I love hiking and walks, but we're otherwise clueless vitamin D-deficient nerds when it comes to this stuff, so we've appreciated her insight.

I suspect her sending us cute fitness-related paraphernalia to buy is a surreptitious, mischievous, and entirely effective ploy to get us to keep these efforts up. Back in late September she sent us a press image, with no further comment beyond three exclamation points and two words: *Manga cats!!!*

<p><img src="https://rubenerd.com/files/2020/arena-mizu@1x.jpg" srcset="https://rubenerd.com/files/2020/arena-mizu@1x.jpg 1x, https://rubenerd.com/files/2020/arena-mizu@2x.jpg 2x" alt="Press image showing the various clothing items in the range, now with cats!" style="width:500px" /></p>

From the [gentleman's jacket](https://www.arenawaterinstinct.com/en_hr/003844-men-s-relax-iv-team-jacket-mizu.html):

> Arena heritage designs meet Japanese streetwear in our men’s Mizu Relax IV Team Jacket. Created in partnership with graphic designer Electric Peo, this black track jacket has logo stripes interspersed with an anime cat and oversize hiragana and kanji writing of the word water, ‘mizu’, across the back. Our signature raised emblem and logo zipper pull bring out this design’s retro spirit.

In case you didn't see, look at the cats!

<p><img src="https://rubenerd.com/files/2020/arena-mizu-2@1x.jpg" srcset="https://rubenerd.com/files/2020/arena-mizu-2@1x.jpg 1x, https://rubenerd.com/files/2020/arena-mizu-2@2x.jpg 2x" alt="Close ups of the cat motif." style="width:500px" /></p>

Unfortunately, just like with their last set of manga-themed costumes, Arena saves their coolest gear for Europe. Which makes sense, given the climate. Ah Ruben, that joke contained a veritable *abundance* of quality. Arena's [international site](https://www.arenawaterinstinct.com/en_global/lp/omizu-collection.html) seems to only let you browse, and despite the awesomeness of this stuff we're not in a hurry to use an expensive proxy service for something like this. Or, we are, but I wouldn't admit to that here. Wait, damn it.

Please flick me an [email](https://rubenerd.com/about/#contact) or [tweet](https://twitter.com/Rubenerd) if you know of anyone selling this range in Australia or New Zealand!

