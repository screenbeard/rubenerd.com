---
title: "People asking about FreeBSD licencing"
date: "2020-10-21T10:28:04+10:00"
abstract: "The fact people keep asking for GPL software in base should be telling us something."
year: "2020"
category: Software
tag:
- bsd
- freebsd
- forums
- social-media
location: Sydney
---
I saw a bash discussion on the FreeBSD forums a few months ago:

> It just perplexes me, because [the GPL is] one of the first things I've learned about in FreeBSD (probably before I ever installed and used it), and I always come across why it's not in base. 
> 
> I don't think anyone is stupid for not knowing that; I don't understand how someone can go so long, without knowing it. It's like someone smart, who didn't do their homework/research for a long time. Newcomers, however, shouldn't be expected to know it. I'm going to leave it at that.

I can empathise, to an extent. FreeBSD's simple, transparent licencing is one of its broad appeals, and is generally one of the first points raised in *BSD versus [GNU/]Linux* discussions. FreeBSD's [removal of GCC](https://rubenerd.com/clearing-up-freebsd-gcc-news/ "Clearing up FreeBSD GCC news") made headlines for months, as just one example. Requests for GPL-encumbered software in base must also grate after reading so many of them.

But it still shouldn't come as a surprise that some FreeBSD users aren't familiar with its licencing. FreeBSD is widely recommended for reasons beyond this, from its documentation, community, ease with which its kernel and world can be rebuilt and reproduced, mature tooling, jail and dtrace infrastructure, thorough ZFS integration, networking performance, stability, reasonable init system, BSD \*nix heritage, mascot, and more.

FreeBSD and the Foundation, like the other BSD projects, don't have massive marketing or PR departments like Linux does to keep it at the forefront of people's minds. I'd guess most BSD users come from recommendations, either word-of-mouth or reading. Licencing is but one of the advantageous angles, which could be easily misunderstood or overlooked coming from GNU/Linux; even from people who've used it for a while. I know, I seem to have the conversations weekly.

Which leads us back to the gentleman's first observation:

> It just perplexes me [..] I always come across why [the GPL is] not in base.

We can blame users to RTFM to an extent, but this recurring misunderstanding should also be telling us something in neon lights. The Free Software Foundation's advocacy sets so much of the tone in the free/open source software community, and those of us on the BSD/ISC/MIT side are demonstrably not providing effective counterpoints. In the paraphrased words of John Siracusa, being technically accurate is necessary, but not sufficient.

Maybe the FreeBSD homepage needs to communicate the licence advantages better, as well as the relationship with the GPL in ports and base. Explain *why* GPL free software is good, but that unencumbered software is better and necessary, especially for projects like FreeBSD.

