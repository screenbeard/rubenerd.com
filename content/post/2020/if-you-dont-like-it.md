---
title: "If you don’t like it, Mori Calliope has the answer"
date: "2020-12-07T08:00:12+11:00"
abstract: "I don’t want you to hurt. You can press the X button. "
year: "2020"
category: Media
tag:
- comments
- hololive
- trolls
location: Sydney
---
I think we have an [evergreen](https://www.youtube.com/watch?v=H6lCL8ka4e4 "Watch Mori Calliope: Let's Build My Crib").

<p><img src="https://rubenerd.com/files/2020/calli-dont-like-it-1@1x.jpg" srcset="https://rubenerd.com/files/2020/calli-dont-like-it-1@2x.jpg 2x" alt="If you become frustrated though, I don't want you to hurt." /><br /><img src="https://rubenerd.com/files/2020/calli-dont-like-it-2@1x.jpg" srcset="https://rubenerd.com/files/2020/calli-dont-like-it-2@2x.jpg 2x" alt="You can press the X button, it's very useful. The one in the corner of your browser." /></p>
