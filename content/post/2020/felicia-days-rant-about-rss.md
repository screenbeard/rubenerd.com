---
title: "Felicia Day’s rant about RSS"
date: "2020-12-16T10:05:54+11:00"
abstract: "“I am so irritated when that choice is taken from me.”"
year: "2020"
category: Software
tag:
- rant
- rss
- web-feeds
location: Sydney
---
*Update: Josh of [The Geekorium](https://www.the.geekorium.com.au/) pointed out that Felicia Day wrote the below passage on her blog, but was not the author of the plugin with which I'm rather enamoured, and linked to below. Thank you for letting me know!*

The author of the [essential RSS Firefox plugin](https://addons.mozilla.org/en-GB/firefox/addon/awesome-rss/) shgysk8zer0 quoted [Felicia Day's](http://feliciaday.com/blog/rss-rant/ "Firefox plugin page with description") amazing RSS rant:

> A long time ago (especially in Internet time), Firefox used to have an icon that showed up in the Awesome Bar when a website had RSS or Atom feeds available for subscribing. This add-on replaces the icon, so you don't have to add a button that will be constantly visible and taking up space. It will only be visible when there is a feed to subscribe to.
> 
> RSS is a way to consume a LOT of information very quickly, and STORE it in nice categories if you miss it. So I can catch up with a small blog's output at the end of the week and, if I so choose, read EVERY article easily in one sitting. You think on Friday I'm gonna go browse that same site's Twitter feed on their page (digging through all the messy @ replies) and see what they did that week?! Or go to their Facebook page that is littered with contests? No way dude, I'm too busy for that!
> 
> I feel like small blogs cut their own throat by taking away the RSS capability. I give this analogy a lot, but social media outlets are INFO COLANDERS! 5% of your followers will see anything you post, and that's probably only within 20 minutes of posting. That's the way it is and it's gonna only get worse. Apart from email lists, RSS is the best way you can collect stuff across the internet to read quickly, and I am so irritated when that choice is taken from me. /rant
