---
title: "Feedback on my blog-as-a-business post"
date: "2020-12-22T08:44:00+11:00"
abstract: "When people are interested in something, or only paid to write about it."
year: "2020"
category: Internet
tag:
- blogging
- feedback
- weblog
location: Sydney
---
I didn't realise what a chord I'd struck with my post yesterday about blogging platforms advertising themselves as getting you more *business* from *audience*. Almost makes me wish I'd spent more than five minutes smashing it out in response to seeing a single line of text on a hero image *cough*.

[@hikupro](https://twitter.com/hikupro) retweeted a few quotes which I appreciate! Lukas V also chimed in with one of the best emails I've had in a while:

> The appeal of reading blogs for me comes from people's passions being displayed on screen. I don't care much for photography, but seeing a chrisjones.io post in my RSS feeds puts a smile on my face. I always know I can find some interesting reading material from ohhelloana.blog's monthly bookmark posts. And, of course, seeing the rubenerd.com appear in the reader sidebar is a clear sign of some fun topics to come! Sadly, the only way to reliably find these kinds of websites are from blogrolls, obscure communities like those around the Gopher protocol, and whitelisted search engines like https://searchmysite.net.

It's true... except for the fun topics from that Rubenerd.com guy, he's a bit dodgy. I've touched on one of the supposed golden rules of blogging that you were never supposed to deviate from a single topic, which even back in the *blogosphere* days I thought was bunk. Merlin Mann has said he loves reading and watching people passionate about things he doesn't know about because its infectious; I like it too because I learn new things about fields I otherwise wouldn't have explored or entertained.

I also quickly touched on the declining quality of so many blogs, which he identified with an especially pernicious example:

> I saw your post about audiences, blogs, and businesses and it struck a chord with something I've noticed as I've been increasingly interested in personal blogging: spammy meta-blog blogs*. You know the ones I've talked about. The ones with headlines like:
>
> - "10 ways to increase your SEO"
> - "how to go from one visitor a week to over 5000"
> - "our hottest 6 tips for blogging about XYZ!!"
>
> Glancing at Reddit's "r/blogging" shows just how big of a problem this is. Every post is about visitor numbers and revenue. These kinds of topics make it near impossible to find new blogs written based on people's interests instead of their wallets.

Rebecca Hales also chimed in:

> [ARE YOU EXERCISING](https://rubenerd.com/rebecca-hales-on-manga-fitness/)?
>
> It is so obvious when a blog is written because somebody is interested in something, and not just because they are paid to write it.

The former is definitely happening, but it's taken the form of late-night walks where I can avoid people, not in the pool. Though it might be good as it gets warmer. The pool, not people churning out PR for a blog.
