---
title: "It’s not that people don’t want kids…"
date: "2020-11-27T10:02:00+11:00"
abstract: "Conversation I heard in a coffee shop this morning"
year: "2020"
category: Thoughts
tag:
- australia
- economics
- singapore
- sydney
location: Sydney
---
I overheard this at the coffee shop I'm sitting at this morning:

> **Person A:** It's not that people don't want kids, it's that they can't.
> 
> **Person 2:** Having three kids in Sydney is the ultimate flex. Especially if they're in private school, what must that be, 100k a year? Each?
>
> **Person Γ:** I'd love kids, but forget about it on both our salaries.

I don't know how people afford kids in Singapore, either. Clara and I are [DINKS](https://en.wikipedia.org/wiki/DINK) and obsessively track our spending, and it still seems crazy to think about.

