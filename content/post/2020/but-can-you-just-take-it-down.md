---
title: "School memories, and can I just take it down?"
date: "2020-11-23T09:17:00+11:00"
abstract: "There's no accounting for some people's class!"
year: "2020"
category: Thoughts
tag:
- memories
- personal
- school
location: Sydney
---
Do you often wonder where people in your high school are today? What they're doing, how they're going? For some of us it was a decade ago, for some of you much further, for others it may have just been last year. I was a palliative, stay at home carer for my high school years too, so I can partly empathise what living through Covid must be. Putting your studies and career on hold, for something that wasn't your fault or under your control is its own special kind of frustrating, to say nothing of the people close to you who are suffering.

But I digest! That was supposed to be *digress*. While most people in my school were great, there was a manipulative minority who really weren't. I think it came with the territory; it was a rich, private international school in an overseas bubble in Singapore. I knew people who'd be dropped off in sports cars, had mobile phones when it was unusual for adults to have them, and took international holidays every few months. It was another world, and I think it got to people's heads.

At least thirteen years ago I made a passing comment on this site about a gentleman who routinely picked me up by the neck. Whether it was a weird flex to show he could lift a scrawny person, or he was taking out his own trauma on me, who knows. I spent a long time trying to understand his motives while trying to hide my neck from friends and family out of shame. Maybe he was just a jerk.

Anyway years later I get an email from him, out of the blue. Maybe he'd been trying to contact me via my dormant Facebook profile and not getting anywhere, but either way it was bizarre seeing his name pop up again. He'd become a caricature in my mind by this stage; a burly meathead who grunted in lieu of speech, did burnouts in utes, and ate gravel for breakfast. When had he learned to read and write, let alone become a computer operator?

He didn't stray far from this image. We all make silly mistakes as kids; I wholesale misinterpreted two girl asking me out&mdash;sorry Kathleen and Fiona!&mdash;and this guy left people like me with permanent breathing problems. Maybe the subsequent years hadn't been kind to him, but he demanded in no uncertain terms that I take his name off my site. No acknowledgement of what he did, no regret, no apology, nothing. Suffice to say, I filed it under *karma is a bitch* and moved on.

Checkups aside&mdash;Ruben, did your parents abuse you growing up?!&mdash;I don't think much about him anymore. But sitting at a coffee shop and hearing Mambo No. 5 instantly transported me back to that clearing outside the art room, wondering when my feet would touch the ground again so I could retreat to the safety of the computer labs or library. It's funny how memory works, isn't it Steven?

Don't be like that guy. You don't want to be remembered as a gravel-eater.
