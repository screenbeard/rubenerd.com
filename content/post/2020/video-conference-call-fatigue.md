---
title: "Video conference fatigue"
date: "2020-11-24T18:14:00+11:00"
abstract: "Video calls are uniquely exhausting."
year: "2020"
category: Internet
tag:
- video
- work
location: Sydney
---
This year has felt overwhelming for all the reasons we expect. Anxiety, fear, that ever-present sense of foreboding. But while at my job we'd done remote working part time already, having it be the norm this year has made me more exhausted than I ever would have expected.

The biggest reason has to be exercise. Sitting on the balcony has helped with vitamin D and getting that important, melatonin-regulating sunlight in the morning, but I still haven't been doing anywhere near enough physical activity. This was laid bare when Clara and I got winded wandering around the Blue Mountains, when even back in July it wasn't as much of a problem.

The [University of Melbourne](https://blogs.unimelb.edu.au/sciencecommunication/2020/10/08/zoomed-to-death-heres-the-science-behind-zoom-fatigue/) have also identified what we all suspected as well: video conference calls are uniquely fatiguing, and its not just because of the tired clich&eacute; used in advertisements of people not wearing pants under the table.

> Due to our restricted working memory, our brains can only do a limited number of things consciously. Meanwhile, we can process more things unconsciously. Online classes increase our cognitive load because we have to rely predominantly on verbal communication, which consumes a lot of our conscious capacity.
>
> ... Without these unconscious cues, we have to put in more cognitive and emotional effort. We rely predominantly on verbal information to infer other people’s emotions and at the same time, focus on what the speaker is saying. This additional conscious processing leaves us feeling tired.

This goes part of the way to explaining why audio conference calls seem to take less mental capacity. We're not trying to interpret what we're seeing in addition to the audio in that case.

Humans&mdash;myself included, as I am one&mdash;don't handle latency well when talking with people. I'm only speculating, but I think it has to do with the fact in nature we're never far enough away from someone where noticeable, significant latency would be introduced. Video tricks us into thinking we're closer to someone than we are, so we have to work harder to process what we're seeing.

Our high-bandwidth sensory devices are all located near our brains, so we're also not used to senses being wildly out of sync. We've evolved to reconcile these into what we perceive to be a clear picture of our surroundings. Video necessarily takes more bandwidth and processing power than audio, so throw in some out of sync mouth movements and audio, and it throws our sensory balance off kilter. These are also contributing factors to motion sickness and vertigo.

I also catch myself squinting at computer screens when on video calls, for no logical reason other than I'm used to squinting if I'm having trouble seeing something. Low-quality or variable pictures trigger this same reflex, as though we were in the real world and trying to get a very close or distant object to focus. Squinting at a backlit display is a sure-fire way make your eyes tired, and can lead to headaches.

Those are the sensory variables, but there's also an interpersonal element I hadn't even realised I've been doing either. I've mentioned how wonderful [microbreaks](https://rubenerd.com/microbreaks/) are for concentration and energy, and the author Devia mentions it's taboo to do it on video calls:

> In physical classes, we’re able to gaze out the window or look at other people in the class. But in [video] meetings, doing so will make us look like we’re not paying attention. We feel obligated to stare at the camera for a whole hour to show that we’re listening. Engaging in this “constant gaze” makes us uncomfortable and tired.

I get that people think video calls make them more personal and engaging, and I'll bet certain bosses think they're a great way to micromanage and make sure their employees are paying attention. But it's counterproductive, at least most of the time.

As I mentioned on the [context switching](https://rubenerd.com/context-switching/) post last Sunday, the only solution I've found thus far is to block out times in my calendar for not taking or engaging in conference calls. I also sneakily join conference calls with just audio at first, signalling to others that for my chats it's socially acceptable to relax and not need the webcam. If I see video start in another square, I turn it on.

