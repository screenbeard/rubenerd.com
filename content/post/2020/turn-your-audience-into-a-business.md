---
title: "“Turn your audience into a business”"
date: "2020-12-21T09:11:00+11:00"
abstract: "I know why blog platforms have to pitch it like this, but narrowing it to just being a business tool makes me a bit sad."
year: "2020"
category: Internet
tag:
- blogging
- ghost
- gohugo
location: Sydney
---
I've recommended people check out the [Ghost blog platform](https://ghost.org/) for a couple of years now. I statically generate my site with [Hugo](https://gohugo.io), but I appreciate that not everyone wants to live in their [text editor](https://www.gnu.org/software/emacs/) and [Git](https://gameoftrees.org/). Frankly, there are times where I long for a simple to use, server-based blogging platform myself too! Ghost is easier to set up and run than WordPress, even though it runs on Node.

But I was disappointed, though not surprised, to see this hero image on their landing page this morning:

> Turn your audience into a business.

I see why they pitch this. Ghost also comes in a paid, hosted version like WordPress.com, and they need to emphasise the fact it's a potential revenue centre, not a sunk cost. It's those paying customers that subsidise their free, open source version of the software that I host for people, and we should want them to be successful. Still, it makes me sad seeing it as their primary pitch.

Blogging shouldn't only be about business, it should be about creativity. Or anything you want it to be. The web for the last decade has been so conditioned to think social media is the place for people to write ideas, and blogs have become another channel to crank out PR or poorly-written tutorials to wrap with hundreds of low-value ads. I don't begrudge people needing to make money and, you know, *eat*. But the framing of blogging in the industry, and even blogging software itself, has shifted away from people who write their ideas and thoughts and into a business tool, and we're all the poorer for it.

Which is my second issue: summarising your readers as an *audience*. This is subtler, and I'm willing to admit I'm bringing my own biases into what that means. *Audience* to me is only a step above saying people who read your blog are customers. The phrase *blogosphere* became a source of ridicule over the years, but it describes something we've lost: a community. *Audiences* are passive consumers. I'd like to think that with tools like RSS and blogging platforms, we're more like federated writers. (I don't like that phrase either, I'm trying to think of something better).

Back in 2005 I remember [Doc Searls](http://blogs.harvard.edu/doc/) hosting a panel for BloggerCon entitled *Making Money* which has stuck with me ever since. His central argument was that we didn't buy our phones to make money, we bought them as a tool to help us. The analogy doesn't entirely hold in 2020 given how many people *literally* run their businesses and computing off their smartphones, but it's still an interesting way to frame it. A good blog is a sales tool by virtue of it having great content.

*(There's a wider discussion about how low-quality sites and bad actors are [incentivised over honest people](https://rubenerd.com/ann-reardon-on-viral-fake-food-videos/ "Ann Reardon on viral fake, food videos"), but how that pertains to blogging specifically is a topic for another post!)*
