---
title: "With whom I normally disagree"
date: "2020-11-21T09:51:32+11:00"
abstract: "This is a frustratingly open-ended post!"
year: "2020"
category: Thoughts
tag:
- ethics
- philosophy
location: Sydney
---
This has been one of the hardest posts I've tried to write, and the oldest draft I've revived after I first started it in 2006. It's at best me thinking out loud though a complicated mental problem I've made for myself, and it's still frustratingly open-ended. How's *that* for an introduction!?

I read a quote in someone's mailing list signature in the early 2000s that, paraphrased, said we "have to be right all the time in computer security, but that attackers only need to be right once". I thought it was such an elegant, succinct summary of the dichotomy and power imbalance infosec professionals face. I think it's overly simplistic now; systems with perfect security at worst can't exist, and at best are unusable. The best we can do is asses the threat domain and security requirements of a new system, and take reasonable and appropriate measures to protect it.

That hasn't stopped me loving this quote though, which only made it harder when I realised it was a tweaked version of what Donald Rumsfeld said about national security. For those who weren't around or didn't grow up in the 1990s and early 2000s, Mr Rumsfeld was one of George W. Bush's advisors who took the US into the disastrous Iraq war under the concocted pretence that they had weapons of mass destruction. Discovering this connection made me supremely uncomfortable.

Since then I've grappled with this idea that I can agree with people with whom I otherwise don't, and even those I consider reprehensible. How does one reconcile this?

I think there are a couple things going on here. I realise I automatically discount what someone says if I've disagreed with them on something else before, and if I make the effort not to, I still feel a form of guilt by association. So my instinct is to keep doing this. There are ethical voices, based in reality, facts, and science, who likely say the same thing as the people I'm worrying about, and without the baggage I find objectionable. But then, am I just falling into the *divide and conquer* trap by doing this?

**My fear is that I legitimise all of someone's views if I quote but one of them.** I would hate for someone to come across my blog, for example, and be swayed by their other comments. But this could also cut both ways; people from their sphere of influence could encounter words like mine, and change the other way. Probably not! But... maybe?

There are so many examples of this. Winston Churchill was deeply racist, and yet his quote about "if you're going through hell, keep going" has helped me through more than a few dark chapters in my life. Should that make me feel guilty? The fact I'm even asking that speaks volumes. What about Deepak Chopra's brief moments of lucidity among his well-publicised nonsense? See, I'm already falling into the trap.

The best I've been able to come up with is identifying what influences my moral compass. Sometimes there really is no middle ground, like that satirical comic with the KKK and Lincoln, and someone asking if they can compromise by "only enslaving *some* people". Or the parable of the wolf and the sheep deciding what's for dinner. But there's gray in plenty of other cases.

Can I quote people provided I give sufficient context to assuage my concerns about their other work? Does that still fall into the legitimisation trap, or does that still make me an accessory? I'm on the fence.

I'd *like* to think I'm mature and reasonable enough to be able to talk with people who hold views I otherwise disagree with, but I'm self-aware enough to know I don't anywhere near often enough. How I navigate that is something I need to improve on. I'm open to suggestions and ideas.

