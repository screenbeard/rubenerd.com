---
title: "Abandoned blogs as time capsules"
date: "2021-01-14T08:50:40+11:00"
abstract: "And what I may or may not do with them."
thumb: "https://rubenerd.com/files/2021/alamanda-putrajaya1@1x.jpg"
year: "2021"
category: Internet
tag:
- abandoned-sites
- malaysia
- nostalgia
- weblog
location: Sydney
---
Blogs I stumble across today are almost always abandoned. Occasionally they'll make a reference to moving on in their last post, but the vast majority will just end abruptly with their last train of thought. I can't help but think what happened to them; I hope [they're doing okay](https://scobeysays.blogspot.com/ "Scobey Says").

Blogs have existed online for a sufficient enough time that it's feasible for them to have been abandoned for a decade or more. It's interesting seeing the stratification from abandoned Blogger sites, then WordPress, and that brief blip of Octopress. The topics are time capsules into what people were thinking about, and their themes and layouts are a product of their time.

I spent at least an hour yesterday reading through [Travel Thru Time](https://travelthrutime.wordpress.com/), written by a Malaysian about attractions and places to eat in Kuala Lumpur and the wider Klang Valley. I [moved to](https://rubenerd.com/operation-move-to-malaysia/) and [lived there](https://rubenerd.com/moving-back-to-singapore/ "Moving back to Singapore from Kuala Lumpur") not long before the blog started, so it was such a nostalgic joy seeing all those familiar places exactly as they were in late 2009. The writer even used the same Blix theme that I did at the time!

*(Many of their earlier posts were around Putrajaya as well, which we spent a lot of time in because my sister had a friend who lived in the area. Their [specific post about Alamanda](https://travelthrutime.wordpress.com/2009/12/27/alamanda-putrajaya/) sent me right to that Starbucks where so many of my early blog posts were written).*

<p><img src="https://rubenerd.com/files/2021/alamanda-putrajaya1.jpg" alt="View inside Alamanda shopping centre in Putrajaya" style="width:400px" /></p>

I've lamented the decline of blogging here for a few years now, more in terms of an industry trend. Writers are being pulled to incomprehensible Twitter threads, Medium, and Facebook to share their extended thoughts, and blog platforms are pushing people away by branding the exercise as a business activity. But on a personal level I understand people come and go; their lives take on different priorities, or they believe&mdash;erroneously!&mdash;they're not interesting enough. It's still a shame when they let us glimpse their creative energy and potential, only to have it vanish.

I may or may not have immediately downloaded the site on account of being a digital <del>hoarder</del> archivist. I usually exploit the fact WordPress exports a sitemap.xml which I can parse and feed into fetch to download, which is more precise and reduced the spam that blunt site mirroring tools generate. Blogger ones are a bit trickier, but I've got a function in my Perl Swiss Army Knife to parse and download content.

I have a WordPress install running in my homelab that just consists of these imported blogs. Copyright means I probably can't share it&mdash;I think?&mdash;but I'd say well over half the sites I archived have vanished from the public web. Submitting to the Wayback Machine is about the only other hope I have of preserving these thoughts in a public capacity. I feel a responsibility to keep these thoughts, ideas, and snapshots of life alive.
