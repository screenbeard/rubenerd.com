---
title: "Arm-wrestling Gura and Ame"
date: "2020-11-26T10:27:00+11:00"
abstract: "This made our evening yesterday."
year: "2020"
thumb: "https://rubenerd.com/files/2020/hololive-arm-wrestle@1x.jpg"
category: Media
tag:
- amelia-watson
- hololive
- gawr-gura
location: Sydney
---
Seeing [Amelia and Gura tilt their avatars](https://www.youtube.com/watch?v=EBIn-P0bgNY) while playing this arm-wrestling mini game made Clara's and my evening yesterday. Damn it, Hololive EN.

<img src="https://rubenerd.com/files/2020/hololive-arm-wrestle@1x.jpg" alt="Screenshot of Amelia and Gura tilting their avatars while arm-wrestling." style="width:500px; height:280px;" srcset="https://rubenerd.com/files/2020/hololive-arm-wrestle@2x.jpg 2x" />
