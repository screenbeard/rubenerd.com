---
title: "John Roderick did have some points about millennials"
date: "2020-08-02T08:24:28+10:00"
abstract: "We need to address structural issues that affect us all."
thumb: "https://rubenerd.com/files/2020/ENWlxCLXkAAMYAW.jpg"
year: "2020"
category: Thoughts
tag:
- john-roderick
- podcasts
location: Sydney
---
**Update 2021:** I’ve taken down fewer than twenty posts out of more than seven thousand in this blog’s history, and posts about this guy are some of them. [MBMBaM’s tweet put it best](https://twitter.com/MBMBaM/status/1345853685902036994) explaining why, though I also have personal reasons. Thanks for understanding.

