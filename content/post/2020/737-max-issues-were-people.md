---
title: "737 Max issues were people"
date: "2020-11-26T10:14:00+11:00"
abstract: "Once again, it's people, not machines."
year: "2020"
category: Hardware
tag:
- aviation
- news
- people
- processes
location: Sydney
---
David Gelles summarised the issues with the troubled Boeing jetliner for the New York Times, via [Today Online](https://www.todayonline.com/commentary/boeings-737-max-saga-capitalism-gone-awry):

> As an avalanche of investigations and reporting over the past 20 months made clear, the true cause of the crashes wasn’t faulty software. It was a corporate culture gone horribly wrong.

He detailed the single point of failure design of MCAS, and the corporate motivations for profit that overrode the concerns of engineers. It's scary, precisely because it's so believable.

I've talked about the journalistic trick of [claiming false equivalency](https://rubenerd.com/the-issue-isnt-privacy-its-privacy/) to offer a fresh take on what's a re-baked idea. But the technical issues here, like so many in engineering, science, and IT, really can be [explained by people](https://twitter.com/Rubenerd/status/1328157301199364097 "Business Requirements Modelling, with Bernard Wong at UTS. Had an inkling for a while, but showed that most of infocomm/IT is people, not machines."). 
