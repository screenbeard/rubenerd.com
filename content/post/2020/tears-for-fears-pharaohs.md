---
title: "Tears for Fears, Pharaohs"
date: "2020-11-16T09:45:08+11:00"
abstract: "Never heard of the song before, it was on Side B of a 45 Clara and I bought."
thumb: "https://rubenerd.com/files/2020/yt-WkNGWGNQX2o@1x.jpg"
year: "2020"
category: Media
tag:
- 1980s
- music
- music-monday
- vinyl
location: Blue-Mountains
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) was a serendipitous discovery! Clara and I went back to the [Velvet Fog Record Bar](https://thevelvetfog.com.au/) up in Katoomba in the Blue Mountains on Saturday, and bought a Tears for Fears 45 as soon as we laid our eyes on it.

Side A was their world-famous *Everybody Wants to Rule The World* single, but we didn't recognise Side B. We played it last night back home, and I haven't been able to get the melody and background instrumentation out of my head since. It sounds like that cinema scene from *Superliminal*.

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=WkNGWGNQX2o" title="Play Pharaohs"><img src="https://rubenerd.com/files/2020/yt-WkNGWGNQX2o@1x.jpg" srcset="https://rubenerd.com/files/2020/yt-WkNGWGNQX2o@1x.jpg 1x, https://rubenerd.com/files/2020/yt-WkNGWGNQX2o@2x.jpg 2x" alt="Play Pharaohs" style="width:500px;height:281px;" /></a></p>

I love that somewhere, someone had that 45 brand new and listened to it. The wear on Side A was significantly higher than Side B, so who knows, this particular track could have been sitting there unknown for decades, waiting to be discovered again. I like that.

