---
title: "The iPhone 8 (and SE) are still better iPhones"
date: "2022-03-01T08:13:58+11:00"
abstract: "They don’t use OLEDs, they’re light and small, and cost less than a kidney."
year: "2022"
category: Hardware
tag:
- apple
- phones
- iphone
- iphone-8
- iphone-se
location: Sydney
---
Astute readers among you noted the screenshot from my [2022–02–22 post](https://rubenerd.com/2022-02-22/) was clearly taken on a classic, pre-notch iPhone.

I inherited Clara's iPhone XR when she upgraded to an iPhone 12 last year. I used it as my personal phone for half a year, and made the iPhone 8 my work phone. It was an interesting experience, but confirmed my suspicions that the 8 (and the SE that shares much of its design) is a better phone. I went back to the 8 as my personal device, and pulled my old 6S out of retirement for work calls again, the difference was that stark.

We all tend to review things after upgrading, so we forget the limitations of older devices, or are eager to validate our expensive new purchases. I didn't pay for this XR, and I used it in parallel with an 8, so I got a much better idea of real-world use.

*Precise* is the closest I can come to describing the iPhone 8. Every interaction with it feels crisp, sharp, and well-executed. It's hard to describe or quanitfy, but the XR, and every newer Apple phone I've tried in store that uses the new rounded-corner and notch design language, feels floaty, clunky, and imprecise by comparison.

This is most obvious when unlocking the phone or using Apple Pay. I can unlock the 8's TouchID as I bring it out of my pocket in one motion, either to use an application or pay for something. FaceID is simply slower, and using it to pay for trains was hilariously bad. It was also, till recently, redundant when wearing a face mask, and I thought the idea of buying an Apple Watch just to unlock a phone was ridiculous.

People told me I'd get used to the notch on modern iPhones, but six months in and I still haven't. There's no escaping the fact there's an ugly, weirdly-rounded chunk taken out of the top of the screen. Apple could have made this better by having a black bar with indicators for time and signal strength, but they leaned into it instead. Even modern Android phones, which have traditionally aped the iPhone in the most shameless ways possible, tend to look better. If that's not an indictment of modern iPhone design, I don't know what is!

When I've said Apple's modern handsets are a hodgepodge of design compromises, this is what I meant. In the service of removing a single button, everything from UI to industrial design took a hit.

Maybe I could have got used to the XR and later-generation iPhones had I used them exclusively, but using the 8 alongside it was a constant reminder of what iPhones used to be. They don't have the elegance of webOS, or the utility of Symbian, but they're good enough.

An SE with an LCD (we can only hope!) will probably be my next phone, so I can use the 8 as my work phone. I guess the advantage now is that I'll also be able to justify their cost.
