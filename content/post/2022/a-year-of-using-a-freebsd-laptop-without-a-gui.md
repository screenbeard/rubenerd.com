---
title: "A year of using a FreeBSD laptop without a GUI"
date: "2022-08-10T10:52:05+10:00"
abstract: "It’s become my favourite writing machine too."
thumb: "https://rubenerd.com/files/2021/rz6-download-02@1x.png"
year: "2022"
category: Hardware
tag:
- bsd
- freebsd
- writing
location: Sydney
---
A year ago I [tried an experiment](https://rubenerd.com/my-freebsd-laptop-with-just-tmux/)\:

> This feels very strange, but I’m writing this post from my on-call FreeBSD laptop, without X! I have everything I need here to remotely troubleshoot stuff and write, all without needing a GUI. It’s been oddly fun getting back into all this stuff, so I thought I’d share it.

I explained that once I'd assembled the VPNs and basic tooling I needed, I realised none of it required a graphical environment at all. I uninstalled Xorg, and since then have been using tmux as my "window manager".

I'll admit, I left out that I'd eschewed (gesundheit) a desktop environment for my [old friend fluxbox](https://rubenerd.com/mikuru-fluxbox-with-rox-filer-on-freebsd/) first, but even that seemed redundant given I was only using it to spawn a single terminal window with tabs. Removing xorg entirely was the logical next step.

<p><img src="https://rubenerd.com/files/2021/rz6-download-02@1x.png" srcset="https://rubenerd.com/files/2021/rz6-download-02@1x.png 1x, https://rubenerd.com/files/2021/rz6-download-02@2x.png 2x" alt="The Panasonic Let's Note CF-RZ6" style="width:250px; height:219px;" /></p>

I'm still using it in this configuration, and it works great! I can add a new Wi-Fi hotspot to `wpa_supplicant.conf` and restart `netif`. I put it on standby with `zzz`. I can edit posts in vim, do a quick web search in links, read email in Alpine, play some [nbsdgames](https://github.com/abakh/nbsdgames) sudoku, and quickly test a stack by cloning an ZFS dataset and firing up a jail. The machine is lighter than my iPad, and is in some ways its polar opposite.

But it did something else for me, in what I'm dubbing *The Robin Effect* after Robin shared a [machine they used for focus](https://twitter.com/buzzyrobin/status/1554611396251648000). It sounds so obvious in retrospect, but not having a wall of distractions in front of your face is *fantastic* for writing. I've made so much more progress in my various silly sci-fi novels, technical writing, and many of the posts I've since published here.

Sitting at a coffee shop on a Saturday morning with just an empty console window and a blinking cursor is wonderful.
