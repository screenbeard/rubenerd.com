---
title: "Retina, HiDPI scaling in KDE Plasma"
date: "2022-03-12T17:52:10+11:00"
abstract: "KDE is almost perfect for running at 200%"
year: "2022"
category: Software
tag:
- kde
location: Sydney
---
The KDE Plasma desktop handles Retina resolutions almost flawlessly. Using a 200% scale to achieve 2x HiDPI on a capable monitor ensures crisp edges, and no shimmering interpolation when moving or resizing windows.

If you run a 4K or better display with the right drivers on FreeBSD or Linux, Plasma helpfully doubles some of the UI elements. To make 200% scaling universal, there are a few more tweaks to make.

Launch **System Settings**:

1. Scroll down to **Hardware** in the sidebar, and choose **Display and Monitor**. It should select **Display Configuration** for you. Use the **Global Scale** slider to set the scale to **200%**.

2. Click the **&lt;** (Go back) button in the top-left corner, then choose **Appearance**, then **Cursors**. From the **Size** dropdown at the bottom-left, choose **48**.

If you're like me and are still running xorg, you might still see tiny icons in menus and buttons. If so, you can set this environment variable to enforce scaling:

    $ echo "export PLASMA_USE_QT_SCALING=1" >> ~/.profile

Log off and back on, and everything will look as it should :).
