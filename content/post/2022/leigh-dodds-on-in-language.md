---
title: "Leigh Dodds on in-language"
date: "2022-01-28T08:01:43+11:00"
abstract: "Thermoclastic Blooming Artifacts, d’uh!"
year: "2022"
category: Software
tag:
- games
- language
location: Sydney
---
As you've probably noticed by now, I thought today might be fun to share a few quotes from people I like. Here's Leign Dodds [sharing a tweet](https://mobile.twitter.com/BungieHelp/status/1436431407245524992) from a gaming company:

> @BungieHelp: Due to an issue, we have disabled the Telesto Exotic fusion rifle in all Crucible and Gambit activities and the Thermoclastic Blooming Artifact mod in all activities.

[His response](https://blog.ldodds.com/)\:

> For a very specific group of people this probably means a great deal. But I frankly haven’t a clue what a Thermoclastic Blooming Artifact might be. It sounds expensive. Or possibly something you should treat liberally with ointment.

*The Algorithm* constantly recommends me gamer videos, probably beause I watch Hololive streamers. One was about Overwatch, a game I've never played or had an interest in. They strung entire sentences together with words I recognised *in isolation*, but together they may as well have been Markov-trained off a dictionary. It was entertaining in the most surreal way possible.
