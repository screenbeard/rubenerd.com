---
title: "“Get anything you want. No, not that!”"
date: "2022-06-11T09:38:14+1000"
abstract: "Another coffee shop encounter with an undesirable person."
year: "2022"
category: Thoughts
tag:
- coffee-shops
- family
location: Sydney
---
I was at a coffee shop last week, when a meathead and his tired son walked in and started looking at the menu.

> **Dad:** Get whatever the fuck you want, I don't care.
>
> **Teenage son:** Okay, I'll have the spanish omelette.
>
> **Dad:** What the fuck? You could get that anywhere! Get something else, for fuck's sake.

His son stood in place for a few moments, shook his head, and walked out. His dad stood there stunned, then looked at the barista and groaned "fucking kids, am I right?"

Good on the kid. [Mess around with gen Z, and find out](https://knowyourmeme.com/memes/fuck-around-and-find-out).

My dad and grandfathers never treated me this way; not once, not ever. The more I see and hear about other dysfunctional families with abusive parents, I count myself lucky.
