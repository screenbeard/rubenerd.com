---
title: "Too hard? Take more steps"
date: "2022-08-13T12:52:19+10:00"
abstract: "Notes from Jessica Joy Kerr and Merlin Mann that helped me a lot this week."
year: "2022"
category: Thoughts
tag:
- productivity
- work
location: Sydney
---
I've been stuck on a few tasks of late. Where to start, how to finish, what finished even looks like, and thinking that middle part is a murky chasm which reinforces procrastination and self-doubt.

[Jessica Joy Kerr proposed a solution](https://twitter.com/jessitron/status/1532005686502096896) that seems so friggen obvious with hindsight:

> When something gets a little bit hard, I ask: how can I do this in more steps?
>
> It works in code: more lines, more variable names, easier to read and understand later. Walk my future self through the process.
>
> It works in household tasks: making coffee, the beans are on a high shelf. Get a ladder; get the beans; put away the ladder. Each small step is safer than the single hard step of reaching the beans without a ladder.

As my favourite software lecturer used to say, it's better to be clear than clever. He was talking about code reviews, but it easily applies to tasks you assign yourself. We all think we're clever, but "do X" in your task manager feels far less intimidating if it has "do A, B, and C" subtasks underneath.

I've applied this thinking to a few personal and work problems again this week, and it's really helped. It also dovetails beautifully with what [Merlin Mann discussed in 2013](https://rubenerd.com/dependencies-and-merlin-mann/)\:

> Every time you realise a dependency, you get closer to what you need to do.

