---
title: "Identity, Ukraine, and thinking aloud"
date: "2022-02-22T07:10:18+11:00"
abstract: "This one meandered a lot."
year: "2022"
category: Thoughts
tag:
- australia
- personal
location: Sydney
---
Update: I've decided to take this post down.

It was a ramble about my fears on nationalism and feeling disconnected in the world, but the timing was in poor taste given what's happening to our friends in Ukraine right now. My attempt to understand Putin's warped mindset also came across as endorsement of those views.

I apologise for my clumsy words here, and if I trivialised any of your struggles in the world. I can claim it wasn't my intention, but the outcome was the same.

I especially wanted to thank Dmitry A. in Russia, and Danil Smirnov in Latvia for their good faith rebuttals. I learned something, but it shouldn't have come at the expense of kind people.
