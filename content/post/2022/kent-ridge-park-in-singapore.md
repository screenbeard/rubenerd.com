---
title: "Kent Ridge Park in Singapore"
date: "2022-09-07T16:03:35+1000"
abstract: "This was one of my haunts during high school. It was fun seeing photos of it again."
thumb: "https://rubenerd.com/files/2022/krp@1x.jpg"
year: "2022"
category: Travel
tag:
- hiking
- nature
- singapore
location: Sydney
---
I saw the name Kent Ridge Park scroll by my feed earlier in the week, and it took me right back to my childhood! I used to hike through it all the time, especially during school holidays. Waking up at sunrise to beat the humidity and heat, then going for a wander for a few hours was a great way to clear my head.

<figure><p><img src="https://rubenerd.com/files/2022/krp@1x.jpg" alt="Photo of Kent Ridge Park from the Singapore National Parks Board" style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2022/krp@1x.jpg 1x, https://rubenerd.com/files/2022/krp@2x.jpg 2x" /></p></figure>

I forgot it also had [historical significance](https://www.nparks.gov.sg/gardens-parks-and-nature/parks-and-nature-reserves/kent-ridge-park):

> A historical park where one of the last battles for Singapore was fought during World War II, Kent Ridge Park is a great place for families and history buffs to learn about the heritage of our nation.

I need to get out and do more walks like this. Sydney has tons of them too.
