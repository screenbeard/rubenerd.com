---
title: "How minds process bad news"
date: "2022-09-23T09:33:07+10:00"
abstract: "“You cannot do all the good that the world needs. But the world needs all the good that yo can do.”"
year: "2022"
category: Thoughts
tag:
- personal
- philosophy
location: Sydney
---
I'll spare the details, but as I [eluded to on Wednesday](https://rubenerd.com/sitting-outside-with-a-coffee/), stuff is rubbish right now. It's the most challenging time since my late mum passed away, for a host of complicated or unavoidable reasons. At any given moment I'm desperate, upset, blue, or frustrated.

I don't mean to worry anyone! Nor do I want to wallow in self pity, nor am I fishing for compliments or reassurance, it's just life right now. Given how the world is, I suspect you might be in a similar boat, or far worse.

What I do want to raise is something I've observed in my head, especially over the last week, which has weirdly helped.

When you get a bit of bad news, it hits you. When you get two or three bits of bad news, it feels twice or three times as bad. But after the forth, and fifth, and twelfth, the trajectory of pain and frustration seems to level off. Each new item is merely relegated to the stack. At least, that's how it's shook out for me lately.

This is interesting, because it exposes the mental lie for the snivelling little shit that it is. In isolation these things would ruin our day, but as part of a set, their impact isn't as severe. There's no logical reason why this should be; their inputs and consequences in the real world are otherwise identical. So what's functionally different here?

A simplistic (though I'm sure popular) interpretation of this would be to dismiss life's troubles as being "all in one's head", which is demonstrably false. A health condition or intractable situation might be better handled or improved with a sunny disposition, but it's not sufficient to solve it.

Instead, the way I've started to see these things as a form of mental training, which may be good or bad. We wire neural pathways in our brains to accept certain feelings and outcomes based on what happens to us, or what thoughts we're having. Each negative experience introduces a new path, or with a sufficient number of similar thoughts, digs an existing one deeper. Eventually they become so dark and distant from the surface, we start to believe our own catastrophising bullshit as being inevitable, because we're incapable of seeing an alternative.

Giving up is not an option. Despite our darkest thoughts, you and I are worth more than that. So therefore, the challenge is to identify when one of these paths are becoming an inescapable trench, and do what you can to extricate yourself and fill that fucker in. It may require difficult conversations or decisions. But&mdash;and this is the hardest lesson I've learned of late&mdash;if you don't look out for yourself first, you can't help anyone else.

Rebecca Hales reminded me of Jana Stanfield's [famous quote](https://www.goodreads.com/quotes/796499-i-cannot-do-all-the-good-that-the-world-needs), which I'm tempted to print as a sticker:

> I cannot do all the good that the world needs.   
> But the world needs all the good that I can do.

