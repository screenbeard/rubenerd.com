---
title: "2022–02–22"
date: "2022-02-22T19:30:50+11:00"
abstract: "The most number of consecutive twos we'll see in our lifetimes."
year: "2022"
category: Thoughts
tag:
- dates
- numbers
- pointless-milestone
location: Sydney
---
Assuming you abbreviate your dates without leading zeros, this is the most number of consecutive twos we'll ever experience in our lifetimes. That's cool!

Here are some other valuable posts:

* Related: [2022–02–02](https://rubenerd.com/2022-02-02/)
* Somewhat related: [2011-01–11](https://rubenerd.com/11-11-11-11-11/)
* Not related: [Dates](https://en.wikipedia.org/wiki/Date_palm#Fruits)

*Update:* 2022-02–22 22:22 unlocked and preserved for posterity! If only I'd worn the battery down just *little* bit more.

<p><img src="https://rubenerd.com/files/2022/22-22-phone@1x.jpg" srcset="https://rubenerd.com/files/2022/22-22-phone@1x.jpg 1x, https://rubenerd.com/files/2022/22-22-phone@2x.jpg 2x" alt="Time on my iPhone showing 22:22" style="width:375px; height:667px;" /></p>
