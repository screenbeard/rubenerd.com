---
title: "Querying TXT records with drill(1)"
date: "2022-11-11T11:49:17+11:00"
abstract: "drill $DOMAIN txt"
year: "2022"
category: Software
tag:
- bsd
- dns
- freebsd
- netbsd
- linux
- macos
- things-you-already-know-unless-you-dont
location: Sydney
---
In today's installment of *[things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont)*, this is how you look up a TXT domain record with **[drill(1)](https://www.freebsd.org/cgi/man.cgi?query=drill)** on FreeBSD:

	$ drill $DOMAIN txt

You'd think that I'd get that order right one of these days.

On NetBSD, macOS, and Linux, the same applies for **[dig(1)](https://man.netbsd.org/dig.1)**.

