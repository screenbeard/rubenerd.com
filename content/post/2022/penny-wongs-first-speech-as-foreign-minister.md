---
title: "Penny Wong’s first speech as foreign minister"
date: "2022-05-27T08:41:25+1000"
abstract: "I can't express what a relief it was to hear this."
thumb: "https://rubenerd.com/files/2022/yt-L7EihsXxF28@1x.jpg"
year: "2022"
category: Thoughts
tag:
- australia
- politics
location: Sydney
---
<p><a href="https://www.youtube.com/watch?v=L7EihsXxF28" title="Play IN FULL: Penny Wong speaks in Fiji on her first solo trip as Foreign Minister | ABC News"><img src="https://rubenerd.com/files/2022/yt-L7EihsXxF28@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-L7EihsXxF28@1x.jpg 1x, https://rubenerd.com/files/2022/yt-L7EihsXxF28@2x.jpg 2x" alt="Play IN FULL: Penny Wong speaks in Fiji on her first solo trip as Foreign Minister | ABC News" style="width:500px;height:281px;" /></a></p>

I can't express what a relief it was to hear this.

She's made some lofty claims, and it'll take more that some positive sentiment and commitments to reassure a community that's been ignored and ridiculed for almost a decade, but it's a start.
