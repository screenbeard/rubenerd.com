---
title: "My lazy approach to FreeBSD dual-booting"
date: "2022-03-11T08:51:59+11:00"
abstract: "Use the BIOS!"
thumb: "http://rubenerd.com/files/2020/beastie@2x.png"
year: "2022"
category: Software
tag:
- bsd
- freebsd
- feedback
- questions
location: Sydney
---
Brad Alexander emailed asking how I dual-boot my FreeBSD workstation that doubles as a Linux Steam game machine.

I must confess, I use the BIOS boot menu!

I used to use [grub](https://www.oreilly.com/library/view/linux-in-a/0596004826/ch04s03.html), but lately I've decoupled OSs to their own SSDs. The FreeBSD NVMe drive boots by default, but striking F11 at boot brings up the motherboard's "boot override" menu where I can select the SATA drive with Linux.

There are a few advantages to this approach:

* No further configuration after installing each OS.

* Drives can be physically removed, upgraded, or wiped without affecting the other OS and its partitions.

* Less stressful OS upgrades, because you won't inadvertently wipe the boot loader the other OS needs.

* Faster boots to a primary OS; FreeBSD in my case. With an active boot menu, you're always choosing an OS or waiting for a timeout.

The biggest drawback is you need two separate drives, or four if you run mirrored pairs as I do on my FreeBSD bhyve NAS. That might prove expensive if you're on a budget, and if this is your primary computer.

My aging Skylake board only has one NVMe slot, which means a mirrored zpool would only write as fast as a SATA. The Ryzen Z570 boards I'm looking at have two NVMe slots that run at comparable speeds, which would make mirroring tenable again.
