---
title: "Discovering 850 watt SFX power supplies exist"
date: "2022-09-06T09:34:23+10:00"
abstract: "Looking at EVGA’s unit made me think whether even having such components makes sense in a Mini ITX build."
thumb: "https://rubenerd.com/files/2022/123-GM-0850-X1_LG_1.png"
year: "2022"
category: Hardware
tag:
- building-pcs
location: Sydney
---
Small form-factor computer enthusiasts have to accept compromises to save space. Tall air coolers for dense CPUs might protrude too far, and liquid radiators might be too wide. Larger graphics cards might be too long to fit, take up too many PCI slots, or need additional physical support. But even if our smaller cases could accommodate all this, our power supplies might not.

My current desktop power supply is a Corsair 750 W Platinum SFX. I've used Corsair power supplies for more than a decade; they're quiet, well constructed, easy to cable up, and have outlived other components by years. They're also packaged thoughtfully; something you don't realise is nice until you get it.

But I thought 750 watts was the highest one could go in this form factor, which limited hardware options like graphics cards.

*Turns out*, in the time I looked there are a few 850 W, which is what the 6900XT and 3080 recommend. [Here's EVGAs](https://www.evga.com/products/product.aspx?pn=123-GM-0850-X1), which fortunately is called a *Supernova*, not a *Collateral Damage*, *Shootout*, *Vengence*, or *Hyper-Extreme Fury*. Ah, PC hardware manufacturers, always at the forefront of class and taste!

<figure><p><img src="https://rubenerd.com/files/2022/123-GM-0850-X1_LG_1.png" alt="EVGA 850W PSU, with its box" style="width:256px; height:239px;" /></p></figure>

I guess the bigger question is whether you'd *want* to run these cards in a confined space, even if you could. The NR200P is an airflow-focused case, but I already feel like I'd be pushing its thermal limits with anything more than it currently has. But it's also where the industry is trending.
