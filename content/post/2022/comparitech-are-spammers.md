---
title: "Comparitech are spammers"
date: "2022-03-12T09:35:18+11:00"
abstract: "Do not do business with this company."
year: "2022"
category: Internet
tag:
- spam
location: Sydney
---
Nearly every day spammers ask me to include sponsored content for nefarious sites, usually for gambling or fake downloads. The other kind asks me to link to their site under the guise of fixing outdated or broken links. It's beyond tedious at this stage.

Here's one such example:

> I noticed you've shared TrueCrypt on this page https://rubenerd.com/p1117/, as you may be aware, development of TrueCrypt was discontinued back in 2014 and has subsequently not been maintained. A number of security flaws have been uncovered and as a result we are reaching out to people to highlight a list of alternatives.
>
> Here's the list (along with further details about TrueCrypt no longer being maintained) - $REDACTED, when you update your page it could be a useful resource to point your visitors to.

You can tell these are automated, or not written by people who've spent more than thirty seconds on their victim's site. I'd written at length about TrueCrypt back in the day, including when it was retired and where to move on to. Their advice wasn't to link to VeraCrypt, or any of the other offshoots... it was to link to their own site.

I consider this a lazy form of spearfishing, not to gain access to a system to steal money or credentials, but to boost their search engine rankings. They're mail merged with just enough information about their intended target to sound convincing and personal, but in reality they're generated from simple web searches.

The above example was from one offender among many called Comparitech. They've emailed me dozens of times on my personal email and at work, but their spamming of the FreeBSD mailing lists was beyond the pale. Their domains have included:

* comparitech.ltd
* comparetech.org [sic]
* comparitech-mail.com
* ctechemail.com

They might claim their site is benign and their content is useful. I call shenanigans, given they're spamming mailing lists, and doing so from multiple domains. Why would a legitimate outfit do this, if not to thwart email filters?

Only the first two domains currently resolve, which point to Dreamhost and Rackspace IP addresses. I've sent emails to their abuse lines reporting their behaviour that clearly violate the platform's terms of service.

I suppose the irony is that I *did* write about them... just not in the way they intended. Here's a *Comparison* for them: you're a processed meat product in a can!
