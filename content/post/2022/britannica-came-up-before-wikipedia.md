---
title: "Britannica came up before Wikipedia"
date: "2022-05-24T07:28:32+10:00"
abstract: "This may have been the first time this happened for me."
year: "2022"
category: Internet
tag:
- code-geass
- encyclopedias
location: Sydney
---
I realise now the title of this post could be read a few ways. Britannica certainly came up before Wikipedia... by more than a century! And if I were being precise, I'd have to specify I'm talking about the Encyclopædia Britannia, not the personification of Britannia, or the Britannia Empire from *Code Geass.* And people say I get distracted easily.

Related to my [Music Monday post](https://rubenerd.com/angelique-kidjo-azan-nan-kpe/ "Angélique Kidjo, Azan Nan Kpe") yesterday, [Angélique Kidjo](https://www.britannica.com/biography/Angelique-Kidjo "Article on Britannica") was the first web search I think I've ever done where the first result was to Britannica, not Wikipedia:

> Angélique Kidjo, (born July 14, 1960, Ouidah, Dahomey [now Benin]), Beninese popular singer known for her collaborations with internationally prominent popular musicians and for her innovative blending of diverse musical styles.

I feel like there's a parallel universe where Kodak produces the best selling and most respected digital cameras, digital LEGO let builders craft with blocks in a massive sandbox mine environment, Flickr is also the way people share photos on their phones, and Britannica rules the open web with community posts and in addition to their professionally written content.

If we think the [butterfly effect](https://en.wikipedia.org/wiki/Butterfly_effect_in_popular_culture "Article on Wikipedia about the butterfly effect in popular culture") is true, it wouldn't have taken much for these to have happened instead. Maybe a short whisper into someone's ear, then slinking back into the night. Or the Tardis. Or the USS *Relativity*.
