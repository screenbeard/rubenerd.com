---
title: "Trying vim-plug"
date: "2022-05-04T14:03:41+1000"
abstract: "Really slick, all it needs is the plugin names in your vimrc."
year: "2022"
category: Software
tag:
- editors
- vim
location: Sydney
---
I've used [Vundle](https://github.com/VundleVim/Vundle.vim) for years to manage my Vim plugins, but I saw [vim-plug](https://github.com/junegunn/vim-plug) referenced in a plugin's documentation, and thought it was worth a try. I don't use many plugins, so the idea of a "minimalist" tool was appealing.

I added the following to my **~/.vimrc**, with a larger and smaller plugin to test:

	call plug#begin()
	Plug 'sirver/UltiSnips'
	Plug 'jsit/toast.vim'
	call plug#end()

Then downloaded vim-plug into the **~/.vim/autoload/** folder:

	$ mkdir ~/.vim/autoload
	$ cd ~/.vim/autoload
	$ curl -OL https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

Now I can run **:PlugInstall!** to get them installed, surprising though it may seem.

	Updated. Elapsed time: 14.727488 sec.
	[==]
	- Finishing ... Done!
	- UltiSnips: Resolving deltas: 100% (527/527), done.
	- toast.vim: Receiving objects: 100% (5/5), 4.94 KiB | 4.94 MiB/s, done.

That was it! Nice.

Thanks to [Junegunn Choi](https://github.com/junegunn) and the [contributors](https://github.com/junegunn/vim-plug/graphs/contributors) for this excellent tool. They've [got a coffee](https://www.buymeacoffee.com/junegunn) coming from me.

