---
title: "How we run Minecraft"
date: "2022-07-11T09:31:06+10:00"
abstract: "We don’t do anything exotic, but putting here in case there’s something useful."
year: "2022"
category: Software
tag:
- freebsd
- games
- minecraft
location: Sydney
---
I'm a decade late to Minecraft, but it's now my favourite game ever. It's the procedurally-generated, open-world block game that captures that sense of joy and enthusiasm I had with LEGO as a kid, *and* I get to play it with Clara.

I've been asked about how we run it every time I post on social media, so I'm finally addressing what I've learned here. There's nothing especially exotic about our setup, but maybe you might find something useful.


### Running a Minecraft server

Clara and I run Minecraft on our [home FreeBSD server in a jail](https://rubenerd.com/starting-with-freebsd-jails/), which keeps Java and other dependencies contained in one isolated place. Theoretically you can run the server anywhere that supports Java, including Linux and possibly even NetBSD, though I haven't tested the latter.

*(Update: I've [tested on NetBSD](https://rubenerd.com/netbsd-can-also-run-a-minecraft-server/), and it works! I wrote a separate post about).*

Once you've installed an OpenJDK or equivalent to run Java software, you download the [official Minecraft server](https://www.minecraft.net/en-us/download/server). I have a script to run it in a screen session so it persists after logging off. I also give it more RAM than the official guide suggests.

	#!/bin/sh
	screen java -X8192M -Xms8192M \
		-jar minecraft_server.1.19.jar nogui

Once that's running, you can connect to your server's IP address from your desktop Minecraft client.


### Backing up

Because we run FreeBSD for the server, we use OpenZFS snapshots to regularly backup our world. Klara Inc has a [great introduction](https://klarasystems.com/articles/lets-talk-openzfs-snapshots/) on how they work. Thus far we haven't had to, but if an update breaks things we can also use a [rollback](https://openzfs.github.io/openzfs-docs/man/8/zfs-rollback.8.html). We also [send](https://openzfs.github.io/openzfs-docs/man/8/zfs-send.8.html) our world to a server sitting at my dad's place for a remote copy.

**Back. Up. Your. World.** Make a tarball, rsyncing elsewhere, or even do a manual copy every week; anything is valuable.


### Upgrading from the standard Minecraft server

Thesedays we run [PaperMC](https://papermc.io) instead. It's a high-performance fork with frequent updates and plugin support. Most importantly for me, you can also take your world back to vanilla Minecraft if you want.

It's a drop-in replacement, so you can [download it](https://papermc.io/downloads) and run using the same script as I had above, substituting the jar file for vanilla Minecraft with the paper version.

Note that once you start using third-party tools like this, official Minecraft updates might take a few more days to come through. Make note of the version on the download page, so you don't get a mismatch between the official client and what your server is running.


### Extras

The Minecraft universe has thousands of plugins and extensions, though I tend to prefer to keep things simple.

Until recently we ran the [Multiverse 2](https://www.spigotmc.org/resources/multiverse-core.390) plugins, which let us import and build portals between multiple worlds on the same server. We have a peaceful primary world, and a world with mobs we go to when we need training or drops. As of 1.19 people are recommending a migration to [MyWorlds](https://www.spigotmc.org/resources/myworlds.39594/). I haven't done that yet, it's probably worth a post itself. Worlds you import still reside in separate folders, so you can always pull them back out and run vanilla if you want.

Plugins and mods in servers are installed by downloading their jar file into the `plugin` folder of your Spigot, PaperMC, or similar install. More extensions are beginning to depend on the [BKCommonLib](https://www.spigotmc.org/resources/bkcommonlib.39590/) library as well, so I'd have that installed.

Another very popular mod is [WorldEdit](https://dev.bukkit.org/projects/worldedit/), that lets you do things like generate structures and change biomes. We've used it to [import buildings from other worlds](https://gaming.stackexchange.com/questions/329861/whats-the-fastest-way-to-move-builds-from-world-to-world) into our primary one using schematics.


### Client-side improvements

Installing [Optifine](https://www.optifine.net/) (or a similar tool) is the best thing you can do on the client side to make Minecraft perform and look better. Once you have that, our favourite shaders are still the [Complementary Shaders](https://www.curseforge.com/minecraft/customization/complementary-shaders) by EminGTR, and not just because we're [Takodachi](https://www.youtube.com/channel/UCMwGHR0BTZuLsmjY_NT5Pwg/featured).

There are plenty of third-party launchers as well, though I haven't ever used them.


### Supporting developers

This is the final step people usually overlook. If you like using those third-party tools, plugins, and shaders, support their work with a donation. Each one of their landing pages has a support button of some description. Even $5 would be $5 more than what almost anyone gives them.

It can be a thankless job maintaining such tooling, especially when a Minecraft update drops and comment fields are full of angry, entitled people insisting they "hurry up". Money doesn't just help them financially, it's a clear signal that people value them and their work. Which we do, or we wouldn't use their stuff!
