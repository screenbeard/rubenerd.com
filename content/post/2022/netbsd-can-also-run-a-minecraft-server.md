---
title: "NetBSD can also run a Minecraft server"
date: "2022-07-17T10:23:54+10:00"
abstract: "Changing pkgsrc to get the required OpenJDK version, some unsupported OS warnings, and other notes. It works!"
thumb: "https://rubenerd.com/files/uploads/netbsd-smaller.png"
year: "2022"
category: Software
tag:
- bsd
- guides
- minecraft
- netbsd
- pkgin
- pkgsrc
location: Sydney
---
Last Monday I wrote this about our [FreeBSD Minecraft server](https://rubenerd.com/how-we-run-a-minecraft-server/)\:

> Clara and I run Minecraft on our home FreeBSD server in a jail, which keeps Java and other dependencies contained in one isolated place. Theoretically you can run the server anywhere that supports Java, including Linux and possibly even NetBSD, though I haven’t tested the latter.

Well, I decided I couldn't leave it at that! The good news is, **yes, you can run a Minecraft server** on my other favourite OS too. This post explores how I went about it, though I'm sure there are other ways.


### Getting Java installed

Once you've installed NetBSD, [configured networking](http://netbsd.org/docs/guide/en/chap-boot.html#chap-boot-rc-conf), and [enabled pkgin](http://netbsd.org/docs/guide/en/chap-boot.html#chap-boot-pkgsrc), you need to get an OpenJDK to run Minecraft. 

Minecraft 1.17 onwards [requires OpenJDK 17](https://minecraft.fandom.com/wiki/Server/Requirements#Server_requirements), but [OpenJDK 16](https://pkgsrc.se/lang/openjdk-bin) is the latest in the most recent snapshot as of July 2022. You can search to confirm if this is still the case:

	# pkgin search openjdk

If it's still 16, you can pull current packages by opening your repo file:

	# vi /usr/pkg/etc/pkgin/repositories.conf

And changing the repo URI, in my case from **9.0** to **9.0_current**:

	https://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/$arch/9.0_current/All

Thank you so much to the ryoon@ for [working on this](https://mail-index.netbsd.org/pkgsrc-wip-changes/2022/03/25/msg023569.html). He does [so much great work](https://pkgsrc.se/bbmaint.php?maint=ryoon|a|NetBSD.org) for the NetBSD community, I feel like I owe him at least a coffee or a beer next time I'm able to go to Japan. 🙇

Now we can install, along with a few other useful tools:
	
	# pkgin install -v openjdk17 fetch tmux


### Running Minecraft

From here, running Minecraft is basically the same as any Java-enabled server. I put all my files in one place:

	# mkdir -p /opt/minecraft
	# chown $YOUR_LOCAL_USER /opt/minecraft

Then log in as my local user and start:

	$ cd /opt/minecraft
	$ fetch $SERVER_URL_FROM_ABOVE.jar

Now we can start!

	$ openjdk17-java -jar minecraft.jar

You'll notice Java will throw a **system not supported** exception, which those of us on NetBSD know all too well. From my testing, you can safely ignore it:

	[ServerMain/WARN]: Failed retrieving info for group hardware
	java.lang .UnsupportedOperationException: Operating system
		not supported: NetBSD
		at oshi.SystemInfo.createHardware(SystemInfo.java:163)

And don't forget to accept the EULA after you run the server the first time:

	$ sed -i '' 's/eula=false/eula=true/' ./eula.txt


### Creating a launch script

I like to symlink the latest version of the server to **minecraft.jar**:

	$ ln -s /opt/minecraft/minecraft_server.1.19.jar minecraft.jar

Then reference it in **launch.sh** with tmux to persist the server after disconnecting. I like to give Minecraft more memory too:

	#!/bin/sh

	tmux new -s minecraft \
		openjdk17-java -Xmx4096M -Xms4096m -jar minecraft.jar

Now we're good to go!

	$ chmod +x start.sh
	$ ./start.sh


### Follow-up

My next step would be creating a proper [chroot environment](https://man.netbsd.org/chroot.8) for Java and Minecraft, similar to what I do with FreeBSD jails. I've had a proper NetBSD chroot exploration post in the works for years; I'll tidy up and post it soon.

In the meantime, you can also install all the same extensions and third-party servers I referenced in my [FreeBSD Minecraft post](https://rubenerd.com/how-we-run-a-minecraft-server/) last week.

I'm chuffed this works! 🧡
