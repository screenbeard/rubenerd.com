---
title: "The surprising backlash against RSS"
date: "2022-03-14T10:22:03+11:00"
abstract: "Anyone happy about reducing data transparency and interop should be treated with suspicion."
year: "2022"
category: Internet
tag:
- rss
- standards
location: Sydney
---
The backlash against RSS, Atom, and other open-syndication formats has been a genuine surprise. Stories predicting their demise are routinely met with glee and edgelord commentary from a vocal minority who should know better.

It speaks to a short memory and lack of imagination.

Can anyone say, objectively, that having multiple incompatible mobile applications with their own UIs and logins are better than having a single podcast client, for example? If so, hook me up with your dealer, I could use a few hits of that for my anxiety right now!

My pet conspiracy theory is that such people are funded by social networks and repressive regimes to keep information locked up, sanitised, monitored, monetised, and controlled. Anyone who's happy about reducing data transparency and interoperability should be treated with suspicion.
