---
title: "Birthday Ryzen Minecraft shaders!"
date: "2022-03-27T09:34:08+11:00"
abstract: "A bit of a rambling mess where I talk about my new game machine build."
thumb: "https://rubenerd.com/files/2022/birthday-shaders@1x.jpg"
year: "2022"
category: Hardware
tag:
- birthday
- minecraft
- pointless-milestone
location: Sydney
---
When building a game machine, the conventional wisdom is to allocate as much money as you can to your graphics card over your CPU, motherboard, and memory. It's the part of the system that will have the biggest impact to the performance you care about.

I took that to heart perhaps a little too much. Rather than replace the parts in my 2016 sixth-gen Skylake machine, in February I threw *all* my money at an RTX 3070 graphics card, and swapped out the previous GTX 970. Turns out this was specularly bad timing, given the precipitous drop in GPU prices over the last month, but [that was for another post](https://rubenerd.com/my-rtx-3070-and-hobby-guilt-again/)!

The difference was *stark*, even with half-decade old hardware! Minecraft at 4K went from "only enable shaders for screenshots" to *almost* playable at 50 FPS. Save for some long chunk loading times when the game initially started, I was surprised at how playable it was.

Last week Clara surprised me with some Ryzen parts for my birthday, including an [AMD Ryzen 5600x CPU](https://twitter.com/Rubenerd/status/1506888755419910148/photo/1), an Asus ROG Strix X570-I board, 16 GiB Corsair 3600 DIMMs for dual-channel, and matching CoolerMaster NR200P mini-ITX cases in teal and pink! Have I told you she knows me *shockingly* well?

We spent the day upgrading yesterday, along with building her new machine. It was a ton of fun, and I was proud that she was able to assemble something for the first time. Build photos and process will be in another post.

<p><img src="https://rubenerd.com/files/2022/birthday-shaders@1x.jpg" srcset="https://rubenerd.com/files/2022/birthday-shaders@1x.jpg 1x, https://rubenerd.com/files/2022/birthday-shaders@2x.jpg 2x" alt="Pathway before our copper bridge to Shy Guy Beach" style="width:500px; height:281px;" /></p>

Booting it [into Kubuntu](https://rubenerd.com/why-my-game-pc-also-runs-freebsd/), I fired up Minecraft and enabled OptiFine and the Complementary shaders at ultra settings. The game loaded almost *instantly*, including all the chunks around me. I walked around in some of our relaxing parks, and it comfortably sat at almost 70 FPS at 4K. On the other side, jumping through nether portals also went from waiting ~20 seconds to 5. Using our over-engineered nether Shinkansen system is now tenable again!

Minecraft is a creaky, unoptimised game, but OptiFine and this new hardware brute forces it so well on this one screen I have space for. I know shooting for 4K is a bit silly, but I need it for my WFH setup, so to be able to swap the input to this game system helps.

I suspect it's less to do with the CPU here and more the memory bandwidth for loading assets. Minecraft isn't that multithreaded either, so the 5600x's fewer cores with higher burst performance might run cooler and work better too.

There are plenty of other sites online where you can get detailed benchmarks; this was more a post to express my joy at this build, and to remind people that while GPUs are the most important part of a build, don't skimp *too* much on the other parts if you can. You can definitely stagger them though so you're not spending too much at once.
