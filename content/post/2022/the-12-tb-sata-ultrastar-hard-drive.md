---
title: "A 12 TB SATA Ultrastar hard drive saves the day"
date: "2022-02-13T09:32:56+11:00"
abstract: "With some lessons and fun with warranties."
year: "2022"
category: Hardware
tag:
- hard-drives
- seagate
location: Sydney
---
This is less of a review of an excellent hard drive, and more a meandering story about how we got here. Last September I talked about a [defective 12 TB Ironwolf drive](https://rubenerd.com/my-first-shipped-doa-drive/) that I was shipped after buying it online. Half a year later, and we finally have a happy ending!

### The drive of doom

To recap, Clara and I needed more drive space in our homelab server for backups. Western Digital had earned my trust and respect after years of flawless service, but their [SMR shenanigans](https://rubenerd.com/hidden-shingled-drive-follow-up/) made me look past backup provider reports, and try Seagate again for once.

The end result was worse than the original post let on. After the drive was shipped back to the retailer, who then shipped it to Seagate, we got a report back confirming the drive was faulty. The retailer sent us a new drive, and it arrived... almost two months later.

I attached this replacement drive to our homelab server, and it exhibited almost the exact same symptoms: random clicking sounds, a regular loud beep, and the BIOS refusing to detect it. This time the Supermicro board even made beeping sounds which indicated an error on one of the SATA ports. Swapping out the SATA cables, and plugging it into a different machine made no difference.

I shipped it back again, but by this point it was Christmas and Covid cases were surging again in Australia. I harbour no ill will to any person in this chain during such a crunch, but we still went for weeks without nay a peep from either the retailer, or Seagate. When I finally opened a new ticket a month later, they offered me the choice of a replacement unit, but I went with a refund.

By this point almost six months had past, and the cost of drives had come down to the point where I could get a 12 TB Ultrastar for an equivalent price to the original Ironwolf! This one arrived within a few days from Mwave this time, and it's been resilvering and scrubbing in our OpenZFS array since. I was super impressed with how well the Mwave team packaged it for shipping, which I'm sure contributed to this success.

	  pool: zwork
	 state: ONLINE
	  scan: resilvered 6.53T in 09:15:07 with 0 errors on Sun Feb 13 03:32:25 2022
	config:

	  NAME                             STATE     READ WRITE CKSUM
	  zwork                            ONLINE       0     0     0
	    mirror-0                       ONLINE       0     0     0
	      gpt/12TB-WDRedPro-XXXXXXXX   ONLINE       0     0     0
	      gpt/12TB-Ultrastar-XXXXXXXX  ONLINE       0     0     0

### Quick review, and lessons

As I said at the start, the 12 TiB SATA Ultrastar is a fantastic drive. Clara thought it sounded like "bubbling water pipes" when seeking, instead of the high-pitched clicking and whirring sounds you typically expect from a drive. It is a bit loud in our tiny apartment, but it's not obnoxious. And it works!

I learned some lessons here. People assume that you can rebuild RAID arrays or ZFS pools by simply swapping out defective drives, but that might not always be tenable in a timely fashion in a residential setting. I built a **ghastly** workaround to maintain redundancy (involving striping some old drives and using USB) once I realised a replacement wasn't coming any time soon. If you're on a budget, or dealing with expensive 12+ TB drives, it might be worth working on such contingency plans.

I strongly suspect based on the identical manufacturing dates and symptoms that the Ironwolfs (Ironwolves?) I was delivered were from the same batch, so it'd be unfair to characterise the entire line as being more likely to die. For all I know, the pallet they were shipped on might have been bumped, which is no fault of the manufacturer or retailer. If that's the case though, I'm sure that means plenty of others were bitten from this batch, so I hope they're all being recalled or written off, and not relying on every single person reporting it.

The experience did validate my long-running practice of mixing drive manufacturers and SKUs so that a fault in one batch doesn't affect the other drive in a mirror or RAID. Some people shun this practice, claiming you'll get differing performance characteristics even if you try and match caches and rotation speeds, but reliability is more important to me.

I've got a post pending where I quantify my experience with various drives over the last decade. But like all anecdotes limited to a dozen or so data points, it'd be disingenuous and illogical to assume there's any meaningful information to glean. The fact my experience broadly tracks with those famous Backblaze reports is probably coincidence.

Having said *all* that, this and other experiences have me leaning WD if the choice came up, and especially those by the former HGST if I can. From my own albeit limit experience with a dozen or so drives, they're just better.
