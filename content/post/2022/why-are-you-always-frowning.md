---
title: "Why are you always frowning?"
date: "2022-04-05T09:42:35+10:00"
abstract: "Frowning feels a lot like bad posture; you’re oblivious to doing it until you make a conscious effort to notice."
year: "2022"
category: Thoughts
tag:
- coffee
- personal
location: Sydney
---
I went to get coffee this morning at my local favourite, and the barista asked if she could ask a question after delivering the brew to my table. *You just did!* I chortled.

She asked why I'm always frowning, and if I was okay.

I'll admit I've had a lot weighing on my mine of late (haven't we all?) but I was surprised that I was wearing this on my face the whole time. I didn't think I had been.

Frowning feels a lot like bad posture; you're oblivious to doing it until you make a conscious effort to notice and correct it. Sure enough, I went back to typing and could feel myself frowning as I was catching up on my morning RSS, then again when replying to an email, and *again* when I accepted the day's meeting invites.

I wonder how much I've been subconsciously informing my morning mood with this expression? I smile at the baristas and wish them a good morning, but I guess I've been regressing this whole time.
