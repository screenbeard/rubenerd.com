---
title: "Dealing with depression (fun!)"
date: "2022-04-29T07:31:11+10:00"
abstract: "Talk with people. Get outside."
year: "2022"
category: Thoughts
tag:
- personal
location: Sydney
---
So many of my technical posts here started because I learned something, and I figured it was worth making someone else's life easier if I shared it. Today we're doing some brain hacking, and exploring some thoughts that I'm sure are not uncommon in Our Current World&trade; right now.

For some personal background, is a phrase with four words. I've had anxiety for as long as I can remember. It was misdiagnosed a few times growing up, but it took a few panic attacks in my late twenties and early thirties for medical professionals to see exactly what was going on and to help with treatment and mental exercises. I still jump when a bus beeps a horn across the street, and my workspace always has to face *into* a room to avoid fight-or-flight panic, but that's become manageable over the last few years.

What's blindsided me over the last six months is depression. For all the family turmoil my sister and I suffered as kids, I only ever had what could best be described as teenage angst and sadness. I've been lucky that I've always had something to do, think about, and enjoy, even in the midst of bad things. I had worries about my future, but that manifested as anxiety, not depression.

I'm not sure what the spark was late last year. I suspect it was some family trauma, an overall case of *[ennui](https://www.collinsdictionary.com/dictionary/english/ennui "Collins dictionary definition: a feeling of being tired, bored, and dissatisfied.")* at the state of the world, not being able to travel (the main thing Clara and I save money to do), and not feeling like I was giving friends and work the attention they deserved. Easy things became difficult, and difficult things became insurmountable. That feedback loop is difficult to unwind once those neural networks are established.

One of my favourite quotes from my dad used to be that "only boring people get bored", but now I appreciate that's simplistic. Depression is insidious *precisely* because it sucks the joy and motivation out of doing things you otherwise love, or that you know you *want* to do. I look over at the pile of half-completed hobbies at my desk now, and shake my head with bemusement and exasperation that I can't work up the fortitude to pick any of them up again.

Which leads me to what to do about it. It's hard, and might not always be possible, but you need to see someone about it. I empathise that in the pits of it, it can seem ridiculous to think someone else could understand what you perceive to be a messed up state of mind, let alone offer help. I went through many sleepless nights thinking *yeah, but what could they do, it'd be pointless*. But in my case she was able to observe and offer insight I didn't expect.

"Touch grass" has become a meme, but it does help too. I hadn't noticed that I'd retreated indoors again instead of using our balcony for work and personal projects like I had last year. Sunshine and fresh air do wonders for my outlook, even if temporarily. I'll take it.

I don't feel like I have an especially severe case, but I have newfound respect for those of you who've spent your entire lives like this. Feeling like your own mind is working against your happiness, self-worth, and productivity is so many levels of fucked. You and I are worth more than these degrading thoughts. Big love from me.
