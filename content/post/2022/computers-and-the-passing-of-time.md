---
title: "Computers and the passing of time"
date: "2022-04-07T08:17:49+10:00"
abstract: "I can account for every crack, dent, and scratch in this old machine. It’s reassuring, somehow?"
year: "2022"
category: Thoughts
tag:
- pentium-mmx
- personal
- time
location: Sydney
---
My coffee-fuelled retrocomputing adventures have reached the point where I've realised these machines are not only older than people at school now, or at university, but even people I meet professionally. I then realise the 1990s weren't "a decade ago", and suddenly it makes sense why finding parts for these things is becoming more difficult.

I'm in my mid-thirties, so I wasn't alive when the Commodore computers I tinker with were first brought into the world; and certainly not this stack of core memory, or this PDP-11 clone! But my Pentium 1 tower has been with me since I learned Visual Basic, partitions, and networking as a kid. I can account for every scratch on her otherwise unremarkable beige bezel, every dent in her metal frame, and why there's a small drilled out hole in the back from a failed drive experiment many moons ago!

[Om Malik had a similar experience](https://om.co/2022/04/04/04-04-2023-musings/) recently by looking at his hands:

> Sometimes, when sitting quietly, enjoying a cooling cup of perfectly crafted pour-over coffee, I find myself staring at the back of my hand. In front of my eyes lies a landscape akin to the red sand of the American Southwest that lay baking under the scorching sun after a week of rain. You can see the time crisscrossing the skin, which has been losing a battle with the vanishing collagen. What was unseen slowly becomes more visible, crack by crack—a slow creep of the wrinkles. You can run, but you can’t hide from time.

Weirdly, this doesn't make me feel melancholic like so many other reminders of mortality. I've been in a less than stellar mental place lately, but somehow these computers have been a reassuring presence... even if their eccentric behaviour drives me up the wall at times. What do you mean I've used up all my upper-memory blocks!?

It's less that I feel bad about getting older, and more feeling thankful that I've been given the opportunity to. Having the income to keep living a day or so a week in "the good ol’ days" with some great coffee sure doesn't hurt either.
