---
title: "Jeff Geerling’s all-flash NAS"
date: "2022-07-21T09:28:02+1000"
abstract: "This is what I wanted!"
year: "2022"
category: Hardware
tag:
- hard-drives
- nas
- ssds
location: Sydney
---
Back in January I dreamed about a personal NAS with flash storage, which I dubbed the [home NAS of the future](https://rubenerd.com/a-home-nas-of-the-future/):

> Last night’s dream was both cruel and unusual. I’d come into some money, and proceeded to spend a chunk of it on a home NAS that only used a stack of SSDs for bulk storage.
> 
> One day I’ll have a cute little FreeBSD hypervisor and storage NAS that I could fit in a small case. Silicon shortages notwithstanding, here’s hoping SSD prices keep dropping over time. Spinning disks have been (mostly!) my loyal servants until now, but I feel like they deserve a retirement.

Well [Jeff Geerling did it](https://www.jeffgeerling.com/blog/2022/building-fast-all-ssd-nas-on-budget)!

> Thus, the all-SSD high-performance edit NAS—on a budget.
> 
> I had five 8TB Samsung QVO SSDs from my insane $5000 Raspberry Pi server build. Until now, I had them inside my 2.5 Gbps NAS. But I wanted to build my own NAS capable of saturating a 10 Gbps connection, and allowing extremely low latency data access over the network to my two Macs, both of which are connected to my wired 10 Gbps home network.

He used a mini-ITX Supermicro board and a Noctua fan, which is exactly what I'd want to do. He also used a cute 2U case which looks great. My only reservation would be using a mix of SSD manufacturers.

The price is still far too steep for me to do, but I'm tempted to let the spinning rust in our current NAS be the last ones we buy. A small FreeBSD bhyve box with OpenZFS running on a mix of SSDs sounds like bliss.
