---
title: "Social media trains us to think in black/white"
date: "2022-06-05T09:46:34+10:00"
abstract: "Claiming one thing is important doesn't invalidate something else."
year: "2022"
category: Internet
tag:
- covid-19
- health
- logic
- social-media
location: Sydney
---
A doctor I follow on social media once commented that cleaners have saved more lives than medical professionals. Twitterers were quick to belittle the comment as being patronising to doctors, missing the fact:

* the statement was probably true
* the person making the comment was a doctor
* the statement didn't invalidate the work medical professionals do

The doctor's intent with the post was clear: she wanted to highlight the extraordinary and essential contribution that cleaners and other sanitation workers make to our society, and how their compensation doesn't reflect either of these when compared to a well-paid doctor like herself.

Had people given it a moment's thought... wait, aaaah, I see the problem.

I've read the argument that social media reduces people's capacity for critical thinking. I'm on the fence about that, but I do agree that it encourages every thought and interaction to be reduced to binary. Maybe it's because two thoughts can't fit into 280 characters. 

Claiming one thing is important doesn't invalidate something else, even if used in a comparison. This is basic critical thinking stuff, like (not) sticking your hand in a boiling kettle, or using the tip of a Swiss Army Knife as a replacement for a tiny Torx screwdriver. Ask me how I know!

