---
title: "Trent Zimmerman writes why the Coalition lost"
date: "2022-07-30T14:53:01+1000"
abstract: "A lack of climate action is true, but voting for him would have kept that government in power."
year: "2022"
category: Thoughts
tag:
- australia
- environment
- politics
location: Sydney
---
The most recent Australian federal election saw many inner-city conservatives lose their seats to an unprecedented number of independents.

Among those seeking a new job was Trent Zimmerman, our local member for North Sydney. Yesterday he [wrote an opinion piece for The Guardian](https://www.theguardian.com/commentisfree/2022/jul/29/the-coalitions-refusal-to-engage-on-climate-legislation-abandons-the-sensible-centre-of-australian-politics), in which he writes in no uncertain terms that the coalition's loss was directly due to climate inaction:

> It’s also a missed opportunity for the Coalition to indicate clearly to voters, particularly former Liberal supporters in many electorates who left us on 21 May, that it has heard their calls for more action on climate. A call that was one of the primary reasons so many seats we regarded as Liberal heartland were lost.

*(For my American readers who might be confused, the Liberal and National coalition are the more right-wing of the two major parties).*

He's right. His party will need to heed this if they want to stand a chance of returning to government. He's also correct that businesses want certainty, and that it's a bipartisan issue.

But while the article is useful for this (and either sour grapes or soul searching, depending on your perspective), I think it's interesting for another reason. It speaks to the struggle of someone trying to reconcile their ideology with the unfolding problem they probably know can't be solved by it.

Take this section where he compares current events:

> This has ironically strengthened the hand of the Greens, whose political approach and radical targets are an anathema to Liberal and Labor – the centre of Australian politics.
> 
> The extreme weather events of the European summer are yet another reminder of why Australia must be a part of global efforts to reduce emissions.

Europe has *extreme* weather, but the Greens' approach is *radical*. The connection is well within grasp, but his ideology prevents him from doing so. Whether he sees it or not, this is exactly the same problem faced by his former colleagues; the ones he now scolds for costing him his seat.

*(There's also the fact neither Liberal or Labor parties are centre. He echoes this sentiment in the headline, claiming there's a sensible centre of Australian politics. I see no evidence for this)!*

What also goes unacknowledged is that a vote for Trent (among other former members) would have kept the party he chides in power. Clearly his past performance hadn't reformed the party from within, if that was his intention. Scott Morrison retained his seat, and would have likely reminded Prime Minister. 2019 demonstrates there's no reason to believe anything would have changed.

I suppose then, for the sake of the climate, it's good Trent lost his seat. The "sensible centre" climate policy Labor proposes is woefully inadequate, but at least it's something. Let's see if the Liberal party heeds his warning.

