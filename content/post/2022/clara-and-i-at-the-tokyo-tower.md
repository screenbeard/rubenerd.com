---
title: "Clara and I at the Tokyo Tower"
date: "2022-01-30T22:44:21+11:00"
abstract: "I clearly wasn’t prepared for the photographer!"
thumb: "https://rubenerd.com/files/2022/tokyo-tower@1x.jpg"
year: "2022"
category: Travel
tag:
- japan
- tokyo
- tokyo-tower
location: Tokyo
---
We were going through more *stuff* this afternoon, and found this cute souvenir from our second-last Japan trip in 2018, when I was travelling for AsiaBSDCon.

I clearly wasn't prepared for the photographer! And who could ask for a better byline at the top there?

<p><img src="https://rubenerd.com/files/2022/tokyo-tower@1x.jpg" srcset="https://rubenerd.com/files/2022/tokyo-tower@1x.jpg 1x, https://rubenerd.com/files/2022/tokyo-tower@2x.jpg 2x" alt="The Deck News! Describe the contents of your flyer here. vol.333 TOYKO TOWER. Very Impressive Panorama!" style="width:420px; height:603px;" /></p>
