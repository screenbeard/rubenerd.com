---
title: "Eight things I don’t regret buying"
date: "2022-02-10T17:56:04+11:00"
abstract: "Takeaway coffee! ☕️"
year: "2022"
category: Travel
tag:
- coffee
- cooking
- esther-golton
- food
- music
location: Sydney
---
Someone on social media posed the question, which I thought would be fun to answer. It'll also be under *Pieces of Eight* on my [Omake page](https://rubenerd.com/omake/).

* Our air fryer
* Takeaway coffee
* A split keyboard
* Technics SL-J300R quartz lock, direct drive, linear tracking turntable
* [Esther Golton’s discography](https://esthergolton.bandcamp.com/album/stay-warm#buyFullDiscography)
* Redundant hard drives
* Pandan essential oil for our diffuser
* Plane tickets

If I had to cheat and add a ninth one, is a phrase with ten words.
