---
title: "The wondrous LG A9 CordZero vacuum cleaner"
date: "2022-06-03T08:50:47+1000"
abstract: "This is the best vacuum cleaner I’ve ever owned."
year: "2022"
category: Hardware
tag:
- cleaning
- home
location: Sydney
---
Last week Clara's and my stick-mounted Miele Swing H1 vacuum cleaner ran out of bags. We tossed up ordering some more in, or using it as an excuse to buy a replacement. My family always had big Miele machines that worked well, but this smaller unit was loud and awkward to use, with mediocre suction power. As a result, we didn't use it much, as became evident based on the amount of dust trapped in our air purifiers.

We had a Dyson V8 Pet in our last share house which worked well, so I did some research into similar stick vacuums. A few stores here have demo units, and I was most impressed with a few Samsung and LG models. Clara and I adore our new LG fridge, so we ended up with the handsome red A9.

<p><img src="https://rubenerd.com/files/2022/50076736_778093@1x.png" alt="Press photo of the LG A9 CordZero vacuum cleaner in red." style="width:92px; height:252px; float:right; margin:0 0 1em 2em;" srcset="https://rubenerd.com/files/2022/50076736_778093@1x.png 1x, https://rubenerd.com/files/2022/50076736_778093@2x.png 2x" /></p>

The A9 sits at the budget end of LG's stick vacuum range, which places it in Dyson price territory. But the build quality is noticeably better; the tolerances between parts is much tighter, with more rigidity when held at weird angles, such as cleaning an air vent. The nozzles also pivot and move smoother than any I've used before, and the red colour means it runs faster than grey units (cough).

Pulling all the parts out of the box felt a bit overwhelming, but everything fits together without looking at the instructions... much. Make sure you triple check the mountain of cardboard lest you throw away an attachment. I was happy to see it even came with spare battery pack.

Even on the *Normal* setting, the suction power feels stronger than the Dyson, and an *order of magnitude* better than the traditional Miele. *Turbo* mode lifts our rugs clean off the floor, while still being quieter. The added power means you're not going over the same spot multiple times, which adds up to save a ton of time.

The A9 comes with two motorized nozzles for hard floors and multi-surface, along with crevice, combination, mattress, and hard dirt nozzles. The first two attach to the freestanding charging dock which doesn't need to be attached to the wall, which is great for renters. It even supports charging both batteries concurrently *and*... **AND**... at the same time. My dad would be proud that I'm invoking one of his lines, I'm sure.

Even with technical features aside, I love the *convenience* of using this thing. I'm not a fan of fitted carpet on account of it hiding and trapping so much crud, but our current rental has it everywhere. This picks up stuff I can't even see, including on a patch of floor I went over with the Miele. But most importantly, I can whip it out to clean up a spill and have it back in the dock faster than you can shake a stick [vacuum cleaner] at.

My only quibbles are needing to empty the canister, though that will save us on bags in the long run. I think I also preferred the trigger on the Dyson over the LG's static power button, because I could have it spin down while walking between rooms. It could go even faster if the handle was also red (cough).

I won't pretend that an AU $800 appliance is an essential, but if you've got the savings to plonk down for it, you could buy something far more frivolous. This is the best vacuum cleaner I've ever owned and used.
