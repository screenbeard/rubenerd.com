---
title: "Facts over feelings"
date: "2022-03-01T08:21:34+11:00"
abstract: "It’s a false dichotomy, at best."
year: "2022"
category: Thoughts
tag:
- philosophy
- social-media
location: Sydney
---
Spend any time on social media, and you'll come across people using the rhetorical tactic of *facts over feelings* when dismissing someone's perspective. I'm certain I used it as a teenager too. Assuming we use the public's definition of feelings and not the clinical one, it's a false dichotomy.

I can see where people are coming from here. Facts are inalienable, provable, and set in stone. Feelings are borne of an emotional state. Saying three fours are twelve is a fact, and someone saying it *feels* like thirteen doesn't change this.

But most of the world isn't defined so neatly. When we talk about economics, or politics, or even software development, we're just as much making ethical, moral, or emotional judgements on the best approach to something. You can say one library is factually smaller than the other, but as many decisions are made by what language, or framework, or application the developer (or her manager) likes using. The preponderance of programming languages and methodologies speaks to this.

Someone's emotional state is also a fact. Ukranians have rallied around their president in light of the ongoing Russian invasion because he makes them feel empowered against the odds. *Morale* is a key and decisive fact in winning conflicts, though it's entirely based on feeling.

There's altogether too many people feeling that facts can exist in isolation. Maybe if we were Vulcans we could, and I'm sure there are stoics who'd like to see more of that. Certainly I'd like to see a more evidence-based approach to government legislation and business processes. But it doesn't change the *fact* that we're imperfect, fleshy sacks of humanity who are driven as much by our hearts as our minds. Acknowledging this fact need not blind us to reality.

The Covid pandemic is another perfect example. We all lavished deserved praise and respect on cleaners, retail staff, nurses, doctors, and medical researches, but without artists creating the shows, manga, music, and games we all enjoy, I can tell you those months of lockdown would have shattered me. Medical professionals let us live, and artists make us want to.

Invariably people wheel out *facts over feelings* to shield themselves from further argument, when really it's simple projection. Their smug bluster, or being *in it for the lulz* belies their own anxiety or fears about an issue. Maybe they feel threatened, or have had their own negative experiences. This is why, for all their pretence of cool calculation and aloof bemusement, the best way to approach people like that is with empathy. That's both a fact, and my feeling.
