---
title: "Travelling to Japan during post-Covid times"
date: "2022-11-29T09:54:44+11:00"
abstract: "Tips about how to navigate the vaccine and arrival requirements."
year: "2022"
category: Travel
tag:
- covid-19
- japan
location: Sydney
---
Japan is one of my favourite places in the world to visit, but the process has understandably changed since Covid. This is accurate as of November 2022, but also check with your airline and/or travel agency before flying.


### Vaccinations

It shouldn't come as a surprise that a minimum of three Covid vaccinations are required. You'll need to contact the health authority in your country for a record of your vaccinations. Australians can export an international vaccine cert from the Medicare site, and Singaporeans can log into HealthHub using your Singpass account. As with your hotel reservations and airline tickets, they're worth printing just in case.

At least a week before arriving in Japan, it should be considered mandatory to register with [Visit Japan Web](https://www.vjw.digital.go.jp/). This new government site lets you pre-fill information for immigration and customs, similar to the ESTA process for the US. This is where you'll upload your vaccine certificate, where it'll be manually verified. You'll be emailed when this process is done.

The Visit Japan Web site has two QR codes which you should screenshot in advance, in case you can't get network connectivity when you land. You'll be asked for them during the arrival process.

Airport staff are friendly and direct you to the right areas to queue when you arrive. If for whatever reason your profile on the Visit Japan Web site stops working for whatever reason, as happened with my sister when we arrived, you're directed to a separate queue where your health documents are verified. I've read this taking hours, but it took less than half an hour for her.

The Visit Japan Web site strongly recommends but doesn't mandate you purchase international travel insurance, but you'd be a class A baka gaijin for not doing so. No, really.


### Wandering around Japan

When you're in Japan, you'll notice *everyone* wears a mask... to the point where trendy fashion stores in Shibuya sell cute charms for them. If this sounds too onerous, don't travel to Japan.

Large shopping centres and attractions have temperature sensors above hand sanitiser bottles you can dispense with your feet which is pretty cool. The sensors aren't generally enforced, though we used them for our own interest. It reminded me of Singapore during SARS, so didn't feel unusual.

A positive development has been the introduction of more point of sale systems everywhere, which makes paying for things *much* easier. This may have been done in advance of the Olympics, but was doubled down during Covid to reduce handling of cash. If you sign up for a bank account in your home country that doesn't change foreign transaction fees, you'll get a nice itemised list of transactions at the end of each day which is easy to reconcile and budget from.

Hotels also continue to be hit and miss when it comes to bookings. Normally Clara and I book accommodation and flights through our credit card's travel agency to use points, and because it makes it much easier to cancel things. We had *two* separate hotels that these Australian agencies screwed up. One international chain charged us for two rooms but only reserved one, and the other let us book two people to a room that only permitted one person. I'd advise booking directly with the hotel.


### Conclusion

We deliberately didn't leave large cities during this most recent trip, given the country has only been fully open to foreigners for the last month. I've been told that you might still be treated differently in more rural areas. Given their isolation and demographics, I can empathise with the fear they may have for foreigners for a while yet. Let's take it one step at a time.

Other than these measures, Japan is the same amazing place to explore as it was before. The people, culture, food, atmosphere, history, and public transport are unsurpassed. It's still one of the only places I've been, and continue to go to, that *surpass* my already high expectations.

*Most* tourists seemed to be behaving well, which was a relief to see. But there was always at least a few at large attractions that either weren't in masks, or were wearing them under their noses as they talked loudly about how Chinese and Japanese "look the same", or that America has "better cheese" (I kid you not). I called out a few white boomers who sheepishly put them back on.

Be polite, quiet, friendly, and respectful, and you'll be fine. *Sumimasen* is your indispensable friend!

