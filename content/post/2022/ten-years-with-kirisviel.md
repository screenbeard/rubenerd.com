---
title: "Ten years with @kirisviel"
date: "2022-07-22T07:28:47+10:00"
abstract: "Recreating out first tea shop trip."
thumb: "https://rubenerd.com/files/2022/IMG_3674@1x.jpg"
year: "2022"
category: Thoughts
tag:
- clara
- personal
location: Sydney
---
Ten years ago I asked Clara out for a date to a Sydney tea shop. We sat there talking for hours until they closed. Last night we did the same thing :).

My relationship with Clara is the best thing to have happened to me. She even still pretends some of my jokes are funny! Every day I wake up next to her not quite believing that our paths crossed, then stayed together.

<figure><p><img src="https://rubenerd.com/files/2022/IMG_3674@1x.jpg" alt="Clara’s Prince Cat with our tea drinks!" style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2022/IMG_3674@1x.jpg 1x, https://rubenerd.com/files/2022/IMG_3674@2x.jpg 2x" /></p></figure>

We're both a bit tired from work and life right now, but hopefully we can travel together again soon. I would never have had the guts to go to Japan had it not been for her ability to speak it ^^;.

I think we'll be digging some more of our Minecraft Metro system this weekend to celebrate.
