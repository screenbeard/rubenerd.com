---
title: "Software engagement versus utility"
date: "2022-09-15T11:39:50+10:00"
abstract: "Software that was made before everyone was tracking and optimizing for engagement is better in a way that’s hard to describe."
year: "2022"
category: Internet
tag:
- dark-patterns
- design
- ethics
location: Sydney
---
I loved this observation by [Bob Poekert](https://twitter.com/bobpoekert/status/1570103373906735105)\:

> Software that was made before everyone was tracking and optimizing for engagement is better in a way that's hard to describe.
