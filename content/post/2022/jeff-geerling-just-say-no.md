---
title: "Jeff Geerling: Just say no"
date: "2022-11-30T07:12:31+11:00"
abstract: "Saying no is hard. But none of us are gods."
year: "2022"
category: Thoughts
tag:
- no
- personal
location: Sydney
---
Jeff [wrote a post yesterday about burnout](https://www.jeffgeerling.com/blog/2022/just-say-no), and the opening is a keeper:

> Saying yes is easy—at first.
> 
> It makes you feel better. And it makes you feel like you can do anything! And the person you're saying yes to also gets a happy feeling because you're going to do something for them.
> 
> Saying no is hard. It's an admission you can't do something. And worse still, you're disappointing someone else who wants you to say yes.
> 
> But here's the thing: none of us is a god. We're people. We have a certain amount of mental resources.
> 
> Some people are kind of crazy and can do a lot more than you or I can, but nobody can do it all. And sometimes you can burn the midnight candle for a little while, but you're just building up debt. Every 'Yes' is a loan you have to pay off.
