---
title: "Why IPv6 isn’t more widespread"
date: "2022-02-12T15:24:45+11:00"
abstract: "It’s more business reality, less malice or incompetence."
year: "2022"
category: Internet
tag:
- ipv6
location: Sydney
---
A Twitter discussion made me wonder why IPv6 adoption contines at a snail's pace, despite IPv4 exhaustion and surging prices. Nuintari claimed that NOCs [became addicted to NAT](https://twitter.com/nuintari/status/1491451076289216521), which is probably true, but insufficient an explanation.

Tailscale's Avery Pennarun [theorised back in 2020](https://tailscale.com/blog/two-internets-both-flakey/) that it had something to do with its design:

> IPv4 evolved as a pragmatic way to build an internet out of a bunch of networks and machines that existed already. Postel’s Law says you’d best deal with reality as it is, not as you wish it were, and so they did. When something didn’t connect, someone hacked on it until it worked. Sloppy. Fits and starts, twine and duct tape.
>
> IPv6 was created in a new environment of fear, scalability concerns, and [Second System Effect](https://en.wikipedia.org/wiki/Second-system_effect) ... its goal was to replace The Internet with a New Internet — one that wouldn’t make all the same mistakes. It would have fewer hacks. And we’d upgrade to it incrementally over a few years, just as we did when upgrading to newer versions of IP and TCP back in the old days.
>
> We can hardly blame people for believing this would work ... here we are 25 years later, and not much has changed.

Others are even less charitable, with [teknikal_domain](https://teknikaldomain.me/post/ipv6-is-a-total-nightmare/) saying IPv6's complexity makes it actively hostile. Certainly IPv4 and NAT might be the most extreme example of Richard Gabriel's [worse is better](https://en.wikipedia.org/wiki/Worse_is_better) that I've seen, especially in the enterprise.

People have been [talking about this for decades](https://cr.yp.to/djbdns/ipv6mess.html), but I've noticed an uptick in popular coverage in the last twelve months. [Rupert Goodwins](https://www.theregister.com/2022/01/24/opinion_column_ipv6/)\:

> Incompatibility equals obsolescence. IPv6 isn't quite the network Itanium [Intel's first attempt at a 64-bit CPU, which was ignored for AMD’s 64-bit extensions to x86 &ndash;ed]. There is no doubt that an IPv6-only planet would be superior, more efficient, support a bigger variety of services and have better security. The trouble is, until you get there, the opposite is true. If running an IPv4 network implies a certain amount of resources required and a certain threat landscape to manage, then adding a parallel IPv6 network means adding to those costs and liabilities. It doesn't get you much in return.

I think this gets close to explaining the root problem from the business side. This related thread [about IPv6 training](https://twitter.com/stubarea51/status/1492137183456677892) is illustrative; even among those who wish they could use it, they're limited by the constraints and operating environment of the real world.

[Darrell Root put it best](https://twitter.com/DarrellRoot/status/1491945530506629120), even if we'd prefer it not to be:

> Hard to convince overworked IT departments at enterprises to deploy.
