---
title: "Chatswood’s round trees"
date: "2022-02-26T11:52:41+11:00"
abstract: "I find it oddly comforting in our current world that these exist."
thumb: "https://rubenerd.com/files/2022/chatswoods-round-trees@1x.jpg"
year: "2022"
category: Thoughts
tag:
- nature
location: Sydney
---
I find it oddly comforting in our current world that these exist. They make me smile every time I walk past.

<p><img src="https://rubenerd.com/files/2022/chatswoods-round-trees@1x.jpg" srcset="https://rubenerd.com/files/2022/chatswoods-round-trees@1x.jpg 1x, https://rubenerd.com/files/2022/chatswoods-round-trees@2x.jpg 2x" alt="Photo of several trees in my suburb that have been pruned into round green spheres." style="width:500px; height:333px;" /></p>
