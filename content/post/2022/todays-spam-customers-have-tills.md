---
title: "Today’s spam: customers have tills?"
date: "2022-09-03T09:11:48+10:00"
abstract: "Wouldn’t you want customers ringing yours?"
year: "2022"
category: Thoughts
tag:
- pointless
- spam
location: Sydney
---
Today's bit of spam made me smile:

> get your customer's till ringing

Wouldn't you want customers ringing *your* till, not the reverse? 
