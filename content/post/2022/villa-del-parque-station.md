---
title: "Villa del Parque station in Buenos Aires"
date: "2022-02-16T09:05:26+11:00"
abstract: "I love the Tudor styling."
thumb: "https://rubenerd.com/files/2022/villa_del_parque@1x.jpg"
year: "2022"
category: Thoughts
tag:
- south-america
- trains
location: Sydney
---
It may surprise a few of you to know I have a thing for train stations. This one on the San Martín Line is lovely.

<p><img src="https://rubenerd.com/files/2022/villa_del_parque@1x.jpg" srcset="https://rubenerd.com/files/2022/villa_del_parque@1x.jpg 1x, https://rubenerd.com/files/2022/villa_del_parque@2x.jpg 2x" alt="Photot showing the single-floor station from the street." style="width:500px; height:333px;" /></p>

The Tudor styling reminds me of the old Harajuku station in Tokyo. I'm relieved Clara and I were able to explore it before they had to knock it down.

Thanks to [JonySniuk](https://commons.wikimedia.org/wiki/File:Lluvia_y_estaci%C3%B3n_en_Villa_del_Parque.jpg) for posting this on Wikimedia Commons.
