---
title: "Booting Fedora into XWayland when Xorg breaks"
date: "2022-07-02T09:06:48+10:00"
abstract: "Edit /etc/gdm/custom.conf."
year: "2022"
category: Software
tag:
- linux
- nvidia
- troubleshooting
location: Sydney
---
I dual-boot my FreeBSD desktop with Fedora Workstation 36 to play Steam games, and to keep my skills up among the Penguins. Unfortunately, I'd started having graphics issues with the Nvidia drivers on XWayland. I had no problems with Xorg in Fedora 35, so I thought I'd try logging out and choosing Xorg from the login options instead. This only returned a blank screen.

This was an issue, because I have Fedora set to log in automatically on boot. This only has games on it, and I'm satisfied the password I type for whole drive encryption is sufficiently secure. But this meant rebooting automatically logged me back in with Xorg, with the same blank screen.

Fortunately I had SSH enabled on the box, so I logged in remotely from my Mac and edited the following file:

	$ sudoedit /etc/gdm/custom.conf

And changed the following line to False:

	AutomaticLoginEnable=False

Now I could reboot and choose XWayland from the login screen instead.

If you don't have SSH available, you should be able to boot a Fedora live image, mount your drive, and change the file from there.
