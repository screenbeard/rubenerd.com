---
title: "Shoko Miyata’s 2022 qualifier floor routine"
date: "2022-11-05T07:03:09+11:00"
abstract: "This was so much fun! And much needed."
thumb: "https://rubenerd.com/files/2022/shoko-miyata-floor@1x.jpg"
year: "2022"
category: Thoughts
tag:
- gymnastics
- sport
location: Sydney
---
[Liviefromparis](https://www.youtube.com/channel/UCVW0my2T-X9pgnNXXGm6HCg/videos) has been doing a great job uploading footage from this year's World Gymnastics Championships, which have been a most welcome respite from current family issues.

Skoko Miyata's [uneven bar](https://www.youtube.com/watch?v=P-cQdgO_jSM) and [beam routines](https://www.youtube.com/watch?v=JcfYBg-fIJs) showed a lot of promise, but her [floor routine](https://www.youtube.com/watch?v=aph9Guy84yo) was *so much fun!* There's something about those who can pull off all some of the most complicated choreography of any sport imaginable, *and* do it with personality and whimsy. 

<figure><p><img src="https://rubenerd.com/files/2022/shoko-miyata-floor@1x.jpg" alt="Recording by Livie from Paris of Shoko Miyata’s floor routine" style="width:500px;" srcset="https://rubenerd.com/files/2022/shoko-miyata-floor@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2022/shoko-miyata-floor-end@1x.jpg" alt="Shoko Miyata finishing her routine" style="width:500px;" srcset="https://rubenerd.com/files/2022/shoko-miyata-floor-end@2x.jpg 2x" /></p></figure>

As my mum always said, you can’t help but smile back when a professional at the top of their game does. Thank you!
