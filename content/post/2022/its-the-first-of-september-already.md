---
title: "It’s the first of September already"
date: "2022-09-01T10:45:38+10:00"
abstract: "A short list of historically important (cough) events that happened on this day."
year: "2022"
category: Thoughts
tag:
- dates
- pointless
location: Sydney
---
To afford ourselves the use of an adverb, that's patently ridiculous.

Here are some historical events that happened on this day:

* I had a [cup of coffee](https://www.apothecarycoffee.com.au/)
* I performed an out-of-cycle [OpenZFS scrub](https://docs.freebsd.org/en/books/handbook/zfs/#zfs-quickstart-data-verification)
* Altare of Holostars Tempus [played Minecraft](https://www.youtube.com/channel/UCyxtGMdWlURZ30WSnEjDOQw)
