---
title: "Ben Sidran, Ballad of a Thin Man"
date: "2022-11-14T08:43:43+11:00"
abstract: "Ben’s vocal style and delivery are perfect for this Bob Dylan song. “Is this where it is!?”"
thumb: "https://rubenerd.com/files/2022/yt-7rKyrJPrpdQ@1x.jpg"
year: "2022"
category: Media
tag:
- ben-sidran
- jazz
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) is a celebration of Bob Dylan, and of Ben Sidran's *brilliant* 2009 jazz cover album.

I don't think I've mentioned it here before, but Ben's vocal style and delivery are so perfect for these songs. The way he acted out that famous scene in the second verse was brilliant:

> You raise up your head, and you ask: "Is this where it is?" And somebody points to you and says "It's his!" And you say: "What's mine?" And somebody else says: "Well, what is?" And you say: "Oh my God! Am I here all alone!?"

<p><a target=_BLANK href="https://www.youtube.com/watch?v=7rKyrJPrpdQ" title="Play Ben Sidran - Ballad of a Thin Man"><img src="https://rubenerd.com/files/2022/yt-7rKyrJPrpdQ@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-7rKyrJPrpdQ@1x.jpg 1x, https://rubenerd.com/files/2022/yt-7rKyrJPrpdQ@2x.jpg 2x" alt="Play Ben Sidran - Ballad of a Thin Man" style="width:500px;height:281px;" /></a></p>


