---
title: "Helping Ukranians via Etsy"
date: "2022-03-02T08:34:10+11:00"
abstract: "Digital files don’t need any shipping. Brilliant idea."
thumb: "https://rubenerd.com/files/2022/ukrainesupport@1x.jpg"
year: "2022"
category: Thoughts
tag:
- ukraine
location: Sydney
---
I've seen an infographic floating around with a suggestion on how you can help people. I've converted into text:

> 1. Go to Etsy.com
> 2. Type *Digital File* in the search box
> 3. Choose the button *All Filters*
> 4. Scroll down and find the *Shop Location* category
> 5. Choose *Custom* and type `Ukraine`
>
> The result: in front of you are all the digital products of Ukrainian sellers. There are patterns, SVG files, and more. All of these products are automatically delivered to your e-mail, the seller does not have to post anything.

Slight correction there: the assets are available to download

Brilliant idea! Here's a low-res version of a [photo](https://www.etsy.com/listing/1186549913/i-stand-with-ukraine-digital-jpg-file) I just bought from Pavel and Anna in Cherkasy:

<p><img src="https://rubenerd.com/files/2022/ukrainesupport@1x.jpg" srcset="https://rubenerd.com/files/2022/ukrainesupport@1x.jpg 1x, https://rubenerd.com/files/2022/ukrainesupport@2x.jpg 2x" alt="I Stand With Urakine" style="width:320px; height:213px;" /></p>

*Update:* People are also [booking Airbnbs lodgings without staying](https://www.theguardian.com/technology/2022/mar/03/ukraine-airbnbs-booked-in-effort-to-get-money-to-residents-pay-not-stay). Another great idea.
