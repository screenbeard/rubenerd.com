---
title: "Feedback on 4K KVMs"
date: "2022-03-10T08:40:26+11:00"
abstract: "Short answer: no!"
year: "2022"
category: Hardware
tag:
- feedback
- kvms
location: Sydney
---
On Monday I asked if DisplayPort or HDMI KVMs [were any good](https://rubenerd.com/are-displayport-or-hdmi-kvms-any-good/).

[Wesley Moore](https://twitter.com/wezm/status/1501335051446865923)\:

> My partner got a StarTech one to switch between work and personal macs with a 4K Dell display… it did not go well. Display switching frequently needed multiple attempts and she ended up going back to two displays on her desk.

Rebecca Hales, via email:

> They are ok if you always remember to wake your computers from sleep before using the KVM to switch them, and kick it regularly.

And Dmitry A:

> No! No! No! Don't!

Thanks everyone 👍. I'll rethink this desk setup instead.
