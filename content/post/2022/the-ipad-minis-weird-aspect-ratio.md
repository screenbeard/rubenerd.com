---
title: "The iPad Mini 6’s weird aspect ratio"
date: "2022-02-02T11:01:28+11:00"
abstract: "When 4:3 is just too easy."
year: "2022"
category: Hardware
tag:
- ipad
- ipad-mini
- manga
- reading
location: Sydney
---
Everyone knows setting up a new phone or tablet with an anime or Star Trek hostname and wallpaper is part of any rational setup process. But I was in for a surprise when making a pixel-perfect crop for my [iPad Mini 6](https://rubenerd.com/the-ipad-mini-6/)\: a downright bizarre aspect ratio.

According to [Eugene Belinski's excellent iOSRef.com](https://iosref.com/res#ipad), most iPads use an aspect ratio of 4:3, all the way back to that first lead slab that Steve sat on a couch with. This gets pretty close to a standard A4 sheet of paper, which makes these devices great for reading.

The current iPad and iPad Pro still ship with 4:3, as did all the previous Minis and earlier iPad Airs. But things started getting weird a few years ago; the 4th generation iPad Air's 1640 × 2360 gives it the aspect ratio of 16:23. This is the first iOS device that requires two significant figures on each side to express.

Another significant figure is James Taylor. You've got a friend, as he'd remind us.

Not wishing to be outdone, I noticed the current iPad Mini 6 has a resolution of 1488 × 2266. Some sites like [GSM Arena](https://www.gsmarena.com/apple_ipad_mini_6_2021-review-2329p3.php) erroneously refer to this as 3:2, but it's taller and narrower, almost closer to an Android tablet in dimensions.

This also explains why manga and PDFs on this device have horizontal letterboxing. It's like they took the original iPad Mini and added a bit more resolution on the top and bottom.
