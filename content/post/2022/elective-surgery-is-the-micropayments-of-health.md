---
title: "Elective Surgery is the “micropayments” of health"
date: "2022-12-07T09:11:45+11:00"
abstract: "There’s the strict medical definition, but the general public reads it very differently."
year: "2022"
category: Thoughts
tag:
- health
- language
location: Sydney
---
While [we're on the subject](https://rubenerd.com/dave-winer-on-micropayments/ "Dave Winer on micropayments") of phrases and words that make me bristle, I think it's time journalists retired the use of the phrase *elective surgery*, and call out politicians to explain what they mean by it.

There's the strict medical definition, but the general public reads it very differently. Elective sounds like *voluntary*, which leads people to think it's for vanity, or worse, surgery someone could do without. It permeates discussions on everything from healthcare to politics.

It's bunk. Surgery that isn't immediately life-threatening still can have a huge impact on quality of life. People also don't (generally) get surgery for the sake of it, which deflates any idea of it being optional.

I've been lucky to only need two elective surgeries in my life thus far, once in my twenties, and another last year in my thirties. If you haven't, you probably know someone who has. Frankly, I resent the idea that anyone in that situation is doing it for kicks; worse if it's being used as evidence for why universal healthcare for the unwashed masses in places like the US would be a waste of money.

People hurt and need help. Let's start from compassion instead.
