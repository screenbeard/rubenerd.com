---
title: "The patch in my Commdoore 1541 was official!"
date: "2022-12-10T09:09:19+11:00"
abstract: "I found a diagram in the official service manual that matches the patch that was applied to my disk drive."
thumb: "https://rubenerd.com/files/2021/commodore-1541-board@1x.jpg"
year: "2022"
category: Hardware
tag:
- commodore
- commodore-1541
- hardware
- troubleshooting
location: Sydney
---
Last year I talked about [fixing my second-hand Commodore 1541 floppy disk drive](https://rubenerd.com/troubleshooting-a-commodore-1541-disk-drive/). It turns on, is detected by whichever Commodore computer I plug it into, and all the mechanical components have been cleaned and lightly lubricated. But the stepping motor that controls the movement of the read/write head doesn't move.

One thing I noted at the time:

> A couple of traces had been bypassed with patches on both sides; I’m not sure if this was done at the factory or someone else tried to fix this drive one before.

Here's a photo showing what I meant. Note the long patch in the lower left, near the two large capacitors, and the smaller patch below it:

<p><img src="https://rubenerd.com/files/2021/commodore-1541-board@1x.jpg" srcset="https://rubenerd.com/files/2021/commodore-1541-board@1x.jpg 1x, https://rubenerd.com/files/2021/commodore-1541-board@2x.jpg 2x" alt="Photo of the motherboard of the 1541, showing the two patches" style="width:500px" /></p>

I've since found another of [Commodore's 1540/1541 service manuals](https://rubenerd.com/files/docs/Commodore1540-1541ServiceManual.pdf), and page 29 spells out the exact upgrade:

> PCB Assembly #1540048 (Schematic 1540049) requires an upgrade to the reset circuit. The upgrade consists of:   
>    
> **1) COMPONENT CHANGES:**   
> R25 Change To 6.8K ohm, 1/4 W, +/-5%   
> R57 Add 220 ohm, 1/4 W, +/-5%   
> R58 Add 2K ohm, 1/4 W, +/-5%   
>   
> **2) CUT circuit trace near UA1** (See diagram)
>   
> **3) INSTALL JUMPER WIRES:**   
> BETWEEN: UA1 pin 8 and UB1 pin 5 UA1 pin 9 and CR4 Anode   
> UB1 pin 6 and UD3 pin 5 on BACK of board   
> 
> **4) CUT circuit trace between CR7 Anode and UD3 pin 5 on BACK of board.**

Here's the diagram from step 2, showing the exact fix:

<figure><p><img src="https://rubenerd.com/files/2022/stepping-motor-c1541@1x.png" alt="Diagram showing patch R57 with a resisto, a cut trace, and another patch R58. These are referenced above, and in the manual" style="width:477px; height:281px;" srcset="https://rubenerd.com/files/2022/stepping-motor-c1541@2x.png 2x" /></p></figure> 

So whatever is wrong with the stepper motor on this drive, this is now less likely to be the problem. Commodore.ca has a copy of a [Commodore service cheat sheet](https://www.commodore.ca/commodore-manuals/commodore-1541-floppy-drive-hardware-failure-cheat-sheet/) that suggests the issue may be with the 325572 or the 6522.

Replacements for these are much cheaper than I thought; and Gamedude in Australia even sells a [modern replacement for the 325572](https://www.ebay.com.au/itm/183384910363). I'm tempted to try swapping these out and see if it improves things, given a scope isn't in the budget right now.
