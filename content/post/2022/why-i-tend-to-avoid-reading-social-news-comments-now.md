---
title: "Why I tend to avoid reading social news comments"
date: "2022-07-10T10:47:54+10:00"
abstract: "Grow up! But not too much."
year: "2022"
category: Internet
tag:
- hacker-news
- reddit
location: Sydney
---
I was told my [latest Walkman post](https://rubenerd.com/my-new-sony-nw-a55-walkman/) made it to Hacker News again, which tracks again with a spike in traffic! I avoided reading it, along with other sites like Reddit.

Most comments on the site are insightful, useful, entertaining, and constructive in their criticism. People might not be friendly, but they're at least cordial and assume good intentions. In other words, I can respect their opinions even if we may disagree.

But there's a growing number that aren't, and I have little motivation to separate the wheat from the proverbial chaff anymore. They're deliberately obtuse, make comments in bad faith, assume things about the author that are hilariously and transparently false, conflate subjective opinions with fact, or are pedantic in a way that isn't constructive. Among the minority who read the article, most demonstrate an alarming lack of basic comprehension skills; wilfully or otherwise. The rest are just dicks.

Maybe the web, BBSs, forums, mailing lists, and chat rooms were always like this. Maybe it's just me getting older and [running out of fucks to give](https://alittlecreative.net/zero-fucks/ "Little Creative").

But I think I've also figured out a big part of the reason why people are so prickly, regardless of topic. People assume that any review or discussion must target them or their interests, preferences, requirements, or circumstances; therefore anything else is an affront. It's not sufficient for someone to like, use, or recommend something else, they must be *wrong!*

You can spend your time writing point-by-point rebuttals, but life's too short. Instead, I propose such commentators grow up... but [not too much](https://rubenerd.com/why-arent-you-more-serious/).
