---
title: "Pop Up Parade’s Rei Ayanami"
date: "2022-03-26T09:38:46+11:00"
abstract: "The detail obviously isn’t there compared to more premium and expensive figs, but she still looks amazing."
thumb: "https://rubenerd.com/files/2022/pop-up-parade-rei@1x.jpg"
year: "2022"
category: Anime
tag:
- anime-figs
- evangelion
- rei-ayanami
location: Sydney
---
Given my breathless excitement over [Wave Dreamtech's Ayanami Rei](https://rubenerd.com/dreamtechs-ayanami-rei-fig/) last year, I was also surprised and happy to see her getting the Pop Up Parade treatment. This is Good Smile Company's "affordable" line of figures designed to bring the hobby into the realm of mere mortals, not just those who can afford hundreds of dollars for statues of their favourite characters.

<p><img src="https://rubenerd.com/files/2022/pop-up-parade-rei@1x.jpg" srcset="https://rubenerd.com/files/2022/pop-up-parade-rei@1x.jpg 1x, https://rubenerd.com/files/2022/pop-up-parade-rei@2x.jpg 2x" alt="Press photos of the figure, showing Rei standing with her long hair and holding one elbow with her other hand." style="width:500px; height:400px;" /></p>

Her *Rebuild of Evangelion* hair has to be her most notable feature. You can tell the level of detail isn't at the level of those gold-pressed latinum versions from premium fig manufacturers, but the colouring, shading and styling are still there. Her reserved pose looks natural and in keeping with her character, and her chest passes my sister's "uncomfortable water balloon" test. The details of her white plugsuit are also all there, though we'd have to see how well that carries through to the final release (you can't always trust initial press photos).

If I had one minor quibble, it'd be that I think her face is a bit flat. Head on it she looks fine, but from the side it's a bit more obvious. I've noticed this trend on anime figs of late, especially for more *moe moe* characters. Compared to her Wave Dreamtech rendition, she doesn't look quite as mature.

She's up for preorder now in the usual places. My precious few sagging shelves will be abstaining, but I'm happy that fans of the franchise now have a more affordable version.
