---
title: "Goodbye Chris Seaton ♡"
date: "2022-12-09T09:00:15+11:00"
abstract: "A giant of the Ruby community, and a lovely person."
year: "2022"
category: Software
tag:
- goodbye
- ruby
location: Sydney
---
Ruby programming language giant and lovely person Chris Seaton passed away last night. Aaron Patterson has a [beautiful post](https://tenderlovemaking.com/2022/12/07/in-memory-of-a-giant.html) remembering his friend and colleague:

> As a college dropout, I’ve always felt underqualified. Embarrassment about my lack of knowledge and credentials has driven me to study hard on my own time. But Chris never once made me feel out of place. Any time I had questions, without judgement, he would take the time to explain things to me.
> 
> I’ve always looked up to Chris. I was at a bar in London with a few coworkers. We started talking about age, and I found out that Chris was much younger than me. I said “You’re so smart and accomplished! How can I possibly catch up to you?” Chris said “Don’t worry, I’ll just tell you everything I know!”

His social media posts showed his struggles with mental health of late. It's heartbreaking that we couldn't save him.

Here's a video he did on the [history of compiling Ruby](https://www.youtube.com/watch?v=Zg-1_7ed0hE). I'll miss his wit and insight.
