---
title: "Happy birthday @dekopatchi 🎂"
date: "2022-03-01T22:27:04+11:00"
abstract: "Ameh! ♡"
year: "2022"
category: Thoughts
tag:
- friends
location: Sydney
---
Sorry Baelz, a more important one beckons! 🧀

I'm still on a self-imposed social media break, but wanted to wish my dear friend *Amehhhhh~~~* a happy birthday. By putting that here, I'm sneakily @mentioning her in the title while still keeping Twitter at arm's length.

I told Seb, another friend of Clara's and mine, that I had a dream that a bunch of us had rented a giant serviced apartment in Osaka and were exploring Japan for a month. Amy had specifically talked me out of buying a CRT oscilloscope from Nipponbashi, only to blow it all on V6 and BanG Dream fans. Every part of this sounds like it'd be so much fun iRL.

At the risk of sounding dreadfully sappy, is a phrase with seven words! *WAH!* Happy birthday, and thank you for still being our friend despite my reclusiveness of late. Dinner and coffee this week? Not *on* me, as in... damn it. I'd absolutely [wear this spectacular item](https://twitter.com/ohnaganoes/status/1497422694580506626) if I weren't already so clumsy!
