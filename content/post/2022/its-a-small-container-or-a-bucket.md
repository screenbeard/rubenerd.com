---
title: "It’s a small container, or a bucket"
date: "2022-06-05T09:04:59+1000"
abstract: "We need to recalibrate incentives, or we’ll keep wasting the planet away."
year: "2022"
category: Thoughts
tag:
- economics
- food
location: Sydney
---
Here's a Sunday mental exercise. Why can't we ever buy a tub of Greek yoghurt in the right size? The options are either tiny tubs that last a few mouthfuls, or buckets so massive we'll only ever eat half before it expires.

Simple economics doesn't see a problem here. The bucket is five times the size, but *only* double the price! Who cares if you end up wasting half of it, you've saved money in absolute terms, and per gram.

Well, Clara and I care. I think it's wasteful to buy food I'm not going to eat. I feels like I'm living in a *Bizarro World* needing to spell that out, yet here we are.

I've noticed a widening and accelerating trend towards smaller and larger food for a decade now. You either buy [shrinkflated](https://en.wikipedia.org/wiki/Shrinkflation) "fun sized" packets, or ultra super duper economy. That works for non-perishables like paper towels and canned soup, but I'm not going to buy a four litre value pack of stock, or a kilo of nutmeg because it makes economic sense.

Businesses don't want to sell medium tubs of yoghurt. They make higher profits on tiny containers, and they push people to buy more with larger tubs. It's why bulk discounts exist, even if the bulk of it festers in landfill.

Last year I [confused a barista](https://rubenerd.com/we-dont-always-need-to-extract-maximum-value/) by only redeeming a small coffee. It didn't matter that I'd never drink a large, we're programmed to think we should extract the maximum value out of something. You don't need to extrapolate this much further before you realise even "human resources" are treated this way.

We need to recalibrate the incentives here, or we'll continue to waste the planet away for no (good) reason. It stinks; just as much as this stale yoghurt I now need to throw away because they were out of the tiny tubs.
