---
title: "My podcast used as a metadata example!?"
date: "2022-02-28T18:42:23+11:00"
abstract: "... “Example: a cool podcast from The Internet Archive (.mp3)”"
year: "2022"
category: Media
tag:
- metadata
- podcast
- show
location: Sydney
---
I'm a bit of a stickler when it comes to metadata, so I was surprised to see an older episode of my silly podcast [used as an example](https://freedom.press/training/everything-you-wanted-know-about-media-metadata-were-afraid-ask/#example-a-cool-podcast-from-the-internet-archive-mp3) of MP3 metadata by the Freedom of the Press Foundation:

> Example: a cool podcast from The Internet Archive (.mp3)

That's probably the coolest (only?) thing it's ever done for someone.
