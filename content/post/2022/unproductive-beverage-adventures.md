---
title: "Unproductive beverage adventures"
date: "2022-08-20T20:36:02+10:00"
abstract: "Pouring unboiled water onto tea leaves, unground coffee into an Aeropress, and other preoccupied pointlessness."
year: "2022"
category: Thoughts
tag:
- coffee
- tea
- pointless
location: Sydney
---
This last week I've:

* Poured unboiled water into a mug with tea
* Fit a mug onto a saucer, somehow, for some reason
* Poured unground coffee beans into an Aeropress
* Poured pre-ground coffee into a coffee grinder
* Written a silly list of silliness

Anyone would think I'm preoccupied! At least, preoccupied with something that isn't making beverages. Or making beverages that aren't tea or coffee. [Sorry Andy Lau](https://www.discogs.com/release/13775765-%E5%8A%89%E5%BE%B7%E8%8F%AF-Coffee-Or-Tea "Andy Lau's 2004 album Coffee or Tea")!

