---
title: "Groucho Marx’s literary review"
date: "2022-03-16T09:47:14+11:00"
abstract: "I’m so happy this is real."
year: "2022"
category: Media
tag:
- quotes
- pointless
location: Sydney
---
I'm so relieved and happy that [this review was real](https://quoteinvestigator.com/2015/01/27/fun-book/)\:

> "From the moment I picked your book up until I laid it down I was convulsed with laughter. Some day I intend reading it."
