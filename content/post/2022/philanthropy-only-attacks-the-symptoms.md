---
title: "Philanthropy only attacks the symptoms"
date: "2022-07-16T14:05:57+1000"
abstract: "You shouldn’t be able to spend your way out of culpability."
year: "2022"
category: Thoughts
tag:
- business
- ethics
location: Sydney
---
Years ago I read of a philanthropic foundation that was providing computers to children in underdeveloped parts of the world, to assist with their education and future employment prospects. It sounded laudable, but I soon learned that a chunk of the money was going towards licencing a commercial operating system for each device.

I fell out of my metaphorical chair! Why would they do that? Open source operating systems existed that would have run better and more affordably on that low end hardware. It sounded like a nonsensical waste of money... until I remembered the background and motivations of the person in charge.

This was my first inkling that philanthropy wasn't always as altruistic as one might expect. And as I've thought about this more, it doesn't just have the potential to solve a symptom rather than a cause, it can hamper meaningful action on the core issues themselves.

None of this is new. [Then & Now did an excellent video](https://www.youtube.com/watch?v=svHCXvQeZfY) explaining the history of philanthropy, from YouTubers to the American Robber Barons who founded libraries while crushing workers' rights and safety. I don't know who Mr Beast is, but taking money from large businesses to show their generosity has always seemed... off to me; especially when their behaviour is responsible for so many of the problems in the first place.

It's a free plaster from the guy who bashed your head.

Cynics point out that philanthropy is just public relations, but there are people who do believe they're helping the world. My concern is that even if that's true, they'd do so much more by directing their funding and action towards systemic change. They're precisely the people with the platforms and power to push such a project, and no I didn't just say that for the perfectly presented piece of alliteration.

On the other side, we'd be better served by billionaires behaving ethically in the first place, rather than salvaging their legacy after they've run monopolistic companies, predatory governments, or have lobbied for changes that have made the lives of people worse.

I'd love to see more scrutiny directed as these foundations and philanthropic projects. Donations are great, but it shouldn't be a way to spend your way out of culpability.
