---
title: "Hobbies becoming work, and getting space"
date: "2022-05-31T10:08:04+10:00"
abstract: "As soon as something is commodified, we can view it in more transactional terms"
year: "2022"
category: Thoughts
tag:
- hobbies
- work
location: Sydney
---
Remember the adage that if you work at what you love, you never work a day again? Or those pseudo-science Venn diagrams that say you achieve life purpose if your work is what you love, you're good at it, it pays well, and is meaningful?

These are all important, but as I also used to say, no love of computers will make you enjoy being woken up to fix one. Unless that's your kick, which I can respect if not understand!

[Victoria Pearson nails it](https://www.theguardian.com/lifeandstyle/2022/may/30/i-havent-eaten-jam-since-how-the-side-hustle-ate-the-hobby)\:

> Perhaps it is my age (31, millennial), or the ever-rising cost of living, or the omnipresent role of social media in my life, but the last decade has often felt like an unending pursuit to monetise my every waking hour.

You see this in *hustle culture* everywhere, and it's not be all that it's cracked up to be. She quotes:

> “As soon as something is commodified, we can view it in more transactional terms,” says Australian Psychological Society president Tamara Cavenett. That can “erode the strong and positive emotional attachment we had previously”.
>
> “It really changes the ‘why’ that underlies the hobby, and can mean it now incorporates deadlines, production and need to service customer wishes, rather than creativity or fun.”

A large part of my hobbies and work life will continue to overlap, but I think it's also useful to establish distance sometimes. Hobbies shouldn't cause burnout.
