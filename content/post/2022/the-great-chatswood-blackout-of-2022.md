---
title: "The great Chatswood blackout of 2022"
date: "2022-02-21T13:54:44+11:00"
abstract: "Traffic lights and associated fun."
year: "2022"
category: Thoughts
tag:
- australia
- chatswood
- sydney
- utilities
location: Sydney
---
[Power is out](https://twitter.com/Ausgrid/status/1495567693533044738) to most of my suburb today. Chatswood is one of Sydney's northern commercial hubs, so navigating the busy streets without traffic lights as a pedestrian got the blood pumping.

*(Still not as fun as crossing that highway in KL during one of their regular outages back in the mid-2000s. The sounds of that metal crunching behind me still haunt my dreams. Whoa, that went dark)!*

It does serve as a reminder of:

* how much we take wires of always-available energy for granted

* how rapidly things have problems when it's taken away, and

* *bullet points!*

It's interesting that it's only affecting specific buildings on the same street. My hunch is it's a single power feed (or a phase?).

*Update: AusGrid [says it's back](https://twitter.com/Ausgrid/status/1495598279844896770), but the traffic lights at our local intersection are still out.*
