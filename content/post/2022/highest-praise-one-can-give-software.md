---
title: "Highest praise one can give software"
date: "2022-02-17T10:51:51+11:00"
abstract: "Anjou Stiles nailed it: get out of our way and do the thing."
year: "2022"
category: Software
tag:
- design
location: Sydney
---
[Anjou Stiles nailed it](https://twitter.com/anjoustiles/status/1494050445056172034):

> "Relief that your software just got out of my way and did the thing" is the highest possible aspiration.

I initially read that as *respiration*. I suppose I'd breathe easier knowing the software I used worked.
