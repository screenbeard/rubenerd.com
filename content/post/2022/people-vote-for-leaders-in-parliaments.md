---
title: "People vote for leaders in parliaments"
date: "2022-06-17T08:17:02+1000"
abstract: "Political pedantry around this isn't even correct any more."
year: "2022"
category: Thoughts
tag:
- australia
- politics
location: Sydney
---
I've written about this before, but I hope the recent Australian elections have demonstrated that people do, in fact, vote for the leader of the country in elections.

Predictable political pedants of parliamentary process and procedure point out the public only place their preferences for local MPs, and parties then nominate leaders. This is true, as much as saying a [pie to the face](https://www.harryscafedewheels.com.au/ "Harry's Cafe de Wheels") is probably preferable to a [hammer](https://www.youtube.com/watch?v=mJag19WoAe0 "Maxwell's Silver Hammer").

People vote for MPs who are (often) a part of a party. You won't vote for someone you otherwise like if you distrust their party leadership. It's *exactly* what happened with the current crossbench of independents.

I see where these commentators are coming from; global press treating a parliamentary election like a presidential campaign ignores a lot of important context. But claiming people "don't vote for leaders" conflates the electoral mechanism with voter intent, which contributes nothing to discussions.
