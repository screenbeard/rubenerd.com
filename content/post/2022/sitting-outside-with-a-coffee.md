---
title: "Sitting outside with a coffee"
date: "2022-09-21T15:16:23+10:00"
abstract: "A photo from the coffee shop I worked at this morning."
thumb: "https://rubenerd.com/files/2022/chats-library@1x.jpg"
year: "2022"
category: Thoughts
tag:
- chatswood
- coffee
- coffee-shops
location: Sydney
---
This past week has been awful for a bevy of personal and family reasons! So I decided to leave the house and spend my mid-morning working from a coffee shop, this time from one of their outdoor tables. Great decision.

<figure><p><img src="https://rubenerd.com/files/2022/chats-library@1x.jpg" alt="View outside the Chatswood Library from the coffee shop, with an overcast sky" style="width:500px; height:375px;" srcset="https://rubenerd.com/files/2022/chats-library@1x.jpg 1x, https://rubenerd.com/files/2022/chats-library@2x.jpg 2x" /></p></figure>
