---
title: "Fire preparedness, and the SCDF emergency handbook"
date: "2022-02-14T08:25:41+11:00"
abstract: "I’ve had versions of this PDF on every laptop, smartphone, and tablet I’ve owned, it’s that good."
year: "2022"
category: Thoughts
tag:
- safety
- singapore
location: Sydney
---
For all the damage and risk to life, at least something good has come from the Telok Blangah Rise fire in Singapore earlier this month. I'm seeing more discussion of fire safety again, which is a topic that's often difficult to get people engaged with.

Channel NewsAsia Davina Tham wrote one of the better articles I've seen discussing what you can do to [prepare your apartment for fire](https://www.channelnewsasia.com/singapore/home-fire-prevention-tips-extinguisher-what-do-2490176), including not leaving cooking unattended, not keeping flammable liquds near heat sources, and only using well-tested electrical appliances. We should all have fire alarms, dry chemical fire extinguishes, clear exit pathways, and do our own regular fire drills.

From my industrial fire training, the only thing I'd add to this advice is to **only use fire extinguishers to clear pathways**, or to help rescue others if you're confident of your own escape. They're not magic bullets, and burning possessions aren't worth risking your life if its engulfing a room.

The [Singapore Civil Defence Force's Emergency Handbook](https://www.scdf.gov.sg/home/community-volunteers/publications/emergency-handbook) has a ton of useful stuff, including preventing and handling fires. I've literally had versions of this PDF on every smartphone and tablet I've ever owned, even when I've lived elsewhere.

Like a bushfire in a detached home, apartment fires can just as easily spread from elsewhere, so it's important not to be complacent even if you've personally done the right thing.
