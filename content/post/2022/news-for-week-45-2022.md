---
title: "News for week 45, 2022"
date: "2022-11-13T08:25:08+11:00"
abstract: "Ukraine, NetBSD 9.3, Neko, the new AMD EPYCs, Urusei Yatsura 2022, a lopsided advice column, and Bruce Schneier has a new book."
thumb: "https://rubenerd.com/files/2022/urusei-yatsura@2x.jpg"
year: "2022"
category: Thoughts
tag:
- bsd
- bruce-schneier
- linkblog
- netbsd
- news
- ukraine
- urusei-yatsura
location: Sydney
---
Some things I've read this week:

* Reading the news of liberated [Kherson](https://www.dw.com/en/ukraine-celebrates-as-its-forces-enter-key-city-of-kherson/video-63735337) and [Mylove](https://www.theguardian.com/world/2022/nov/12/they-ran-away-like-goats-villagers-celebrate-liberation-in-kherson-region) brought tears to my eyes. Foreign Minister Dmytro Kuleba hailed it as a [joint victory](https://www.dw.com/en/ukraine-updates-kherson-liberation-a-victory-for-peace/a-63736368); we're humbled, but this victory belongs to Ukraine. A reminder that you can support their efforts by donating to the [United24](https://u24.gov.ua/) cause. 🌻

* I'm finally getting around to trying [NetBSD 9.3](http://netbsd.org/releases/formal-9/NetBSD-9.3.html), which came out in August. It comes with a bunch of quality of life fixes, including several for [sh(1)](http://netbsd.org/releases/formal-9/NetBSD-9.3.html) that I've worked around for years. One day I'll get out of my shell and try submitting fixes myself.

* neozeed has [revived Neko again](https://virtuallyfun.com/2022/11/11/missing-neko98/). I can't believe I didn't have this little animated friend in my list of essential Windows features on my [Omake](https://rubenerd.com/omake/) page.

* ServeTheHome wrote a [great, detailed post](https://www.servethehome.com/amd-epyc-genoa-gaps-intel-xeon-in-stunning-fashion/) about the new AMC EPYC server chips, and [TechSpot](https://www.techspot.com/news/96633-amd-announces-epyc-9004-up-96-cores-around.html) has a good set of tables for a quick summary. I haven't had a chance to look at in any detail, but if their density claims stack up (geddit?), this could be amazing. We often joke at work that we run out of power before rack space, but fewer, higher density machines would make a ton of stuff easier and cheaper.

<img class="side-image" src="https://rubenerd.com/files/2022/urusei-yatsura@1x.jpg" alt="Poster for Urusei Yatsura's 2022 release" style="width:212px; height:300px; float:right; margin:0 0 1em 2em" srcset="https://rubenerd.com/files/2022/urusei-yatsura@1x.jpg 1x, https://rubenerd.com/files/2022/urusei-yatsura@2x.jpg 2x" />

* I can't wait to watch the [2022 adaptation of *Urusei Yatsura*](https://myanimelist.net/anime/50710/Urusei_Yatsura_2022) which has just started airing! The series originally ran in 1981, and it became a cult classic. It was the first series I ever saw by [Rumiko Takahashi](https://twitter.com/rumicworld1010), and her work has since had a huge impact on my life. *Inuyasha* and *Rin-ne* were among the first things Clara and I bonded over when we met, and it was the [first anime-themed cafe](https://rubenerd.com/coffee-at-the-urusei-yatsura-pop-up-cafe) we ever went to together in Japan. She's a national treasure.

* This lopsided pop psychology by an ABC Australia contributor about [new step-parents](https://www.abc.net.au/everyday/step-parent-and-parent-dating-someone-new-have-impact-on-adult/101616066) fails to acknowledge what step parents should do. I'd consider respect for the deceased parent, and the relationship the kids had with them, to be the absolute minimum. Love to all those who've had to cope with such people; maybe one day the popular press will get a clue, [like some step-parents](https://rubenerd.com/whats-going-on/). It takes two to tango.

* Bruce Schneier announced he has a new book coming out in February called *[A Hackers Mind: How the Powerful Bend Society’s Rules, and How to Bend them Back](https://www.schneier.com/books/a-hackers-mind/)*. Bruce has been among the sharpest minds in the industry about this for decades, and his writing style manages to be engaging both for professionals and the layperson. It doesn't look like I can preorder via Kobo, but the other major platforms are supported.
