---
title: "The selfcare.tech bot is a new favourite"
date: "2022-04-09T13:42:25+10:00"
abstract: "“Turn off a few notifications”"
year: "2022"
category: Thoughts
tag:
- health
location: Sydney
---
I have derived so much strength and comfort [from this Mastodon account](https://botsin.space/@selfcare). You can follow them from your instance, or [even with RSS](view-source:https://botsin.space/@selfcare.rss) in your blog reader.

Here are some of my recent favourites:

> Turn off a few notifications. [#](https://botsin.space/@selfcare/108077365058999695)
>
> What makes you laugh? Go do it, watch it, read it, if you can. [#](https://botsin.space/@selfcare/108073118583902358)
>
> Consider unfollowing that person that always leaves you feeling worse about things. [#](https://botsin.space/@selfcare/108070994996346753)
>
> It's not just you. You're not alone. [#](https://botsin.space/@selfcare/108067456324684301)
