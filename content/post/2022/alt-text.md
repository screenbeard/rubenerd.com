---
title: "Vintage social network sentences"
date: "2022-04-12T21:34:09+11:00"
abstract: "Mixx Facebook Twitter Digg delicious reddit MySpace StumbleUpon"
year: "2022"
category: Internet
tag:
- language
- pointless
location: Sydney
---
I read this on someone's old blog post, and it took me a moment to realise it wasn't a sentence:

> Mixx Facebook Twitter Digg delicious reddit MySpace StumbleUpon 

Also, how many of those are still around? Are we down to three?
