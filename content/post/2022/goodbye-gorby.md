---
title: "Goodbye, Gorby"
date: "2022-09-01T10:33:24+10:00"
abstract: "“Gorbachev died in a symbolic way when his life's work, freedom, was effectively destroyed by Putin”"
year: "2022"
category: Thoughts
tag:
- europe
- news
- russia
location: Sydney
---
[Channel NewsAsia](https://www.channelnewsasia.com/world/last-soviet-leader-mikhail-gorbachev-who-ended-cold-war-dies-aged-91-2910751)\:

> Gorbachev, the last Soviet president, forged arms reduction deals with the United States and partnerships with Western powers to remove the Iron Curtain that had divided Europe since World War II and bring about the reunification of Germany.
> 
> But he saw that legacy wrecked in the final months of his life, as the invasion of Ukraine brought Western sanctions crashing down on Moscow, and politicians in both Russia and the West began to speak of a new Cold War.
> 
> "Gorbachev died in a symbolic way when his life's work, freedom, was effectively destroyed by Putin," said Andrei Kolesnikov, senior fellow at the Carnegie Endowment for International Peace.

To be clear, he was no saint. Much of the Western press were happy to turn a blind eye to all the horrible things he either authorised, or was complicit in. Working class Russians also saw their living standards plummet after Glasnost, even if the intentions were broadly positive. Whether you want to call it shock therapy, or necessary, depends on your view of history.

Still though, he did more for my German family than anyone in living memory, and the dissolution of the USSR resulted in the independence and freedom of millions of people across Eastern Europe. It's telling that Putin characterises this as a blunder.
