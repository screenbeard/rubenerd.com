---
title: "UI design is as much about expectations"
date: "2022-02-14T10:16:37+11:00"
abstract: "Great site designs don’t try and be clever."
year: "2022"
category: Internet
tag:
- design
location: Sydney
---
A few of you might remember years ago a blog design I wrote to mimic an email inbox. I had a column of category "folders" on the left, a column of recent posts in the middle, and the post itself in the right.

The feedback I got from people was mostly negative, ranging from it being confusing or difficult to use, and the implementation not working that well across multiple viewports. The latter could be solved with a better understanding of responsive design, but it's the former I'm interested in.

Recently I [came across a site](https://brianlovin.com/bookmarks) implementing a similar UI. I was bamboozled, just like my readers were back then. I assumed I'd stumbled on someone's private email server; it wasn't until I clicked around that I got my bearings, and translated that mental model into that of a personal site with posts and bookmarks.

Not to critique that site specifically, but in general such mismatches are the perfect demonstration of two related axioms:

* the principle of least surprise, and
* context is king

Save for games and certain art projects, software and websites shouldn't surprise people. If you want your words read, goods bought, or tasks completed, you want to reduce cognitive overhead. [Danny Halarewich's 2016 article](https://www.smashingmagazine.com/2016/09/reducing-cognitive-overload-for-a-better-user-experience/) is still the best summary I've found on this topic:

> If the user experience design does what it’s supposed to do, the user won’t notice any of the work that went into it. The less users have to think about the interface or design, the more they can focus on accomplishing their goal on your website.
>
> The biggest culprit in cognitive overload is confusing UIs. The user should never have to spend long figuring out how to complete the action they want, nor waste brain power deciphering an icon.

Likewise, context explains why we find UIs applied to alternate settings confusing, even if we understand their original implementation. We may grok car steering wheels and bicycles, but merging the two creates a new, foreign interface that nullifies people's experience and training.

Personal websites like ours get more of a pass, because they're as much a living demonstration of technical and design ability. But by the same token, they're an expression of one's values. I try and keep things simple and predictable because I value that when I see it on other blogs, and it's jarring when I encounter ones that aren't.

In the legendary words of Rob Pike, clear is better than clever. I wish that were emphasised more in modern webdev.
