---
title: "Pouring one out for David Boggs"
date: "2022-03-19T09:13:53+11:00"
abstract: "The co-inventor of Ethernet 🥃"
year: "2022"
category: Hardware
tag:
- goodbye
- networking
location: Sydney
---
You would not be reading this site today if it weren't for the pioneering work of David Boggs. The co-inventor of the Ethernet standard died in late February at the age of 71. 🥃

It's hard not to think of an industry or life that David hasn't touched. The framework of our entire modern world wouldn't be possible without his forward-thinking contributions to packet switching and the IP stack we take for granted today.

I'm on a bit of a computer history kick lately, so if any of you have a documentary or short film to recommend about this gentleman and his achievements, please let me know and I'll share it here.
