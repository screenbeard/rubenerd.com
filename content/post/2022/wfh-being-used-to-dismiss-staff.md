---
title: "WFH being used to dismiss staff"
date: "2022-08-17T13:26:23+10:00"
abstract: "You must return to the office full time. Also our profits are down, and we're finding it hard to retain staff."
year: "2022"
category: Thoughts
tag:
- work
location: Sydney
---
Large tech and finance companies have been doing this lately:

1. Announcing workers must return to the office full time, or face dismissal.

2. Reporting a loss, or lower than expected profits, and will need to cut staff. If only they had a convenient excuse.

3. (Bonus): Complaining about staff retention.

I'm seeing a lot of reports on each point individually, but few finance journalists connect the dots. [Upton Sinclair](https://quoteinvestigator.com/2017/11/30/salary/ "It Is Difficult to Get a Man to Understand Something When His Salary Depends Upon His Not Understanding It") would like a word!
