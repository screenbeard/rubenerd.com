---
title: "Things I don’t like aren’t the worst thing (usually)"
date: "2022-06-08T08:53:49+1000"
abstract: "I’ve got this sort of writing wrong. I’ll do better."
year: "2022"
category: Thoughts
tag:
- sociology
- writing
location: Sydney
---
There are a few aspects of my personality and writing that I'm not proud of that I'm working to change. Seeing other people display them has been a mirror into how I do things too, and I don't like what I see.

One of them is this verbal tick I've picked up where I describe something I don't like as being the worst possible thing, and that people who like them are evil, or a variation on this.

It's said in jest, but I feel like it's one of those things that are harmless jokes... until they're not. Even not accounting for [Poe's Law](https://en.wikipedia.org/wiki/Poe%27s_law), it's a well-worn and predictable progression from those who harbour benign views begin to believe their own parodies.

The ubiquity of such language also disarms it for people, events, and things that deserve it. The pen is mightier than the sword, but like any weapon it needs to be used strategically or it burns out.

Embellishment and rage get clicks, but that's not my motivation for writing here. I've got this sort of writing wrong. I'll do better.
