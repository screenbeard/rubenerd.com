---
title: "Using innocuous prompts as hooks"
date: "2022-02-03T08:22:42+11:00"
abstract: "Asking us a yes/no question isn’t an invitation!"
year: "2022"
category: Internet
tag:
- advertising
- antipatterns
- design
- dr-hook
- psychology
location: Sydney
---
An software and web design antipattern is a feature that coerces a user to perform an action they otherwise wouldn't have. Among thier self-defeating downsides, they train users in advocate to avoid you!

I got a notification this morning from an iOS application asking if I was enjoying using it. I thought of tapping *Yes!*, but then I remembered that nobody asks these questions in isolation. Marketers and advertisers call such prompts a *hook*.

<p><a href="https://www.youtube.com/watch?v=zhDKoDumhQI" title="Play Dr. Hook Greatest Hits || Best songs of Dr. Hook ( full album )"><img src="https://rubenerd.com/files/2022/yt-zhDKoDumhQI@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-zhDKoDumhQI@1x.jpg 1x, https://rubenerd.com/files/2022/yt-zhDKoDumhQI@2x.jpg 2x" alt="Play Dr. Hook Greatest Hits || Best songs of Dr. Hook ( full album )" style="width:500px;height:281px;" /></a></p>

*Nah* I thought, it's a beautiful morning and I'm in a good mood, surely this will be the exception to the rule? Walk Right In, Sit Right Down, tap yes on a prompt and move on. You can't go through live being that cynical.

I tapped *Yes!*, which opened a browser window to a long questionnaire with dozens of "compulsory" fields. As my grandfather used to say *I hate when I'm right*.

And here's the thing: it almost worked. I'm sure there's some psychological studies that show that people are adverse to leaving things unfinished, or not having their advice or opinions recorded. I hovered over that page for a few solid seconds before deciding I'd risk not having them record my *Yes*.

Their invasiveness aren't necessarily what I find offensive, it's that they lie. They didn't ask if we'd fill out a questionnaire to help them improve their software, they obfuscated their intentions. They also traded on our goodwill, something I'm surprised at how willing companies are to part with.

But back to my first point. I've been trained further by this industry to ignore their prompts for feedback, and especially from software produced by this company. I can't be alone for thinking this.
