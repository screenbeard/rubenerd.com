---
title: "P.G. Morgan’s insights into D.B. Cooper"
date: "2022-09-17T10:06:28+10:00"
abstract: "“I find it fascinating because it’s really kind of a little look at America.”"
year: "2022"
category: Thoughts
tag:
- aviation
- history
location: Sydney
---
Claire Barrett did a great interview with P.G. Morgan [over on HistoryNet](https://www.historynet.com/why-were-still-obsessed-with-d-b-cooper-51-years-after-his-hijacking/) about his recent D.B. Cooper documentary (they also publish an [RSS feed](http://www.historynet.com/feed) which you should definitely add).

For those who haven't heard of him, D.B. Cooper, as the press dubbed him, successfully hijacked a Boeing 727 in 1971, and parachuted out the plane's rear airstairs mid-flight with millions of dollars. The man, and most of the money, have never been found. The fact he didn't injure or kill anyone, and the mystery surrounding his disappearance, have lead to a cultural fascination that persists to this day.

In addition to talking about the logistics of the hijacking itself, Morgan also lent some broader insight into the public's perception of the man and what it tells us, fifty years later:

> I would say I think the reality is that he was more prosaic than people think. So many layers and ideals have been projected onto this one man, and I think the reality is probably a bit more mundane. With time, he has become this mythical figure. I understand the Robin Hood analogy, because no one was hurt, it was a victimless crime. This isn’t Ted Bundy, or some other ghastly serial killer. 
>
> That was one reason we liked it, as well. It’s true crime, but there isn’t any blood in there. I’m just fascinated where the space between the reality and the myth lives. And the reality when you get back to it could have been quite mundane and straightforward. But everyone’s brought their fantasies and obsessions and yearnings to the story, and it has become something much bigger. As someone who’s not from here, I find it fascinating because it’s really kind of a little look at America. 

Clara and I will definitely be carving out some time to watch this.
