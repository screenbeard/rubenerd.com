---
title: "A numbered list of encountered tech"
date: "2022-02-17T08:35:58+11:00"
abstract: "One-time password, two's compliment, triple DES...!"
year: "2022"
category: Hardware
tag:
- lists
- pointless
location: Sydney
---
What's the first thing that comes to mind when you think of each number? I thought of this tech over my morning coffee:

1. One-time password
2. Two's compliment
3. Triple DES
4. Commodore Plus/4
5. Windows 95
6. MOS 6502
7. 7zip
8. 8-bit Zilog Z80
9. Mac OS 9, the last of the classics
10. 2<sub>10</sub>!
11. DEC PDP-11
12. The age OrionVM turned yesterday... wow.
13. FreeBSD 13, my favourite release in years, especially for desktop
14. Raspberry Pi (3.14... is that a stretch?)
15. 15-inch MacBook Pro, my first Intel Mac from back in the day
16. Commodore 16 and 116
