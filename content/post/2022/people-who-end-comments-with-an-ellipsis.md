---
title: "People who end comments with an ellipsis…"
date: "2022-08-05T12:34:43+1000"
abstract: "… can be ignored!"
year: "2022"
category: Internet
tag:
- filters
- regex
- social-media
location: Sydney
---
I've noticed comments that end with an ellipsis disproportionately skew towards trolls and condescending people. They often accompany a "you do know that" as well.

> Errrrrr, you do realise that not everyone who does this is a troll...

I just added \...$ (three full stops) and &hellip;$ (an ellipsis) to my social media and chat clients that support regex filters. Instant improvement!
