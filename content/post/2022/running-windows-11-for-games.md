---
title: "Running Windows 11 for games"
date: "2022-09-22T08:38:48+10:00"
abstract: "11 is the first positive UI direction Microsoft has taken since Windows 2000. Pity it’s still Windows."
thumb: "https://rubenerd.com/files/2022/fauna@1x.jpg"
year: "2022"
category: Software
tag:
- windows
- windows-11
location: Sydney
---
I got back into PC gaming again during Covid lockdowns, and it's been a joy researching, building, and tinkering with desktops again! I used to do this as a teenager, and it brought so much enthusiasm back into this space again.

Part of this fun came down to being able to [run Windows games on Linux](https://rubenerd.com/the-current-state-of-linux-gaming/ "The current state of Linux gaming"), thanks to the efforts of the Steam, Proton and Wine communities. Compatible games work shockingly well, to the point where I'd recommend people try the [Fedora KDE Spin](https://spins.fedoraproject.org/kde/) for games first. It's not a sentence I'd ever think I'd type.

But perhaps unsurprisingly, there are a few key titles I wished I could play, and others that had niggling issues that became a nuisance. I realised I was spending more of my precious downtime troubleshooting instead of playing, so I made the decision to replace the Fedora partition with Windows. My current Ryzen build met the minimum requirements for Windows 11, so I thought I'd give it a try.


### Going with Windows 11

I grew up on DOS and Windows (and classic Mac OS, but that's another story). I still consider 2000 to be the high water mark for Microsoft interfaces; everything since has been gaudy or a regression. I knew Windows 11 received mixed reviews for its questionable hardware requirements, but I was encouraged by the screenshots I'd seen.

Not to put too fine a point on it, but Windows 11's UI is the first meaningful course correction Microsoft has taken since 2000. The garish Aero Glass of Vista/7, and awkward Metro UI of 8/10 have been replaced with subtle gradients, borders, and dual tones, reminiscent of modern KDE Plasma. It looks surprisingly decent, and more importantly makes everything easier to discern, especially in complicated Settings screens. I hope to see more of an industry trend towards this.

Under the hood though, it's still Windows. The UI is a hodgepodge of settings interfaces borrowed from Windows 95, 2000, and Vista. Windows Explorer limps along with its baffling mix of toolbars. Windows Defender lets you know it found nothing on a regular basis. ClearType is still ugly, though it looks better running at a clean 2x HiDPI scale at 4K. Updates take forever. Microsoft also can't help themselves with [ridiculous decisions like this](https://answers.microsoft.com/en-us/windows/forum/all/ads-in-microsoft-solitaire-collection/f01669dc-e8bb-4838-835d-4732c066fa5e), but at least we can get the [classic games back](https://win7games.com/).

The installer is also still pushy and confusing, though you can get away with a local account if you upgrade for free from a licensed copy of 10 as I did.

<figure><p><a href="https://rubenerd.com/files/2022/fauna@full.jpg"><img src="https://rubenerd.com/files/2022/fauna@1x.jpg" alt="Screenshot of the Windows 11 desktop, with Steam installing games in the background" style="width:500px;" srcset="https://rubenerd.com/files/2022/fauna@1x.jpg 1x, https://rubenerd.com/files/2022/fauna@2x.jpg 2x" /></a></p></figure>


### Kitting it out

For all my reservations, there's still enough I recognise and remember. [VeraCrypt](https://veracrypt.fr) still works for whole-drive encryption, which I trust more than BitLocker (and if you run Windows Home, this would be your only practical option). There's also a new set of [PowerToys](https://learn.microsoft.com/en-us/windows/powertoys/install). 

The best thing that's happened to Windows since I've been gone is the [scoop package manager](https://scoop.sh/). I'd tried using Chocolatey before, but scoop gets far closer to what I miss on \*nix. Within a few commands I had Firefox, Steam, Origin (unfortunately!), Minecraft, and so on installed.

After a few minutes of PowerShell, I wondered how difficult it would be to replace with a proper shell. Rather than install Cygwin or WSL, I downloaded [Ron Yorston's Windows BusyBox port](https://frippery.org/busybox/), and pinned it to the taskbar. It ain't no BSD userland, but it has all the basic \*nix commands covered. I even have a basic `.ashrc` for it, which makes me chuckle. It has no business working as well as it does.


### Conclusion

Running Windows again feels surreal; nostalgic even. The UI is still an inconsistent mess, but at least it's an improvement for the first time in decades. There's also enough third-party tooling for a \*nix guy to get by, which was a pleasant surprise.

That said, I'm still booted into the FreeBSD desktop partition most of the day. This lesson has taught me that while Windows makes a great game launcher, I still wouldn't want to subject myself to it full time. I suppose for Microsoft it's still a sale either way.
