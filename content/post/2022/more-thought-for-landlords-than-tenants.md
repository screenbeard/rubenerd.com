---
title: "More thought for landlords than tenants"
date: "2022-08-19T08:15:29+10:00"
abstract: "It’s a classic power imbalance, but one the press don’t seem that interested in covering in any detail."
year: "2022"
category: Thoughts
tag:
- economics
location: Sydney
---
It's all all-too-familiar story, but one that's taken on new urgency in light of our current economic situation!

With rising inflation and living costs, journalists are covering the plight of landlords. Rising interest rates are sharply increasing their loan payments, eating into their rental yields, and throwing off their budgets. It's scary, especially for those who bought into the fad with bad investment advice.

I have empathy for landlords with a single mortgage that live and rent elsewhere to be close to work, to care for family, or other reasons. On the other side are institutional investors with large portfolios that price prospective owner-occupiers out of the market.

It's a classic power imbalance. Landlords can increase rent to cover costs. But with inflation higher than wage growth, tenants must make up the shortfall by eating into savings, heating bills, and other non-discretionary funds. Moving may be required in the worst case, which is *also* expensive and disruptive.

It's another example of upwards wealth transfer that, once again, disproportionately affects people on lower incomes. Which sucks, because they're the ones who need the most help. Progressive governments need to recognise and do something about it, rather than tip toe around investors with eight houses complaining that Eugene put a poster up.

