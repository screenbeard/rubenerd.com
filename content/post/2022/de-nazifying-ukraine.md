---
title: "“De-Nazifying” Ukraine"
date: "2022-03-06T19:24:01+11:00"
abstract: "Fuck you, Putin"
year: "2022"
category: Thoughts
tag:
- family
- ukraine
location: Sydney
---
This is a hard post to write, but it has to be said.

My dad's family emigrated from Germany to Australia in the 1950s. They were part of a wave of immigrants from post-war Europe seeking a better life for themselves and their families that came down under as part of the government's sponsorship programme. They settled in Sydney, where I'm typing from today.

I have few memories of my paternal grandfather Hans, and the ones I do have are probably a blend of experience and stories from my parents. My mum adored him; she said he was one of the kindest, softest spoken people she'd ever met.

Hans was a field medic during World War II. The family still has a box of his medals and ribbons for his services tending to soldiers on the battle field, complete with the Third Reich's colours and swastikas. My dad told me it was so we wouldn't forget.

I remember coming home from primary school after learning about the war, and asking my parents of it. My dad rarely wanted to discuss it, and my mum said Hans would break down in uncontrollable tears at the mere mention of it, even decades later.

So you can understand why some of us are more than a little infuriated at Putin's idea that Ukraine has to incur a Russian "special operation" to "de-Nazify" their beautiful country, and why we're so invested in the outcome of this war.

Putin's justification is a cynical, transparent, ghastly insult to the millions of Jewish people and the mentally ill who were killed, and for the families of those conscripted at gun point into a senseless war to kill their fellow citizens.

Russian parents are being lied to about where their sons are heading, and innocent Ukrainians are being compared to some of the worst atrocities committed by humanity. It looks an awful lot like the *opposite* to what Putin claims is happening. As they say, judge psychopaths on what they do, not what they claim.

A trial at the Hague would be the most humane outcome I'd wish upon Putin and his sycophantic cabal of advisors and cronies.
