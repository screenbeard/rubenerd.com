---
title: "Always check for the wrong item too"
date: "2022-01-27T21:07:35+11:00"
abstract: "People list items with typos, or the wrong unit numbers all the time."
year: "2022"
category: Hardware
tag:
- shopping
- tips
location: Sydney
---
My boss gave me the opportunity a few years ago to work out of our San Francisco office. It was a life-changing experience, and despite my choice words for Silicon Valley hubris of late, I loved it. I never properly wrote about it; maybe I should one day.

Anyway, is a phrase with one word. I decided to take advantage of the fact that one of my favourite electrical engineer YouTubers had a sponsor that only shipped to the US. I ordered a specific cool little LCR and multimeter, only to be told they were out of stock and had no idea when they'd get another batch. They weren't alone; every retailer and online store I checked were clean out.

I was about to give up, when a well-known online retailer had one in stock, and for almost half the price. I couldn't figure out why until I realised it was misspelled *Mustimeter*. Perhaps their super clever algorithms of brilliant genius slash prices overtime for units that don't sell, but they're not smart enough to realise *why* they're not selling.

I was probably being recommended the unit after looking at devices with similar descriptions or sellers, but the chance is low that someone doing an organic web search would have found it. People with these sort of hobbies are generally looking for *specific* model or part numbers.

I see this all the time on sites like eBay. I saw a German auction for a Commodore 64, that upon closer inspection was a much rarer, early-generation VC-20. Another DOS tower was advertised as having an Intel 8038 instead of an 80386. Even my mum used to talk about her rare powder compacts being listed as "mirrors". Sally Oldfield would have choice words about that.

My current favourite is my retro Iomega Ditto 2 GB drive which was listed as a "cassette deck". I'm sure they think the joke is on me, given a cassette deck would be eminantly more useful than an early Travan drive, but don't they realise I'm a nostalgic fool!? A fool... *with a Ditto drive!* Take that, Peter!

Sometimes they're typos, or the person listing them don't understand what they're selling. Either way, **I always forget** that it's worth checking for items *close* to what you're after, or to try a few queries with more vague language. You might be as surprised as I regulary am.
