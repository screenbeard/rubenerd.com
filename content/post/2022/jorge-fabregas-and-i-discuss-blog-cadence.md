---
title: "Jorge Fábregas and I discuss blog cadence"
date: "2022-08-16T08:11:29+10:00"
abstract: "Answering reader email about why I post in batches."
year: "2022"
category: Internet
tag:
- feedback
- weblog
location: Sydney
---
Jorge emailed some feedback over the weekend:

> I've noticed that your feed shows new items in "pairs"?  Initially I
thought perhaps you write two posts each time...or do you  queue them
until you have two and then publish them? I have hundreds of feeds (using
[Liferea](https://lzone.de/liferea/)) and this is the only one where I see this.

My American friends might say this is a bit *Inside Baseball*, but it gives me an opportunity to discuss how I write stuff. Maybe there's something useful for other people here too.

I used to blog upwards of four times a day [in the late-2000s](https://rubenerd.com/year/2009/), but the feedback I got was that I was spamming people's RSS aggregators. I took this seriously; I don't want my writing to detract from other things you all read. I subscribe to a specific aviation blog that post dozens of times a day with giant photographs and walls of text. While I personally find it all interesting and fun, I can absolutely see why it would be annoying having it dominate your other feeds.

The upshot of this was I:

* Scaled back how often I write, which I think has (slightly!) improved the quality of what I submit.

* Submit posts in batches; usually twice, sometimes thrice. Is that how you spell thrice? It doesn't look right. English is such a weird language.

This is also a side-effect of how the site is published too. I used to run server-side software, but now I use the Hugo static site generator so I can edit posts as text files. Batch posting lets me get away with (lazily) committing them to my git repo at the same time!

Having said all that, is a phrase with four words. How you write is up to you. My experience is that people are more lenient with the spamming if your posts are short, because they don't take too much vertical screen real-estate as they scroll by. Social media may have also trained (or is the correct word desensitised?) people to accept endless scrolling in 2022, but I'm wary of contributing to that.

Appreciate the question Jorge, gracias :).
