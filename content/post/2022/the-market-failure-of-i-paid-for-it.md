---
title: "A market failure with sunk costs"
date: "2022-04-11T15:16:15+10:00"
abstract: "Arriving in a new office space and finding all the Ethernet cables chopped off... again."
year: "2022"
category: Hardware
tag:
- economics
location: Sydney
---
Have you ever wondered why people seem content on making the world a bit crappier, if it means they don't lose a buck? A quid? Another insignificant amount of financial value?

Every time our company has moved to a bigger office, we're faced with a wall of Ethernet cables that have been cut off a patch panel. The previous tenant would have spent peanuts on the panel, and probably won't be using such a panel with butchered Ethernet connectors it at their new address, but they had to take it because *it was theirs*. Our ops guys then have to spend days tracing where each cable goes, crimping new connectors, and attaching them to a new panel.

That paragraph invoked the word panel at least four times. Panel. Patch panel. I heard you like panels.

The same thing happened when Clara and I moved panels. Moved *house*... damn it. The previous <span style="text-decoration:line-through">panel</span> tenant must have affixed a water filter to the kitchen tap. When they moved out, they took the liberty of removing it and part of the tap assembly with them. *Because they paid for it.* It's unlikely their new apartment would have the same tap as here, so they probably threw it away. But it now means we have a kitchen tap that *sort of* works, unless we or the landlord could be bothered to track down the specific model number and find a replacement.

This attitude of *I paid for it, so screw the next person* is the perfect example of a market failure and a negative-sum game. Both deal with societal loss when someone acts in their own selfish interest. Any incidental benefit gained from the previous tenants in these examples is disproportionately borne by the next person in money and effort. That sucks!

Technically, a tap that sucks would be the opposite of what you'd want. Like a decimated patch panel, which would be a patch panel with 10% of its ports damaged. Pardon, *10pc*; with apologies to incorrect style guides.

Clara and I leave apartments in better condition than when we moved in, and the company I work for always leaves offices with things plugged in neatly for the next tenants. But that just shows that under our current economic system, regular people like you and I subsidise the selfish.

As with everything from digital privacy to patch panel shenanigans, you have to look at the incentives to understand the ways in which people behave. If you were selfish, why would you want to fix this system if others are willing to part with their time and money to accommodate you?

They may be right... but it won't stop me oiling those hinges even if we're about to move out, or leaving a labelled patch panel behind. *Sucker!*
