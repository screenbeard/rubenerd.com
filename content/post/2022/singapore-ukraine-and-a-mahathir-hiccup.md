---
title: "Singapore, Ukraine, and some Mahathir irredentism"
date: "2022-06-23T09:12:30+10:00"
abstract: "The former Malaysian PM's appreciates Singapore, but thinks it should be demanded back. Welcome to the new world order."
year: "2022"
category: Thoughts
tag:
- singapore
- ukraine
location: Sydney
---
Singapore Prime Minister Lee Hsien Loong's May Day Speech last month touched on the [wider implications of the Ukraine invasion](https://www.straitstimes.com/opinion/the-shadow-cast-by-the-ukraine-war-over-singapores-outlook), especially for similarly smaller countries facing down much larger ones:

> Russia's attack on Ukraine has undermined the global order: the basic rules and norms for countries, big or small, to interact properly with one another. That means not invading somebody else, claiming to put right "historical errors and crazy decisions", because that is a flagrant violation of the United Nations Charter. It is bad for every country, but especially for small states like Singapore. Our security, our very existence, depend on the international rule of law. That is why Singapore has taken a strong stand, condemned the attack and imposed targeted sanctions against Russia.

Singapore is bigger than I think people realise. For a country a [quarter the size of Long Island](https://www.wolframalpha.com/input?i=total+area+of+singapore+vs+%22long+island%22 "WolframAlpha comparison of land area") in New York, its population is higher than that of the Republic of Ireland or any two of the Baltic states combined, and about equivalent to Norway or Finland. Like these counties, it's understandably concerned about the power imbalance represented by larger states.

Singapore was unique among Southeast Asian counties for its swift, staunch support for Ukraine, and for imposing sanctions against Russia for their illegal and unjustified invasion. I applauded their resolve, though I remember people being surprised at just how far they went.

Former Malaysian Prime Minister Mahathir Mohammad's [surreal comments yesterday](https://mothership.sg/2022/06/mahathir-johor-return-singapore/) may provide some context as to why:

> But he then went on to say “We should demand not just that Pedra Branca, or Pulau Batu Puteh, be given back to us, we should demand Singapore as well as the Riau Islands, as they are Malay land.”
>
> Applause from the audience interrupted him when he mentioned the return of Singapore, and he made a brief pause.
>
> "However, there is no demand whatsoever of Singapore. Instead we show our appreciation to the leadership of this new country called Singapore," Mahathir said.

It's worth remembering Singapore was ejected from the Federation of Malaysia *against* their will, so these comments make even less sense. It's the geopolitical equivalent of firing an employee for what you perceive to be acting out, then complaining they're absent.

🌲 🌲 🌲

I was going to leave it at that, but I can't shake a few other thoughts. Thanks for indulging me.

Something missing from the analysis of this talk is just how deep this perception probably runs. It's fair to assume that if public comments like this are bubbling up, they've probably already been shared privately behind closed diplomatic doors. Mahathir has been Malaysian PM twice now, and certainly has a reputation for speaking his mind. Granted he may be sabre rattling now to garner attention, but perspectives like this don't come out of nowhere. It'd also certainly explain Singapore's strong response.

Malaysia and Singapore are tied at the hip geographically, economically, and culturally, and both were subject to British colonialism. But I think such comments are still irresponsible, especially given the current global situation.

This leads me to the final point that these political leaders, whether they're in Malaysia, or Russia, or the PRC, or any number of other places, ignore at their own peril. Arrogance leads them to think *they* alone are responsible for choosing the point of time in which to rewind the tape. Like all irredentist viewpoints, it's a look backwards (but not too far!), not forwards.

Regardless of whether you think such claims have merit, they're self-defeating, and should be immediately dismissed on that ground alone. They:

* Rob their target people of agency over their future, which human nature shows will only breed resentment, and eventual conflict and death among the people they claim to hold dear (see Russia's butchering of their Slavic kin). Malaysia won't take Singapore by force anytime soon, but can you blame Singaporeans for feeling uneasy when a former leader so openly talks about their country like that?

* Hamper the opportunity for cooperation, respect, and peace; actions for which a remote community might eventually want to reciprocate in the form of a political union, or other arrangement at some point.

From families to nation states, if you want to drive people apart and keep it that way, violence and words like this are *exactly* how you do it. You'd think history repeating itself would be a reminder. Take it from someone coming from another former British colonial outpost.
