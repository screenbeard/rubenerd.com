---
title: "Fascinating new pencil soldering irons"
date: "2022-12-31T10:09:21+11:00"
abstract: "Trading in a soldering station for a NovelLife or Pinecil device"
year: "2022"
category: Hardware
tag:
- homelab
- soldering
location: Sydney
---
I first learned of pencil soldering irons while watching Adrian's Digital Basement, and reading [his list of parts](https://adriansbasement.com/parts/) he uses to fix computers. I've since noticed many technical YouTubers use them.

There seem to be a few major benefits. They're much smaller, which lets you be more nimble around tiny ICs and other components. Yet advances in technology mean they heat up just as fast, or even faster, than traditional soldering irons and stations. Again, for someone with almost no free space, this seems really compelling.

<p style="width:233px; margin:0 auto"><img src="https://rubenerd.com/files/2022/pinecil@2x.jpg" alt="Press image of the Pinecil" style="width:233px;" /></p>

The two biggest ones on the market seem to be the [NovelLife TS100/TS101](https://www.aliexpress.com/item/1005004795605009.html), and the [Pinecil](https://pine64.com/product/pinecil-smart-mini-portable-soldering-iron/) from the Pine64 project. Both can be bought with a set of different tips, including the flat chizel I find the easiest to work with. They can also be charged over USB-C or a power brick.

I'm a complete novice when it comes to this stuff; I studied computer science and business IT at university, and spent my childhood tinkering with text and software without much thought to what it was running on. But I've been thoroughly bitten by the bug in my thirties, and enough people I trust seem enamoured with these specific irons, so I'm thinking I'll give one a try.

