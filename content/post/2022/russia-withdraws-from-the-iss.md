---
title: "Russia withdraws from the ISS"
date: "2022-07-27T14:59:36+10:00"
abstract: "Was any of this worth it, Putin? Lavrov? You small, small men."
year: "2022"
category: Thoughts
tag:
- news
- politics
- science
- space
- ukraine
location: Sydney
---
[The Economist reported](https://www.economist.com/the-world-in-brief) in their World in Brief section:

> Russia will withdraw from the International Space Station after 2024, said Yuri Borisov, Russia’s space chief. The country will focus on building its own orbital outpost. Mr Borisov’s predecessor had previously said that Moscow would consider extending its participation in the station’s operations only if the West lifts sanctions on Russia. Since the invasion of Ukraine, the space station had represented a rare avenue of co-operation between America and Russia.

This made me choke up.

In my [last silly show](https://rubenerd.com/show422/) I talked about how I missed looking forward to the future. The ISS was a symbol of global cooperation, and represented the idea that we could rise above (and maybe even solve) our terrestrial issues. The crew wear flag patches, but it's a station full of humans and scientists first.

Was any of this worth it, Putin? Lavrov? You small, small men.
