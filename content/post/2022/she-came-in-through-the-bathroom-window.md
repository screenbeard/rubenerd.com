---
title: "She Came In Through the Bathroom Window"
date: "2022-07-11T08:50:31+10:00"
abstract: "I misheard this lyric my entire life. It's SUNDAY."
year: "2022"
category: Media
tag:
- the-beatles
- music
- music-monday
location: Sydney
---
It's [Music Monday](https://rubenerd.com/tag/music-monday/) time! Each and every Monday, except when I forget, I discuss music to help make the start of the work week a bit nicer. It has absolutely nothing to do with the fact that Music and Monday start with the same consonant, or that I ripped the idea off my university's anime club.

Today's Music Monday post, which comes each Monday, is an embarrassing admission about a misheard lyric, however its easily eclipsed by the relief and joy I feel having now learned the truth.

On side B of *Abbey Road* we're treated to a medley of Beatles music, one of the tunes being [the title of the post](https://www.youtube.com/watch?v=NVv7IzEVf3M "The Beatles singing She Came In Through the Bathroom Window"). The chorus ends with:

> Some day's on the phone to Monday;   
> Tuesday's on the phone to me.

Except, it's not *some day*, it's *Sunday*. That makes **so much more sense**. Well, as much sense as Beatles lyrics make.

I joke that it took me until I was 15 to realise they were called the Beatles because it had the word *beat* in it. Tack on another two decades, and I now know the lyrics to one of their most iconic songs. [Joe Cocker](https://www.youtube.com/watch?v=TedvuER50Lk "Joe Cocker's cover of She Came In Through the Bathroom Window") would be proud.
