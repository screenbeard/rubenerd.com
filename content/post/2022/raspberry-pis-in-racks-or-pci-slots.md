---
title: "Raspberry Pis in racks or PCI slots"
date: "2022-09-29T08:41:09+10:00"
abstract: "I’d love to consolidate my Pis into one of these things."
thumb: "https://rubenerd.com/files/2022/u6261_1_@1x.jpg"
year: "2022"
category: Hardware
tag:
- raspberry-pi
- racks
location: Sydney
---
I'm on a bit of an efficiency kick of late. I'm interested to learn what I can run with less, to save power, money, and space.

I'd always wanted my own home server rack, before I decided to consolidate everything into a server pedestal case. But then I learned you can get rack mounts for the Raspberry Pi too. Here's [an example from UCTRONICS](https://www.uctronics.com/cluster-and-rack-mount/for-raspberry-pi/1u-rack-mount/uctronics-19-1u-raspberry-pi-rack-mount-with-ssd-mounting-brackets.html), with some nice thumbscrew sleds:

<figure><p><img src="https://rubenerd.com/files/2022/u6261_1_@1x.jpg" alt="Rack mountable Raspberry Pi system with thumb screws, and four Pis mounted." style="width:500px; height:255px;" srcset="https://rubenerd.com/files/2022/u6261_1_@1x.jpg 1x, https://rubenerd.com/files/2022/u6261_1_@2x.jpg 2x" /></p></figure>

I've seen the Raspberry Pi Compute Modules and their boards mounted in racks for ARM development, but the idea of mounting regular Raspberry Pis into racks tickles me. Think of how *orderly* all my generations of hardware would be, *and* with all the cable clutter hidden behind panels!? I could even see these potentially fitting my cute new [RISC-V board](https://rubenerd.com/backing-the-visionfive-2-risc-v-dev-board/) I backed on Kickstarter. I could even get a [LACK rack](https://wiki.eth0.nl/index.php/LackRack) in the interim.

The alternative would be to figure out how to mount them in a pedestel server case, along with my existing mATX Supermicro board. I have a bunch of empty PCI slots; would Pis fit on a sled mounted to those? I expect the IO would be too tall, but then if they're all internal to the case that wouldn't matter.

Money is tight right now, but I love working through hypotheticals like this... and not just on account of that word sounding like hippopotamus. There's something about having a dense, kitted-out box that does everything, much like my mythical external drive enclosure that would contain all the power bricks and SCSI cables for all my external retro Iomega drives.

Weirdly, I'd be interested in making something out of wood before I did any metalwork. Pity I live in an apartment either way.
