---
title: "Bob Malach, Michel Petrucciani: For All Time’s Sake"
date: "2022-03-14T09:11:46+11:00"
abstract: "Such a gorgeous track!"
thumb: "https://rubenerd.com/files/2022/yt-9F1wctmiSek@1x.jpg"
year: "2022"
category: Media
tag:
- music
- jazz
- gojazz
location: Sydney
---
It's [Music Monday](https://rubenerd.com/tag/music-monday/) time. Each and every Monday, except when I don't, I share a musical piece of music on Mondays, without fail, except when I fail.

Today we have Bob Malach and Michel Petrucciani performing *For All Time’s Sake*. I discovered this gorgeous track on a Japanese jazz compilation from Ben Sidran's Go Jazz label I [bought in February](https://rubenerd.com/gojazz-nakano-miku-bluescsi/).

<p><a href="https://www.youtube.com/watch?v=9F1wctmiSek" title="Play Bob Malach, Michel Petrucciani - For All Time's Sake"><img src="https://rubenerd.com/files/2022/yt-9F1wctmiSek@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-9F1wctmiSek@1x.jpg 1x, https://rubenerd.com/files/2022/yt-9F1wctmiSek@2x.jpg 2x" alt="Play Bob Malach, Michel Petrucciani - For All Time's Sake" style="width:500px;height:281px;" /></a></p>
