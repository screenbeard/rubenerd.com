---
title: "My first shower thought in a while"
date: "2022-08-30T13:01:54+10:00"
abstract: "Aaah, that water feels nice."
year: "2022"
category: Thoughts
tag:
- health
- shower-thoughts
location: Sydney
---
From this morning:

> Aaah, that water feels nice.

That might sound like a joke, but it was a genuine revelation. I've long treated showers as something one does on autopilot. This was the first time in a while I appreciated it for what it was.

Showers feel *great*. I'm lucky I live in a time and place where one is accessible.
