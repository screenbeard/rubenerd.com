---
title: "Tweaking my blog theme fonts for *nix users"
date: "2022-02-18T21:57:01+11:00"
abstract: "Switching from Liberation Sans to Nimbus Sans"
year: "2022"
category: Internet
tag:
- design
- fonts
- weblog
location: Sydney
---
I've changed my theme to use Nimbus Sans instead of Liberation Sans. I've fallen back to the latter for years, but the former looks better to me on my FreeBSD and Linux desktops.

If you're on a BSD, Linux, illumos etc, let me know if this breaks things horribly for you. People use wildly different display and subpixel settings, so I want to make sure it's as legible as possible.

I may have even switched KDE Plasma to use Nimbus Sans over Robo Sans.
