---
title: "New-ish theme for 2022"
date: "2022-08-07T10:38:45+10:00"
abstract: "Paper is back!"
year: "2022"
category: Internet
tag:
- design
- weblog
location: Sydney
---
A few eagle-eyed readers among you noticed my new blog theme. Truth be told it's the same theme I wrote and used up to 2015, albeit with backported fixes for modern Hugo (cough)!

I realised that for all my talk about how I missed depth and textures in desktop interfaces, my site was also entirely flat. I think the border around each section makes them much clearer.

The "paper" methaphor extends to posts, the About Me section, and archives. Some of you think it should only be around the posts themselves, but I didn't want large blocks of text floating in the background.

Rubi is also taking a break. Having an anime mascot annoys so many people on link aggregator sites which can only be a good thing, but Clara wants to redraw her in her current style.

**Update:** A special thanks to [James](https://jamesg.blog/) for helping to troubleshoot a few issues with the dark mode version of the theme, much appreciated! You should see this reflected if you force a refresh. 
