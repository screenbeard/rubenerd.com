---
title: "CallMeKevin’s ancient bear proverb"
date: "2022-02-18T14:46:25+11:00"
abstract: "There's only so many snowballs you can throw before a bear eats you."
year: "2022"
category: Media
tag:
- callmekevin
- quotes
- video
- youtube
location: Sydney
---
From his most recent episode where he [spawns natural disasters](https://www.youtube.com/watch?v=vaIk06OFsLs)\:

> There's only so many snowballs you can throw before a bear eats you.
