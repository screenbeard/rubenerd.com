---
title: "The 86Box PC emulator"
date: "2022-11-08T09:54:59+11:00"
abstract: "An emulator that faithfully emulates classic PC hardware. It’s a ton of fun, and practically quite useful."
thumb: "https://rubenerd.com/files/2022/86box@2x.jpg"
year: "2022"
category: Thoughts
tag:
- 86box
- dos
- os2
- virtualisation
- windows
- windows-31
- windows-95
- windows-nt
- windows-nt-351
location: Sydney
---
For someone who's been [using virtualisation tools](https://rubenerd.com/tag/virtualisation) since Virtual PC on a 1999 iMac DV, and who makes his living documenting and architecting systems on Linux Xen, I had no idea of the existence of PCem, and of the 86Box fork. [ozzmosis](https://mastodon.cloud/@ozzmosis/109245756467401663) sent me a screenshot on Mastodon, and I've been hooked on it ever since.

Compared to other virtualisation tools like QEMU and DOSBox, 86Box aims to faithfully reproduce original hardware going back to the first IBM PC. It does this using [original ROMs](https://github.com/86Box/roms/releases/tag/v3.7), coupled with period accurate virtual drives, interfaces, sound cards, GPUs, and NICs.

This is an important distinction. It can run on non-PC hardware like Apple Silicon because it isn't a hypervisor. It also means vintage operating systems receive hardware they expect, and for which they have driver support. If you ever wanted to see how Windows 3.0 or OS/2 looked with 256 colours on a Tseng VGA card, this is how you do it.

<figure><p><img src="https://rubenerd.com/files/2022/86box@2x.jpg" alt="A few 86Box VMs showing Encarta 95 running in Windows NT 3.51, the BIOS on my 486, and OS/2 Warp 4.5" style="width:500px;" /></p></figure>

This software has been nothing short of amazing for me. I've been able to reproduce all my childhood computers down to the BIOS, and with the exact graphics and sound I remember fully supported on the OSs I ran. The team suggests you duplicate the binary if you want multiple machines, but I've also been using PowerQuest's Boot Magic like I used to to divvy up my machines.

The software struggled to emulate more than a 486 SX and Sound Blaster 1.5 board on my sister's i5 MacBook Air, but even the most kitted out Pentium II machine ran flawlessly on my M1 MacBook Air at a coffee shop, and my home FreeBSD Ryzen box. There's something that tickles me about an RTX 3070 emulating an S3 ViRGE, and an M1 pretending it has a PnP AWE Sound Blaster daughter card.

There's also a practical element to it. My childhood Pentium 1 has reached an impasse for storage with its flaky socket 7 VIA chipset, but I've been hesitant to throw even more money at it to fix it. This emulator has let me experiment the feasibility of using different SCSI cards before buying one second hand, including OS support and a ballpark idea of performance.

You likely already know about this tool if you're into retro hardware and emulation, but if you were living under a rock like me, give it a try!
