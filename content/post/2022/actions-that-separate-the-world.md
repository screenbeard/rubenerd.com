---
title: "Actions that separate the world"
date: "2022-04-06T11:50:32+10:00"
abstract: "We’re partitioning and closing ourselves off again."
year: "2022"
category: Thoughts
tag:
- politics
- ukraine
location: Sydney
---
Not a day goes by now without an embassy closing, or visas being denied, or goods and services being blocked. We're partitioning and closing ourselves off from each other again. It's not another Iron Curtain yet, but it's sure starting to look like one.

It was necessary when Covid first hit, but this time it's *entirely* the preventable fault of a small man in Russia. His countrypeople are being removed from the modern world because his government is doing unspeakable things. He wasn't the first to, nor is he the only one, and he won't be the last.

I don't like when the world closes. We're one big family on this planet, and politics is a ridiculous reason to separate families and lives. I have Russian friends, and friends in Eastern Europe. It sucks knowing their lives are being turned upside down through no fault of their own, to say nothing of those who's lives and livelihoods are at risk.

If there's a silver lining to any of this, it's that people are rediscovering and embracing who their friends really are. A healthy, happy world will depend on these bonds when we start to open back up again after this latest fuckery.
