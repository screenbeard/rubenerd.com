---
title: "Playing with CD-RWs on FreeBSD"
date: "2022-01-25T09:54:02+11:00"
abstract: "Written in memory of Jörg Schilling ♡"
thumb: "https://rubenerd.com/logo.png"
year: "2022"
category: Software
tag:
- bsd
- cds
- freebsd
- guides
location: Sydney
---
<p>I didn’t realise until just now that Jörg Schilling, the principle maintainer of cdrtools, died last year. I’ve been writing about his software for ages, yet never got around to sending him a thank you. I consider this a wakeup call to appreciate people who make my life better.</p>

<p>But I digress. I’ve been throwing away tons of old crap, some of which were ancient CD-Rs which I imaged and saved as ISOs. My plan is to use a few CD-RWs to boot and ferry data to my retrocomputers and for data centre trips, and only burn stuff as I need it. It saves space and money, and reduces the amount of one-time use plastic waste I’m generating.</p>

<p>The FreeBSD Handbook already includes excellent <a href="https://docs.freebsd.org/en/books/handbook/disks/#creating-cds">CD-burning instructions</a>, so instead I’m going to explore how rewriteable media works. As a quick refresher, you find out which device ID your burner is, then provide it an ISO:</p>

<pre>
# cdrecord -scanbus
==> ...
==> scsibus7:
==> 7,0,0 700) 'PLDS' 'DVD-RW DU8A6SH' 'DL62' Removable CD-ROM
==> ...
&nbsp;
# echo "CHECK THIS IS THE ID OF YOUR DRIVE BEFORE COPY/PASTA"
# cdrecord dev=7,0,0 image.iso
</pre>

<p>My first attempt to burn an ISO onto an old CD-RW failed:</p>

<pre><code>cdrecord: Cannot get next writable address for 'invisible' track.
cdrecord: This means that we are checking recorded media.
cdrecord: This media cannot be written in streaming mode anymore.
cdrecord: If you like to write to 'preformatted' RW media, try to blank the media first.
cdrecord: Disk capacity is unknown.
</code></pre>

<p>The hint was on line four. This disc was already full of data, so I have to blank it first. I hadn’t had coffee all morning, so my mind was doing this already.</p>

<p>For those who don’t remember the joys of optical media, there are many ways to blank, wipe, clear, or reformat a disc prior to writing new data. The quickest and most common is <strong>fast</strong>, which only overwrites the table of contents (TOC) and program memory area (PMA) at the start of the disc, indicating the rest of the disc can be overwritten, not unlike a quick format of a hard drive. My anecdotal experience is you can do this a few times before the error rate and failed burns become more common.</p>

<pre><code># cdrecord dev=$DEVICE blank=fast</code></pre>

<p>It’s probably overkill, but I’ve got into the habit of always blanking <strong>all</strong> the disc each time. It takes the equivalent of writing an entire disc worth of data, because that’s what it’s doing! But the end result is a nice, clean disc for new data.</p>

<pre><code># cdrecord dev=$DEVICE blank=all</code></pre>

<p>Now we can record our ISO image again. I’m always interested to see the capabilities of drives I use, even if at best I understand half the features here. This is a simple SATA slimline unit in my primary FreeBSD tower:</p>

<pre><code>Copyright (C) 1995-2019 Joerg Schilling
scsidev: '7,0,0'
scsibus: 7 target: 0 lun: 0
Using libscg version 'schily-0.9'.
Device type    : Removable CD-ROM
Version        : 0
Response Format: 2
Capabilities   :
Vendor_info    : 'PLDS    '
Identifikation : 'DVD-RW DU8A6SH  '
Revision       : 'DL62'
Device seems to be: Generic mmc2 DVD-R/DVD-RW/DVD-RAM.
Using generic SCSI-3/mmc   CD-R/CD-RW driver (mmc_cdr).
Driver flags   : MMC-3 SWABAUDIO BURNFREE FORCESPEED
Supported modes: TAO PACKET SAO SAO/R96P SAO/R96R RAW/R16 RAW/R96P RAW/R96R
</code></pre>

<p>It’s worth mentioning as well that CD-RWs do typically take longer to burn that a CD-R, even discounting the time taken to blank them. It’s more than an acceptable compromise for me, but don’t be surprised if the drive reports <strong>4</strong> for the drive speed. I did briefly have a CD burner and CD-RW media as a kid that worked at 8×, but both those are long gone.</p>

<p>This was a fun experiment! Now I have a reliable way to generate these disc images with a few CD-RWs.</p>
