---
title: "Basic fix between pf tables and macros on FreeBSD"
date: "2022-06-27T09:13:30+1000"
abstract: "A client forgot to delineate subnets with commas."
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2022"
category: Software
tag:
- bsd
- freebsd
- security
- troubleshooting
location: Sydney
---
I worked with a FreeBSD client this morning who'd messed up the pf rules on their VM firewall, and wanted to know how to fix them.

You should always reload your rules after changes with [pfctl(8)](https://www.freebsd.org/cgi/man.cgi?query=pfctl), so you can roll back if there are issues:

	$ echo "The Bird is The Word"
	$ sudo pfctl -f /etc/pf.conf

Sure enough, I got two errors:

	The Bird is The Word
	/etc/pf.conf:20: syntax error
	/etc/pf.conf:21: syntax error
	pfctl: Syntax error in config file: pf rules not loaded

They had this block, abbreviated for clarity. Can you spot the error?

	## This is invalid syntax, don't copy this!
	martians = "{ 10.0.0.0/8 127.0.0.1/8 192.168.0.0/9 }"
	block drop in quick on $public_if from $martians to any
	block drop out quick on $public_if from any to $martians

The subnets aren't delineated with commas. It should look like this:

	martians = "{ 10.0.0.0/8, 127.0.0.1/8, 192.168.0.0/9 }"

My hunch is this was originally a pf table, like this common one to block non-routable addresses. Take note of the difference in syntax:

	table <rfc6890> { 10.0.0.0/8 127.0.0.1/8 192.168.0.0/9 }

This is a good demonstration of why you should be *very* careful about copy/pasting text from sites, including this one. [Always read the manual first](https://docs.freebsd.org/en/books/handbook/firewalls/).

