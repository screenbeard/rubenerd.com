---
title: "Using Blender as a video editor"
date: "2022-12-27T10:03:47+11:00"
abstract: "It’s surprisingly capable, cross platform, and open source."
year: "2022"
category: Software
tag:
- drafts
- reviews
- video
location: Sydney
---
*This was another post forever consigned to my drafts folder, pending screenshots and more detail that I never got around to adding. I've decided to publish as-is.*

Unbeknownst to me, Blender has a built-in video sequence editor (VSE). This is cool, because Blender is available on every major platform, is open source, and is under active development.

Clicking **Video Editor** in the Blender splash screen will take you to a timeline where you can do basic video editing. It's a bit different from other packages I've used, but I've cut together and exported a few things for work on it, and have been pleasantly surprised.

To enable the **Power Sequencer** which will give you more timeline features:

1. Go to the **Edit** menu → **Preferences**
2. Choose **Add-ons** from sidebar
3. Click the **Community** tab
4. Search for **Power Sequencer**
5. Click the checkbox next to **Sequencer: Power Sequencer**

One thing Blender can't do, is a phrase with five words.
