---
title: "My new Sony NW-A55 Walkman! Also a review"
date: "2022-07-02T09:58:57+10:00"
abstract: "A fun, small, affordable, and beautifully-crafted media player that’s easy to transfer files to, and sounds excellent."
thumb: "https://rubenerd.com/files/2022/sony-nw-a55m-hand@1x.jpg"
year: "2022"
category: Hardware
tag:
- music
- reviews
- sony
location: Sydney
---
I'm now the proud owner of a *new* Sony Walkman. Yes, they're still a thing, and I couldn't be more chuffed! The photo below shows us [Staying Warm](https://esthergolton.bandcamp.com/album/stay-warm) out on the balcony on this brisk winter afternoon.

This post is equal parts review, and a broader discussion about using [dedicated music players](https://rubenerd.com/portable-digital-audio-players-still-exist/) in a world of mediocre smartphone apps and streaming services. If you want the summary, this is an excellent little device!

<figure><p><img src="https://rubenerd.com/files/2022/sony-nw-a55m-hand@1x.jpg" alt="Photo showing the tiny A55 in my hand, playing Esther Golton's Stay Warm album" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/sony-nw-a55m-hand@1x.jpg 1x, https://rubenerd.com/files/2022/sony-nw-a55m-hand@2x.jpg 2x" /></p></figure>


### About the Walkman

The NW-A55 is part of Sony's [budget digital audio player lineup](https://www.sony.jp/walkman/lineup/). I bought it from [Sony Japan](https://www.sony.jp/walkman/products/NW-A50_series/) using a [proxy service](https://buyee.jp/), given they seem to be scarce as hen's teeth in the West. It worked out about AU $300 including shipping, not bad considering how expensive iPods and cassette Walkmans used to be.

*(You'll need to [enable the English firmware](https://www.rockbox.org/wiki/SonyNWDestTool) if you buy from Japan too, which I'll discuss in a future post along with [other sound profiles](https://www.nwmods.ml/p/sony-nw-a50series-custom-firmware.html) that are available. You're also taking a risk, given I'm sure warranties are only valid in Japan).*

It's a *gorgeous* piece of industrial design. The whole unit is smaller than a deck of playing cards, but feels heavy for its size, and the materials and finish feel premium, just like consumer electronics used to. I bought the gold one on a lark, which is even more subtle in person than in the press photos. The icons in the UI are even colour matched to it.

You'll often see the A50-series compared to the newer [A100](https://www.sony.jp/walkman/products/NW-A100_series/), which is Sony's Android-powered music player. People I respect like [Mat from Techmoan](https://www.youtube.com/watch?v=Rwal4qYWgK8) claim the audio quality of the A100 is a bit better, and you can install streaming apps on it. But I only want to play local media, and I don't really like Android. The A50 also (reportedly) has better battery life, though the gap narrowed after an A100 software update. It might still be worth checking out the A100 too if you want Wi-Fi and app support.


### Listening to music

I'm not an audiophile, and I'm sure said people would blanch if they [read about the files I play](https://rubenerd.com/choosing-audio-codes/ "Choosing audio codecs"). But the audio quality is a *noticeable* upgrade from my iPhone 8. It sounds so much clearer, especially in the midrange. Its EQ settings also add extra sparkle and pop, for want of better words! It's the biggest uplift in sound I've experienced since I bought AKG monitors for home. I'm tempted to try using it as an external USB [DAC](https://en.wikipedia.org/wiki/Digital-to-analog_converter#Audio "Wikipedia article on Digital to Analogue converters") now, which it supports.

It has NFC for pairing with wireless headphones, but it also has a headphone jack *so you can choose.* Shock of horrors! Remember when smartphone manufacturers justifiably mocked Apple for no longer taking music seriously, then they removed the jack too? Not that I have strong opinions about hubris or anything.


### Interface and hardware

The software on this Walkman gets as close as any player I've used at replicating the experience of a classic iPod. Touchscreens will never be as good as a fully-tactile interface like a click wheel, but the menus are blissfully simple to navigate. You drill down to what you want to listen to, then swipe up to change equaliser settings and fun modes like vinyl. You can customise the home screen to present what you want; I have albums and artists.

Once you've selected what you want to play, you can pocket the player and use its physical side controls for play/pause, stop, back, forward, and hold, just as you would on a cassette or Minidisc Walkman. *It even beeps like one when you change tracks!* This isn't just an MP3 player with a Walkman badge slapped on it, the whole experience has been faithfully recreated.

I can't describe what a breath of fresh air this is compared to how bad modern smartphone music and streaming apps have become. It's also nice having a dedicated player that isn't inundated with notifications and other modern distractions. Separating out ebooks to a dedicated tablet worked the same wonders for my anxiety.

<figure><p><img src="https://rubenerd.com/files/2022/sony-nw-a55m-iphone8@1x.jpg" alt="Photo comparing the size of A55 atop my iPhone 8. The A55 is shorter and narrower than the iPhone 8's display, and a bit thicker than an iPhone" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/sony-nw-a55m-iphone8@1x.jpg 1x, https://rubenerd.com/files/2022/sony-nw-a55m-iphone8@2x.jpg 2x" /></p></figure>


### Transferring media

The other reason I was interested in this device over a classic iPod was the flexibility in media management. My A55 comes with 12 GiB of usable capacity, but it also has a microSD card slot for (effectively) limitless storage.

You don't need to use any special desktop software; you mount the Walkman as a USB storage device and transfer files. Wait... that's it? Yes!

The player indexes any music you add, but keeps the files in place. This lets me use rsync to regularly diff and copy new ripped CDs or downloaded tracks across, even on FreeBSD. It also decouples syncing from music organising, so no more finagling iTunes in Wine, or using the [garbage new macOS Music.app](https://www.iphoneincanada.ca/news/apple-music-on-mac-is-an-utter-embarrassment-for-apple/). As a (diagnosed) OCD suffer, this *literally* makes me happier than it should.

I don't use playlists, so I haven't investigated how they'd work. You might want to research this first if that's important to you.


### Wrinkles

In the [words of Ol' Blue Eyes](https://www.youtube.com/watch?v=qQzdAsjWGPg "YouTube: Frank Sinatra, My Way"), I have a few, but (almost) too few to mention.

The biggest is the proprietary Sony connector, which the A50 series inherited from the A40 and A30. Newer players in Sony's lineup feature USB-C, but this requires its own syncing/charging cable. This is fine at home, but it'd one more thing to track when travelling. The good news is that I have a power brick with multiple USB ports I can connect it to.

As I mentioned previously, I've seen some reviewers claim the A50-series doesn't *quite* have the same fidelity and audio quality as the A100, and is decidedly mediocre compared to Sony's NW-WM1ZM2 at more than AU $4,000 dollars. It's true, and I don't know what I'm missing, but I think this sounds great.

I also wish there were a faster way to adjust the volume with the physical buttons. They provide very granular control, but it means I'm pressing on it for several seconds before I get what I'm after.


### Conclusion

I won't pretend this is a device for everyone. I happen to have strong opinions about the evolving state of the music industry, from predatory streaming services to the frankly embarrassing state of smartphone music applications. But if they work for you, that's fine (though I'd encourage you to still directly support musical acts you care about by buying their music, merchandise, and concert tickets).

If you're after a fun, small, affordable, and beautifully-crafted media player that's easy to transfer music to, sounds great, and has a touch of portable music nostalgia, I can't recommend this highly enough. I'm so happy devices like this are still being made.
