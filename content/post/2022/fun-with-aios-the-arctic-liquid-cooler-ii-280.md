---
title: "Fun with AIOs: The Arctic Liquid Freezer II 280"
date: "2022-05-08T18:38:32+1000"
abstract: "A longer post exploring my first liquid CPU cooler installation"
thumb: "https://rubenerd.com/files/2022/nr200p-arctic-rad@1x.jpg"
year: "2022"
category: Hardware
tag:
- arctic
- game-machine
- games
- fans
- noctua
location: Sydney
---
This is a bit of a longer post explaining my adventures installing and testing an all-in-one (AIO) liquid cooler in my current build. If you want the executive summary: *wow* it's good.

*I won't say it's cool, I won't say it's cool, I won't say... hey, Ruben, that AIO is pretty... it's pretty... cool right? Get it, because it's a... it's a... cooler! Damn it.*

### About AIOs

I've been rebuilding my game machine and personal tower for a few months now, and the time came this month to review the CPU cooler. The Ryzen 5600X comes with AMD's stock Wraith cooler, which is surprisingly decent at keeping the CPU below throttling temperatures. But it's loud at full speed, and the CPU still runs hotter than I'd prefer.

My instinct was to buy another trusty Noctua vapour chamber air cooler, such as the legendary [NH-U12A](https://noctua.at/en/nh-u12a) I have in my FreeBSD server. But I'd never used an all-in-one (AIO) cooler before, and thought it'd be fun... if utterly overkill for this budget CPU with only six cores.

AIOs are a great introduction to liquid cooling. They contain the radiator, tubing, pumps, coldplate, and fluid all pre-installed in a closed loop. You attach the coldplate to the CPU, then route the tubes to the radiator which you affix to the case.

Liquid has greater thermal density than air, meaning a correctly-installed and properly spec'd AIO can transfer heat more efficiently than air alone. Their far greater thermal mass compared to air coolers also lets them "soak" bursty workloads for longer before the fans need to speed up.

AIOs tend to be referred to by the cumulative size of their fans. For example, a 120 mm AIO has a single 120 mm fan, a 240 has two 120 mm fans, and a 280 has two 140 mm fans. Some AIOs can get as large as 360 and 420 mm.

<figure><p><img src="https://rubenerd.com/files/2022/arctic-liquid-freezer-ii@2x.jpg" alt="Press photo of the Arctic Liquid Freezer II 280, showing its 140 mm fans, coldplate, and tubes." style="width:496px; height:285px;" srcset="https://rubenerd.com/files/2022/arctic-liquid-freezer-ii@1x.jpg 1x, https://rubenerd.com/files/2022/arctic-liquid-freezer-ii@2x.jpg 2x" /></p></figure>

### Why I chose the Arctic

There are dozens of different AIOs on the market, but I went with the Arctic Liquid Freezer II 280 for a few reasons:

* The name is *so cool*. I know it's probably water and ethylene glycol, but it sounds like I'm introducing cryogenic fluid into my machine. This may have played more of a role in my purchasing decision than I care to admit.

* Online reviews have it consistency outperforming other AIOs of a similar size, both in cooling capacity and noise. Most of this is due to having the thickest radiator available, and Arctic's superior fans. The different pump design might also play a part.

* It has integrated cable management, and connects to the motherboard's CPU  fan header via a single cable. No need for fan controllers or messy wires! That said, Arctic still expose the fan headers on the device with cables, so you can replace or flip fan orientations.

* It doesn't have *das blinkin lights*, infinity mirrors, or other visual gimmicks that can't be easily disabled. I didn't want to pay extra for features I'll want to immediately cover up with tape.

* It doesn't require third-party controller software; it takes the PWM signal from the motherboard and that's it. My Asus BIOS lets me be granular with fan curves, so this works well.

* It's the largest AIO that feasibly fits (unmodified) into a Cooler Master NR200P Mini-ITX case. *Just!*


### Installation

For my first AIO, it was surprisingly easy to install. The Arctic box comes with a QR code that redirects you to [their instructions](https://support.arctic.de/lf2-280r4 "Liquid Freezer II 280 Rev 4"), along with a [video](https://www.youtube.com/watch?v=yiCtC-1hvD8 "ARCTIC Liquid Freezer II 240/280 Rev.4: Installation on AMD AM4") showing you the process, though be warned the video is missing the section where the brackets connect to the coldplate.

Unsurprisingly, the biggest challenge was routing the stiff cables within the tight confines of the NR200P case, and without pushing against the fans. I ended up with the tubes towards the front of the case, so I could route them down and around the radiator towards the CPU. I still recommend getting a metal fan grill for the fan pointing at the CPU just in case.

One thing I did do was mount the radiator one screw higher on the side panel to give the GPU below a bit more clearance. I'm also not happy with motherboard power connector to the power supply; I might get a longer one from Cablemod that lets me route it away from the AIO fan to give it more clearance.

Provided you have fans at the top for exhaust, channels like OptimumTech recommend the counter-intuitive approach of blowing air through the radiator and *into* the case. This way, the fans are pulling in fresh air, not hot air from the GPU mounted below. It was easy enough to detach and flip the fans to pull air rather than push.

Here's a top-down view of the case with the top fans and panel detached; note the sleeved tubing with the diagonal white stripes. Air flows fine, but the Arctic's thicker radiator really is the limit you'd probably want to put in this case! The smaller 240 mm version has the same radiator depth, so you're not saving much space going with that model.

<figure><p><img src="https://rubenerd.com/files/2022/nr200p-arctic-rad@1x.jpg" alt="Top-down photo of my NR200P case showing the radiator mounted to the side bracket opposite the motherboard, with fans pulling air through the radiator into the case." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/nr200p-arctic-rad@@1x.jpg 1x, https://rubenerd.com/files/2022/nr200p-arctic-rad@2x.jpg 2x" /></p></figure>

### Results

There's nothing surprising here, this cooler is amazing. Plenty of people have already done detailed reviews, and these are by no means scientific, so take with a mL of antifreeze from a day with 22 degree ambient temperature:

* **Idle from boot dropped from ≈55° to ≈25°**. I knew Ryzen booted warm, but that's wild (to use the technical definition).

* **Average loads went from ≈75° to ≈50°** in games like Minecraft and Train Simulator. Again, not especially taxing, but being under that magic 75 means the fans run quieter... and they're already barely audible.

* **Peaks dropped from ≈90° to ≈70°**. I've yet to stress this CPU hard enough to get it past this! This oversized AIO is *easily* able to keep up with the thermal loads of this lower-end 65W CPU.

I suspect those temperature deltas wouldn't be as stark if you were coming from something better than a stock cooler, and if you were using something closer to a 5950X or a 12th-gen Intel. But I'd consider this a success :).

My fear of the case heating up was also unfounded, and I got the same results with or without the dust filter which was encouraging. The AIO is able to wick away heat more efficiently and with much less noise than the stock cooler, and the top-mounted fans effortlessly circulate that warm air out of the case.

This experiment also validates Arctic's fan design. I've been a loyal Noctua customer for more than a decade for personal and work projects, but these Arctic fans are whisper quiet and move a ton of air. I'd consider getting a mix of both manufacturers for future projects to play with different pressure profiles.

Building and tinkering with computers are fun! I'm glad I'm rediscovering all of this again.

