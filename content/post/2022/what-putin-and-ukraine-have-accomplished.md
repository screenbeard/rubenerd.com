---
title: "What Ukraine and Putin have accomplished 🇺🇦"
date: "2022-02-27T08:33:57+11:00"
abstract: "Well done, you fuckwit."
year: "2022"
category: Thoughts
tag:
- politics
- ukraine
location: Sydney
---
[Eli Stokols](https://twitter.com/EliStokols/status/1497609700371832833) summarised just what Putin has done for the world:

> So far, Putin’s war, borne of his own insecurity and nihilism, has:
>
> • unified the West
> • unified Ukraine
> • drawn condemnation of even Orban (!)
> • exposed the weakness of his own army
> • cratered Russia’s crap economy
> • made Zelensky into a legend [though I'd argue he's done that himself]

I'd also say he's further galvanised the opposition in his own country, put the finances of his cronies in peril, exposed just how sycophantic the likes of the GOP and Farage were, and demonstrated that the world can agree on things sometimes. I hope to see more of this.

I don't know who started the joke, but at this rate NATO will want to join Ukraine. It's hard for me to communicate just how much respect I have for what they've accomplished and gone through already, and how resilient they've been in the face of overwhelming odds. If we're to believe the reports that captured Russian soldiers are also being given the opportunity to phone their parents back at home, they're also doing it with humanity.

Also, it cannot be overstated: fuck you Putin. Whatever's coming to you, you deserve more of it.
