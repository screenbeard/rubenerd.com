---
title: "Guess who paid off their student debt!"
date: "2022-08-13T09:06:20+10:00"
abstract: "Got confirmation from the ATO yesterday."
year: "2022"
category: Thoughts
tag:
- education
- studies
location: Sydney
---
Boom! Though I couldn't have done it without Clara, thank you :').

Australia has a government loan system where the Commonwealth pays for your tertiary education, and you pay it back once you reach an income threshold. You either pay it as part of tax, or you can have it withheld from your salary.

It's a flawed system; education should be free. Even the fiscally conservative acknowledge that qualified people tend to earn more and pay more tax, even if they don't buy altruistic arguments that everyone should be given an opportunity. Lower-income earners are also far more likely to spend whatever extra money they receive, so it's better for the economy not to be garnishing this.

The good news is, it's not counted as a normal loan under most circumstances, and you don't need to pay anything back if you fall on hard times. It's also indexed against inflation, rather than attracting a traditional interest rate. That distinction became laughably meaningless this year when it was indexed higher than actual personal loans would have been a year ago! But it's still better than traditional student loans.

I got an effective pay rise from this, which I'm going to use to invest in my future.
