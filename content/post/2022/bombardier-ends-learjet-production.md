---
title: "Bombardier ends Learjet production"
date: "2022-09-16T08:09:36+10:00"
abstract: "It was the first plane I flew... in Flight Simulator 98."
thumb: "https://rubenerd.com/files/2022/Bombardier_Learjet_45.jpg"
year: "2022"
category: Thoughts
tag:
- aviation
- engineering
- flight-simulator
location: Sydney
---
I didn't see this news last year, [as reported by CTV](https://montreal.ctvnews.ca/bombardier-announces-the-elimination-of-1-600-positions-and-end-of-the-learjet-1.5304555)\:

> Bombardier said it will end production of Learjet aircraft later this year, allowing it to focus on its more profitable Challenger and Global aircraft families.
>
> "[&hellip;] given the increasingly challenging market dynamics, we have made this difficult decision to end Learjet production," [Bombardier chief executive Eric Martel] said in a statement.
>
> Bombardier said it will continue to fully support the Learjet fleet and launched Thursday a remanufacturing program for Learjet 40 and Learjet 45 aircraft.

I resent private jets for their environmental and social impact, but I can still appreciate their engineering and history. I also have a soft-spot for the 45, becuase it was the first plane I flew and landed successfully in [Flight Simulator 98](https://microsoft-flight-simulator.fandom.com/wiki/Learjet_45). A rational person would have started on the [Cessna 182S](https://microsoft-flight-simulator.fandom.com/wiki/Cessna_182S_Skylane), but I was fascinated by the Learjet's cockpit and controls.

For those intererested in audio history (though apparently not spelling), the 8-track tape cartridge was also developed by Bill Lear of Learjet. Even the audio cassette was a bit before my time, but I'm tempted to get some 8-track carts one day for no good reason whatsoever.
