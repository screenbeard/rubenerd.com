---
title: "AMD Ryzen 7000"
date: "2022-09-04T10:59:53+10:00"
abstract: "The specs look great, though I note the uptick in TDP."
year: "2022"
category: Hardware
tag:
- cpus
- hardware
location: Sydney
---
AMD recently launched their new Ryzen 7000 series CPUs, which look awesome. [ServeTheHome](https://www.servethehome.com/amd-ryzen-7000-and-zen-4-launch/) had the best summary with screenshots, and [GamersNexus](https://www.youtube.com/watch?v=hVnJbiYOCq4) did a detailed video going over the specifications and expected US pricing.

This latest generation gets the new AM5 socket, which uses [LGA](https://en.wikipedia.org/wiki/Land_grid_array "Wikipedia: Land grid array") like modern Intel CPUs in lieu of having pins on the CPU itself. I wonder how long it'll take before unscrupulous American retailers start shipping boards with [bent LGA pins](https://www.youtube.com/watch?v=CL-eB_Bv5Ik "GamersNexus video about a US retailer sending them a damaged board")?

The new generation also supports DDR5 memory, and PCIe generation 5. DDR5 remains stubbornly expensive and difficult to buy, and consumer GPUs still aren't even saturating PCIe4, but the latter would be useful for high-speed storage.

I don't have much more to add than what the those outlets discussed, though I do note the [TDP](https://en.wikipedia.org/wiki/Thermal_design_power "Wikipedia: Thermal design power") has also gone up. AMD claim these chips will have improved performance-per-watt over past Ryzen chips, though I'm surprised even the entry level silicon will be more power hungry than the last AM4 generation. Couple that with Nvidia's projected massive power draw for their 4000-series GPUs, and we're in for some fun... especially with current power prices.

Thermals and power are really where ARM continues to kick proverbial arse. I will continue buying AMD kit for home game machines and workstations, but MacBooks and mobile phones operate at a whole other level.
