---
title: "The quest for high-resolution album art"
date: "2022-08-18T09:52:16+1000"
abstract: "For playing on my new Walkman, and to make old tunes look awesome!"
year: "2022"
category: Media
tag:
- music
- walkman
location: Sydney
---
Welcome to the latest installment in my unintentional series talking about curating a local music library. Using these in lieu of streaming services has become a bit of a theme here of late. It's as if I have a strong opinion about it!

Having manually fixed all the metadata in my (re)imported music, my attention has been directed at cover art. I tend to play music on the desktop via the console [musikcube](https://musikcube.com/) application, but I love seeing the covers on my [Walkman](https://rubenerd.com/my-new-sony-nw-a55-walkman/) when walking around.

*(For the trolls out there, you don't need to buy a modern Walkman if you think it's a waste of money. It's pretty cool that you have the agency to make that decision, when you think about it)!*

<figure><p><img src="https://rubenerd.com/files/2022/sony-nw-a55m-hand@1x.jpg" alt="Photo showing the tiny A55 in my hand, playing Esther Golton's Stay Warm album" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/sony-nw-a55m-hand@1x.jpg 1x, https://rubenerd.com/files/2022/sony-nw-a55m-hand@2x.jpg 2x" /></p></figure>

It surprised me how few of my tracks had embedded covers. In the iTunes of yore, you could request the covers be downloaded automatically, but unbeknownst to me at the time they were stored in separate folders and not embedded in the tracks themselves. This means they weren't carried along with the tracks, and opening songs in other applications presented nothing.

I can see why they might have wanted to do this. Most cover art embedded in ID3 tags were fairly low-resolution, which would have looked awful on Apple's then-new HiDPI/Retina displays. Having these in a separate directory structure meant they could still be viewed and synced to your iDevice, but without hugely inflating the size of your music files.

This doesn't seem as much of an issue thesedays. Storage is cheap, and the idea of increasing the size of an audio file (audiophile?) by a quarter to include high-resolution art doesn't seem so ridiculous. Or at least... to me! Music is art, and while some might consider the exercise frivolous, I think anything that improves the subjective experience of listening to music is worth it, especially if we're collecting local music to emulate what it was like to own physical albums.

I've been going through all my albums one by one, searching for them online, downloading as big a piece of cover art as I can, and embedding them. Some are easy, others have been extremely difficult to impossible, but reverse-image searches are my friend. Cantonese stuff by Andy Lau, and some of the African albums I love from my dad have proven the most challenging, at least at the resolutions I've been getting other albums at.

The other benefit is that you can embed covers from other releases you prefer, or even different ones entirely! I found a database of old Beatles photos taken during shoots for their various albums, so now my copy of *Abbey Road* has the gents  waving, and the cover for *Rubber Soul* has them smiling. My two favourite Marian Call albums also have scanned art that she signed with gold pen with my name from the original Whole Wheat Radio days, which looks *awesome!*

I can see why rational people would decry such an exercise as a waste of time, but music means a lot to me, and giving these tracks a fresh lick of paint has been rewarding in a way I didn't expect.

