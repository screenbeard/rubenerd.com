---
title: "The vaccines cause autism lie"
date: "2022-03-30T09:07:25+11:00"
abstract: "It’s up to us to make sure people don’t forget this. "
year: "2022"
category: Thoughts
tag:
- ethics
- health
location: Sydney
---
I was in a waiting room for a while last week, so I finally got around to watching [Brian Deer's 2004 documentary on Andrew Wakefield](https://www.youtube.com/watch?v=7UbL8opM6TM), the former English doctor who claimed vaccines cause autism. Wakefield's 1998 press conference spurred a wave of needless controversy and press coverage, and gave ammunition to anti-vaxers that persists, and continues to kill people, to this day.

I'd always wondered where this nonsense came from, ever since I heard about it on a podcast I used to listen to. I had no idea it was so transparent and blatant, and anyone who still subscribes to it now is grossly misinformed (deliberately or otherwise).

Brian Deer uncovered that Wakefield had a legal and financial incentive in an alternative mumps, measles, and rubella (MMR) vaccine in the late 1990s. Wakefield ignored findings from his own lab that contradicted his thesis that measles in the gut caused autism. Wakefields's co-author of the retracted Lancet paper claimed he could cure autism with his bone marrow, and his tenuous conclusions were based on a sample size of a dozen children, some of whom *didn't have autism*.

*(Ducks quack too, but they don't pass themselves off as medical professionals. At least, none that I'm aware of. Not even [Subaru-chan](https://www.youtube.com/channel/UCvzGlP9oQwU--Y0r9id_jnA) would admit to being a doctor... shuba shuba 🦆).*

Wakefield is no longer permitted to practice medicine in the UK, and has relegated himself to being a conspiracy theorist in the States. This seems to be the standard career trajectory for people of his ill... pardon, ilk, when academia and the medical community have exposed your shenanigans.

But as I eluded to in the opening, this is moot given the damage has already been done. The irony is, Wakefield wasn't anti-vax, he had a vested interest in people taking a different one. This is one of the ways that he disingenuously passes himself off as an underdog taking on the establishment to sympathetic crowds. People eat this stuff up, especially when it conforms to their worldview.

It's up to us to make sure people don't forget any of this. Lives are at stake.
