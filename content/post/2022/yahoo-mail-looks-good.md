---
title: "Yahoo Mail looks… good!"
date: "2022-08-02T09:03:12+1000"
abstract: "I logged in after a few years, and the UI is way better than the likes of Gmail."
year: "2022"
category: Internet
tag:
- mail
- reviews
- yahoo
location: Sydney
---
I have mixed feelings about Yahoo Mail. It was fine when I had to use it in a previous job in Singapore, but they long lagged behind other providers in capacity and [offering HTTPS support](https://rubenerd.com/yahoo-mail-finally-gets-ssl/). SSL/TLS wasn't as ubiquitous then as it is now, but I still thought it was poor form for something so critical.

Anyway, fast forward a decade later and I got a notification this morning that I had to log in to keep my account active. On a lark, I went to check out how it'd changed in the intervening years.

The UI is... **good**. It's been steamrolled into flatness like most modern interfaces, but it's otherwise laid out well. I could grok the toolbars, icons, and compose window within seconds. In an age where most web designers conflate minimalism with simplicity, I thought it was worth calling out.

I still advocate for registering your own domain so you can swap providers, and using a paid email service like Fastmail ([referral link here](https://ref.fm/u12044749)!) so your communications aren't the product. I also haven't vetted what Yahoo does with their mail. But strictly on the basis of interface, I'd say Yahoo Mail is one of the better ones today.
