---
title: "The wonderful tee(1) command"
date: "2022-10-15T09:40:49+11:00"
abstract: "You don’t need to use cat after a command to show redirected output, use this tool instead."
year: "2022"
category: Software
tag:
- bsd
- freebsd
- guides
- linux
- netbsd
- things-you-already-know-unless-you-dont
location: Sydney
---
In today's installment of [things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/), I'm visiting the stupendously useful **[tee(1)](https://www.freebsd.org/cgi/man.cgi?query=tee)** command. I use it daily, yet I see plenty of scripts that brute force alternatives to it. It's one of the most common superfluous uses of **cat(1)** I see.

**tee(1)** duplicates standard output, letting you preview output and write it to a file. Why would you want to do this? Say you want to capture the output of a fancy script by redirecting it to a file:

	$ ./reuschling.sh > output.log

What did that script do? We wouldn't know, unless we viewed the log file. To show us the output *and* write to a file, we can pipe the script output to **tee(1)**, which will print the output and write to a target file:

	$ ./reuschling.sh | tee output.log
	==> this is the output of a fancy script

If we inspect the file:

	$ cat output.log
	==> this is the output of a fancy script

Nice! You can also append to an existing file with **-a**, the same way you'd redirect with **\>\>** in a script:

	$ ./reuschling.sh >> output.log
	$ ./reuschling.sh | tee -a output.log
	==> this is the output of a fancy script

And checking it again having run those two lines:

	$ cat reuschling.log
	==> this is the output of a fancy script
	==> this is the output of a fancy script
	==> this is the output of a fancy script

Even the name of this tool is cool. If you picture a letter T and follow the vertical stroke upwards, it branches into two... clever! Or if you stitched the top of a tee-shirt shut, then started filling the bottom with whipped cream, eventually it'll come out of the arms. 

