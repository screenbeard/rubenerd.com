---
title: "Music Monday: McCartney III"
date: "2022-06-06T07:15:56+1000"
abstract: "Loved this new release!"
thumb: "https://rubenerd.com/files/2022/yt-8MnHkXcqvJ8@1x.jpg"
year: "2022"
category: Media
tag:
- the-beatles
- music
- music-monday
- paul-mccartney
location: Sydney
---
I was a Beatles fan growing up, and Clara and I have been getting back into them of late. We've bought my favourite *Abbey Road*, *Revolver*, and their *Magical Mystery Tour* albums on vinyl, and are getting the rest on CD. Have you noticed some acts have even stopped selling *those?* That's a topic for another time.

Today's [Music Monday](https://rubenerd.com/tag/music-monday/) is a review of a new album we picked up that I've rather enjoyed. As with *McCartney* and *McCartney II*, all the instrumentation, recording, and mixing was done by the man himself. It gives it a more realistic and less produced sound, and earned Clara's admiration for his ability to play more than one instrument!

Winter Bird/When Winter Comes is our favourite:

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=8MnHkXcqvJ8" title="Play Paul McCartney - Winter Bird / When Winter Comes (Lyric Video)"><img src="https://rubenerd.com/files/2022/yt-8MnHkXcqvJ8@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-8MnHkXcqvJ8@1x.jpg 1x, https://rubenerd.com/files/2022/yt-8MnHkXcqvJ8@2x.jpg 2x" alt="Play Paul McCartney - Winter Bird / When Winter Comes (Lyric Video)" style="width:500px;height:281px;" /></a></p>

This production method was panned when his original 1970 album came out, though I suppose one can't divorce it from the circumstances surrounding its release. John had already quietly quit by the time *Let It Be* was released, but Paul broke the news. The timing of his first solo album, and the perceived lack of polish compared to the exquisite mastering of The Beatles left people with a sour taste of Apple. I can empathise, but I still loved it.

*(It's also interesting to compare to the others, especially George who's Cloud Nine album is one of my all-time favourites, Ringo who's solo output I feel is underappreciated for how much fun it is, and John who's music I admit to being mostly apathetic about. McCartney's solo work has a completely different vibe and attitude to The Beatles, which some have found difficult to swallow).*

Today, there's an entire culture around "lo-fi" music, and what Clara and I call "chill". It's probably why we love folk music; its good for the soul *and* anxiety. *McCartney* fits this bill, and so does McCartney III; though this album also has some great blues and rock-inspired tunes on side A.

This post was a bit all over the place, but if you have even a passing interest in these four gents from Liverpool, give McCartney III a listen. It's made it on our regular rotation now in the evenings when we disconnect from the Internet and think of other things.
