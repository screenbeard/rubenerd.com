---
title: "Brand loyalty isn’t always irrational"
date: "2022-12-23T16:08:45+11:00"
abstract: "It can be a helpful indicator based on your past experience."
year: "2022"
category: Thoughts
tag:
- ethics
- shopping
location: Sydney
---
I want to push back a little on the idea that brand loyalty blinkers you from making good decisions (I can already feel the collective hackles raised from the likes of the Orange Peanut Gallery from here)!

Brands are owned by companies, and companies are run by people. People have values, behaviour, and history. You're not being rational by discounting these; you're falling into the trap of ignoring the human factor.

It's reasonable to favour a brand if they've treated you well before. You might be familiar with their processes, have an account with them, or have existing devices that work well together. Their build quality might have impressed you, or their after-sales support. You might have friends, family, or colleagues familiar with them. Their design aesthetic, ergonomics, or functionality might appeal to you, or they *click* with how your mind works. These qualitative metrics play a role in any purchasing decision; even from those who claim absolute objectivity in comment sections or forums.

In *The Paradox of Choice* (one of my favourite books of all time), psychologist Barry Schwartz argues that the number of choices the modern world bombards us with can make decisions difficult. I'd argue that a brand, with which you have a history, can be one helpful indicator to navigate this.

Assuming your loyalty has been earned&mdash;as opposed to blindly following a trend or crowd&mdash;I see no problem with it. Because it's also theirs to lose.

