---
title: "Voting in the Australian elections by post"
date: "2022-05-15T09:23:34+1000"
abstract: "Thanks to the AEC for making this so easy."
thumb: "https://rubenerd.com/files/2022/postal-vote@1x.jpg"
year: "2022"
category: Thoughts
tag:
- australia
- elections
location: Sydney
---
Clara and I just sent in our votes. We may not be in the electorate on voting day, so we were approved for a postal ballot. Voting is compulsory in Australia, though even if it weren't, its still a civic responsibility.

<figure><p><img src="https://rubenerd.com/files/2022/postal-vote@1x.jpg" alt="Photo showing the postal vote envelopes and some of the material that came with them." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/postal-vote@1x.jpg 1x, https://rubenerd.com/files/2022/postal-vote@2x.jpg 2x" /></p></figure>

My kudos to the AEC for making this so easy. In past years I've voted early, but this was even better. About the only difficulty was refolding that metre-long senate paper!

