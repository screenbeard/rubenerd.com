---
title: "Fixing a Comet server after misconfiguring S3"
date: "2022-10-12T08:48:50+11:00"
abstract: "I had a trailing forward-slash in my URL endpoint."
year: "2022"
category: Software
tag:
- backups
- orionvm
- troubleshooting
location: Sydney
---
I was configuring an S3 backup target for Comet backup server on Monday, like a gentleman, when the page went blank. I restarted the server and checked its status:

	$ sudo service cometd status

And got this result:

	cometd.service: Main process exited, code=exited, status=1/FAILURE
	cometd.service: Failed with result 'exit-code'.
	cometd.service: Scheduled restart job, restart counter is at 5.
	Stopped Comet Server
	cometd.service: Start request reported too quickly
	cometd.service: Failed with result 'exit-code'
	cometd.service: Failed to start Comet Server.
	
Welp! Something I changed must have done something. I tailed the logs and restarted:

	$ cd /var/log/cometd/
	$ tail -F *log

And found the issue:

	Couldn't connect to storage: Connect: Endpoint: $DOMAIN/ does not \
	follow ip address or domain name standards.

Comet doesn't seem to like trailing slashes. Maybe I need to put in a feature request to detect them.

You can fix it checking the config file:

	# vi /etc/cometd/cometd.cfg

And removing the trailing forwardslash:

	"S3Server": "$DOMAIN"

Now you can restart the server, and it'll work.
