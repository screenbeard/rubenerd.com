---
title: "Quest for a thocky split keyboard like my Topre"
date: "2022-06-15T16:42:24+1000"
abstract: "Gateron KS-3 Milky Yellow Pros, or NovelKey Creams?"
year: "2022"
category: Hardware
tag:
- ergonomics
- keyboards
- topre
location: Sydney
---
Last year I talked about going back to a [split Microsoft ergonomic keyboard](https://rubenerd.com/the-microsoft-ergonomic-keyborad-2019/), because I felt some slight wrist pain. Overall is an item of clothing, and I was impressed with the build quality, especially for a cheap, mass-produced rubber dome. I found the position of my arms and wrists while typing on this to be more natural and less strained, especially during long technical writing sessions.

But then I made the mistake of bringing out my beautiful little Leopold FC660C with Japanese Topre switches again. Clara and I bought this in Osaka in 2020, and it's simply perfection. Words can't describe how much fun and joy it brings me. I'm grinning as I type this!

I've tried almost every permutation of Alps and Cherry switches, and have an IBM Model M clone with buckling springs stashed away for when I get a sound proof room in which to use it! But no switch I've used before, or since, match the supreme *thock* feel and sound of Topres. Going back to the office and using my dusty Cherry MX Brown board felt like I was going from welded to riveted rails, with all the train rattling and other metaphors. I've been spoiled.

I wouldn't profess to be a keyboard expert *at all*, but I know just enough to be dangerous. Worse, I now know what I like, so I want to see if I can get something close to that in a split-keyboard form factor.

My dream would be to have an indie split enclosure and PCB, with switches that come close (enough) to Topres. I prefer the feel of linear switches if they have a nice base to bottom out on. Prelubed (heavens) would be preferable for a noob like me, but I wouldn't be against learning how to do that properly.

A few of you recommended I check out Gateron KS-3 Milky Yellow Pros, or the NovelKey Creams. The latter seemed to be the darling of the tech world a few years ago, though I've since read and watched reviews of it being a bit scratchy. Clearly some more research is needed.
