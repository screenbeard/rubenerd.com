---
title: "aria2 can download torrent files, then their files"
date: "2022-09-20T08:06:10+10:00"
abstract: "Pass it the file, and it downloads the associated files too. That’s cool!"
year: "2022"
category: Software
tag:
- guides
- things-you-already-know-unless-you-dont
- torrents
location: Sydney
---
In this installment of [things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont/), you can provide Tatsuhiro Tsujikawa's [aria2](https://aria2.github.io/) with a URL to a torrent file, and it will also download the files from the torrent:

For example, I can download NetBSD 9.2 by giving it the torrent file, instead of having to download it first:

	$ aria2 https://cdn.netbsd.org/pub/NetBSD/images/9.2/NetBSD-9.2-sparc.iso.torrent

I now have an ISO, not just a torrent file. Cool!
