---
title: "Expertise is expensive, conspiracies aren’t"
date: "2022-05-15T09:57:19+10:00"
abstract: "Conspiracy theories are spread for free, which fits the business model of social media companies and search engines."
year: "2022"
category: Thoughts
tag:
- journalism
location: Sydney
---
I've been preoccupied again thinking about these:

* Good journalism costs money, and journalists need to get paid.

* Conspiracy theories, <span style="text-decoration:line-through">misinformation</span> lies, and junk science can be (and are) generated and posted for free.

* Such nonsense is easily manipulated by people with ulterior motives, and is corrosive to the health of our societies and democracies.

* The primary business model of search engines and social media is to monetise free content.

* Paywalls are the digital equivalent of buying a newspaper, but they run against the above business model, and therefore isn't the content that gets organically or algorithmically disseminated.

I was of the firm belief that a technical solution existed for this, waiting to be discovered and implemented. But as with *so* many things, the real issue is business models, incentives, and structure. Those are meatspace issues.

