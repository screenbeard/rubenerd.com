---
title: "Beeping fire alarms taught me about irritations"
date: "2022-03-11T17:30:26+11:00"
abstract: "All our “papercut problems” are unique, both in terms of what we’ll tolerate, and from what."
year: "2022"
category: Thoughts
tag:
- papercut-problems
- philosophy
location: Sydney
---
I've talked before about *papercut problems*, or those that are irritating, but not enough to waste effort fixing. I used the example of a [dodgy Ethernet dongle](https://rubenerd.com/ethernet-papercuts/) that caused me problems, but it wasn't until it reached a certain threshold did I replace it, and realised just how much better my life could be.

Walking down the common hallway in our builidng lead me to realise that all our papercut problems are unique, both in terms of what we'll tolerate, and from what.

At least two apartments on our floor (possibly three, it's hard to tell) have smoke alarms with low batteries. You can clearly hear them beeping as you walk past, indicating that their batteries are due for replacement. The sounds are deliberately irritating and impossible to ignore, for safety reasons.

Or at least, *you'd think so!* Assuming those apartments aren't vacant, their occupants either *don't* find those shrill beeps annoying, or have relegated them to *papercut problem* territory. They could replace the batteries, in other words, but it'd takes too much effort.

This demonstrates to me we all have different tolerances for such things. Which lead me down the rabbit-hole of thinking whether half the stuff that bothers me is worth being bothered about, if others aren't. An ideal world would be free of irritating problems, but pragmatically I'd be doing myself a favour by tapping into that coping mechanism and dismissing it as so many others seem to be able to.

Though I'd say it's still worth replacing smoke alarm batteries.
