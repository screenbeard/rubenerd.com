---
title: "Boeing finishes its last 747"
date: "2022-12-09T08:30:11+11:00"
abstract: "What a run, huh? Wait, flight."
thumb: "https://rubenerd.com/files/2022/sia-747-thumb.jpg"
year: "2022"
category: Travel
tag:
- aviation
- boeing
- boeing-747
- design
- engineering
- history
location: Sydney
---
You know something has reached iconic status when laypeople have heard of it. Many people still recognise the *Commodore 64* name, even if all they know is its a old computer. Everyone has also heard of the *747* or the *Jumbo Jet* since they barrel-rolled into the aviation world in the 1960s... and Boeing just built their last one.

Our flight to move to Singapore was in the "hump" of an SIA 747. All our childhood holiday to Bali and Lombok started in the *Queen of the Skies*. Its one of those planes you expected to always be there.

<figure><p><img src="https://rubenerd.com/files/2022/sia-747@1x.jpg" alt="" srcset="https://rubenerd.com/files/2022/sia-747@2x.jpg 2x" /></p><p style="font-style:italic;"><caption>Photo by <a href="https://commons.wikimedia.org/wiki/File:SIA_Boeing_747-400,_9V-SPF,_SIN_5.jpg">Terence Ong</a> on Wikimedia Commons.</p></figure>

If Jack Tramiel worked at Boeing, he'd have said the 747 introduced commercial aviation to the masses, not the classes. Its size was designed to permit swift conversion to cargo operations in anticipation of supersonic passenger transports, but turns out building a massive plane also reduces the per-set cost. If ocean liners introduced the burgeoning middle class to tourism and exploring the world, the 747 made it accessible; and in doing so, made the world a bit smaller.

The A380 took up the passenger capacity baton upon its introduction, but if anything its bulbous&mdash;if efficient&mdash;design demonstrated what a sleek airframe the 747 really is. The fact it was designed in the 1960s, only half a decade after the *Wright Flyer*, feels absurd. It would have been incredible working in those engineering departments, with that heady optimism for the future.

Most passenger operators retired their 747 fleets a decade ago or earlier, either for the double-deck A380, or usually for smaller twin-engine aircraft to operate point-to-point services. But now we're likely to see it fulfil its destiny as a freighter for many more years.

What a run, huh? Wait, flight.
