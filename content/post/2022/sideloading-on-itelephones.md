---
title: "Sideloading on iTelephones"
date: "2022-12-20T09:10:17+11:00"
abstract: "Bruce Schneier’s comments to the US Senate Judiciary Committee."
year: "2022"
category: Software
tag:
- apple
- bruce-schneier
- iphone
- privacy
- security
location: Sydney
---
I haven't felt strongly about sideloading software on phones either way; maybe I should have. But [Bruce Schneier's latest submission](https://www.eff.org/files/2022/01/31/schneier_letter_to_senate_judiciary_re_app_stores.pdf) to the US Senate Judiciary Committee makes a compelling case for granting third party access.

This is from paragraph three on page four:

> Giving tech companies a veto over which software users can and can’t trust is a system that fails badly. That is: it’s one thing to seek a company’s recommendations about what constitutes a security risk, and another to let that company’s judgement override your own. The former requires that the company be reliable, the latter requires that the company be infallible.

