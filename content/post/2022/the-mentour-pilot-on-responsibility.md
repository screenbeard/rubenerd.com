---
title: "The Mentour Pilot on responsibility"
date: "2022-09-04T09:04:55+10:00"
abstract: "His reaction to a YouTuber who crashed his plane on purpose is important for everyone. You have a responsibility when you publish your words and actions."
year: "2022"
category: Media
tag:
- ethics
- mentour-pilot
- safety
- youtube
location: Sydney
---
Petter produces my [favourite aviation videos on YouTube](https://www.youtube.com/c/MentourPilotaviation). He's sincere, thorough, avoids sensationalism, and takes the time to explore human and procedural factors when discussing everything from incidents to aircraft design. He's also just really engaging and fun to watch, and has been responsible for getting me back into playing flight sims again.

Clara and I have been recently going through his [Mentour Now](https://www.youtube.com/c/MentourNow) spinoff channel, and just watched his response to a YouTuber who deliberately crashed his plane for clicks. Now there's a phrase I never thought I'd write.

[Petter concluded](https://www.youtube.com/watch?v=5Eh4ttX8rFU), emphasis his:

> The aviation industry is serious. It's built on the premise of safety, and that everyone involved thinks about safety as their **primary priority**. And this is definitely not what [he] has done here.
> 
> As an aviation content creator, you have to be aware of the kind of message you're sending. I am very aware that hundreds of thousands of people are going to see what I'm saying. So I have to make sure what I'm saying and doing is well researched, and that it has a **positive impact** on my viewers. It has to be positive, constructive, and it has to forward safety. 
>
> [He's] set a really bad example, for people [in aviation], and the general public who might get the complete wrong perception about what we're supposed to do as pilots in emergency situations.
>
> From that perspective, I feel bad for all the great aviation YouTubers out there who spend their days creating good, constructive, instructive content. Their life is likely going to become harder now because of his actions.

Food scientist [Ann Riordan has voiced similar sentiment](https://rubenerd.com/ann-reardon-on-viral-fake-food-videos/) about responsibility, the potential for serious harm, and how bad-faith actors make the lives of honest people that much harder.

I'm relieved to see more people using their platforms to discuss these issues.
