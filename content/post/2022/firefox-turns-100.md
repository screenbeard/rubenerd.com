---
title: "Firefox turns 100"
date: "2022-05-13T07:48:12+1000"
abstract: "It’s a big milestone, but it’s hard not to see the external and self-imposed headwinds."
year: "2022"
category: Software
tag:
- firefox
location: Sydney
---
Granted its version number stopped being meaningful to most people after they adopted Chrome's numbering scheme, but it's still a milestone worth celebrating. Yay, Firefox! But it's also hard to avoid the headwinds the browser faces, with dwindling marketshare compounded by mismanagement and a seeming lack of focus and direction.

Firefox is the last holdout from Webkit/Blink/etc, and its vanguard role in ensuring an open and healthy web cannot be understated. Anyone who relishes its diminished position, downplays current issues, or ignores it entirely has a short attention span and an unwillingness to learn from history.

*(I wrote about [The Second Browser Monoculture](https://rubenerd.com/firefoxs-situation-reminds-me-of-openssl/) back in 2020, including how surreal it felt to be having the same arguments again).*

I've been a Firefox user (barring a few adventures with Mozilla's Camino and SeaMonkey browsers) since the original Phoenix days in 2004. Those were dark days of the web, and Firefox was a breath of fresh air. The Mozilla Suite had all these other tools inherited from Netscape, but Firefox was simple and fast. People flocked to the browser for its tabbed interface, popup blockers, plugin support, and because it was something new after years of IE stagnation.

If you think about it, Chrome won for similar reasons. It was a complicated piece of software, but was sold with a simple interface and promises to include better integrations with tools people care about. Anyone who says pretty isn't a feature doesn't understand human psychology.

It's detractors today claim Firefox stands no chance of usurping or challenging the functional Chrome monopoly it in any meaningful way. It's funny, they said the same thing the first time, and were proven wrong. It might be able to do it again.
