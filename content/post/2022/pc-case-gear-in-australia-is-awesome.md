---
title: "Review: @PCCaseGear in Australia is awesome"
date: "2022-02-25T09:17:17+11:00"
abstract: "Trying to get better at leaving positive reviews too."
year: "2022"
category: Hardware
tag:
- australia
- reviews
location: Sydney
---
I've written before about how we're always quick to write about a negative experience, but we don't take the time to talk about stuff we appreciate, from restaurants to IT support queues. I want to get better at this.

Earlier this week I received another batch of parts from [PC Case Gear](https://www.pccasegear.com/) for my Compaq sleeper PC build. It was packed *very* well, arrived quickly, and came with a treat!

Having experienced a different company with their months-long returns policy, bad packing, and completely opaque communication, it's important to take care of retailers who take care of us. They're now my default unless they're out of stock or don't have a part I need.

Give them a look if you're building a computer in Australia.
