---
title: "Rebecca, Michael recommend the Gateron Black Ink V2 keyswitches"
date: "2022-06-16T11:56:58+1000"
abstract: "Now I'm tossing up between these and the Creams."
year: "2022"
category: Hardware
tag:
- keyboards
location: Sydney
---
Rebecca Hales was quick on the draw in response to my [thocky keyboard post](https://rubenerd.com/quest-for-a-thocky-split-keyboard/), a phrase I swear is real.

> GATERON BLACK INK V2s! But the Creams sound better. ARE YOU SWIMMING AGAIN YET?

I'm not much yet, but the [site mascot Rubi](https://rubenerd.com/about/#mascot) did get an update to her bio recently to say she was inventing a line of self-heating swimsuits and towels that were inspired by an... adventure. But I digress. 🥶

Michael B. recommended the same keyswitch, though he said the Creams were better if I'm still not into RGB.

The consensus seems to be that the Gateron Milky Yellows are the smoothest linear switches on a budget, the Creams are sublime if you can tolerate the break-in period to wear down their scratchiness, and the Gateron Black Ink V2 is the smoothest out of the box, but you pay for the privilege.

This is what I meant in the previous post about only knowing enough to be dangerous. I've never lubricated a keyboard before, and I didn't even know key films were a thing until yesterday. Why do I get the feeling I'm piloting myself into another expensive obsession, right after paying off a huge chunk of HECS to avoid the huge spike in indexation?
