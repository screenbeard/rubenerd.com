---
title: "Features every microwave should have"
date: "2022-08-12T16:59:58+10:00"
abstract: "Microwaves are brilliant inventions of brilliance. They could be even better."
year: "2022"
category: Hardware
tag:
- food
location: Sydney
---
Microwaves are brilliant inventions of brilliance. Food that would an hour to cook in an oven, or twenty minutes in an air fryer, can be rendered into seething, bubbling cauldrons of molten lava within the space of sixty seconds. But that doesn't mean they can't be improved upon.

Here are some technically simple, logical, and scientifically feasible additions we could add to these devices to vastly improve their utility:

* A stirrer for mixing food as it cooks. Not a spinning waveguide, a stirrer that contacts the food and mixes it as the comestible is irradiated. I'm sick of fishing out lumps of ice that could sink a ship alongside steam that could boil the water around a ship. That analogy made no sense.

* A device that senses the presence of metal, and immediately disengages the device with an audible warning.

* A window on top of the microwave instead of the side, to permit easier visual monitoring of foods that can rapidly boil over and cause a huge mess.

* A control to rapidly freeze food instead of cooking it.

* Funky translucent colours, like the original iMacs. If I'm going to clean up and spend time watching this thing, may as well do it in late-1990s style.

* The ability to immediately stop that infernal **BEEP BEEP BEEP** upon opening the door latch. If I'm hurriedly opening the door after the timer has stopped, it's almost certainly to avoid waking someone up. There is no point **LOUDLY** telling me to check the food... if I'm already checking the food.

* While we're on the subject of beeps, the ability to replace them with a MIDI ringtone. *The Spanish Flea* might get tiresome after a few times, but that's why you can program a bank of them for different days, seasons, and temperatures.

If you'd like to implement any of these features, [contact me](https://rubenerd.com/about/#contact) so we may discuss my commission.

