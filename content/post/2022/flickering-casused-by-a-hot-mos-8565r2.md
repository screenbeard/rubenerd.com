---
title: "Flickering caused by a hot MOS 8565R2"
date: "2022-12-24T08:34:29+11:00"
abstract: "The flickering disappeared completely when it cooled down; I might need a heatsink or two."
thumb: "https://rubenerd.com/files/2022/mos-8565r2@1x.jpg"
year: "2022"
category: Hardware
tag:
- commodore
- commodore-64
location: Sydney
---
[I mentioned on Mastodon](https://bsd.network/@rubenerd/109562762004114366) that while the chips in my Commodore 128 barely break a sweat most of the time, there are a few in the new Commodore 64C that definitely do. The SID gets warm to the touch, and the VIC-II gets *hot*; enough that it made me flinch.

No sooner had I posted that, I noticed the screen border had developed a slight, narrow band of flickering midway down. It isn't visible when the screen has a black background, which is why I hadn't noticed it after an hour of use. But booting back into BASIC afterwards, and the purple border makes it obvious. I discounted my S-Video cable and RetroTINK by connecting them to the 128, which rendered a flawless image.

I let it cool down, and the flickering disappeared completely.

<figure><p><img src="https://rubenerd.com/files/2022/mos-8565r2@1x.jpg" alt="Close up of the shortboard showing the MOS 8565R2" style="width:500px;" srcset="https://rubenerd.com/files/2022/mos-8565r2@2x.jpg 2x" /></p></figure>

I've read that these later generation HMOS-II chips don't get as warm as the 65xx chips in the breadbin 64s, but this one certainly does. I wanted to get some proper heatsinks for this and a few of the others anyway, so its no big deal.

The machine has a new, modern power supply, and my handy datasette port voltage detector shows a near perfect 5 V, so I don't think its a power delivery issue. I *have* read it could be a flaky MOS 8701 timing chip too, but that didn't get hot.

If it still gets very warm, I might need to source a replacement to do a proper comparison; fortunately they're not as expensive as I thought. The VIC-IIe in the 128 barely gets warm running the same software in its 64 compatibility mode, despite being manufactured three years prior. Maybe I won the silicon lottery on that one.
