---
title: "Debating whether to have blog post types"
date: "2022-03-02T08:55:52+10:00"
abstract: "Discrete types can be rendered in specific ways to make them easier to parse."
year: "2022"
category: Internet
tag:
- weblog
location: Sydney
---
A blog is whatever you want it to be; it's why they'll always be better than social networks. But broadly speaking they're collections of writing sorted chronologically with titles, publish dates, categories and/or tags, and RSS feeds for subscribing in an aggregator.

This formula is a big part of what made the Blogosphere&trade; so successful in the early days. We could own, control, and present our writing in whatever way we wanted, but there was enough common structure and layout that you could easily grok whichever blog you came across. I don't think this is appreciated anywhere near as much as it should be; we talk so much about accessibility from the perspective of web standards, but rarely do we acknowledge consistency.

I'm not sure if Tumblr were the first to do this, but at some point in the late 2000s content management systems began to offer additional structure and visual optimisations for different post *types*, instead of assuming every post is textual.

These seem to be the most common ones:

* Text. Like a traditional blog post. The only type with a mandatory heading, and body text. Also acts as a fallback type if one isn't defined.

* Images or video. These would have an optional caption and no heading. Some CMSs differentiated between a single image/video and a gallery type, where images are presented in little thumbnails or tiles.

* Quotes. These could be considered text, but the heading is the only element. Usually rendered in larger text with curly quotes.

* Tables, figures, or source code. These would be considered the primary post element, with the title serving as a caption.

I've resisted the urge to have different types of posts thus far. My [shows](https://rubenerd.com/show/) are just regular posts with additional metadata and enclosures for podcast clients to slurp up. Even posts imported from my old Tumblr from back in the day were also just converted to text.

But I'm tempted to give it a try. Often times posts with a single quote don't have much meaningful information in their titles. Either that, or the title could easily contain the entire quote, rendering the body text pointless.

I think my American friends would consider this a bit *Inside Baseball*, but I think taxonomies are interesting. Every other type of printed media optimises for certain types of content, why not blogs?
