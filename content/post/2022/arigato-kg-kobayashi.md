---
title: "Arigato, K.G.Kobayashi"
date: "2022-03-31T09:31:03+11:00"
abstract: "It was pkg-bootstrap, not bookstrap!"
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2022"
category: Software
tag:
- bsd
- feedback
- freebsd
location: Sydney
---
I wanted to thank K.G.Kobayashi for their recent email about FreeBSD, and for the JoJo references! In my [post back in 2019](https://rubenerd.com/freebsd-shared-object-libssl-so-8-not-found/), I wrote:

	# pkg-static bookstrap -f

Instead of:

	# pkg-static bootstrap -f

*Bookstrap* sounds like an independent light novel or manga publisher. Ora ora ora!
