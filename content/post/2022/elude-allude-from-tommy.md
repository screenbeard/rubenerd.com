---
title: "Elude and allude, from Tommy"
date: "2022-05-20T08:54:07+1000"
abstract: "Where good spell checkers go to dye."
year: "2022"
category: Thoughts
tag:
- english
- feedback
- spelling
location: Sydney
---
Tommy emailed me with an embarrassing spelling mistake:

> I think you meant to use "allude" instead of "elude" on [/about#mascot](https://rubenerd.com/about/#mascot)

This is absolutely correct, and frankly quite silly that I've had it there for years without noticing. It reminds me of the adage that *computers are where good spell checkers go to dye*.

