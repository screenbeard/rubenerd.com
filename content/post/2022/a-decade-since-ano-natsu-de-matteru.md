---
title: "A decade since Ano Natsu de Matteru"
date: "2022-07-17T11:50:24+10:00"
abstract: "I... feel old."
thumb: "https://rubenerd.com/files/2022/fb6c32d992bfdd274cbfe4ca4e967d22@1x.jpg"
year: "2022"
category: Anime
tag:
- 2012-anime
- jc-staff
- toradora
location: Sydney
---
I remember watching this a decade ago, and talking about a few of the figs that came out with Clara whom I'd just met. I had it recommended to me because I was so helplessly obsessed with *Toradora*, which was also a JC Staff and Nagai Tatsuyuki production. Also cameras.

I was about to say it feels like an age ago, but it was *ten years*. To celebrate, the [Komoro Tourism Bureau](https://www.komoro-tour.jp/blog/id_7970/) released some new artwork, including on this rather handsome four-wheeled conveyance:

<figure><p><img src="https://rubenerd.com/files/2022/fb6c32d992bfdd274cbfe4ca4e967d22@1x.jpg" alt="Artwork of the characters along the length of a large truck." style="width:500px; height:221px;" srcset="https://rubenerd.com/files/2022/fb6c32d992bfdd274cbfe4ca4e967d22@1x.jpg 1x, https://rubenerd.com/files/2022/fb6c32d992bfdd274cbfe4ca4e967d22@2x.jpg 2x" /></p></figure>

