---
title: "EasyJet seats"
date: "2022-05-10T13:32:06+10:00"
abstract: "Removing seats for more… is it legroom?"
year: "2022"
category: Thoughts
tag:
- aviation
- ergonomics
location: Sydney
---
Business Insider [reported on some aviation news](https://www.businessinsider.com/budget-airline-easyjet-planes-labor-shortage-cabin-crew-remove-seats-2022-5?op=1) yesterday:

> UK budget airline, EasyJet, will remove a row of seats from its planes so it can operate flights with...

...more legroom? Is it legroom?

> ...fewer cabin crew it announced Monday.

Ah schucks.

Being just over 0.00186 kilometres tall makes airline travel fun, but at least we have the money to avoid airlines like that. Do they still provide seats on Rynnair, or are they optional now too?
