---
title: "We Serve. Coffee"
date: "2022-06-01T08:32:56+10:00"
abstract: "Any readers in Findlay, Ohio?"
thumb: "https://rubenerd.com/files/2022/we-serve-coffee@1x.jpg"
year: "2022"
category: Travel
tag:
- cafes
- coffee
- united-states
location: Sydney
---
I found [this photo by Mbrickn](https://commons.wikimedia.org/wiki/File:We_Serve_Coffee.jpg) while looking at coffee shops on Wikimedia Commons. I love it! If I have any American readers in Findlay, Ohio, is it any good?

**Update:** I [found their site](https://weservecoffee.org/). I love everything about this place.

<figure><p><img src="https://rubenerd.com/files/2022/we-serve-coffee@1x.jpg" alt="Photo of the outside of a coffee shop in Findlay, Ohio." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/we-serve-coffee@1x.jpg 1x, https://rubenerd.com/files/2022/we-serve-coffee@2x.jpg 2x" /></p></figure>

