---
title: "Another mention on @BSDNow!"
date: "2022-03-17T15:40:18+11:00"
abstract: "If the guys mention *this* post, would that be inception or recursion?!"
thumb: "https://rubenerd.com/files/2020/bsdnow.jpg"
year: "2022"
category: Software
tag:
- bsd
- bsdnow
- freebsd
- netbsd
- podcast
location: Sydney
---
It's hard for me to talk up or share accomplishments, positive reviews or mentions; it still feels a bit like showing off.

If you'll indulge me, the [BSD Now podcast](https://www.bsdnow.tv/) has featured another post of mine, this time on my [FreeBSD/Linux game machine setup](https://rubenerd.com/why-my-game-pc-also-runs-freebsd/). It's available for Patreon members now, and should be available on their site and public feed soon.

BSD Now have been sharing a lot of posts of mine lately, which I credit for a big uptick in my readers and subscribers. Their show has also become a highlight of my week!

[Check them out](https://www.bsdnow.tv/) if you're at all interested in BSD operating systems and broader discussions about *nix, networking, databases, and computer science. I'd even recommend it to Linux people as an introduction to the ecosystem.

If the guys mention *this* post, would that be inception or recursion?! 🤔
