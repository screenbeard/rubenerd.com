---
title: "CallMeKevin, and the quarter life crisis?"
date: "2022-12-29T17:31:59+11:00"
abstract: "Everything is going well, but I can’t shake the feeling."
year: "2022"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
I'm invoking [Betteridge's law](https://en.wikipedia.org/wiki/Betteridge%27s_law_of_headlines) here in the hopes that the title isn't true! Because for me it'd be a thirds life crisis. Or who knows, maybe it is a midlife one, or something else.

CallMeKevin's best video of the year wasn't about a game he was playing, it was a [heartfelt reflection](https://www.youtube.com/watch?v=2bTwY43yp7M) on changes he made to his channel and life over the past year. He delivered it with his trademark honesty and good humour, and I have tremendous respect for him for doing so. Kevin is *Good Civ*.

He's not the first person to do this. Twenty one (!!!) independent video creators, podcasters, and bloggers I'm subscribed to have voiced similar sentiment this year in dedicated episodes and posts, and I'll bet plenty more are suffering in silence or private. Two are suspected to have taken their own lives.

I'm sure there's an element of confirmation bias here, but it feels like for the first time that more people I know on social media are voicing worries than before, even among those whom I thought were the most well adjusted, happy, and optimistic. There's a collective malaise sweeping the world.

🌲 🌲 🌲

I've been in a similar boat this year. I work hard, but I also live a privileged life. I have a great technical job with colleagues I like being with. I live comfortably and debt free in a stable country with plenty of savings and no financial stress. I have a successful enough blog that attracts comments from interesting, kind, and intelligent people almost every day. Most importantly, I have a patient, loving partner to share my life with, who's much smarter than me, and who's even cuter than the anime mascots she draws for me.

But as I've blogged about a few times this year, things aren't all right. Kevin mentioned he distracted himself by "being busy", which I've also identified in myself. This façade fell apart a bit during remote work and lockdowns, but was laid bare when I started leave and had time with my thoughts. They're utterly perplexing and terrifying.

It's not depression, at least not in the way I understand it. I have plenty of passions and joys in my life, lots to look forward to, and an endless list of projects to explore and learn from. I have awesome iRL and Internet friends, family, and readers including you. I harbour no ill will or resentment towards almost anyone, and I've cut out those for whom I do. I think my anxious, guilty teenage self would be thrilled, and relieved, to see where my efforts have lead.

And yet, like more and more people, I can't shake the feeling that something is wrong. It's deep seated, frustrating, ambiguous, and fundamental. But without knowing what it is, it's hard to extricate myself from it, or pretend around people that it doesn't exist.

I hope you're going okay. One comforting thing for me is that we're all in this together.
