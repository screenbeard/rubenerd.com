---
title: "My essential Firefox fixes (and add-ons) in 2022"
date: "2022-03-13T09:55:45+11:00"
abstract: "Despite Mozilla’s best efforts, I’m still using their browser nearly 20 years on. Here are my tips."
year: "2022"
category: Software
tag:
- firefox
- plugins
- privacy
- security
location: Sydney
---
*Update: This may be my first time I've appeared at the [top of Hacker News](https://news.ycombinator.com/item?id=30663266); thanks [@koobs](https://twitter.com/koobs/status/1503104068025139202) for alerting me. There are some great plugin ideas and feedback there. Mozilla: please pay attention, these are your biggest advocates.*

I haven't done a *how I use Firefox* post for a few years now, so I thought it was worth revisiting everyone's favourite runner-up browser. It's scary to think I've been using this for almost twenty years!

This is valid for Firefox 96, another number that doesn't seem real. Though I suppose [technically it is](https://en.wikipedia.org/wiki/Real_number "Wikipedia article on real numbers"). How irrational of me! Wait, [rational](https://en.wikipedia.org/wiki/Rational_number "Wikipedia article on rational numbers").


### Interface fixes

Mozilla recently removed the Search box by default, and added redundant padding to the toolbar to mimic Safari. You can reclaim this space, and get the Search and titlebar back for easier dragging:

1. Right-click the toolbar and click **Customise Toolbar**
2. Hover over the flexible spaces and drag them out
3. Drag the **Search** box back next to the **URL** bar
4. Drag the **Pocket** icon out if you don't use it
5. Click the **Title Bar** checkbox to enable it again

If you're on \*nix or Windows, you can also click the Hamburger menu on the top-right and choose **Menu Bar** to get that back too.

You also should definitely *not* install the [Horizontal Wood](https://addons.mozilla.org/en-GB/firefox/addon/horizontal-wood/) theme to make your browser look like a piece of IKEA furniture. I didn't, and am definitely not loving it for its cheesy fun.


### Privacy extensions

* [uBlock Origin](https://addons.mozilla.org/en-GB/firefox/addon/ublock-origin/) is still my go-to for privacy protection and filtering.

* I also use [Decentraleyes](https://addons.mozilla.org/en-GB/firefox/addon/decentraleyes/) to proxy common CDN requests. *Update: People are recommending [LocalCDN](https://codeberg.org/nobody/LocalCDN/) as an alternative.*

* I keep wanting to use [NoScript](https://addons.mozilla.org/en-GB/firefox/addon/noscript/), but unfortunately the modern web beats me back into submission each time. Maybe you'll have more luck.

* [I Don't Care About Cookies](https://addons.mozilla.org/en-GB/firefox/addon/i-dont-care-about-cookies/) is essential if you scrub cookies. Without a way to save preferences, those disingenuous implementations of EU law asking you for permissions each time get mighty old!

I used to use cookie blocking tools, but thesedays I use Firefox's built-in **Cookies and Site Data** tools in **Preferences**. There you can click **Delete cookies and site data when Firefox is closed**, and also click **Manage Exceptions** to add frequent sites to spare you having to re-auth every time.


### Missing functionality

* [Tree Style Tab](https://addons.mozilla.org/en-GB/firefox/addon/tree-style-tab/) is still essential for putting tabs on the side, so you don't end up with a row of tiny tab icons. It also helps you group tabs, so you can see where tabs came from.

* [KeePassXC-Browser](https://addons.mozilla.org/en-GB/firefox/addon/keepassxc-browser/) is essential if you use the world's best password manager!

* And finally, [OneTab](https://www.one-tab.com) converts your open tabs into an HTML file with a list. It's a great way to save memory and bookmark a ton of tabs instead of keeping things open.
