---
title: "2022-02-02"
date: "2022-02-02T14:06:51+11:00"
abstract: "It’s ALMOST symmetrical... right?"
year: "2022"
category: Thoughts
tag:
- dates
- pointless-milestones
location: Sydney
---
It's *almost* symmetrical! I thought it was worth mentioning. Like hummus.

Here are some other pointless date posts:

* **[2010-10-10](https://rubenerd.com/happy-binary-day/)**: Happy Binary Day
* **[2011-01-01](https://rubenerd.com/powermenu-11111-date/)**: with PowerMenu for DOS
* **[2011-11-11](https://rubenerd.com/11-11-11-11-11/)**: with the Mac OS X Dashboard
* **[2020-10-20](https://rubenerd.com/its-20-10-2020-10-20/)**: with... something?
* **[2022-02-02](https://rubenerd.com/2022-02-02/)**: that's some devilish recursion, Seymour
