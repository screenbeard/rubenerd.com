---
title: "My journey back to Fedora Workstation"
date: "2022-05-14T10:08:31+10:00"
abstract: "It mostly just works, just like Fedora of yore."
year: "2022"
category: Software
tag:
- fedora
- linux
- reviews
location: Sydney
---
Long-time readers of my rambling would remember I [ran Fedora on my laptops](https://rubenerd.com/tag/fedora/) for many years. FreeBSD's Wi-Fi and suspend/resume support was patchy enough that I kept it on the desktop, and Fedora just worked. I also like to keep my toes in a few camps to see how things are progressing on the other side of the fence, and to keep my Linux skills up.

I stopped using Fedora around the time I made the switch on the server side to Debian for Linux workloads. I figured I'd make my life easier by running the same Linux family everywhere.

People categorise Linux distributions based on their package managers, because it's such an important interface and probably among the first things people use when configuring a system. But the philosophies behind their designs peek through in other ways too, from configuration locations to file systems.

Over time I got work Macs, and FreeBSD became good enough on my laptops. But then Steam and Proton [got me back into the Linux desktop again](https://rubenerd.com/why-my-game-pc-also-runs-freebsd/)... I can't understate how much fun it is having access to PC games and GPUs again without needing to touch Windows.

Ubuntu had been fine for this, but its use of Snaps over Flatpaks (or even just package repos) and a few other wrinkles have made it feel decidedly less Debian-y than I remember. It's hard to quantify, but it felt flaky.

After distro hopping more times than I care to admit, I tried Fedora again after a few friends and people I trust reported positive experiences with Workstation and the immutable Silverblue. It just worked again, save for some Nvidia adventures which I don't fault Linux distro maintainers for. I even did an update from 35 to 36, and I haven't had a system upgrade outside the BSD ecosystem go so smoothly.

There are broader questions about the direction Linux is taking, some of which I think is warranted, and others that make me relish my time in FreeBSD, NetBSD, and illumos. But for a desktop game machine I mostly dual-boot into to play Steam games and Minecraft, I'm remembering why I liked using this. If you're a desktop Linux user in 2022, it's probably the best experience you'll get.

