---
title: "Expelled from Paradise seven year anniversary"
date: "2022-02-06T09:58:39+11:00"
abstract: "... seven years? Obligatory feeling of being old."
thumb: "https://rubenerd.com/files/2022/FEM2KXpaUAAFAF3@1x.jpg"
year: "2022"
category: Anime
tag:
- cgi
- expelled-from-paradise
- reviews
- saitom
location: Sydney
---
I only just saw the [official post from November](https://twitter.com/efp_official/status/1460070281284751360) last year:

<p><img src="https://rubenerd.com/files/2022/FEM2KXpaUAAFAF3@1x.jpg" srcset="https://rubenerd.com/files/2022/FEM2KXpaUAAFAF3@1x.jpg 1x, https://rubenerd.com/files/2022/FEM2KXpaUAAFAF3@2x.jpg 2x" alt="Expelled from Paradise" style="width:500px; height:282px;" /></p>

It got mixed to *meh* reviews from the anime community, but Clara and I loved this film. I joked at the time that it was as if WALL&middot;E had a spinoff with characters designed by Saitom, one of my favourite artists. It tackles many similar themes (no spoilers), and even has a cute robot with a hat!

It was most polarising for its use of CGI, when most anime was&mdash;and continues to be&mdash;hand drawn. It was a bit jarring for the first minute, but it was probably the most well-executed use of it at the time. My primary frustration was realising I wasn't *nearly* rugged enough to cosplay Zarik Kajiwara.

Seven years does seem ridiculous though... I saw it as such a *modern* film. I guess it still can be.
