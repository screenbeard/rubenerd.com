---
title: "Buying a new graphics card"
date: "2022-02-17T11:28:15+11:00"
abstract: "Only realising what digital designers and gamers have been crying about for years."
thumb: "https://rubenerd.com/files/2022/gpu-3060ti@2x.jpg"
year: "2022"
category: Hardware
tag:
- blockchain
- economics
- gaming
- graphics-cards
location: Sydney
---
I've been looking to buy a new graphics card for the last year or so. Already you can probably tell where this is going.

My Asus GTX 970 OC edition has served me well over the years playing Minecraft with shaders, and games like X-Plane and Train Simulator. The Nvidia binary blob drivers [work great on FreeBSD](https://rubenerd.com/freebsd-12-1-nvidia-desktop/ "My blog post about FreeBSD 12.1 with Nvidia drivers"), and [Debian for Steam games](https://wiki.debian.org/Steam "Steam page on the Debian wiki"). Alas, it doesn't fit in my [smaller sleeper PC case](https://rubenerd.com/finally-buying-a-compaq-spaceship/) rebuild, so I've sold it and am using the integrated Intel silicon while I decide what to do.

*(My MacBook Pro ended up filling the void here with its 8 GiB AMD 5500M. I was surprised that it gave comparable performance to that much older 970, though it does struggle a bit on my new monitor with anything greater than medium settings. Even with an active cooling pad, the laptop also sounds like its roasting itself, which can't be good for the battery).*

In the words of Gough Whitlam, *it's time*. Unfortunately, I'm learning about the dreaded trifecta that digital designers and gamers have been throwing their hands up in despair over for years now:

<p><img src="https://rubenerd.com/files/2022/gpu-3060ti@1x.jpg" srcset="https://rubenerd.com/files/2022/gpu-3060ti@1x.jpg 1x, https://rubenerd.com/files/2022/gpu-3060ti@2x.jpg 2x" alt="The Asus 3060 Ti Dual Mini" style="width:160px; height:139px; float:right; margin:0 0 1em 2em;" /></p>

* crypto-"currency" miners,
* silicon shortages, and
* the effects of the pandemic

These have driven prices to eye-watering heights. GPU manufacturers have paid lip service to discouraging mining, and have set up elaborate systems to allow people to bid on the cards they want, but the situation is still grim. Cards now cost as much, if not more, than game consoles or all the remaining components of a PC build combined.

But wait, *there's more!* Top-end cards were always [halo devices](https://en.wikipedia.org/wiki/Halo_effect#Marketing), but the price for what is considered mid-range or even low-end (that's a lot of hyphens) have all shifted up a few notches. A bit of a *Train Simulator* pun there. The Asus 3060 Ti Dual Mini or Zotac 3070 Twin Edge I'm looking at would fit perfectly in my target sleeper PC tower, but both breach *four figures* in Australia despite being decidedly mid-range. That's a lot of coffee.

I've had money saved for more than a year waiting for prices to become even a *tad* more reasonable, but as they say, the market can remain irrational longer our patience can last.
