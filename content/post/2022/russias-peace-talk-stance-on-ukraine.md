---
title: "Ukraine/Russia peace talks “didn’t make sense”"
date: "2022-07-25T08:36:07+10:00"
abstract: "For once, Russian Foreign Minister Sergei Lavrov told the truth."
year: "2022"
category: Thoughts
tag:
- news
- ukraine
location: Sydney
---
Russian Foreign Minister Sergei Lavrov told the truth, for once:

1. He claimed peace talks "didn't make sense" ([ABC News Australia](https://www.abc.net.au/news/2022-07-21/peace-talks-with-ukraine-make-no-sense-says-russia/101256186)).

2. Russia, Ukraine, and Turkey agreed in talks to resume Black Sea grain exports blocked by Russia ([Sydney Morning Herald](https://www.smh.com.au/world/europe/ukraine-russia-reach-deal-on-grain-but-peace-a-long-way-away-20220714-p5b1go.html)).

3. Russian missiles attack a grain export ship in Odesa. Russia and their paid social media bots denied the attack, then they rolled back. Really. ([Moscow Times](https://www.themoscowtimes.com/2022/07/24/russia-says-odesa-strikes-hit-western-arms-a78389)).

Don't forget: this war would be over *today* if the Kremlin wanted. Hypocrisy is but one of their crimes against their Slavic brethren.

As a reminder, [Ukraine's United24 site](https://u24.gov.ua/) lets you donate for defence, demining, medical aid, and reconstruction. You can also set up an automatic monthly donation.

We need to [keep talking about this](https://rubenerd.com/zelenskyy-keep-sharing-about-ukraine/). Bad people win in darkness. 🌻
