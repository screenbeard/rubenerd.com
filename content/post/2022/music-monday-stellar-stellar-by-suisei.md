---
title: "Music Monday: Stellar Stellar by Suisei"
date: "2022-05-09T15:15:33+1000"
abstract: "Aaaaaaah!"
thumb: "https://rubenerd.com/files/2022/yt-a51VH9BYzZA@1x.jpg"
year: "2022"
category: Media
tag:
- hololive
- music
- music-monday
location: Sydney
---
This is Suisei's second appearance here on [Music Monday](https://rubenerd.com/tag/music-monday/), that ongoing series about music I always post on a Monday, except when I don't.

Suisei's performance of her 2021 hit during the Hololive *Link Your Wish* concert was everything I'd hoped it'd be. Her vocals at the tail end of the chorus are especially stunning, as are the music video graphics! ♡

<p><a href="https://www.youtube.com/watch?v=a51VH9BYzZA" title="Play Stellar Stellar / 星街すいせい(official)"><img src="https://rubenerd.com/files/2022/yt-a51VH9BYzZA@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-a51VH9BYzZA@1x.jpg 1x, https://rubenerd.com/files/2022/yt-a51VH9BYzZA@2x.jpg 2x" alt="Play Stellar Stellar / 星街すいせい(official)" style="width:500px;height:281px;" /></a></p>

