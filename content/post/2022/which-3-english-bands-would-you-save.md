---
title: "Which 3 English bands would you save?"
date: "2022-07-26T09:07:10+10:00"
abstract: "Putting on my blog to spare me from socials #cough."
year: "2022"
category: Media
tag:
- music
- the-beatles
- pet-shop-boys
- dire-straits
location: Sydney
---
This was a [mental exercise](https://twitter.com/NoContextBrits/status/1550871013973528576) so fraught with peril and judgement, I've decided to post my response here instead.

<figure><p><img src="https://rubenerd.com/files/2022/FYXM9vZXwAIcjvi@1x.jpg" alt="A selection of English bands." style="width:420px; height:560px;" srcset="https://rubenerd.com/files/2022/FYXM9vZXwAIcjvi@1x.jpg 1x, https://rubenerd.com/files/2022/FYXM9vZXwAIcjvi@2x.jpg 2x" /></p></figure>

Brace yourself, mine are, without reservation:

* The Beatles
* Fleetwood Mac
* Pet Shop Boys
* Dire Straits

I count from zero because I work with computers. That's legit, right?

Note the question was specifically *which you'd save*, not which you think are the best. These bands were the soundtrack to my childhood, and I still love them today. There are also some others I'd sorely miss. Don't judge me... but I know you still will.

