---
title: "NetBSD 9.3 and whimsy"
date: "2022-08-17T14:09:37+1000"
abstract: "luna68k - make kernel messages green to match other ports and show off color support!"
year: "2022"
category: Software
tag:
- bsd
- netbsd
- openbsd
- whimsy
location: Sydney
---
This was my favourite addition to the [release notes](https://netbsd.org/releases/formal-9/NetBSD-9.3.html)\:

> luna68k - make kernel messages green to match other ports and show off color support. 

NetBSD is a professional operating system, but I also love a bit of whimsy. Same goes for OpenBSD's songs and art for each release.

NetBSD have also long had an [RSS feed](https://www.netbsd.org/changes/rss-netbsd.xml) available for their release announcements. I tend to read mailing lists for this sort of information, but I welcome anything to make these more accessible.
