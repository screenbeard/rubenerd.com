---
title: "STH’s Supermicro X13SAE-F board review"
date: "2022-10-13T09:15:46+11:00"
abstract: "I’d be tempted to even build my next home tower/game machine out of a board like this."
year: "2022"
category: Hardware
tag:
- supermicro
- workstations
location: Sydney
---
[ServeTheHome did a quick review](https://www.servethehome.com/supermicro-x13sae-f-intel-w680-motherboard-mini-review/) of a new Supermicro motherboard yesterday which is worth a read:

> The Supermicro X13SAE-F is an ATX motherboard measuring 12” x 9.6” and a feature set straddling the line between the workstation and server. It sports an Intel LGA-1700 (H5) socket, which will accept 12th-generation Core i3/i5/i7/i9 CPUs. This board is powered by Intel’s W680 chipset, which is their workstation variant of the Z690 consumer chipset.
>
> Rear I/O on the X13SAE-F definitely looks more workstation than a server, with a full complement of display and audio outputs, along with more USB ports than most servers. The RJ45 network ports are interesting as well, with a dedicated BMC NIC along with both an Intel i219-LM 1 GbE port as well as an i225-LM 2.5 GbE port.

I love Supermicro kit; I've used their boards for DIY home servers for years, and we use a ton of their stuff at work. Weirdly, I see the inclusion of a standard green PCB and a PCI slot in 2022, and it makes me smile. No *Raptor*, *Mortar*, or *Collateral Damage* branding here.

I'd be tempted to build my next personal desktop out of a Supermicro board too, even if it ended up being a game machine half the time. Having ECC memory would also be wonderful, assuming you get the right silicon to support it.

