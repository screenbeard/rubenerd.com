---
title: "Hanging out with nice people"
date: "2022-12-28T08:55:20+11:00"
abstract: "You know who you are!"
year: "2022"
category: Thoughts
tag:
- introvert
- personal
- social
location: Sydney
---
On Tuesday Clara and I spent the day with friends [playing boardgames](https://bsd.network/@rubenerd/109584229466503339) and cards while eating unholy amounts of holiday treats and drinking tea. We spent Christmas with my sister and my new brother-in-law. And now I'm working out a time to meet the first friend I made in Sydney, among others.

I've been an introvert since before I knew the term and understood it; I need to be alone (or just with Clara) to mentally recharge or I get exhausted, anxious, and irritable. It's probably why I blog, write code, and tinker with servers and old computers. Chances are if you read my ramblings, you're in a similar boat!

*(Weirdly this doesn't usually extend to work interactions. I think its because I have a mental "out" with meetings lasting a specific period of time, and there are unambiguous expectation of what's to be delivered).*

Unfortunately, Covid lockdowns and remote work reinforced these tendencies to such a degree that I think it started to impact my mental health. Even the most closeted of individuals need to spend time with people they care about, and being in a small group sharing in a hobby for a few hours is lovely.

Conversely, it's also lead to the realisation that I want to spend my rationed social attention on people I care about, not those who make my life miserable, difficult, or exhausting.
