---
title: "Numbers that don’t have eight in them"
date: "2022-07-31T09:15:15+10:00"
abstract: "It's been a while since I've enumerated a list of numbers here for our collective education."
year: "2022"
category: Thoughts
tag:
- numbers
- pointless
location: Sydney
---
In January 2019 I listed numbers that [don't have seven](https://rubenerd.com/it-has-numbers-in-them/). In 2020 I [did it again](https://rubenerd.com/it-also-has-numbers-in-it/). Today I thought I'd up the ante by an entire integer.

Here are a list of numbers that don't have eight in them:

* Eight
* &hellip;

Why do I do this to myself.
