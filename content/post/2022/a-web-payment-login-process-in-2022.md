---
title: "A modern web payment login process"
date: "2022-05-16T20:17:27+10:00"
abstract: "I couldn’t help but chuckle about how desensitised I’ve become to this."
year: "2022"
category: Internet
tag:
- accessibility
- internationalisation
- design
location: Sydney
---
I couldn't help but chuckle about how desensitised I've become to this:

1. Click the link to the well-known external payment processor.

2. Type my username (from my password manager).

3. Click **Continue**.

4. Click all the "traffic lights".

5. Click **Continue**.

6. Be told I missed some, and to try again.

7. Click all the "crosswalks", after figuring out what a "crosswalk" is. An angry hiker? This isn't a word we use here, so that's an <abbr title="internationalisation">1i7n</abbr> fail.

8. Click **Continue**.

9. Type my password (from my password manager).

10. Choose a two-factor auth method out of a list of one.

11. Click **Continue**.

12. Wait for the 6-digit code.

13. Type the 6-digit code.

14. Read premature validation text warning me in scary red text that the code I'm in the middle of typing "should be six digits".

15. Click **Continue**.

16. Click **Agree and Pay**.

17. Wait to return to merchant store.

18. Receive timeout alert.

Wait, what was I buying again? Was this service trying to tell me I needed to save money? *Touché.*
