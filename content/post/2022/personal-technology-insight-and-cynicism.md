---
title: "Personal technology insight and cynicism"
date: "2022-03-04T14:16:22+11:00"
abstract: "Technology Connections learned about them via can openers."
year: "2022"
category: Hardware
tag:
- optimism
- philosophy
- quotes
location: Sydney
---
Technology Connections [learned something from can openers](https://www.youtube.com/watch?v=i_mLxyIXpSY)\:

> This can opener has lead me to ask a pretty deep question. What else might we be using, or doing, in day-to-day life which we just assume is *how things are* and never question?
>
> I think it's very normal and useful to be cynical towards what purports to be technological progress. There are plenty of instances where tried-and-true simple techniques are, frankly, fine, if not superior. And a lot of what we're sold as new, and exciting, turns out to be a gimmick&hellip; if we're lucky. It's very easy to overcomplicate things in the name of the *new*. Understandably, that can make us skeptical of innovation, full stop.
>
> The trouble is though, we make the assumption that there aren't improvements to be made, you may never look for them, and you can miss a lot of progress that's happening around you while you aren't paying attention.

I've caught myself in this feedback loop a *lot*. I predict that a new technology, framework, device, or piece of software doesn't improve anything, introduces its own problems, and comes from people who's motivations and ethics don't align with my own. Unfortunately, my reservations have been proven right *so many times* that it's hard to break out of cynicism as a learned behaviour.

For all its foibles, technology has also made huge and positive leaps in progress in plenty of areas. I don't appreciate it, because I'm worrying about something else. I'll bet there's a ton of stuff that could make my life better, easier, or more fun, but I'm blinkered to it.

I've started calling myself a *cautious optimist*. Maybe calling it out will reinforce it.
