---
title: "Zelenskyy: Keep sharing about Ukraine"
date: "2022-06-08T09:20:47+1000"
abstract: "With links to his United24 initiaitive for donations for defence, demining, medical aid, and reconstruction"
thumb: "https://rubenerd.com/files/2022/u24@1x.jpg"
year: "2022"
category: Media
tag:
- news
- ukraine
location: Sydney
---
<figure><p><img src="https://rubenerd.com/files/2022/u24@1x.jpg" alt="" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/u24@1x.jpg 1x, https://rubenerd.com/files/2022/u24@2x.jpg 2x" /></p></figure>

[The President of Ukraine](https://redditvids.com/v/v6fnz8)\:

> The maintenance of attention cannot be stopped for a single moment. That applies to everyone. Every modern person is well aware of how the media works. It is very difficult to keep an eye on one topic for a long time.

This is unfortunately true, and especially so on the modern web.

> For the attention to Ukraine, to our struggle, for freedom not to decrease, everyone must continue to talk about what is happening. **Please share information**. Support our needs. The more we say about Ukraine in the world, the sooner we'll be able to end the war and liberate our land.

The President's [United24 initiative](https://u24.gov.ua/) includes links for where you can send donations for defence, demining, medical aid, and reconstruction. Please consider if you can. 🌻
