---
title: "Kyle Lanchman on iOS navigation, and music"
date: "2022-05-29T08:58:15+10:00"
abstract: "Other people remember being able to customise the navbar as well!"
year: "2022"
category: Software
tag:
- feedback
- ios
location: Sydney
---
iOS developer Kyle Lanchman emailed about [my frustration over iOS navigation](https://rubenerd.com/the-first-iphone-and-portable-music-players-in-2022/):

> The customizable tab bar! It seems like there’s not an app left in existence that’s using Apple’s built-in More menu and tab bar customization. This hit me just recently as I began work on an update to my app. I want to add a sixth tab, so I thought I’d use Apple’s “More” menu / customization, but I was having second thoughts: “Why doesn’t anyone use this? Is the API terrible to work with?” About the only complaint I have as a user is that the customization process is bit unintuitive at first.

We're a dying breed, but I do remember how brilliant that feature was. I tend to listen to albums over playlists and individual songs (especially during a commute or a long walk), so I'd always rearrange the footer navigation to put links for artists and albums first.

Also, check out Kyle's [iOS AniList client Ryuusei](https://ryuusei.moe/)! I've been decidedly lax with my anime and manga logging these days, but I remember having a lot of fun with that in *The Before Times* on MAL. I might write a proper post about this soon.
