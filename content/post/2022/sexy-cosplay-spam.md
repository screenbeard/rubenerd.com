---
title: "“Sexy cosplay” spam"
date: "2022-02-28T12:41:18+11:00"
abstract: "I’d like to cosplay as a spam filter, can you arrange that?"
year: "2022"
category: Internet
tag:
- pointless
- spam
location: Sydney
---
> Hello, I'm john from $LEGIT_PLACE.
> We are one of best manufacturers of Sexy Lingerie & Swimwear.
>
> About us,
>
> NO.1 Production base for Cosplay Sexy Lingerie.
> Top 3 Sexy Lingerie/Gown/Corset/Swimwear Manufacturer in China,
> 17+ Years Export Experience &hellip;
>
> If we have a chance to cooperate? I?d like to provide more information for you.

I'd like to cosplay as a spam filter, can you arrange that? I imagine this person with a huge net, and cargo pants stuffed with nefarious letters.
