---
title: "A late review of the M1 MacBook Air"
date: "2022-12-22T12:06:02+11:00"
abstract: "About the same performance as my Intel MacBook Pro, but cheaper, less weight, far more battery life, and never gets hot."
year: "2022"
category: Hardware
tag:
- apple
- macbook-air
location: Sydney
---
I thought I'd blogged about my new MacBook Air, but I've only made passing mention of it a couple of times. This is how I summarised its performance on a [post about QEMU](https://rubenerd.com/windows-2000-in-qemu-on-an-m1-mac/) in October:

> But, anecdotally the responsiveness is marginally better than my last Intel MacBook Pro.

That's it in a nutshell. When you consider the Air was a quarter of the price, half the weight, has battery life measured in days not hours, and stays cool the entire time, it's genuinely impressive. Not to mention it has a better screen and trackpad than most PC laptops twice the price.

It still has soldered storage, which means you need active and tested backups for when this kicks the bucket; warranties protect your wallet, not your data. It's also not as flexible or light as the original 11-inch MacBook Air, presumably because Apple wants you using an iPad at that size instead. And while its keyboard is better than the butterfly, it's no ThinkPad.

But if you need a portable Mac, the difference between this and the Intel kit of yore is stark.

