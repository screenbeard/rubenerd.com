---
title: "You searched for a user’s guide, sir?"
date: "2022-12-14T07:32:33+11:00"
abstract: "Online store recommendation engines aren’t very smart."
year: "2022"
category: Internet
tag:
- ai
- shopping
location: Sydney
---
Online store recommendation engines are always funny, whether they're advertising mattresses to the happy owner of a new one, a guide to Windows 11 after you bought a MacBook, or cuts of meat to someone wheeling out a stack of new vegetarian cookbooks. They offer us a glimpse behind the hubris of so many systems sold as intelligent, but in practice are anything but.

I found a user's guide to a specific piece of vintage computer tech on eBay, and was subsequently told I'd be interested in these:

* Sweet William: A User's Guide to Shakespeare, by Michael Pennington

* How To Live: A User's Guide, by Peter Johns

* Menschliche Genom: A User's Guide, by Julia E. Richards

* The Body: A Complete Users Guide, by National Geographic

* A User's Guide to Public Sculpture, by J. Darke

Clearly the *User's Guide* string was matched, but it amazes me that it assumed tomes across so many different categories would be relevant. Can you imagine walking into a brick-and-mortar book store, and being told your Perl programming guide belied an interest in freshwater shelled mollusks?

Maybe it's telling me to store a User's Guide to how the Genome in Shakespeare's Body Lived in Sculpture on my Commodore 1571.
