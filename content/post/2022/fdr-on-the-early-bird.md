---
title: "FDR on the early bird"
date: "2022-04-05T13:14:32+10:00"
abstract: "I think we consider too much the good luck of the early bird and not enough the bad luck of the early worm."
year: "2022"
category: Thoughts
tag:
- quotes
location: Sydney
---
Om Malik [reminded me](https://om.co/2022/04/03/slow-horses/) of this great quote:

> I think we consider too much the good luck of the early bird and not enough the bad luck of the early worm.
