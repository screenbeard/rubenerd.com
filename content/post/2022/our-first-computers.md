---
title: "A list of my first computers"
date: "2022-04-10T09:01:45+11:00"
abstract: "Starting with our whitebox 486-SX :’)"
year: "2022"
category: Hardware
tag:
- nostalgia
location: Sydney
---
I talk about some of the machines I grew up with, but I don't think I've ever compiled an actual list of them. It was a bit of a bittersweet exercise, given most of these no longer exist. My parents probably saved me from hoarding a ton of this stuff, though I miss it.

I'm sure you've got your own list; [contact me](https://rubenerd.com/about/#contact) if you compile one :).

### The 1990s

1. **A whitebox 486-SX**. I think it ran MS-DOS 5.x, then Windows 3.0, then MS-DOS 6.0 with 3.1. Was recycled when the hard drive died (much to my chagrin), but I did harvest the 5.25-inch floppy drive and ISA Sound Blaster 32 card.

2. **A whitebox Pentium 133** when we moved to Singapore. Was the first computer I used the Internet on, thanks to its external K56Flex modem. Was recycled when a power surge took it out. Years later I found someone on eBay selling the same bezel for the otherwise unremarkable chassis this machine was built in. No idea what I'll use it for, but naturally I bought it.

3. **A DIY Pentium 200 MHz tower**. This was the first computer I ever built myself from parts as a kid, and she still runs to this day! She now even includes the aforementioned parts from that first machine.

4. **An HP Brio BAx**. 450 MHz Pentium III. This was our first "branded" computer we got at a tradeshow in Singapore. First ATX machine with PS/2 ports and USB... and the first I bricked with a BIOS update. Long taken to recycling.

5. **A Blueberry iMac DV**. The recording studio my sister and I worked had them, and I'd been fascinated by Mac OS from the computers at school. Such an icon of the 1990s. I still have her, but she doesn't boot.


### The 2000s

1. **NTSC Commodore 16, 64, and Plus/4**, parents bought from eBay for my 18th birthday. The 64 was sent to the Geekorium after he generously donated his **PAL Commodore 128** to me a few years ago.

6. **An AMD Athlon XP tower** I built from parts to play PC games on, mostly Age of Empires, Worms II, and Need For Speed. Didn't know much about graphics, so don't even remember what GPU I bought for her. My first experience with a burntout CPU when I realised I didn't attach the heatsink properly. Was recycled.

7. **A Sony Vaio PCG-C1VM** [sic] subnotebook. While attempting to upgrade her hard drive, I slipped with a knife (long story) and sliced a part of my hand open. I still have the scar! Needless to say, I recycled her and shouted *good riddance, you piece of schmidt!*

8. **An iBook G3, dual-USB port version**. Probably my favourite laptop ever. Mac OS X barely ran on the iMac DV, but was beautiful on this machine. I did all my high school library studies on it. Also the first computer I booted NetBSD and FreeBSD on.

10. **PowerMac G5**, parents bought for my graduation and when I started uni. Produced the first episodes of my silly, long-running podcast on her. Was such a fun machine to tinker with. Ended up selling her on eBay to fund my first MacBook Pro.

11. **ThinkPad X40 and X61**, picked up from eBay for peanuts. I ran Fedora on them, because Wi-Fi was a bit flaky on FreeBSD. These were my coffee shop and library study computers. Absolute tanks. Ended up donating them, but I miss them.

12. **MacBook Pro 1,1**, the first Intel Mac released. People forget that this generation used the 32-bit Core Duo CPU. Was a beta tester for the first Parallels Desktop virtualisation software on her, and was also set to triple-boot Solaris and FreeBSD.

2. **Toshiba Libretto 70CT**, a tiny laptop I'd always wanted as a kid,  picked up on eBay for peanuts.
