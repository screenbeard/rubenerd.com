---
title: "paprok gets CDE running on NetBSD"
date: "2022-03-24T20:18:48+11:00"
abstract: "I got hit in the nostalgic feels."
thumb: "https://rubenerd.com/files/2022/dSGZij3.png"
year: "2022"
category: Software
tag:
- bsd
- netbsd
location: Sydney
---
CDE was a bit before my time dabbling in UNIX, but I've always loved that chiselled Motif aesthetic. Remember when Xfce was billed as CDE-like, and had that bottom panel configured in a similar way?

But I digress. I wanted to give a shoutout to /u/paprok getting <a rel="nofollow" href="https://www.reddit.com/r/NetBSD/comments/thussq/build_and_install_of_latest_cde_240c_on_netbsd_92/">CDE running on contemporary NetBSD</a>, which I might have to try on my vintage builds!

<p><img src="https://rubenerd.com/files/2022/dSGZij3.png" style="width:500px;" alt="Screenshot showing paprok's CDE desktop on NetBSD"/></p>

As an aside, is a phrase with three words. I subscribe to various BSD subreddits in The Old Reader using RSS feeds, so I don't have to go to the site directly.

* https://www.reddit.com/r/FreeBSD/.rss?sort=new
* https://www.reddit.com/r/NetBSD/.rss?sort=new
* https://www.reddit.com/r/OpenBSD/.rss?sort=new
