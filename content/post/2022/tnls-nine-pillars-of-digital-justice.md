---
title: "TNL’s Nine Pillars of Digital Justice"
date: "2022-05-12T08:27:16+1000"
abstract: "Some of these seem simplistic, but I’m glad someone is thinking about them."
year: "2022"
category: Internet
tag:
- australia
- politics
- privacy
- security
location: Sydney
---
TNL is a new political party in Australia that seeks to attract disenfranchised LNP voters (the incumbent conservative coalition). I hope they're successful; I'm not a voter of those parties, but they need to see that lurching away from moderate positions has consequences. But I digress!

I was checking out their policies page, and was intrigued by a linked whitepaper by Julian Webb called [Digital Justice Strategy](https://tnl.net.au/wp-content/uploads/2022/01/The-New-Liberals-Digital-justice.pdf):

> [D]igital justice policy is defined as a range of initiatives designed to advance equality of access to digital technology; to prioritise the digital participation of people who have been traditionally excluded from and marginalised by technology, and to provide citizens with individual rights and remedies appropriate to the digital age.

The paper proposes *Nine Pillars of Digital Justice*, the first three of which are preconditions, and the the latter are rights. I thought it was worth taking a look.

> ⒈  Access to digital services is a right, not a privilege

Yes. Everything is done online now. Those who don't have Internet, either because it's unavailable or too expensive, are at a steep disadvantage.

> ⒉  Key digital infrastructures should be owned and/or governed as public utilities.

I'm not sure. It's true the Internet was successful owing to the fact no single corporation owned it, and that layers of it lend themselves to natural monopolies. Higher in the stack we have services so critical to the Internet's function that it seems ridiculous to have them beholden to corporate mismanagement and Wall Street. Maybe we need to identify these and fund non profits as well.

> ⒊  Digital justice requires an educated and risk-aware population. Education in digital and information literacy is a key, continuing, government responsibility.

Definitely. Ditto financial literacy, which is increasingly connected with this.

> ⒋  Data subjects own their own data

Yes we should, along with all the corollaries that come from this.

> ⒌  Everyone has autonomy in respect of their technological choices.
> 
> ⒍  No person should be discriminated against as a consequence of their technological choices.

I agree with 5, and access should be a right (from 1), but there should be minimum required specifications for connected hardware, like the phone and electrical systems. My credit union shouldn't be prosecuted for discriminating against my Commodore 16, which is physically incapable of interacting with their systems securely (inb4 serial cables to ssh jump boxes)! If this presents a financial burden for disadvantaged people, the Government should step in and subsidise or provide them hardware for free.

> ⒎  The use of automated decision-making by governmental and public bodies to
determine the rights, entitlements or liabilities of any person should be transparent and subject to human oversight and review.

Absolutely; this is a bug bear of mine. Anyone against greater transparency should be judged with suspicion.

> ⒏  Any government service proposing to use an artificial intelligence (AI) system to deliver or augment their services should ensure that it complies with current best practice in respect of ethical AI design.

This would be a good start, but I don't even think "best practices" are sufficiently understood. We should reverse the responsibility, which 9 addresses:

> ⒐  The use of facial recognition systems or similar biometric-based technologies for law enforcement and related functions shall not be pursued by any government, public or statutory body unless and until a proper system of legal safeguards has been put in place that takes due regard of fundamental human rights.

Sounds good. Give people effective recourse, and I'll bet a lot of the issues we currently see would mysteriously resolve themselves. It's funny how that works!

I'm glad to see people thinking more about these issues. The current coalition have been actively malicious, and Labor have been such a mixed bag as to be hopeless. These are points we should be debating.
