---
title: "My favourite Train Simulator routes on special"
date: "2022-03-28T18:29:50+11:00"
abstract: "There’s still time to snap most of these up for half price or less."
year: "2022"
category: Software
tag:
- europe
- games
- japan
- marias-pass
- montana
- switzerland
- train-simulator
- united-states
location: Sydney
---
Speaking of locomotives, you've still got half a day to buy [Train Simulator](https://store.steampowered.com/app/24010/Train_Simulator_2022/) at a steep discount, and a bunch of the routes and rolling stock. The game is [incredibly expansive](https://store.steampowered.com/dlc/24010/Train_Simulator_2022/lists/), so sales are a great way to build up a new library.

These are some of my favourite routes that are on special:

* [Marias Pass](https://store.steampowered.com/app/222561/Train_Simulator_Marias_Pass_Shelby__Whitefish_Route_AddOn/): My favourite line from the original Microsoft Train Simulator is even better here. Such an amazing mix of landscapes from flatlanes and lakes to hilly pine forests. I've joked that Ruben in an alternate timeline became a recluse in the Montana highlands near Whitefish; Clara and I will travel on the [Empire Builder](https://store.steampowered.com/app/222627/Train_Simulator_Amtrak_P42_DC_Empire_Builder_Loco_AddOn/) to get there one day!

* [Tadami Line](https://store.steampowered.com/app/1267502/Train_Simulator_Tadami_Line_AizuWakamatsu__Tadami_Route_AddOn/?curator_clanid=958135&curator_listid=43440): the most beautiful route I've tried, though you need a good GPU to render all that detail with the flowers and trees! While we can't go to Japan, we may as well explore it here.

* [Arosa Line](https://store.steampowered.com/app/621870/Train_Simulator_Arosa_Line_Route_AddOn/?curator_clanid=958135&curator_listid=43440): this Swiss route is challenging and beautiful, with cute trains and steep, winding curves leading to charming Swiss stations. I'd say its the most unique route in the game.

* [Stevens Pass](https://store.steampowered.com/app/222617/Train_Simulator_Stevens_Pass_Route_AddOn/): another stunning route, this time in the Cascade Mountains in Washington State in the US. I swear this route has done more to alleviate my anxiety than almost anything else.

And an honourable mention with caveats:

* [Northeast Corridor: New York to Philadelphia](https://store.steampowered.com/app/65232/Train_Simulator_Northeast_Corridor_New_York__Philadelphia_Route_AddOn/?snr=1_5_9__405): this is fun nostalgia trip, especially on the [Acela consists](https://store.steampowered.com/app/65231/Train_Simulator_Amtrak_Acela_Express_EMU_AddOn/?snr=1_5_9__405) that Clara and I were lucky to experience before the pandemic. Hi Frank, Denise, Esther, and Jim! Alas, it has a bunch of bugs which can get frustrating when doing some of the scenarios.

*(Full disclosure, Dovetail did not pay for this post!)*
