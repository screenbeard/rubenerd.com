---
title: "John Jakes, March Into Darkness"
date: "2022-03-22T22:17:28+11:00"
abstract: "A vision of gaunt shapes, sharp shiny steel, dim lamps flaring in the rain."
year: "2022"
category: Thoughts
tag:
- quotes
- ukraine
- war
location: Sydney
---
John wrote his American Civil War trilogy in the 1980s, but this haunting description could easily apply today. [Via Wikiquote](https://en.wikiquote.org/wiki/John_Jakes#March_into_Darkness)\:

> He saw it all summed up in the blind marching of that nameless unit. A vision of gaunt shapes, sharp shiny steel, dim lamps flaring in the rain. The war machine was rolling.
