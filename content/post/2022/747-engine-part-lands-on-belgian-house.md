---
title: "747 engine part lands on Belgian house"
date: "2022-09-14T10:52:53+10:00"
abstract: "... could I keep it?"
year: "2022"
category: Thoughts
tag:
- aviation
- news
location: Sydney
---
[Joanna Bailey reported](https://simpleflying.com/parts-of-air-atlanta-icelandic-boeing-747-fall-on-belgian-house/) for Simple Flying:

> An Air Atlanta Icelandic Boeing 747-400F, operated by Magma Aviation, shed an engine part during its climb out of Liege Airport earlier this week. The 747 was heading to Malta when the engine cowling fell from the jet, crashing down into the property of a local resident.

I bring this up, because would this fall under the law of *Finders Keepers?* I'm an aviation buff, and would love a piece of such an iconic aircraft on my wall!


