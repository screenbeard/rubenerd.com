---
title: "Australian energy company data collection"
date: "2022-02-11T14:12:18+10:00"
abstract: "We may also collect personal information about you... why?"
year: "2022"
category: Internet
tag:
- australia
- ethics
- privacy
location: Sydney
---
A large Australian energy retailer sent us another letter threatening to disconnect our non-existent gas supply if we didn't open an account and start paying. I'm tempted to take them up on the offer, then lodge a formal complaint that there's no gas being delivered over our invisible pipes!

The welcome pack came with their chunky *Standard Retail Contract*. I'm always interested to read sections on data collection and privacy, and this one was especially fun. Here's what they included on page 27, paragraph 6:

> We may also collect personal information about you by accessing data from other sources and then analysing that data together with the information we already hold about you to learn more about your likely preferences and interests.

Had I not known this was for a power and gas company, I'd think it was for a web startup. Have companies always been doing this? Or is this *learning about your preferences* stuff that's so rife on the Internet now spilling over to unrelated industries?

The language is full of goodies:

* You "may" collect? Does that mean we're granting you permission (yes, you *may* do this), or that there's only a *chance* that you'll do this?

* What are these "other sources"? What are their terms of use? How do they store data? How did they get our information? Where am I consenting to a business relationship with them?

* "Information we already hold" is what? How long do you hold it? Where? What data security is in place? Do you keep it private, or share it back with your "other sources"?

* What do our "likely preferences and interests" have to do with delivering gas to our homes that may or may not even have gas lines? Giving us coupons for heaters? Thinking that our "interest" is drying clothes? Or are you selling this information? If so, to whom? Are they given our contact details? How do they store our data?

These contracts go into so much legalese and detail on claims and terms for which they're potentially liable, but transparency around privacy and data security is still very much in the dark ages.

But above all, I just find it funny that a gas company wants to know my interests in the first place. I'm not sure my preferences for retrocomputers, anime, FreeBSD, and travel are that useful for someone selling hydrocarbons to a non-existent pipe.
