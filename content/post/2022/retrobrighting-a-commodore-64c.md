---
title: "Cleaning and retr0brighting a Commodore 64C"
date: "2022-12-21T09:17:47+11:00"
abstract: "I swapped out the Oxi Vanish for a bottle of hair developer, and it worked a treat!"
thumb: "https://rubenerd.com/files/2022/commodore-64c-looking-nice@1x.jpg"
year: "2022"
category: Hardware
tag:
- commodore
- commodore-64
- retr0bright
location: Sydney
---
A fortnight ago I [bought a Commodore 64C](https://rubenerd.com/i-bought-a-commodore-64c/) because I'd always wanted one, though I rationalised it by saying it was so I could try out carts that aren't compatible with my 128. It was in excellent working order, but the keyboard was a bit gross, and the case had an ugly, blotchy band of yellow along the front and edges of the case.

Last weekend I popped the keys off, removed the springs, washed the individually in warm water mixed with dishwashing detergent, then gave them a scrub with 303 cleaner as [Adrian's Digital Basement](https://adriansbasement.com/) recommended in a throwaway line on a video. They look practically brand new; I couldn't be happier with the results.

While the keys were soaking, I took a look at the computer case. The blotchy yellow didn't look half as bad on camera as in real life, but you can see the difference when compared to the inside of the case that received no sunlight:

<figure><p><img src="https://rubenerd.com/files/2022/commodore-64c-yellowed@1x.jpg" alt="Photo up close of the lower-right edge of the top part of the 64C case, showing cloudy yellow marks across the beige plastic surface. The case underneath is a uniform, lighter beige colour." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/commodore-64c-yellowed@2x.jpg 2x" /></p></figure>

I had success [retr0brighting my Commodore 128](https://rubenerd.com/retr0brighting-my-commodore-128-keyboard/) last year, so I thought I'd try again. This time I bought some hair developer cream from our local pharmacy to see if it'd be more effective than the Oxi Vanish that required days of soakings to do anything.

I put on gloves and smoothed the cream across the worst parts of the case, then wrapped it in plastic cling wrap and set it on our balcony for some Australian sun. Every half an hour I went back out and smoothed the cream around to reduce the chance of streaks where it was thicker than in other places.

We've been having an uncharacteristically [brisk summer](https://rubenerd.com/exploring-sydneys-xmas-decorations/) in Sydney again, so it ended up staying outside for much of the day. But by the evening I unwrapped the shell, gave it a solid soak in the shower, and was shocked at how well it worked. The blotchy texture was completely gone, and what was an ugly yellow streak is now an almost unnoticeable line. Paired with the freshly washed and buffed keys, and it almost looks new.

<figure><p><img src="https://rubenerd.com/files/2022/commodore-64c-looking-nice@1x.jpg" alt="Badly composed photo of the cleaned 64C in natural light, showing the uniform colour of the beige across the case. Yay!" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2022/commodore-64c-looking-nice@2x.jpg 2x" /></p></figure>

*(Light from the adjacent window makes the right side look a bit paler than the rest of the case, though it isn't the... case. Thank you. I love natural light, but maybe I need a lightbox for finer comparisons like this).*

The next step will be to figure out how to smooth out some small stippling marks on the front of the case, below the spacebar. I could try some light sanding, or maybe even applying a tiny amount of Brasso as Mat from Techmoan does for his plastic Hi-Fi gear.

As I said in my first 64C post, I adore the industrial design of this generation of Commodore hardware, and I'm chuffed to have this alongside the C128.
