---
title: "Password managers must encrypt metadata too"
date: "2022-12-27T09:07:41+11:00"
abstract: "The fact they didn’t is extremely concerning."
year: "2022"
category: Internet
tag:
- privacy
- security
location: Sydney
---
In Australia's previous government, attorney general George Brandis struggled to define metadata in one of the defining moments in modern political TV history. At one point he said it was details on an envelope, before recanting, then saying it was, sort of. His argument, when he eventually got to it, was that metadata was meaningless without the contents of the envelope anyway, and that security professionals were overplaying their significance.

This was, to use the technical term, nonsense. An envelope from a suicide line, then a GP, then a psychologist, can be interpreted pretty easily. PGP email has the same weakness.

LastPass's most recent security disclosures are worrying for this reason. While attackers can only hope to brute force the leaked binary blobs containing credentials (assuming we trust their implementation), the service is unique among password managers in that it doesn't encrypt the URLs of sites themselves. This has been known about for years, and it still floors me. I'm practically parquet at this point.

Even with a password manager, you and I know some people still use the same password across multiple sites. Being able to cross reference a leaked list of sites against compromised credentials you might already have makes attacking these accounts much easier. This is less of a concern with two-factor authentication, but you'd better hope victims have a different password for their email account if that's their way to recover credentials.

As worrying is its potential for profiling, phishing attacks, identity fraud, and extortion. Here are just a few examples:

* A URL list paints a picture of who someone is and what they do, which is useful for stalkers and other criminals.

* Have a password for an underground journalist site in a repressive country? That can now be public knowledge, and can be tied to your identity via other accounts you have.

* Victims are also more likely to trust scammers if they provide specifics about you, which they now have.

* If you thought "I hacked your webcam!" spam is scary for people who don't know better, wait till they threaten to publish specific sites you frequent. You almost couldn't think of a better extortion vector.

There's a wider discussion to be had that passphrases as authentication are well past due for replacement, that password managers help you use unique credentials per site, and that services doing this contribute to security. The worst outcome here would be for laypeople to think that password managers, while we still need to store and use passwords, are insecure.

I'm sure detractors are already victim blaming LastPass customers for storing such sites, but the reality is people were sold on password managers protecting them from such threats. If the outcome of this is that people need more education, so be it. But password managers needs to encrypt metadata. No added feature is worth such data leakage; even if your parquet is properly sealed.
