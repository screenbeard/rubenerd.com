---
title: "My first dev job, and using double pound"
date: "2022-07-05T08:46:36+10:00"
abstract: "## is easier to read, easier to disambiguate, and looks cooler!"
year: "2022"
category: Software
tag:
- nostalgia
- perl
- programming
location: Sydney
---
I had a friend (and crush!) in high school who's dad was after someone to do some short-term dev work at his engineering company. My jobs during school mostly involved voice acting and media production, so this was my first experience in a corporate office in Singapore performing an infocomm role.

I learned a lot from Peter. He was the one who introduced me to Perl, the programming language that still fits me better than any I've learned before or since. His dry sense of humour and wit had a lasting impact on mine. It's also hard to describe, but he had a specific lateral way of thinking that I've rarely encountered in industry since.

Clara and I went back to the company's office more than a decade later on our last trip to Singapore, but they'd moved and the original building razed. At least the Coffee Bean was still there! But I digress.

Perhaps the most visible impact he had on my work was the use of the double pound in code and shell script comments:

	## This greeting greets people with a greeting
	print("Hello, world\n");

He had a few reasons for doing this. Config files often have their own comments, so prefacing yours with a double pound makes them easier to disambiguate. A double pound is also easier to see when skimming source code, and with half-width characters they (closer) resemble a square, which looks nice visually. Or at least, I thought so.

I distinctly remember losing marks in an early programming course at university for "stylistic" reasons for doing this. I copped the penalty :).
