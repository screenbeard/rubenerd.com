---
title: "Queen Elizabeth II"
date: "2022-09-11T09:50:13+1000"
abstract: "Lots of conflicting thoughts, so I've listed them instead of trying to reconcile."
year: "2022"
category: Thoughts
tag:
- australia
- canada
- new-zealand
- politics
- uk
location: Sydney
---
I have lots of conflicting thoughts, so I've listed them here instead of trying to reconcile them. Make with them what you will.

* Liz was always just... *there*. Before I even knew who she really was, I saw he everyday on coins, banknotes, and in portraits at school. Now she's... *not* there. Granted we weren't close, but I've lost several people of that generation in the last few years, and I'd be lying if this latest one hasn't churned me up.

* Liz had, by several accounts, a wicked sense of humour. There was the story of her [meeting American tourists](https://twitter.com/davidmackau/status/1567894552744271872) who were oblivious to who she was, [trolling Mr Orange](https://thehill.com/blogs/in-the-know/in-the-know/397540-queen-elizabeth-wore-brooch-from-obamas-on-the-day-trump/) during a visit, and [personally driving the Saudi king](https://www.smh.com.au/world/europe/when-queen-elizabeth-took-the-saudi-prince-for-a-drive-20220628-p5axf1.html). It was the latter my mum especially admired, even though she didn't like the rest of the family.

* Liz represented a family that has wrought untold suffering and theft to people around the world, and altogether not enough has been done to atone and compensate for this, let alone acknowledge it. Charles III's coronation will also cost billions of pounds right when working class people are struggling to keep the heater on.

* To that point, conventional wisdom in Australia was that Liz's personal popularity far eclipsed that of her bumbling, scandal-ridden family, or the monarchy in general. I guess we're about to find out whether that's true.

* The world's relationship with Liz was more complicated than people on either side acknowledge. This is [most starkly represented in Hong Kong](https://www.sbs.com.au/language/chinese/en/article/we-love-her-hongkongers-mourn-death-of-lady-boss-queen-elizabeth-ii/j50689t88), where her family were responsible for the Opium wars on one end, and represented lost freedoms and [resistance](https://twitter.com/rhokilpatrick/status/1568010440265039872) since the handover.

* I'm a [republican](https://en.wikipedia.org/wiki/Republicanism_in_Australia), because the monarchy is an [aberration to democracy](https://rubenerd.com/the-queens-birthday-and-pouring-one-out-for-gough-whitlam/). I happen to think that if Commonwealth realms like Australia, Canada, and New Zealand are to have ceremonial figureheads, it'd be an honourable gesture to have a First Nations' member with ancestral links to the land be it. Otherwise, ditch the idea entirely; heads of state are an antiquated idea and [places operate without them](https://en.wikipedia.org/wiki/Federal_Council_(Switzerland)).

I guess the closest I can come to a conclusion here, is a phrase with eleven words. 👑
