---
title: "Taking a break e312643b"
date: "2022-10-19T20:49:50+11:00"
abstract: "That’s quite the UUID chunk."
year: "2022"
category: Thoughts
tag:
- perl
- personal
- uuid
- weblog
location: Sydney
---
We've got a lot going on for the next couple of weeks, so I'll be spending all my time on that instead of writing.

I couldn't remember how many breaks I've taken here over the years, so I appended the first four octets from a [Data::UUID](https://metacpan.org/pod/Data::UUID) output, like a gentleman, so I'd have a unique identifier and title for this post. I take these things seriously.

Catch you all again soon 👋.


