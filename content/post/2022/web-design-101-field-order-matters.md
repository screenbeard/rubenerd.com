---
title: "Web design 101: field order matters"
date: "2022-06-21T10:03:31+10:00"
abstract: "Waiting for a 2FA code to arrive, because the button was on the wrong side."
thumb: "https://rubenerd.com/files/2022/sendcode@2x.png"
year: "2022"
category: Internet
tag:
- accessibility
- design
location: Sydney
---
I couldn't log into a site over the weekend. No matter how many times I refreshed the page, or logged in from another browser, the promised two-factor code never arrived. Can you spot the issue?

<figure><p><img src="https://rubenerd.com/files/2022/sendcode@2x.png" alt="Screenshot of a web form showing a text field labelled 'Enter Code', followed by a 'Send Code' button on the right." style="width:500px; height:295px;" /></p></figure>

The code never arrived because I didn't press **Send code**. Once I did, the **Send code** button was disabled, and the **Verify** button below worked.

I wondered why such a simple UI perplexed me. Maybe I thought **Send code** meant "sending the code I just typed to the server". But I think it's even simpler than that.

Web forms have trained us to recognise text boxes on the left, and submit buttons on the right. The text box comes first because you have to type something before you can submit it. This is also consistent with the direction English speakers read text (as opposed to Hebrew, for example). Placing these fields in reverse order broke my mental model for how this works, and I was stuck refreshing the page like a schmuck... and not the [better kind](https://de.wikipedia.org/wiki/Schmuck "German Wikipedia article on schmuck, which refers to jewellery").

The modern web is replete with such examples, where convention is discarded for stylistic reasons, or through insufficient user testing. We wouldn't fault someone for being confused by a screwdriver with a bit on the same end as the handle, yet such inaccessible design [passes muster](https://idioms.thefreedictionary.com/pass+muster) on the web.

