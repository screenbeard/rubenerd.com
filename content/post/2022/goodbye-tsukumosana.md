---
title: "Goodbye, @tsukumosana 🪐"
date: "2022-07-31T13:54:39+1000"
abstract: "You brought Clara and I so much joy, right when we needed it the most. Thank you 💛"
thumb: "https://rubenerd.com/files/2022/goodbye-sana@1x.jpg"
year: "2022"
category: Media
tag:
- hololive
- tsukumo-sana
location: Sydney
---
It doesn't feel like almost a year ago since you [introduced yourself](https://rubenerd.com/tsukumo-sanas-debut-stream/) to our world. You brought Clara and I so much joy, right when we needed it the most. Your [bread stream](https://rubenerd.com/sana-and-baes-culinary-adventures/) remain one of my favourite Hololive streams I've ever seen.

Big love, and all the best for your future. 💛

<figure><p><a href="https://www.youtube.com/watch?v=t8vPcBsVIqY"><img src="https://rubenerd.com/files/2022/goodbye-sana@1x.jpg" alt="Goodbye Sana graphic from her last stream today." style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2022/goodbye-sana@1x.jpg 1x, https://rubenerd.com/files/2022/goodbye-sana@2x.jpg 2x" /></a></p></figure>
