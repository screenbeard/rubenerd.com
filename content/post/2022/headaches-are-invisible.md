---
title: "Headaches are (almost) invisible"
date: "2022-07-07T16:22:53+10:00"
abstract: "Headache sufferers don’t have crutches."
year: "2022"
category: Thoughts
tag:
- health
- personal
location: Sydney
---
Headaches and migraines are a pain in the arse. Well, pain in the *head* to be more specific. It's as if they're called that for a reason. There's a joke about Boris Johnson in there somewhere.

This is true for a host of obvious reasons. They hurt! But just as bad, they rob us of the ability to think. A [broken ankle is horrifying](https://rubenerd.com/i-broke-ground-on-sunday-with-my-ankle/ "Post about breaking my ankle in 2013"), but at least we can prop them up while we distract ourselves. Headaches reduce sufferers to irritable mush.

I don't care about garnering sympathy during a headache, and *especially* during a migraine. The universe could collapse for all I care. I need to be left alone in a dark, quiet place where I can sit upright and meditate; my forehead smothered with Tiger Balm while I try and keep a couple of strong pills down long enough to dissolve. People could think I'm "putting it on"; it couldn't matter less to me.

But it *does* suck a little after the fact, I'll admit! My ankle had a CAM boot on it, and I needed crutches to walk around. There are no outwardly visible signs of a headache or migraine to others. Worse are the people who say you can "deal with it", because it's all in one's head, right?

I'm lucky that I don't have anyone like that in my life. My work hears I have a migraine, and they take the load while I disappear for a day. Clara gives me a hug and leaves me to recover.

All that said though, I have a working theory that bad headache and migraine sufferers *can* see it in other people. I could tell when my mum had one; ditto when my brother-in-law has one of his regular episodes. It was all in their eyes.
