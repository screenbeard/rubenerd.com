---
title: "GamersNexus launches a Ukraine charity shirt"
date: "2022-04-29T07:35:23+10:00"
abstract: "Limited run shirt with all the proceeds going to Ukrainian food and animal welfare charities."
thumb: "https://rubenerd.com/files/2022/ukraine-gamersnexus@1x.jpg"
year: "2022"
category: Thoughts
tag:
- ukraine
location: Sydney
---
My favourite hardware review channel on YouTube has [launched another in their Hardware Heart series](https://store.gamersnexus.net/products/limited-charity-shirt-ukraine-gn), and it looks beautiful. From the description:

> 100% of the profit from the sale of this shirt will be given evenly to two charities (World Central Kitchen & International Fund for Animal Welfare). These organizations are donation-funded and are leading the charge on providing food and necessities to the people who have been affected by the conflict. World Central Kitchen supports both the remaining inhabitants of Ukraine and the refugees who have fled to neighboring countries. They work with IFAW on delivering food for people and their pets around Ukraine and outside of it.
>
> THIS IS A PRE-ORDER AND SHIRTS WILL BE PRINTED TO ORDER. Sales will close on 11th May 2022 and WILL NOT re-open.

Steve also discussed it on [his most recent news video](https://www.youtube.com/watch?v=cdpWBGomksg "HW News - AMD Silently Launches RX 6400, Zen 4 Pre-Production, GPUs Back In Stock") which is worth checking out.

They did a shirt for the Australian bushfires in 2019-20 which raised more than $20,000. Let's get the number even higher this time. Слава Україні. 💙💛

<p><a title="Limited charity shirt for Ukraine relief" href="https://store.gamersnexus.net/products/limited-charity-shirt-ukraine-gn"><img src="https://rubenerd.com/files/2022/ukraine-gamersnexus@1x.jpg" srcset="https://rubenerd.com/files/2022/ukraine-gamersnexus@1x.jpg 1x, https://rubenerd.com/files/2022/ukraine-gamersnexus@2x.jpg 2x" alt="Limited charity shirt for Ukraine relief." style="width:500px; height:334px;" /></a></p>
