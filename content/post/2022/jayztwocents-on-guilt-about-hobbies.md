---
title: "JayzTwoCents on guilt about hobbies"
date: "2022-02-23T14:27:56+11:00"
abstract: "“I’ve spent money on these hobbies... and then I think it’s a waste”."
year: "2022"
category: Hardware
tag:
- jayztwocents
- hobbies
- youtube
location: Sydney
---
I've only just started watching JayzTwoCents's YouTube channel, but he has so much fun and interesting stuff. His videos about everything from choosing graphics cards to using primers correctly for custom computer builds have been invaluable and fun, and I love how he interacts with his staff on the channel.

A month ago [he did a Q&A video](https://www.youtube.com/watch?v=8PPT1sak2Og) where someone asked him about hobbies which *really* helped me a lot:

> I have to round robin. I just imagine my hobbies are on a lazy Susan, and it's kind of *what do I have time for right now?* Sometimes it's difficult. Do I want to play guitar, play video games&hellip;? I don't have time for all of it. I really just have to sometimes be honest with myself and say *I want to do all these things, but what do I want to do most right now?*
>
> The problem is, sometimes guilt sets in a little bit. I've spent money on certain things like hobbies and whatnot, and if I'm not utilising them I feel like that's just a waste.

**YES!** Sorry, editorial comment.

> But I also know that having these different hobbies is kind of what keeps me sane. If you always fall back on the same thing all the time to try and keep your sanity, especially when the world is all kinds of crazy and living upside down right now, that hobby can also start to become frustration. Because if you have a bad day at it, then you're mad at your hobby!
>
> *What the hell!? This is supposed to be my vice, I'm supposed to be relaxing doing this, and it's just making me mad!*
>
> If I get in that particular mode, then I can literally just go to something else.
