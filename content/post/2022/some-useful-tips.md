---
title: "Some useful tips"
date: "2022-01-27T21:25:27+11:00"
abstract: "Here are some useful tips that may prove useful in a life of usefullness"
year: "2022"
category: Thoughts
tag:
- advice
- pointless
- tips
location: Sydney
---
In no particular order, here are some useful tips that may prove useful in a life of usefullness:

* Pencil tips
* Hat tips
* Poisoned arrow tips

Hmm, scratch that last one. Wait no, don't scratch *with* it.
