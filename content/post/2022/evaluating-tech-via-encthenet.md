---
title: "Evaluating tech, via @encthenet"
date: "2022-03-31T12:39:10+11:00"
abstract: "One of the hard parts of evaluating tech is viewing it through the lens of what was possible/available when the project started."
year: "2022"
category: Software
tag:
- architecture
- design
location: Sydney
---
[Via The Bird Site](https://twitter.com/encthenet/status/1509311458126925826)\:

> One of the hard parts of evaluating tech is viewing it through the lens of what was possible/available when the project started, not when it was finally delivered, possibly many years later.

I haven't thought of it this way before. My mind is racing in multiple directions!

So often I've come into a project with people professing their exquisite hindsight, forgetting that the project started when their chosen solution wasn't available, or is more often the case, wasn't production-ready. It's the system architect's version of hiring people with ten years of industry experience in a technology that's only existed for five.

Developers of consumer tech and web development frameworks have the attention span of gnats. After this you have orchestration platforms and tools, which seem to go in and our of vogue every few years. Then you have programming languages, and finally big iron stuff which can run for years. Most modern systems have to bisect every layer of this technical cake.

The existence of *the new* also doesn't invalidate the old (other than perhaps in hiring). There are huge Java, Ruby on Rails, and even FORTRAN code bases in production and being updated, today. There are people like me who still think tech operating systems like systemd (burn!) were a needless distraction, and still use POSIX-like tools when available.

An old colleague often referred to the "maturity tax", or paying more in perceived time or money for a mature, tested solution as opposed to the new shiny. Lately I've noticed it being paid in the future as well, with people who question why an architect used something from "back in the day".
