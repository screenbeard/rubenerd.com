---
title: "Maya Angelou on cynicism"
date: "2022-11-29T22:07:45+11:00"
abstract: "Cynicism means going from knowing nothing to believing nothing."
year: "2022"
category: Thoughts
tag:
- quotes
location: Sydney
---
I've seen a [couple](https://en.wikiquote.org/wiki/Maya_Angelou "Quote on Wikiquote") of [variations](https://www.brainyquote.com/quotes/maya_angelou_578818 "Quote on BrainyQuote") of this:

> A cynical young person is almost the saddest sight to see, because it means that he or she has gone from knowing nothing to believing nothing.

