---
title: "PCLZIP_ERR_BAD_FORMAT (-10) : Invalid archive structure"
date: "2022-04-05T08:13:24+10:00"
abstract: "You might not have curl or php-curl installed."
year: "2022"
category: Software
tag:
- bsd
- freebsd
- guides
- wordpress
location: Sydney
---
If WordPress spits out the above error message when installing plugins or themes, you might not have the required PHP curl installed, or a mismatched version.

Assuming you're using PHP 7.4, you'll want to install [php74-curl](https://www.freshports.org/ftp/php74-curl/) or [php-curl](https://pkgsrc.se/www/php-curl) on FreeBSD, NetBSD/pkgsrc, or Debian-based Linux distros.

This a perfect case study in bad error messages. A layperson would see that error and assume an invalid archive means... an invalid archive. It's technically correct, and entirely meaningless, to say a non-existent download would also be an invalid archive.
