---
title: "Five things I’m thankful for, October 2022"
date: "2022-10-18T08:44:44+11:00"
abstract: "Health, poppyseed bagels, Clara, my phone surviving another year, and actually having five things."
year: "2022"
category: Thoughts
tag:
- personal
- thankful
location: Sydney
---
I thought it was worth doing a post like this in light of more difficult family news.

* My personal health is good. I only wish it extended to more people I care about. No, this is supposed to be positive!

* Poppyseed bagels with smoked salmon and cream cheese, and a place locally that I can go during work-from-home lunch breaks to sit outside in the sun and have them.

* Clara, my friends, (most of!) my family, and my colleagues.

* My half-decade old iPhone 8 still being supported by the latest iOS, so I can kick the new phone purchase down the road another year, and budget the money to a camera instead!

* The fact I could think of more than five things. That *sounds* like a copout answer, but it's more true each time I read it.

