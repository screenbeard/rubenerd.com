---
title: "The Uber files"
date: "2022-07-12T09:55:30+10:00"
abstract: "I’m almost surprised it wasn’t worse."
year: "2022"
category: Internet
tag:
- ethics
- politics
location: Sydney
---
The Guardian [investigated and dropped this incredible story](https://www.theguardian.com/news/series/uber-files) yesterday:

> A leaked trove of confidential files has revealed the inside story of how the tech giant Uber flouted laws, duped police, exploited violence against drivers and secretly lobbied governments during its aggressive global expansion.
> 
> The unprecedented leak to the Guardian of more than 124,000 documents – known as the Uber files – lays bare the ethically questionable practices that fuelled the company’s transformation into one of Silicon Valley’s most famous exports.
>
> Uber undercut established taxi and cab markets and put pressure on governments to rewrite laws to help pave the way for an app-based, gig-economy model of work that has since proliferated across the world.

The cynic in me isn't surprised, and I'm sure I'm not alone in this. If anything, I expected it to be far scummier. But cynicism provides cover. This is jaw-dropping behaviour, and they shouldn't be let off the hook for any of it. This is our industry at its unfettered worst.

This is also independent of what one thinks of the taxi industry or other software companies as well, in case that inevitable [whataboutery](https://rationalwiki.org/wiki/Whataboutism) comes up.

