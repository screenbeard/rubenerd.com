---
title: "Running Unix commands in real life"
date: "2022-08-30T09:34:10+10:00"
abstract: "I elect for doas, scp, zzz, stty sane, and ps aux."
year: "2022"
category: Software
tag:
- bsd
- freebsd
- linux
- netbsd
location: Sydney
---
This was a fun mental experiment, via [nixCraft](https://twitter.com/nixcraft/status/1563886021435342848).

* **[sudo(1)](https://www.freebsd.org/cgi/man.cgi?query=sudo)** or **[doas(1)](https://man.openbsd.org/doas)** are the obvious choices, for overriding bad decisions.

* **[scp(1)](https://www.freebsd.org/cgi/man.cgi?query=scp)** to globally teleport wherever I want. I'd need to make sure I have access to a remote server, and keep the local server alive so I can upload myself back.

* **[zzz(8)](https://www.freebsd.org/cgi/man.cgi?query=zzz)**, for dealing with insomnia. I might want this even more than the first one.

* **[stty(1) sane](https://www.freebsd.org/cgi/man.cgi?query=stty)**, if my vision gets blurry, or if I need to clean my glasses.

* **[ps(1) aux](https://www.freebsd.org/cgi/man.cgi?query=ps)**, to see what unhelpful thoughts my subconscious mind is wasting time on, so I can **[kill(1)](https://www.freebsd.org/cgi/man.cgi?query=kill)** them.

