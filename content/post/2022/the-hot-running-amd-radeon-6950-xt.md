---
title: "The hot (running) AMD Radeon 6950 XT"
date: "2022-05-13T16:02:13+10:00"
abstract: "Nvidia, hold my beer."
year: "2022"
category: Hardware
tag:
- amd
- nvidia
location: Sydney
---
Hot on the heels of [Nvidia's new RTX 3090 Ti](https://rubenerd.com/nvidias-3090-ti/) graphics card, AMD announced a revision to their 6000 series including their high-end 6950 XT.

For those outside the niche world of high-end consumer GPUs, these are the halo cards that gamers look at from afar with binoculars, creative professionals judge the performance per watt/dollar and stick with their current kit, and the affluent purchase for bragging rights. I hope they have the case space, and an air conditioner hose to point directly at the card's intakes.

These cards *supposed* to be ridiculous, and a showcase of what the company is capable of producing. But impressions from the outside world are that Nvidia and AMD are inking out minor performance gains by throwing escalating amounts of power at the problem.

It feels like we're back in the Pentium 4 days, where clocks and performance were the only guiding consideration. Intel had to go back to the mobile chips to base their Core architecture on after NetBurst became more prophetic a name than they must have first thought.

Will we see a similar reset with GPUs? The rumours are that the 4000 series from Nvidia will be even more power hungry, and I've read journalists speculate that the RTX 3090 Ti was to get ahead of public perception before a wider launch of these space heaters.

Intel is also a wild card here. If they could offer even three quarters the performance of an equivalent AMD or Nvidia card but with half or less of the power draw, I'd be *very* interested. I think the planet would be too.

