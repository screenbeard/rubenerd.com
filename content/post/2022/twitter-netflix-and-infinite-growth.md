---
title: "Twitter, Netflix, and infinite growth"
date: "2022-05-11T14:06:23+10:00"
abstract: "Why do businesses need to grow ad infinitum?"
year: "2022"
category: Internet
tag:
- business
- twitter
location: Sydney
---
*(This was a draft post written a couple of months ago, before the latest musky buyout offer entered the fray. I think the rambling point here still stands though, so I'm publishing it).*

Tech journalists have been in a lather over Twitter's moribund market performance and user growth of late. The conclusion among the talking heads is clear: Twitter needs new management to be a going concern.

This is interesting to read in the context of Netflix, the video streaming service that announced they lost users for the first time. But while people are up in arms and talking about losses to their catalogue, the hostile introduction of ads, and cancelling important shows, the same assumption made about Twitter above goes unacknowledged.

Why do businesses need to grow *ad infinitum*? It's not just a moral question, it's one of simple physics. Even if desirable, is it possible?

The tech world is still trapped in the mindset that growth is the primary or only metric with which to gauge success. A stable, profitable business with happy employees and a good corporate culture is nonetheless shunned as being stagnant in IT, where it would be hailed as sustainable, or a bluechip elsewhere.

Whether Netflix is having a blip, or its part of a larger worrying trend remains to be seen. But if it's the former, what's the problem with a profitable company?

I suspect this is another symptom of the venture capital mindset that still dictates so much of IT. Some of us still haven't learned the lessons of the first Dot Com Bubble, and that was two decades ago. Bummer, now I feel old in addition to confused.

