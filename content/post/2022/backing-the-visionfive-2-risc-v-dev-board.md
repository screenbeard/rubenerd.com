---
title: "Backing the VisionFive 2 RISC-V dev board"
date: "2022-09-08T11:24:48+1000"
abstract: "An open source quad-core RISC-V dev board."
thumb: "https://rubenerd.com/files/2022/b7dc8884e5da14ebb9719ed7dbdd4685.jpg"
year: "2022"
category: Hardware
tag:
- 
location: Sydney
---
I just [backed a Kickstarter project](https://www.kickstarter.com/projects/starfive/visionfive-2?ref=ksr_email_backer_backer_confirmation) for the first time!

> VisionFive 2 - open source quad-core RISC-V dev board. High-performance quad-core RISC-V single board computer (SBC) with an integrated 3D GPU, 2G/4G/8G LPDDR

Here are the press photos, showing the front and back of the board:

<figure><p><img src="https://rubenerd.com/files/2022/b7dc8884e5da14ebb9719ed7dbdd4685.jpg" alt="Photo showing the front of the board, showing the GPIO header, LCD and DSI ribbon connectors, USB-C, USB ports, audio, HDMI, and dual Ethernet" style="width:500px;" /><br /><img src="https://rubenerd.com/files/2022/74b838e0b3fa41336c8b7ebec14067bd.jpg" alt="Back of the board showing eMMC, M.2 slot, FT card, and QSPI Flash" style="width:500px;" /></p></figure>

It will support various Linux distributions, but naturally I'll be seeing what BSD I can get on it too. I think it'll be a fun project.
