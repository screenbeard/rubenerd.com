---
title: "Having a uniform"
date: "2022-01-31T08:40:08+11:00"
abstract: "I don’t ever want to choose clothes."
year: "2022"
category: Thoughts
tag:
- nagato-yuki
- school
- shopping
- suzimiya-haruhi
location: Sydney
---
Pamela and I had a fun chat on Mastodon last week about clothes... a sentence I never thought I'd write. We both agreed that clothes shopping was the worst, and wondered why stuff doesn't just last forever.

They say high school were supposed to be the best years of your life, though the uniform was probably among the only good things about it. I didn't have to worry about what I'd wear for five days a week; it was all sorted. My year 12 jersey was about the only variation I had to worry about, and even then I forgot it half the time. Each morning it was a white shirt, khaki coloured slacks, and some awful socks. Done!

*(The flat, green skirts the girls wore would have been even simpler, especially in the ironing department. But then they had to wear these awkward shirts with an extra seam, so I guess we balanced out in the end. Yay mid-2000s international schools in Singapore)!*

I knew plenty of people who hated uniforms on account of feeling so stiff and uncomfortable in collared shirts... as opposed to collared greens which nobody should have been wearing in the first place. Wait, damn it, I should have told Jae Yun and Laura that's what they were wearing! Why am I only witty either by mistake, or a decade or more after it would have been funny?

<p><img src="https://rubenerd.com/files/2022/yuki-seifuku@1x.jpg" srcset="https://rubenerd.com/files/2022/yuki-seifuku@1x.jpg 1x, https://rubenerd.com/files/2022/yuki-seifuku@2x.jpg 2x" alt="Nagato Yuki wearing her uniform on the weekend with Kyon." style="width:500px; height:281px;" /></p>

Anyway I can empathise, but their utility in not having to decide weighed higher for me. Even today I wear collared shirts and chinos as my default outfit for work and personal outings, to the point where friends ask me why I'm wearing work clothes on weekends.

The remainder of my clothes are t-shirts from podcasters, now that I think about it. Those go under cardigans or open jumpers when the weather gets colder, or if I have to sit in aircon all day. Weirdly, I also realised they're *all blue* as well... I wonder if that was by chance or it was my subconscious not wanting to make decisions?

It's another reason I envy Bertie Wooster from P.G. Wodehouse's books. Having my own gentleman's gentleman to take my measurements and buy flattering clothes for my body type would be amazing. Especially if I could wear [this fabulously dapper stuff](https://twitter.com/Rubenerd/status/1487624050147229697). Yuki from the Suzumiya Haruhi universe had it right when she wore her uniform on weekends.

There are plenty of things I want to think about and research, but clothes are not one of them. 12-year old Ruben was right to be bored on those family shopping trips.
