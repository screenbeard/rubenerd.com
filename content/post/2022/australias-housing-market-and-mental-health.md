---
title: "Australian housing and mental health"
date: "2022-07-06T13:40:58+10:00"
abstract: "“financial worries could also have implications for mental health”"
year: "2022"
category: Thoughts
tag:
- politics
location: Sydney
---
[This ABC article](https://www.abc.net.au/news/2022-06-27/renters-facing-increased-pressure-cost-of-living-increases/101182030) was light on details on how Australia got into this current situation, though I'm glad it acknowledged this:

> &hellip; financial worries could also have implications for mental health.

Clara and I are [DINKs](https://www.investopedia.com/terms/d/dinks.asp) with no debts and comfortable incomes, and even we're reviewing our power bills and other expenses. I can't imagine the anguish of figuring out whether to stay warm, eat, or pay rent.

This is how we fail our society's most vulnerable people.
