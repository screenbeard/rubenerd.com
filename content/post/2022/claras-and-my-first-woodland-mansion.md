---
title: "Clara’s and my first Woodland Mansion"
date: "2022-02-09T08:35:11+11:00"
abstract: "More than 16,000 blocks from spawn, wow."
thumb: "https://rubenerd.com/files/2022/woodland-mansion@1x.jpg"
year: "2022"
category: Software
tag:
- games
- minecraft
location: Sydney
---
It took us more than a year on our little Minecraft server, buying a map from a villager, and wandering more than 16,000 blocks from our spawn point, but we got here!

<p><img src="https://rubenerd.com/files/2022/woodland-mansion@1x.jpg" srcset="https://rubenerd.com/files/2022/woodland-mansion@1x.jpg 1x, https://rubenerd.com/files/2022/woodland-mansion@2x.jpg 2x" alt="View of our Woodland Mansion, with a Nether Portal in the foreground." style="width:500px; height:275px;" /></p>

There was mercifully a nether portal just near it, so we can get back home faster. The next step is to make a little building for it to match the mansion, and build a nether rail back.

What's curious is that it didn't spawn *anywhere near a woodland* or forest at all. The area is featureless tundra. The map topology also changed completely as we walked closer to the building. Our world started in 1.16, so maybe 1.18 changed the area.

We thought we had some big buildings by now, but this mansion is *huge*. It probably drawfs the footprint of our scaled-down Suntec City replica.
