---
title: "What’s going on"
date: "2022-11-08T19:31:29+11:00"
abstract: "An update about my sporadic posts for the last couple of months. Thanks for reading."
thumb: "https://rubenerd.com/files/2022/chatswood-sky@1x.jpg"
year: "2022"
category: Thoughts
tag:
- family
- personal
location: Sydney
---
Hi everyone. 👋 

I've mentioned in a few posts that we're going through some issues. Many of you have since emailed some kind messages for which I'm hugely grateful. I suspected readers of this blog over the last eighteen years were wonderful people, and now I have proof. You know who you are.

<figure><p><img src="https://rubenerd.com/files/2022/chatswood-sky@1x.jpg" alt="Photo of a mid-afternoon in Chatswood" style="width:500px;" srcset="https://rubenerd.com/files/2022/chatswood-sky@2x.jpg 2x"/></p></figure>

My dad Rainer had another health scare again recently. He'd had heart trouble a few years ago for which he received a coronary bypass and a strict regime of follow up visits and lifestyle changes. The surgery terrified me for his sake, but it also made me question my own mortality in ways I wasn't prepared for, given how similar he and I look, and how healthy he'd been until then.

Fast forward to this year, and what was to be a routine test turned up something potentially worrying. Had it turned out to be the doctor's biggest fear, he would have been struck with the same disease and treatments my mum had for most of my sister Elke's and my childhood. Reintroducing tests, doctors, surgeries, and hospital visits into our lives surfaced a lot of repressed trauma.

Against his wishes, we also found out via someone who is either clumsy or blasé with this sensitive stuff. She's also shown no remorse or interest in apologising for repeatedly stepping in it, and exacerbating an already distressing experience. It's bitterly disappointing and unhelpful.

Needless to say, *is a phrase with three words!* This hasn't helped with some existing mental health issues that have accumulated over the last few years. It's lead to some dark places which I've since been pulled out of, but it was scary. I've felt that I can tackle a few things at a time, but when a sufficient number accumulates it all becomes a bit much.

I apologise if this all came across a bit self-centred; I promise I'll be back to my discussions of tech, coffee, anime figures, and travel just as soon as I'm over this mental hump. Thanks for reading. Time to watch an eclipse.
