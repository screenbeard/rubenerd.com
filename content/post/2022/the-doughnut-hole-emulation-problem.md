---
title: "The Doughnut Hole emulation problem"
date: "2022-12-18T08:02:54+11:00"
abstract: "We can run very old and new stuff, but not stuff from the recent past."
references: "https://86box.net/2022/03/21/why-not-p3"
year: "2022"
category: Software
tag:
- emulation
- virtualisation
location: Sydney
---
When Apple moved from PowerPC to Intel, there was sufficient performance headroom in the new architecture for us to run legacy software on Apple's Rosetta translation system. I ran the PowerPC Macromedia Fireworks on Intel for several years without feeling any difference. It's felt the same running Apple Silicon, and I've been told it was the same between Motorola and PowerPC.

But that headroom might not always be available, or it may be more complicated to achieve.

Graphics are the obvious example. My FreeBSD tower, with a recent CPU and GPU, can emulate Commodore hardware with enough performance to render sprites faster than the original; ditto an S3 card for DOS games, and those cool Windows 95 OpenGL screensavers. It can also run contemporary titles. But attempt to virtualise anything in the middle, such as an 2000s GPU to run an early DirectX title, and it drops more frames than a clumsy optometrist.

The 86box developers have a great blog post about why they're [not emulating newer CPUs](https://86box.net/2022/03/21/why-not-p3), but it could just as easily apply to anything more than a Voodoo2, for example.

Your best shot here is to run your retro (but not retro enough!) games in a hypervisor that supports PCI passthrough, and bypass the need to emulate the underlying graphics hardware entirely. This assumes you're running on the same architecture, and that the much newer GPU supports the platform upon which your game or workload was written.

Otherwise, you might need to keep that old tower around a bit longer, or build another one. No, Ruben, you don't have the money to build a PIII era computer right now.
