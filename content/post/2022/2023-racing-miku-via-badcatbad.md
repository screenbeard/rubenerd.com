---
title: "2023 Racing Miku, via @_BADCATBAD"
date: "2022-12-19T20:47:14+11:00"
abstract: "toridamono leaves a mark on the enduring Good Smile Racing franchise... WITH A BERET."
thumb: "https://rubenerd.com/files/2022/jbnhsu3b2h6a1@1x.jpg"
year: "2022"
category: Anime
tag:
- hatsune-miku
- hats
- racing-miku
location: Sydney
---
Clara's and my dear friend, costume maker, cosplayer, DJ, performer, and perennial self-doubter for no valid reason [@_BADCATBAD](https://twitter.com/_BADCATBAD) shared Good Smile Racing's [2023 Racing Miku](https://twitter.com/_BADCATBAD/status/1604436296579289088) on her timeline this evening, and it was necessary for me to pass this on:

<figure><p><img src="https://rubenerd.com/files/2022/jbnhsu3b2h6a1@1x.jpg" alt="" style="width:500px;" srcset="https://rubenerd.com/files/2022/jbnhsu3b2h6a1@2x.jpg 2x" /></p></figure>

I haven't got around to writing about my trip to the official Koei Tecmo store in Tokyo last month, in which I spent more money than I care to admit on Atelier Sophie and Ryza merchandise, including wall scrolls which I can't even hang up on account of being in a rental. But I'd have recognised [toridamono's](https://www.toridamono.com/) art style and... tastes, from anywhere!

Madam BAD admired the structure of her garments, some of which are nice nod to her [2015 version](https://rubenerd.com/racing-miku-2015/). I love her beret and her billowy shoulder sleeves (are they sleeves)? Her cuffs, hair ribbons, and bright belt are a nice touch too, as is her BERET. Did I mention her hat wear, and that I might have a thing for it?

Hats... Hats-une? Hatsune? *Gesundheit*.
