---
title: "Why my game PC runs FreeBSD and Kubuntu"
date: "2022-02-27T10:00:27+11:00"
abstract: "FreeBSD for Minecraft and tools, Kubuntu for Steam."
thumb: "https://rubenerd.com/files/2020/beastie@2x.png"
year: "2022"
category: Software
tag:
- bsd
- freebsd
- kubuntu
- openzfs
- zfs
location: Sydney
---
My sleeper PC game machine posts have generated feedback from people asking why I bother dual-booting FreeBSD with Linux (technically it's triple-booted with NetBSD, but that's for another post!) I've done variations of this post more than a few times now, but I'll never shy from taking people's questions as an opportunity to talk about one of my favourite platforms.

Linux is now my game platform. I used to dual-boot Windows like most people, but Steam and Proton have got *so good* at what I want to play, I was able to leave desktop Windows and its frustrating mix of patches, telemetry, ads, and bad UI behind. Couple that with Kubuntu's well-integrated KDE Plasma desktop, and I'm actually *happy* to boot into it. I can't overstate just how much dread I had booting into Windows (I run Windows Server enough at work!) that has been alleviated thanks to the tireless efforts of Valve.

*(Kubuntu LTS, being based on Ubuntu, is officially supported by Steam. I ran Debian previously, but having stuff just work, or knowing I'm not running anything exotic when I need to troubleshoot a launcher makes my life easier).*

<p><img src="https://rubenerd.com/files/2020/beastie@1x.png" srcset="https://rubenerd.com/files/2020/beastie@1x.png 1x, https://rubenerd.com/files/2020/beastie@2x.png 2x" alt="The BSD Beastie!" style="width:192px; height:208px; float:right; margin:0 0 1em 2em" /></p>

Which then raises the question, why continue to dual-boot with FreeBSD? I use Plasma there too, and the same desktop applications. Clara has joked in the past that she can't tell from walking past my screen which I'm booted into. I'll admit there have been times I've fired up Konsole and typed the wrong system commands.

It all comes down to the toolchain. Linux has things like Docker, but FreeBSD jails and OpenZFS snapshots are so ingrained into how I work now, I tinker and test everything with them (ditto illumos, now that I think of it). It's a genuine feeling of relief and joy to use this stuff compared to Linux.

I treat desktop Linux now like my work Macs: a graphical environment that's officially supported by applications I need such as Steam and Microsoft Office, and supports consoling into something better when I need to do real work. Even my Kubuntu partition still accesses our FreeBSD home server for the heavy lifting.

FreeBSD's graphics driver support is also sufficiently advanced that I can play games like Minecraft without having to reboot, which is where I spend most of my time. The progress the team and the Foundation have made on the [Linuxulator](https://freebsdfoundation.org/blog/update-on-freebsd-foundation-investment-in-linuxulator/) is also *mighty* impressive, and leads me to hold out hope that one day we'll be playing accelerated Steam games on FreeBSD too. That'd be the dream :).
