---
title: "Doc Searls versus printers"
date: "2022-01-28T08:23:18+11:00"
abstract: "“... meaning I was dealing with a feature.”"
year: "2022"
category: Hardware
tag:
- doc-searls
- quotes
- printers
location: Sydney
---
[This sentence](https://blogs.harvard.edu/doc/2022/01/15/bothering-with-brother/) wins the Internet for this month:

> After parking an access point (we have four in our house, all connected by Ethernet through a switch to the cable modem) right on top of the printer, I gave up, assumed it was bad, took it back and swapped it for another that had the same problem, meaning I was dealing with a feature.
