---
title: "B-words that are a letter off another"
date: "2022-07-16T17:13:58+10:00"
abstract: "It’s brash to run bash(1)."
year: "2022"
category: Thoughts
tag:
- language
- pointless
location: Sydney
---
* **Baccara** playing **baccarat**
* **Backset** a **backseat**
* **Backstab** a **backstay**
* **Baled** that **bale** with a **baler**
* **Ban** a **band** or **bank**
* A **bare** **bear** on a **brae**
* **Baste** a **bass** 
* **Bait** a **bat**
* Buying a **bath** with **baht**
* A **bead** of **bread** in a **beard**
* **Beat** a **beet**
* A **bit** of a **bite** in a **bin** you **bid** for
* A **blur** of a **bur**
* **Boo** to that **boot** or **book**
* It's **brash** to run **bash**
* **But**... a **bun** to **bunt!**
