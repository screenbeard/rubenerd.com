---
title: "Rejected cards and waking an IT team up"
date: "2022-12-30T09:00:45+11:00"
abstract: "Someone probably had their holiday break interrupted when my transaction failed."
year: "2022"
category: Internet
tag:
- cloud
- finance
- monitoring
location: Sydney
---
One of my payment cards has been flaky the last few days, to the point where multiple cafes and shops have timed out or rejected accepting it. The card has plenty of balance and isn't physically damaged; I suspect it's an issue at the payment processor.

I have cash and backup cards on different networks specifically for failure scenarios like this, but it's still disconcerting to be disconnected like that. I have money, in a database somewhere, that a payment processor refuses to connect to someone from whom I'm buying food.

Most of the time we're shielded from IT problems by legions of tired engineers working overtime to keep enough of it running, and probably getting less for it than they should. When enough holes in the Swiss Cheese model line up and expose this stuff for what it is, it's disconcerting!

The cloud isn't "just someone else's computer", just like commercial aviation isn't just someone else's Cessna. It's also someone else's rosters, architecture, monitoring, maintenance, upgrades, and fixes, often times with significant complexity and (hopefully) redundancy. But despite the best efforts of certain marketing teams claiming otherwise, there's no magic behind any of it. It's people all the way down.

Someone, somewhere, probably had their holiday break interrupted when my transaction failed. I hope they can get back to the Christmas pud soon.
