---
title: "It sure felt like a long weekend!"
date: "2022-08-21T19:24:17+1000"
abstract: "… at least, it sure dragged on."
year: "2022"
category: Thoughts
tag:
- personal
location: Sydney
---
Long in the sense that it dragged on, not that we had an extra day.

We were informed of a close contact who contracted Covid, so we self-isolated for the two days. We took a brief walk outside with masks, but otherwise stayed inside the apartment.

I still can't believe we did this for two years; even just a weekend of it left me feeling irritable and tired. My dad used to say the most exhausting thing you can do is nothing, and I've *really* come to understand this of late. Worse still, I missed seeing the sakura blooming in Sydney with friends for another year in a row now.

Some days you feel privileged, lucky, and grateful for at least having your health. That's still important to remember regardless. But other times you remember that *mental* health is also health. If only you could fix that easily with an ibuprofen and some Tiger Balm!
