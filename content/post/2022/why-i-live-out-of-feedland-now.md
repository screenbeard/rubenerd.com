---
title: "Why I live out of FeedLand for RSS now"
date: "2022-12-13T13:03:31+11:00"
abstract: "It uses a river-of-news format, it supports posts without titles correctly, it’s public, and you can customise it. Boom!"
year: "2022"
category: Internet
tag:
- feedland
- rss
location: Sydney
---
I've spent a lot of time [thinking about RSS](https://rubenerd.com/tag/rss/), and have defended it from charges that it's [irrelevant](https://rubenerd.com/the-surprising-backlash-against-rss/) or only useful for [plumbing](https://rubenerd.com/rss-is-more-than-plumbing/). But when it comes to actually *reading* RSS feeds, I've been everywhere.

[FeedLand](http://feedland.org/) is Dave Winer's latest project to bring a social aspect to reading and sharing RSS feeds. It gets four key things right:

* **It defaults to a river-of-news format**. Inboxes were a useful metaphor to teach people how to think about web feeds, but the analogy fails when you have thousands of unread posts. Think of it this way: do you treat social media as an inbox, or do you just want to read what's new?

* **It supports posts without titles**. A careful reading of the [RSS 2.0 specification](http://cyber.law.harvard.edu/rss/rss.html) shows that item titles *are not mandatory* to be considered well-formed. Almost all aggregators implicitly assume they are, and either fail to render entries correctly, or derive redundant text from entry description elements. I like using headings on my blog, but my [Mastodon posts](https://bsd.network/@Rubenerd) don't have them!

* **It's public.** Like blogrolls of yore, everyone's subscriptions are visible. If you click a feed you're subscribed to, you can see who else is, then click through and see other feeds they have. It makes RSS more discoverable and fun, just like the old days of Radio UserLand.

* **The end product is yours.** Your public, curated news river is yours, complete with the ability to inject your own CSS and JS. I styled mine to match my blog here, but you could see the potential to go much further.

And best of all, if you don't like it, export it as OPML. Easy.

This is [my FeedLand](http://my.feedland.org/rubenerd), and my [running log of notes](http://feedland.org/?river=http://data.feedland.org/feeds/Rubenerd.xml) about the service. I've also started a [tag](https://rubenerd.com/tag/feedland/) for related posts, and replaced the blogroll link in my sidebar to the site instead.
