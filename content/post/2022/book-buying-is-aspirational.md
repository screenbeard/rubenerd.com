---
title: "Book buying is aspirational"
date: "2022-11-05T08:45:23+11:00"
abstract: "A quote from Alan Baxter had me counting the books I’ve bought and have yet to read."
year: "2022"
category: Media
tag:
- books
- personal
- reading
location: Sydney
---
The imitable [Alan Baxter](https://twitter.com/AlanBaxter/status/1583561422759677952) brought me out of my self-imposed [blogging break](https://rubenerd.com/taking-a-break-e312643b/) with this great observation:

> Book buying is aspirational af. One day I'll have time to read them all. You can read when you're dead, right?

*Surely* I haven't done the exact same thing... right? I checked my bedside table and Kobo ebook reader for purchased books I've yet to read or finish. This is but a tiny fraction: 

<figure><p><img src="https://rubenerd.com/files/2022/books@1x.jpg" alt="Books from my Kobo, some (most?) of which I haven't read yet... whoops." style="width:500px;" srcset="https://rubenerd.com/files/2022/books@1x.jpg 1x, https://rubenerd.com/files/2022/books@2x.jpg 2x" /></p></figure>

*(I've gravitated back to light novels and manga over anime of late, not due to a misplaced belief that the proverbial book is always better, but because I can read them at my own pace).*

At my current rate of reading, I'll get through this backlog never. I don't think I'd want it any other way.
