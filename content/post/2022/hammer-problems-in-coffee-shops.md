---
title: "Hammer problems in coffee shops"
date: "2022-02-20T07:53:50+11:00"
abstract: "Every problem can be resolved with a hammer"
year: "2022"
category: Thoughts
tag:
- coffee-shops
- overheard
location: Sydney
---
Overheard this morning at one of our local coffee shops:

> "When you have a hammer, every problem looks like a nail."
>
> "Nah mate, *every* problem can be resolved with a hammer."
