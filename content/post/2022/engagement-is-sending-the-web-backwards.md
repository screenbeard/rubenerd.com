---
title: "Thinking aloud about web engagement"
date: "2022-08-06T08:55:15+10:00"
abstract: "As Michael Dexter notes, announcements wrapped in ads now eclipse the announcements themselves. Also quotes from Sandy Maguire and Doc Searls"
year: "2022"
category: Internet
tag:
- advertising
- indie-web
- longer
- privacy
- tracking
location: Sydney
---
Last Wednesday I talked about the growing trend of superficial Linux distro reviews, both on YouTube and in thousands of cookie-cutter websites. [Michael Dexter](https://callfortesting.org/) has lamented the fact that site wrapping software announcement with ads places higher in search results than the announcements themselves.

I have intimate experience with this. Software and writing I once published under my (now retired) alias would routinely get picked up and disseminated, usually without attribution. My primary blog here is now big and old enough that its harder to get away with this, but I still find people wrapping my words wholesale so they can get cents of ad revenue. I still continue to publish full articles in my RSS feeds, but I'm starting to understand why others only want to include summaries.

This is a fractal of what's going on at a much larger scale, and more people are starting to notice. [Sandy Maguire explores this in Reasonably Polymorphic](https://reasonablypolymorphic.com/blog/monotonous-web/index.html), emphasis added:

> Why would someone go through the hassle of making a website about something they weren’t interested in? In 2022, we have a resoundingly sad answer to that question: advertising. The **primary purpose of the web today is “engagement,”** which is Silicon Valley jargon for “how many ads can we push through someone’s optical nerve?” Under the purview of engagement, it makes sense to publish webpages on every topic imaginable, regardless of whether or not you know what you’re talking about.

As I said in that Linux desktop review post, I don't think *everyone* is guilty of this. But it does go part of the way to explain why we're seeing so many more of these mass-farmed videos and blogs, all saying broadly the same thing. Substance has been replaced with SEO (an abbreviation I've long thought a red herring), quality with quantity, and search engines like Google are, at best, enablers. There's a reason everyone thinks search results aren't as good now as they used to be.

Doc Searls touched on a similar point in mid-July when he used the evolution of television to [discuss subscriptions](https://blogs.harvard.edu/doc/2022/07/15/subscriptification/)\:

> Economically speaking, all that built-in smartness is about two things. One is facilitating subscriptions, and the other is spying on you for the advertising business.

It is interesting to think about. Sandy is correct that we'll need to technically architect better search engines to separate the wheat from the chaff, but we'll also have to address the incentives that lead people down this advertising, tracking, *engagement* stuff in the first place. People will continue to act this way as long as it makes financial sense. I keep going back to former Australian Prime Minister Paul Keating's comment that markets move behaviour.

The web seems to be cleaving in two directions: rubbish, and paywalls. I'd guess there are just as many people sharing knowledge, experience, and ideas as ever before, but they're being drowned out by an increasing tide of *churnalism*, theft, and low-effort spam. Sandy demonstrates as much when doing some basic geographic and health searches in the first linked post, some of which has already cost lives.

The necessity (or not) of paywalls and subscriptions is another massive topic, and one I may even be more sympathetic to than I first thought. I'd prefer to pay for a newspaper than have it track me, but that accepts the premise that those are the only two models in which the web (and society) can work, which I'm not yet willing to concede. It also has the pernicious side effect of locking out important information while spreading lies and misinformation.

The [people I read](https://rubenerd.com/blogroll.opml) are such a welcome reprieve from all this, and I love when I find someone new to add. That's the other takeaway I want people to get from posts like this: if you're reading my blog here and aren't writing yourself, I think you should! Let's tip the balance back towards good stuff again.
