---
title: "A Sydney (Harbour!) wave and a smile"
date: "2022-04-20T14:31:29+10:00"
abstract: "Walking across the bridge for some exercise."
thumb: "https://rubenerd.com/files/2022/bridge-walk-thumb.jpg"
year: "2022"
category: Travel
tag:
- exercise
- sydney-harbour-bridge
- sydney-opera-house
- walking
location: Sydney
---
The last week has been hard again, so I took an unscheduled break. The highlight of the long weekend had to be walking across the Harbour Bridge for a change of scenery with Clara and her Blue Bear Cat. The break in our monsoon weather was rather nice.

Hope you're well. We'll return to our regularly-scheduled programming soon.

<p><img src="https://rubenerd.com/files/2022/bridge-walk@1x.jpg" srcset="https://rubenerd.com/files/2022/bridge-walk@1x.jpg 1x, https://rubenerd.com/files/2022/bridge-walk@2x.jpg 2x" alt="Photo taken from the Sydney Harbour Bridge on a fresh but partly-cloudy day." style="width:500px; height:333px;" /><br /><img src="https://rubenerd.com/files/2022/bridge-walk-bear-cat@1x.jpg" srcset="https://rubenerd.com/files/2022/bridge-walk-bear-cat@1x.jpg 1x, https://rubenerd.com/files/2022/bridge-walk-bear-cat@2x.jpg 2x" alt="Photo of Clara's little Blue Bear Cat enjoying the sunshine in Chatswood." style="width:500px" /></p>
