---
title: "The iPhone 14"
date: "2022-09-08T10:43:17+1000"
abstract: "If I wanted to vomit after spending a thousand dollars, I'd buy some fugu!"
year: "2022"
category: Hardware
tag:
- accessibility
- iphone
- oled
location: Sydney
---
This is a reminder that the latest iPhones, once again, only ship with OLEDs, which millions of people can't use without adverse effects. If I wanted to vomit after spending a thousand dollars, I'd buy some fugu! 🐡

This year, let's have the [OLED Association](https://www.oled-a.org/people-with-high-flicker-sensitivity-bothered-by-oled-displays-_061221.html) explain\:

> [&hellip;] the eye responds to flicker, and the iris expands and contracts according to changes in brightness. This involuntary physiological reaction can explain the cause of headaches [&hellip;] 
> 
> The PWM frequency range of the OLED screen is ~50~500 Hz, while LCD starts at around 1000 Hz or higher. Second, since the human eye is sensitive to flicker up to 250 Hz (at least for most people), OLED screens are more likely to cause eye fatigue than LCDs.

Apple used to respect accessibility. I'm perfectly happy with the iPhone SE and 8, but it sucks for people who want these generational improvements, because a trillion dollar company can't justify an LCD version, despite them previously being their [best selling SKUs](https://www.techradar.com/news/iphone-xr-was-best-selling-smartphone-of-2019).

I'll call this out each time.
