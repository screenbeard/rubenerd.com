---
title: "The @Om summarises music software"
date: "2022-05-26T22:09:47+1000"
abstract: "It is not just Apple. All music apps at present suck. From Spotify to Tidal to Qobuz to Apple. Do a comparison — all shit."
year: "2022"
category: Software
tag:
- music
- om-malik
location: Sydney
---
Literally two days after I wrote about the [current state of music players](https://rubenerd.com/the-first-iphone-and-portable-music-players-in-2022/), Om Malik [chimed in](https://twitter.com/om/status/1529430181982613505)\:

> It is not just Apple. All music apps at present suck. From Spotify to Tidal to Qobuz to Apple. Do a comparison — all shit.

It's the frustrating thing with so much current consumer tech. It could be rose tinted glasses, but I *swear* certain things were better before.
