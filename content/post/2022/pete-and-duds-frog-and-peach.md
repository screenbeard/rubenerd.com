---
title: "Pete and Dud’s Frog and Peach"
date: "2022-05-17T17:19:16+10:00"
abstract: "We have frog à la pêche. It’s one of the most disgusting things I’ve ever seen."
year: "2022"
category: Media
tag:
- comedy
- pete-and-dud
location: Sydney
---
I have a local playlist of old-time radio and English comedy that I play when I need a bit of cheering up. Clicking *Random* this afternoon gave me this wonderful routine from the legendary English duo. I'm sure it's on the Internet somewhere:

> **Dudley Moore:** I'm talking tonight, to Sir Arthur Greeb-Streebling.
>
> **Peter Cook:** Oh no you're not.
>
> **Dudley Moore:** I'm not?
>
> **Peter Cook:** You're not at all. You're talking to Sir Arthur *Streeb-Greebling*.
>
> **Dudley Moore:** Oh, I...
> 
> **Peter Cook:** You're confusing me with Sir Arthur *Greeb-Streebling!*
>
> **Dudley Moore:** I'm so sorry.
>
> **Peter Cook:** Yes, well my name is *Streeb-Greebling*. The "T" is silent, as in fox.

Regarding the food served at his new restaurant:

> **Peter Cook:** We have *frog à la pêche*. It's one of the most disgusting things I've ever seen.

And on the transport options to get to the establishment:

> **Peter Cook:** Stuck out here in the middle of a bog, in the heart of the Yorkshire moors, there's no problem parking the car. A little difficulty in *extracating* it...

