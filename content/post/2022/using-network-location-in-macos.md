---
title: "Using Network Location in macOS"
date: "2022-10-02T15:53:39+10:00"
abstract: "Defining different settings depending on where you are. How did I only learn of this!?"
thumb: "https://rubenerd.com/files/2022/macos-location@2x.jpg"
year: "2022"
category: Software
tag:
- guides
- macos
- tips
location: Sydney
---
I've been using modern macOS since the original Mac OS X betas were shipped on CD. Yet this is, to the best of my memory, the first time I've ever used the Location feature for networks.

If you open **System Preferences** and choose **Network**, there's a dropdown labelled **Location**. It's set to **Automatic** by default, but if you select it, you can choose **Edit Locations...** and add another. From then on, subsequent network settings will only be changed for that location. 

Cooler still, you can then select that location from the Apple menu. Now I have all my custom DNS, IP, and VPN settings available within two clicks:

<figure><p><img src="https://rubenerd.com/files/2022/macos-location@1x.jpg" alt="Screenshot showing the Location submenu on the Apple menu, with a list of places I've defined" srcset="https://rubenerd.com/files/2022/macos-location@1x.jpg 1x, https://rubenerd.com/files/2022/macos-location@2x.jpg 2x" style="width:500px; height:302px;"/></p></figure>

*Schveet!*
