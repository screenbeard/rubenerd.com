---
title: "Bandcamp bought"
date: "2022-03-04T09:17:47+11:00"
abstract: "We badly need to treat artists better."
year: "2022"
category: Media
tag:
- independent-musicians
- music
location: Sydney
---
When market forces lead CD Baby to wrap up, I felt pangs of concern for people who's livelihoods depended on independent distribution. I redirected my money and links to Bandcamp, though I feared we were just delaying the inevitable there too.

I saw the news that Bandcamp has been bought my Epic Games. I know little about them, but my gamer friends *burn* with loathing for them which doesn't bode well. I *do* know they're dabbling in NFTs, which illustrates they have no interest in independent creators, instead opting into the redundant, planet-burning grift.

I'm starting to think we can't rely on the private sector for distribution like this. We need to establish a trust of some sort, or a non-profit. This is too important a project to leave up to the whims of people who'll cash out. What that would look like, and how it would work across a global audience base, I'm not sure. But I haven't been able to stop thinking about it.

I say this as a technical person: we *badly* need to treat our artists better. I'm beyond tired of people in my industry screwing over those who make our lives worth living.
