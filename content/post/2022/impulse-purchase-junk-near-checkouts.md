---
title: "Impulse-purchased junk near checkouts"
date: "2022-05-22T08:10:45+10:00"
abstract: "I thought this was common knowledge, but I saw this CNN article floating around."
year: "2022"
category: Thoughts
tag:
- business
- health
location: Sydney
---
I thought the reason for snacks near checkouts was common knowledge, but I saw [this CNN report](https://www.cnn.com/2022/05/21/business/impulse-shopping-stores/) shared with surprise in a few places:

> "Impulse purchasing represents a much, much larger component of consumer behavior than people realize," said James Burroughs, who studies consumer patterns at the University of Virginia's McIntire School of Commerce. "The front of the store is prime real estate to put impulse items."
> 
> [S]tores map out nearly every inch of their physical environment to influence shoppers' decisions. For example, the dairy case is placed way in the back of stores, forcing customers to wander and scoop up plenty of other products before they buy milk. The meat case is often over on the other side of the store to get shoppers to walk around and toss even more items into the cart.

One thing I didn't know was that people are trying to change this:

> Top grocery chains in the United Kingdom have eliminated candy from checkout altogether. In the United States, Berkeley, California, passed a "healthy checkout" law in 2020 regulating which products can be sold near the register. Out: junk food, candy and soda. In: fresh or dried fruits, nuts, yogurt and sugar-free gum.

This is a great idea at addressing a clear market failure. A healthy population is better than an unhealthy one, so making the choice for the latter easier for people can only be a good thing.

Nuts have been great for this personally. A small handful from my desk with a cup of black coffee can keep me full and happy for a long time, and was another great way to wean myself off sugar. Even the last dark chocolate bar I had a bite of tasted sickly sweet before I gave it up for an almond.
