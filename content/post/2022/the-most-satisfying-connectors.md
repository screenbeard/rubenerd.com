---
title: "The most satisfying connectors"
date: "2022-03-29T13:47:50+11:00"
abstract: "@Chordbug made a most excellent observation"
year: "2022"
category: Hardware
tag:
- connectors
- design
- engineering
- ergonomics
- networking
location: Sydney
---
[@Chordbug wrote](https://twitter.com/chordbug/status/1508406177830510603) this excellent observation:

> I think Ethernet cables are simply the most satisfying ones to plug in/out of a computer

I would tend to agree. When the clips aren't broken or obscured by mushy plastic, they have among the most satisfying and rigid clicks. The only connector I liked more were those coax Ethernet cables you'd screw and click into place on those T-shaped bus connectors.

We had some weird connectors in my past process control life, and the main thing I remember about them was the reassuring click when connected. Think old school Centronics or SCSI port connectors, but with a latch mechanism.

QDR+ InfiniBand, Fibre Channel, and SFP transceiver plugs also feel like you're sheathing a sword, which is a bonus.

On the other side we have USB-C, which feels mushy, imprecise, and has zapped me more than any other connector I've ever owned, to say nothing of its multitude of incompatible modes and cables.
