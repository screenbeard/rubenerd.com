---
title: "Time involved in healthy eating, by @FoldableHuman"
date: "2022-07-29T13:41:35+1000"
abstract: "As usual, it’s easier to judge working class people."
year: "2022"
category: Thoughts
tag:
- food
- health
location: Sydney
---
Dan Olsen of *Folding Ideas* made a video last year about [Jamie Oliver's chicken nugget aversion](https://www.youtube.com/watch?v=V-a9VDIbZCU). It's a brilliant metaphor for so many economic and social issues, not just a critique about whether you're using the "dirty" part of a bird.

He concluded:

> It's an extension of the argument that poorness is the result of food choices, rather than food choices being largely dictated by being poor.
>
> The biggest hole is the issue of time. With [cooking store-bought nuggets, and making them from scratch], the linear time is about comparable. But what's not is the actual amount of work. It's so self-evident I feel insane even pointing it out. Jamie's fried chicken bites are good, but you spend *the entire time* making fried chicken. Plus, it generates a huge amount of mess that you'll then need to clean up.
> 
> Food prep is extremely time and energy intensive, and it's maddening that so much of the hey about healthy eating relies on pretending that it's not.

There are entire YouTube channels and lines of books that claim to reduce food prep times and lead you to healthier meals, some of which I even watch and read. But it's ridiculous that we rarely, if ever, talk about the systemic issues that lead to the situation where time equals health in the first place.

It reminds me of that *South Park* episode where the new Walmart and subsequent collapse of Main Street are blamed on people in the town penny pinching, with only a shallow exploration of the economic circumstances that lead to it. As usual, it's easier to judge the unwashed masses for not saving what precious little money they have.

