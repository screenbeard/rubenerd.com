---
title: "The double-barred finch"
date: "2022-03-06T20:48:25+11:00"
abstract: "That’s a handsome little bird."
thumb: "https://rubenerd.com/files/2022/double-barred-finch@1x.jpg"
year: "2022"
category: Thoughts
tag:
- birds
- photos
location: Sydney
---
I haven't posted a [featured bird photo](https://rubenerd.com/tag/birds/) for a while. Here's a handsome double-barred finch, [taken by JJ Harrison](https://commons.wikimedia.org/wiki/File:Taeniopygia_bichenovii_2_-_Glen_Davis.jpg) and featured on Wikipedia today.

<p><img src="https://rubenerd.com/files/2022/double-barred-finch@1x.jpg" srcset="https://rubenerd.com/files/2022/double-barred-finch@1x.jpg 1x, https://rubenerd.com/files/2022/double-barred-finch@2x.jpg 2x" alt="Photo of a double-barred finch perched on a tree branch, by JJ Harrison" style="width:500px; height:333px;" /></p>

The site had [this to say](https://en.wikipedia.org/wiki/Altern-8 "Altern-8 on Wikipedia") about the bird:

> Best known in the early 1990s, their trademark was electronic rave music with a heavy bass line. Notable Altern 8 tracks included "Activ 8", "E-vapor 8", "Frequency", "Brutal-8-E", "Armageddon", "Move My Body", "Hypnotic St8" and "Infiltrate 202"

That's clearly the wrong article.

> The double-barred finch (Stizoptera bichenovii) is an estrildid finch found in dry savannah, tropical (lowland) dry grassland and shrubland habitats in northern and eastern Australia.
