---
title: "Some blog ideas I’m considering for 2023"
date: "2022-12-29T09:19:19+11:00"
abstract: "Server-side blogging, image hosting, more Rubi mascot art, Gemini, and more interesting stuff with RSS"
thumb: "https://rubenerd.com/files/2022/rubi-padoru.png"
year: "2022"
category: Internet
tag:
- blogging
- wordpress
location: Sydney
---
In no particular, general, specific, numerical, or other order that one may sort such a set of ideas:

* Going back to a server-side blog engine. Static site generation is fast, but it's a pain in the posterior if I'm not in front of my primary computer. Not sure what it'd be.

* Hosting an image gallery package, or writing my own. I stopped using Instagram, and have weaned myself off the dopamine hits you get from likes, but I'd still like to share photos from trips. Heck, maybe it'd be a static webpage.

* Clara has offered to draw Rubi in more clothing styles too, which I'm looking forward to!

* Running a Gemini server for specific topics and ideas that would get me trolled or "reply guy'd" on a public HTML server. I think it'd be a fun experiment.

* More cowbell. Do people still say that?

* Doing more interesting things with RSS, namespaces, and post metadata. I think there's potential to deliver new things, or even classify and render different types of posts with alternative syntax. *We have the technology.*

* Related to the above, classifying blog posts depending on their data. Maybe I could finally branch out into quote, snippet, image, or even code posts. I'd love to revive my git lunchbox and integrate it somehow.
