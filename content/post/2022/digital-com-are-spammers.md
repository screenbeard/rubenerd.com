---
title: "Digital.com are spammers"
date: "2022-08-20T09:44:04+10:00"
abstract: "Naming and shaming people who do this."
year: "2022"
category: Internet
tag:
- spam
location: Sydney
---
I got some more ["you wrote about X, can you link to me?"](https://rubenerd.com/you-wrote-about-x-can-you-link-to-me/) spam, this time from Digital.com. And of course they [followed up](https://rubenerd.com/did-you-have-a-chance-to-review-my-last-email/).

> My name is Carrie Smith, I’m a Research Manager here at Digital.com and I wanted to reach out about your post on your website https://rubenerd.com/starting-a-blog-and-unoriginal-ideas/ - I found it to be really helpful!
>
> After reading through your site and seeing the topics you cover, I thought I’d connect with you and share our recently published resource on web hosting, as it might be something your readers would find useful.

[Manton Reece](https://www.manton.org/2020/10/19/ive-fallen-behind.html) also talked about the scourge of this back in 2020.

Before you fall into the trap of thinking this is a legitimate outfit, I've received multiple emails from them using various obfuscated email addresses. The last was from *dgtl.email*. Legitimate businesses don't do this.

Consider this a name and shame, Digital.com. If you get email from them, do us all a favour and train your spam filters against them. I've also reported them to their hosts.
