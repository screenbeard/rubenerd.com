---
title: "Website security word salad"
date: "2022-12-01T09:04:38+11:00"
abstract: "Saying you use the “latest technology” to ensure “complete safety” means absolutely nothing!"
year: "2022"
category: Internet
tag:
- privacy
- security
location: Sydney
---
I collect security FAQs. Like some of my high school essays, they often say a lot and nothing at the same time. Pardon, they impart information with sufficiently impressive volume, length, and scale without communicating anything of meaningful substance to the reader of the aforementioned content whatsoever and under any reasonable circumstance. *Still got it*, Mrs Gravina.

Here's one for a dodgy looking file-sharing site, to use technical funicular. Wait, that's a type of [railway](https://en.wikipedia.org/wiki/Funicular).

> We at **$SITE** take matters of security very seriously. We use the latest technology to ensure that not only account is safe and secure, but the payment procedure and any traffic exchange between you and **$SITE** is fully encrypted. That means that both incoming and outgoing traffic between you and **$SITE** net can’t be hijacked or compromised in any other way ensuring complete safety and anonymity.

This is CSI Miami-level fluff. What sort of encryption? Where? For what data? Not compromised "in any other way" is either careless phrasing or a Freudian slip, and "complete safety" is impossible.

The payment processor section attempts to be more prescriptive:

> **$SITE** utilizes state of the art 256-bit data and traffic encryption with RSA, ensuring your privacy is always protected. Top level SSL technology protects whatever actions you do on site and keeps your info safe at the moment of purchase. **$SITE** security is on par with what world’s top banks use in their on-line security setups. **$SITE** has a lot of payment options. Each and every single one is fully secure against any wrongdoers.

There's little substance here. It can be distilled to *we use SSL*, which has been [deprecated since 2015](https://en.wikipedia.org/wiki/Transport_Layer_Security#SSL_1.0,_2.0,_and_3.0).

That might be sufficient to assuage concerns of the general public, but anyone technical wouldn't be impressed. Missing from this is how your data is used, stored, complies with legislation, and how you can report issues. "Fully secure" is impossible, RSA doesn't ensure privacy is protected, and keeping your data safe "at the moment of purchase" implies it's not after the fact. The payment options are also not elaborated, nor are their security or privacy documents provided.

I originally ended this post saying you and I aren't the target audience, and that such faff is fine for sales. But that's not true; an FAQ that puts out inaccurate, incorrect information like this is [worse than not saying anything](https://rubenerd.com/paypal-account-spam/).

Any site that has such a policy should either be avoided, or treated with extreme care and suspicion. Unlike *real* salads, which are excellent. 🥗

