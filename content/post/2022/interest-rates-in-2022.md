---
title: "Interest rates in 2022"
date: "2022-07-01T13:01:25+1000"
abstract: "“Neither the RBA nor other central banks around the world have explained how raising interest rates will mitigate the underlying reasons for the inflationary spike”."
year: "2022"
category: Thoughts
tag:
- australia
- economics
- news
location: Sydney
---
Australian professor Chris Wallace [perfectly summarised](https://asia.nikkei.com/Opinion/Australia-s-central-bank-s-reasoning-needs-rethinking) something I've been thinking about the last few months, for the Nikkei Asia Review:

> Neither the RBA nor other central banks around the world have explained how raising interest rates will mitigate the underlying reasons for the inflationary spike, namely higher energy prices, labor shortages and related pandemic-driven global supply chain disruption.

Not to mention the upward pressure on food and raw materials thanks to Putin.

Interest rates are used, theoretically, to encourage or discourage demand. But to use a tech analogy, I feel like we're operating a black box right now, and with completely different inputs. We're in uncharted waters. Fiscal policy also needs to play a role, like a chocolate horn. I could keep these tenuous analogies going all day, but even I’m losing interest.
