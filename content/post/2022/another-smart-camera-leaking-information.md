---
title: "Another smart camera leaking information"
date: "2022-12-31T08:53:54+11:00"
abstract: "Who’d have thought these companies would lie about what their hardware does!?"
year: "2022"
category: Hardware
tag:
- iot
- privacy
- security
location: Sydney
---
As the *tech guy* among friends and family for years, people are surprised by my militant (that's foreshadowing!) reluctance to buy smart, Internet-connected home devices. I wish it were based on a philosophical stance on privacy and sticking it to the proverbial man who wants to track everything we do to sell us rubbish... or heck, even for the mundane reason that half the stuff never works properly!

> Hey Ruben, check out my new smart pantry door! Wait no, it worked last time, give it a second... here it goes! No, sorry, must be my router... should work any second...

The reality is smart device manufacturers have proven themselves incapable and unwilling to deliver anything meeting even the most minimum of security standards, and they have *zero* excuses. Well, zero good excuses; I'm sure they have plenty of reasons to wave away their incompetence. There's a frustrating and inexcusable lack of maturity in much of industry, with all the same basic mistakes other IT had to go through.

Worse, most of the tech press is blasé about it in their reviews, because who cares if people have credentials or personal data stolen? In the [words of Mikko Hyppönen](https://twitter.com/mikko/status/808291670072717312), any appliance described as being “smart” is vulnerable. George Neville-Neil assures us though that the [S in IoT](https://rubenerd.com/iot-and-hypponens-law/) stands for security.

I bring this up again, because yet another smart camera device manufacturer [was caught out](https://www.techdirt.com/2022/12/07/anker-tries-to-bullshit-the-verge-about-security-problems-in-its-eufy-smart-camera/) earlier this month making claims about their tech that were unsubstantiated, and doubled down when security researches proved otherwise. This is completely predictable if still frustrating. But in this case, the company even falsely claimed their devices kept video local, in an attempt to assuage concerns about smart devices in the first place. This is worse than false advertising, it's fraud.

But hey, they claimed it had [military-grade encryption](https://rubenerd.com/website-security-word-salad/)! All they need was to start each sentence with a Zuckerbergian "So," and sprinkle in some disruptive paradigm synergies to really sell it.

You can use these devices responsibly by:

* isolating them on their own networks, and

* acknowledging that *everything* stored and transmitted through them was, is, and always will be, public.

But we all know that's not what buyers are told. If you don't think so, ask your non-technical family and friends about how they configured their new smart device.

