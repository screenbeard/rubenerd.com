---
title: "My Apothecary Coffee mug"
date: "2022-03-24T08:25:21+11:00"
abstract: "The first in my new series sharing my aqueous comestible conveyances."
year: "2022"
category: Hardware
tag:
- coffee
- girls-und-panzer
- mugs
- pointless-anime-reference
location: Sydney
---
Given how much of my life I spend brewing in, lifting, drinking from, and washing these aqueous comestible conveyances, I thought it'd be fun to share some of them here, and a bit of the history behind them.

We start this inagurel... inaugural... ina... *first* episode with our most recent purchase. This black mug comes from [Apothecary Coffee](https://www.apothecarycoffee.com.au/) near where we live in Chatswood, pictured with Darjeeling from *Girls und Panzer* holding her own namesake beverage, an Adelaide Starbucks mug we used as a flowerpot, and my Ryzen CoolerMaster NR200P computer case. Reviews for all of these are pending, especially the crockery.

<p><img src="https://rubenerd.com/files/2022/mug-apothecary@1x.jpg" srcset="https://rubenerd.com/files/2022/mug-apothecary@1x.jpg 1x, https://rubenerd.com/files/2022/mug-apothecary@2x.jpg 2x" alt="Photo of the black Apothecary coffee mug next to an anime figure of Darjeeling holding a cup of tea" style="width:500px; height:333px;" /></p>

Apothecary Coffee is one of my new favourite places. In Adelaide I practically lived in the Boatdeck Café in Mawson Lakes, and in Singapore I had the Viennese Coffee House and a few Coffee Bean and Tea Leafs. Apothecary Coffee has reached that level for me in Sydney. Their batch brew coffee is exquisite, the staff are friendly, and I love the atmosphere.

The coffee shop is situated on the commercial side of Chatswood, so they were hit especially hard by Covid and remote work. Clara and I would often walk down there and be their only customers for upwards of an hour at a time. We started buying their swag to support them, but they turned out to be great purchases in their own right. Like this mug!

It's a solid device with thick walls, a generous handle, and is machine washable. My only criticism is the shiny black surface is *almost* impossible to photograph without reflections obscuring the logo. Fortunately, this does not affect its hot beverage capacity, and I can only assume the colour's light absorption characteristics help to keep contained beverages hot. That's how that works, right?
