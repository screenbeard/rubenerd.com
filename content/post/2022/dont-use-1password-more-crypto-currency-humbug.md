---
title: "Sad to see @1Password’s blockchain shenanigans"
date: "2022-02-26T10:12:31+11:00"
abstract: "I can’t recommend their software any more if this is a signal of the company’s direction."
year: "2022"
category: Software
tag:
- 1password
- authentication
- blockchain
- ethics
- security
location: Sydney
---
The fall from grace of this once respected and essential tool has been sad to witness. 1Password [dropped this news](https://twitter.com/1Password/status/1496487659702829061) last Thursday:

> Cryptocurrency? We got you. 💸
>
> We’ve partnered with $NONSENSE to create a simpler, more secure way to manage cryptocurrencies, tokens and NFTs on the $NONSENSE blockchain.

This wasn't a once-off, it's now the account's pinned tweet. This means people arriving on their profile will see this news above anything else they post. This is a clear signal of the company's ethical stance and priorities.

I stopped buying and recommending their software after [their move to Electron](https://rubenerd.com/1password-electron-flareup-is-good-news/) and the unnecessary shifting of technical debt onto their users. These concerns seem quaint in light of legitimising hostile technology.

I'm sure people will analyse this *ad naseum*, but what I'm interested in is how they thought such news would be broadly received:

* Positively, which I can scarcely believe.

* Negatively, but they thought the positive press among blockchain spruikers would offset this wider impact.

* Whichever; someone thought the feature was a good idea and they didn't think that far ahead.

None of these are encouraging. Which leads us to *why?*

Companies that seemingly act against their own interests are usually influenced by an external force, or something we can't see. We don't know how many strings came attached to their venture capital injection, or what influence those people are now having on the direction of the company. But the timing seems awfully coincidental.

I don't envy AgileBits’ position right now. Their software is under threat of <a href="https://en.wikipedia.org/wiki/Sherlock_(software)#Sherlocked_as_a_term">Sherlocking</a> from first parties, and their competitors have caught up in feature set and polish. But from the outside, their shenanigans over the last seven months haven't won them many friends, and have alienated their advocates and base.

There's also the broader question about the continued viability of user/password authentication, something AgileBits are in the perfect position to offer legitimate thought leadership on. I think this is what makes me sad above all else.

Most of the industry professionals I talk to have already decided the company has lost the plot and have written them off, but I still think there's time for them to turn around if they commit to developing good and ethical software again. There's a reason people used to respect them and love their tools.
