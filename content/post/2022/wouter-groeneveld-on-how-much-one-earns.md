---
title: "Wouter Groeneveld on how much one earns"
date: "2022-08-23T09:21:13+10:00"
abstract: "Being candid about pay, how it works in academia and the private sector, and larger questions."
year: "2022"
category: Thoughts
tag:
- work
location: Sydney
---
Wouter [wrote a post yesterday](https://brainbaking.com/post/2022/08/how-much-do-you-earn/) discussing being candid about pay, how pay structures work in academia, the inevitable comparisons that come up in the private sector. This is something I've been thinking about a lot again lately, but also in a broader context.

Money is a cell. It's a store of energy that we charge with labour, investments, and other activities, then discharge on things we need and want. Clara and I are in the lucky position where we're topping up and never reaching zero. Much of the world doesn't (or can't) live like this, as evidenced by payday loans, rolling credit card debt, and other predatory finance.

But if a battery is a collection of cells, the other constituent ones are charged from other things. Charge them in a lopsided fashion, and it'll only perform suboptimally. Your devices might not even work with it.

Other cells might be charged with work perks or benefits that can't easily be expressed, such as a boss that treats you with respect, and gives you work that's rewarding and fulfilling. Another might be charged with your own mental health and self care.

I have friends who work in finance and corporate IT who make oodles of money; far, far more than I do. I'm on six figures, but *decidedly* on the lower end! But I hang out with the CEO and his doggo over coffee, and my friends can't stand their jobs. I think once you reach a certain paygrade, these other intangible benefits start meaning more. Good bosses and managers recognise this, while bad ones lament their retention issues and blame it on other things.

Speaking as a Millennial, one thing that fills me with hope for the Zoomers is that they're challenging the assumption that one needs to sacrifice health for money. I see it with the ones I hang out with, work with, and read about. There's a reckoning coming for people who assume that they'll put up with it like so many of us oldies did. Show us the money, but also what it comes with.
