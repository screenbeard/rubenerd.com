---
title: "Beautiful photos from the US @Interior"
date: "2022-03-31T09:37:32+11:00"
abstract: "This photo of early spring by Scott Eliot is my current favourite."
thumb: "https://rubenerd.com/files/2022/FOTSzLHXMAQ3Hx1@1x.jpg"
year: "2022"
category: Thoughts
tag:
- hiking
- nature
- photos
- united-states
location: Sydney
---
If you follow any new social media accounts this week, make it the US Department of the Interior. They post some stunning photos, including [this adorable seal](https://twitter.com/Interior/status/1507477504851943424), and [this stream in Wisconsin](https://twitter.com/Interior/status/1509289407194542081/photo/1). But [this photo](https://twitter.com/Interior/status/1505559953985130508) by Scott Eliot still takes the cake:

<p><img src="https://rubenerd.com/files/2022/FOTSzLHXMAQ3Hx1@1x.jpg" srcset="https://rubenerd.com/files/2022/FOTSzLHXMAQ3Hx1@1x.jpg 1x, https://rubenerd.com/files/2022/FOTSzLHXMAQ3Hx1@2x.jpg 2x" alt="Photo by Scott Eliot showing a field of lilacs and other flowers with tall pine trees, mist, and a snow-capped mountain in the background." style="width:500px; height:344px;" /></p>

I still have a dream of hitching up some Timberland boots and doing some serious nature hiking one day, especially around Montana and British Columbia. All it would take is sixteen hours of flights, give or take!
