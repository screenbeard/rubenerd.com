---
title: "isapc with 16-bit colour in QEMU"
date: "2022-04-03T16:31:01+10:00"
abstract: "It works again, including classic Windows without any extra drivers!"
thumb: "https://rubenerd.com/files/2022/qemu-isapc-nt351@1x.png"
year: "2022"
category: Software
tag:
- qemu
- nostalgia
- windows-nt-351
location: Sydney
---
I still use QEMU for a bunch of my vintage computing stuff. I have physical kit, but sometimes its useful to do testing on my modern machines. I also cheat and use it to install certain classic OSs direct to CompactFlash cards, which I then plug into old computers as replacement hard drives.

QEMU can emulate an AT, ISA-era PC with the **-M isapc** option, but for many years it was broken. I might be getting my wires crossed, but I'm fairly sure I read a newsgroup thread discussing various issues, and that fixing it "wasn't a high priority". I was disappointed, but didn't blame them.

I'm not sure when it was fixed, or the awesome contributor who made it possible, but I tested it on a lark recently and it's possible to invoke the option again! This means we have ISA cards back, and best of all, it gives us native Cirrus VGA support in mid-1990s retro OSs again without extra drivers.

Here's an example of booting Windows NT 3.51, one of my favourite retro OSs because its such an oddball mishmash of win32 and Windows 3.x UI:

	qemu-system-i386 \
		-M isapc \
		-m 64 \
		-hda harddrive.img \

The installer detected the Cirrus 5430 card and installed the appropriate drivers, meaning we get full 16-bit colour and high-resolution graphics on first boot! Here's the Display Settings window in Control Panel, complete with its Windows 95-era CRT preview graphic and 3.x UI:

<p><img src="https://rubenerd.com/files/2022/qemu-isapc-nt351@1x.png" srcset="https://rubenerd.com/files/2022/qemu-isapc-nt351@1x.png 1x, https://rubenerd.com/files/2022/qemu-isapc-nt351@2x.png 2x" alt="" style="width:500px; height:500px;" /></p>

The OS reports the following details for the detected card:

	Chip Type: CL 5430
	DAC Type: Integrated RAMDAC
	Memory Size: 2 MB
	Adapter String: Cirrus Logic Compatible

Curiously, Windows NT 4.0 can go to 1024×768 with 16-bit colour, but NT 3.51 drops back to 256 colour on anything higher than 800×600. I haven't yet tested whether installing a newer Cirrus Logic driver would unlock those higher modes, but I suspect that'd do the trick.

This is why I adore these classic Cirrus Logic cards. They're used everywhere, and has excellent support in these vintage OSs. Nothing beats Matrox's Mystique and Millennium for 2D quality, but Cirrus Logic makes everything easier.

The next OSs to test will be BeOS, OS/2, Red Hat Linux, and maybe some classic 32-bit BSD with XFree86.
