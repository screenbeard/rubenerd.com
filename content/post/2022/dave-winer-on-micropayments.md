---
title: "Dave Winer on “micropayments”"
date: "2022-12-02T08:12:54+11:00"
abstract: "Implications for online journalism, and how it belies a lack of appreciation for smaller prices."
year: "2022"
category: Thoughts
tag:
- dave-winer
- economics
location: Sydney
---
I've blogged the term *micropayments* in lieu of something better a few times, but it's always rubbed me the wrong way. [Dave Winer expands on why](http://scripting.com/2022/12/01/143014.html)\:

> Micropayments is such an awful term. Let's say I buy a Metrocard to ride the NYC subway. Every time I slide the card through a turnstile, what is that called? Is it a micropayment? Or I have an EZ-Pass responder on my car, so I drive through where the toll booths used to be without even slowing down. I don't know how much that costs, but I assure you there's nothing micro about it!

He also draws a link to online journalism I hadn't considered:

> One of the reasons business people at pubs must be tired of this discussion is it's their job to get MACRO payments for their employer, not teeny little micropayments. As I envision it, I would probably spend $100 a month on various pubs, and maybe more and more over time. When fluidity is added to the economics, how can you help but pay more. It's only because of the attitude about this at publishers that we can't have more news (and they get more money).
> 
> Right now I only pay three pubs -- NYT, WP and Consumer Reports. My money should be spread around a lot better than that.#

*(Being in the glorious Commonwealth of Nations, I first read his post assuming he spent $100 at the [pub](https://en.wikipedia.org/wiki/Pub "Public house on Wikipedia"), not on publications. I still get a kick out of regional differences like that).*

The *micro* in *micro*-payments also belies a lack of appreciation Silicon Valley types, and people in the industry like me, have for smaller prices. I buy a tasty coffee and don't think twice, but for someone making minimum wage, living paycheque to paycheque, or even trying to save for something, there's nothing "micro" about a payment. It's quite patronising, the more I think about it.
