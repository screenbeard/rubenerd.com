---
title: "Latvia’s Soviet-era monument removed 🇱🇻"
date: "2022-08-27T13:20:19+1000"
abstract: "Dedicated to Erna Adolphs, Michael Dexter, Vadim Brodski, Aivars Gipslis, and all my other Latvian friends."
thumb: "https://rubenerd.com/files/2022/latvia-arms@2x.png"
year: "2022"
category: Thoughts
tag:
- europe
- politics
- latvia
- ukraine
location: Sydney
---
[The Associated Press reported, via CNA](https://www.channelnewsasia.com/world/soviet-era-monuments-iconic-obelisk-comes-down-latvia-2900431)\:

> The monument was built in 1985 while Latvia was still part of the Soviet Union. It had stirred controversy since Latvia regained independence in 1991 and eventually became a NATO and European Union member.
>
> The obelisk, which comprised five spires with three Soviet stars at the top, stood between two groups of statues - a band of three Red Army soldiers and on the other side a woman representing the “Motherland” with her arms held high.

The symbolism here can't be overstated.

I've talked about this many times, but I have tremendous affection for the Baltic states, and Latvia in particular. It's had an oversized influence over my life, from family friends I grew up with and consider family, to schoolmates, colleagues, and their excellent contributions to my industry. I wrote most of my high school assignments on the country and their Lithuanian neighbours, much to the chagrin of one teacher who "had to do extra work" to grade it. 🎻

I always smiled when I saw Latvia in the headlines, but recently those feelings have turned to concern. Their country, and their neighbours, border an increasingly hostile state, and their NATO allies are connected by a strip of land far too small for comfort, and incursions into their airspace and political sphere are all too common.

Which leads to this one paragraph which gave me pause:

> The country shares a 214-kilometre border with Russia and has a large ethnic Russian population. On Russia’s annual Victory Day, which commemorates the Soviet victory over Germany in World War II, people gathered in front of the Riga monument to lay flowers.

I read a couple of books on the American civil war a few years ago which offered insight into how different people perceive history. A broad theme was that certain political circles want to preserve monuments to traitors, despots, and slave holders, because it's an indelible part of history that must not be forgotten. Their view is that such monuments *are* history themselves, which I don't buy.

On the other side, you have people like me who see monuments as depictions, commemorations, or endorsements of bad people and events. It's a sign of progress when people confront these symbols and either take them down or modify them, and quite another when they let these overt and physical glorifications stand.

<figure><p><img src="https://rubenerd.com/files/2022/latvia-arms@1x.png" alt="The Latvian coat of arms" style="width:128px; float:right; margin:0 0 1em 1em;" srcset="https://rubenerd.com/files/2022/latvia-arms@1x.png 1x, https://rubenerd.com/files/2022/latvia-arms@2x.png 2x" /></p></figure>

Admittedly this Soviet monument was built far more recently, and it depicts an event within the living memory of people alive today. I'm half German, and my dad's side have long struggled with what their leaders and people did in their name. Regardless of who was responsible for defeating the Nazis and fascists in World War II, they deserved to be.

But taken in context, I think the decision to take down the monument was reasonable.

First, let's not forget that this liberating force (to put it charitably) subsequently occupied and illegally annexed their countries into the Soviet Union against their will. The Baltic states never ceded their sovereignty, nor did many Western governments recognise this change. It's hard not to see the monument as anything more than a cynical attempt at an illegitimate and failing government to reframe their occupation as a liberation.

Second, and more alarmingly, the Soviet Union's successor state (both *de jure* and widely accepted by the public) is now involved in its own reprehensible, terrifying, and completely unjustified invasion, and has lobbed threats against the Baltic states that have never posed a threat to them. I'm sick and tired of governments thinking their size justifies them bullying others who just want to live in peace. It's the height of arrogance, not to mention strategically counterproductive, to assume one can dictate the future of another people against their will.

Maybe a future Russian government will atone for what they're doing to their neighbours, and the Baltic states (and wider Europe) will welcome them back with open arms. That'd be a great reason for a new friendship monument that acknowledges the past, pays respects, and cements their goodwill for their future.

Right now, they can [go fuck themselves](https://postcardsua.com/collections/stamps/products/russian-warship-done-envelope-postcard-without-stamp-and-cancellation).
