---
title: "Feeling validation for Apple Pay anxiety"
date: "2022-12-16T07:31:51+11:00"
abstract: "Getting stuck at a supermarket checkout without my wallet working isn’t my idea of fun!"
year: "2022"
category: Software
tag:
- apple
- apple-pay
- finances
location: Sydney
---
Clara and I were waiting in line at the checkout counter of an Aldi supermarket, when the woman in front of us couldn't pay for her groceries. She tapped a series of iDevices against the POS machine; first a watch, then the watch again, then a phone, then another phone. The queue got longer as she scrambled to test each one in succession.

At some point she realised the endeavour was fruitless, so decided to start activating Apple Pay on her phone again. This entailed adding the card back to her Apple Wallet, then getting a call from the card company to approve the addition. When that didn't work, she tried adding another card, but only after transferring some funds (according to what she was telling the cashier).

By this point, most of the people in the queue had given up, and another checkout had opened next to us. There were so many people behind us, we decided to stay put. But this meant watching this person work in vain to get a virtual card working on their phone.

There are a couple of lessons here. Test and validate your new payment stuff before trying it at a packed supermarket! Keep carrying your cards, even if you have them in your bag. Added convenience is fine, but what's your plan B when it fails?
