---
title: "Claire Saffitz makes creamed spinach pie"
date: "2022-06-19T19:58:17+10:00"
abstract: "This sounds sublime."
thumb: "https://rubenerd.com/files/2022/yt-svgBpVZKel4@1x.jpg"
year: "2022"
category: Thoughts
tag:
- food
- videos
location: Sydney
---
<p><a target="_BLANK" href="https://www.youtube.com/watch?v=svgBpVZKel4" title="Play Creamed Spinach Pie & Baked Eggs With Claire Saffitz | Dessert Person"><img src="https://rubenerd.com/files/2022/yt-svgBpVZKel4@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-svgBpVZKel4@1x.jpg 1x, https://rubenerd.com/files/2022/yt-svgBpVZKel4@2x.jpg 2x" alt="Play Creamed Spinach Pie & Baked Eggs With Claire Saffitz | Dessert Person" style="width:500px;height:281px;" /></a></p>

My dad made a [similar recipe to this](https://www.youtube.com/watch?v=svgBpVZKel4), only he blitzed the greens further and dipped bread into the mixture instead, like a savoury French toast. He called it a [Freeman's Reach breakfast](https://www.openstreetmap.org/relation/5571381#map=10/-33.7078/150.9315) after the outer suburb of Sydney we lived in at the time.

I don't eat breakfast anymore, but this would make a cracking weekend brunch. There's something about greens with egg, a bit of salt, and classic red Tabasco that's simply sublime. I might need to try Claire's twist on this, though I'll be the first to admit I don't have a cute, sentient hot sauce bottle. Merchandise idea, perhaps?

