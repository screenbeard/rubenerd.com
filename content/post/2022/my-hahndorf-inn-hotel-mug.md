---
title: "My Hahndorf Inn Hotel mug"
date: "2022-05-04T06:35:00+11:00"
abstract: "The second mug in this silly series comes from the Adelaide Hills."
year: "2022"
category: Hardware
tag:
- adelaide
- mugs
- south-australia
location: Sydney
---
Back in late March I started [reviewing my coffee mugs](https://rubenerd.com/my-apothecary-coffee-mug/). Today's comes from the Hahndorf Inn Hotel in the Adelaide Hills.

I studied in Adelaide for a few years in the mid-2000s, and went up to the German village of [Hahndorf](https://en.wikipedia.org/wiki/Hahndorf,_South_Australia) as often as I could for a mental break and to see the sights. South Australia remains my favourite part of the country, and Hahndorf would rank up there among my favourite towns.

For a while they offered a promotion where you could "keep" your mug if you paid for a mugaccino or hot chocolate. They ended up giving me a brand new one in a padded box, which let it survive the trip back home in my luggage to KL where my family lived at the time.

Vim's spell checker is insisting *mugaccino* isn't a word. How barbaric!

<p><img src="https://rubenerd.com/files/2022/hih-mug@1x.jpg" srcset="https://rubenerd.com/files/2022/@1x.jpg 1x, https://rubenerd.com/files/2022/hih-mug@2x.jpg 2x" alt="Photo of the white porcelin mug on our balcony, with a black and white line drawing and lettering for the Hahndorf Inn Hotel." style="width:500px; height:333px;" /></p>

It's one of my favourites, as you can probably tell from the wear. The walls are a bit thicker, so it retains heat well. The lettering has only just started coming off after thousands of dishwasher and hand washing cycles, and the inside isn't stained from coffee or tea at all. I've had mugs fifteen years its junior not fare as well.

Today I'm drinking some [Madura English Breakfast](https://www.maduratea.com.au/shop/black-tea/english-breakfast/english-breakfast-50-tea-bags/) from it on our balcony in [Chatswood](https://en.wikipedia.org/wiki/Chatswood,_New_South_Wales), which feels like another world away. Their loose leaf is the best, but even their teabags are excellent.
