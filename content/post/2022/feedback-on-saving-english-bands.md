---
title: "Feedback on English bands"
date: "2022-07-29T17:34:24+1000"
abstract: "Black Sabbath, Radiohead, and Pink Floyd were the ones people thought I’d missed out on."
year: "2022"
category: Media
tag:
- england
- music
- united-kingdom
location: Sydney
---
I was wondering how many people would take exception with [my list of English bands I'd save](https://rubenerd.com/which-3-english-bands-would-you-save/)!

Here's the final tally from email and The Bird Site:

* **Pink Floyd**: 6
* **Radiohead**: 3
* **Black Sabbath**: 2
* **The Who**: 1

I thought I'd get a ton of heat for not including Queen, a band I didn't even see in the original list. Maybe you all didn't either.

I can also tell my mum would have been *incredibly* disappointed that I didn't include T. Rex, and I'm sure my dad would have wanted Cream. Can't win them all.
