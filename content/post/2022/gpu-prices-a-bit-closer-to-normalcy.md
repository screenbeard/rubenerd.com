---
title: "GPU prices a bit closer to normalcy"
date: "2022-07-27T10:36:24+10:00"
abstract: "I bought at the wrong time, but I don’t regret it! Sort of!"
year: "2022"
category: Hardware
tag:
- games
- graphics
location: Sydney
---
GPU prices continue to fall across the board. I bought my RTX 3070 at the exact wrong time earlier this year; I could now get 3080 Ti or an AMD 6900 XT for about the same money, and I'll bet they'd have more robust cooling than the two inadequate fans on this Zotac card that make me long for the days of my PowerMac G5's over-the-top cooling. That was a long sentence.

But then I think back to all the fun memories of playing multiplayer games with Clara during the past four months, and I'd say it's worth it.

It's a perverse instinct to not be happy with what one has, because you *could* have something else. It means you fall into the trap of never being happy with anything. I've decided to make a note of whenever this happens in the hopes that maybe I can train my little capitalist brain out of it!

One way I've found that helps is realising that you had the same chance of getting something worse as well as better. Or as the late great PC gamer Henri Cartier-Bresson said, the best graphics card is the one you have. Wait, that was cameras.
