---
title: "OpenSSH 9.0 released"
date: "2022-04-10T09:10:40+10:00"
abstract: "With important chances to scp(1)."
year: "2022"
category: Software
tag:
- bsd
- openbsd
- openssh
- security
location: Sydney
---
This is a big release from the OpenBSD project. Alongside a new quantum-resistant key exchange method and several important portability improvements and bug fies, probably the biggest change is the way scp(1) is handled. [From the release notes](https://www.openssh.com/releasenotes.html#9.0)\:

> This release switches scp(1) from using the legacy scp/rcp protocol to using the SFTP protocol by default.
>
> Legacy scp/rcp performs wildcard expansion of remote filenames (e.g.
> "scp host:* .") through the remote shell. This has the side effect of
> requiring double quoting of shell meta-characters in file names
> included on scp(1) command-lines, otherwise they could be interpreted
> as shell commands on the remote side.
>
> This creates one area of potential incompatibility: scp(1) when using
> the SFTP protocol no longer requires this finicky and brittle quoting,
> and attempts to use it may cause transfers to fail. We consider the
> removal of the need for double-quoting shell characters in file names
> to be a benefit and do not intend to introduce bug-compatibility for
> legacy scp/rcp in scp(1) when using the SFTP protocol.
>
> Another area of potential incompatibility relates to the use of remote
> paths relative to other user's home directories, for example -
> "scp host:~user/file /tmp". The SFTP protocol has no native way to
> expand a ~user path. However, sftp-server(8) in OpenSSH 8.7 and later
> support a protocol extension "expand-path\@openssh.com" to support
> this.
>
> In case of incompatibility, the scp(1) client may be instructed to use
> the legacy scp/rcp using the -O flag.

If you use SSH, and that includes practically every Linux user and modern Windows Server admin now, please consider donating to the [OpenBSD Foundation](https://www.openbsdfoundation.org/donations.html).
