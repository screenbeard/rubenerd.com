---
title: "Moving forward, with @kiriappeee, @geofftech"
date: "2022-08-09T07:23:00+1000"
abstract: "Two people who’s recent contributions to my life have helped me a lot. Thank you."
thumb: "https://rubenerd.com/files/2022/yt-9QXktBQnOBc@1x.jpg"
year: "2022"
category: Thoughts
tag:
- feedback
- personal
- mental-health
- videos
- youtube
location: Sydney
---
This is hard to write, but probably has shown up a few times by now.

I've been in a melancholic funk for a couple of years now, as I'm sure we all have been. I've felt rudderless, tired, distant, and depressed to tears. Travel has been out of the question, and it's been difficult to find joy in the things I usually love. Some days are easier than others, but frankly I haven't felt it this bad since my mum died, bundled with all the regret that I couldn't save her.

I've avoided talking about it for fear of judgement, and I don't want to bring people down. But I'm seeing more people I respect and care about discussing their own struggles with this stuff, and I've found it both comforting and helpful. I wanted to share a couple of examples here.

### Adnan Issadeen

Adnan wrote a [lovely review of my writing](https://adnanissadeen.com/blog/rubenerd-is-an-inspiration/) back in 2020, which was a huge source of motivation. He recently wrote a [poignant post](https://adnanissadeen.com/blog/coming-out-of-a-hole/) where he opened up on his own journey:

> It's been... A difficult year. It's taken a lot of reflection to come to terms with the fact that it's actually been years of emotional strain at this point. It's taken even more reflection to accept that there's only more of this ahead of us. My heart hurts, and that's ok. I'm not looking for a cure anymore. I'm not searching for the fix anymore. I've accepted that I'm in a tunnel, not a hole, and there's no light I can see yet, and that's ok. All any of us can do at this point is to find something to guide us and to let us keep moving forward, come what may.

Adnan lives in Sri Lanka which is dealing with [unprecedented economic and political issues](https://rubenerd.com/sri-lanka-in-2022/) *on top* of these other feelings we're going through. I feel like I've got to know him over the years through his writing, and am confident that if anyone can take on the world, it'll be him.

His metaphor has been helpful for me over the last few weeks. It's easy to think we're stuck in this intractable situation, but it's important to remember we're in a tunnel not a hole (or a tarpit, as it's felt to me). Tunnels are dark and restrictive, but they *go somewhere.*


### Geoff Marshall

This tunnel metaphor leads me to Geoff, who's [delightful smile and enthusiasm](https://www.youtube.com/user/geofftech2/videos) have done so much for Clara's and my mental health during Covid lockdowns. His recent excitement for London's Elizabeth Line opening was especially infectious. We subscribe to a lot of YouTube channels, but Geoff is one of the precious few we're completionists about.

<p><a target=_BLANK href="https://www.youtube.com/watch?v=9QXktBQnOBc" title="Play I've Not Been Happy // USA 2022 Roadtrip 🇺🇸"><img src="https://rubenerd.com/files/2022/yt-9QXktBQnOBc@1x.jpg" srcset="https://rubenerd.com/files/2022/yt-9QXktBQnOBc@1x.jpg 1x, https://rubenerd.com/files/2022/yt-9QXktBQnOBc@2x.jpg 2x" alt="Play I've Not Been Happy // USA 2022 Roadtrip 🇺🇸" style="width:500px;height:281px;" /></a></p>

Geoff recently admitted he hasn't been happy of late either, and took to making a video about his [recent American roadtrip](https://www.youtube.com/watch?v=9QXktBQnOBc) to explore why. It's the most beautiful video&mdash;in every sense of the word&mdash;I've ever seen on the platform.

At the risk of spoiling the ending, I thought his last lines were perfect. He's talking about being trapped in nostalgia here, but I think it broadly applies to so much:

> Like most things in life, the answer is a complicated balance. And you have to find your way and find your balance, which isn't easy no matter who you are or what you do.
> 
> After two years of trauma, I'm going to crack on loads more. Make some new memories, new good times, which in the future I'll be able to look back on as part of my nostalgia. Just have to find that tricky balance. Keep on travelling, keep on adventuring, and thanks for watching.

Big love to both of you, and thanks for sharing your strength. Know that by working through your struggles, you've helped someone else too ♡.
