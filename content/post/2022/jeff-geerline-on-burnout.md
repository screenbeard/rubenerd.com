---
title: "Jeff Geerling on burnout"
date: "2022-01-28T08:03:23+11:00"
abstract: "“the best case end up a woodworker or farmer.”"
year: "2022"
category: Thoughts
tag:
- quotes
location: Sydney
---
This tidbit from his [recent blog post](https://www.jeffgeerling.com/blog/2022/burden-open-source-maintainer "The burden of an Open Source maintainer") is so true:

> There have been a few times I've burned out. That's typical for many maintainers. You either learn coping strategies or burn out completely, and in the best case end up a woodworker or farmer. At least that's what I see most of the time.
