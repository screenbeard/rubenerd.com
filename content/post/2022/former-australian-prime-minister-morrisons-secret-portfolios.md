---
title: "Former Australian Prime Minister Morrison’s secret portfolios"
date: "2022-08-16T17:42:11+1000"
abstract: "Secretly voting yourself into portfolios is not something he should be able to survive."
year: "2022"
category: Thoughts
tag:
- australia
- politics
location: Sydney
---
This is the sort of story that a publisher would reject from a prospective author for being too far fetched. I already regarded them man and his policies [with contempt](https://rubenerd.com/australian-prime-minister-morrison-is-out/), but this is the rotting cherry on top.

It's come out that Morrison had himself sworn into a number of additional portfolios while Prime Minister, including Minister for Resources, Home Affairs, Finance, Health, Bullshit, and the Treasury. Not sure where that penultimate one came from.

Weirder still, *this was done in secret*, having been sworn in by the Governor General but not reported. Ministers in those same portfolios, whether we believe them or not, also professed no knowledge of this arrangement, meaning they were doing their jobs in tandem with a secret collaborator.

Already some commentators have dismissed the news as nothing, claiming the Government at the time did far dodgier things. But in the words of former Australian Lieutenant General David Lindsay Morrison, the standard we walk past is the standard we accept. 

It's unprecedented, and speaks to the callous disregard the man had for Australian democracy and process. It also throws into doubt every decision made in these portfolios during the last Parliament. Taxpayer money and Government time will now have to be wasted sorting through this.

The Westminster system we inherited from the UK relies on convention, transparency, and Ministers acting in good faith to function. Whatever concocted reasoning I'm sure he'll eventually give for doing this, and in secret, these tenants were violated. He treated Australians for fools.

I like to contrast such scandals with the consequences one would face in the private sector for similar behaviour. Can you imagine the furore and legal trouble if I was secretly voted in as co-chairperson of a public company without the board or shareholders' knowledge?

This also further erodes the argument monarchists make about the Queen and her Australian stand-in. We're told the Governor General serves an important role in keeping Parliament and our legislative system in check, which Presidential systems lack. Yet the Crown approved these appointments, claiming the decision to go private or public was a matter for the Parliament. What's the point of his role then?

The way I see it, he's no more useful than his boss in London that we share with the UK, Canada, New Zealand, and other Commonwealth realms. I'm happy to keep the crumpets, comedy, and language, but they can take their royalty back.
