---
title: "My little sister got married"
date: "2021-12-13T22:53:56+10:00"
abstract: "I couldn’t be a prouder brother ♡"
thumb: "https://rubenerd.com/files/2021/elke-jesse-wedding@1x.jpg"
year: "2021"
category: Thoughts
tag:
- family
- personal
location: Sydney
---
On Sunday the 12th of December 2021, my little sister Elke married her long time partner and soulmate Jesse. The outdoor ceremony couldn't have had more perfect Sydney weather, and the well rehearsed reception and evening went off without a hitch. And did I mention *garden alpacas!?*

It was everything we've come to expect from them. Dedicated and creative, respectful and cheeky. Their energy and love for life was on full display, and it was spectacular.

<p><img src="https://rubenerd.com/files/2021/elke-jesse-wedding@1x.jpg" srcset="https://rubenerd.com/files/2021/elke-jesse-wedding@1x.jpg 1x, https://rubenerd.com/files/2021/elke-jesse-wedding@2x.jpg 2x" alt="" style="width:500px; height:333px;" /></p>

It seemed at times like the world was conspiring against the event. Multiple Covid-related cancellations, venue consternations, family and friends scattered interstate and around the world, and to top it off her inconsiderate brother having to go in for surgery a week before!

I mentioned in my recent post [saying farewell](https://rubenerd.com/dave-ross/) to our beloved uncle Dave that I admired his and my late mum's sibling relationship, and I'm realising again now just how lucky that Elke and I have inherited the same thing. I can only assume it's our excellent genes.

But the event also gave me time to reflect on how much Elke has touched my life. My earliest childhood memory was my parents coming home with her from the hospital. I'd like to think I was the dependable if goofy older brother, but the truth was she was as much my rock during all our family trauma, if not more. Okay, definitely more. I've always been in awe of her fiery spirit and attitude, even if I haven't always been the best at expressing it.

Jesse... where do I start. He's all of this too, and together they make a formidable pair. His infectious laugh and smile. His incredible singing voice, curiosity, and raw talent, which I know will carry him far. His *excellent* hair (damn it)! He's personable, intelligent, and genuine. I couldn't think of a better person to take care (and be taken care of by!) my sister.

*(I mean, clearly Elke thought so too, or they wouldn't have got married. Hey, there's the Ruben awkwardness coming in, if it hasn't yet been on full display during this post)!*

I know these emotions make no rational sense, given the event was formalising an arrangement we were all used to by now. But seeing them together that night was something else. I caught sight of her at the dinner table, and I swear I saw our mum in her profile for a second. I know she'd be as proud as my dad Rainer and I are with what she's achieved, and the woman she's become.

I wish them all the best in their future together. You're more of an inspiration than I could ever articulate. ♡

