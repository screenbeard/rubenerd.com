---
title: "Motivation myths"
date: "2021-06-09T09:45:14+10:00"
abstract: "Telling people to just stop something won’t affect change."
year: "2021"
category: Thoughts
tag:
- psychology
location: Sydney
---
Dr Amanda L. Giordano wrote for Psychology Today about why [telling people to change doesn't work](https://www.psychologytoday.com/us/blog/understanding-addiction/202106/why-just-telling-people-change-doesn-t-work)\:

> Now, suppose someone approaches you and begins telling you all the reasons why you should make [a] change. [H]ow likely are you to change? If you are like the average person, not very likely at all. In fact, paradoxically, all the external pressure to make the change may actually make you less likely to change.

She raises an intersting point:

> Oftentimes, a person’s most compelling reasons for making a change are linked to their personal values and goals—something that others might not know or might not fully know.

It reminds me of what Daniel E Lieberman [wrote about exercise](https://www.theguardian.com/lifeandstyle/2021/jun/06/just-dont-do-it-10-exercise-myths) this month for The Guardian:

> **Myth 9: ‘Just do it’ works.** Let’s face it, most people don’t like exercise and have to overcome natural tendencies to avoid it. For most of us, telling us to “just do it” doesn’t work any better than telling a smoker or a substance abuser to “just say no!” To promote exercise, we typically prescribe it and sell it, but let’s remember that we evolved to be physically active for only two reasons: it was necessary or rewarding. So let’s find ways to do both: make it necessary and rewarding.

Back to Dr Giordano, she also cites research by Miller and Rollnick in 2013 that suggests a motivational interview (MI) approach:

> The heart of MI is to join with the ambivalent person and create opportunities for them to give their own reasons for making a change (you see, we are much more apt to listen to our own advice than the advice of others!). [..]  Rather than falling into the predictable volley of “yes, but…” statements, MI encourages a specific communication style that invites individuals to voice their own reasons for making a change.

Some of the examples she provides from Miller and Rollnick include asking what would be the benefits of making the change? How significant is making this change to you? What would you like to be different from how things are now? If you don’t make the change, what might happen? What are the most important reasons why you would make the change?

She's given me a lot to think about. I can see how my efforts to help people have failed by not doing this, and I'm starting to see how others offeirng to help me have fallen on deaf ears because I fell into this trap myself.

