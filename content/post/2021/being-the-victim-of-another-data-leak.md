---
title: "Being victim of another data breach"
date: "2021-02-02T23:05:37+11:00"
abstract: "And I found out from a reader before the official company!"
year: "2021"
category: Internet
tag:
- privacy
- security
location: Sydney
---
I got an email last week advising that some of my personal information retained by an online store had been leaked, despite the database being on a hard drive (thank you). Not from the original store itself, but by an astute reader who knew I'd bought from that store them in the past, and had seen the company's troubles in the news.

An official email appeared a few days later, apologising for the breach and with promises to build their levees higher next time, presumably to take a Chevy to it. It's unclear whether they were already intending to inform us, or were in damage control mode after bad press and suddenly felt compelled to. I'd prefer to give them the benefit of the doubt, but given the extended time frame between the breach and when we were notified, the latter conclusion seems, regrettably, plausible.

I'm relieved I hadn't gone to that store in several years, so all my data with them was different. I've moved house twice since, my credit and bank cards have new numbers, and my mobile was changed after a specific charity made my life miserable on a daily basis. I'm sure other customers weren't so lucky.

*(I regularly donate what I like to think is a reasonable amount of my salary each month to charities, aid organisations, medical research labs, and sites like Archive.org. Most are reputable, but I've instantly regretted some of them based on calls I can't opt out of demanding more. No good dead goes unpunished).*

This certainly happens more than we know; even sites like [Have I Been Pwned](https://haveibeenpwned.com/) only capture a portion. Databases are connected to leaky pipes everywhere. The fact it doesn't have wider consequences on a more regular basis surprises me no end.

