---
title: "Cory Wong limited-edition vinyl!"
date: "2021-06-21T15:26:14+10:00"
abstract: "Stop living in a limited world! Living in a limited world!"
thumb: "https://rubenerd.com/files/2021/yt-VVM8bDWHfOE@1x.jpg"
year: "2021"
category: Media
tag:
- funk
- music
- music-monday
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is an urgent reminder for Cory Wong fans who might have missed the fact he's [selling limited-edition vinyl](https://www.corywongmusic.com/store)! *The Optimist* is already sold out, but his others are still available. He's only shipping within the United States, so I used Australia Post's [ShopMate](https://shopmate.auspost.com.au/) proxy service.

Cory Wong's optimistic jazzy funk tunes have helped tremendously over the last couple of years of Global Troubles. Buy his stuff on [Bandcamp](https://corywong.bandcamp.com/) or his [website](https://www.corywongmusic.com/store "Cory Wong's official store").

<p><img src="https://rubenerd.com/files/2021/cory-wong-lps@1x.jpg" srcset="https://rubenerd.com/files/2021/cory-wong-lps@1x.jpg 1x, https://rubenerd.com/files/2021/cory-wong-lps@2x.jpg 2x" alt="Cory's limited-edition vinyl LPs." style="width:500px; height:200px;" /></p>

