---
title: "When contacting IT support"
date: "2021-09-05T09:51:33+10:00"
abstract: "“Support is a dear resource. Use it when you need it, but be sure first that you do need it.”"
year: "2021"
category: Software
tag:
- psychology
- support
location: Sydney
---
Customer support is one of those terrifying phrases that customers don't want to engage with, and workers don't want to think about. There is rarely much positivity to be gained from workers who have to enforce arbitrary rules and decisions, and customers who feel like they're not being taken seriously. The entire edifice of modern customer support is set up for failure from both sides.

I did customer-facing support for an IT company (albeit among other things) and I don't begrudge either party here. Customers have a right to be frustrated when they can't get the answers they need, and support staff need to professionally detach lest they become emotional basketcases from the abuse. Some places do support properly, and some callers are patient. But we all know those are vanishingly rare.

*(Check out Moisés Chiullan's [Thank You for Calling](http://esn.fm/tyfc/) podcast archives for some of the best discussions on this topic).*

Dave Winer [raised another aspect to this](http://scripting.com/2021/09/03.html#a145848) last Friday:

> I'm doing user support for the first time since Fargo days and am reminded of what sucks about it. In the groups I am part of, as a user, when I ask for help I have reproduced the problem on my machine several times. I have tried workarounds. I've thought about it. I don't immediately ask for help, because I know the other people are like me, doing it for love, it's not their job, they're helping because they like to help.

This is in the context of a free website or service, but you could see the same frustration borne out even if you were a client or paying for a support contract. 

> For whatever reason the groups I assemble often don't do this. At the first sign of trouble they post something that's impossible to understand, and it's really clear they didn't try it again, or they would have spotted their mistake.

What's the old adage? *Help us to help you?*

He concludes:

> Support is a dear resource. Use it when you need it, but be sure first that you do need it.
