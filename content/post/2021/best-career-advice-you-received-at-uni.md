---
title: "Best career advice you received at uni"
date: "2021-06-19T15:22:05+10:00"
abstract: "Be memorable, vocal about your interests, and remember your mental health."
year: "2021"
category: Thoughts
tag:
- personal
- studies
location: Sydney
---
*This post was originally drafted in August last year. I didn't post it for some reason, but I'm rectifying this now.*

A tweet went by yesterday soliciting the best career advice you received in your twenties. Most of the replies followed the standard tropes of being yourself, seeking forgiveness over permission, not being afraid to take risks, the importance of perseverance, and to not take things personally.

Forgiveness over permission aside, all of these are true. But I also put them in the same bucket as well-meaning \*nix people saying you should use FreeBSD because it's a complete system compared to Linux that requires a distro. That might be true, but what does it mean? How is it actionable? How does it help people, today?

It reminded me of the Sunscreen Song:

> Advice, is a form of nostalgia. Dispensing it is a way of fishing the past from the disposal, wiping it off, painting over the ugly parts, and recycling it for more than it's worth.

So here's some of my own rehashed experience I wish people had told me. They may only apply if you have a similar personality or disposition!

* Build rapport with your lecturers. Ask them questions, make jokes, offer to grab coffee to go over assignments. You'll learn useful stuff, including more honest takes than what they may give publicly. Being memorable is an asset that will help in sticky situations or group assignment disputes. Heaven forbid, you might even realise than some (cough) of them are people too! I count a few from UTS as friends now.

* Be vocal about your interests, especially if they're related to the field where you want to go. Start a blog, or message about it on social networks. IT employers love seeing you be passionate about something, because it shows you're genuine.

* Don't take risks if they impact your mental health. I know some people who quit jobs or changed majors in a blaze of glory, and live off that high. I've always had the next job or study path lined up well in advance. There is absolutely no shame in that, despite what certain self-help gurus might suggest.

And most important of all:

* Put on your own oxygen mask before attending to others. Give yourself permission to do self-care, even if you need to blow off extra commitments to do so. Others won't benefit from your good will, compassion, empathy, or generosity if you're a nervous, tired wreck. Or worse, it can backfire.

