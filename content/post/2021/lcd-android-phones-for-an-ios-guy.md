---
title: "LCD Android phones to sync with FreeBSD"
date: "2021-05-29T23:55:29+10:00"
abstract: "Any ideas?"
year: "2021"
category: Hardware
tag:
- motorola
- phones
location: Sydney
---
I'll likely be sticking with iOS for now. It's still the closest thing we have to a nice platform, now that webOS no longer exists. It runs the software I need, and I don't feel like I need to upgrade the XR I'm using now.

But a few things are starting to give me pause. Syncing iPhones is one of the last things keeping me on the Mac platform, which is only becoming more difficult with each macOS release (the M1 also throws a spanner in the works for how I use my machines). The iPhone 12 has no LCD option, which signals Apple is shifting away from supporting OLED-sensitive people like me. And the prices are harder for me to justify.

It's starting to look like Android is the only viable option moving forward, for people who need to use applications in the real world. Which to get then?

My wishlist would be an LCD, manufactured by a reputable company, a track record of delivering updates, and that I could sync music to locally with FreeBSD. A headphone jack would be spectacular. I wouldn't mind if it were plastic, as long as I can get a nice, grippy case for it.

[@nuintari suggested](https://twitter.com/nuintari/status/1398649561208471553) the Moto G Power as an example. I was always partial to Sony's engineering and designs, but I have no idea about their phones. For the first time since my Palm Centro, I might have to *research* phones again.

