---
title: "The @tsukumosana and @hakosbaelz's culinary adventures"
date: "2021-09-02T09:04:25+10:00"
abstract: "AAAAAAAAAH!"
thumb: "https://rubenerd.com/files/2021/naania@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- tsukumo-sana
- hakos-baelz
location: Sydney
---
The last couple of weeks have been rough, but both Clara and I **were in tears with laugher** over these recent Hololive Council streams. *Thank you!*

<p><a title="【I Am Bread】 Empty head. Only bread. #holoCouncil" href="https://www.youtube.com/watch?v=-yslb2ru1ME"><img src="https://rubenerd.com/files/2021/naania@1x.jpg" srcset="https://rubenerd.com/files/2021/naania@1x.jpg 1x, https://rubenerd.com/files/2021/naania@2x.jpg 2x" alt="Sana playing I Am Bread." style="width:500px" /></a></p>

<p><a title="≪COOKING SIMULATOR≫ chop chop fry fry BAM. food." href="https://www.youtube.com/watch?v=C8pCAOHOokM"><img src="https://rubenerd.com/files/2021/bae-aaaaa@1x.jpg" srcset="https://rubenerd.com/files/2021/bae-aaaaa@1x.jpg 1x, https://rubenerd.com/files/2021/bae-aaaaa@2x.jpg 2x" alt="Bae attending to final food preparation in Cooking Simulator" style="width:500px" /></a></p>

