---
title: "Adelaide Metroticket words of inspiration"
date: "2021-08-22T20:24:08+10:00"
abstract: "“You can’t get anywhere unless you start.”"
thumb: "https://rubenerd.com/files/2021/metro-card-found@1x.jpg"
year: "2021"
category: Travel
tag:
- adelaide
- quotes
- nostalgia
- south-australia
- trains
location: Sydney
---
I found a pile of old magnetic [Adelaide Metrotickets](https://www.adelaidemetro.com.au/) from when I studied there in the late 2000s. Each used to come with a few words of inspiration on the back, which one of our figs got to reading rather intensely.

<p><img src="https://rubenerd.com/files/2021/metro-card-found@1x.jpg" srcset="https://rubenerd.com/files/2021/metro-card-found@1x.jpg 1x, https://rubenerd.com/files/2021/metro-card-found@2x.jpg 2x" alt="Clara’s and my sword girl fig with an old blue Adelaide Metroticket" style="width:500px; height:333px;" /></p>

These were the five that came on this specific card:

> You can't get anywhere unless you start.
>
> Each day comes just once in a life time.
>
> Fear is the parent of cruelty.
> 
> Timetable/Route information call the Passenger Transport InfoLine on 8210 1000. Lines open 7am-8pm daily.
>
> Subject to Passenger Transport Act and Regulations and Conditions of Travel.

The first three were more inspirational than the latter two.

Fun story, I thought I recognised these from back in the day. *Turns out*, the [Paris Métro](https://www.ratp.fr/en/plan-metro) also used the same system. There's a joke about French rubber-wheeled trains and the Adelaide O-Bahn in there somewhere.

