---
title: "Repairability of laptops"
date: "2021-09-22T08:31:18+10:00"
abstract: "Soldered storage is my red line, though it’s becoming all too common."
year: "2021"
category: Hardware
tag:
- design
- environment
- right-to-repair
location: Sydney
---
I've talked about this in passing a few times, and I'm certainly not the first to point any of this out! But a discussion with a family friend, and their surprise at what the industry had become, lead us to hash this out. My hope is the current state of most laptops aren't a sign of things to come, and that people are making responsible devices.

It was inevitable that most people would stop buying desktops and switch to laptops when the price was there, and that most people would treat them as an appliance. By which I mean a device you use for an expected period of time and throw away. Appliances didn't used to be like that either, but that's a broader discussion on disposable culture.

Then the trend towards soldered and integrated components picked up steam. Laptops made this way can be smaller, which is what the public wants in their bags. They also have the potential to be more reliable, owing to the fewer number of physical connections that can rattle or shake loose over time. You and I treat our machines well, but have you seen how the general public manhandle them?! In the case of systems-on-a-chip (SoCs) like the MacBook M1, huge improvements in performance and battery life can also be gained (albeit one that may have started to reach a ceiling based on the iPhone 13).

But such integration comes at a steep cost. The most obvious is serviceability, as pointed out by the right-to-repair movement. People without the financial resources to have backup machines can't send their primary work device off to repair, and will likely be forced to live with a machine with a fault. It almost sounds silly and obvious spelling it out, but serviceable machines can be fixed. It puts control of the device into the hands of the user, should they require it.

A single fried or malfunctioning component on a motherboard can be enough to relegate the entire machine to scrap. Manufactures with specialised tooling to handle BGA packaging or de-soldering tiny parts *still* routinely see such repairs as a financial sink, and are more likely to scrap the device as well. Large companies love advertising their recycling chops, forgetting that the first two Rs are *reduce* and *reuse*.

I've (begrudgingly) come to accept a future of soldered CPUs and RAM in my laptops, but I agree with Michael Dexter of [Call for Testing](https://callfortesting.org/) and the [BSDFund](https://bsdfund.org/) that storage is a bridge too far. Unlike the former two, storage has the potential to contain things that are *irreplaceable* to their owner. Robust tooling like OpenZFS also exposes just how flaky and unreliable storage can be relative to other system components. It's also something you can run out of.

*(Detractors of removable storage&mdash;a phrase I never thought I'd write&mdash;claim this is negated by cloud storage and backups. I don't begrudge people not trusting the former, and how many laypeople do you know have robust backups they test? The burden of proof is on people to make the case that its removal is a good idea, and I've yet to be convinced the pros outweigh the cons for consumers).*

The good news is people are starting to take this seriously. I was pleasantly surprised to see just how many HP laptops rated a 9 or a 10 on the iFixIt [laptop repairability score](https://www.ifixit.com/laptop-repairability)... that deserves being called out. The [Framework Laptop's](https://frame.work/) modular, labelled components are a work of engineering art, and with a screen not saddled with [PC Screen Syndrome](https://rubenerd.com/tag/pc-screen-syndrome/) for once! 

I have to use Macs for work, but the repairability of a machine will be a deciding factor in my next personal device. Storage is not something I want to gamble with.

