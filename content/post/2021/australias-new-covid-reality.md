---
title: "Australia’s new Covid reality"
date: "2021-07-28T08:51:33+10:00"
abstract: "It’s all a bit of a shitshow."
year: "2021"
category: Thoughts
tag:
- covid-19
- health
location: Sydney
---
I get a bit ranty in this one, strap in! In March I talked about the unreality of the so-called [Fortress Australia](https://rubenerd.com/fortress-australia-and-covid/) in response to the global Covid-19 pandemic:

> Given we've eliminated the disease in the community, our new cases come from this hotel quarantine system. And this is where those thoughts from bad science fiction stories come in. The last small outbreak in Sydney was genetically identified as having come from the US. Even from our selfish perspective, why was that disease allowed to run rampant, mutate further, then come back to us? How long can we hold this at bay?

The answer was: until June. We're now into our second month of enforced lockdown in Sydney thanks to a spread of the Delta Covid variant, with another month being announced today. Melbourne and Adelaide are only just coming out of their stage 4 restrictions, with the former still reporting new cases. Sydney is dealing with more than a hundred new cases a day, predominately spread by essential workers. Lockdowns are keeping the curve from becoming an exponential, but it stubbornly refuses to trend downwards.

The press, medical establishments, and governments are starting to talk about vaccination being the only path out this time. *No shit!* But this is where Australia's previous success with Covid ends. We had more than a year of this clamped down to get people vaccinated, but our leadership didn't fumble with the ball so much as throw it across the fence. *It's fine, we don't have Covid here, amirite?!* Australia's universal healthcare system was more than capable of making vaccination dissemination easy, but take-up rates have been abysmal. Lockdowns are the only way to prevent a largely unvaccinated population from spreading the disease. 

There were a few reasons for this situation.

* The federal government bet early on AstraZeneca, and the press scared people away with their irresponsible amping up of the rare blood clotting risks. This also fed anti-vaxxers (and vaccine hesitancy, for which I see no difference). Millions of doses of AZ now sit in fridges while the bulk of the population "wait for Pfizer".

* Mixed messaging and naive risk assessment, based on the idea that it was more dangerous to get an AZ jab in a country without Covid. It should have been painfully obvious that this situation wasn't going to last, and I'm floored that this was even considered a tenable position.

* It's too hard for young people to find a vaccine, even those who've read the medical literature and have come to their own informed decision to get an AZ jab instead of waiting months for another brand. Steps are being taken to address this, but the fact people were having to go to a third party website like Hotdoc to look up and book appointments, then be rebuffed several times by GPs not wanting to administer them to those under 60, is appalling.

Australia still ranks among the lowest number of cases and deaths from Covid in the world, both in absolute terms and per capita. But it's clear our previous strategy is no longer viable. The good news is vaccination rates are now climbing, and more people are getting access to them. The best time for this would have been months ago, *before* Delta got a foothold here from someone retuning from overseas. The second best time is now.

