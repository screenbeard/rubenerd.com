---
title: "Coffee shop chats"
date: "2021-05-14T09:15:31+10:00"
abstract: "If everything was chill, he wouldn’t be so stressed out!"
year: "2021"
category: Thoughts
tag:
- coffee-shops
- economics
- overheard
location: Sydney
---
I hear so many random things in this specific coffee shop.

> My mortgage broker is telling me to chill, it's fine, everything is okay. Yeah nah, if everything is chill, I wouldn't be so stressed out!

Someone else was talking about smart speakers:

> I hear you [*heh!* –ed] but don't like that it listens to me.

Another person who's in tune with their surroundings:

> I see a red light and go *aaaaaaaa!*

And some life advice disguised as sports discussion:

> Why would I bet on a losing team?

