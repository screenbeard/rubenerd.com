---
title: "A Commodore 128 shipping update"
date: "2021-06-19T19:07:53+10:00"
abstract: "My VDC memory upgrade arrived, but little else."
year: "2021"
category: Hardware
tag:
- commodore
- commodore-128
- shopping
location: Sydney
---
It's been months since my last [Commodore 128](https://rubenerd.com/tag/commodore-128/) post, so some of you might be wondering what's up. Unfortuantely, nothing really has progressed.

In my [post back in March](https://rubenerd.com/best-attribute-for-8-bit-enthusaists-patience/) I listed all the stuff I had in transit:

* A Commodore VIC-II to S-video cable
* **C128 VDC 64 KiB memory upgrade, which arrived!**
* Replacement 8563 VDC IC for the C128’s 80-column mode
* New switching power supply for my Plus/4

The other three have disappeared. Tracking numbers indicate they've been "handed over to the domestic carrier", or indicate they've never left their country of origin. The most recent update was the third of March, which was already late when I wrote that post a month ago.

My [Arena tote backpack](https://rubenerd.com/the-best-laptop-bag-ive-ever-owned/) was more than three months late owing to the current state of the world, so I'm holding out that these will arrive soon. The world is still going through a lot right now, and I've got plenty of other vintage computer projects to occupy myself in the interim.

