---
title: "Umar Getazazov helps me fix things"
date: "2021-06-16T08:45:59+10:00"
abstract: "Fixes issues in my Audioboom script and RSS"
year: "2021"
category: Internet
tag:
- feedback
- errors
location: Sydney
---
*Update: My apologies to Umar, his last name is <strong>Getagazov!</strong> You'd think someone who regularly gets called Reuben Shade instead of Ruben Schade would be more empathetic and careful about name spelling (cough).*

At the start of the month I talked about discovering and archiving my [Audioboom files](https://rubenerd.com/audioboom-found-and-archived/) with a Perl script. Umar emailed to let me know that I'd mixed up a few variables, which confirmed for me that I'd uploaded the wrong version to my lunchbox!

Umar was back at it again on Monday, by finally pointing me in the direction of a bug that has existed in my RSS feed for a while. Here's what the commands look like on my [recent Minecraft post](https://rubenerd.com/updating-to-minecraft-1-17-in-freebsd/)\:

	# pkg remove openjdk8
	# pkg install openjdk16

But this is what shows on my RSS feed:

	# pkg remove openjdk8# pkg install openjdk16

I like having clean HTML and XML syntax, but in my haste to programmatically remove whitespace and fix indentation, I'd inadvertently moved text in `pre` elements to a single line. Regular HTML doesn't respect whitespace or indentation, but these blocks sure do!

Thanks to Umar for helping me out. 👍

