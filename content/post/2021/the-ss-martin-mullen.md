---
title: "The SS Martin Mullen"
date: "2021-01-12T09:01:25+11:00"
abstract: "I haven’t been able to stop looking at this photo."
thumb: "https://rubenerd.com/files/2021/martin-mullen@1x.jpg"
year: "2021"
category: Media
tag:
- photos
location: Sydney
---
The *SS Martin Mullen* was a Canadian freighter that started plying the North American Great Lakes from the turn of the century. [This photo](https://commons.wikimedia.org/wiki/File:Str._Martin_Mullen,_Pioneer_Steam_Ship_Co._,_coaling,_Houghton,_Mich..jpg "Photo page on Wikimedia Commons") was taken in 1906 by the Detroit Publishing Company, and I haven't been able to stop looking at it. The smokestack lets you know you're dealing with a steamer, but somehow the scene looks oddly modern.

<p><img src="https://rubenerd.com/files/2021/martin-mullen@1x.jpg" srcset="https://rubenerd.com/files/2021/martin-mullen@1x.jpg 1x, https://rubenerd.com/files/2021/martin-mullen@2x.jpg 2x" alt="Photo of the SS martin Mullen being loaded, in 1906." style="width:500px" /></p>

