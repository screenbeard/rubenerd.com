---
title: "Faux chatbot notification tabjacking"
date: "2021-12-14T15:53:35+10:00"
abstract: "(1) New Messages [sic]!"
year: "2021"
category: Internet
tag:
- accessibility
- antipatterns
location: Sydney
---
Website chatbots can be useful for phone-shy or time-poor introverts, or *supremely* frustrating depending on their implementation. And not the good kind of supreme, like [The Supremes](https://en.wikipedia.org/wiki/The_Supremes) or supreme vegetarian pizza with pineapple. I like pineapple, because I'm not a monster.

I've written before about bad chatbots that display fake unread notifications within a red circle, akin to a phone screen. It's an anti-pattern designed to irritate you enough to want to clear it... but then they've hooked you into an interaction you wouldn't have otherwise engaged in.

Worse than these though are what I'm dubbing *tabjacking*. The page will change title every second, like this:

> **Large IT Company Page Title**  
> **(1) New Messages!**   
> **Large IT Company Page Title**  
> **(1) New Messages!**

Leaving aside the grammar offence committed here, it's enough to drive me bananas. Or pineapples, or tomatoes, or any other fruit. Doubly so when you're on a client demo call and...

> **(1) New Messages!**   

... keeps blinking in a tab out the corner of my eye.

Frontend web developers: I know you have a lot on your plate being forced into making the web bad either through personal malice or via the uninformed decisions of management, but if you could sneakily not push those changes into prod, we'd all appreciate it!

Maybe the web needs some good ol’ fashioned civil disobedience, packaged into NPM so they don't need to think too hard about it.
