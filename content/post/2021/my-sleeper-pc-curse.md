---
title: "My sleeper PC... curse!"
date: "2021-08-07T09:54:09+10:00"
abstract: "I keep buying dead computers to use their cases, but then inadvertently fix them."
thumb: "https://rubenerd.com/files/2021/presario-spaceship@2x.jpg"
year: "2021"
category: Hardware
tag:
- compaq
- compaq-spaceship
- sleeper-pc
location: Sydney
---
The best thing about having a job in your thirties is having the privilege and means to buy things you always wanted as a kid. Okay it's not the *best* thing, but it has to rank up there. In the words of my old man, what's the point of working your brains out at a job if you can't grant yourself permission to be a little silly sometimes?

The dream for me has been to collect the computers I wanted as a kid. I've now spent more money and time refurbishing my childhood Pentium 1 tower than when I got it new! I've got a beautiful little Toshiba Libretto laptop that was literally advertised as being "smaller than a VHS tape" when it came out. Josh Nunn of [The Geekorium](https://the.geekorium.com.au/) generously gave me his Commodore 128 which I use to play the games I missed out on growing up, and even some CP/M stuff which filled in a missing piece of the puzzle in my understanding of 1980s DOS.

This desire for pointless nostalgic revelry also extends to contemporary machines. I've been obsessed with the idea of sleeper PCs that, like their automotive cousins, are modern computers inside old cases. To that end I've had saved searches for specific computer towers on eBay that are listed as being in good cosmetic condition, but "for parts only" or are otherwise non-functional. The plan was to buy them, carefully remove their internal parts, then shoehorn a new motherboard, power supply, a couple of SSDs, and a blu-ray burner into them. I'd probably stick with integrated graphics for my needs, but I'd also explore perhaps using a drive bay or other case intake to get better air circulation and eventually put a discrete GPU in.

Emphasis on *the plan was*. The problem is, I keep buying dead machines **and fixing them**. The first was a Gateway 2000 full-height AT tower from 1992 that I was originally going to turn into a vertical NAS with drive sleds, but the only reason it wasn't functional was a loose power connector and RAM that needed re-seating. I couldn't bare to rip it apart, so I sold it to a Korean gentleman in Brisbane who, like me, wanted the PC from his childhood.

The second was an IBM Aptiva tower from the mid-1990s. It was also listed as DOA, only this time it just needed some contact cleaner on the mechanical power switch and a hard drive. I ended up giving it to a friend who'd been after an IBM tower to put OS/2 on.

Which leads us to the [Compaq Presario tower I blogged about](https://rubenerd.com/finally-buying-a-compaq-spaceship/) a fortnight ago. Yes, once again, *I got it working!* The specs of this machine are very close to my Pentium 1 tower already, so I feel fewer reservations about removing the parts, but I'll still be keeping them in anti-static bags in case I want to restore it in the future.

I'm not an electrical engineer. I know enough about how to use oscilloscopes and LCR metres to be dangerous, and can follow the most basic of service manuals to troubleshoot problems, but I'm by no means an expert. But this got me thinking how many other childhood gems are out there for other people, just waiting for a bit of TLC.

