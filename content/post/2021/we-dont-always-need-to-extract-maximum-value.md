---
title: "We don’t always need to extract maximum value"
date: "2021-04-06T07:51:46+10:00"
abstract: "And I don’t need the free coffee upsize, thanks!"
year: "2021"
category: Thoughts
tag:
- coffee
- economics
- life
- work
location: Sydney
---
Clara and I went to a coffee shop last night for a decaf. I know saying the latter is provocative enough for certain drive-by readers, so I especially won't divulge the name of the chain, lest I be inundated. It doesn't matter if they were all that was open, or that I have nostalgic family memories tied up with the chain from back home. The coffee nerds, with whom I otherwise agree, would have a field day.

But I digress! I had a loyalty card, and could redeem one of the two coffees for free. Great! But then I declined the offer for a free upgrade, which puzzled them. *It's free, why wouldn't I want it?* I said I only ever get tall-sized coffee, and that a free one of those would still be great. They pressed me again if that's what I *really* wanted.

I pass no judgement on the barista. I suspect if they hadn't pushed me for the upgrade they may have got in trouble with their manager, which I don't want. They might have thought I misunderstood, and from years of bad retail experience were covering their backs in case I realised after the fact and came back angry.

But there's a wider point here. It's so ingrained that **we should extract the most possible money and value out of things, even if we don't need or want it**. I didn't want a large coffee. It doesn't matter to me if I could have got extra, it would have been wasted and poured down the drain. Maybe I should have anyway to cost Starbucks more... whoops, there's the name of the chain. Maybe I'd be seen as a sucker, because Starbucks could then sell the difference. But why does everything have to be such a calculated, zero-sum game?

It reminds me of one of those superficially-inspiring quote memes from the mid 2000s. We're told that we have an ATM card and the ability to withdraw $1,400 dollars a day. Naturally then, it would be foolish not to withdraw that full amount, or we'd lose out. Then we're told that each of those dollars is a minute in a day, and that we shouldn't waste time. I can see what they were attempting, but it still falls into the same trap; we all *need* to waste time lest we burn out. Or to use their metaphor, we're not robots that can have value extracted like money from an ATM. And if we're dealing with hypotheticals, what if extracting that money shortens your life from stress, or takes it from someone else who needs it more? Whoops, that sounds a bit like a zero-sum game too! But you get my drift.

Another somewhat-related example is rewards on credit cards. I treat credit cards like charge cards for points, insurance, and fraud protection; the balance always exists in my bank account and is auto-debited each month\*. Last month I got a call from my card company telling me that I could redeem a free trip somewhere. It was my birthday and I wanted to spend time with family, so I politely declined. *But it's a limited-time offer* they said. That's fine, I'm not going to take a financial reward if it means I can't have a beer with my dad.

*(\* I say this each time, because I'm worried how much credit cards have become normalised, and don't want to perpetuate that by mentioning them here. They're dangerous if you use them as the banks intend you to. Please be careful).*

I'm still going to ask for a smaller coffee when I get my next free one. That might make me a sucker, but it makes me happy.

