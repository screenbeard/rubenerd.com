---
title: "My state of macOS virtualisation, with FreeBSD and NetBSD digressions"
date: "2021-05-07T09:25:34+10:00"
abstract: "Answering a question from Antranig Vartanian."
thumb: "https://rubenerd.com/files/2021/vms-2021@1x.jpg"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- netbsd
- virtualisation
location: Sydney
---
*It only occured to me halfway through writing this that what I've said here applies to Intel Macs. Clara has an M1, but I'm still on the fence if I'm going that way.*

[Antranig Vartanian](https://antranigv.am) asked me on The Bird Site:

> How's your Virtualization setup on macOS? I have to use macOS for work for a while, since I use a mix of bhyve and VirtualBox on my FreeBSD machines, I don't know what to do on macOS [..] last time I installed VirtualBox it messed up my USB ports and I was not able to transfer data, only charge.

I had a similar experience with VirtualBox, only with NICs. It added a network bridge that for some reason kept messing up my Ethernet to USB-C dongle when cutting over from Wi-Fi. Or at least, I think it did. I removed it and it magically all worked again. It was frustrating, because VirtualBox is otherwise pretty capable and has good support for tools like Vagrant.

I used to write a lot about virtualisation here, I should get back into it. It's a fascinating field of IT, and recent developments on mu tipple fronts are intriguing and worth exploring, especially on [NetBSD of late](https://blog.netbsd.org/tnf/entry/from_zero_to_nvmm "NetBSD Blog: From Zero to NVMM") too. I got my current job, in no small part because my boss read VM-related stuff I posted here.

But I digress! As with everything on the Mac, I offload whatever I can to my homelab server running FreeBSD. VMs and jails on my Holo server running a mix of FreeBSD, NetBSD, and Debian run Minecraft, PleX, Netatalk file shares, build environments, dev testing, automated backups, and a music server. Some of these are backed onto an OpenVPN tunnel so I can access wherever. I need to get back into bhyve more; I run a Xen dom0 because it's what we use at work and its what I'm most familiar with.

<p><img src="https://rubenerd.com/files/2021/vms-2021@1x.jpg" srcset="https://rubenerd.com/files/2021/vms-2021@1x.jpg 1x, https://rubenerd.com/files/2021/vms-2021@2x.jpg 2x" alt="Screenshot of Parallels Desktop running Telemetry OS and FreeBSD 13, and QEMU running Windows 2000" style="width:500px; height:260px;" /></p>

With that set up, this leaves me three use cases for Mac virtualisation:

* **Games.** I don't play enough Windows-specific titles anymore to justify having a dedicated game machine, so I spent a bit extra on my MacBook Pro to get the 5500M GPU. I can dual-boot with Boot Camp, but Parallels Desktop performs *so well* for 3D acceleration, and is easier.

* **Local testing.** Because I already paid for Parallels for games, I use it elsewhere too. It's simple to use and performs well, even for OSs it doesn't officially support with Parallels Tools like NetBSD.

* **Retro stuff.** For this I use straight QEMU without acceleration. It can emulate an isapc, SoundBlaster 16 card, and Cirrus graphics which all work well with ancient OSs. I wrote about this stuff in detail [a decade ago](https://rubenerd.com/qemu-sound-blaster-win-31/ "View all posts tagged with QEMU"), and most of it still applies :). I've written my own scripts to automate booting QEMU VMs.

I can say from experience that VMware Fusion is also excellent, and was slightly more useful than Parallels when I was also doing more VMware ESXi stuff back in the day. I standardised back on Parallels to make my life simpler, just as I did with the [first betas in 2006](https://rubenerd.com/parallels-desktop-freebsd-issues/)!

All this works great, which is why I'm hesitant to move to an M1 Mac, at least for now. A small part of me wonders if this might be impetus I need to jump completely to a FreeBSD laptop, assuming I can run the software I need for work and a few games in a Windows guest. bhyve with PCI passthrough for a discrete GPU would be *boss.*

