---
title: "What I miss during These Times™"
date: "2021-08-11T08:32:52+10:00"
abstract: "Travel, mostly."
year: "2021"
category: Travel
tag:
- covid-19
- japan
- mental-health
- personal
- united-states
location: Sydney
---
We have it *unconscionably* good in certain parts of the world. The more news I read from our neighbours in Indonesia, or the plight of people in Brazilian favelas, it puts any whinge or sadness I have for our circumstances into stark reality. My girlfriend and I have a comfortable little home, stable incomes that can be made remotely, food on our table, access to our friends and family via a decent Internet connection, and live in a country with universal healthcare and access to free vaccines.

No amount of empathy, awareness, or charitable action will change the fact that this post will sound entitled, spoilt, and ignorant. But as a dear friend once told me, *feeling miserable doesn't solve the world's problems either*. So I'm giving us permission to talk selfishly for a bit.

Motivation is easily the biggest thing I miss. I've been fortunate that I'm almost never bored; I've always had oodles of complicated and interesting hobbies that have even translated into employment. Writing, tinkering, and building things bring me endless fascination and joy. But despite being perfectly suited to being done while stuck at home, I've lacked the motivation of late to progress them. Half-finished projects litter my desk while I sit in my chair facing the wall. The pilot light is still on, but the mental boiler remains stubbornly tepid. It's hard to explain.

Sitting at [coffee shops](https://rubenerd.com/omake/coffee/) to write and code is certainly another; you don't need to read me wax lyrical about my affection for these places of wonder again here. I also miss hanging out with my old man over a cup of coffee and pontificating about the world, or having lunch with my sister and my precious few friends who put up with my introversion and deserve the world.

<p><img src="https://rubenerd.com/files/2021/photo-me-ny-2016@1x.jpg" srcset="https://rubenerd.com/files/2021/photo-me-ny-2016@1x.jpg 1x, https://rubenerd.com/files/2021/photo-me-ny-2016@2x.jpg 2x" alt="Photo from The Top of The Rock I took with Clara on one of our US trips." style="width:500px; height:333px;" /></p>

The world... hey, that's a segue into travel! I've written here in the past that Clara and I deliberately lived frugal lifestyles so we could save which makes us feel safe, and afford to go overseas for holidays and events whenever we could. I adore Japan and the United States, and going back home to Singapore on a regular basis made me feel connected again. We'd made plans to go to Europe for EuroBSDCon and back to Japan last year, which have been put on hold indefinitely.

Clara's and my immediate bucket list also included Canada, South Korea, Taiwan, and Vietnam. Australia and New Zealand are beautiful places, but they even felt isolated *before* our borders slammed shut.

What do you miss? [Feel free to vent to me](https://rubenerd.com/about/#contact), and let me know if I can share.

