---
title: "An updated clearance sign"
date: "2021-04-11T18:13:31+10:00"
abstract: "Why did they block out one of the numbers? I have some ideas, all entirely legitimate."
thumb: "https://rubenerd.com/files/2021/photo-clearance@1x.jpg"
year: "2021"
category: Thoughts
tag:
- australia
- pointless
- sydney
location: Sydney
---
Walk past the entrace to Lemon Grove's carpark in the Sydney suburb of Chatswood within which I reside, and one may notice a revision to its clearance sign. A lighter patch of yellow next to the scalar value of the ceiling height now exists.

<p><img src="https://rubenerd.com/files/2021/photo-clearance@1x.jpg" srcset="https://rubenerd.com/files/2021/photo-clearance@1x.jpg 1x, https://rubenerd.com/files/2021/photo-clearance@2x.jpg 2x" alt="Photo of a carpark entrance with the above-described sign." style="width:500px; height:333px;" /></p>

Clara and I discussed what that could be for. It could be covering:

* An innaccurate number. Perhaps the carpark ceiling posessed less clerance than first thought, resulting in the wrong number being printed. Maybe engineers measured it wrong during or after construction. Maybe it was a clerical or printing error.

* An outdated number. Perhaps the ceiling of the building sank, or the ground rose like a cake. Maybe they built new ductwork or pipes that reduced the ceiling's effective height. Maybe the road was resurfaced which added *just* enough height to round the advertised clearance up by a significant figure.

* A number in the wrong scale. Perhaps the sign was first installed prior to [Australian metrication](https://en.wikipedia.org/wiki/Metrication_in_Australia), and the architects had the "metres" sign added by mistake.

* An erroneous character. Perhaps the 0 underneath the paint was actually the capital letter O, and the building's management couldn't deal with being inundated by frustrated typeograpers. Maybe it wasn't a meaninful shape at all, or an unlucky one, or something vulgar painted either by the original builders or a subsequent vandal.

* A rip in the space/time continuum. Perhaps covering it with a striking colour sealed it, or rendered it less dangerous. Perhaps the bright colour was to serve as a warning for those so inclined to be affected by such a vortex, or for those actively seeking them.

* Nothing at all. Perhaps it's blank space underneath. Maybe the owner of the paint had more than he needed and didn't want to go to the trouble of disposing it in an environmentally concious way. Maybe they like the visual chaos afforded by the contrast of two differing yellows, or desire to frustrate people who are angered by such visuals.

It's safe to say that all were contributing factors. Like 2 and 8 to 16. Hey, maybe one of those were a former number on this sign.

