---
title: "Revisiting my emacs and Vim/nvi post"
date: "2021-06-23T09:00:28+10:00"
abstract: "Back to Vim from Emacs for editing text after a six-month experiment."
year: "2021"
category: Software
tag:
- editors
- emacs
- vim
location: Sydney
---
Last November I thought aloud about my [relationship with the Vim and nvi text editors](https://rubenerd.com/thinking-out-loud-about-vim/). I said that I'd written millions of words in them, but that I felt like I hadn't progressed beyond an intermediate user of either.

I hinted that I was trialling GNU Emacs as an alternative. I went absolutely all in, and thanks to the excellent [MELPA](https://melpa.org/) repository I've now consolidated a ton of stuff into it, from RSS to IRC. Most valuable of all, [Org Mode](https://orgmode.org/) replaced nvALT, one of my last Mac-only holdout applications that I couldn't run on FreeBSD. It was like stepping into the proverbial lolly store.

*(Every so often I realise just how much of our industry is based around abbreviations and acronyms. That paragraph had no fewer than six).*

But as the emacs joke goes about it being a great OS that's missing a text editor, I somehow moved back to Vim for writing. Using another editor full time made me re-evaluate the harsh criticism I levelled at myself as a Vim user. Turns out I had better muscle memory, and understood the Vim way of doing things more than I expected; to the point where even using the Evil Mode in Emacs didn't do it for me.

Just as everyone has their own way of learning, I've also since discovered Drew Neil's [Practical Vim](https://pragprog.com/titles/dnvim2/practical-vim-second-edition/), [Modern Vim](https://pragprog.com/titles/modvim/modern-vim/), and [VimCasts](http://vimcasts.org/). The built-in Vim tutorial is comprehensive, but I grok his explanations far more. I feel as though I've levelled up significantly since I wrote that last post. I might do a series about it at some point.

