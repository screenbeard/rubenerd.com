---
title: "Random Wikipedia Article: Niigata, Japan"
date: "2021-03-22T08:05:57+11:00"
abstract: "Toki Messe is a multi-purpose international convention center in Niigata, Niigata Prefecture, Japan."
thumb: "https://rubenerd.com/files/2021/TOKI_MESSE@1x.jpg"
year: "2021"
category: Travel
tag:
- architecture
- japan
- travel
- wikipedia
- wikipedia-random-article
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/TOKI_MESSE@1x.jpg" srcset="https://rubenerd.com/files/2021/TOKI_MESSE@1x.jpg 1x, https://rubenerd.com/files/2021/TOKI_MESSE@2x.jpg 2x" alt="Photo of the Toki Messe complex by HIRO on Wikimedia Commons" style="width:500px" /></p>

It's been a couple of months since we hit the [Wikipedia Random](https://en.wikipedia.org/wiki/Special:Random) link. Today we got this building on the Sea of Japan:

> Toki Messe (朱鷺メッセ, Toki Messe) is a multi-purpose international convention center in Niigata, Niigata Prefecture, Japan. The center was opened on May 1, 2003, and contains a hotel, restaurants, an art museum, conference rooms, and the offices of several international organizations.

The [photo by HIRO](https://commons.wikimedia.org/wiki/File:TOKI_MESSE.jpg) bears a striking resemblence to the [United Nations building](). The layout of the building bears the resemblence I mean, not the photo. Though the photo is the device we're using to visualise said resemblence.

> Toki Messe is the tallest building on the Sea of Japan, and has an observation deck on the 31st floor where one can view the areas in and around Niigata. Depending on the weather, one can also see Sado and Awashima islands. 

I live for observation decks! I've informed Clara that it's made *The List&trade;* for this reason.

The building is in Niigata, which looks like a great place to visit. It has the largest urban rice fields in the country, and is connected to Tokyō by the Jōetsu Shinkansen. Those two facts were unrelated, save for their relation to Niigata.

