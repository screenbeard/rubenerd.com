---
title: "Disabling memberships in Ghost 4 blogs"
date: "2021-05-03T22:09:12+10:00"
abstract: "A new developer flag exists in 4.3."
year: "2021"
category: Internet
tag:
- ghost
- troubleshooting
- weblog
location: Sydney
---
I use the [Hugo](https://gohugo.io) static site generator for the site you're reading now, but I've long recommended [Ghost](https://ghost.org/) to people who want a server-side blogging platform. It's *fast*, simple to use and administer, and has far more focus on blogging than other platforms with which you may be familiar.


### The new feature

A membership feature was enabled by default starting with version 4, allowing bloggers to accept signups, collect subscriptions, and send email newsletters. It's *precisely* the independent support I've been shouting about here for years, and a critical counterpoint to writing farms like Medium, and social networks like Facebook.

*(I've talked about the subtle change in their messaging from a kickarse blogging platform launched on Kickstarter, to something [specifically pitched for business](https://rubenerd.com/turn-your-audience-into-a-business/). But it still makes a great blog platform).*

The problem was it couldn't be disabled anymore from the web UI. Some of us host blogs for people and organisations who don't want memberships, get income elsewhere, or for whom a membership relationship with readers doesn't make sense. Others were precluded on fiduciary or legislative grounds. Even among users of the feature, confusion abound about how to remove users, change the messaging, and the wider security implications.

After some heated forum discussions and partial workarounds, the Ghost team have [indicated on Twitter](https://twitter.com/Ghost/status/1372894588810186759) that a toggle for this feature is returning, though they haven't committed to a timeline.


### The simplest workaround for now

[mdeneen spotted](https://forum.ghost.org/t/how-to-disable-members-function/20775/27) this feature in the Ghost config starting with 4.3:

    SELECT value,key FROM settings WHERE key = 'members_signup_access';
    ==> all|membership_signup_access

Setting this to `none` disables the feature, as we would have done with v3's graphical checkbox. I can confirm this works!

    UPDATE settings SET value = 'none' WHERE key = 'members_signup_access';

Kevin from the Ghost team, who's been especially patient during all the hand-wringing about this, [cautioned](https://forum.ghost.org/t/how-to-disable-members-function/20775/28):

> This will only work in Ghost 4.3 onwards. The UI for this option is currently behind a developer experiments flag and will be made generally available in a future release.
> 
> As this is currently in development be aware this setting value may change.

I'd recommend people stay on Ghost 3.x until an official release includes this option. But for those who've already made the jump 4.x, this is the simplest way to fix it, provided you keep an eye on updates.

Thanks to the developers for implementing this; I can say it's improved my quality of life more than I care to admit! 🍻

