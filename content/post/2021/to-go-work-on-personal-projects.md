---
title: "To go work on personal projects"
date: "2021-01-31T10:20:24+11:00"
abstract: "I build and talk about servers all week... then spend my weekend doing the same thing."
year: "2021"
category: Thoughts
tag:
- personal
- work
location: Sydney
---
I spend most of my week building, fixing, tinkering, and talking about computers with clients and new sales leads. I feel exhausted at the end of the day; usually in a good way because I feel like I'm well tuned for what I'm doing. But occasionally I sit there, exhausted, and counting down the clock. I'm sure it happens to all of us.

Then the weekend comes around and, assuming I don't have after-hours work booked or if I'm not on call, I sit around our little apartment, go for a long walk, grab a cup of coffee, and relish the opportunity to think about something else... *anything* else. Then I build a new webserver, or tinker with a FreeBSD template, or update our homelab.

Sure, I'll do it under the false pretence of making our backups more reliable, or our Plex server that little bit faster. But Clara knows what's going on.

Those two silly adages are that you *do what you love*, and that if you enjoy your work you never work a day in your life. Both are well-meaning but patently nonsense: loving tinkering with servers doesn't mean you want to be woken out of bed by an outage.

Sometimes you need a break from what you do, even if weirdly it's doing the same thing. I suppose it's because its your timeline, and your own trail of curiosity that lead you down those delightful rabbit-holes, rather than meeting an external expectation.
