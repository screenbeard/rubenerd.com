---
title: "A 1980s PBS documentary about early diesels"
date: "2021-01-16T11:19:11+11:00"
abstract: "This ripped VHS tape is how I spent my morning while packing!"
thumb: "https://rubenerd.com/files/2021/youtube-vhs-diesel@1x.jpg"
year: "2021"
category: Travel
tag:
- diesel-locomotives
- trains
location: Sydney
---
This is [how I spent my morning](https://www.youtube.com/watch?v=AnOZHOM8UXM) while packing for a house move. The 1980s music, the people they interviewed, the VHS artefacts; everything was amazing.

<p><img src="https://rubenerd.com/files/2021/youtube-vhs-diesel@1x.jpg" srcset="https://rubenerd.com/files/2021/youtube-vhs-diesel@1x.jpg 1x, https://rubenerd.com/files/2021/youtube-vhs-diesel@2x.jpg 2x" alt="Screenshot from the documentary showing a Baldwin centre cab diesel locomotive" style="width:500px" /></p>

But I'm sure other rail nerds would especially appreciate this zinger at 37:30:

> Besides the [Minneapolis, Northfield and Southern](https://en.wikipedia.org/wiki/Minneapolis,_Northfield_and_Southern_Railway), six other roads owned centre cabs. In fact, Baldwin designed the unit for the [Elgin, Jolliet and Eastern](https://en.wikipedia.org/wiki/Elgin,_Joliet_and_Eastern_Railway); a beltline around Chicago owned by US Steel. EJ&E's centre cabs continued in service into the 1970s after most of the others were scrapped.
> 
> The secret? EJ&E replaced the Baldwin engines *with EMD.*

