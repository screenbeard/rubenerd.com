---
title: "The scripting spiral"
date: "2021-09-26T08:59:12+10:00"
abstract: "I’ll write a script! Nah, I should do it properly using XYZ. Wait, that sounds like too much work. Eh, I’ll do it manually (!?)."
year: "2021"
category: Software
tag:
- dire-straits
- psychology
- scripting
location: Sydney
---
I'm not sure what to call this post, or even this phenomena, so I figured something short with alliteration would be *better than nothing*. Microwave ovens. Custom kitchen delivery. Refrigerators. Colour TVs.

Wait, that's *[Money for Nothing](https://www.youtube.com/watch?v=qmRzbKNbTsY)*.

If you're the kind of person who reads this blog (voluntarily, though I can't imagine too many situations where people would be coerced to, unless it was a lesson about how *not* to write about something), chances are you probably also employ scripts somewhere in your life or work. Why not, if a computer can do things for you?

Like most kids from the early 1990s, my first scripts were QBasic, QPascal, and batch (BAT) files. I had a couple of programs that would copy files like my short stories into folders with timestamps, so I could "roll back" to an earlier draft and compare where the story went. I guess it was a rudimentary form of version control, but it was more just a practical way to work within the limits of the 8.3 filename system. I also constantly changed my mind about plots, and would integrate ideas from multiple drafts.

I've *long* since moved to Perl as my language of choice for scripts, then shell scripts for more basic tasks. I've mentioned here before that I've tried all manner of other languages, but while Ruby got close in some aspects, Perl has stuck with me given how its data structures map terrifyingly well to how my mind works. Read into that as much as you want. Microwave ovens.

I raise that I do both, because lately I've found myself realising that I get stuck in a loop deciding how to solve a personal problem. It goes, a little something, like this:

1. Identify a repetitive or tedious thing I do.

2. Think about how I'd throw together a quick script to automate it. It'd only take a dozen or so lines of shell script, right?

3. Think about how I'd "do it properly" with a formal structure, methods, and [Moose](https://metacpan.org/pod/Moose). I'll check out CPAN, and think about how I'll store the resulting data. Ooh I could use sqlite3 for this, or even XML or a plist.

4. Think that step 3 introduced all this overhead, when all I needed was a quick and dirty script to do what I want. 

5. Decide it's too much work and abandon it.

Rather than possessing the wherewithal and introspective capability to identify and rectify this verbose recognition of counter-productive foundering, I'll end up performing that task manually again, *when I could have written a short script*. It's somewhat ironic, given Perl's detractors see the language as an ugly hack to write quick scripts too.

It's probably yet another manifestation of [decision paralysis](https://rubenerd.com/the-paralysis-of-decisions/). Maybe I should write a script to figure out why. I could write a full CMS in [Catalyst](http://www.catalystframework.org/) to solicit feedback and to explore this idea. Wait, that sounds like too much work.

