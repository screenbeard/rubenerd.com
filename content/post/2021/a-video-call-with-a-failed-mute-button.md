---
title: "A video call with a failed mute button"
date: "2021-08-31T14:57:47+10:00"
abstract: "This is how you would open a 2020-21 thriller novel."
year: "2021"
category: Software
tag:
- covid-19
- video-conferencing
- work
location: Sydney
---
Move aside every Korean horror film, I had something ghastly and terrifying happen to me today that eclipses them all. I can already tell the experience will become a fixture of my weekly nightmare routine, alongside such memorable dreams as needing to get to the airport but having too much stuff to fit in my luggage, and not knowing where my passport is.

I was preparing for a video conference call, which is the new way to do business during *These Times*. I have the drill down pat on account of doing a few million of these a month. I start the conference bridge, take a sip of water, orient myself correctly in my chair, do a few shoulder rolls, prop my phone on the stand within eye shot, make sure I haven't bumped the camera off kilter, then wait for my colleagues and the client to appear.

This time started like any other. One of the clients was early, so we got down to some small talk. He appreciated the weather too, missed being able to come up to Sydney, and apologised for his unkempt hair. I appreciated that neither of us were in collared shirts. We laughed, and I excused myself to get some water and wait for everyone else to join.

I hit the mute button on the conference bridge software. Nothing happened.

I pressed it again. Still nothing, my audio remained active.

I smashed it again, this time with enough force that I was surprised my mouse remained in one piece. Nothing.

I'm in a room with Clara who also works full time and has to take conference calls. Sometimes I have to sneeze, or take a drink of water. Nobody needs to hear that. Now I was facing the prospect that I'd be live, on air, the entire time.

It hadn't occurred to me before just how much I'd come to rely on the ability to silence myself, or remove my video for a moment. If we're going to be stuck working remote and using these tools all the time, at the very least we could get some perks. But now, there I was, in full broadcast mode, and for a call in which I wasn't even the primary participant.

I caught myself wanting to mute many more times during that arduous call. I had to signal to Clara to be quiet, and I didn't have time to explain why, which made me feel like a dick.

We were all lucky that I didn't have to mute for anything serious during that call. But I was on edge the whole time. *Never again!*
