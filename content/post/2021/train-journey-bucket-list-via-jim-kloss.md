---
title: "Train journey bucket list, via @JKloss4"
date: "2021-07-30T08:38:27+10:00"
abstract: "The Ghan, Japan, Empire Builder, The Rocky Mountaineer, German ICE, even KTMB!"
thumb: "https://rubenerd.com/files/2021/sakura-hankyu-inside@1x.jpg"
year: "2021"
category: Travel
tag:
- australia
- singapore
- empire-builder
- japan
- kyoto
- montana
- osaka
- united-states
- whitefish
location: Sydney
---
Jim Kloss shared some Australian train journey videos from the BBC's wonderful [Michael Portillo](https://www.bbc.co.uk/programmes/m0009v33), and asked if I've ever been on one. No sooner had I read that tweet, than Clara and I watched a video from [Geoff Marshall](https://www.youtube.com/watch?v=wnQleaMe8PU) who mentioned an occasion where he'd managed to photobomb one of his filming trips! Alas, while Clara and I have many of Michael's blu-rays exploring Australia and Europe, we have yet to go on them.

The Ghan stretches about 3,000 km from my old uni stomping grounds in Adelaide up through the Red Centre to Darwin via Alice Springs. The Indian Pacific starts at Perth on the west coast and snakes its way through South Australia and eventually to Sydney over 4,300 km of track. I've long wanted to both, but cost has always been the primary issue. Isn't it always?

Jim's comment got me thinking though: which other trips would I want to do with Clara as well?

The biggest for us is, unsurprisingly, Japan. I could enumerate every single little rural railway, Shinkansen route, inner-city metro line, and tourist train, but I'd be here all day. Suffice to say, Japan takes their trains seriously, and there are some [incredible journeys](https://www3.nhk.or.jp/nhkworld/en/tv/japanrailway/) in which to partake. Everything from the *[Nozomi Express](https://global.jr-central.co.jp/en/company/about_shinkansen/)* to the beautiful little *[Randen Arashiyama](https://randen.keifuku.co.jp/en/)* tram far exceeded even our hyped expectations. I've written many of the rural train articles for the English Wikipedia, and I'd love to actually go on them.

*(Sitting in the driver's cab of a [maroon Hankyū](https://www.hankyu.co.jp/global/en/) looking out at the blooming sakura trees in semi-rural Kyotō is among the highlights of my life).*

<p><img src="https://rubenerd.com/files/2021/sakura-hankyu-inside@1x.jpg" srcset="https://rubenerd.com/files/2021/sakura-hankyu-inside@1x.jpg 1x, https://rubenerd.com/files/2021/sakura-hankyu-inside@2x.jpg 2x" alt="View out the drivers cab of a Hankyu suburban train of a train platform and blooming sakura trees." style="width:500px; height:375px" /></p>

Not far behind is the *[Empire Builder](https://www.amtrak.com/empire-builder-train)* in the north-western United States. I've talked about it in detail here a few times, but suffice to say it travels from Seattle eastward through the Rockies to the Whitefish ski resort town. I can't tell you how many times I've taken the route in the original Train Simulator, and in Dovetail's version of the game. There's an alternate-reality Ruben out there who moved to the pine-covered mountains of rural Montana and became a telecommuter. *Oh oh, I'm an alien, [I'm a legal alien](https://www.youtube.com/watch?v=d27gTrPPAyk "Sting: Englishman in New York"), I'm an... Australian in a tree?* ♫

Speaking of the Rockies, Canada's [Rocky Mountaineer](https://www.rockymountaineer.com/) looks even more spectacular, heading from Vancouver to Banff. It looks exceedingly expensive like those Australian trains, but looks worth it given the documentaries and photos I've seen. My old man may have travelled on part of during a business trip to Alberta in the early 2000s, much to my chagrin.

One day I also want to take a train journey across Germany to see all the villages and cities where my dad's side of the family came from. [ICE trains](https://www.bahn.com/en/trains/ice-ice-sprinter) also duck 🦆 into Austria too, which would be great to explore. [Geoff Marshall](https://www.youtube.com/channel/UCd18OhMfRmjMjzSHP7Zrzmw) has also reinvigorated my interest in British railways as well.

As for nostalgia, I do want to go with Clara on the [KTMB service](http://www.ktmb.com.my/intercity-routes-map.html) from Woodlands in Singapore to Kuala Lumpur. Not because it's especially nice by fancy train standards, but because I took it as a kid so many times for school excursuions and camps. The original station in [Tanjong Pagar](https://en.wikipedia.org/wiki/Tanjong_Pagar) is no longer in use, but much of the route and rolling stock look exactly the same now as they did back then.

Now that I think about it, there are few train trips I *wouldn't* want to take.
