---
title: "Douglas Brebner on integration and complexity"
date: "2021-02-14T09:53:56+11:00"
abstract: "He refers me to The Cycle of Reincarnation regarding SOCs. I knew I wasn’t the first person to think about this!"
year: "2021"
category: Hardware
tag:
- comments
- feedback
- nostalgia
- retrocomputing
location: Sydney
---
Douglas sent through a couple of fascinating quotes about some recent blog posts, of which I will share with you over the coming days. I heard Bob Marley's voice as I wrote that.

I was *sure* I [wasn't the first person to notice](https://rubenerd.com/that-complexity-inflection-point/ "That complexity inflection point") that devices reach a certain level of complexity and sophistication before they start becoming more integrated. Douglas writes:

> The bit about computers integrating everything into one chip reminds me 
of the term *The Cycle of Reincarnation* that was coined by Ivan Sutherland. There's actually a cycle where computers get more integrated, then add functionality in outboard hardware which is then integrated again. It happened all the way back to the mainframe days and it'll probably happen again. Retrocomputing can be so educational :)

One of these days I'll buy a PDP-11 kit with a SOC chip! If we need to ink out more performance by making our devices more like sealed appliances, may as well get something good out of it too. I need *all* the blinking lights!

Douglas quotes FOLDOC on *The Cycle of Reincarnation*:

> A term coined by Ivan Sutherland ca. 1970 to refer to a well-known effect whereby function in a computing system family is migrated out to special-purpose peripheral hardware for speed, then the peripheral evolves toward more computing power as it does its job, then somebody notices that it is inefficient to support two asymmetrical processors in the architecture and folds the function back into the main CPU, at which point the cycle begins again.
> 
> Several iterations of this cycle have been observed in graphics-processor (blitter) design, and at least one or two in communications and floating-point processors. Also known as "the Wheel of Life", "the Wheel of Samsara" and other variations of the basic Hindu/Buddhist theological idea.
