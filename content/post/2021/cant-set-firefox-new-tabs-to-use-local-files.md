---
title: "Can’t set Firefox new tabs to use local files"
date: "2021-02-23T09:50:04+11:00"
abstract: "In Safari it’s a simple dropdown."
thumb: "https://rubenerd.com/files/2021/safari-set-homepage@2x.png"
year: "2021"
category: Software
tag:
- firefox
- troubleshooting
location: Sydney
---
For years I've used a simple page of links as my browser homepage. It's a launcher with everything I need for my day-to-day activities, including online banking, personal projects, tools for work, online radio, forums, and so on. I thought it'd be easy enough to set this for new tabs as well as new windows. It's not possible anymore, as far as I can tell.

I used to use the **browser.newtab.url** setting in **about:config**, which most of the mass producd tutorial sites still say works, and is listed as the [chosen solution](https://support.mozilla.org/en-US/questions/1073254) on Mozilla's support page. It doesn't work since Firefox 40.

Mozilla determined it was being used for malware, so it's no longer possible without the use of a plugin. Mozilla's support articles reference [New Tab Homepage](https://addons.mozilla.org/en-GB/firefox/addon/new-tab-homepage/) which "is not actively monitored for security by Mozilla", and [New Tab Override](https://addons.mozilla.org/en-US/firefox/addon/new-tab-override/). I'm sure there are others. None of them work.

Among the *many* problems with removing features and delegating them to plugins is they'll never have the same features. Plugins can't reference `file://` locations for security reasons, so I can't use my page of links with any of these plugins. This wasn't a problem when it was a core feature. None of the online tutorial sites, or Mozilla support articles, mention this limitation.

For comparison, Safari let's you set this with a simple dropdown, in a native preferences pane.

<p><img src="https://rubenerd.com/files/2021/safari-set-homepage@2x.png" alt="Screenshot showing dropdown for 'New Tabs Open With'." style="width:500px" /></p>

It reminds me of that old adage that people only use 10% of a program's features, but everyone's 10% is different. Mozilla's software has been chipping away at my 10% for a while now, which sucks.

