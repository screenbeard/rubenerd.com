---
title: "Hales on unexpected LiveJournals"
date: "2021-03-07T11:52:04+11:00"
abstract: "He is disappoint."
year: "2021"
category: Internet
tag:
- comments
- hales
- livejournal
location: Sydney
---
Last Friday I joked that I [had a LiveJournal](https://rubenerd.com/rubenerd-com-has-a-livejournal/) without knowing it. Hales of [Halestrom.net](http://halestrom.net/darksleep/) emailed to say that *his* posts aren't being scraped, and that *he's disappoint*.

Could it be LJ filters out high-quality content, so I slip under the radar? Or maybe they can't parse Atom, which would be pretty funny :).

As an aside, I'm not sure I've spruiked his site properly before. If you've been interested in my [Commodore 128 posts](https://rubenerd.com/tag/commodore-128-series/), you'd love reading about his electronics projects. Unlike me, Hales knows what he's doing. You can read on his [site](http://halestrom.net/darksleep/), or subscribe to his [feed](http://halestrom.net/darksleep/feed.atom).

