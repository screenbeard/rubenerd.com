---
title: "Tech and online meetings"
date: "2021-08-19T09:56:29+10:00"
abstract: "Alamak! Plus, my own idea for a Shark Tank show."
year: "2021"
category: Internet
tag:
- business
- meetings
- singlish
- sociology
- work
location: Sydney
---
What's the most dystopian thing you can think of? Mine would be the word "dystopian", as it immediately conjures dystopian images of dystopia merely as a function of its definition. But what if that dystopia was tracking your emotional state, attentiveness, and speech performance during a meeting about that aforementioned dystopian imagery and mental state?

Wired magazine republished a <a rel="nofollow" href="https://www.wired.com/story/ai-can-run-work-meetings-now-headroom-clockwise/?utm_campaign=UTM-IS-SPAM">story from 2020</a> attempting to answer the question no rational person asked:

> Research has shown that meetings correlate with a decline in workplace happiness, productivity, and even company market share.

True. Many (if not most) meetings are pointless, and only serve to dilute the value of necessary ones. Plenty of studies have concluded this, and we continue to ignore them at our peril. I read that back as "paril", which sounds like "apparal", which is also mispelled. As was the word misspelled just then. *Hi, I'm Miss Pelled, and boy have I got an awful premise for you!*

> Can tech, like automated scheduling tools & facial recognition that measures who’s paying attention, make them better?

*Alamak*.

Sometimes I feel like starting my own parody version of *Shark Tank* in which I fund ghastly, Dickensian ideas tarted up as productivity porn. Introducing the *Kiasu 5000* that <del>narks</del> reports the accuracy of your typing to your manager, so the company doesn't have to pay for time spent backspacing words. Why should a business pay for words you didn't produce? It also encourages the takeup of touch typing, which is some fantastic alliteration and a great idea!

Why do I get the sinking feeling this already exists?
