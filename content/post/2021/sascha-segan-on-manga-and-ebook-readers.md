---
title: "Sascha Segan on manga and ebook readers"
date: "2021-10-12T08:55:28+10:00"
abstract: "Someone who’s tested the larger Kobos for manga says they work well."
thumb: "https://rubenerd.com/files/2021/kobo-sage@1x.jpg"
year: "2021"
category: Anime
tag:
- kobo
- manga
- reviews
location: Sydney
---
Jonathan W saw my recent post about [reading manga on a larger Kobo](https://rubenerd.com/revisiting-kobos-for-reading-manga/) device, and referred me to [this PC Mag review](https://www.pcmag.com/reviews/kobo-forma) that discusses my *exact* use case! Thank you for sharing this.

> I read a bunch of books and manga on the Forma over two weeks. Manga tend to have trouble on smaller e-readers because some of the English annotations can be quite tiny, but even the tiniest text in a CBZ version of Kiyohoko Azuma's Yotsuba&! (a family-friendly book about a cheerful, possibly alien five-year-old) was totally readable. 

I assumed a larger screen would be better for manga, but it was useful to have someone validate it.

I've just put in my preorder for a Sage after a year of indecision. I'll report back how I go. 

