---
title: "When Ubuntu is off topic for Ask Ubuntu"
date: "2021-05-29T15:38:02+10:00"
abstract: "How many other forums limit discussions to supported releases?"
year: "2021"
category: Software
tag:
- forums
- linux
- ubuntu
location: Sydney
---
<a rel="nofollow" href="https://askubuntu.com/">Ask Ubuntu</a> is a Stack Exchange question and answer site for Ubuntu, *surprising though it may seem!* But did you know:

> The following version of Ubuntu and its derivatives officially reach End of Standard Support on April 30, 2021: Xenial Xerus (16.04)
>
> New questions specific to the above versions of Ubuntu posted on or after the enforcement date may need to be considered offtopic. Old questions (before the end of standard support date) that are still being answered regarding these releases should have a comment included on them about upgrading to a supported release to get continued support for their versions of Ubuntu.

That's bizarre; forgive me for saying. How many other forums and Q&A sites enforce whether an OS is supported to ask questions? Isn't the *entire point* of community sites to solicit feedback and advice outside paid support channels? Posts could easily be tagged *legacy* and filtered if potential confusion were a concern.

Sure enough, it's right there in the [on topic](https://askubuntu.com/help/on-topic) page:

> Support for versions for Ubuntu releases past their "End of Standard Support" or "End of Life" dates (whichever is earlier) — unless the question is asking how to upgrade to a supported release.

The existance of the Ask Ubuntu site always puzzled me, given there's already a [Unix and Linux](https://unix.stackexchange.com/) Stack Exchange with significant expertise and toolchain overlap. I'd be opting for posting in the latter now if I liked the Stack Exchange Q&A format... assuming that site also doesn't have such a condition.
