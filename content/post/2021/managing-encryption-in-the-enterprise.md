---
title: "Managing encryption in the enterprise"
date: "2021-04-26T09:51:16+10:00"
abstract: "You have to put trust somewhere!"
year: "2021"
category: Software
tag:
- security
location: Sydney
---
Data Centre Knowledge ran an article about some of the practical challenges [managing encryption in enterprise settings](https://www.datacenterknowledge.com/security/managing-encryption-data-centers-hard-and-it-will-get-harder). They identified:

* Managing keys, including dissemination and revocation

* The arms race between patches, exploits, and brute-force catching up to mathematical complexity 

* Hardware implementations of ciphers that physically impede future upgrades

* Quantum computing

I can think of a few more I've seen:

* Not Invented Here Syndrome&trade;, leading to insecure implementations based on misunderstandings or hubris.

* Monocultures, where we end up with an OpenSSL heartbleed situation, speculative attacks against x86, or network security breaches based on the use of only one company's router.

* The backfire effect, where people work around overbearing security policies to do their jobs. Checking out encrypted documents and putting them on Dropbox, or printing their regularly-rotated passwords on paper next to their desk, for example.

* Misplaced priorities or worries. I liken it to putting armour [where you have bullet holes](https://www.motherjones.com/kevin-drum/2010/09/counterintuitive-world/).

* *Still* an industry-wide lack of appreciation for internal threats. *I don't need outbound firewall rules!*

Security is trust, and you have to anchor it somewhere. The two questions are: what data are you trying to protect, and from whom?

*Just* on software, you're trusting the integrity of its communications, access controls, compiler, cipher suite (both its implementation and academic proof), container, operating system, the silicon running it, physical security of the hardware, and the jurisdiction in which it resides. You can distrust *all* of this, but living in caves isn't tenable for most people. The alternative is to weigh up the pros and cons of each tool, what mitigations you have in place for a breach, and what risks are appropriate and reasonable for a given circumstance.

My silly blog doesn't need Fort Knox. Enterprises depend on well-tested, patched software from reputable companies with established track records, legally-binding contracts, and SLAs. If I'm an informant, I'm handing sensitive data to law enforcement the old fashioned way and leaving the phone at home. But even that assumes that the room where the exchange takes place isn't bugged, or I haven't been followed, or the person I'm giving to is who they say they are, or aren't acting under duress.

Absolute security is impossible. Where you place the pin on the scale is up to you. That goes for individuals and businesses.

