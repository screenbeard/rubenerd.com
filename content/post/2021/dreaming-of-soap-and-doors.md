---
title: "Dreaming of soap and door nomenclature"
date: "2021-10-14T08:22:26+10:00"
abstract: "One of the weirder dreams I’ve had lead me to researching the names for door components."
year: "2021"
category: Hardware
tag:
- doors
- dreams
- personal
location: Sydney
---
People who read dreams, maybe you can help me out.

According to my journal last night, I had a dream in which I felt cheated that a bar of Dove soap I'd purchased didn't have the characteristic dove logo stamped into it. I packed some boxes in disgust for some reason, but was barred from leaving the apartment on account of our front door being jammed. I tried breaking it open with a crowbar, but then I heard a bookshelf collapse, and became distracted over how I'd explain to the landlord why we'd painted the ceiling blue.

Upon waking up for my morning coffee run, I noticed our shoe horn stuck into the side door jam, and a freshly unpacked bar of soap in the sink. I'd have jumped out of my skin if the energy was forthcoming.

As an aside, I had to look up what the various parts of a door were called, and stumbled upon this [excellent article by Marvin](https://www.marvin.com/blog/parts-of-a-door)\:

> If you’re embarking on a door replacement or new build project and you’re overwhelmed with all of the technical terms, we’ve got you. This post breaks down all the parts of a door so you can understand the terminology and how all the parts and pieces come together. Understanding the basic parts of a door makes it easier to talk with contractors and dealers to choose the right door for your project. 

For example, did you know the bar between a sidelight and a door is called a *mullion?* Doors are great, they let you enter a room that would otherwise be inaccessible with just walls.

Another interesting fact would be, if one were included here.

