---
title: "Missing MacBook Pro features returning"
date: "2021-03-05T15:40:01+11:00"
abstract: "Nobody mentions soldered storage anymore. Have we just given up?"
year: "2021"
category: Hardware
tag:
- environment
- macbook-pro
- storage
location: Sydney
---
*This post is dedicated to [Michael Dexter](https://bsd.network/@dexter) of [Call For Testing](https://callfortesting.org/), [bhyvecon](https://bhyvecon.org/) and the [BSD Fund](https://https://bsdfund.org/).*

Rumours are swirling that the latest MacBook Pros will have HDMI ports and an SD card slot again. It's been met with universal praise on Twitter, blogs, and podcasts, with pundits going as far as to say Apple will finally be returning to their functional peak with these additions, before they fell off the keyboard bandwagon. They won't be.

HDMI is sorely needed. Dongles are breakable, are easily lost or forgot about, and will never match the reliability and performance of an internal port. If Apple thought they'd force projector and television manufacturers to adopt DisplayPort the way the accelerated adoption of USB with the original iMac, they failed, and their customers have paid for it in lost time and money.

SD card slots are the same story. There's a reason every MacBook Pro user carries a dongle or port expander, and why almost all of them include USB-A and an SD card slot. Those same pundits laud its inclusion given its ubiquitous use in AV, but what nay gets a mention is how it serves as the only way to physically expand internal storage without a protruding appendage. Wait, why is that you ask?

This leads us specifically to why a 2021 MacBook Pro still doesn't match their earlier models, despite those hypothetical ports and a functional keyboard. *These new laptops still have soldered storage*. It baffles me how rarely this is mentioned, and how resigned the industry is to it.

I can already hear the justifications. Flash storage is more reliable now, and will likely outlive the other components on the machine. Likely? Okay, but what if it does fail? What if the operator thinks warranties are a guarantee of service, but that they still intend to use the machine after as well?

What about privacy and security? While we now have to return entire machines just to fix storage, at least whole drive encryption means a nefarious actor can't do anything with it in transit or in a repair centre, and it'd be safe to throw away, right? Except, full drive encryption is *not* enabled by default, so you can bet most users haven't configured it. Granted the people who know how to swap an NVMe drive or an SSD likely *would* be the ones who use whole drive encryption, but what if you're helping a friend or family member?

Soldering storage to the board saves space though right? It might, but then other laptops with a smaller physical footprint have more storage, so that's immaterial.

There might be performance benefits, but if they're smart enough to build their own CPUs, I'm sure they could design their own high-performance interconnect if they saw it as a design priority.

Soldering storage and memory is a deliberate design decision, which aside from being a great form of alliteration, is tragic. It ties the lifespan of the entire machine to the first failed component. Replacing an entire motherboard because a process has worn-levelled your SSD to oblivion is absurd, not to mention environmentally wasteful. Sure Apple have the tech to refurb the board; I know, I'm using a refurb'd machine for work right now. But you know what's even better? A plug.

I suppose the industry is trending in this direction, and you know that if Apple does it other companies are soon to follow. It won't stop be being all crotchety though.

