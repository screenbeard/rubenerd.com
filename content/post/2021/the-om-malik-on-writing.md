---
title: "The @Om Malik on writing"
date: "2021-11-11T09:46:40+10:00"
abstract: "Your writing should reflect your thinking."
year: "2021"
category: Thoughts
tag:
- om-malik
- writing
location: Sydney
---
Om Malik continues to be a source of inspiration, care, and clarity in a tech world increasingly devoid of it. I hope he knows how important his writing is. If his writing sounds familiar, I've internalised a *lot* of it here over the years.

This was his [advice last year about writing](https://om.co/2020/07/30/write-like-a-human/)\:

> Be real. Write like a person. That is how your words will be unique because only you can be you. 
> 
> Your writing should reflect your thinking. You don’t need to become someone else. You have to look no further than inwards to find your words and your writing style. 
>
> Your writing should have the same compassion you have when you speak and communicate with those you love and respect. Compassion always translates into civility. It shows that you care. 

And a related idea: you can't fake enthusiasm or interest. At least, not for long. You have an interest or passion the world needs to hear about. Write it, and share it with us! And please [let me know](https://rubenerd.com/about/#contact), so I can add you to my blog roll. ♡

