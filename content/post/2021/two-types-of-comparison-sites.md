---
title: "Two types of comparison sites"
date: "2021-05-08T15:19:52+10:00"
abstract: "And they’re almost all pointless!"
year: "2021"
category: Internet
tag:
- writing
location: Sydney
---
It's human nature to compare systems you're researching to ones with which you're already familiar. Varnish and nginx for web proxying, or Firefox and Safari for web browsing, for example. Do a search in 2021, and chances are you'll come across one of these:

* Automated comparison sites. These are glorified databases containing basic stats that can be put into a table and wrapped with ads. They rarely offer anything more than what could be gleaned from sites like Wikipedia; and may have even been scraped from it. They're especially funny when they compare things that have nothing in common, like a package manager and a text editor.

* Churnalism sites (for churn and journalism). These are written quickly and in bulk with barely a superficial understanding of the topic. They'll mention software A, software B, then some form of *in conclusion, both are good options*. This isn't helpful either.

I mentioned something similar at my Linux.conf.au talk about using FreeBSD in the field. Sites tell us that we should go with FreeBSD for broad support, NetBSD for portability, OpenBSD for security, and DragonFly BSD because something something Amiga. That's all great, but what does any of this mean?

You could be forgiven for thinking that with sufficient data mining you could extract information and provide quantitatively-informed advice. Sports commentary is now largely written by bots, so why couldn't the same apply to software?

The thing is, **algorithms can never replace expertise**. Expertise comes from experience and knowledge, and is opinionated.

*In conclusion, Varnish and nginx are both web proxy software with different use cases, strengths, and weaknesses. Choose the one that's right for you!*

We don't need a thousand articles telling us that Varnish and nginx are both caching servers, we want to know your battle stories, experience using in the field, how you think their configuration compares, the implications elsewhere in the stack over choosing one, how responsive and cordial their community is, whether the documentation is useful, how frequently it's updated, and so on. You can't get that information by parsing Wikipedia infoboxes.

I've been talking about the rise and fall of blogging again for the last few years in the context of independent publishing and RSS. Maybe I need to broaden my scope to wish for more *human* writers!

