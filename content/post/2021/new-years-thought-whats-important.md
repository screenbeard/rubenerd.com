---
title: "New year resolutions, goals, and what’s important"
date: "2021-01-27T13:44:42+11:00"
abstract: "Moving on from vague resolutions and goals."
year: "2021"
category: Thoughts
tag:
- personal 
location: Sydney
---
[Phil Gerbyshak](https://philgerbyshak.com/) used to leave blog comments here advocating for defining New Years goals, rather than making resolutions. You're far more likely to follow through with concrete, measurable steps like "enrol for classes" instead of "learn Japanese".

This year I decided to go back to basics and define *exactly* what's important to me, with the aim of writing goals against them. These were the first five things that came to mind, which just happened to be in alphabetical order:

* Family and friends
* Learning (at work, tinkering, building, fixing)
* Security (health, financial)
* Travel
* Writing

It was a learning experience for me to realise just how *few* of my past goals fit into those categories, which goes a large way to explaining why I still didn't have a great track record of fulfilling those goals. I've now got a bit more of a framework to approach which goals I want to achieve this year.

It sounds like a silly exercise, like writing a grocery list. *I'm a smart person who knows what I need to get, right?* Then you come back from Coles and you forgot the avocado. Give it a try, you might be as surprised as I was.

