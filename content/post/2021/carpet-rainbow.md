---
title: "Our carpet rainbow"
date: "2021-08-05T08:37:28+10:00"
abstract: "Always thankful to see this."
thumb: "https://rubenerd.com/files/2021/carpet-rainbow@1x.jpg"
year: "2021"
category: Thoughts
tag:
- colour
- home
- personal
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/carpet-rainbow@1x.jpg" srcset="https://rubenerd.com/files/2021/carpet-rainbow@1x.jpg 1x, https://rubenerd.com/files/2021/carpet-rainbow@2x.jpg 2x" alt="View of a small rainbow streaking across our carpets" style="width:500px; height:333px;" /></p>

She's visited for a few minutes every morning this winter. It's been much appreciated.

