---
title: "Room in New York"
date: "2021-02-01T13:26:27+11:00"
abstract: "By Edward Hopper, 1932"
thumb: "https://rubenerd.com/files/2021/Room-in-new-york-edward-hopper-1932@1x.jpg"
year: "2021"
category: Media
tag:
- art
- whole-wheat-radio
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/Room-in-new-york-edward-hopper-1932@1x.jpg" srcset="https://rubenerd.com/files/2021/Room-in-new-york-edward-hopper-1932@1x.jpg 1x, https://rubenerd.com/files/2021/Room-in-new-york-edward-hopper-1932@2x.jpg 2x" alt="" style="width:500px" /></p>

By Edward Hopper, 1932.
