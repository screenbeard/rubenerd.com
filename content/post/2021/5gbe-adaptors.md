---
title: "2.5 and 5GbE adaptors"
date: "2021-01-04T09:16:00+11:00"
abstract: "Or 10GbE, if the dongles were small and the price was right"
year: "2021"
category: Hardware
tag:
- networking
location: Sydney
---
Clara's and my [new home FreeBSD server](https://rubenerd.com/building-a-new-homelab-server/) *consistently* outperforms my old HP Microserver for Samba and Netatalk file shares to our Macs and other \*nix machines. The fact that the switching hardware, cables, and end points are identical shows just what a bottleneck that hardware was; I suspect it was a combination of poor cooling, not having AES-NI for drive encryption offloading, and some suboptimal ZFS settings that I've since corrected!

But now I'm interested in inking out more, especially when I use the InfiniBand kit at work and see how blistering fast transfers can get. Allan Jude and Michael Dexter suggested during one of the BSDCan streams last year that it was worth paying the extra for 10GbE, but dongles on the client side seem to be more expensive and much bigger.

I've been following Serve The Home's [5GbE adaptor reviews](https://www.servethehome.com/usb-3-1-gen1-to-5gbe-network-adapter-guide/). Here's their most recent for a [StarTech device](https://www.servethehome.com/startech-usb-to-5gbe-adapter-review/) with which they were underwhelmed, and a [Sabrent unit](https://www.servethehome.com/sabrent-nt-ss5g-review-usb-to-5gbe-nic/) which is a bit more robust. All of them use Aquantia/Marvel AQC111U chipset.

I already have an Intel 10GbE card for the aforementioned FreeBSD tower, so these dongles would be used for Clara's and my Macs. I'd love to avoid installing kernel extensions to get one of these working. Sonnet earned my trust and respect with their Mac expansion cards in the late 1990s, but the [website for their Solo5g](https://sonnettech.com/product/solo5g/overview.html) says drivers need to be installed. *Update:* they use the same chipset as above.

Some Mac hardware ships with 10GbE; I'd love for an adaptor that uses the same chipset, if that were at all possible somehow. One can dream :).
