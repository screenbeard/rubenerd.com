---
title: "Hitting random on geographic articles"
date: "2021-07-13T15:50:38+10:00"
abstract: "1914 in the Philippines, and Bookshelf 1995’s contemporary reference to West Germany."
year: "2021"
category: Thoughts
tag:
- encyclopaedias
- germany
- geography
- wikipedia
location: Sydney
---
Encyclopædias were my favourite books growing up, and little has changed as evidenced by my effort to invoke the anarchistic æ. Sometimes I like to go to a random article in my Mac OS 8.6 WorldBook CD-ROM, or Encarta on my Libretto, or Bookshelf on my Windows 95 machine, or Wikipedia on my current tower. They've often lead me down some delightful rabbit–holes.

This was the Wikipedia article about [1914 in the Philippines](https://en.wikipedia.org/wiki/1914_in_the_Philippines)\:

> **1914 in the Philippines** details events of note that happened in the Philippines in the year 1914.

Makes sense.

Bookshelf 1995 that came with Office 95 doesn't have a Random button, but this was the first entry, emphasis added by me:

> Aachen or Aix-la-Chappelle, city (1989 est. pop. 233,000) North Rhine-Westphalia, **W Germany**. It is an industrial center producing textiles, machinery, and other manufacturers. Its mineral baths have been famous since Roman times.

Hadn't the Iron Curtain fallen four years ago by the point Bookshelf *1995* came out? I am disappoint, *Columbia Concise Encyclopedia*.
