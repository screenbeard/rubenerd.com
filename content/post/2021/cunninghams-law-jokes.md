---
title: "Jokes as alternative input for Cunningham’s Law"
Date: "2021-07-01T12:58:53+10:00"
abstract: "The opportunity for people to correct jokes is another untapped resource."
year: "2021"
category: Thoughts
tag:
- jokes
- laws
location: Sydney
---
We all know the Simpsons meme of McBain saying "that's the joke". But what if we could tap this potential hoard of explainers and put their retorts to good use?

*Cunningham's Law* states that the best way to get the right answer on the Internet is not to ask a question; it's to post the wrong answer. I propose jokes are as effective a delivery mechanism to achieve this desired outcome.

**Exhibit A:** A friend forwarded me [this brilliant video](https://www.reddit.com/r/redneckengineering/comments/ob3qj9/) showing an oscillating fan bumping a mouse to prevent a computer sleeping. It was creative, original, and *just a bit silly* in the best possible way.

What do you think most of the responses were? You don't need a hint, but let me provide you with an example anyway: 

> You know you can turn off power saving mode.... right??

The deductive brilliance of these people is wasted on sites like Reddit. Next they'll be asking *have we considered* that video doorbells render knock knock jokes obsolete, or that the chicken could have just crossed at the traffic light. *You do know* that airline food has a reputation for being bad, don't you?

Whether jokes form a subset of "wrong answers", or whether they warrant a corollary theorem to Cunningham's Law, is something I'm still investigating.

