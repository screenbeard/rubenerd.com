---
title: "Linus Tech Tips: Nothing EVER works!"
date: "2021-05-04T18:27:22+10:00"
abstract: "Home automation, foiled by a change in Terms of Service."
year: "2021"
category: Hardware
tag:
- design
- architecture
location: Sydney
---
Linus from Linus Tech Tips did a rant video about the [failings of home automation](https://www.youtube.com/watch?v=x7pSkVarixU), which I felt in my soul. I don't do any of this stuff, but easily substituted so many of my own technical problems for the arduous process he went through to get even the most basic thing right.

He summarised the issues with smart tech in general:

> **Number one:** unexpected service interruptions or policy changes that can brick already perfectly-functioning setups. And **number two:** poor or incomplete interoperability between different brands and platforms.

Related to point one, I'd also blame the industry shift to subscriptions, which is one more thing you need to track and budget for. I used to think this was our fault, because we demand cheaper devices which have to be subsidised somehow. But I'm starting to think it's greed.

I'd also add quality control. There's no reason that half his problems should have existed in the first place, all other things being equal. My Hi-Fi system components that were made in Japan in the 1980s will outlast all the rubbish we buy and use now... *and with style!* Granted it was more expensive, but that can't be the only variable here. The whole *run fast and break things because we can do remote updates* thing has been a tremendous failing of our industry and needs a fundamental rethink.

Linus's solution, at least for point two, was to use Home Assistant, which you run on a server yourself. This is great if you're technical, but an absolute non-starter for anyone who lives in the real world. What do those people do? Hope that they have a knowledgable family member or friend? Pay even more money for installation and ongoing maintenance? 

Clara and I don't use Internet of Things (IoT) devices into our apartment. We like to pretend it's because we're taking security seriously, and that we're taking a stand for privacy and ecological sustainability. Tying the lifespan of something like a lightbulb to an ephemeral web service or smartphone is the last thing the planet needs. But in reality, it's just *easier*. Press a light switch, **boom**. Use a remote control, **boom**. Adjust an aircon remote, **cooling boom**. I've got enough to be anxious over without worrying whether something will turn on or not because we don't have Internet, or a service has changed.

