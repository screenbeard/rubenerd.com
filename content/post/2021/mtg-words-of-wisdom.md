---
title: "MTG words of wisdom"
date: "2021-02-18T07:39:32+11:00"
abstract: "“The difference between a nuisance and a threat is often merely a matter of numbers.”"
year: "2021"
category: Media
tag:
- magic-the-gathering
- quotes
location: Sydney
---
*Magic: The Gathering* cards are underappreciated as sources of funny and profound quips. Here's one from a 2004 *[Chittering Rats](https://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=46096)* card:

> Bottom feeders sometimes rise to the top.

And [Muck Rats](https://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=4231) from 1997:

> The difference between a nuisance and a threat is often merely a matter of numbers.

