---
title: "What you can’t install with NetBSD’s pkgsrc"
date: "2021-06-01T17:09:27+10:00"
abstract: "To clarify for people who think I was bought off."
thumb: "https://rubenerd.com/files/2019/screenie2008-maihime-netbsd@2x.jpg"
year: "2021"
category: Software
tag:
- bsd
- netbsd
- pointless
location: Sydney
---
I've been advised on Mastodon and Twitter that *some* of you think I was paid off or unduly influenced by the NetBSD maintainers to [spruik their cross-platform pkgsrc](https://rubenerd.com/using-netbsds-pkgsrc-everywhere-i-can/) package manager. [pkgsrc](https://pkgsrc.se/) is a cross-platform package manager by the NetBSD maintainers that's a cross-platform package manager.

To assuage any concerns that I was uncritical in my approach to discussing pkgsrc, below is a short list of things it cannot install:

* Bagels
* The Firth of Forth
* Season two of *Star Trek: The Next Generation*
* A coaster depicting Hatsune Miku or Renoir's *La Grenouillè*
* Renoir
* A 3D-printed 3D printer
* Apple's Final Cut Pro X
* A palmtop tiger
* Chocolate-covered mozzarella balls
* Clothing racks, with or without clothing
* A can of compressed air for repairing butterfly keyboards
* Jelly
* Drop-tile ceiling panels
* The source code to either Palm OS or Garnet OS
* Esther Golton's 2007 album *[Unfinished Houses](https://esthergolton.bandcamp.com/album/unfinished-houses)*
* Silicate sand
* A Japanese maid and/or butler café
* A Teac A-30 integrated amplifier with phono input
* Exactly 3 grams of peanut butter
* Plastic slippers (not that you should wear them anyway)
* Patio furniture and awnings
* *Tsundere* trope characters, with or without *zettai ryouiki*
* Apple pies, but with a banana filling
* Carbon nanotubes fashioned into gravity-defying pants
* A 1930s edition of the Encylopedia Britannica
* Knödel

