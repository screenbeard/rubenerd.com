---
title: "British Rail’s Pacer trains"
date: "2021-06-26T09:47:57+10:00"
abstract: "What do you get when you shove a bus chassis on a freight wagon chassis?"
thumb: "https://rubenerd.com/files/2021/yt-WoCROPHjORw@1x.jpg"
year: "2021"
category: Travel
tag:
- trains
- uk
- video
- youtube
location: Sydney
---
What do you get when you put a Leyland bus frame onto a freight wagon chassis with only four wheels? You get a railbus that met British Rail's Thatcheresque 1980s budget requirements, but one that was notoriously bumpy, loud, and uncomfortable. There isn't much love for these trains for the souls who had to ride them everyday, but they're a fascinating curiosity for railfans. *Flippy doors!*

I'd never heard of these trains before, despite growing up obsessed with British and European railways. Geoff Marshall did a great video exploring the last day of these trains operating in Wales.

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=WoCROPHjORw" title="Play The Last Ever Pacer Train"><img src="https://rubenerd.com/files/2021/yt-WoCROPHjORw@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-WoCROPHjORw@1x.jpg 1x, https://rubenerd.com/files/2021/yt-WoCROPHjORw@2x.jpg 2x" alt="Play The Last Ever Pacer Train" style="width:500px;height:281px;" /></a></p>

Check out [Geoff's other videos](https://www.youtube.com/user/geofftech2/videos) too while you're there; his series on London's Lost Railways was beautiful.
