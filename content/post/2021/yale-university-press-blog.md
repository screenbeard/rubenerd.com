---
title: "Yale University Press Blog"
date: "2021-01-29T17:02:54+11:00"
abstract: "I only just found out about this. They have some fascinating book excerpts and stories."
year: "2021"
category: Media
tag:
- blogs
- history
- ocean-liners
location: Sydney
---
I only just found out about this site. The [Yale University Press Blog](http://blog.yalebooks.com/2020/08/04/sinking-the-ss-athenia/), not the site you're reading now. It'd be concerning if I'd only just discovered my own site. Though I suppose... `$_incessant_rambling`.

Here's a published excerpt from *The War for the Seas* by Evan Mawdsley, as posted back in August 2020. I feel like I wouldn't be able to put this book down:

> The SS Athenia was a substantial vessel, but not one of the great liners; a passenger ship of some 13,500 tons, with accommodation for 1,000 passengers, her speed was 15 knots: the white stripe on her single thin black funnel marked her as one of the ships of the Donaldson Atlantic Line. Completed in 1923, she regularly carried passengers – often emigrants – from the British Isles to Canada. In August 1939 there was a new urgency to get aboard, among those hurrying to escape the outbreak of another European war. The Athenia left Glasgow, bound for Montreal, on the evening of 1 September; that day, Germany had invaded Poland. After picking up passengers at Belfast and Liverpool, the liner sailed out into the open Atlantic on the 3rd. A few hours earlier the Prime Minister, Neville Chamberlain, had announced a state of war with Germany.

