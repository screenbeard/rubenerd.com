---
title: "Nora Tindall on the modern web"
date: "2021-09-21T17:27:12+10:00"
abstract: "All that optimised dopamine."
year: "2021"
category: Internet
tag:
- design
- social-media
- sociology
location: Sydney
---
Via her [Mastodon account](https://cybre.space/@tindall/106967151800888113)\:

> Imagine if we had put 20 years of effort into making the internet a safe and productive place for people to live their lives instead of optimizing dopamine output from each individual interaction

Let's take this as a challenge, and not a sign of defeat :).

