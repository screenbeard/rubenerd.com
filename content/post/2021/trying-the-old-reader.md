---
title: "Trying The Old Reader"
date: "2021-12-05T12:34:49+10:00"
abstract: "The experience is so nostalgic, it made me smile."
year: "2021"
category: Internet
tag:
- rss
location: Sydney
---
I've run Fever, TinyTinyRSS, Miniflux, and even a homebrew LibXML-based CGI.pm script on a few boxes over the years, but recently I decided to check out some hosted options for things that I tend not to customise or tweak that much. 

[The Old Reader's](https://theoldreader.com/) name and design is an omage to the original Google Reader, which itself was copied from the likes of Bloglines. There's a lot of familiar UI around, from the sidebar of feeds to the ability to share and star posts.

The experience is so nostalgic, it made me smile. It perfectly captures what I missed about the Internet of yore. It's also easy to recommend to others to try who might not be up for maintaining a VM, or yet another web stack somewhere.

