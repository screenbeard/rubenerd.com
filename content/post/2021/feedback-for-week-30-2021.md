---
title: "Feedback for week 30, 2021"
date: "2021-08-01T09:20:21+10:00"
abstract: "Comments from Jim Kloss, Lucas Vacula, an anonymous anime fan, and Rebecca Hales."
thumb: "https://rubenerd.com/files/2021/harukana-cafe@1x.jpg"
year: "2021"
category: Thoughts
tag:
- feedback
- jim-kloss
location: Sydney
---
I talk to enough people outside IT and finance who are surprised that we refer to weeks with numbers as well. I know its a tad arcane, but it's useful shorthand to disambiguate this post from others. Well, I say that, but then I spend a paragraph discussing it, thereby negating and shorthand that I would have gained from merely referring to this block of time in seven-day increments.

You know what else came incrementally over the last seven days? Blog comments! Ah, that was an *excellent* segue. Though I feel like if I have to tell you it was excellent, it probably wasn't.

At this stage I should rename this blog *An Asynchronous Conversation with Jim Kloss* (the quality would no doubt improve). Jim observed my [mobile-friendly site test](https://rubenerd.com/this-site-is-mobile-friendly/) last Thursday, and decided to take it upon himself to see if Google's Mobile-Friendly Test passed Google's Mobile-Friendly Test. Surprisingly, [it had issues](https://search.google.com/test/mobile-friendly?utm_source=UTM-IS-SPAM&utm_medium=UTM-IS-SPAM&utm_campaign=UTM-IS-SPAM&id=nwqQKuzoU5sPTwVIDr5U3Q)\!

> **Page partially loaded:** Not all page resources could be loaded. This can affect how Google sees and understands your page. Fix availability problems for any resources that can affect how Google understands your page.

Does that mean my silly little site here is better than Google? I'll let you be the judge.

In the embarrassing mistakes department, [Lucas Vacula](https://lvacula.com) sent in a correction for the title of my [CISA vulnerability report](https://cisa-exploited-vulnerabilities-report-for-2021/) post, which I've since fixed:

> Just a friendly heads-up that your CISA post's title says 2011, not 2021. I know a lot of people say that the US government is both a decade ahead and a decade behind in terms of tech, but I don't think this is what they meant! :p

Appreciated, thank you. I'd make a joke here too about US intelligence effectiveness, but I live in Australia, and those who live in glass houses shouldn't throw proverbial shade. I say shade, even though my self-deprecating last name is spelled Schade. Does that mean I'd throw myself?

A new contributor who wished to remain anonymous sent me a long and beautiful email thanking me for my discussions about [mental health at the Olympics](https://rubenerd.com/mental-health-at-the-olympics/), and to say she binge-watched the Beach Volleyball anime *Harukana Receive* on my [passing recommendation](https://rubenerd.com/harukana-receive-and-olympic-sports/) ("only a few butts" and "it made me cry!"). She's had some rough personal circumstances over the last few years, so I was happy that my silly ramblings here helped. When people ask me what motivates me to write, *this* is up there. ♡

<p><img src="https://rubenerd.com/files/2021/harukana-cafe@1x.jpg" srcset="https://rubenerd.com/files/2021/harukana-cafe@1x.jpg 1x, https://rubenerd.com/files/2021/harukana-cafe@2x.jpg 2x" alt="The coffee shop scene from episode 8, my favourite in the series." style="width:500px; height:281px;" /></p>

And finally we get to Rebecca Hales, one of the two great people who share that name who contribute here, and arguably the most clued-in commenter I've ever had when it comes to sports and fitness.

> I didn't pick you as a gymnastics guy but thinking of you being jealous by an acrobatic Russian guy in Singapore was too funny. Have you been watching the swimming?!

I'll admit, I find swimming boring. This is tantamount to blasphemy given it's one of Australia's strongest sports, and the only one where Singapore has won gold. I'm sure they'd deport me if the borders were open.

That's it for this week, is a phrase with five words. Thanks :).

