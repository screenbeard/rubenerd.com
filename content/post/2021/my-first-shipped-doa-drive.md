---
title: "My first shipped DOA drive"
date: "2021-09-29T08:12:42+10:00"
abstract: "I always feared for the safety of hard drives in the post. D’oh."
year: "2021"
category: Hardware
tag:
- hard-drives
- shopping
- storage
location: Sydney
---
There were certain things that I've preferred buying online if I could. Anime magazines and figures, because I used to fear judgement. And among the things I'd prefer to buy in person: fresh vegetables, coffee, and hard drives... though not from the same location. Maybe Tesco, Carrefour, or Giant could do it.

There was something about having the delicate <del>sensibilities</del> components of a hard drive rattling around and being manhandled that made me nervous. I'd rather go to a brick and mortar store, pick it up, and carry it home on a pillow before installing it into its pedestal case. I joke, but that's what tower server cases seem to be called.

It is a *bit* absurd when I think about it now, given the drives would have had to go through transit to get from the factory to the distributor, then to the retailer. But I'd expect a pallet of the things would be treated better, and have more packaging, than an individual drive being rattled around in a postal sorting room.

My fears had thus far been unfounded during These Times&trade; of working from home, with multiple new HGST and Seagate units arriving without any issue over the course of two years. Here comes the proverbial posterial prognostication: *but...* yesterday was different.

*(I won't disclose the manufacturer of this specific device, lest I be told about a certain backup provider's statistics of their unusual use case, followed by someone else saying I made the right decision given another manufacturer's SMR shenanigans, followed by someone else saying the manufacturer everyone cites as the most reliable is owned by the aforementioned shenanigan company, followed by another person asking why Toshiba doesn't get more love, followed by...)!* 

I like to think of myself as a rational person (glaven), but somehow I opened the box from this large Australian retailer and was immediately filled with trepidation. I couldn't tell you why, but something didn't seem right. The padding and packaging wasn't as good as others I'd seen, and had seemed to be chosen so they could shoehorn the larger 140 cm Noctua fan into the same box. But I'd seen far worse, including a bottle of whisky that had managed to arrive with nay a layer of bubble wrap and miraculously survived.

Plugging the drive into my homelab server tower and powering it on resulted in that dreaded metal-on-metal grinding sound that so routinely inhabit my nightmares. I also learned a new feature about my Supermicro board: it beeped at a regular interval to indicate an IO issue. Unplugging the drive silenced these beeps.

ipmitools, dmesg in FreeBSD, and even the BIOS didn't report any drive was connected. The drive had replaced a previous one, so I knew the SATA cables and the board connector were fine, but in case I swapped the former and changed the latter, with the same result.

Now I get to learn how to do another thing during lockdown: file warranty claims and ship devices. I'm sure it's a barrel of fun, unlike the barrel that aforementioned whisky was aged in. Wait, that's the wrong way round. It is round though, at least in one dimension.
