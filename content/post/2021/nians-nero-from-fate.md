---
title: "NIAN’s Nero from Fate"
date: "2021-12-17T22:25:51+10:00"
abstract: "One of my kicks is seeing characters from fantasy or sci-fi series in casual stuff."
thumb: "https://rubenerd.com/files/2021/nian-nero-thumb.jpg"
year: "2021"
category: Anime
tag:
- art
- fate
- umu
location: Sydney
---
One of my kicks is seeing characters from fantasy or far-flung futuristic franchises featuring casual wear instead. I was proud of that alliteration. [NIAN's drawing of Nero](https://www.pixiv.net/en/artworks/74031560) from *Fate/EXTRA* and *Fate/Grand Order* is so cool; I'd buy this rendition as a fig in a heartbeat.

*Heartbeat*... now there's a show and an accompanying theme song cover I haven't thought about in a long time. I could imagine Nick Berry being summoned as a [Caster](https://fategrandorder.fandom.com/wiki/Caster) or [Ruler](https://fategrandorder.fandom.com/wiki/Sherlock_Holmes).

<p><img src="https://rubenerd.com/files/2021/nian-nero@1x.jpg" srcset="https://rubenerd.com/files/2021/nian-nero@1x.jpg 1x, https://rubenerd.com/files/2021/nian-nero@2x.jpg 2x" alt="" style="width:490px" /></p>

That reminds me, I haven't logged into *Fate/Grand Order* in a while. I feel like I've had a bunch of [unintentional](https://rubenerd.com/sitting-in-that-comfy-chair/ "surgery fun!") spare time recently, but it's been taken up by other things. Isn't that how it always goes?

