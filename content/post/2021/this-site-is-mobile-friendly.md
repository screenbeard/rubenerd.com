---
title: "This site is mobile friendly"
date: "2021-07-29T16:50:57+10:00"
abstract: "... good to know!"
year: "2021"
category: Internet
tag:
- accessibility
- design
- google
- phones
location: Sydney
---
I ran this site through [Google's Mobile-Friendly Test](https://search.google.com/test/mobile-friendly?utm_source=UTM-IS-SPAM&utm_medium=UTM-IS-SPAM&utm_campaign=UTM-IS-SPAM&id=EbnGQyBHD8-uzlnPDf35Ig) yesterday, a tool I can appreciate on account of having correct hyphenation. I'm looking at you, *open source software*.

> Tested on: 28 Jul 2021 at 13:42   
> Page is mobile friendly   
> This page is easy to use on a mobile device

That's good to know.

