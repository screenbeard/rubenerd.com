---
title: "Feedback about a slow CF card"
date: "2021-04-12T09:46:40+10:00"
abstract: "It’s almost certainly not a protocol issue, based on feedback."
year: "2021"
category: Hardware
tag:
- memory-cards
- mmx-machine
location: Sydney
---
Yesterday I wrote about a [new CF card](https://rubenerd.com/newer-cf-card-slower-in-my-pentium-tower/) with better specifications performing *worse* as a boot disk on my Pentium 1 tower. I didn't have hard figures, but the machine was noticeably sluggish when performing writes compared to the slower 16 GiB card it replaced. I got more than a dozen comments from people like you who read this, thank you!

Turns out that among the electrical engineers and developers among you, my theory that there's a protocol mismatch is almost certainly false. To recap, I thought maybe its use of UDMA7 somehow confused the VIA firmware on this 1998 motherboard, but that's not how this hardware works. Hales and Jonathan Belford even sent me photos showing the discrete controller next to the flash memory, which makes sense. CF cards are designed to be backwards compatible, and to implement all prior specifications.

I'm still leaving open the remote possibility that's an incompatibility, if only based on the fact that this motherboard has surprised me before. My favourite was the MIDI not working on my Sound Blaster AWE32 card when I had a PCI Adaptec SCSI card installed, but not a PCI Jaz Jet SCSI accelerator card. It also stubbornly refuses to recognise more than 64 MiB of memory, regardless of the density, layout or voltage of the DIMMs I buy for it, and the board's supposed support for up to 288 MiB [sic].

But by far the most likely issue in this case is either a faulty card, or a ripoff. We've all seen the photos of SSDs being sold that are merely a SD card and some glue in a case. I bought this from a (supposedly!) reputable seller in Australia, so I'd *hope* it wasn't the latter. But I'd be interested to see!

Now I'm motivated to do some proper tests. I'll boot it into NT4 and NetBSD next weekend and run it through its paces. I also remembered I have a PCI ATA controller card that I originally bought for an ISA machine; I might try that too to isolate if the controller on the motherboard is flaky.

