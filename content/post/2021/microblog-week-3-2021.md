---
title: "Microblog, week 3 of 2021"
date: "2021-01-19T13:31:44+11:00"
abstract: "Homelabs, Minecraft, chatting with people, hawker food..."
year: "2021"
category: Internet
tag:
- microblog
- twitter
location: Sydney
---
We're into the third week of 2021 already, though confusingly this is only the second week of my Twitter break. Here are my accumulated microblogs for the week:

* I have the uncanny ability to injure myself just before a big house move. Last time it was a multi-day migraine. Today I stubbed my toe so hard it swelled up. Yay!

* *"You don't ring true, so please stop calling me"* ♫

* Hey look, Zendesk logged me out. It must be a day that ends in Y.

* The guy at our local coffee shop knows me by name and my order now. I like that if I fell off the edge of the world tomorrow, there would be people who'd wonder *where did that awkward guy go?*

* Aiyo, I miss Singapore hawker food.

* Clara's and I now have a non-studio apartment for the first time since we moved in together! Having a bedroom sealed off from the work and kitchen areas is a game changer.

* What would we have done stuck at home if not for the joys of homelabs, vintage Hi-Fi, Hololive, and Minecraft?

* Unscientific experiment tallying when people are rude to me in public (queue jumping, not holding open doors or saying thanks, etc). Gen Z: 0. Millennials: 1. Gen X: 1. Boomers: 14.

* Bumped into the American guy on our floor again. He's still desperately trying to figure out how he can bring the rest of his family over. "It's all fucking nuts".

* Should I post this silly entry about the sound of a diesel locomotive? Ah, screw it.

