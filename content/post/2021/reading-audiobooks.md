---
title: "Reading audiobooks"
date: "2021-05-05T09:58:38+10:00"
abstract: "Yes."
year: "2021"
category: Media
tag:
- books
- reading
location: Sydney
---
Craig Thomler [asked on The Bird Site](https://twitter.com/craigthomler/status/1389704760236797953)\:

> Hey folks, if you only listen to audio books, can you really call it 'reading'?

Absolutely :).
