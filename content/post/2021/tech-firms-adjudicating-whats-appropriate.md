---
title: "Tech firms adjudicating on what’s appropriate"
date: "2021-10-19T09:34:26+10:00"
abstract: "A few companies are deciding what we can and can’t see, which should worry us."
thumb: "https://rubenerd.com/files/2021/tumblr-banned-2.jpg"
year: "2021"
category: Thoughts
tag:
- ethics
- identity
- k-on
- technicality-tarpit
location: Sydney
---
jklaiho hit the [nail on the head](https://www.metafilter.com/192976/Vienna-laid-bare-on-OnlyFans#8160805) on MetaFiter:

> [A] small number of US-based megacorporations unilaterally deciding what is appropriate for a global audience.

Cue again the official art from the family-friendly anime *K-On!* that got my Tumblr account auto-moderated as violating their company guidelines. These false positives, and the spectacular failure of the system to block legitimately explicit content made headlines back in the day. Unsurprisingly, the filters and those who implemented them also had no issue with gore (an ethical standard my late mum always thought was "ridiculous").

<p><img src="https://rubenerd.com/files/2021/tumblr-banned@1x.jpg" srcset="https://rubenerd.com/files/2021/tumblr-banned@1x.jpg 1x, https://rubenerd.com/files/2021/tumblr-banned@2x.jpg 2x" alt="A picture of Yui and Mio from K-On that was flagged as explicit by Tumblr." style="width:360px; height:526px;" /></p>

Most online debates of these issues are perenially stuck in the *Technicality Tarpit*: how such systems are trained and implemented, whether they leak personal data, the legality of such systems, and so on. It's to be expected in a room full of nerds, but we also need to talk about:

* Motives, some of which are reasonable, whole others don't adequately balance the public good.

* To what extent we're comfortable letting functional monopolies dictate morality without oversight or input from the public. Assurances thus far here have done little to assuage my concerns.

It sure bolsters the argument for self-hosting too, if you're able to.

