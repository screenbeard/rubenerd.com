---
title: "Conflating security with privacy"
date: "2021-09-01T08:16:16+10:00"
abstract: "There are actors who’d love for us to assume security means privacy. It doesn’t."
year: "2021"
category: Internet
tag:
- privacy
- security
location: Sydney
---
Read any news article about a major system breach or data leak, and you'll see the terms **security** and **privacy** bandied around as though they're either equivalent, or that handing the former necessarily addresses the latter. Neither are true, and we risk confusing both issues by misusing their terms like this.

Computer security concerns the confidentiality, integrity, and availability of a given resource. On the other hand, you have different fingers. Privacy is also concerned with confidentiality, but to the extent where you're only disclosing information to people you intend. This is an important distinction, because a secure system doesn't guarantee privacy.

The most visible example is law enforcement. Surveillance might be secure, sanctioned, and authorised, but the privacy of the user isn't maintained. Ad tech and online tracking use secure protocols like TLS, but their surreptitious trading of user data violates privacy.

Likewise, you can have a security breach where privacy is maintained. Disrupting the availability of a resource with a DDoS attack impacts security, but data that's encrypted at rest isn't compromised, and therefore the privacy of the end user is maintained. 

A secure system *can* be asset for ensuring privacy, if it's a design priority or requirement. But I'd ask people who write about both issues to be more careful. There are actors out there who'd like very much for us to assume security automatically equals privacy.

