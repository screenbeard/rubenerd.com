---
title: "Testing from Big Sur"
date: "2021-05-20T16:25:50+10:00"
abstract: "The screenshots did not prepare me for how bad the UI is."
year: "2021"
category: Software
tag:
- apple
- colour
- design
- macos
location: Sydney
---
This is a test post of my blogging pipeline from my new macOS Big Sur install. It was a success if you're reading this!

macOS 11 Big Sur is *rough*. Lots of ugly and inconsistent interface elements, inexcusably-poor contrast, and plenty of wasted space. It looks like a bad knockoff of macOS made by an open-source software project, or what Microsoft would have released a decade ago. The worries I had for the direction of macOS after Yosemite have all come true.

It's all so beautifully awful and absurd, I'm cradling this coffee and can't wipe the smile off my face! I might go for a nice walk.

