---
title: "Japan’s new PM Fumio Kishida"
date: "2021-10-09T10:05:28+10:00"
abstract: "“Without distribution of wealth there won’t be a rise in consumption and demand.”"
year: "2021"
category: Thoughts
tag:
- economics
- japan
location: Sydney
---
As reported in by Reuters, [via The Asahi Shimbun](https://www.asahi.com/ajw/articles/14435972):

> Japan should strive for a new form of capitalism to reduce income disparity that has worsened under the coronavirus pandemic, says former foreign minister Fumio Kishida.
>
> “Without distribution of wealth there won’t be a rise in consumption and demand... there won’t be further growth if distribution of wealth is lost,” Kishida said at a presentation of his economic proposals in Tokyo on Wednesday.

Makes sense.

