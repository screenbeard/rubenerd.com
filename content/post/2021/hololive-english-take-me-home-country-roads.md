---
title: "Hololive English: Take Me Home, Country Roads"
date: "2021-05-24T09:30:35+10:00"
abstract: "I didn't know how much I needed this."
thumb: "https://rubenerd.com/files/2021/yt-I1cdQp5uz5s@1x.jpg"
year: "2021"
category: Media
tag:
- amelia-watson
- hololive
- music
- music-monday
- ninomae-inanis
location: Sydney
---
<p><a href="https://www.youtube.com/watch?v=I1cdQp5uz5s" title="Play Take Me Home, Country Roads - hololive English -Myth- Cover"><img src="https://rubenerd.com/files/2021/yt-I1cdQp5uz5s@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-I1cdQp5uz5s@1x.jpg 1x, https://rubenerd.com/files/2021/yt-I1cdQp5uz5s@2x.jpg 2x" alt="Play Take Me Home, Country Roads - hololive English -Myth- Cover" style="width:500px;height:281px;" /></a></p>

[Music Monday](https://rubenerd.com/tag/music-monday/) is a long-running series in which I discuss music on Mondays, surprising though the nomenclature may appear. There's so much backstory to this song in the Hololive universe, so seeing them do a collab for it made me smile more than I care to admit!

> "We are here for you to overcome the difficult times."

Now I just need them to do James Taylor's *Carolina in my Mind* to spur further nostalgia for a place I've never been. It'd play so well in Ame’s Café, right? Right?
