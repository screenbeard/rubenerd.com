---
title: "Yeah but, like, you weren’t there man!"
date: "2021-09-04T09:08:14+10:00"
abstract: "Saying someone “wasn’t there” isn’t the comeback certain people think it is, especially in tech."
thumb: "https://rubenerd.com/files/2021/the-big@1x.jpg"
year: "2021"
category: Internet
tag:
- social-media
location: Sydney
---
You know when you're talking with someone online, or reading a forum thread, and the conversation devolves from discussing ideas and opinions to dismissal based on something about the OP themselves? Logicians refer to these as *ad hominem* attacks, which to me always sounds like *ad hummus* despite not being spelled anywhere near close to that.

*(Vim is telling me hominem is a misspelling of hominid, which I suppose we all are. There's a profound statement about the human condition in there somewhere. Emacs with aspell, on the other hand, had different fingers).*

Among the sillier of these retorts I've read is that someone "wasn't there" if they don't remember a circumstance the exact same way they did, or arrive at different conclusions. I'm seeing this pop up more frequently on social media, because of course those sites turn anything good into stale hummus. Maybe we're all a bit anxty being IT workers stuck at home without introvert time for years now.

Aside from being an aforementioned self-defeating *ad hummus*&mdash;damn it&mdash;laced with some pretentious gate-keeping, it's trivial to challenge if one were bothered to. *Yes, I was there!* Now we have a feedback loop justifying a person's lived experience, rather than the original discussion. Gee, I wonder why they'd want to direct attention away like that?

<p><img src="https://rubenerd.com/files/2021/the-big@1x.jpg" srcset="https://rubenerd.com/files/2021/the-big@1x.jpg 1x, https://rubenerd.com/files/2021/the-big@2x.jpg 2x" alt="The Big Lebowski: Yeah, well, you know, that's just like, uh, your opinion, man." style="width:485px; height:243px;" /></p>

My experience is that it's simple projection; people fall back on these when they themselves have something to hide, or haven't been entirely truthful themselves. I'm no saint here; I've caught myself saying words to this effect *many* times, especially in the [heat of the moment](https://www.youtube.com/watch?v=_7n2-wANjkU "Asia: Heat of the moment! ♫"). It's easier to make sweeping statements and assert the OP "wasn't there" when challenged. Facts have to be argued on evidence, but someone's experiences are just that. Experiences are as much emotional journeys too, and we all know they aren't governed by logic.

I get there are some circumstances where such dismissals could be warranted. Say, if someone cosplaying a cancer survivor talks about the herbal remedies that cured them, or a veteran claiming to have served in a country in which they've never set foot. But even then, the fact "someone wasn't there" wouldn't be the most egregious detail in that exchange.

Heaven forbid such trolls ever need to converse with a historian. Those damned professors, they weren't ever in third century Crete, eating hummus.

