---
title: "John Oliver on hair"
date: "2021-05-18T16:30:31+10:00"
abstract: "I had no idea that Black and African American people had to deal with this bullshit."
thumb: "https://rubenerd.com/files/2021/yt-Uf1c0tEGfrU@1x.jpg"
year: "2021"
category: Media
tag:
- human-rights
- john-oliver
- psychology
location: Sydney
---
John Oliver did a segmnent on his [Last Week Tonight](https://www.youtube.com/channel/UC3XTzVzaHQEd30rQbuvCtTQ) programme about [hair](https://www.youtube.com/watch?v=Uf1c0tEGfrU), and the unique pressures placed on Black and African American people. I feel ashamed to admit that I was completely oblivious to all of this. Which now has me thinking what else I take for granted, or assume, or don't realise.

<p><a href="https://www.youtube.com/watch?v=Uf1c0tEGfrU" title="Play Hair: Last Week Tonight with John Oliver (HBO)"><img src="https://rubenerd.com/files/2021/yt-Uf1c0tEGfrU@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-Uf1c0tEGfrU@1x.jpg 1x, https://rubenerd.com/files/2021/yt-Uf1c0tEGfrU@2x.jpg 2x" alt="Play Hair: Last Week Tonight with John Oliver (HBO)" style="width:500px;height:281px;" /></a></p>

Those outside the US might need to use a VPN to watch it, but please do if you can, it's worth it.

*As an aside, Clara and I saw John Oliver when he did a standup show at the State Theatre in Sydney. He was witty and lovely, exactly what we hoped!*

