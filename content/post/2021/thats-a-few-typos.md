---
title: "That’s a few typos"
date: "2021-08-20T08:15:53+10:00"
abstract: "A yak-shaving post that’s a bit inside baseball, whatever that means."
year: "2021"
category: Thoughts
tag:
- feedback
- language
- weblog
- writing
location: Sydney
---
I think I've set a new weekly record for the number of posts including a spelling, grammar, or punctuation mistkae. Wiat, *mistake*. **WAIT**, mistake. Thanks to all of you who sent corrections, and others who saw the mistakes, shook your heads in bemusement, and moved on.

People ask me how I blog regularly. *Turns out* it's the same way that all but guarantees such linguistic issues. Are linguistic issues limited to speech? Most of my posts are written within the space of a few minutes, if that. I find the *stream of conciousness* approach easier to write, and sounds more conversational when read back.

Long-form posts tend to be written on weekends. Those are ones that have more research, maybe inline images, and references. With hindsight I should have classified them differently from posts like this, so you could only read long-form content if you wanted. I suppose I could write a script to tag each post with more than a specified number of words. I'll just add that to the burgeoning *one day* pile, along with migrating my Jekyll paper theme to Hugo to replace this one, and learning how to fry garlic without it spitting everywhere.

I can't remember who's book on writing I gleaned this from, but they suggested the best way to write is in "typewriter" mode without a backspace key. It forces you to get your ideas down so you can wordsmith them later if needed. It's such a simple idea, but it's the best bit of advice I've ever read.

Ideas are what are import to me, not flair or style. *As I'm sure you've noticed!*
