---
title: "That distinctive ALCO engine rattle"
date: "2021-01-19T13:50:21+11:00"
abstract: "Wandering into the weeds of an obscure hobby to answer a question from my childhood."
thumb: "https://rubenerd.com/files/2021/emd-f7-alco-pa4@1x.jpg"
year: "2021"
category: Travel
tag:
- australia
- alco
- nostalgia
- railfan
- trains
location: Sydney
---
Have you ever had a specific sound from your childhood that you've never been able to place? Then years later you realise what it was, but you can't feel like you can tell anyone because it's *so* niche that nobody of a sound mind would understand or care? That's what having your own blog is for.

I had a few railway photo books as a kid, one of which predominately featured an early generation EMD diesel operated by Australian National (ne. ANR). These bull-nosed cab locomotives are easily the most distinctive and recognisable trains of the time period, and I thought looked especially fetching in the green and gold livery of AN. Years later I'd find myself studying in Adelaide [just outside one of the yards](https://www.flickr.com/photos/rubenerd/albums/72157619024753938) the former AN used.

I also had a few VHS tapes about Australian steam locos, one of which I distinctly remember being photobombed by the same EMD diesel I saw in that book. I was struck at the time how distinctive it sounded; almost like a rattling kettle on a stove. Was it supposed to sound like that? Was it just old? I never knew, and wasn't in a rush to find out given I thought I was only interested at steam and commuter rail at the time.

Years later I started learning about dieselisation. Just like the conversion of ocean liners from coal to oil firing; the shift from steam to diesel was nothing short of revolutionary... if the engines had been Union Pacific turbines. That was an engineering pun I've wanted to make for years. It shook up the entire industry, and coincided with the introduction of containerisation that's the hallmark of rail freight today.

<p><img src="https://rubenerd.com/files/2021/emd-f7-alco-pa4@1x.jpg" srcset="https://rubenerd.com/files/2021/emd-f7-alco-pa4@1x.jpg 1x, https://rubenerd.com/files/2021/emd-f7-alco-pa4@2x.jpg 2x" alt="" style="width:500px" /></p>

General Motors’ Electro-Motive Division weren't the first to make diesel electric locomotives, but they had the biggest impact during this transition period from the 1940s. I read up about their F and E units, and how they'd been built under licence in Australia for various railways. But they didn't have a monopoly; A.E. Goodwin also built Alco locomotives in Australia, some of which bore a similar, if boxier, resemblance to these EMD units. Compare the above cab photos of a <a href="https://commons.wikimedia.org/wiki/File:ATSF_309_(Flickr_22322834666).jpg">EMD F7</a> by Roger Puta, with this preserved [ALCO PA-4](https://commons.wikimedia.org/wiki/File:Nickel_Plate_Road_190_at_Oregon_Rail_Heritage_Ctr,_Sep_2012.jpg) taken by Steve Morgan.

Then it dawned on me this year: the loco I saw in that grainy, worn VHS tape might not have been an EMD at all, but an Australian-built analogue to the Alco FA or PA. RailPictures.net [has a few domestic examples](https://www.railpictures.net/showphotos.php?locomotive=930%20Class) taken from the 1970s and 80s.

So I did some digging on YouTube, and found an Australian video with that *exact sound I remembered from my childhood!* I'm by no means the first person to think these locomotives sounded distinctive and unusual, given the comments and description on the video. YouTube is full of people discussing these locos, and seeking them out around the world from India to Argentina.

<p><a href="https://www.youtube.com/watch?t=240&v=fqw3lx-jVqI" title="Play Alco Action (!) Alco fans"><img src="https://rubenerd.com/files/2021/yt-fqw3lx-jVqI@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-fqw3lx-jVqI@1x.jpg 1x, https://rubenerd.com/files/2021/yt-fqw3lx-jVqI@2x.jpg 2x" alt="Play Alco Action (!) Alco fans" style="width:500px;height:281px;" /></a></p>

If there's anything better than wandering deep into the weeds of an obscure hobby to answer a childhood question, I don't want to know. *Rattle rattle*.
