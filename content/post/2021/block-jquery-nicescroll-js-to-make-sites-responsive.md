---
title: "Block jquery.nicescroll.js to make sites responsive"
date: "2021-06-09T10:06:06+10:00"
abstract: "Web devs: Don’t replace browser functionality."
year: "2021"
category: Internet
tag:
- accessibility
- anti-patterns
- design
- javascript
location: Sydney
---
Have you ever visited a website, attempted to scroll, and...<br />&nbsp;&nbsp;&nbsp;&nbsp;...it feels really...<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...jerky and...<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...slow?

Check out the [Ring of Saturn website](https://networking.ringofsaturn.com/Protocols/dhcp.php) to see what I mean. I don't mean to pick on them specifically, it's just a useful demonstration.

I looked at the page source, and found this ironically-named **Nicescroll jQuery** package which they'd imported. The <a rel="nofollow" href="https://nicescroll.areaaperta.com/">official site</a> showcases a newer version which is smoother than the above site, but is so floaty and imprecise it could make you feel seasick. The <a rel="nofollow" href="https://jsfiddle.net/inuyaksa/thohu1se/">JS Fiddle</a> is equally poor, with a visible delay between when you scroll and when the page responds.

If you use uBlock Origin or a similar plugin, you can banish it by adding the following to your blocklist:

	*/jquery.nicescroll.js

Anti-patterns like this aren't new. Certain web developers have been duplicating browser functionality in JavaScript for years, at huge cost to accessibility and page load times. It's equal parts hilarious and tragic, like the kid who bounced a basketball into my face and bent my glasses last week. *Only at least the kid didn't try to sell the result as a vision improvement!*

If this were a [@ShitUserStory](https://twitter.com/ShitUserStory)\:

> **As a:** website visitor   
> **I want to:** have native scrolling replaced with a JS library   
> **So that:** I can s-s-s-scroll worse.

