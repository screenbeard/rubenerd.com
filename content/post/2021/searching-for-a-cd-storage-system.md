---
title: "Searching for a CD storage system"
date: "2021-06-25T08:52:21+10:00"
abstract: "Closest I've found is DiscSox, anyone else have ideas?"
thumb: "https://rubenerd.com/files/2021/cd_pro_1024@1x.jpg"
year: "2021"
category: Hardware
tag:
- cd
- cd-roms
- music
- storage
location: Sydney
---
I have a *lot* of multimedia CD-ROMs, and Clara and I have an expanding music collection of CDs, cassettes, vinyl, and MiniDiscs. There isn't much we can do about the physical dimensions of most of these, but CD jewel cases have a *lot* of wasted space. Given they constitute the bulk of our physical media, we thought it was worth trying to consolidate them down.

CD binders were the first thing we tried. We cracked open the jewel cases, and put the front and back paper inserts into empty CD pouches in the folders. It's already has saved us a *ton* of space in this tiny apartment. It's also fun to take the binder over to the couch and leaf though them to choose what music to play for that evening.

Have I mentioned that streaming services suck, and you should buy music? But I digress.

There's just one problem with the CD binder approach. The back inserts are wider than the front inserts, because they don't need to accomodate the hinge for the CD jewel case. That means they need to be put into the pouches at a 90 degree angle, which makes reading the text difficult. They also only fit in the lower pockets and require you to overlap the CDs above them, otherwise they'd protrude from the binder and the zip won't close.

I feel like there should be a better way, so I started looking for alternatives.

<p><img src="https://rubenerd.com/files/2021/cd_pro_1024@1x.jpg" srcset="https://rubenerd.com/files/2021/cd_pro_1024@1x.jpg 1x, https://rubenerd.com/files/2021/cd_pro_1024@2x.jpg 2x" alt="Press picture of a DiscSox sock, with space for the back and front paper inserts and the CD." style="width:400px; height:426px;" /></p>

<a href="https://mmdesign.com/Music/Sleeves/cd-pro-poly-sleeve.php#">DiscSox</a> are an interesting idea. The sleeves are taller than they are wide, so they can accommodate the back inserts. I'd prefer to have a folder though rather than loose pages though.

As far as I can tell, is a phrase with six words. I can't see anyone who makes a folder that can accomodate both paper CD inserts. Surely Clara and I can't be the first people to want something like this?

Anyone have [ideas](https://rubenerd.com/about/#contact)?

