---
title: "Minibarring people who use your system"
date: "2021-03-11T08:50:29+11:00"
abstract: "When requirements are implemented badly."
year: "2021"
category: Software
tag:
- antipatterns
- design
- minibarring
- user-hostile-design
location: Sydney
---
I had a oddly-specific dream last night, this time about a bizarre, recurring encounter from a holiday when my sister and I were kids.

Every year or so my dad's company would pay for a holiday back to Australia, so we could visit friends and relatives we left behind when we moved to Singapore. Usually we stayed with our favourite family friends up in Normanhurst, but sometimes we'd stay at the Novotel near the airport.

One early morning, exhausted from a late flight, we were jolted from sleep by the sound of a shrill person bashing on our door:

> **KNOCK-KNOCK KNOCK-KNOCK    
> GOOD MORNING... MINIBAR    
> KNOCK-KNOCK KNOCK-KNOCK**

My roller bed was closest to the door, so I bore the full brunt of her assertiveness. I blinked a few times, rolled over, and tried to go back to sleep. 

> **KNOCK-KNOCK KNOCK-KNOCK    
> GOOD MORNING... MINIBAR    
> KNOCK-KNOCK KNOCK-KNOCK**

I might not have fully woken up by this point, but through a fog of anxiety and irritation I started wondering why this crazed person was bashing on our door, wishing our minibar a good morning. I wasn't about to; the minibar was expensive and tempting even while I was awake and in full control of my faculties, let alone when I was half asleep and wanting a snack.

After the third **MINIBAR**, I saw my dad trudging out from the bedroom in his hotel gown and slippers, rubbing the sleep from his eyes. He opened the door.

> **GOOD MORNING SIR, DO YOU NEED ANYTHING REPLACED IN YOUR MINIBAR?** 

His blank, slightly confused expression should have been enough, but he also almost strung together a cohesive response.

> I, um... good, I'm, no I'm, thanks.

He closed the door, caught my eye and raised his eyebrows. He trudged back to bed, though I think neither of us got any more sleep.

The next morning around 07:00:

> **KNOCK-KNOCK KNOCK-KNOCK    
> GOOD MORNING... MINIBAR    
> KNOCK-KNOCK KNOCK-KNOCK**

It was the same person from the day before, with the same double knock and forceful but flat voice. This time my dad bolted out of bed and stormed to the door with a determined gate. I don't remember exactly what he said, but paraphrasing:

> Is respecting the DO NOT DISTRUB sign optional? Please don't come back.

And of course, she did, the next day. This time I opened the door, and replied with a meek "no", then closed the door before she could get a word in edgewise.

That night we went to the office space downstairs and took some paper to write "PLEASE NO MINIBAR", which we affixed to our door. It worked; we never heard from **MS MINIBAR** again.

My mind is still full of questions even after two decades. Why did management think it was appropriate to bash on people's doors that early? The Novotel chain is primarily catered to business travellers, so surely some of them would have arrived on redeye flights. Even if they hadn't, why even ask in the first place? Surely housekeeping could just do it. Were their margins so thin, and the minibars so profitable, that they factored in *its* needs over their own patrons?

I'm going to start using the term *minibarring* for when I have a work requirement that sounds necessary and logical (restocking a minibar when it's low) that has a user-hostile implementation (asking guests loudly at 07:00).

