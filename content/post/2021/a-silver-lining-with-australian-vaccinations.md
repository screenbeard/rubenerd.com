---
title: "A silver lining with Australian vaccinations"
date: "2021-08-11T12:20:16+10:00"
abstract: "Hopefully the people waiting for Pfizer or taking their time have got the message."
year: "2021"
category: Thoughts
tag:
- covid-19
- health
location: Sydney
---
If we're to take even a sliver of a silver lining (silver of a sliver lining?) about Sydney's current Delta outbreak, [it's this](https://www.theguardian.com/australia-news/live/2021/aug/10/australia-covid-melbourne-covid-gladys-berejiklian-sydney-lockdown-andrews-vaccine-melbourne):

> Gladys Berejiklian: [...] People who previously thought, I can wait for vaccinations, they are now incentivised.

It's been encouraging seeing all the young people snap up AstraZeneca's Covid vaccine, despite a concerted and baffling campaign to obfuscate and limit access to it. Not to mention people who should have known better discrediting its effectiveness and safety. Anyone who's "waiting for Pfizer" without a specific medical condition now is now as culpable as an anti-vaxxer or <del>misinformation</del> lie spreader.

Australia's political and medical leadership coasted for far too long on vaccines, content that we didn't have domestic cases to worry about compared to the *infinitesimally* small risks any one vaccine carries. Anyone but a fool would have seen that position wasn't tenable.

It's also easy to see how the rest of the world is doing from Australia, or New Zealand, or Singapore, and think we've got time. We don't. Get a jab. Do it for yourself, the people you care about, and for good old fashioned civic duty and responsibility.

