---
title: "Benelux has a logo"
date: "2021-12-10T14:29:58+10:00"
abstract: "I thought it was clever how they had a stylised map and colours from their flags."
year: "2021"
category: Thoughts
tag:
- colour
- design
- geography
location: Sydney
---
I caught myself reading Microsoft Bookshelf 96 on my Pentium tower again last week, like a gentleman. The random article of the day talked about Benelux, the customs and economic union between the Low Countries of Belgium, the Netherlands, and Luxembourg. I always thought it was a clever name.

Little did I realise that they have an official logo, which you should see below if your browser supports SVG. Are there any conteporary browsers that still don't?

<p><img src="https://rubenerd.com/files/2021/benelux.svg" alt="Logo of Benelux, from Wikimedia Commons" style="width:320px;" /></p>

The dots on the left are an interesting stylised version of the geographic area, and the pallete has at least one colour from each member state. We've got:

* Belgium: 🇧🇪
* The Netherlands: 🇳🇱
* Luxembourg: 🇱🇺

I only noticed recently that red is the only colour they have in common, unless you count the Dutch royal blue and Luxembourgish cyan as simply blue. *Da Ba De, Da Ba Di*.

I reckon it'd look *way* more awesome typeset in [Helvetica](https://en.wikipedia.org/wiki/Helvetia) though! That said, wouldn't that make more sense for the arrangement [Switzerland has with Liechtenstein](https://en.wikipedia.org/wiki/Liechtenstein%E2%80%93Switzerland_relations#Cooperation)?

I'm also only but a typeography and logo enthusiast, but there's something about a grid of dots that also looks decidedly European. I don't know why.

