---
title: "Playing female characters in games"
date: "2021-12-31T10:25:39+11:00"
abstract: "This generated way more feedback than I expected. I hope I do it justice."
thumb: "https://rubenerd.com/files/2021/avatars@1x.jpg"
year: "2021"
category: Media
tag:
- games
- masculinity
- personal
- pokemon
location: Sydney
---
Thanks everyone for the feedback on my [Nintendo Switch Lite post](https://rubenerd.com/the-nintendo-switch-lite/); there's more interest in this budget console than I realised! Many of you [noticed from screenshots](https://rubenerd.com/beginning-pokemon-brilliant-diamond/) that I'm playing a female character again, and wanted to know why. A few more were trolls, though that's to be expected for people who have yet to face their own repressed feelings.

The answer isn't anything special: it's because I can. Back to *Pokémon!*

Okay, maybe there's more going on here. Some of the nicest, most heartfelt comments came from my scribbled posts on [masculinity](https://rubenerd.com/feminine-men/) and [male hobby judgement](https://rubenerd.com/hobby-judgement-as-an-adult/) that I thought it was worth exploring more here.

<p><img src="https://rubenerd.com/files/2021/avatars@1x.jpg" srcset="https://rubenerd.com/files/2021/avatars@1x.jpg 1x, https://rubenerd.com/files/2021/avatars@2x.jpg 2x" alt="Characters from Fate/Grand Order, Minecraft/Hololive, and Pokémon." style="width:500px; height:225px;" /></p>

The most obvious reason, and one that my friend Amy pointed out, is that *women tend to be more interesting* in games! With a few genre exceptions, anime, mobile apps, and related fandoms clearly spend more time and attention on the designs of their female characters, for reasons that could skew either way. I was tired of looking over Clara's shoulder at all the cooler stuff she could get with her avatars compared to mine. If you'll forgive the [self-quote](https://rubenerd.com/hobby-judgement-as-an-adult/)\:

> ... the general idea that it’s permissible for boys to be interested in what’s basically dress up of electronic dolls is a monumental step forward in breaking down these gender stereotypes. I don’t think that gets enough credit.

Coupled with the fact that almost all my favourite characters in pop culture are female ([WAH!](https://www.youtube.com/channel/UCMwGHR0BTZuLsmjY_NT5Pwg "Hololive's Ninomae Ina'nis")), the choice is obvious. In the timeless words of an anime club member who's name escapes me, *Kaito is great, but who wouldn't choose Miku?*

People using avatars with a different gender, especially in the anime and fantasy communities, is also nothing new. I've read armchair philosophers ascribe this to escapism, but I also see it as a way of living out an alternative persona, especially for those who may be too shy to explore certain aspects of themselves in public. Game characters are no different. They have no basis in reality whatsoever, so why not?

That question wasn't always easily answered, at least personally. I still cringe over a trip to an arcade when I was an early teenager, where some classmates tore me a new one for choosing a female character in one of those forgettable fighting games (and not even one of the "sexy" ones). I was already regularly bullied enough to tears, but coupled with my anxiety about masculinity at the time, it stung enough for me to retreat further into my shell. I'm relieved that I can be a grown, mostly-straight gentleman now and choose things without caring about fragile egos... *theirs and mine included!*

We could leave it at that, but I'm the son of Debra Schade, and I like to pretend there's a subversive reason too. I resent the idea that being male is the default, and that the onus is on us to defend an overt choice to play as someone else. She was the first to tell me how everything from psychology to pop culture are set up in this way, given the taboo in most circles for men to exhibit any traits that would be perceived as feminine. As she said, there's an attitude that doing so is a "demotion", which is so much tosh.

And finally, who can resist the temptation to push back against those who decide, without authority or invitation, through whom we can play. As [@Azusa__cat said](https://twitter.com/azusa__cat/status/1476155481441902601)\:

> They’re called fantasy games for a reason. Enjoy the games however you want. There is a reality game called life, and it’s shit. There is too much “I don’t like this so you can’t have it” around lately. Life is too short so just have fun.

I need that on a shirt, that my alter-ego Ruby wears in her isometric worlds. It's amazing how much more travel she gets up to than me thesedays.
