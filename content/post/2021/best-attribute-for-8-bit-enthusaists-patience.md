---
title: "Best attribute for 8-bit enthusiasts: patience"
date: "2021-03-28T20:19:53+11:00"
abstract: "Still waiting for parts for months, even assuming they’re the correct ones."
year: "2021"
category: Hardware
tag:
- commodore
- commodore-128
- commodore-128-series
- commodore-plus-4
location: Sydney
---
In my continuing [Commodore 128 series](https://rubenerd.com/tag/commodore-128-series/), today I'm taking a break from the hardware to discuss the very idea of troubleshooting 8-bit computers, and why there has been such a gap between these posts. I've been surprised at just how popular these have been, so I'm happy to shoot the breeze :).

In short, is a phrase with two words. Very little has happened since my last excited post about the [1571 disk drive](https://rubenerd.com/my-commodore-1571-drive-arrived/) and adventures with [monitor stands](https://rubenerd.com/a-commodore-128-monitor-stand/). But it hightlights something that I'm only beginning to remember again when dealing with vintage tech.

But first, I have to mention just *how much respect* this experience has given me for people who create vintage tech videos! I avoided talking about Commodore computers for years here not just because trolls had attacked me for it before, but because shipping times can blow out troubleshooting by *months*. Those YouTubers must have a continous supply of parts on planes and boats around the world to feed their various projects, otherwise they'd have nothing to film. I hadn't considered the fact that their videos can often seem out of order for what they're working on, given they're dependent on global supply chains.

This is only exaserbated by the troubleshooting process itself. A part you order online and have delivered a month later might not solve your problem, or it might introduce a new one. I've come tantalisingly close to fixing so many things before realising I need something else, and waiting another month or few weeks. No wonder other 8 and 16-bit enthusiasts I see have well-stocked boxes of various parts.

Don't get me wrong, the Internet is the lifeline that's keeping these machines running! I'm *so* thankful that it let me get in touch with someone in Hungary for replacement key mechanisms, and a chap in the UK for a VDC RAM upgrade, and a gentleman in South Australia who offered to send me the C128 in the first place! We can complain about how disrupted and slow global logistics are right now due to COVID and more evergreen reasons, but it still amazes me that it's even possible. Nobody would open a brick and mortar Commodore nostalgia store now, so it's either the Internet, a close local friend if you're lucky enough, or nothing.

Right now I have the following in transit:

* A Commodore VIC-II to S-video cable
* C128 VDC 64 KiB memory upgrade
* Replacement 8563 VDC IC for the C128's 80-column mode
* New switching power supply for my Plus/4

I now have, waiting in the wings:

* A small VGA upscaler that accepts VGA, composite, and s-video
* A CGA2RGB converter for the 80-column RGBI signal
* A new monitor stand that can also accomodate the 1571

None of these were expensive, though the shipping costs either eclipsed or matched the listing price for some of them. I guess living in an isolated geographic bubble has its pros and cons!
