---
title: "Today’s errors"
date: "2021-01-27T21:29:43+11:00"
abstract: "Arcing power supplies, Minecraft launchers, ThinkPads, mouses, tethering, and more!"
year: "2021"
category: Hardware
tag:
- hi-fi
- security
location: Sydney
---
This all happened today. Rational people would say this is all confirmation bias, and I was out to find further examples given I got grumpy from a few failures! I like to pretend the world was out to get me.

* The power supply in my 1980s TEAC amplifier arked a bright blue inside, and died.

* My ThinkPad T550 reports detecting no internal keyboard or trackpad.

* The Minecraft launcher on Clara's and my server initially crashed on start. Rebooting the server now leads to "lost connection: Disconnected" errors.

* Our NBN connection that was supposed to be enabled in our new apartment this morning wasn't.

* My trackball mouse no longer responds to its left button, and one of the screws was attached too hard to remove.

* Our AppleTV couldn't get to any of our tethered or ad-hoc Wi-Fi networks.

* [Sudo](https://blog.qualys.com/vulnerabilities-research/2021/01/26/cve-2021-3156-heap-based-buffer-overflow-in-sudo-baron-samedit).

* Misspelling "arcing" as "arking"

