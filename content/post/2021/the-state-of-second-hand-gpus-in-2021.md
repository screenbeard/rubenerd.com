---
title: "The state of second-hand GPUs in 2021"
date: "2021-09-06T13:46:19+10:00"
abstract: "“Not used for mining”."
year: "2021"
category: Hardware
tag:
- bitcoin
- cryptocurrency
- scams
location: Sydney
---
I just saw [this description](https://www.ebay.com.au/itm/284412032082) on an auction site:

> GEFORCE GTX 1660 VENTUS XS 6G OC. **Not used for mining.**

I've been having a lot of reverse-flashbacks lately. Could you imagine telling someone in the early 2000s that they could buy a GPU with confidence, given the above reason? People would think you were a few triangles short of a pyramid [scheme].
