---
title: "Ideas waiting for their time"
date: "2021-02-01T06:26:11+11:00"
abstract: "Explaining podcasting to its current fans, back in 2004..."
year: "2021"
category: Media
tag:
- podcasting
- rss
- web-feeds
location: Sydney
---
Dave wrote this [last Friday](http://scripting.com/2021/01/29/164743.html?title=ideasMayHaveToWaitForTheirTime "Ideas may have to wait for their time")\:

> If I tried to explain [podcasting] to any of its most avid supporters today, back in 2004, they would have ignored it as the rantings of a crazy software developer. Now there's so much distance in time, and there are many more media things to do with networks, things that we need now.

I'd say this went for RSS too, the infrastructure that made podcasting possible. I use the past tense, because websites publishing and displaying feeds have been on the decline for a while. Podcasts are the last use case that's in direct, widespread use by the public.

We can make it the time for RSS again.

