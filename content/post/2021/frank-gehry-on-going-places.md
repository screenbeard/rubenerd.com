---
title: "Frank Gehry on going places"
date: "2021-09-29T17:02:58+10:00"
abstract: "“If I knew where I was going, I wouldn’t go there.”"
year: "2021"
category: Thoughts
tag:
- architecture
- design
- philosophy
- quotes
location: Sydney
---
A quote from a [Curiosity Stream](https://curiositystream.com/video/1353) documentary:

> If I knew where I was going, I wouldn't go there.
