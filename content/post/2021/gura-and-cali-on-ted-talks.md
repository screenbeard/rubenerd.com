---
title: "Gura and Cali on TED Talks"
date: "2021-03-04T21:42:56+11:00"
abstract: "The mouth sounds those mics pick up..."
thumb: "https://rubenerd.com/files/2021/hololive-ted1@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- gawr-gura
- mori-calliope
- ted-talks
- video
- youtube
location: Sydney
---
With Clara as my witness, I jumped out of my chair when [Gura mentioned](https://youtu.be/l2ENZDBQrJY?t=2801) why she can't watch TED Talks. For all the reasons people have become cynical about them, *this* is reason why I can't either!

<p><img src="https://rubenerd.com/files/2021/hololive-ted1@1x.jpg" srcset="https://rubenerd.com/files/2021/hololive-ted1@1x.jpg 1x, https://rubenerd.com/files/2021/hololive-ted1@2x.jpg 2x" alt="Gura: I can't watch TED Talks. Have you heard? I'm not sure if it's the type of mics. Lavalier or something?" style="width:500px" /><img src="https://rubenerd.com/files/2021/hololive-ted2@1x.jpg" srcset="https://rubenerd.com/files/2021/hololive-ted2@1x.jpg 1x, https://rubenerd.com/files/2021/hololive-ted2@2x.jpg 2x" alt="Cali: Oh yeah." style="width:500px" /><img src="https://rubenerd.com/files/2021/hololive-ted3@1x.jpg" srcset="https://rubenerd.com/files/2021/hololive-ted3@1x.jpg 1x, https://rubenerd.com/files/2021/hololive-ted3@2x.jpg 2x" alt="Gura: The mouth noise those mics pick up... drives me up the wall! I can't watch it!" style="width:500px" /></p>

