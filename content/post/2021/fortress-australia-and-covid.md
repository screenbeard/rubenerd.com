---
title: "Fortress Australia and New Zealand"
date: "2021-03-13T09:46:13+11:00"
abstract: "COVID is all but gone here, but how long can we hold it at bay?"
year: "2021"
category: Thoughts
tag:
- australia
- covid-19
- health
- new-zealand
location: Sydney
---
I remember a recurring theme in dystopian science fiction, perhaps fed from Cold War fears, that most of the planet would be obliterated and either covered with radioactive fallout or disease. In these stories Australia and New Zealand would be spared based on our geographic isolation.

The parallels with what we're dealing with now are palpable. There are pockets in our region doing well with COVID now, including Singapore and Taiwan, but by and large we look on in despair at the thousands of new cases and deaths our friends overseas are enduring.

Australia and NZ have new COVID cases in the single digits at any one time. When we've had community transmission, we contract trace, provide free tests, isolated the affected suburbs, and in some cases lock down our cities. Victoria's spike last year looked horrifying, until you realise the cumulative total is what our friends in other parts of the world are dealing with *daily*.

We've been able to do this with proactive measures, and our state premiers and health departments have filled the policy void left by Australia's populist, irrelevant, and largely MIA prime minister. But there's no doubt that living on large islands has helped. International flights into our countries have all but stopped&mdash;[unless you're Hollywood](https://www.theguardian.com/australia-news/2021/mar/12/hollywood-down-under-stars-flock-from-us-to-film-in-covid-free-australia)&mdash;and repatriated citizens have to spend time in mandatory quarantine. Even a bubble between NZ and Australia has been met with caution whenever a new case is found in Auckland, or Melbourne, or wherever.

Given we've eliminated the disease in the community, our new cases come from this hotel quarantine system. And this is where those thoughts from bad science fiction stories come in. The last small outbreak in Sydney was genetically identified as having come from the US. Even from our selfish perspective, why was that disease allowed to run rampant, mutate further, then come back to us? How long can we hold this at bay?

**This is why events like this need a global response.** This disease will be with us for a *long* time, and left to fester in parts of the world, it will only continue to evolve, mutate, and affect everyone in new ways. Trump, Johnson, and Bolsonaro's despicable, negligent responses, just as three examples, affect us all. It's the same with climate change, plastic pollution, and countless other issues.

In the meantime, we pretend to live life here largely as we did before, albeit with compulsory masks on trains and restaurant QR code checkins. Peak hour trains are full again. Shops are open. Cafés are there for laptop work. We can fly interstate. 

But there's still that niggling worry in the back of our minds. That *fortress* the press likes to mention is more permeable than we'd like to think.

