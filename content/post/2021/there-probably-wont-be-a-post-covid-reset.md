---
title: "There probably won’t be a post-COVID reset"
date: "2021-05-22T10:18:51+10:00"
abstract: "Our experience in Australia and NZ shows society didn’t restructure."
year: "2021"
category: Thoughts
tag:
- covid-19
- news
location: Sydney
---
I've been reading a lot of optimistic press about the idea of a great post-COVID reset. Our lives won't return to what they were in 2019, and our lived experiences during this pandemic will usher in a rethink in work obligations, social interactions, and a healtier relationship with technology.

Kaitlyn Tiffany [perfectly summarised](https://www.theatlantic.com/technology/archive/2021/05/life-returns-normal-great-offlining-may-begin/618936/ "America Offline: We’ve just lived through the most online period in history. What comes next?") the prevailing attitudes for The Atlantic yesterday:

> As vaccination rates tick up, and IRL social life resumes, it’s getting easier to imagine that we’re on the brink of something big: a coordinated withdrawal from swiping and streaming, a new consensus that staying home to watch Netflix is no longer a chill Friday-night plan, but an *affront*.
> 
> [..] It’s possible that the nightmare of being too online lifted only temporarily, while we were trapped at our computers, and that the tech backlash might resume its prior course once life returns to “normal.” But I can’t help feeling as though the pandemic has forced us to confront—and overcome—some of the fears that animated the urge to drown our phones in 2019.

Australia and New Zealand have largely lived without COVID this year. An American friend living in our apartment complex said that society here offers a glimpse into what the rest of the world can expect when we're all vaccinated, and the curves trend downwards.

I can report: there was no reset here. For those of us lucky enough not to work in hospitality, we returned to the same offices with the same commutes and the same expectations as before. We're as wed to our phones and technology. Workaholism is still held in high esteem. We snapped back to our old routines *quickly*. Mid-2021 is mid-2019, albeit without international holidays outside the Australia–NZ travel bubble.

Part of this perhaps stems from the urge and desire to achieve normalicy again, after a period of uncertainty and fear. There's comfort in the familiar.

I get where Kaitlyn and other excited journalists are coming from; I was making predictions like this a year ago too. But don't be at all surprised if the world largely looks the same. That may be a good or a bad thing depending on your view.

