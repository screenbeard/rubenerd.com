---
title: "Getting our first Covid jab"
date: "2021-07-12T12:24:39+10:00"
abstract: "The Roseville Covid Vaccination Centre staff were professional, kind, and patient."
thumb: "https://rubenerd.com/files/2021/jab-jab@1x.jpg"
year: "2021"
category: Thoughts
tag:
- covid-19
- australia
- health
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/jab-jab@1x.jpg" srcset="https://rubenerd.com/files/2021/jab-jab@1x.jpg 1x, https://rubenerd.com/files/2021/jab-jab@2x.jpg 2x" alt="" style="width:500px; height:375px;" /></p>

Clara and I got our first jab of the AstraZeneca vaccine this morning. Everyone at the [Roseville Covid Vaccination Centre](http://www.rosecentre.com.au/northshore-covid-vaccine-centre/) were professional, kind, and patient. It might go down as the least painful needle I've ever had, which speaks to the skill of the nurses!

If you're in the area around the North Shore in Sydney, make an appointment today and fill out the online forms. Even your [Medicare](https://www.servicesaustralia.gov.au/individuals/medicare) card is optional. They've got plenty of vaccines and booking times to go around. You'll be helping yourself, and everyone around you. ♡

*(Update: These are the worst side effects of any vaccine I've ever had, so I've set up my blog to publish from a ready-written stash for the next few days. Was it still worth it? Of course)!*
