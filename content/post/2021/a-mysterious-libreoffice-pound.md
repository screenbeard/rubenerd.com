---
title: "A mysterious LibreOffice Calc £"
date: "2021-04-17T15:33:04+10:00"
abstract: "I downloaded the British locale, and didn’t set the specific Calc currency... whoops!"
year: "2021"
category: Software
tag:
- libreoffice
- troubleshooting
location: Sydney
---
Australia eschewed (gesundheit) the pound for the decimal dollar back in 1966, but LibreOffice Calc on my FreeBSD 13 laptop started putting pounds everywhere in my Australian budget workbook. It took me far too long to figure out why.

I went through my Ansible deployment scripts and realised that I'd changed the LibreOffice install to the [British localisation](https://www.freshports.org/editors/libreoffice-en_GB/) for its dictionary:

    - name: Install package foo
      community.general.pkgng:
        name: en_GB-libreoffice
        state: present

I checked a few of the cells, and the **Number Format** under the **Properties** sidebar showed the "Format as Currency" as "£ Default". Changing this to "AUD $ English (Australia)" put the expected symbol back.

I really should [donate more](https://www.documentfoundation.org/donate/) to the Document Foundation. Their software has done more for my financial security and well-being than anything else.

