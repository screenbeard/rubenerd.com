---
title: "Anxiety tips: stop playing personal FUD"
date: "2021-03-09T14:56:26+11:00"
abstract: "Catch yourself when you feel these, and nip them in the bud."
year: "2021"
category: Thoughts
tag:
- anxiety
- personal
location: Sydney
---
I explained this on The Bird Site recently, but I don't think I did it justice. I'll attempt to explain it better here.

My mind is a narrator. It maintains a running commentary on everything I do and see, regardless of whether I'm sitting in a chair or walking outside. It's like there's another person living within me, planting thoughts in my head. I think I've done this my whole life, though it took meditation to realise just how frequently they appear, and how *vivid* they are.

Some of these thoughts are harmless, and can occasionally be delightful. *Those flowers are nice. That dad giving his daughter a piggy back ride is adorable. Wow this coffee is good, it must be from Costa Rica. It has cherry notes, or is that some other fruit? How do you think it compares...?* On good days its like my mind is a photographer composing a scene, without the camera.

But most of these thoughts, perhaps unsurprisingly, feed anxiety. My mind plays through scenarios about every interaction and experience, usually with a bad outcome. *What if they get my order wrong? What if the conductor can't scan my valid train ticket? What if this lift stops?* These enter a feedback loop that amplify those feelings of personal [FUD](https://en.wikipedia.org/wiki/Fear,_uncertainty,_and_doubt).

Anxiety is complicated; there's never a singular cause. But I've found that identifying when I'm having those unhelpful thoughts already helps. It's even better when I can stop, think about how my mind concocted this entirely artificial idea, and laugh it off. *Oh brain, get better script writers! The least you could have done is involve Godzilla*.

