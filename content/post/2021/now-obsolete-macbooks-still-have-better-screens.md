---
title: "Now obsolete MacBooks still have better screens"
date: "2021-04-02T16:55:55+11:00"
abstract: "Machines with HiDPI in 2021 are just embarrasing."
year: "2021"
category: Hardware
tag:
- apple
- pc-screen-syndrome
- laptops
- mac
location: Sydney
---
Sami Fathi [reported on MacRumors](https://www.macrumors.com/2021/03/31/macbook-pro-13-2012-retina-obsolete/)\:

> Apple today added the late 2012 13-inch MacBook Pro, the first 13-inch MacBook Pro to ship with a Retina display, to its list of obsolete products [..] Apple first introduced the Retina display in its Mac lineup with the 15-inch MacBook Pro released in mid-2012.

This only makes it more absurd that most PC laptops shipped today *still* have worse screens a decade later, either with nasty 1.5× scaling or ancient 1080p panels. It'd be like mid-1990s machines being shipped with CGA or Hercules when the Amiga and SGI machines existed.

HiDPI screens should be mandatory, and not just for accessibility.

