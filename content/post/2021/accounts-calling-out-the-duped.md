---
title: "Accounts calling out the duped"
date: "2021-12-26T08:46:19+11:00"
abstract: "I’m torn on their utilitarian value, and the ethics of doing it."
year: "2021"
category: Internet
tag:
- covid-19
- cryptocurrency
- ethics
- health
- scams
location: Sydney
---
I've noticed a revival of social media profiles and accounts that call people out or publicise their failings at something, and I'm ethically torn.

Two of the biggest right now are Crypto Bros Taking Ls, and the Herman Cain Awards. The former screenshots those duped into crypto-"currencies" and NFTs who have either lost their money, encourage others to do so, or are on the cusp of an epiphany. The latter documents the timeline of anti-vaxxers as they start with shrill memes and angry posts about masks, to their eventual hospitalisation and death.

Even if we (generously) assume a tenth of the stories submitted to these accounts are legitimate and not embellished or completely made up to earn ephemeral Internet points, they paint a grim picture. People are always being duped, but seeing the tip of the proverbial iceberg documented in such stark terms, with real-world consequences, offers a glimpse into a world that rational people like you and I may otherwise never see.

I'm wary of those who see them as a form of entertainment, or take glee in it. The blockchain stuff I'm more forgiving of, given how incessant and exhausting its misguided proponents are. Maybe there's a dose of schadenfreude, or its a form of reassurance for those who've made it out of those cesspools of thought. I find the idea of deluded people losing their lives or money to be sad and a waste of human life, regardless.

But in a utilitarian sense, these accounts might be necessary.

All the information in the world about how pointless crypto-"currencies" are, or how necessary and vital vaccines are, won't stop vast swaths of the population from being duped. The idea that the Internet and ubiquitous information would save people from ignorance has proven to be overly optimistic, if not entirely false. I'm sure it's helped a *lot* of people, such as those in repressive families or in parts of the world where information is strictly controlled. But in other cases, it's only served to reinforce existing views, biases, and opinions. I'm as guilty of falling for this as anyone.

In that environment, maybe we need something more overt. Maybe we do need to regularly post about those who've lost what's important to them. If we can reduce the chance of someone dying, or losing a loved one, or pissing their hard-earned money away on a Ponzi scheme designed to enrich con artists, it might be worth it.

I guess I just wish it wasn't necessary. As 2021 draws to a close, we're still witnessing electronic tulip mania, and people pushing back again on the wonders of modern medicine without any good reason. What is it they say about history not repeating, but rhyming?
