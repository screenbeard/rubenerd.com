---
title: "Docks charging more than just kettles"
date: "2021-08-27T08:23:02+10:00"
abstract: "Just don’t get them clogged with pancake batter."
year: "2021"
category: Hardware
tag:
- cooking
- design
- iot
- memory
- nostalgia
- security
location: Sydney
---
Memory is a weird thing. Those of us who weren't blessed with [ECC DIMMs](https://en.wikipedia.org/wiki/ECC_memory) in our brains have to settle for scattered, incomplete, and poorly-recorded records of what happened in the past. These memories encompass the entire human experience, from what we ate for lunch yesterday to embarrassing moments in high school when you attempted to learn enough Korean to ask your crush out only to panic, inhale too much air, and hiccup instead.

It would come as no surprise that many of my formative memories revolve around gadgets, computers, and hi-fi gear. Some of these even involved using these devices as intended, and not warning tales that generally involved me pulling something apart Humpty Dumpty style (aka: couldn't put it back together again). I'm floored that my parents were as accommodating as they were; I imagine it must have been a never-ending source of frustration to come home and find the cover for the VCR taken off, and the home computer hard drive formatted for the third time that month.

One such memory involved a Philips (or was it a Breville?) kettle my parents brought home when I was in primary school. In the late 1990s kettles were being introduced with circular heating docks that negated the need for an attached cable. The kettle would make electrical contact with a circular peg protruding from the dock (heavens) to boil water, then could be lifted off the pad and carried around wirelessly. Wireless in the sense that it physically had no wires; it'd be two decades before [IoT schemes](https://twitter.com/internetofshit "Internet of Things") would exist to add insecure kitchen appliances to botnets.

This was an important development. Good electric kettles had featured detachable safety cables for decades, but they were still prone to being mishandled, resulting in scalding water being splashed onto undesirable surfaces. Kettles still terrify me despite my daily use of them, though at least I can take comfort from knowing I won't be accidentally still tethered to a wall when I yank that handle (heavens).

The kettle came with a thick instruction book and pack of promotional material, both of which my parents stashed in various kitchen knick knack drawers for years. The cover said something along the lines of "the first device to use our new universal dock" and "we anticipate future devices to work with it". Glossy photos depicted a kitchen counter with a kettle dock embedded in its surface.

The reason I remember this was due to my bewilderment at such a proposal. The dock was circular to match the shape of the kettle, and presumably so it didn't have to feature a fixed orientation. What other kitchen devices could feasibly be operated from this? All I could think of were spice or coffee grinders, the latter of which you'd likely be using *while you were boiling the kettle*.

I can see the appeal of having a dock of some sort for lesser-used appliances like waffle irons, sandwich presses, and jaffle makers that usually reside in a cupboard and require you to unravel their cords and plug them in *like a schmuck*. But the shape of the dock didn't seem ideal for these, and you'd still be limited to using one device at a time. I could also see the protruding dock connector getting gunked up with spilled pancake batter; an especially-tragic outcome for those with the aforementioned devices embedded into counters. It'd be a fun exercise to learn about the electrical conductivity of various breakfast foodstuffs though; I *have* been told I need to eat more iron.

But the biggest issue would be one we're currently facing with phones and electric cars: you'd be stuck buying devices from one manufacturer, unless a universal standard were adopted. And what if the standard changed, and you were stuck with a deprecated device embedded in your counter, silently judging you while you boiled your water elsewhere? *Like a schmuck*.

It's interesting that companies didn't take this concept and run with it (that I know of). These docks still exist today, but are still only powering kettles. We've got induction and wireless charging now, maybe there are a family of kitchen appliances waiting to make my life easier with these. *Hard pass* if they want to join my home network though.
