---
title: "Simon Whistler on road trips"
date: "2021-06-24T08:12:15+10:00"
abstract: "With the wind blowing through our... hair."
thumb: "https://rubenerd.com/files/2021/megaprojects-bigdig-02@1x.jpg"
year: "2021"
category: Media
tag:
- megaprojects
- videos
- youtube
location: Sydney
---
From his [Megaprojects episode on the Boston Big Dig](https://www.youtube.com/watch?v=Kq1MFTAPeIY):

<p><img src="https://rubenerd.com/files/2021/megaprojects-bigdig-01@1x.jpg" srcset="https://rubenerd.com/files/2021/megaprojects-bigdig-01@1x.jpg 1x, https://rubenerd.com/files/2021/megaprojects-bigdig-01@2x.jpg 2x" alt="In a top-down convertible, with the wind blowing through your... through your hair." style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/megaprojects-bigdig-02@1x.jpg" srcset="https://rubenerd.com/files/2021/megaprojects-bigdig-02@1x.jpg 1x, https://rubenerd.com/files/2021/megaprojects-bigdig-02@2x.jpg 2x" alt="(blank stare)" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/megaprojects-bigdig-03@1x.jpg" srcset="https://rubenerd.com/files/2021/megaprojects-bigdig-03@1x.jpg 1x, https://rubenerd.com/files/2021/megaprojects-bigdig-03@2x.jpg 2x" alt="Thanks a lot." style="width:500px; height:281px;" /></p>

