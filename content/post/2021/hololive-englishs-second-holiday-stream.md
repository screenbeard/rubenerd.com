---
title: "Hololive English’s second holiday stream"
date: "2021-12-24T12:54:34+11:00"
abstract: "So much fun :')"
thumb: "https://rubenerd.com/files/2021/hololive-holoholiday@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- ninomai-inanis
- youtube
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/hololive-holoholiday@1x.jpg" srcset="https://rubenerd.com/files/2021/hololive-holoholiday@1x.jpg 1x, https://rubenerd.com/files/2021/hololive-holoholiday@2x.jpg 2x" alt="Screenshot of all the girls on the stream." style="width:500px; height:282px;" /></p>

[This VR stream was so much fun](https://www.youtube.com/watch?v=2iXo0YDhoCA). Myth have grown so much in the last year, and IRyS and Council have been such a great fit.

Happy holidays!
