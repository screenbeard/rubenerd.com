---
title: "Palm LifeDrive in a Windows 2000 QEMU VM"
date: "2021-05-01T10:44:31+10:00"
abstract: "It’s surprisingly simple once you get USB passthrough working, save for one hiccup with an old DLL."
thumb: "https://rubenerd.com/files/2021/palm-lifedrive-manager@1x.png"
year: "2021"
category: Hardware
tag:
- guides
- nostalgia
- palm
- palm-lifedrive
- qemu
- windows-2000
location: Sydney
---
Today I wanted to run the Palm Desktop to HotSync my Palm LifeDrive PDA, as well as run the LifeDrive Manager to transfer media. I ran down a few options before deciding on a Windows 2000 VM in a QEMU VM. Windows 2000 is my go-to for legacy Windows stuff in VMs, given how badly Microsoft went off the design reservation from XP onwards. QEMU also works perfectly for this use case.


### Installing Windows 2000

The first step is to create a boot volume for the OS:

    $ qemu-img create -o qcow2 Windows2000.qcow2 32G

Then installing Windows 2000 the VM. Note that `cd` means boot from drive C first, then drive D which is the optical drive. `cirrus` graphics will also give you the nicest experience out of the box:

    $ qemu-system-i386                           \
        -m 256                                   \
        -name "Windows 2000",process=qemu-win2k  \
        -hda Windows2000.qcow2                   \
        -boot cd                                 \
        -cdrom Windows2000-Installer.iso         \
        -vga cirrus                              \
        -net nic,model=rtl8139                   \
        -net user                                \
        -usb                                     \
        -device sb16                             \
        -writeconfig Windows2000.qemu.conf

Qemu has long had a `-win2k-hack` option to prevent a drive full error when installing Windows 2000, but I haven't encountered a need for it.

Once installed, grab yourself copies of [Service Pack 4](https://archive.org/details/msdn-green-2005-12-2427-3) and the [Update Rollup 1](https://www.catalog.update.microsoft.com/Search.aspx?q=KB891861) if they weren't slipstreamed into your installer.

<p><img src="https://rubenerd.com/files/2021/qemu-palm-install@1x.png" srcset="https://rubenerd.com/files/2021/qemu-palm-install@1x.png 1x, https://rubenerd.com/files/2021/qemu-palm-install@2x.png 2x" alt="Screenshot showing the About dialog box for Solitaire, running in a Windows 2000 QEMU VM. Thanks, Wes Cherry!" style="width:500px; height:333px;" /></p>


### USB Passthrough

I plugged the USB HotSync cable into my FreeBSD machine and the Palm LifeDrive and saw the following details from `dmesg(8)`:

    # dmesg
    ==> ugen1.2: <palmOne, Inc. palmOne Handheld> at usbus1
    ==> uvisor0 on uhub0
    ==> uvisor0: <Serial Interface> on usbus1    

I took that address and put it into `usbconfig(8)`:

    # usbconfig -d ugen1.2 dump_device_desc
    ==> ugen1.2: <palmOne, Inc. palmOne Handheld> at usbus1, cfg=0 md=HOST spd=HIGH (480Mbps) pwr=ON (100mA)
    ==> bLength = 0x0012 
    ==> bDescriptorType = 0x0001 
    ==> bcdUSB = 0x0200 
    ==> bDeviceClass = 0x0000  <Probed by interface class>
    ==> bDeviceSubClass = 0x0000 
    ==> bDeviceProtocol = 0x0000 
    ==> bMaxPacketSize0 = 0x0040 
    ==> idVendor = 0x0830 
    ==> idProduct = 0x0061 
    ==> bcdDevice = 0x0100 
    ==> iManufacturer = 0x0001  <palmOne, Inc.>
    ==> iProduct = 0x0002  <palmOne Handheld>
    ==> iSerialNumber = 0x0005  <PVG0C5V5V1VD>
    ==> bNumConfigurations = 0x0001 

This gave me the Product ID and Vendor ID that I needed to pass to QEMU:

    -device usb-host,vendorid=0x0830,productid=0x0061 \

When I booted the VM, I was greeted with the **Found New Hardware** wizard which asked me about my my **palmOne Handheld**. *Yay, it worked!* I dismissed this dialog, because I wanted to use the Palm software to install the drivers and software.

<p><img src="https://rubenerd.com/files/2021/qemu-palm-found@1x.png" srcset="https://rubenerd.com/files/2021/qemu-palm-found@1x.png 1x, https://rubenerd.com/files/2021/qemu-palm-found@2x.png 2x" alt="" style="width:500px; height:333px;" /></p>


### Installing Palm Desktop

The next step was to install the associated Palm software. I created an ISO disc image of the CD that came with my LifeDrive from eBay, and [uploaded to Archive.org](https://archive.org/details/palmone-lifedrive-software-for-windows "palmOne LifeDrive Software for Windows") if you need a copy.

Installation was straight forward, and I soon had my familiar Palm Desktop and HotSync system tray icon. No PIM software or smartphone since matches the simple elegance of these tools!

<p><img src="https://rubenerd.com/files/2021/qemu-palm-desktop@1x.png" srcset="https://rubenerd.com/files/2021/qemu-palm-desktop@1x.png 1x, https://rubenerd.com/files/2021/qemu-palm-desktop@2x.png 2x" alt="Screenshot of the Palm Desktop, with icons for the LifeDrive Manager, Palm Desktop, and palmOne Quick Install" style="width:500px; height:333px;" /></p>


### HotSyncing

Now I could HotSync this device! I booted the Windows 2000 VM, pressed the HotSync button on the connector to the LifeDrive, and I was transported back to my Palm Tungsten W smartphone days. I was also able to launch the LifeDrive Manager software which I'd never used before! Now I can copy what I want over to the device, from the comfort of my FreeBSD Panasonic laptop, or my MacBook Pro.

<p><img src="https://rubenerd.com/files/2021/palm-lifedrive-manager@1x.png" srcset="https://rubenerd.com/files/2021/palm-lifedrive-manager@1x.png 1x, https://rubenerd.com/files/2021/palm-lifedrive-manager@2x.png 2x" alt="Screenshot showing the LifeDrive Manager." style="width:500px; height:333px;" /></p>


### Follow-up

My next experiment will be to see if this Palm software works in 32-bit Wine. I'm not sure how that works with USB, but it'd be worth a try.

