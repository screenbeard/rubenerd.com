---
title: "The paralysis of decisions"
date: "2021-09-08T11:21:47+10:00"
abstract: "If I could decide how to proceed..."
year: "2021"
category: Thoughts
tag:
- decluttering
- psychology
location: Sydney
---
I've talked a *lot* about my quest to rid my life of "stuff". I've tied much of my anxiety to physical clutter, having grown up in an environment in which it was a fixture. Paring back to useful or meaningful stuff has also give me more a sense of control over my environment and life, which is welcome during These Times.&trade;

Ironically, it's another hang up of mine that has successfully helped me stave off buying too much more. I get into this mental cycle of research and confusion about what to buy, then I'm hit with buyer's remorse upon committing. Did I end up with the best option? Was it necessary? This means the next time I'm even less likely to commit to something, even if it's important or necessary.

I don't envy impulse shoppers; the financial and mental impact could be ruinous. At worst I end up with a neurotic fixation on my perception of stuff, boo hoo! But surely there's a happy medium.

Haven't I written this same post a few times now? Probably.
