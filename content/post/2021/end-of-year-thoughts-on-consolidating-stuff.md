---
title: "End of year thoughts on consolidating stuff"
date: "2021-12-31T10:11:11+11:00"
abstract: "Homelabs, kitchens, storage pools, web accounts, and consoles!"
thumb: "https://rubenerd.com/files/2021/small-things@1x.jpg"
year: "2021"
category: Hardware
tag:
- cooking
- decluttering
- ipad-mini
- laptops
- nintendo-switch
location: Sydney
---
I've been on a multi-year quest to reduce the amount of *stuff* that I have, in real life and electronically. I like being tidy, and clutter is a source of anxiety.

One way I thought I'd achieve this is through consolidation of multiple things into one. Yes Ruben, that's the definition of consolidation! Or in the words of culinary philosopher Alton Brown, do we need all these unitaskers? I've had some positive results this year, but I deviated in ways I didn't expect.

Building a new homelab server was one of the best things I could have done for myself. I was going headfirst into a proper homelab with racks, switches, and cabling a few years ago, but in the end I decided to replace multiple boxes and NAS devices into one hulking tower. bhyve and jails on FreeBSD makes virtualising stuff so simple. It's cleaner, quieter, takes up much less space, and is less physical stuff to maintain.

In the kitchen we've been using an air fryer for so much, but we also splurged and got an electric kettle. *Shocking!* It is a bit, because our apartment is so tiny and our countertop space so precious, but working from home meant Clara and I were boiling water for tea and coffee often enough that it was worth it. Going from minutes to seconds to boil water has been a boon.

<p><img src="https://rubenerd.com/files/2021/small-things@1x.jpg" srcset="https://rubenerd.com/files/2021/small-things@1x.jpg 1x, https://rubenerd.com/files/2021/small-things@2x.jpg 2x" alt="Photo of my Panasonic laptop, iPad Mini 6, and Nintendo Switch Lite." style="width:500px; height:375px;" /></p>

Portable electronics were probably the most interesting takeaway this year for me. I intended to replace everything with one laptop and a larger phone, but I ended up hating the compromises. Maybe smaller devices have narrower tolerances. I'm now back to a smaller phone, a large work laptop, a tiny "on-call" writing laptop, a tablet just for reading newspapers, books, and manga, and a game console. I cheat and consider this a win, given it all still takes up a fraction of the space that my aforementioned, decommissioned homelab did. Those were some big words.

I also finally got around to retiring an old XFS array I may or may not have forgotten about... what is it about the cobbler's son walking barefoot? Now *all* Clara's and my data sits in redundant, backed up, regularly scrubbed OpenZFS pools on FreeBSD 13. The array was so old and fragmented, the performance improvement alone could have been worth it, let alone having peace of mind.

Online, is a word with one word. I've been ruthlessly closing accounts and preparing their content and links to import. I already did this with Tumblr, but Instapaper and a few others are still works-in-progress. I'm learning to be more circumspect about these sorts of services, because you never know when they'll arbitrarily change their rules, move you somewhere else, or close. At least if I self host everything in one place, I only have myself to blame when it goes down.

The last thing I'd wanted to figure out was our Hi-Fi system. I'd wanted to consolidate playing CDs, VCDs, and Laserdiscs in the one unit, but unfortunately I suspect our AC-3 equipped Kenwood has a shorted power supply. Hi-Fi repair people haven't wanted to touch it with a ten-foot pole on account of it being unusual. Anyone in Sydney or surrounds know of a repair place for old stuff rational people wouldn't use?
