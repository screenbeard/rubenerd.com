---
title: "David Gray, White Ladder LP"
date: "2021-05-17T15:10:00+10:00"
abstract: "We got our limited edition white vinyl LP of this classic album!"
thumb: "https://rubenerd.com/files/2021/white-ladder-lp@1x.jpg"
year: "2021"
category: Thoughts
tag:
- david-gray
- music
- music-monday
- nostalgia
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) gets physical. Clara and I just received our twentieth anniversary, limited-release white LP of David Gray's *White Ladder*. It looks gorgeous spinning in our track selecting, quartz locking, direct drive, linear tracking turntable atop my robot.

*(I have a longer post pending about Mr Robot, but my dad built him for me as a chest of drawers before I started primary school. Last year I removed the drawers and now use him for Hi-Fi gear. It's almost as though he knew in advance, given he's just wide enough for a standard RU of equipment)!*

<p><img src="https://rubenerd.com/files/2021/white-ladder-lp@1x.jpg" srcset="https://rubenerd.com/files/2021/white-ladder-lp@1x.jpg 1x, https://rubenerd.com/files/2021/white-ladder-lp@2x.jpg 2x" alt="Photo showing the White Ladder LP playing in the aforementioned turntable, with the sleeve on the left." style="width:500px; height:333px;" /></p>

*White Ladder* remains one of my all-time favourite albums. It became a breakout hit in the early 2000s, but I always thought it had more of a mid-to-late 1990s vibe for reasons I can't articulate. I since learned from its Wikipedia article that it was indeed recorded during that time, which I was happier to hear than I perhaps should have been!

The album is such a nostalgic snapshot into my childhood, both from radio play at the time, and its later inclusion in my iTunes chill playlists. There was something so unique about his instrumentation, which I think wasn't appreciated as much as his distinct voice at the time. Pianos are such a perfect fit for pop music, it's a shame it isn't used more often.

*Babylon* was probably his most well-known track, but my favourite is still *[Please Forgive Me](https://www.youtube.com/watch?v=VsDMjet0fyo)*.

