---
title: "Feedback on repurposed eBay packaging"
date: "2021-07-07T08:30:56+10:00"
abstract: "From Asherah Connor, Bradley M, and Rebecca Hales."
year: "2021"
category: Hardware
tag:
- ebay
- feedback
- shopping
location: Sydney
---
On Monday I wrote about [unscrupulous eBay sellers](https://rubenerd.com/dodgy-listings-using-repurposed-packaging/) who repurpose packaging to sell unrelated items, because they're unscrupulous sellers who are unscrupulous. I said it was false advertising and raised accessibility concerns over the *caveat emptor* defence.

[Asherah Connor](https://kivikakk.ee/)\:

> *"Much as people say about my physical appearance, the problem isn’t just cosmetic."*
>
> Thank you for the laugh.  I am having a hard morning and reading that first thing really helped. 

I live to serve ^^; like a Gemini server running on a Zigbee box!

Ivalderrama:

> One week later, I have already installed Trojan virus to Operating Systems of all the devices that you use to access your email.
In fact, it was not really hard at all (since you were following the links from your inbox emails). All ingenious is simple. =)

Wait, that's spam. I'm tempted to make that last line the slogan for this site though. *Hey suckers, welcome to Rubenerd.com! All ingenious is simple!*

Bradley M:

> You can always return them.

True, but that's beside the point. Like the pen I snapped the tip off  yesterday by rolling over it with my chair.

Rebecca Hales:

> Bingo! But you didn't mention the main problem: it erodes the trust in legitimate sellers as well which hurts everyone.

I hadn't considered this. How many people have been scared off buying anything in the future because they were burned by another seller? Unless that's your thing, in which case, carry on.

