---
title: "First coffee shop in four months"
date: "2021-10-16T08:45:09+10:00"
abstract: "The sun and breeze were glorious. ☕️"
year: "2021"
category: Thoughts
tag:
- coffee-shops
- covid-19
- personal
location: Sydney
---
I'm *sitting* at a coffee shop again for the first time in what feels like an age. I'm at their outside tables with the sun on my face, and the nippy morning breeze ruffling my uncut hair! Talk about having a renewed appreciation for what you once took for granted.

The only sign of our times was the barista asking for my Covid certificate. She told me all her customers thus far have been great about it, but I feel for retail staff who may encounter a horse tablet snowflake.

Lockdowns were eased in New South Wales a week ago, but Clara and I decided to be cautious and see how things played out. Neither of us are heading to our respective offices for another few months, and we're avoiding large gatherings. It might be overkill given the vast majority of the population are now vaccinated, but let's see.

