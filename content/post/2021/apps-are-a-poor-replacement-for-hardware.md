---
title: "Apps are a poor replacement for hardware"
date: "2021-09-17T08:16:15+10:00"
abstract: "Tying the lifetime of a hardware device to an unmaintained phone app is quite the trick."
year: "2021"
category: Hardware
tag:
- design
- ergonomics
location: Sydney
---
Speaking of Wouter Groeneveld, he wrote of [his adventures with a portable photo printer](https://brainbaking.com/post/2021/09/hp-sprocket-mini-printer/) earlier this month. It went as well as you probably expect:

> I don’t get that reviews from PC Mag and others focus on the printing quality (the paper is bad, but I don’t care, and I knew that), and not on the quality and accessibility of the obligatory software that comes with it. Not a single negative word is mentioned about it: are we somehow getting used to shitty apps? Other bloggers that use it for scrapbooking seem to be generally content with the printer.
> 
> What really kills it though, is that one word. Smartphone. I cannot use my MacBook to send a picture to the mini printer. I have to use the app. This is simply ridiculous.

This is a plague. Designers are offloading their appliance UI to a smartphone app, *en masse*. And as Wouter points out, you know they're not using open protocols.

I *used* to get the business case here, but I don't think it holds. Embedded systems can be bought for cents on the dollar, which would be a tiny fraction of the overall cost for a complicated device like a printer or a kitchen appliance. Then there's the time consuming cross-platform development one would need to do to build and test a tolerable mobile application across a variety of handsets (assuming they're doing that, or we have another problem), to say nothing about keeping it maintained and current with platform updates.

*(I set the bar at "tolerable" here, because we all know these apps are rubbish. And we all know they won't be maintained, or will become abandonware in a few years. Tying the lifetime of a device to an app is quite the trick; pity about all the ewaste).*

Clara and I had the same experience recently with a home laser printer. Rather than implement standard printer drivers like we've all had for decades, it only works when paired with a phone's Bluetooth, or connected to your home Wi-Fi network. *Why?* Printers are the butt of IT jokes for their perceived flakiness, and somehow they managed to make it worse! I (almost) couldn't believe it.

I'm also not surprised by the silence from reviewers. My bugbear I'm sure you're all sick to death reading about is my [bafflement](https://rubenerd.com/tag/pc-screen-syndrome/) about PC monitors and laptops. No reviewer, **ever**, talks about HiDPI. It's as though they're all stuck in 2006, when widescreens were the new shiny. You'll read about every attribute about a monitor, but when it comes to how many pixels it has... oh yeah, it's "Full HD" or something something, move on. Is it because they all know PC monitors are rubbish compared to Macs, or are they simply not doing their jobs?

I think it's time to buy a new coffee grinder, or stand mixer to channel my frustration into baking instead. They don't need a smartphone app to turn on, do they?
