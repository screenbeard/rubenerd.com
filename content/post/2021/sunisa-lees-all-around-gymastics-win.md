---
title: "Sunisa Lee’s All-Around Gymnastics win"
date: "2021-07-31T09:31:16+10:00"
abstract: "No, she didn’t “steal” it from Ms Biles who stepped out for mental health reasons. Internet people are classy!"
thumb: "https://rubenerd.com/files/2021/yt-KEbo7RGqb2k@1x.jpg"
year: "2021"
category: Thoughts
tag:
- gymnastics
- health
- mental-health
- psychology
- olympics
- tokyo-2020
location: Sydney
---
I saw my American and Southeast Asian friends explode with joy over Sunisa Lee's gold medal win in All-Around Gymnastics at the Tokyo Olympics yesterday. I haven't caught up with Gymnastics yet (I used my precious spare time last week for Table Tennis), but that's what the weekend is for :).

Alongside this personal achievement, she's also the first Hmong-American Olympic gymnast in the States, and either the first or among the first medallists of Lao decent. [Seeing the wholesome joy](https://www.youtube.com/watch?v=KEbo7RGqb2k) of her family, friends, and supporters made my morning.

*(I've been to Cambodia and northern Thailand when I lived in Singapore, but Laos is on the proverbial bucket list. Vietnam too. But I digress)!*

<p><a href="https://www.youtube.com/watch?v=KEbo7RGqb2k" title="Play Suni Lee's family and Hmong community celebrate her gold medal gymnastics win at Tokyo 2020 Olympics"><img src="https://rubenerd.com/files/2021/yt-KEbo7RGqb2k@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-KEbo7RGqb2k@1x.jpg 1x, https://rubenerd.com/files/2021/yt-KEbo7RGqb2k@2x.jpg 2x" alt="Play Suni Lee's family and Hmong community celebrate her gold medal gymnastics win at Tokyo 2020 Olympics" style="width:500px;height:281px;" /></a></p>

In light of recent news though, it didn't take long for social media to have the hottest of takes. The most prevalent was the assertion that she somehow "stole" it from Simone Biles, who bowed out to take care of her mental health. Others euphemistically said she "stepped in" or "replaced" her, presumably in an attempt to not sound so callous. It's the barest of distinctions.

I'll admit, after [writing about mental health](https://rubenerd.com/mental-health-at-the-olympics/) last Thursday and how much I respected Ms Biles for what she did, I anticipated any subsequent American winner would be tarred with this accusation, or worse from someone overseas. I say "tarred" because the slimy goop comes from the same primordial pool that her critics likely climbed out of. Those pools Ms Lee could easily do a double backflip over without a second thought.

This was a great achievement. Anyone who claims otherwise is, to reintroduce a descriptor I used throughout high school, a *sackbutt*.

