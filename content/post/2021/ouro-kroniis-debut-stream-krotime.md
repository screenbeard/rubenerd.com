---
title: "Ouro Kronii’s debut stream #krotime"
date: "2021-08-28T08:35:00+10:00"
abstract: "I think she may have had the best stage presence of any debut stream we’ve ever seen... wow!"
thumb: "https://rubenerd.com/files/2021/yt-W3oVrq94mGM@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- ouro-kronii
- video
- youtube
location: Sydney
---
Okay, back to these Hololive English Gen 2 debuts! I'm cheating a bit here, because I watched these last two with Clara earlier in the week. Next up was the HoloCouncil’s Warden of Time, Ouro Kronii.	

<p><a href="https://www.youtube.com/watch?v=W3oVrq94mGM" title="Play 【DEBUT STREAM】The Time Has Come⌛ #hololiveEnglish #holoCouncil"><img src="https://rubenerd.com/files/2021/yt-W3oVrq94mGM@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-W3oVrq94mGM@1x.jpg 1x, https://rubenerd.com/files/2021/yt-W3oVrq94mGM@2x.jpg 2x" alt="Play 【DEBUT STREAM】The Time Has Come⌛ #hololiveEnglish #holoCouncil" style="width:500px;height:281px;" /></a></p>

There's a bit of personal context I have to explain here; fortunately it's the weekend now and I have all the *time* in the world. Can I already digress here and say that her gap moe and puns were the cherry on top of one of the best debut streams either of us had ever seen.

Clara and I were already *Kroniis* on account of her artist, as I'm sure many in the fandom were. WADARCO's character designs for anime and games are the stuff of legend. I've written for years about [my love for *Umu!*](https://rubenerd.com/fate-grand-order-nero-fest-2018/), and [Bride *Umu!*](https://rubenerd.com/a-second-chance-for-fgo-bride-umu/), and all [her other Fate art](https://rubenerd.com/wadarcos-art-in-chaldea-ace-2/) among much, much more. Few artists match the culture (cough!) and expressive detail with which she imbues her characters. It was such a treat seeing her art animated in live 2D in the hands of such a capable and brilliant new actor.

*(Also, we've heard of mismatched stockings, but an asymmetrical skirt? With pinstripes!? Clara may have her work cut out for her when she updates Rubi this year).*

The second bit of context concerns Kes, my favourite *Star Trek Voyager* character on account of... wait for it... *her deep, calming voice.* I used to joke that if I won the lottery, I'd hire her to read *Lord of the Rings* or *War and Peace* to me. It's also one of the first things that first attracted me to Clara as well (naw).

In a similar vein, we had been told to anticipate Kronii's voice acting skills, but I still don't think any of us were prepared. She was a *professional* who set up this stoic, aloof demeanour only to let it crack and melt as she sparred with chat and discussed more of her background. That skill and talent paired with WADA's art and her self-effacing helicopter humour lead her stream in directions none of us expected. I haven't had stratospheric expectations exceeded that thoroughly since Clara and I first went to Japan.

I'm calling it, I think Kronii has the best stage presence of any vTuber I've ever watched, and this was on her *debut!* Her character claims to not be a happy person who aims to be one someday, but she's already done that for her new fans. Mad respect.

*(Here's hoping one day she can do a Korean stream with Ina too, that'd be amazing. I'm already picturing them sharing a barbeque and talking about Korean culture in the West 🇰🇷).*
