---
title: "Last night’s sunset"
date: "2021-09-11T09:58:21+10:00"
abstract: "🌄"
thumb: "https://rubenerd.com/files/2021/photo-sunset@1x.jpg"
year: "2021"
category: Travel
tag:
- colour
- editors
- midnight-commander
- sky
- sunset
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/photo-sunset@1x.jpg" srcset="https://rubenerd.com/files/2021/photo-sunset@1x.jpg 1x, https://rubenerd.com/files/2021/photo-sunset@2x.jpg 2x" alt="A deep pink, yellow, and red sunset over a suburban street in Chatswood, in the north of Sydney, in Australia, on Earth." style="width:500px; height:375px;" /></p>

This post was also written in the Midnight Commander's editor `mcedit`. I keep forgetting that exists. I adore the DOS-style dropdown menus. Not as much as a pretty sunset, though.

