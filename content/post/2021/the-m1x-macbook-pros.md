---
title: "The M1X MacBook Pros"
date: "2021-10-20T08:11:52+10:00"
abstract: "Some great steps, but I’ll bet it still has soldered storage."
year: "2021"
category: Hardware
tag:
- apple
- macbook
- macbook-pro
location: Sydney
---
The new MacBook Pros have done away with the touchbar, and have reintroduced HDMI and SDXC slots. This is **fantastic** news. Ignore the pundits mocking Apple for doing this; they'd have mocked Apple if they'd still only included USB-C in the next version.

The soldered storage and ridiculous screen notch, less so. These machines are designed to be disposable appliances that aren't user servicable, and it's hard for me to be excited for that thesedays. I also continue to use the Intel CPU in my current MacBook Pro for work stuff, and am not looking forward to the day when I can't.

**CONJECTURE ZONE!** I also can't shake this deeply unsettling feeling about the security of those M1X CPUs. A lot of disparate, integrated components is only asking for trouble; just look at how chips with far less complexity have held up. Here's hoping that's just my linerging anxiety.

Is there one of these machines in my future? For work, probably. But my current 16-inch is fine, just as my 2017 iPhone 8 is. It won't be anywhere near as performant, but I want a [Framework](https://frame.work/) as my next personal machine, if and when they start shipping down here. They're also not perfect, but at least someone's giving it a shot.
