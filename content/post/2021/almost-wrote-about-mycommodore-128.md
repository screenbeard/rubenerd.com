---
title: "Almost wrote about my Commodore 128"
date: "2021-02-18T13:50:08+11:00"
abstract: "Feeling inadequate about talking about this era of computers, and getting over it!"
thumb: "https://rubenerd.com/files/2021/commodore128@1x.jpg"
year: "2021"
category: Hardware
tag:
- bil-hurd
- commodore
- commodore-128
- commodore-128-series
location: Sydney
---
This post about my favourite 8-bit computer has been three years in the making, but I kept putting off over fears I can best describe as inadequacy. It's strange and difficult to describe.

There are *so many* video creators, podcasters, and bloggers who have done incredible work at assembling information and presenting fascinating takes on 8-bit computers. Combine that with my own long journey to learn about this machine, and the feeling of guilt that I hadn't done [The Geekorium's](https://www.the.geekorium.com.au/) generosity justice by blogging sooner, and I ended up not posting about it at all. It's like self-doubt-informed procrastination, which I suppose are usually interrelated.

It's *incredibly* silly in retrospect. My blog has always been about the process, and thinking out loud. The steps involved in cleaning, upgrading, refurbishing, kitting out, programming, and learning about this new (to me) machine could have been an entire blog post series, but instead I kept plodding along, amassing thoughts and ideas for a big reveal blog post that ended up being too ambitious to be practical. Or became too big to write, in other words. I've only stumbled like this a few times in the history of my personal projects and writing, but when it happens it hits *hard*. So much so that I lose the ability to write cohesive metaphors.

<p><img src="https://rubenerd.com/files/2021/commodore128@1x.jpg" srcset="https://rubenerd.com/files/2021/commodore128@1x.jpg 1x, https://rubenerd.com/files/2021/commodore128@2x.jpg 2x" alt="" style="width:500px" /></p>

*Photo by [Evan-Amos](https://commons.wikimedia.org/w/index.php?curid=17835372)*.

But there's another reason why this feeling of defeat is so silly. This machine has been *so much fun!* Researching the intricacies of VDC memory for 80-column output, comparing the performance of 1541 and 1571 disk drives, how to load applications from cassette, and the process of physical restoration have been a source of deep fascination and joy, and a welcome distraction during *These Times&trade;*.

Over the coming months I'm going to pick up and write what I should have done from the start, and *write about this fun old computer*. It's unique dual-CPU, quad-OS architecture, surprisingly contemporary design, and the interesting moment in history it occupied between the 64 and the Amiga. I'll write about the hardware upgrades I've given it, how I've transferred current software to it and added it to my home network, what it's meant for a guy who grew up in the 16bit DOS era, how it relates to the Commodore Plus/4 and 16 that I also have, how I built an IKEA desk around it, and potentially more.

Isn't it weird that we convince ourselves with these concocted narratives, when we should have just written what was on our mind from the start?

