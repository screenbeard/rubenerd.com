---
title: "Robert F. Kennedy quoting George Bernard Shaw"
date: "2021-12-05T16:38:29+10:00"
abstract: "Some men see things as they are, and ask “why?”. I dream things that never were, and ask “why not?”"
year: "2021"
category: Thoughts
tag:
- quotes
location: Sydney
---
> Some men see things as they are, and ask "why?". I dream things that never were, and ask "why not?"

