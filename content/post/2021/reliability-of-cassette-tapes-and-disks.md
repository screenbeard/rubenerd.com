---
title: "Reliability of cassette tapes and disks"
date: "2021-03-11T15:21:15+11:00"
abstract: "They’re better than we remember."
year: "2021"
category: Hardware
tag:
- cassettes
- commodore
- music
- techmoan
- the-8-bit-guy
location: Sydney
---
Talk about cassettes and floppy disks today, and you'll get people saying they were everything from unreliable to just plain junk. Casettes would get caught in players and unspool, necessitating a pencil to wind it back or worse. Floppy disks would encounter bad sectors, make unsettling noises, and die.

I long assumed this was because people weren't thinking about the devices in historical context. Why would I use tapes today when I can throw artists I care about a pittance on a streaming platform, or store data on floppies when I can have syncing issues with a remote server? I kid, but while modern hard drives, SSDs, and cloud storage come with their own problems, you *could* argue that they're more reliable, at least per gigabyte. But it's a different story when compared to the alternatives at the time, if they even existed.

I often wonder whether I even exist. Wait, what?

People's most recent exposure to these formats were also in their twilight years, when the drives and media had been in use for decades or more. A worn tape or badly maintained disk drive will never be as reliable as a new one, assuming they were even still in production. And as Michael Dexter one quipped in the context of file systems, all it takes is one data loss event to lose trust forever.

Except, neither of these tell the whole story. Tapes and disks seemed less reliable immediately before going out of production *because they were*. Inevitable cost-cutting and loss of their former premium status meant there was little motivation for manufacturers to build them at the same standard and quality they did before. The conspiratorial sectors of my brain almost like to think this was intentional to sell people on the reliability of new formats, though it's not necessary here to attribute business reality to malice. People weren't buying this media anymore in the face of devices with higher storage capacities and speed.

The 8-bit Guy discussed this on his [video about floppy disks](https://www.youtube.com/watch?v=EHRc-QMoUE4)\:

> Those drives and disks were actually extremely reliable back in the 1970s and 1980s. Of course, they also used to be expensive. In the 1990s they started making them cheaper and cheaper and reliability suffered. And by the 2000s when the last batches were being produced, they were total junk.
> 
> I think that because floppy drives and the disks had become so unreliable towards the end of the era, I think a lot of people remember them that way, even though they were actually pretty darn reliable back in their heyday. In fact I have plenty of disks that are more than 35 years old and they still work just fine.

[Matt from Techmoan](https://www.youtube.com/watch?v=jVoSQP2yUYA) also discussed cassettes in their historical context:

> No, [tapes weren't terrible]. In fact, they could be pretty good!
> 
> The reason most people bought cassettes as opposed to CDs was because they were a heck of a lot cheaper. [..] But however cheap something is, people won't go back and buy it again unless they're happy with it. Obviously they *were* happy with cassettes because it was the best selling format for the best part of ten years.
>
> However, because most of the people buying cassettes were on a restricted budget, they were also listening to them on budget equipment, and therefore never hearing the best of what you could get out of a cassette.

We all have horror stories about drive and disk failures. Ironically a failure now can feel so much worse given how much denser they are, even if statistically they're more reliable.

I mostly had Zip disks and CDs as a kid, but I can speak from experience that almost all my disks and cassettes still work. I can't say that about some of the other consumer tech I bought in the last few years, let alone decades. 💾

