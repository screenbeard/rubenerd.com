---
title: "A suburban Buenos Aires train"
date: "2021-09-27T11:37:22+10:00"
abstract: "Photo by Maruicio V. Genta."
thumb: "https://rubenerd.com/files/2021/eiden500@1x.jpg"
year: "2021"
category: Travel
tag:
- argentina
- photos
- public-transport
- trains
- south-america
location: Sydney
---
Everyone knows about global landmarks, but I'm just as interested in seeing photos of how people live in various places.

A post-migraine stupor over the weekend had me reading Wikipedia's articles on the mass-transit system in Buenos Aires, and to [this photo of a 500-series train](https://commons.wikimedia.org/wiki/File:Eidan-en-el-FCU-1_(cropped).jpg) by Maruicio V. Genta. I haven't been able to stop looking at it.

<p><img src="https://rubenerd.com/files/2021/eiden500@1x.jpg" srcset="https://rubenerd.com/files/2021/eiden500@1x.jpg 1x, https://rubenerd.com/files/2021/eiden500@2x.jpg 2x" alt="Train flanked by trees and a side-street in suburban Buenos Aires" style="width:500px" /></p>

It also lead me down the rabbithole of thinking where else has yellow suburban carriages like this. I could think of <a href="https://en.wikipedia.org/wiki/Berlin_U-Bahn#/media/File:Berlin_U-Bahn_IK_at_Olympia-Stadion_(3).jpg">Berlin</a>, [The Netherlands](https://en.wikipedia.org/wiki/List_of_trains_in_the_Netherlands#/media/File:NS_icm_koploper_3.JPG), and <a href="https://en.wikipedia.org/wiki/Ch%C5%AB%C5%8D%E2%80%93S%C5%8Dbu_Line#/media/File:Chuo-Sobu_201_set_6_Akihabara_20010504.jpg">Tokyo</a> off the top of my head. I wish more did; it's a striking colour and potentially safer.
