---
title: "Crypto-“currency” snakeoil"
date: "2021-12-16T10:38:17+10:00"
abstract: "A Bank of England blog post did a thorough job explaining the architectural and speculative dangers."
year: "2021"
category: Internet
tag:
- bitcoin
- cryptocurrency
- scams
location: Sydney
---
Thomas Belsham over at the The Bank of England's Bank Underground blog [does a thorough job](https://bankunderground.co.uk/2021/12/14/what-is-a-bitcoin-worth/) explaining the mechanics of Bitcoin and its inherent architectural and speculative dangers. This was my favourite quote:

> The problem is that, unlike traditional forms of money, Bitcoin isn’t used to price things other than itself. As Bitcoiners themselves are fond of saying, ‘one Bitcoin = one Bitcoin’. But a tautology does not a currency make.

Its volatility, unworkable transaction volume, energy use, silly name (it should be 2-bit coin, *amirite!?*) and lack of financial regulation (aka, what's my recourse when I make a mistake, or am the victim of fraud) also don't.

Thomas concludes:

> [S]imple game theory tells us that a process of backward induction should, really, at some point, induce the smart money to get out. And were that to happen, investors really should be prepared to lose everything. Eventually.

But here's the rub, and [not the good kind](https://www.onegreenplanet.org/vegan-food/ways-to-make-awesome-bbq-sauces-rubs-and-marinades/). You and I know Bitcoin, NFTs, and all this associated architecture is pure, uncut nonsense. It demonstrates on a regular basis how ineffective it is, and it implements this ineffectiveness with grossly inefficient technology that's more centralised in practice than its proponents claim.

But the quoted advice will fall on deaf ears *precisely* because it's coming from the Bank of England. And dare I say, I'm sympathetic to that.

Whether your memory stretches as far back as the Great Depression and hyperinflation, or even the 2008 global financial crisis, the public is wary of bankers and financial markets seemingly getting away with blue murder and poor behaviour that would be criminal if you or I engaged in it (token gestures like Bernie Madoff notwithstanding). The general public don't know the differences between monetary or fiscal policy, or how the complex web of manipulation, government bonds, and the terms of trade dictate exchange rates and the value of currencies. All most people have now is cynicism.

That's a huge problem, because cynicism is so easy to exploit.

Crypto-"currencies" are the perfect technocratic snakeoil for this receptive audience. Point out its foibles, and you'll have *whataboutism* thrown back that would make a Soviet official blush. It's the perfect defence.

I'm staring to think that's what makes it dangerous above all else. It breaks my heart seeing all the people falling for this and losing their life savings based on the advice of charlatans.

