---
title: "Feedback about FreeBSD, Linux VM gatekeeping"
date: "2021-06-11T08:46:08+10:00"
abstract: "Putting on bare metal does have its learning advantages, too."
year: "2021"
category: Software
tag:
- bsd
- feedback
- freebsd
- netbsd
- virtualisation
location: Sydney
---
[Last month I talked about](https://rubenerd.com/umbridge-with-people-trying-freebsd-or-linux-in-vms/ "Umbrage at trying FreeBSD and Linux in VMs") people who get angry or smug at those who run FreeBSD, Linux, and other OSs within VMs, with the implication that they're not *really* using them. I pointed out that this was a) technically inaccurate and b) not the way to foster good will or new users in open source software communities.

*(The title of that post was originally misspelled as Umbridge instead of Umbrage, which remains in the permalink. It's the punny name I've given to network bridges on personal hypervisors for years, which leads me to instinctively misspell the actual word each time. It's a bit ironic and especially silly given the topic at hand).*

Most of the well-intentioned feedback I've had suggests that running an OS directly on a laptop teaches you things you can't get on a VM. This is a good point; sometimes getting your hands dirty is the best way to learn. Workstation experience doesn't directly translate to servers, but things like package managers, reading logs, and the boot process certainly would. Who knows, your experience replacing a broken btrfs volume with ZFS might translate into industry experience that will come in handy in the future.

My point wasn't that running an OS directly on a laptop wasn't useful, but that it shouldn't pose a barrier to entry. I'm wary of gatekeepers in general, and this struck me as an especially counterproductive example.

It's also a wash as far as elitism goes. The most competent, professional, talented people I've met at industry events, conferences, and at work all use the right tool for the job, whether they be kernel developers or pre-sales engineers. Unfortunately, that might mean carrying a Mac or Windows machine to use specific software, and a BSD or Linux VM. That VM may be sitting on your local machine or the cloud. Whether you run such an OS directly on your laptop is not indicative of anything useful, nor does it speak to technical competency.

