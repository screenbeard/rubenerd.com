---
title: "Audacity fork Tenacity"
date: "2021-07-09T08:15:35+10:00"
abstract: "And of course the maintainer was harassed."
year: "2021"
category: Software
tag:
- audio
- open-source
location: Sydney
---
The Audiacity team found themselves in hot water last month after telemetry data was included in the venerable open source audio editor. This was only ever optional, but the loss of trust it represented was enough to spawn a fork.

The initial problem with the [Tenacity](https://tenacityaudio.org/) fork was as *open source community* as they come: harassment levelled at the maintainer over the name. I'd be more surprised if it was the result of something substantive.

Audacity has been the *de facto* indie podcast audio editor since the concept was first floated in the mid-2000s. I had come to expect it would always be there, but these tools are built, maintained, and supported by people in the real world, often times with other motives.

