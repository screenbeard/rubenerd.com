---
title: "When did it become: “delivered to your inbox”?"
date: "2021-12-09T15:12:01+10:00"
abstract: "It reads like all those sentences that start with: So, …"
year: "2021"
category: Internet
tag:
- email
- language
- newsletters
- spam
location: Sydney
---
When did websites stop saying this:

> Sign up to our email newsletter.

And start saying this:

> Our newsletter, delivered [right/straight] to your inbox.

I suspect it's another Silicon Valley tick everyone has internalised, like [prefacing sentences with so](https://rubenerd.com/people-starting-their-sentences-with-so/).

