---
title: "Your most annoying things about TV"
date: "2021-05-14T15:29:56+10:00"
abstract: "Empty coffee cups, subtitles, and convenient phone rings."
thumb: "https://rubenerd.com/files/2021/higehiro@1x.jpg"
year: "2021"
category: Anime
tag:
- higehiro
- fansubs
- subtitles
location: Sydney
---
Rachel Obordo compiled a fun list of people's [frustrations about TV shows](https://www.theguardian.com/tv-and-radio/2021/may/13/stop-drinking-fake-coffee-your-most-annoying-things-about-tv), and the *first one* was:

> Empty coffee cups. You can tell from the way people hold them that there’s no liquid in them, never mind hot coffee – surely they could at least fill them with water?

Many, *many* times this! It'd also get me that they'd bring the cup to their mouth to drink without looking at it. Maybe some people are able to pull off this [manufear](https://rubenerd.com/whats-a-manufear/) without making a mess, but I sure can't. It looks unnatural and weird.

And subtitles:

> Please complain about bad subtitles as often as you can. I translate STs for a living. Sometimes my work is really good. Sometimes less so, when I am asked to translate 1200 STs overnight, for a Chinese/Korean/Spanish etc video and with the help of an English ST file that is often even less intelligible than the Chinese/Korean/Spanish etc audio. BaZu007

<p><img src="https://rubenerd.com/files/2021/higehiro@1x.jpg" srcset="https://rubenerd.com/files/2021/higehiro@1x.jpg 1x, https://rubenerd.com/files/2021/higehiro@2x.jpg 2x" alt="Screenshot from Higehiro, text reading: What the hell?" style="width:500px; height:281px;" /></p>

Funnily enough, the best subtitles I've ever seen were from anime fansub communities, who were doing it for free. Modern anime distribution is still pretty good, but none go into that same level of detail, complete with live footnotes and avoiding covering up something important on-screen. Some would even translate signs.

My other peeve was the fact that phones always ring at a convenient moment when people aren't talking. Millennials like me hate phone calls *precicely* because they're such a disruptive incursion.

