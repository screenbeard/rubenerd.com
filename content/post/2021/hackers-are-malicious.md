---
title: "Hackers are malicious"
date: "2021-03-12T13:59:04+11:00"
abstract: "We need to deal with it."
year: "2021"
category: Internet
tag:
- ethical
- language
location: Sydney
---
[The Register ran a poll](https://www.theregister.com/2021/03/03/debate_hackers_for/) about whether the word *hacker* should be used as a pejorative. The majority voted no, preferring to retain the meaning of someone *hacking* on a problem.

I empathise, but that ship has sailed. The public thinks hacking is malicious, just as it was during Jack the Ripper's time. We can wish it weren't so, but that's been the reality for years. The fact we're even having this debate is evidence.

Effective communication isn't just accuracy, it's about being understood. A term requiring a qualifier is useless. Which is why I'm sidestepping the issue and simply not using the term *hacker* anymore. It frustrates technical people like you and I, and confuses the public. I think that's the only winning strategy.

*(It's also why we can't abbreviate cryptography to crypto anymore, based on non-technical speculators who think we weren't heating the planet fast enough).*

