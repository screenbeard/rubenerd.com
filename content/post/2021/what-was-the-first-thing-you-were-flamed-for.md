---
title: "What was the first thing you were flamed for?"
date: "2021-04-07T16:23:43+10:00"
abstract: "It was Star Trek fandom for me!"
year: "2021"
category: Internet
tag:
- comments
- sailor-moon
- star-trek
- star-trek-voyager
- trains
- weblog
location: Sydney
---
Comments, feedback, and civility&mdash;or lack thereof!&mdash;seem to be recurring themes here of late. Lest anyone think I'm repetitive, here's another related post I thought of while commuting to work today. Wait, I got that backwards.

My blog has been running on **rubenerd.com** since 2004, though it also briefly spent time on **rubenerdshow.com/blog** when I lost control of the former domain for a few years. That's an adventure to recount another time. But prior to this it hosted a personal site I'd carried over from Tripod to a shared hosting account, most of which was split among a few fandoms. I had the four big S's: Maxis Sim games, steam locomotives, Sailor Moon, and the world of Star Trek which I was just getting into.

The page on starship schematics had a complicated layout that relied on the schematic tables not getting too wide. I'd read that the USS *Voyager* could operate at Warp 9.999 for brief periods, which I summarised as Warp 10.

I could feel the collective bristling of all the Trekkies reading that. For the rest of you, Warp 10 is impossible with "conventional" warp drives; you can *approach* it but you can't meet or exceed it without transwarp. This is what the USS *Excelsior* first attempted to do.

And *boy* did I get told that! I was getting emails from all around the world from people not only telling me that my website was factually inaccurate, which was true, but the entire site was shit, and that I was an ignoramus... all recounted in colourful language. Some of the screeds went on for *paragraphs*. I wish I'd kept some of them now, they'd been hilarious.

I was told once that friction and fireworks in anime and tech clubs at university was all but inevitable because you're putting socially awkward people together in one place. Isn't that just the Internet in general now?

