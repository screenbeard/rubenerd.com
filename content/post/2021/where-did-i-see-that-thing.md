---
title: "Wait, where did I see that thing?!"
date: "2021-12-30T10:01:27+11:00"
abstract: "A quote by Ken Kocienda got me thinking about data again."
year: "2021"
category: Software
tag:
- data
- open-data
- search
- social-networks
location: Sydney
---
I've been feeling this for a while, but [Ken Kocienda crystalised it](https://twitter.com/kocienda/status/1475862637577596929) so perfectly, I have to share it:

> "I saw this thing a few days ago and I want to see it again but I can’t find it." This is my most persistent frustration while using my computer. The OS gives me no help. Where did I see it? Twitter? Slack? YouTube? Messages? Web? Email? Ugh!

This happens to me *at least* a few times a week, if not more. *The thing* could be something concrete, like a document, quote, or photo; or something intangible like an idea or concept. Each time is a maddening, and often times fruitless, exercise in using all the search boxes across all the applications trying to find something.

I'm often shocked at how tenuous some of those searches end up being to the final result. I knew a client I was talking with before Christmas had a fruit in their name, but it was the fact he made a dad joke that I quickly transcribed that let me find the text file where my notes were.

Two things amaze me about this. At a high level, I think it's surprising that we've turned computers&mdash;these logical, mathematical devices with enough raw processing power to crunch through billions of records&mdash;into something that can't process data... for us, at least. It'd be like taking water and making it flow a bit better, but it loses its ability to fall from the sky or wet things.

Yes information is distinct from data, and that data have to be stored and organised and indexed and all those other fun things data scientists spend their lives working on. But it borders on surreal to think how so much stuff is *designed* to be hostile to interop, when it doesn't need to be. Social media closing their graphs is but one *tiny* manifestation of this. There is *public good* in being able to do these searches, but we've let private companies dictate the practical future of how the web works.

I'm also amazed at a personal level. I've always meticulously maintained my file systems and directory trees going back to my teens. Almost every file I've ever saved ends up being organised by purpose, then date, then context. I have a glorified personal database that logs when and where backups are stored, when I last did a ZFS scrub, and so on. If I have trouble finding things in this hierarchy sometimes, what chance does a person with a life outside of tree pruning possibly have?!
