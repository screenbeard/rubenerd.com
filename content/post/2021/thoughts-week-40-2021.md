---
title: "Thoughts, week #40 2021"
date: "2021-10-10T15:11:11+10:00"
abstract: "I need a better title for these list posts."
year: "2021"
category: Thoughts
tag:
- listpost
location: Sydney
---
I'm thinking I might do more posts like this, where I write short snippets or include links that don't need their own post. I used to do this indirectly with [daily imports from del.icio.us](https://rubenerd.com/tag/delicious/), but what if I did the same thing but with one-off thoughts as well?

In the space of twenty-four hours, Sydney has gone from a smoke haze from back-burning, to being cold and overcast, to being clear and hot, to having an almost tropical thunderstorm. Having lived in Melbourne, three seasons in a day was almost nostalgic.

People saying "I'll get into trouble for saying this, but..." belong to the same category as those who say "no offence, but...". Anything proceeded by such a proverbial posterial prognostication can only be perennially and purposely provocative.

Today I learned of the phrase [teetee](https://www.urbandictionary.com/define.php?term=teetee) on Ina's latest Minecraft stream. It's vtuber slang for wholesome or precious comments. Fun!

Sitting on the balcony this afternoon I counted an almost equal number of tiny, fuel-efficient hatchbacks, and SUVs. Nay a sedan at all. Would I be reading too much into it if I felt like it was further evidence of society stratifying?

I'm also relieved to see [one of my old haunts](https://www.doppioespresso.com.au/) from back in the day is still around. Lockdowns can't have made the lives of coffee shop owners easy. They're my favourite places in the world, and I feel for them.
