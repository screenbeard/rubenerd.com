---
title: "Effectiveness of the Free Software Foundation"
date: "2021-04-19T08:16:26+10:00"
abstract: "Right now their attitudes are not productive."
year: "2021"
category: Software
tag:
- licencing
- open-source
location: Sydney
---
*This post is dedicated to [Michael Dexter](https://callfortesting.org/), for his encouragement and support.*

The Free Software Foundation has been in the news again for all the wrong reasons. Their board has reappointed a well-known person of disrepute, and details of their toxic work culture continues to emerge from former employees and contributors. The Foundation should welcome the scrutiny and use it as an opportunity to renew focus and be more welcoming, but there's no indication they're about to budge. 

The good news is that it's forced a spotlight onto the Foundation and its viability, effectiveness, and impact. Open source licence discussions have been moribund since version 3 of their GNU General Public Licence (GPL), though that's been to their detriment as more people adopt non-copyleft, permissive licences for their projects.

But let's take a step back for a moment. Alienating a portion of your support base as a movement seems counterproductive at best. At the risk of sounding a little *realpolitik*, an ideology of software freedom matters little if you outwardly project hostility and toxicity to your potential allies. Or put another way, ideas of such importance should be able to stand on their own, and not be dependent on one orator who has a record of shitty behaviour. I'd be tempted to leave this post here.

This alienation seems to be theme, regardless of intent. I've made no secret of the fact that I think the GPL has been a mixed blessing for open source software. Its goal of enforcing freedom on derivative software sounds noble, but has locked contributions out of other open source code such as the BSDs, MIT, and ISC. Non-copyleft code lifted into a GPL walled garden is as inaccessible to the original project as proprietary code. Suddenly the proposition that it ensures freedom comes across a little disingenuous.

The FSF have also used their clout to affect software outside their remit. The BSD and Python licences have had clauses updated or removed to accommodate GPL restrictions, at the behest of the FSF. I happen to think these clarifications were useful, but I'm wary of one organisation dictating how everyone else does software. This is why I find the [Open Source Definition](https://opensource.org/osd) more useful.

But has it at least been worth it? I'm not sure. The Linux kernel, once the poster child of GPL code protection and collaboration, won't budge from the GPLv2. I haven't seen much evidence that the GPL shepherds code more effectively than permissive licences, given the latter's success without the GPL's redistribution requirements. It's also complicated integrations, for no good technical reason. Like dedicated licencing servers for certain proprietary software, I bristle at the idea of code and infrastructure being built to service arbitrary human constructs instead of improving functionality.

GPL violations are also among the worst-kept secrets in infocomm circles. This isn't the fault of the FSF or GPL, but political movements are judged on their effectiveness. High-profile legal cases have forced dodgy companies to comply with their licence obligations. But we all know there are *plenty* of others, and there aren't enough lawyers or money in the world to go after them. We can keep chasing people down this rabbit hole, or acknowledge this reality and figure out alternative ways to foster collaboration that aren't as punitive.

There's a philosophical debate about proprietary code I don't want to get into here, though I feel I should at least acknowledge that permissive licences do more to spread good code and standards than GPL'd code. The FSF would treat that as a feature not a bug, considering they see proprietary software as an injustice, and liberal licences as not enforcing freedom. But this philosophical position has real-world consequences; see Clang/LLVM's encroachment on the GCC as another example. The opportunity cost spent thinking about all this could also be spent on building software.

I've wanted to write this post for many years, but have shied away owing to the toxic comments this will generate. It wasn't my intention to be inflammatory for no reason, nor do I want to sound ungrateful for all the GPL'd software I use on a daily basis. You might also disagree on my views about the GPL, which there's room for. I just think there's **so much damned potential in open source software**, and right now the FSF is not a productive partner, nor does it show any sign of changing. That hurts us all, and we should want better.

