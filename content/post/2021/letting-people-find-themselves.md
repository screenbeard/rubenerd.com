---
title: "Letting people find themselves"
date: "2021-03-31T22:11:38+11:00"
abstract: "Not only be themselves. Great tweet by Seamus Bryne."
year: "2021"
category: Thoughts
tag:
- philosophy
location: Sydney
---
Seamus Byrne may have written one of the [best tweets](https://twitter.com/seamus/status/1377067932216295425) of all time:

> The most important lesson I’ve learned is to just let people not only be themselves but *FIND* themselves. Everyone deserves room to explore their uncertainty! And that’s where visibility and support matter most – letting people feel comfortable wherever their journey takes them.

