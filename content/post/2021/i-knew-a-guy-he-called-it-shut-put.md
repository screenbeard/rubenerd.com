---
title: "I knew a guy, he called it shut put"
date: "2021-12-15T14:31:35+10:00"
abstract: "And he wasn’t even a Kiwi."
year: "2021"
category: Thoughts
tag:
- language
- pointless
location: Sydney
---
Out of absolutely nowhere, I used an adverb. I also remembered someone I knew in primary school who insisted to me that *shot put* was pronounced *shut put*. And not subtly either, he referred to the specific action one takes when closing a door or gate. As opposed to *shot*, which it literally was.

He didn't even have the excuse of [being a Kiwi](https://www.babbel.com/en/magazine/new-zealand-vs-australian-accent).

It makes me think what confidently incorrect statements I've made to people that they remember. Like using an adverb again, which writers say we're not supposed to do. It's all *remarkably* worrying.

