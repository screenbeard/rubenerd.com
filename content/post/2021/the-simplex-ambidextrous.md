---
title: "The simplex ambidextrous?"
date: "2021-10-20T07:50:42+10:00"
abstract: "I use my right hand for writing/mousing, but basically nothing else. It’s weird."
year: "2021"
category: Hardware
tag:
- health
- personal
location: Sydney
---
I have mad respect for the ambidextrous. Where one hand would do, they trained themselves to be capable wielders of the world with both. *Wielders of the world?* Sounds like an American comic book reboot, or a bad British tabloid. Was prefacing the latter with "bad" redundant?

I bring it up, because a few months ago a nurse was asking me what my dominant hand was so she could give me a Covid shot in the opposite arm. I instinctively said I was right handed, but it turned out not to be as clear cut as I thought. Owies.

I use both hands, but not interchangeably as an ambidextrous person would. I write (the one to three times a year when I do), move a computer mouse, and use those tiny screwdrivers with my right hand. But for racquet sports, minigolf, and anything that requires strength like opening pickle jars, I have to defer to my left. I also wield a soldering iron and sew with my left, the latter owing to being taught by a left-handed person. I tend to thrust out my left hand first when freestyle swimming too.

Maybe years of using a computer mouse in my right hand has weakened it, or given it RSI, so my body compensates by using my left. All I know is unscrewing the top of my Aeropress this morning with my right hand (having bashed my left arm into a door handle) made me appreciate the situation again.

Ruben? Absentmindedly injuring himself? *Say it isn't so!*
