---
title: "The Sinatra Christmas Album"
date: "2021-12-26T09:17:15+11:00"
abstract: "It wasn’t at all what I was expecting!"
thumb: "https://rubenerd.com/files/2021/sinatra-xmas-album@1x.jpg"
year: "2021"
category: Media
tag:
- frank-sinatra
- music
- yuletide
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/sinatra-xmas-album@1x.jpg" srcset="https://rubenerd.com/files/2021/sinatra-xmas-album@1x.jpg 1x, https://rubenerd.com/files/2021/sinatra-xmas-album@2x.jpg 2x" alt="" style="width:240px; float:right; margin:0 0 1em 2em;" /></p>

I overheard Ol’ Blue Eyes at a coffee shop singing a Christmas song yesterday, so I had to go home and buy the album it came from. I buy commercial music from ZDigital, but I'm sure the other usual suspects have it.

I was expecting it to be a swinging affair with lots of brass and that fat, big band sound that I've always loved. There were a few tracks like that, but the rest was more soulful, intimate, and chill than I thought. It was a side of Frank I'm not as familiar with, and both Clara and I liked it a lot.

I might need to look out for the original vinyl if I can get my hands on it.
