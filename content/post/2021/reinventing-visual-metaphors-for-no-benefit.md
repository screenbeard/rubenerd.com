---
title: "Reinventing visual metaphors for no benefit"
date: "2021-02-03T17:45:58+11:00"
abstract: "I place a decent chunk of the blame on CSS."
year: "2021"
category: Internet
tag:
- design
- interfaces
- usability
location: Sydney
---
Windows 3.1 came with a tutorial application for those unfamiliar with computer mouses and push buttons on screens. What did it mean to click something? How can you tell where you're supposed to click? Why are some buttons inactive, and some aren't? How can I adapt my understanding of the DOS prompt to a graphical environment? Then we had to teach ourselves multitouch smartphones, voice assistants, and Skynet.

We've build precedent into graphical desktops and phones with a standard(ish) set of controls, much like we did with buttons and dials on appliances before this. There are always exceptions, and specific functionality that requires a variation on a theme or, in extreme cases, an entirely new widget. But fundamentally we understand how to interact with things because we've done it with similar widgets before.

I think we're too quick to discount how *critically* important this is.

The web may as well be the wild west in this regard. Simple visual cues like coloured, underlined text for hyperlinks have given way to designs that bare little to no resemblance between sites. Antipatterns that work against users are rife, and solved problems are reinvented for no benefit beyond superficial appearance; aka, form over function.

Twitter lets you assign users to lists. Rather than showing you checkboxes, or toggle switches indicating their state and mutability (the ability to be changed) Twitter just shows them as a wall of plain text, styled the same way as a tweet. It's only when you click them that you realise they toggle, as indicated by a low-contrast tick. Why did they do this? Or more specifically, why did they feel the *need* to do this?

A banking website I use does a similar thing for accounts, the fundamental thing&mdash;pardon, "product"&mdash;that you use them for. Buttons without backgrounds and borders, dropdown lists that look like plain text, draggable elements styled the same way as fixed areas, overloading right-mouse clicks and text selection to do other things, widgets and controls hidden behind undocumented and unconventional gestures; they've achieved making *hamburger menus* look usable. Just because you can do something, doesn't mean you should. 

I'm starting to think it's another way CSS has failed us: in the rush to separate content from presentation, we've made it easy to jettison visual semantics and cues. This is a bug, not a feature.

