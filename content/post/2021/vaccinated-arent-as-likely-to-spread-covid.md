---
title: "Vaccinated aren’t as likely to spread Covid"
date: "2021-09-24T10:27:52+10:00"
abstract: "Dispelling this entrenched belief among anti-vaxxers and the overly-cautions."
year: "2021"
category: Thoughts
tag:
- covid19
- health
location: Sydney
---
Craig Spencer, an emergency-medicine physician and global health director at New York Presbyterian/Columbia University Medical Center, [had this to say about](https://www.theatlantic.com/ideas/archive/2021/09/the-vaccinated-arent-just-as-likely-to-spread-covid/620161/ "No, Vaccinated People Are Not ‘Just as Likely’ to Spread the Coronavirus as Unvaccinated People") this post title for The Atlantic:

> Vaccinated people are not as likely to spread the coronavirus as the unvaccinated.

And he adds:

> Even in the United States, where more than half of the population is fully vaccinated, the unvaccinated are responsible for the overwhelming majority of transmission.

He puts some of the blame on this on the media and health authorities for their confusing messaging. I agree with this regarding Australia as well. People who should have known better, should have. Is that a tautology? It might be, because it's a tautology.
