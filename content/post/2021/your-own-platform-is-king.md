---
title: "Your own platform is king"
date: "2021-09-12T19:55:34+10:00"
abstract: "Advice from Eric Kim about owning your platform."
year: "2021"
category: Internet
tag:
- self-hosting
- writing
location: Sydney
---
Photographer Eric Kim had [this bit of advice](https://erickimphotography.com/blog/2021/08/21/your-own-platform-is-king/) for photographers, though I think it applies to anyone with a creative project:

> Simple way to thrive as a photographer: **Own all your platforms.**
>
> For example, own your own self-hosted blog. Bluehost.com or 1and1.com and install wordpress.org
>
> Instead of social media, make your own platform.

This isn't tenable for everyone, but well worth it if you can.

