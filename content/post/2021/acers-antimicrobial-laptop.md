---
title: "Acer’s antimicrobial laptop"
date: "2021-07-16T10:06:21+10:00"
abstract: "Of course it's plagued with PC Screen Syndrome, but the other selling point is compelling."
year: "2021"
category: Hardware
tag:
- acer
- design
- health
- laptops
- pc-screen-syndrome
location: Sydney
---
Acer have released a [svelte new set of machines](https://www.acer.com/ac/en/AU/content/series/swift5) that purport to have a unique hygiene feature I haven't seen before:

> Introducing the first thin and light consumer laptop with a full-featured antimicrobial solution. The BPR & EPA compliant silver-ion antimicrobial agent runs through the surface of the chassis, keyboard, hinge, and fingerprint reader; showing a consistent [3 log5 (>99.9%)4] microbial reduction rate against a broad range of bacteria under the JIS Z 2801 & ISO 22196 test protocol.

I've seen tech journalists claim this is a gimmick, but I think it's genuinely intriguing and innovative. We rest our gross palms on these things for thousands of cumulative hours, so why not?

But is it plagued with the pervasive *[PC Screen Syndrome](https://rubenerd.com/why-do-most-pc-laptops-have-awful-screens/)*?

> The 14-inch Full HD IPS touchscreen display [...]

Once again, this is lower density than [what Apple shipped a decade ago](https://rubenerd.com/now-obsolete-macbooks-still-have-better-screens/), and a lower resolution *that a phone*. Come on, PC makers.

