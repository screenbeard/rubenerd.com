---
title: "Rest in peace, Bert Newton"
date: "2021-10-31T10:16:39+10:00"
abstract: "Probably the biggest icon in Australian television history."
year: "2021"
category: Media
tag:
- australia
- television
location: Sydney
---
My sister and I spent most of our formative years overseas, but even we remember Bert Newton. He was a fixture of Australian television from the black and white era to the present. My parents regularly invoked his legendary sketch comedy skits with Graham Kennedy, and we remembered him as being the ever-present host of Good Morning Australia.

<details>
<summary style="font-style:italic;">Warning for Aboriginal Australians: This contains an image of a deceased person. Photo of Lucy Durack and Bert Newton in 2014 by <a href="https://en.wikipedia.org/wiki/File:2014_Moomba_Parade_(60th_Anniversary)_Melbourne_(13054867724).jpg"> Chris Phuttly</a>. Click to expand.</summary>

<p><img src="https://rubenerd.com/files/2021/bert-newton-parade@1x.jpg" srcset="https://rubenerd.com/files/2021/bert-newton-parade@1x.jpg 1x, https://rubenerd.com/files/2021/bert-newton-parade@2x.jpg 2x" alt="Photo of Lucy Durack and Bert Newton in 2014." style="width:500px" /></p>
</details>

Years ago I was stuck at home with a fever, and watching morning television to pass the time. Bert had just cut over to one of GMA's infomercial segments to advertise a vacuum cleaner, but unbeknownst to him he was still slightly in frame. He got out of his chair and started plowing the ground with an invisible vacuum cleaner, while the poor people on the other set tried to keep straight faces while delivering their lines. Eventually the advertiser broke down and waved Bert over to try the real thing. I can't remember the line Bert gave them as he came onto their set, or I was too young to understand the joke, but he had them in stitches.

I like to think that was how he was. Like most Australians I've ever met in business and life, he was professional, but just a bit cheeky. 

I also assumed he'd be around forever. Rest in peace, Old Moonface.

