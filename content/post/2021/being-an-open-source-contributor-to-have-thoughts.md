---
title: "Being an open source contributor to have thoughts"
date: "2021-09-02T08:25:33+10:00"
abstract: "We need respect from both sides."
year: "2021"
category: Software
tag:
- communities
- open-source
location: Sydney
---
Do you need to contribute code to an open source project to be granted permission to critique it? I saw the latest theme limitations in GNOME have generated legitimate concern, which in turn have lead to the metric of contributions being used to put people in their place. If you don't contribute code, you don't get a say. It's as simple as that.

Leaving aside documentation, QA testing, security audits, reviews, graphic design, financial assistance, project management, and the litany of other ways people contribute to open source, it's a bizarre and self-defeating position to take (can you have an opinion on a book you didn't write, or a movie you didn't direct?). But its one I can empathise with to an extent.

Being an open source developer can be a thankless, tiring job. The web is replete with articles discussing maintainer burnout stemming from long hours, little recognition, and an over-dependence on their code. Open source was supposed to foster collaboration, but predictably most businesses see it as a source of free code to pilfer. OpenSSL was a tragic example of the industry taking critical software, and its developers, for granted.

Open source projects like GNOME have corporate backers and salaried staff writing code, given its use in OSs like Red Hat Enterprise Linux and Ubuntu that offer paid support packages. But even if it didn't, and it was just the proverbial developers in a basement scenario, there are real people behind the software we use.

But that attitude and desire for respect cuts both ways. Enthusiasts aside, real people use software to solve a problem or complete a task. They're exactly the ones who can provide the most valuable feedback for how software works in the real world. Those people are your advocates, and also your potential future contributors. Gatekeeping someone's feedback on the grounds they have insufficient green squares on GitHub is a guaranteed way to evaporate good will. Proprietary software vendors would love nothing more.

My younger, naive self assumed the collaborative nature of open source software would be a self-correcting mechanism for bad behaviour. After all, many of these projects don't have the deep pockets and yachts to absorb the impact of churn and treating people poorly, and their users are getting something for free for which they should be thankful. We can still get to that point, but it'll take respect from both sides.

