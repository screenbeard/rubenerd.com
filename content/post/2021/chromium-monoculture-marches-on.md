---
title: "Chromium monoculture marches on"
date: "2021-03-20T13:56:00+11:00"
abstract: "Don’t worry, they said!"
year: "2021"
category: Internet
tag:
- browsers
- chrome
- chromium
- monoculture
location: Sydney
---
I rasied the impending Chromium monoculture [back in August last year](https://rubenerd.com/firefoxs-situation-reminds-me-of-openssl/). I raised the issue about why monocultures are bad, why it *is* the same thing we dealt with during the dark IE days, and what we need to do to address it again.

The feedback was mostly sympathetic, though I still had a few people claiming Chrome is better placed than IE (which I already addressed), and that the number of Chrome-specific sites are too insignificant to matter.  As my old man says, *I wish I shared their optimism!*

<p><img src="https://rubenerd.com/files/2021/screenshot-monoculture.png" alt="For the best user experience, we recommend using the Google Chrome Browser." style="width:490px; height:102px;" /></p>

What's the situation half a year later? Mixed, but the trajectory is clearly downwards. Three new sites that Clara and I frequent either mandate Chrome, or recommend it. Two further sites don't mention Chrome, but specific functions no longer work. We raise complaints with the bank or site operators pointing out the accessibility concerns of their exclusionary designs, and how they're in breach of their own charters and industry guidelines. But this needs to be a concerted effort, just as we all did before.

Be wary of people making excuses for this state of affairs, just like they did in the 2000s. This is not the direction for a healthy web.

