---
title: "Wave Dreamtech Ayanami Rei fig"
date: "2021-12-20T10:01:27+10:00"
abstract: "Finally a good Rei fig!"
year: "2021"
category: Anime
tag:
- anime-figs
- evangelion
location: Sydney
---
The *Evangelion* franchise continues to get new merchandise more than two decades after the initial anime aired, which speaks to its enduring appeal and loyal fanbase. Maybe a touch of cash-cowism too, a phrase I'm sure doesn't exist but should.

Unfortunately, I think most of the figs of the star characters have been underwhelming or downright painful to look at, and I'd argue nobody has copped this more than Rei.

<p><img src="https://rubenerd.com/files/2021/dreamtech-rei-closeup@1x.jpg" srcset="https://rubenerd.com/files/2021/dreamtech-rei-closeup@1x.jpg 1x, https://rubenerd.com/files/2021/dreamtech-rei-closeup@2x.jpg 2x" alt="Press photos of the fig." style="width:500px; height:400px;" /></p>

So many sculptors took the characteristic mecha plugsuits from the series as an opportunity to run roughshod over the characters, forcing the heroines into all manner of [back-breaking poses](https://rubenerd.com/aizu-project-ayanami-rei/), fabric so tight I'd scarcely believe they could breathe, and body proportions well in excess of what the original character had. At some point they're not even caricatures if the only thing the fig has in common with their reference is hair colour and a common number of eyes.

Wait, hold on, I have the same number of eyes as her too. Maybe I should adopt her posture as well.

By comparison, is a phrase with two words. Wave's Dreamtech 1/7-scale Ayanami Rei is the best fig of her I've seen in *years!* Everything about her is completely reasonable... as far as anime goes. Her breasts aren't water balloons (to borrow a phrase from my sister), and she won't require spinal surgery after the fact. Even her expression is one I'd expect her to have, and her aforementioned eyes are faithful to the original art style of the series. *Thank you* Miki Ousaka!

<p><img src="https://rubenerd.com/files/2021/dreamtech-rei-zoomedout@1x.jpg" srcset="https://rubenerd.com/files/2021/dreamtech-rei-zoomedout@1x.jpg 1x, https://rubenerd.com/files/2021/dreamtech-rei-zoomedout@2x.jpg 2x" alt="Press photos of the fig." style="width:500px; height:280px;" /></p>

This is a mixed blessing at best though, because now I might have to preorder her. I can already hear our scaled-down shelving infrastructure crying out in frustration. Clara just ordered another Prince Cat, but at least he's squishy and compressible. Not like a spine.
