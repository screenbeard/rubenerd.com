---
title: "Dodgy listings using repurposed packaging"
date: "2021-07-05T11:07:17+10:00"
abstract: "This is, to use a phrase from Hololive’s Subaru: a shocking image."
thumb: "https://rubenerd.com/files/2021/dodgy-mouse@1x.jpg"
year: "2021"
category: Hardware
tag:
- ebay
- ethics
- mouses
- shopping
- windows-95
location: Sydney
---
I've been buying retro computer gear online for many years now, and have seen my fair share of tricks and swindles designed to part money from unsuspecting nostalgic shoppers for something they didn't want or expect. Peripherals listed as "new old stock" that have scuff marks from regular usage, for example. Or hardware with minor version differences being passed off as something rarer, newer, or in better condition.

For each seller who makes a mistake, I'd be willing to bet there are a billion others who do it on purpose. Statistically speaking, is a phrase with two words.

Something I've started seeing more of is packaging being used to sell something else entirely from what the box depicts, describes, and other d-words. Take this box for a Microsoft Mouse 2.1a from 1996:

<p><img src="https://rubenerd.com/files/2021/dodgy-mouse@1x.jpg" srcset="https://rubenerd.com/files/2021/dodgy-mouse@1x.jpg 1x, https://rubenerd.com/files/2021/dodgy-mouse@2x.jpg 2x" alt="A dodgy mouse listing showing an entirely different mouse stuffed into the wrong box." style="width:500px; height:312px;" /></p>

Your eyes are not deceiving you! This isn't the right mouse. It isn't even the right manufacturer. The cutout for the box is comically wrong. The photograph of the instruction manual *clearly* depicts a different mouse from what's being sold. It's the wrong shape, the wrong size, has the wrong number of buttons, and it even has the wrong physical connector. I wouldn't be surprised to crack open that other mouse only to find the trackball is a Maltezer.

Is that what you want us to use as a mouse? A *chocolate* ball!?

The seller in these circumstances could claim they were being transparent by providing photos that are your responsibility to review, as a responsible civic shopper of responsibility. What you see is what you get, and *caveat emptor*, right? I'd be sympathetic to this, but why list something in the wrong box for any reason other that to deceive? There's no escaping the trifling fact that the title of the listing is *for a different device*, and the description only doubled-down like that proverbial fast food chicken burger. Only that isn't heartburn, that's the feeling of a hole being burned in your wallet. *Brilliant.*

Yes, this is an especially silly and contrived example that's visually obvious, but other sellers are far more subtle. I've seen old peripherals put into newer packaging as a way to tart up their condition, or to mislead buyers into thinking it's a rarer device, or one with additional features. I saw someone try to pass an IntelliMouse with a scroll wheel off as one that Microsoft shipped for MS-DOS. That's why they called those games *side-scrollers*, don't you know?

Much as people say about my physical appearance, the problem isn't just cosmetic. Minor version differences in legacy computer peripherals could mean the difference between compatibility and not. Without even leaving the realm of mouses, there were multiple competing protocols from the likes of Microsoft and Logitech, requiring different driver configuration and software support. Some work with passive PS/2 to serial DB9 adaptors, but some don't. Ah USB-C, we've learned absolutely nothing.

The end result is a buyer could be on the receiving end of a device that looks the part, but simply doesn't work for the use case they bought it for. That sounds a bit less like false advertising and more outright fraud, if one were susceptible to embellishment to make a broader point about the reputability of eBay auctions as I'm attempting to do now. Like a gentleman.

But it's even worse than that. I can tell immediately the bundled IntelliPoint software on disk would not be compatible with this device either, nor would be the PS/2 to serial DB9 adaptor.  An unsuspecting buyer would be receiving something that’s either not functional at all, or wouldn't have the features they expected by reading reviews or the box itself. I can only imagine the frustration of someone trying to troubleshoot why bundled software doesn't work with what they bought. *Is it a problem with my machine? Why would this manufacturer bundle incompatible software!?*

Even if you're not convinced this is an issue, consider accessibility. People with poor visual acuity or those who use screen readers only have the false or misleading information to base their purchasing decision on. Is it reasonable to expect these users, or anyone for that matter, to enquire about every device they buy online to confirm whether the box includes what it claims to? I appreciate this is a second-hand market and not retail, but in what other context does that burden on the buyer make sense? People would shout murder if Amazon sent you a budget Android phone in your iPhone box.

My approach thus far has been to assume good intentions, and to ask the seller to revise their listings for what must have been a mistake. Those who aren't responsive to this need to start being called out like the rodents they are. Hands down.

