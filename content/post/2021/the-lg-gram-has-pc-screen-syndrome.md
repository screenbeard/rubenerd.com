---
title: "The LG Gram: does it have PC Screen Syndrome?"
date: "2021-08-12T09:30:51+10:00"
abstract: "Check it out in the next gripping installment! (the answer is yes)"
year: "2021"
category: Hardware
tag:
- displays
- laptops
- pc-screen-syndrome
location: Sydney
---
**Update:** I've removed the text of this post. I was in a foul mood, and it was cynical and mean-spirited. That's not who I want to be.

My point remains that PC manufacturers need to [lift their game](https://rubenerd.com/tag/pc-screen-syndrome) with screen resolutions. There's no good reason for a modern laptop to have a lower resolution screen than a smartphone, or what Apple shipped a decade ago. PC users deserve better.

Unfortunately, even LG's new Gram laptop line suffer from low DPIs and resolution, even on their high-end models.

