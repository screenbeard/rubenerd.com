---
title: "Geri Halliwell on material science"
date: "2021-06-08T23:12:21+10:00"
abstract: "Fake honey, real plastic."
year: "2021"
category: Media
tag:
- environment
- music
- pointless
location: Sydney
---
I [overheard a song](https://en.wikipedia.org/wiki/Look_at_Me_(Geri_Halliwell_song)) at a coffee shop that transported me right back to the late 1990s. Then I heard this lyric again:

> Fake honey, real plastic;   
> What you see ain't what you are getting.

It's funny that we see both as bad. Authenticity isn't as important as taste or impact. Give me real honey, and compostable, plant-based fake plastic instead, please.

*(Though is it "fake" plastic if it's still a polymer, just not based on synthetic hydrocarbons? Why do I do this to myself)?*
