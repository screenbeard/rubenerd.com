---
title: "The Cloud Media Remote for the PS4"
date: "2021-08-16T09:02:46+10:00"
abstract: "Do you also use your PS4 as a glorified Blu-Ray and DVD player? This remote is great!"
thumb: "https://rubenerd.com/files/2021/pdp_cloud_media_remote_ps4@1x.jpg"
year: "2021"
category: Hardware
tag:
- ergonomics
- design
- gaming
- movies
- ps4
- reviews
- shows
location: Sydney
---
Clara and I inherited a Sony PS4 a few years ago, which we've proceeded to use as a glorified Blu-Ray and DVD player. It works fine, though the lone controller was already starting to fall apart after what I can only assume was some *heavy* gaming on the part of the previous owner. I joked that we should replace it with a remote control instead.

<p><img src="https://rubenerd.com/files/2021/pdp_cloud_media_remote_ps4@1x.jpg" srcset="https://rubenerd.com/files/2021/pdp_cloud_media_remote_ps4@1x.jpg 1x, https://rubenerd.com/files/2021/pdp_cloud_media_remote_ps4@2x.jpg 2x" alt="Photo of the aforementioned remote." style="width:131px; height:400px; float:right; margin:0; padding:0 0 1em 2em;" /></p>

*Turns out*, they exist! We found and ordered a PDP Cloud Media Remote on special and have been putting it through its paces. I didn't realise that DEC were making 12 and 16-bit remote controls for their [1970s minicomputers](https://en.wikipedia.org/wiki/PDP-11), let alone game consoles. I'll get one of those gorgeous [PiDP-11 kits](https://hackaday.io/project/8069-pidp-11) one day.

The remote's design is nothing remarkable, but that's a *good thing*. It was slightly larger than I expected, has a textured back that's easy to grip, the buttons are nice and tactile, and the rounded bottom makes it easy to orient. The latter might seem like an odd thing to mention, but we do use an Apple TV with its slippery, inscrutable remote as well. It still floors me that a company so lauded for its industrial design an engineering can have such spectacular misses.

It has all the directional and shape controls (is that what you call them?) as a standard PlayStation 4 game controller, but arranged as you'd expect on a remote for light use. You pair it with Bluetooth by opening the system settings and adding a "New Bluetooth Device" and it just works. Don't be tempted to try adding a "New Bluetooth Controller" as we first did, it's not recognised. The instructions make this clear, had I been bothered to read them prior to messing with it.

I have but a few minor quibbles:

* The volume controls don't work with our TV, even though they do work with the Apple remote. This was something I was most looking forward to using, but its not a deal breaker.

* Like a kid stuck with a toy in the 1980s, *batteries are sold separately*. We have a stash of rechargeable IKEA/Varta AAAs we were able to pop in, but I can see this being a source of frustration for some buyers.

* It's not any cheaper than a standard game controller, despite its simple interface and smaller size. It wouldn't ever be given economies of scale, but it's worth pointing out if your aim is to save money.

Overall is an item of clothing, and we're happy with it. It's probably redundant if you have a nice game controller, but it pairs well if you use your PS4 primarily as a media device.

Now if only the PS4 could play standard music CDs. The fact it can't *floors* me. Back into the LaserDisc player they go!
