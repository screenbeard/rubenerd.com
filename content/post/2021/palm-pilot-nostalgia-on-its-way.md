---
title: "Palm PDA nostalgia on its way!"
date: "2021-04-21T09:54:17+10:00"
abstract: "Remembering this other class of devices I was obsessed with growing up."
thumb: "https://rubenerd.com/files/uploads/photo.palmiiix.jpg"
year: "2021"
category: Hardware
tag:
- palm
- palm-iiix
- nostalgia
location: Sydney
---
In light of my [Pentium 1](https://rubenerd.com/tag/mmx-machine/) and [Commodore 128](https://rubenerd.com/tag/commodore-128/) posts (that are still pending because replacement parts are now more than a month late!) I realised I've only ever briefly talked about my other childhood electronic obsession, and a series of devices I'm also getting back into with earnest.

I saw my first Palm device at a COMDEX trade show in Singapore. The late [EDPOL Systems](https://opengovsg.com/corporate/53133751X) from Funan Centre had a booth where they were showing various generations of Palm devices. This little grey slab with its giant monochrome screen and friendly green power button sitting in its docking cradle *looked like the future*. Seeing the EDPOL staffer write in Graffiti and have her strokes translated into characters on the screen was one of my first *whoa* moments in IT. Those stick with you.

<p><img src="https://rubenerd.com/files/uploads/photo.palmiiix.jpg" alt="Photo of a Palm III device" style="width:105px; float:right; margin:0 0 1em 2em" /></p>

For the next few months I poured over the specifications for the devices in PC World and PC Magazine reviews, and cut them out into folders. I was in primary school at the time, and had no need for complicated calendars, to-do lists, expenses, or business contacts. But I was *deeply* fascinated with how they could fit an entire computer into such a small space, and how the interaction model worked. The idea of HotSyncing things from a desktop to a small device was equally compelling. I wanted to know *how* it worked as much as wanting one.

The Palm V was a sleek new addition to the line, but I was most interested in the IIIx that I'd seen demoed. It also had a whopping 2 MiB of extra memory! Both had a new inverted backlight which made the screen even sharper and clearer in low light compared to the original III and PalmPilots, a feature those aforementioned reviewers raved about.

That Christmas my parents surprised me with one! I was told years later that it was one of the best things they'd ever bought, considering how captivated I was with it during long trips or holidays. They even bought my sister a IIIe, which had less memory but a translucent case which looked *awesome*. We used to swap programs and write all sorts of things on these little slabs.

Windows CE soon came out, and a few years later I was using a clamshell HP 620 LX with a physical keyboard. But tellingly I kept going back to the IIIx, and longed for the 256-colour IIIc which had come out. I used the infrared on my first Motorola phone to use a WAP browser. I even had a BASIC interpreter application for it to write my own little programs, right on the device.

<p><img src="https://rubenerd.com/files/uploads/photo.palmtw.jpg" alt="Palm Tungsten W" style="width:87px; float:right;" /></p>

Over time the Pocket PC, Blackberry, and Symbian eclipsed the performance and feature set of these Palm devices. My first smartphone in the 2000s was a Palm Tungsten W, which was *incredibly* underpowered compared to what others in the industry offered. But it pared the elegant Palm interface and HotSync software with a colour screen and a phone. What more would you want?

By late high school I'd started using these Palms for their intended purpose. I had my class and exam schedules loaded into the calendar, to-do lists for homework, and study notes to revise from on the train. My school had official paper organisers you were supposed to use, but I hollowed out one so I could use a Palm inside on the sly. I don't think I fooled anyone.

My first high school crush even realised what was up when I let her take a look at one of my Palms, and realised her email address was the only one in my Contacts app that wasn't family. I still get flustered about that even now! How do you say *whoops!* in Korean?

I still maintain the classic PalmOS has the best mobile UI ever designed, and their Palm Desktop PIM software still kicks the pants off anything else. The mobile OS had no discoverability requirements, unintuitive gestures, or other required nonsense; everything was behind context menus that you'd tap a program's title to access. Tappable elements were outlined buttons, what a concept! I'm not sure how the UI would have carried over to touch screens over resistive ones, but the UI mess of Android and the *precipitous* sliding standards of iOS pale in comparison.

Palm no longer exists. After a string of musical-chair buyouts, they eventually released webOS to compete with modern smartphones. It was, again, better than Android or iOS. But their underpowered hardware struck again, and they were eventually bought by HP and LG. It was the end of an era.

I say all this because I've had some saved eBay searches and price ceilings up for a few years, and have got some replacements for my long-lost and stolen original Palms in the post. I even managed to score a Grail that I always wanted growing up, but could never afford. The good news for people like me is that Palms haven't achieved any kind of cult status to the same extent Commodore hardware has. Or at least, yet. You can pick them up for very cheap, and thanks to their build quality and hardware simplicity, they all largely still work. Hopefully I'll be able to write about them soon :).

