---
title: "Feedback from my week 38 links post"
date: "2021-09-21T17:04:12+10:00"
abstract: "Thanks Jim Kloss, Hugh Lawrie, Adam Spencer, and Rebecca Hales :)."
year: "2021"
category: Thoughts
tag:
- feedback
- links
location: Sydney
---
Jim Kloss, Hugh Lawrie, Adam Spencer, and Rebecca Hales sent, tweeted, and smoke signalled that they liked the recommendations in my [week 38 link post](https://rubenerd.com/read-list-for-week-36-2021/) from the weekend. One of those people, and one of their communication methods, were a lie.

*(What was that line from the Highlander TV series? "He called me a cheap person and a thief! I... was never cheap!")*

I've done a few link posts in the past, though this time it was to cull dozens of accumulated draft posts. It's a silly irony that for each post I write, at least three draft posts spawn than never see the light of day... or moonlight, if you're as adverse to stakes through the heart as I am.

The feedback says to me that these posts are more useful to *you* as well than I thought too. I'm thinking I'll post separate lists for news and tool recommendations, and not only so I can categorise them properly.

Recommendations is such a long word. *Rebeccamentations?*

