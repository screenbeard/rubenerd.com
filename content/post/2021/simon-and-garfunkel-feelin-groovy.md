---
title: "Simon and Garfunkel, Feelin’ Groovy"
date: "2021-08-16T08:33:13+10:00"
abstract: "Doop in doo doo ♫"
thumb: "https://rubenerd.com/files/2021/yt-So0ZrTwf8vI@1x.jpg"
year: "2021"
category: Media
tag:
- art-garfunkel
- music
- music-monday
- paul-simon
- simon-and-garfunkel
location: Sydney
---
Today's [Music Monday](https://rubenerd.com/tag/music-monday/) was a bit of a rediscovery. My dad let me borrow a small portion of his and my late mum's vinyl LP and 45 collection last Christmas, and you *know* I had to take some of their Simon and Garfunkel. Some of them even had my mum's maiden name signed on them with calligraphy and flowers. ♡

I feel like I can play these precious albums now that I have a [proper turntable](https://rubenerd.com/my-new-old-technics-sl-j300r-turtable/) with the gentlest of tracking forces and a linear tone arm. I live firmly in the digital world, but there's something so intimate and special knowing that these *exact* grooves were producing audio signals my parents listened to when they were my age and younger.

I thought *Feelin’ Groovy* was fun growing up, but wouldn't have ranked it among my favourites. It's been the perfect pick me up over the last couple of weeks, not least for the *doop-in-doo-doo* in the chorus which I've been repeating with gusto since.

<p><a href="https://www.youtube.com/watch?v=So0ZrTwf8vI" title="Play Simon & Garfunkel - Feelin' Groovy (from The Concert in Central Park)"><img src="https://rubenerd.com/files/2021/yt-So0ZrTwf8vI@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-So0ZrTwf8vI@1x.jpg 1x, https://rubenerd.com/files/2021/yt-So0ZrTwf8vI@2x.jpg 2x" alt="Play Simon & Garfunkel - Feelin' Groovy (from The Concert in Central Park)" style="width:500px;height:281px;" /></a></p>

Clara and I didn't make it to the 59th Street Bridge when we went to New York. Next time.
