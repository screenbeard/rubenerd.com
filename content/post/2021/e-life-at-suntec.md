---
title: "e-life @ Suntec"
date: "2021-04-19T11:49:41+10:00"
abstract: "Remembering the pavilion at Suntec City in Singapore."
thumb: "https://rubenerd.com/files/2021/elife-suntec@1x.jpg"
year: "2021"
category: Travel
tag:
- architecture
- nostalgia
- singapore
- suntec-city
location: Sydney
---
I'm all for electronic nostalgia, but I haven't written much about my haunts themselves growing up. Singapore's electronic retailers aren't faring much better than their American counterparts, based on the sad news I heard about Fry's Electronics.

I still fondly remember Funan Centre before it was gutted, and am counting down the days before Sim Lim Square meets the same or worse. But I'd completely forgotten that there was a period during the 2000s when Suntec City even had its own dedicated electronics section. Sengkang shared this picture on [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Suntec_City_Mall,_e-life@suntec,_Jul_07.JPG)\:

<p><img src="https://rubenerd.com/files/2021/elife-suntec@1x.jpg" srcset="https://rubenerd.com/files/2021/elife-suntec@1x.jpg 1x, https://rubenerd.com/files/2021/elife-suntec@2x.jpg 2x" alt="View of the e-life @ Suntec pavilion upstairs in Suntec City, by Sangkang on Wikimedia Commons." style="width:500px; height:333px;" /></p>

Everything about this screams 1990s and 2000s, from the name and use of the @ symbol, to the glass panels and Suntec's industrial-looking metal cladding. This was just above the round atrium and escalators as you came in from the CitiLink Mall from the Town Hall MRT. I'm fairly sure this is all gone now, probably replaced with an ESPRIT or similar.

I almost got a job at the EpiCentre, albeit their Wheelock Place branch back in the day. I even have [photos of it](https://www.flickr.com/photos/rubenerd/1760034775/in/photostream/) from when Mac OS X Leopard was released, back when Apple made kick-arse UNIX workstations.

I miss Suntec, more than I realised.

