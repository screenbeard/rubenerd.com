---
title: "Mix “tapes” in 2021"
date: "2021-09-22T15:28:13+10:00"
abstract: "The advantages of buying music, and nerding out making your own compilation albums with custom ID3 metadata."
thumb: "https://rubenerd.com/files/2021/yt-yWT5IJ7Cusc@2x.jpg"
year: "2021"
category: Media
tag:
- ipod-classic
- palm-lifedrive
- music
- streaming
location: Sydney
---
I've mentioned here my motivation to ditch streaming services and buy music with the money I saved. There are so many benefits:

* You're supporting artists directly, which is always going to be worth more than the pittance (if that) they receive from streaming platforms. I've long said these services "solved piracy" for the record labels, but made no practical difference for anyone else. That's a topic itself.

* You don't have tracks, albums, or playlists revoked without notice due to the ever-shifting world of licencing, nor can they be replaced with ones you didn't want or expect. A studio album version isn't a replacement for a live recording, for example.

* It puts more value into music somehow (for me). Buying an album, whether digital or physical, feels more intimate and real. Streaming music makes it feel disposable.

* You can nostalgically burn or record them to physical media, or buy an LP, or a cassette, or a minidisc in the first place! Many new LP pressings also give you a code to download MP3 or FLAC versions to take on the go.

But there was another massive benefit I forgot about from the days where I maintained a carefully-curated iTunes library, and it's one that this [clinically-diagnosed] OCD sufferer can *really* appreciate: you're the ultimate arbiter of metadata!

This is important for three reasons. First, is usually proceeded by second... ah, so good. Second, you can fix up erroneous or inaccurate metadata that you'd be stuck with on a streaming service that can't maintain your favourite artists as well as you could. I was bugged for ages that a Michael Franks album had a typo in one of the songs, and that another album of his was listed as "rock" with poorly-scanned cover art.

But third is where it gets exciting. **You decide what metadata you want.** If I want to set the genre of Esther Golton's *[Aurora Borealis](https://esthergolton.bandcamp.com/album/aurora-borealis-conversations-with-alaskas-northern-lights)* as "chill", I can. If I want to subdivide my electronica into downbeat and synthpop, why not? It doesn't matter if the consensus among fans or music critics is something else, or even if the genre exists. Clara's the only other person who'd ever see this library, and it makes it more meaningful for us.

This also applies to playlists. I'm a huge fan of Palm Pilots and MP3 players like the iPod Classic and Creative Zen. Apple has made their original "widescreen iPod with touch controls" that happens to be a phone into a device that's confusing and frustrating to use for portal music collections. The iTunes music store is similarly plastered with ads for the streaming service in lieu of what used to be recommendations. But I digress.

These classic devices don't support each others playlist formats, which makes syncing a pain. But we have the solution already in the form of compilation albums, and who says only the record labels can produce them? It's been fun creating my own glorified electronic mix tapes for various genres, including coming up with silly names and cover art to embed in their ID3 tags. Granted these compilations duplicate tracks in a way playlists don't need to, but with my original 15 GB iPod and my Palm Lifedrive upgraded with 128 GiB CF cards, who cares?

That reminds me, I should write about how I upgraded those in the future too. Music is fun!

