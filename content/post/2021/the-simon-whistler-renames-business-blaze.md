---
title: "The @SimonWhistler renames Business Blaze"
date: "2021-08-19T13:19:23+10:00"
abstract: "I just Purched Some Merch, and you should too."
thumb: "https://rubenerd.com/files/2021/blaze-thumb.jpg"
year: "2021"
category: Media
tag:
- brain-blaze
- simon-whistler
- video
- youtube
location: Sydney
---
*Big Brain* and *Certified Legend* Simon Whistler has made the decision to [change the name](https://www.youtube.com/watch?v=4E_MFtFKAgQ) of YouTube's most excellent YouTube channel (allegedly) to *[Brain Blaze](https://www.youtube.com/channel/UCYY5GWf7MHFJ6DZeHreoXgw)*. I was enjoying the dichotomy between "business" and the actual topics Danny ended up writing for Simon each week, but I approve. I'm sure he was waiting with baited breath for that.

<p><img src="https://rubenerd.com/files/2021/blaze@1x.jpg" srcset="https://rubenerd.com/files/2021/blaze@1x.jpg 1x, https://rubenerd.com/files/2021/blaze@2x.jpg 2x" alt="BLAZE!" style="width:500px; height:123px;" /></p>

*(Why would you bait someone's breath? I don't want a bass flying into my mouth from a river or some sh•t. Also, wouldn't putting bait in your mouth taste awful? Better than being stuck in a basement I suppose, without the finest of vintage memes. Danny [sic], chill)!*

I [purched the merch](https://purchthemerch.co) yesterday to celebrate this historic milestone of one of my favourite independent media creators, and to get something with *Business Blazyness* while I still can. You should too. Also, check out [Megaprojects](https://www.youtube.com/channel/UC0woBco6Dgcxt0h8SwyyOmw), one of his many other channels I find delightful that I've mentioned here before. [Geographics](https://www.youtube.com/channel/UCHKRfxkMTqiiv4pF99qGKIw) is another favourite.

Did I just do an unsponsored ad for Simon? Yes. But haughtiness aside, his videos have made living through Covid, quarantines, and lockdown that much more tolerable. I appreciate all the laughs and thought he's brought during These Times&trade; across his three hundred or so channels. [Purch some merch](https://purchthemerch.co), don't make me come over there. Because I'm stuck at home, and it'd be difficult.

<p><img src="https://rubenerd.com/files/2021/business-baze-opening@1x.jpg" srcset="https://rubenerd.com/files/2021/business-baze-opening@1x.jpg 1x, https://rubenerd.com/files/2021/business-baze-opening@2x.jpg 2x" alt="Brand. New. Epsiode. Of. Business. Blaze." style="width:500px" /></p>
