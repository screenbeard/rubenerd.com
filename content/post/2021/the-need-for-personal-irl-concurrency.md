---
title: "The need for personal iRL concurrency"
date: "2021-07-26T08:19:06+10:00"
abstract: "It seems like a great idea to save time in theory, but it also feeds anxiety."
year: "2021"
category: Thoughts
tag:
- personal
- psychology
location: Sydney
---
Concurrency... *con currency*... is that Bitcoin? *Badda boom boom tish! Tish tish! Badda.*

While I'm on a bit of a tear talking about rituals and metal thought processes, I thought I'd also mention something I started to do as a teenager. I'm not sure if programming made me think this way, but even today I have to see pretty much everything I do through the lens of what I can run in parallel.

*(That was supposed to be **mental** thought processes, not **metal**. I can't stand metal music. I'm enamoured with the material in laptops and structural framing, though. I miss Apple hardware back when they used magnesium, that was amazing. But I digress).*

Take the [morning routine](https://rubenerd.com/morning-routines-tech-or-otherwise/) I was rambling about over the weekend. I know that it takes a couple of minutes for the shower to warm up, so I shave my face while waiting. I know it takes me as much time to get dressed as it does to boil water, so I put the kettle on first. The kettle only runs when I've pre-ground the coffee the day before, otherwise I start grinding first, then boil the water while I'm tipping the grounds into the Aeropress so the water is the optimal temperature.

More specifically during Covid Times, I only go down to reception to pick up mail while I'm on my way back from buying groceries, to limit my use of shared lifts and hallways. I do an embarrassing amount of my podcast production and listening while cleaning. I watch engineering and Hololive YouTube while I eat and exercise.

And yes, it spills over to IT. I can't update one FreeBSD jail, I have to do them concurrently in different tabs. Audio-only conference calls&mdash;the best kind&mdash;are when I also mindlessly sort and organise email.

Doing these things concurrently, **and** at the same time (as my illustrious dad would say) presumably came from the desire for efficiency. It doesn't take a rocket surgeon or the sharpest spoon in the drawer to realise you complete a bunch of tasks faster if you do as many of them at once. It's another manifestation of my need to feel like I have control over my environment and circumstances, something that is especially true during These Covid Times.

Unfortunately, it introduces the chance for error. My mental orchestrator is a well-oiled machine when things are predictable and going to plan, but throw a spanner in the works and suddenly all the spinning plates wobble and crash around my feet. The metaphors today are all over the place, like lipstick on a pig. That one didn't even make sense.

There's also a sense of paralysis when I have to make a decision about what to do, because I can't perform tasks in isolation. What **could** I do at the same time morphed into what **should** I do, and now its what **must** I do. Yes, I conform my brain to [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119), like a gentleman. Seeing the world through this rigid framework is frustrating, and leads to dissatisfaction and a sense that I've failed if I only perform a single task.

I'm also starting to realise that the gains I thought I was making from these increasingly complex processes are feeding anxiety in ways I didn't understand until recently. There's a reason mindfulness exists as a concept; there's value in being deliberate, and excelling at one thing before moving onto the next.

Learned habits are hard. There are some things I'll keep doing in tandem, but I need to give myself permission to smell the roses. By themselves, not while attempting to water them or perform gymnastics above them with a conference call going on my phone.
