---
title: "I’m not sure that UNIX won"
date: "2021-05-16T08:22:56+10:00"
abstract: "Linux did."
year: "2021"
category: Software
tag:
- bsd
- freebsd
- linux
- netbsd
location: Sydney
---
I've been reading a *lot* of articles lately proclaiming that UNIX won, and I'm on the fence about whether that's true.

Apple's [Darwin underpinnings](https://www.opengroup.org/openbrand/register/brand3653.htm) aside, most of these articles discuss Linux, and how it powers everything from clouds and virtual machines to embedded devices and phones. Even Microsoft, the company formerly chaired by a classy gentleman claiming Linux was a [malignant cancer](https://www.theregister.com/2001/06/02/ballmer_linux_is_a_cancer/) and funded [dubious TCO research](https://www.crn.com/news/applications-os/18838616/idc-windows-2000-offers-better-total-cost-of-ownership-than-linux.htm), now has the backwards-named Windows Subsystem for Linux.

Except, GNU is not UNIX. Real UNIX has been on the wane for years, most spectacularly with the buyout of Sun Microsystems and the subsequent close-sourcing and snuffing out of Solaris. SGI's IRIX was discontinued two decades ago. SCO and its various phoenixes can't be pushing too many new UnixWares and OpenServers. The likes of IBM AIX and HP-UX are relegated to backroom duties, with Tux taking centre stage by their vendors. And who can forget Xenix.

This leaves Linux, a system Dennis Ritchie [described in 1999](http://www.linuxfocus.org/English/July1999/article79.html) as "draw[ing] so strongly on the basis that UNIX provided". My worry is Linux and the community around it have strayed further from UNIX for a *long* time since, to the point where systemd discussions include [comments like this](https://www.reddit.com/r/linux/comments/h086fd/why_linuxs_systemd_is_still_divisive_after_all/ftkmy04/) with hundreds of upvotes:

> People see Linux as a UNIX-like operating system. This is what it originally was, but it's now outgrowing that legacy. And this upsets some people.

This attitude is perhaps more telling than the OP intended.

My first industrial and educational UNIX experiences were with SunOS. These systems had their own frustrating limitations compared to what I could do on my FreeBSD and Red Hat Linux boxen at home at the time, but part of using them was *learning about UNIX*. Even with the BSDs, I remember reading that great quote that learning FreeBSD made the gentleman a better Linux user. There's value in understanding why specific decisions were made, and why we caution against others.

As the oft-quoted saying goes, those who don't understand UNIX are condemned to reinvent it, poorly. Unfortunately for us, there are now influential people in the industry for whom Linux their only UNIX or UNIX-like experience. Some, like the OP above, see UNIX as a vestige.

The inevitable result of this is a narrowly-focused monoculture. Software is increasingly assuming Linux, and the quaint ideas of POSIX compliance and portability are seen as time-wasting anachronisms. Some of the funnier Freudian slips in systemd bug reports stem from fundamental misunderstandings of UNIX, and the indifference of the developers to it. This should concern you if you understand and value UNIX development philosophies and system design.

I'm relieved Linux is in widespread use; as FreeBSD and macOS users we benefit from its ecosystem and mindshare. Make no mistake, had Microsoft got their way we'd all be running Windows Server today and their about-face with Linux would not have happened. You're also free to think traditional UNIX is antiquated and Linux is the future. Either way, UNIX winning is only a half-truth; at best it was supplanted by an OS that once resembled it more in design, philosophy, and attitudes than it does now.

*(I wrote a [follow-up post to this](https://rubenerd.com/feedback-on-my-not-sure-unix-won-post/) in September 2021. Yes, follow-up post... we'll go with that)!*
