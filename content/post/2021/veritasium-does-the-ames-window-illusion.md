--- 
title: "Veritasium does the Ames Window Illusion"
date: "2021-01-06T09:14:00+11:00"
abstract: "He did a video about another of my favourite things"
category: Media
tag:
- science
- youtube
- video
location: Sydney
---
Last year I raved about [Derek Muller's video](https://rubenerd.com/derek-mullers-video-on-penrose-tilings/) on Penrose Tilings, something I'd been fascinated by since I was a kid. [He just did it again](https://www.youtube.com/watch?v=dBap_Lp-0oc), and it's beautiful. Take some time this morning to watch it if you can :).

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=dBap_Lp-0oc" title="Play The Illusion Only Some Can See"><img src="https://rubenerd.com/files/2021/yt-dBap_Lp-0oc@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-dBap_Lp-0oc@1x.jpg 1x, https://rubenerd.com/files/2021/yt-dBap_Lp-0oc@2x.jpg 2x" alt="Play The Illusion Only Some Can See" style="width:500px;height:281px;" /></a></p>
