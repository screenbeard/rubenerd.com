---
title: "Rejoining, or losing Scotland"
date: "2021-01-12T09:09:40+11:00"
abstract: "From The Economist last Saturday"
year: "2021"
category: Thoughts
tag:
- brexit
- politics
- scotland
- united-kingdom
location: Sydney
---
[From The Economist last Saturday](https://www.economist.com/britain/2021/01/09/declinism-is-booming-in-britain):

> Brexit cuts Britain’s long-term growth potential, reduces its influence in Brussels and Washington, and strains the bonds of the United Kingdom. [..] "Which is the more likely, I’m asking myself: that we lose Scotland, or that we rejoin before we've lost Scotland?" says Michael Heseltine, a former Tory deputy prime minister, who thinks Brexit is a disaster.

