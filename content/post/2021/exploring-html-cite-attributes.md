---
title: "Exploring HTML cite attributes"
date: "2021-06-22T09:08:37+10:00"
abstract: "The cite tag, and cite attribute in blockquotes are there... why aren’t we using them?"
year: "2021"
category: Internet
tag:
- blogging
- html
location: Sydney
---
I was adding missing metadata to my 2005 post archive, like a gentleman, when I [found one](https://rubenerd.com/linux-on-an-ibook-g3) using the <code>&lt;cite&gt;</code> HTML tag which I forgot about entirely! The <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/cite">Mozilla Developer Network</a>:

<blockquote cite="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/cite"><p>The <code>&lt;cite&gt;</code> HTML element is used to describe a reference to a cited creative work, and must include the title of that work. The reference may be in an abbreviated form according to context-appropriate conventions related to citation metadata.</p></blockquote>

I used to wrap these around anchor links everywhere to denote where I had read something, or sourced an image. It adds powerful semantic meaning to links:

<pre>
"Don't quote me on that."
&lt;cite&gt;~ &lt;a href="//example.com"&gt;example.com&lt;/a&gt;&lt;/cite&gt;
</pre>

HTML <code>&lt;blockquote&gt;</code> tags also have a useful inline <code>cite</code> attribute which directly ties a source to the text you're quoting:

<pre>
&lt;blockquote cite="//example.com"&gt;
"Don't quote me on that!"
&lt;/blockquote&gt;
</pre>

I partly blame Markdown for me forgetting about these. When you're used to its limited syntax and output, you naturally gravitate away from these other tools. I'm slowly moving back towards writing HTML for posts; snippets make the work of this easier anyway. I should share these at some point.

The broader and more interesting question though is why there is so much potential supporting infrastructure that isn't being tapped. We don't need social media and their closed networks to establish relationships between people, sites, and media with even just the most basic metadata and semantic markup. It could also help with licencing.

