---
title: "IRyS’s first collaboration with HoloEN"
date: "2021-08-14T10:28:01+10:00"
abstract: "This stream had Clara and I in stitches yesterday!"
thumb: "https://rubenerd.com/files/2021/yt-qPPTKLi9uw8@1x.jpg"
year: "2021"
category: Media
tag:
- amelia-watson
- hololive
- irys
- ninomae-inanis
location: Sydney
---
This stream had Clara and I in stitches yesterday. If you need something lighthearted and silly to end your week, [let it be this](https://www.youtube.com/watch?v=qPPTKLi9uw8)!

<p><a target="_BLANK" href="https://www.youtube.com/watch?v=qPPTKLi9uw8" title="Play 【Gartic Phone】 HoloENana Phone"><img src="https://rubenerd.com/files/2021/yt-qPPTKLi9uw8@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-qPPTKLi9uw8@1x.jpg 1x, https://rubenerd.com/files/2021/yt-qPPTKLi9uw8@2x.jpg 2x" alt="Play 【Gartic Phone】 HoloENana Phone" style="width:500px;height:281px;" /></a></p>

I thought a Garlic Phone was something you order a loaded pizza from, but it looks like Pictonary meets Chinese whispers. Ina's art was absolutely 👌, and I'm glad to see IRyS get along so well.

