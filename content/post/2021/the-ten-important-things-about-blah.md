---
title: "The ten important things about blah"
date: "2021-04-30T21:30:51+10:00"
abstract: "It’s the top ten ways why these lists feel so fake and unreal."
year: "2021"
category: Media
tag:
- advertising
- lists
- pointless
location: Sydney
---
I was walking home this evening and saw a billboard advertising *ten ways to quit smoking*. Another further down the street promised to divulge the *ten steps towards financial independence*. Then just as I got off the train at Chatswood, a video screen shouted in colour and sound to check out *the ten best places to travel in within Australia and NZ while the outside world is unsafe.* Classy! 

Why are lists always in groups of ten? It's either:

* ten discrete things
* the top ten of something
* or ten steps reqired to do something

Except, ten is arbitrary. We think of it as this magical number because we use a base 10 number system (unfortunately), derived from the average number of digits we possess on our hands, feet, and brain hemispheres. Wait, scratch that last one... *with your hands!* But the universe generally doesn't fit within the delightfully deterministic domiciles of decimalised demarcations. Dang.

This is why I don't trust them. Save for the precious few times when there *really are* ten of something, these lists are artificially inflated from a smaller list, or some pruning went on. What was removed? What was added? What isn't *necessary?*

Related to these are alliterative lists. The seven signs of ageing. The four flavours of ribo*Flaven* (glaven). It'd be less satisfying to find out there are five styles of sausage native to a region of Germany and not six. Though I suppose that'd need to be fünf for frankfurters. 

George Carlin's Ten Commandments bit used to be my favourite deconstruction of top ten lists, but Philip Greenspun's [10th rule of programming](http://philip.greenspun.com/bboard/q-and-a-fetch-msg?msg_id=000tgU) is the style I might start adopting for my future ones. Ruben's thirteenth law of shirts: more than five words is too many.

