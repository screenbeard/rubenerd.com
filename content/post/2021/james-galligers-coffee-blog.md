---
title: "James’s (aka capjamesg’s) coffee blog"
date: "2021-06-17T14:31:22+10:00"
abstract: "An independent writer with considered, fun content and an infectious passion for the aforementioned beverages."
year: "2021"
category: Internet
tag:
- coffee
- independent-writers
- people
- reviews
location: Sydney
---
There are still independent writers out there, blogging about cool stuff on their own domain, and with a simple theme that's fast and easy to navigate! Today I'm checking out [James’ Coffee Blog](https://jamesg.blog), which has a [web feed here](https://jamesg.blog/feed.xml). I was going to make a pun about drip-feeding you espresso, but aren't you glad I didn't?

James got in contact with me last year about my posts and podcast episodes on coffee, mugs, and associated comestible apparatuses (apparatii?). He takes the craft of coffee making seriously, with reviews, lists of the best [Scottish coffee roasters](https://jamesg.blog/2021/03/27/scottish-coffee-roasters), and even does interviews with [coffee industry insiders](https://jamesg.blog/interviews/). I like the considered, yet conversational style of his posts, and his infectious enthusiasm. I'll bet the posts they're even more fun in [printed form](https://jamesg.blog/2021/06/04/printed-blog)!

One thing he let me know today is that he's even published an [interactive coffee ratio calculator](https://jamesg.blog/ratio/). I've found you can mostly wing it when it comes to brewing coffee, but you'd be amazed how much more flavour and less bitterness can be extracted with the correct ratios of water, coffee, and grind size. I came to my optimal ratio for my Aeropress machine and the [beans I get shipped](https://cassiopeia.com.au/) from the Blue Mountains through trial and error (and error, and error). This looks like a much more rational approach.

When I eventually (!) get around to publishing my OPML wire service thingy, he'll be in it. And not just because he uses an Aeropress and a Hario V60, the two greatest mechanical devices for extracting caffeinated goodness.

