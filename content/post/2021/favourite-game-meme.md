---
title: "Favourite game meme"
date: "2021-09-14T14:53:25+10:00"
abstract: "Very late to the party, but why not? Parts shamelessly stolen from @_BADCATBAD."
thumb: "https://rubenerd.com/files/2021/favourite-game-meme@1x.jpg"
year: "2021"
category: Software
tag:
- age-of-empires
- commander-keen
- entertainment-pack
- fate
- fate-grand-order
- firewatch
- flight-simulators
- midtown-madness
- minecraft
- persona-5
- portal-2
- simcity
- simcity-3000
- superliminal
- the-sims-2
- the-stanley-parable
- train-simulator
- trains
- xplane
location: Sydney
---
I'm always late with memes, but why not?

<p><img src="https://rubenerd.com/files/2021/favourite-game-meme@1x.jpg" srcset="https://rubenerd.com/files/2021/favourite-game-meme@1x.jpg 1x, https://rubenerd.com/files/2021/favourite-game-meme@2x.jpg 2x" alt="Of all time: SimCity 3000. Story: Atelier Ryza 2. Soundtrack: LEGO Island. Protagonist: Commander Keen. Personal impact: The Stanley Parable. You like, everyone meh: Train Simulator. Everyone likes, you meh: First person shooters. Art style and character design: Persona 5 franchise. Active franchise: Fate. Family memories: Age of Empires II. Always come back to: Microsoft Entertainment Pack. Biggest letdown: SimCity (2013). Bad day cure: Minecraft. After work chill: X-Plane. Childhood: The Need for Speed SE. That atmosphere: Superliminal. Want to try, but haven't yet: Portal 2. Guilty pleasure: Hyperdimension Neptunia (cough)! Indie pick: Firewatch. Way too many hours: The Sims 2." style="width:500px" /></p>

