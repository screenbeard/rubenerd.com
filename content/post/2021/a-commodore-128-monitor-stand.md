---
title: "A Commodore 128 and TED monitor stand"
date: "2021-03-18T08:52:27+11:00"
abstract: "The IKEA SIGFINN is unfortunately too snug."
thumb: "https://rubenerd.com/files/2021/c128-stand@1x.jpg"
year: "2021"
category: Hardware
tag:
- commodore
- commodore-128
- commodore-128-series
- commodore-16
- commodore-plus4
- sailor-mercury
- sailor-moon
location: Sydney
---
In my ongoing [Commodore 128 series](https://rubenerd.com/tag/commodore-128-series/), today I'm talking about something I didn't ever expect to be: how I can set up my desk to use these machines properly!

Whereas the Commodore 64C is [bigger in real life](https://rubenerd.com/on-the-size-of-the-commodore-128/) than I expected, the Commodore 128 is surprisingly smaller. But it's still quite deep relative to its width, which pushes the monitor I want to use with it too far back. This is compounded by all the peripherals and cables that are plugged into the back of the unit, which leaves only a small off-centre gap for the monitor.

With limited table space, a Commodore 1571 drive <del>on the way</del> that just arrived, and my surprise fixing of my C16 and Plus/4&mdash;posts coming!&mdash;I wanted a nice way to use these machines. I've always been intrigued by the [Apple IIe's monitor stand](https://commons.wikimedia.org/wiki/File:Apple_IIc_with_monitor.jpg), which let the machine sit underneath while still preserving airflow over the vents. I thought I could try getting a similar setup for the C128.

I borrowed an [IKEA SIGFINN](https://www.ikea.com/au/en/p/sigfinn-monitor-stand-fixed-height-bamboo-veneer-90388394/) from the office, which was listed as 53 cm wide and 10 cm high, which should have been ample space for the machine. It's a bit snug, but I thought it tied the whole setup together well.

*(Again, don't worry about the missing keycap, I've got a few replacement springs and plungers in the mail, and a new keycap for that broken one).*

<p><img src="https://rubenerd.com/files/2021/c128-stand@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-stand@1x.jpg 1x, https://rubenerd.com/files/2021/c128-stand@2x.jpg 2x" alt="Photo showing the Commodore 128 snugly under an IKEA monitor stand with a 1571 disk drive." style="width:500px" /></p>

But then I realised a few problems. The biggest was that I hadn't centred the monitor for the photo, which bugs me more than it should. And worse, Ami from Sailor Moon was the oldest character I could find to pose on the 1571. I need to get a Lum the Invader fig, or someone else from the 1980s, to be period correct.

Jokes aside, is a phrase with two words. The IKEA website measured the stand's width from the base, and I hadn't accounted for its upward tapering. This results in less clearance than I thought, which isn't a problem for the machine fitting but I'm concerned about air circulation.

But it turned out to be worse than that! Veteran Commodore users could probably tell *immediately* why this wouldn't work: there's no space for the chunky power connector! Not only is the connector itself long and bulky, but the cable that comes out is stiff and difficult to route around.

This sent me down the rabbit hole of right-angled Commodore cables, of which I could only find examples for the C64. The C128 has a bizarre square power connector like the Plus/4, so I wasn't even sure if I could wire up one myself. At the very least I'd need to source a new C128 connector which might be hard, and even then I'm still very much a soldering novice. I'm fine with signal cables, but I care too much about this machine to subject it to a Ruben power cable at the moment.

<p><img src="https://rubenerd.com/files/2021/c128-ted-stands@1x.jpg" srcset="https://rubenerd.com/files/2021/c128-ted-stands@1x.jpg 1x, https://rubenerd.com/files/2021/c128-ted-stands@2x.jpg 2x" alt="Commodore 16 and Plus/4 under the new monitor stand" style="width:500px;" /></p>

The other issue was my 264/TED machines. The Plus/4 fits beautifully, which isn't surprising given its diminutive size. But alas the C16 breadbin *just* pushes against the upper-edge of the stand on both sides, so a VIC-20/VC-20 would too if I'm ever able to source one of those (cough).

(I have more posts about my beloved TED machines in drafts which should come out soon. The C128 is my favourite 8-bit machine of all time, but I have a real soft spot for these as well. 

IKEA have a [few other monitor stands](https://www.ikea.com/au/en/p/elloven-monitor-stand-with-drawer-white-80474783/), but while they're all the right external height, they include drawers which would either rob it of sufficient vertical space, or cut off airflow to the vent. Likewise, [OfficeWorks](https://www.officeworks.com.au/shop/officeworks/search?q=monitor%20stand) have a ton of monitor stands, but they're either black which wouldn't match anything else, are either way too wide or shallow, or are made of cheap plastic.

Clara and I live in an apartment and without access to power tools, so building one is unlikely. I could go down the VESA mount avenue, but I'd prefer the look of a wooden shelf of some sort. I see them in so many photos of people's C128 and C64 setups from back in the day, and I want to emulate it :).

I'm going to keep looking, but if anyone in Australia has any ideas, I'll all ears.

