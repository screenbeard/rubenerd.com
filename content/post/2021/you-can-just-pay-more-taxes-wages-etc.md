---
title: "You can just pay more taxes/wages/etc!"
date: "2021-01-18T08:55:00+11:00"
abstract: "Lifting all boats is defeated by lifting one, check mate?"
year: "2021"
category: Thoughts
tag:
- economics
location: Sydney
---
I had a nightmare last night where someone argued that the safety of a train shouldn't be improved, because you can *just* drive a car instead. Yes, even my dreams have weird interests. Yet that line of thinking is very much real.

When wealthy people advocate for higher tax rates, they're told they can *just* pay extra. When a restaurant owner advocates for higher minimum wages, they're told they can *just* pay their staff more. When someone falls off a cliff, they're told they can *just* avoid being pushed off next time.

They're absolutely true, and absolutely miss the point. *[Absolutely](https://www.youtube.com/watch?v=iCJLOXqnT2I)!* The opportunity to lift all boats isn't challenged by *just* lifting one. It's basic arithmetic, though it eludes armchair economists.

I'm also starting to become wary of the word "just". It's a sign that someone's about to belittle or downplay an issue. That's not very just.
