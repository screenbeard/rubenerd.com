---
title: "Formatting SD cards in the Nintendo Switch"
date: "2021-12-28T14:48:48+11:00"
abstract: "Home, System Settings, System, Formatting Options"
year: "2021"
category: Hardware
tag:
- guides
- nintendo
- switch
location: Sydney
---
As of system software 13.2.0, current for December 2021:

1. Go to the **Home** screen, and tap **System Settings**
2. Scroll to the very bottom, and tap **System**
3. Scroll to the very bottom, and tap **Formatting Options**
4. Tap on **Format microSD card**

Every guide I read was either wrong, or missed steps. Maybe an update rearranged things, or changed menu text.

Also remember that SD cards aren't plug-and-play on the Switch, probably because there's no clean way in the UI to unmount them.
