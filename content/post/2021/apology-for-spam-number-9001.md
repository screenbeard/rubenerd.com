---
title: "Apology for spam #9001"
date: "2021-01-30T10:22:57+11:00"
abstract: "My git hook didn’t fire again, sorry about that."
year: "2021"
category: Internet
tag:
- weblog
location: Sydney
---
My git hook didn't fire, so all my posts since last Wednesday just generated at once. Apologies for the spam to your [RSS reader](https://rubenerd.com/feed/) and [Twitter](https://twitter.com/Rubenerd_Blog).

In other news, I'm working on putting my blog on a dynamic CMS again for specific reasons I'll enumerate on at some point. Enumerate on? That'll stop this from happening <del>unless my cache gets stale</del>.

