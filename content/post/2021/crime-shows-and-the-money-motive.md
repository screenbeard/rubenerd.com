---
title: "Crime shows, and the money motive"
date: "2021-03-03T22:36:59+11:00"
abstract: "Yes, I’d be willing to pay for it."
year: "2021"
category: Thoughts
tag:
- ethics
- philosophy
location: Sydney
---
Clara and I watch a lot of those true crime shows. My mum and I used to watch the Discovery Channel's *Crime Night* on Friday evenings growing up, just around the time CSI was taking off. Over time I've found more and more things that are, for want of a better word, problematic. They raise important questions, but this post just observes one of them.

*Money is the motive for most of these crimes*.

In which case, what if everyone had a baseline of income, either a [UBI](https://en.wikipedia.org/wiki/Universal_basic_income) or similar? What if food and shelter weren't an issue? If people had access to free mental and physical healthcare, a good education, and a feeling of safety? I guess what I want to know is: could some of this be preventable?

To be clear, I'm making no excuse for these people. Most of them are sociopaths who can't be reasoned with, as best we understand. I'm already not sure whether I should even be posting this. My compassion and empathy have also been exploited by people my whole life which I should learn from. But they're still my core values.

If just *one* person, or ten percent of those who'd otherwise be driven to crime, could be dissuaded by the stabilising influence of a regular income, I say it's worth trying. It could end up being the best investment in society we've ever made.

And yes, I'd be willing and honoured to pay for it. Even if I'm still working at my full time job and they're not working at all. Because the alternative is to cast people out, then spend money investigating and prosecuting them later.

