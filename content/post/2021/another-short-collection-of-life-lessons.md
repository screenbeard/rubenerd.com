---
title: "Another short collection of life lessons"
date: "2021-08-01T12:55:09+10:00"
abstract: "Now with exploding laksa."
year: "2021"
category: Thoughts
tag:
- life-lessons
location: Sydney
---
In no particular order this Sunday evening:

* Always use microwave handles instead of stop buttons. A mechanical override to prevent a literal stack overflow will always be faster and more reliable than a soft button. That is, unless you like cleaning exploded porridge or laksa.

* You don't always need to agree with someone to find their other points valid or interesting. Twitter has trained us to think we can't.

* Sometimes it *is* worth crying over spilled milk. Maybe you've had a rough day and you need the release. Bottling it up and putting it back in the fridge isn't healthy.

* Always check your pants leg to ensure they're not tucked into your socks. Unless that's your style, in which case... always check your pants leg to ensure they're not tucked into your socks.

* Experiences are usually, but not always, more valuable than things.

* The Friday Night Rule preventing new code deployments should also apply to First Thing Monday Morning, despite being the furthest from another Friday Night. You don't need to start your week with a broken build or offline server.

* Don't take a mid-thirties guy on the Internet seriously, especially if he write lists in no particular order.

