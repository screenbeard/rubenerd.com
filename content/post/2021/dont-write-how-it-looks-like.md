---
title: "Don’t write “how it looks like”"
date: "2021-03-24T14:32:03+11:00"
abstract: "It’s a common mistake, and I empathise."
year: "2021"
category: Media
tag:
- language
location: Sydney
---
I encounter a lot of people in technical writing circles who stumble on this phrase, which can unfairly affect professional perception:

> How does it look like?

This isn't grammatically correct, and sounds clumsy to native speakers. We'd likely say:

> *What* does it look like?  

This invites you to describe what you're seeing, or to compare it to something else. You might reply with "it looks like a forest with green trees" or "it looks just like the Black Forest in Germany".

Another form is:

> How does it *look?*

This is more of a judgement or a summary. You might say "it looks pretty".

I empathise with people learning English. Granted it makes more sense than *some* other European languages, but it still has its quirks.

