---
title: "Rhyming socks"
date: "2021-04-23T12:59:17+10:00"
abstract: "He’ll be darned if he does."
year: "2021"
category: Thoughts
tag:
- pointless
- quotes
location: Sydney
---
I read this in the wrapping for my lunch today:

> Said the toe to the sock: "Let me through, let me through!"
>
> Said the sock to the toe: "I'll be darned if I do".

I really stepped in that one. Toed the line. Feet to the fire. A shoe-in. Sock it to me! Nailed it. Heel... hmm, lost it. *Sandals!*
