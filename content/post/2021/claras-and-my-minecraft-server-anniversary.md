---
title: "Clara’s and my Minecraft server anniversary"
date: "2021-10-13T20:36:00+10:00"
abstract: "Having a virtual cake in the park :')."
thumb: "https://rubenerd.com/files/2021/minecraft-1-year-anniversary@1x.jpg"
year: "2021"
category: Thoughts
tag:
- games
- minecraft
- personal
location: Sydney
---
We've been building this world together since the 13th of October last year. It's been a wonderful Covid distraction and evening chill activity.

Pictured below is our anniversary cake in the park from this evening, and a map of our peninsula. We've been avoiding chopping down trees or levelling the land as much as possible, which has lead to all sorts of boardwalks, bridges, and underpasses to get around. Fun!

<p><img src="https://rubenerd.com/files/2021/minecraft-1-year-anniversary@1x.jpg" srcset="https://rubenerd.com/files/2021/minecraft-1-year-anniversary@1x.jpg 1x, https://rubenerd.com/files/2021/minecraft-1-year-anniversary@2x.jpg 2x" alt="Clara (right) and I having cake!" style="width:500px" /><img src="https://rubenerd.com/files/2021/minecraft-1-year-anniversary-map@1x.jpg" srcset="https://rubenerd.com/files/2021/minecraft-1-year-anniversary-map@1x.jpg 1x, https://rubenerd.com/files/2021/minecraft-1-year-anniversary-map@2x.jpg 2x" alt="Map of our world in the town square." style="width:500px" /></p>

Thanks to the [Hololive characters](https://www.youtube.com/playlist?list=PLgssf7mV7dhjBzFuYi4evDC3KtzrMdVAw) for finally getting us into this. They may as well rebrand themselves as Holocraft given all the hundreds of hours they've played even in the leadup to the server merge! Which would be fabulous.

