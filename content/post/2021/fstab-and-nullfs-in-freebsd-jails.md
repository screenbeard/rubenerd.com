---
title: "fstab in FreeBSD jails"
date: "2021-02-10T21:07:53+11:00"
abstract: "In sharing my silly mistake, you can avoid being as silly."
year: "2021"
category: Software
tag:
- bsd
- containers
- freebsd
- jails
location: Sydney
---
This was a silly mistake I made. In sharing it, maybe you'll avoid being as silly. Read up on my [introduction to FreeBSD jails](https://rubenerd.com/starting-with-freebsd-jails/) featuring Gawr Gura if you need context.

I had defined an `fstab` for my jail called `ina`, with some nullfs mounts. This lets you pass in a mounted device or volume into the jail. In this case I'm using ro to use as read-only; you can just as easily do this with rw for rewriteable:

    # cat /etc/fstab.ina   
        
    ==> # Device  Mountpoint        FStype  Options  Dump  Pass#
    ==> /zpoolx   /jail/ina/zpoolx  nullfs  ro       0     0

So imagine my surprise when I booted ina and there was nothing there:

    # service jail start ina
    # jexec ina /bin/sh
    # ls /zpoolx
    # ==> *crickets*

*Turns out* that I hadn't added the appropriate `fstab` option to the jail-specific config in my `jail.conf` file.

    ina {
        ip4.addr="10.8.8.88"
        mount.fstab="/etc/fstab.ina"
    }

Ruby on Rails was the first place where I read *convention over configuration*, and I've held it near and dear since. As much of my `jail.conf` is consistent with the name of the jail for things like its path and hostname:

    path="/jail/${name}";
    host.hostname="${name}.lan";

But while I use a convention of `/etc/fstab.${name}` for each jail, I define it manually within each jail's config. Not all jails have extra mountpoints.

Maybe I could define a generic mountpoint line as I did for `path` and `host`, and use empty fstab files in those cases. Those are still valid, even if it does feel a bit like a hack.

