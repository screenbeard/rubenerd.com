---
title: "Krita version 5"
date: "2021-12-30T09:09:09+11:00"
abstract: "Everyone’s favourite graphics editor reached a major milestone yesterday."
year: "2021"
category: Software
tag:
- kde
- krita
location: Sydney
---
It felt like Christmas came early this year with the [release of Krita 5](https://krita.org/en/krita-5-0-release-notes/)!

I'm not a digital artist, as evidenced by anything I've posted here. Krita is first and foremost a digital art tool, but I've found it invaluable for graphics and photo editing as well, especially projects that require layering and transformations. You can [download it for all the major OSs](https://krita.org/en/download/krita-desktop/) here. I'm sure it'll be in package managers soon, too.

If I had *one* quality-of-life wishlist for the software though, it would be more consistent and foolproof grid and guide snapping regardless of layer type. Emphasis on the *fool* in foolproof! A year into using Krita as my primary graphics editor, I'm still utterly confounded about how it works; most of the time I give up.

I suspect it might be a case of me using the tool wrong, or expecting it to do something for which it wasn't designed... refer to my comment above about not being an artist. But The Gimp, Pixelmator, and Acorn let me align and snap layers without a second thought.
