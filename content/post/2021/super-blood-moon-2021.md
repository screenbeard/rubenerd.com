---
title: "Super blood moon, 2021"
date: "2021-05-27T10:03:44+10:00"
abstract: "Wow :')"
thumb: "https://rubenerd.com/files/2021/202105-moon@1x.jpg"
year: "2021"
category: Media
tag:
- astronomy
- photos
location: Sydney
---
Wow :').

<p><img src="https://rubenerd.com/files/2021/202105-moon@1x.jpg" srcset="https://rubenerd.com/files/2021/202105-moon@1x.jpg 1x, https://rubenerd.com/files/2021/202105-moon@2x.jpg 2x" alt="Bad cameraphone photo taken in the moment." style="width:500px; height:333px;" /></p>
