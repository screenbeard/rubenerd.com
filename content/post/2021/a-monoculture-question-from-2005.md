---
title: "A monoculture question from 2005"
date: "2021-04-01T21:38:36+11:00"
abstract: "A post that I’d say aged poorly, but didn’t even really make sense when it came out."
year: "2021"
category: Software
tag:
- business
- monoculture
location: Sydney
---
All issues are transparent with the power of hingsight; but this question posed for [Real World IT in ZDNet](https://www.zdnet.com/article/is-the-fear-of-the-monoculture-genuine/) wouldn't have even made that much sense in 2005:

> I have a very simple question [for those] who either propagate or agree with the concept that monoculture is dangerous.

Okay, shoot :)

> If an organization was running all Linux on their desktops and servers, would you tell them that they have a big monoculture problem and they should immediately convert half of their desktops and servers to Windows XP and Windows 2003 in the name of cyber diversity?

No. Next?

> Monoculture is being singled out because it is seen as one of the key advantages of the incumbent -- which in this case is Microsoft. The efficiencies of monoculture are so obvious that few organizations actually try to deviate from it. You would be hard-pressed to find a single CIO or IT Director who would go against the grain and choose to double their desktop complexity and associated support costs.

Conflating industry monoculture with that of a company installation is quite the stretch. I'm glad this train of thought went the way of my teenage angst. Which I was still living with back then.

*Update: The original post wasn't posted on April Fools. Although, this post was. So I could be lying about the original post. Or maybe this one. What about this one, but not that one? Which one am I? Is this what people read my blog for?*

