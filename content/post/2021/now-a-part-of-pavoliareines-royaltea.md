---
title: "I’m now a part of @pavoliareine's Royaltea!"
date: "2021-05-03T22:23:42+10:00"
abstract: "Terima kasih :)"
thumb: "https://rubenerd.com/files/2021/screen-reine@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- ninomae-inanis
- pavolia-reine
- youtube
location: Sydney
---
<p><a href="https://rubenerd.com/files/2021/screen-reine@orig.jpg"><img src="https://rubenerd.com/files/2021/screen-reine@1x.jpg" srcset="https://rubenerd.com/files/2021/screen-reine@1x.jpg 1x, https://rubenerd.com/files/2021/screen-reine@2x.jpg 2x" alt="Screenshot from YouTube showing Reine and Ina's Minecraft collab stream" style="width:500px; height:248px;" /></a></p>

Salamat malam! Hot on the heels of *finally* [signing up for Ina](https://rubenerd.com/im-in-inas-tentacult/), Clara and I are now supporters of [Pavolia Reine](https://www.youtube.com/channel/UChgTyjG-pdNvxxhdsXfHQ5Q), our favourite Hololive Indonesia member. Her [stream with Ina](https://www.youtube.com/watch?v=9qW-D7OnyKg) basically sealed the deal! ♡

Terima kasih :). I miss Indonesia something fierce, I hope to head back there again someday. 🇮🇩

