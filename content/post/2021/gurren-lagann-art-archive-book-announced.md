---
title: "Official Gurren Lagann art book announced"
date: "2021-07-01T14:00:54+10:00"
abstract: "This is the most dangerous news I’ve read in weeks."
thumb: "https://rubenerd.com/files/2021/71DYqHdLxOS@1x.jpg"
year: "2021"
category: Anime
tag:
- art
- books
- gurren-lagann
- shopping
location: Sydney
---
This is the *[most dangerous news](https://www.famitsu.com/news/202106/30225319.html "Article in Famitsu")* news I've read in weeks. Everyone's favourite drill-piercing anime from the 2000s is getting a new 488-page art book with over 350 illustrations. It has all the CD and DVD covers, character designs, image boards, and those that made it into anime magazines like Newtype.

<p><img src="https://rubenerd.com/files/2021/71DYqHdLxOS@1x.jpg" srcset="https://rubenerd.com/files/2021/71DYqHdLxOS@1x.jpg 1x, https://rubenerd.com/files/2021/71DYqHdLxOS@2x.jpg 2x" alt="Cover of the new official archive art book... cries in wallet!" style="width:367px; height:512px;" /></p>

It's available for pre-order on the [Anime Style Online Shop](https://animestyle-shop.sakura.ne.jp/main/?p%3D120), and Amazon Japan, to be shipped on the 10th of August this year. Remember when anime figs were released a short time after pre-orders too, and not five hundred years later!?

I [blogged obsessively](https://rubenerd.com/tag/gurren-lagann/) about this series a decade ago. There's a reason anime fans consider it a seminal work!

