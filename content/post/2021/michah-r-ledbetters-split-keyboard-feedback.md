---
title: "Micah R Ledbetter’s ErgoDox feedback"
date: "2021-08-04T16:23:15+10:00"
abstract: "It looks like a great split-keyboard option, but having two physical pieces is a downer."
year: "2021"
category: Hardware
tag:
- ergonomics
- keyboards
location: Sydney
---
Last month I wrote about getting a new [Microsoft Ergonomic Keyboard 2019](https://rubenerd.com/the-microsoft-ergonomic-keyborad-2019/) keyboard that's a keyboard Microsoft released in 2019... keyboard. I got a flurry of comments which I summarised in a [feedback post the next day](https://rubenerd.com/feedback-about-the-microsoft-ergonomic-keyboard/).

[Michah R Ledbetter](https://me.micahrl.com/blog/) wrote in to let me know about the Ergodox for those in a market for split keyboards:

> I recommend an ErgoDox, IF you have typing pain that was like mine.
> 
> The thing is, assembled ErgoDox boards are really expensive! I agonized over it for a long time before deciding to pull the trigger. It turned out to be worth it, though - it was instrumental in making my typing pain-free again. I know different people's RSI is different; for me, typing with pinkies had become extremely painful, and moving keys like enter, control, and shift to under the thumbs was a huge relief.

I'm relieved to hear! Most of my pain was in my wrists, so having the split in this Microsoft unit already feels so much better. But I know people who deal with joint pain in their fingers; it sounds like as much fun as living with someone who types on an IBM Model M (I say this as someone who used to, but lives in much closer quarters with a significant other than he did before)!

But I digress. Ruben, getting distracted? *NEVER*.

I took a look at the [ErgoDox EZ keyboard](https://ergodox-ez.com/) Micah recommended, and it's on a whole other level. The configuration screen reminded me of the CODE keyboard; it was fun window shopping a  white board, no lights, printed keycaps, black wristpads, and Kailh Brown switches I've always wanted to try as an MX Brown afficionado:

<p><img src="https://rubenerd.com/files/2021/ergodox.png" srcset="https://rubenerd.com/files/2021/ergodox.png 1x, https://rubenerd.com/files/2021/ergodox.png 2x" alt="The Rubenerd ErgoDox keyboard!" style="width:500px" /></p>

*(For those interested in the middle wing keys, Micah also [wrote a great visual tool](https://keymap.click/) to demonstrate how customisable they and the rest of the board are. I already have a few ideas about what I'd map them to too. To two? Two tutu? Too to... English is weird).*

The same team also sell a [Moonlander keyboard](https://www.zsa.io/moonlander/) that I only found about today. It looks like an evolution of the ErgoDox, with a more refined design. It shares the same *ortholinear* key laoyout as the ErgoDox, which reduces the contortions and stretching each finger needs to perform when typing.

But here's where my primary reservation comes in. Both devices have two physically-seperated pieces, one for each hand. This is great for getting the initial positioning right, but they're prone to shifting around if you're as animated a typist as I am. It was among the reasons I returned my Kinesis board a few years ago; it drove me batty constantly having to shift and rearrange each piece. Maybe I could sketch and 3D print a board that each side would slot into, or take a piece of wood and make a few indentations for the feet of each board piece. But then, that's a lot of extra work to accomodate a device that's already pricier than some of my laptops.

Realistically, is a word with fourteen letters. I'm just fine with my Microsoft split keyboard for now, but it's good to know that there's an avenue to mechanical joy if I ever want or need to tread that path again. "Mechanical joy" was not something I ever thought I'd write, and it sounds mildly terrifying.
