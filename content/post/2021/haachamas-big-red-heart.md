---
title: "Music Monday: Haachama’s Big Red Heart"
date: "2021-02-08T16:09:51+11:00"
abstract: "Damn it!"
thumb: "https://rubenerd.com/files/2021/yt-A9HY4DsRTCg@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- music
- music-monday
location: Sydney
---
I [needed](https://rubenerd.com/that-grumpyness-spiral/) this [Music Monday](https://rubenerd.com/tag/music-monday/)! I can't even this.

What has Hololive done to me? Don't answer that, either.

<p><a href="https://www.youtube.com/watch?v=A9HY4DsRTCg" title="Play 1st Single 【 REDHEART 】"><img src="https://rubenerd.com/files/2021/yt-A9HY4DsRTCg@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-A9HY4DsRTCg@1x.jpg 1x, https://rubenerd.com/files/2021/yt-A9HY4DsRTCg@2x.jpg 2x" alt="Play 1st Single 【 REDHEART 】" style="width:500px;height:281px;" /></a></p>
