---
title: "A 1 TB DIMM, with a clock speed of 2!"
date: "2021-09-18T11:46:48+10:00"
abstract: "I love looking at technical specifications on Amazon, even if I don’t buy from there."
year: "2021"
category: Hardware
tag:
- funny
- memory
- pointless
- shopping
location: Sydney
---
Sometimes I look for reviews and details on Amazon, even if I avoid buying from there if I can. The technical details [for this DIMM](https://web.archive.org/web/20210918015657/https://www.amazon.com/Kingston-16GB-DDR4-2666MHZ-Module/dp/B078WWLQ6K) were great:

> Computer Memory Size: 1 TB   
> Memory Clock Speed: 2   
> Memory Speed: 2666 MHz   
> Memory Storage Capacity: 16 GB

Sometimes I feel like my memory clock speed operates at 2, *and it ain't ECC!*
