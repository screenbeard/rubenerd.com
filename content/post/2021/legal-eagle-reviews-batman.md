---
title: "Legal Eagle reviews Batman"
date: "2021-09-04T20:52:12+10:00"
abstract: "Heh. Heh heh."
thumb: "https://rubenerd.com/files/2021/legal-eagle-batman3@1x.jpg"
year: "2021"
category: Media
tag:
- legal-eagle
- reviews
- videos
- youtube
location: Sydney
---
This has become one of Clara's and my [favourite episodes](https://www.youtube.com/watch?v=JNX6ybKqlqA) of his. This was a proximate cause of mirth.

<p><img src="https://rubenerd.com/files/2021/legal-eagle-batman1@1x.jpg" srcset="https://rubenerd.com/files/2021/legal-eagle-batman1@1x.jpg 1x, https://rubenerd.com/files/2021/legal-eagle-batman1@2x.jpg 2x" alt="Batman: Catching them? Don’t bother." style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/legal-eagle-batman2@1x.jpg" srcset="https://rubenerd.com/files/2021/legal-eagle-batman2@1x.jpg 1x, https://rubenerd.com/files/2021/legal-eagle-batman2@2x.jpg 2x" alt="View out the window of a perpetrator, strung up and hanging." style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/legal-eagle-batman3@1x.jpg" srcset="https://rubenerd.com/files/2021/legal-eagle-batman3@1x.jpg 1x, https://rubenerd.com/files/2021/legal-eagle-batman3@2x.jpg 2x" alt="Heh. Heh heh." style="width:500px; height:281px;" /></p>
