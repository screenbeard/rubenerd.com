---
title: "Whimsy and the new coloured iMacs"
date: "2021-04-22T03:46:28+10:00"
abstract: "This machine makes too many compromises, but it’s good in one way."
thumb: "https://rubenerd.com/files/2021/imac2021@1x.jpg"
year: "2021"
category: Hardware
tag:
- cp
- apple
- colour
- design
- imac
- quotes
- nostalgia
location: Sydney
---
I just discovered [Terraaeon's Misc Stuff site](http://misc-stuff.terraaeon.com/articles/different-looking-laptops.html), where he laments the bland "me too" attitude of computer manufacturers:

> I am tired of nearly every new laptop computer looking like a MacBook Air clone [..] What is it about large companies that makes them nearly incapable of creating a fun, interesting, and nice-looking laptop with a unique personality? I can't argue that MacBook Airs are not great-looking laptops, but does nearly every laptop made in the last eight years really have to look like one?

You can see what he means just by walking down the isle of any electronic retailer today. It's obvious computer companies would rather be making phones; the computers they make look largely the same. They're basically uglier, plastic Macs with the same island keyboards wed to a nasty screen and imprecise trackpad (though we all know ThinkPad TrackPoints are the best)!

But even the Mac has been boring for a long time. Not from a technical perspective, but they've had no personality, charm, or what I've started saying again: *whimsy*. It's one of the things that makes computing fun. So many technical people and engineers see no point in whimsy; if it can't be distilled into a feature spec sheet, it's meaningless fluff. Or as that Amiga marketing critic once said, it's like arguing that sushi is just raw fish. The nutrients are the same as what I can get frozen at the supermarket; anyone who goes to a sushi train is a sucker for advertising.

<p><img src="https://rubenerd.com/files/2021/imac2021@1x.jpg" srcset="https://rubenerd.com/files/2021/imac2021@1x.jpg 1x, https://rubenerd.com/files/2021/imac2021@2x.jpg 2x" alt="Press photo of the new coloured iMacs by Apple" style="width:500px; height:216px;" /></p>

Which is why, for all the shortcomings, compromises, and design flaws of the new coloured iMacs, I'm happier it exists over than the machine it replaced.

I got my first blueberry iMac DV back in the late 1990s because it was *fun*. Also because I discovered years too late than classic Mac OS kicked the pants off Windows, but that's for another post! It was *so cool* having this fun piece of tech sitting on my table. I felt the same way about those funky coloured SGI machines, those Iomega drives, heck even that gorgeous purple of old Sun Microsystems kit. The G3 CPU in that machine was *probably* better than the Pentium tower it replaced; its Gigabit Ethernet and USB ports *definitely* were.

In the paraphrased words of Keith from the TryGuys talking about Taco Bell, why do some things in IT have to look like... all of it? Why does everything have to be grey metallic (or in the case of PC hardware, metallic-coloured plastic) or black? Why must the only alternative be tasteless, RGB gamer kit? Okay, a bit of my own biases are coming through there, but my point is there's value in how a computer looks to people in the real world. If it didn't matter, we'd all just be driving Corollas and eating nothing but salads. We may even be better for it, but what's the point of life without having a bit of fun, too?

I'm not a fan of 2021 Apple. Their hardware is disposable; not as recyclable as they claim, nor is it user serviceable. The UI of Big Sur continues the precipitous decline in macOS usability and design. Even the latest batch of iPhones can't be used by a significant amount of people owing to OLED photosensitivity. Apple writers who lamented the state of the company during the butterfly keyboard days seem to be all on board with this new direction though, so I know that I'm in the minority here.

But! A *bit* of that inner child that bought that first iMac smiled today for the first time in a while. I hope the PC industry does what it's always done well: copy this, but with a few more intelligent design decisions. They can start with not shipping crappy screens.

If I had a billion dollars, I'd buy the rights to SGI from HPE, and make modular, coloured computers. Surely Apple can't be the only company with industrial design chops making UNIX workstations in 2021.
