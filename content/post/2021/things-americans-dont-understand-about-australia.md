---
title: "What Americans don’t understand about Australia"
date: "2021-06-11T14:55:41+10:00"
abstract: "The Tim Tam Slam wasn’t invented by an Australian, though."
year: "2021"
category: Thoughts
tag:
- australia
- language
- united-states
location: Sydney
---
Brenda Beenken summarised [twenty things Americans don't know about Australia](https://www.papercut.com/blog/20-things-this-american-didnt-know-about-australia/) for PaperCut's blog. It's fun reading things written from an outsider looking in; I had to relearn so much of this when I moved back from Singapore myself. At least Singapore and Australia both use Commonwealth spelling.

I can't fault almost any of her list. Except point 1, you're more likely to hear "hey mate" thesedays. I also don't think an Australian invented the *Tim Tam Slam*, just like none of us had ever heard of *bloomin‘ onions* until American TV shows starting talking about a fast food chain over there.

I also would have added *root* among things you probably don't want to say over here. Rooting for someone in America is wishing them success. Rooting in Australia is to fornicate, which perhaps you might want to be less liberal about suggesting in public.

The American predilection for `month/day/year` dates continues to baffle me too. The `day/month/year` everyone else uses also isn't as good as [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html), but at least it logically ascends.

That's not to say Australians are always logical. Singapore uses floor 1 to refer to floor 1, like the US. Australian buildings insist on floor 1 being *ground*, which sounds fine until you realise they insist on floor 2 being floor 1. Therefore, a ten-story building will go up to floor 9. At the very least, *ground* should replace floor 1, not shift them up.

