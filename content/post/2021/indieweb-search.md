---
title: "James Gallagher’s IndieWeb Search"
date: "2021-10-15T08:12:11+10:00"
abstract: "A valuable resource for searching for IndieWeb content"
year: "2021"
category: Internet
tag:
- indieweb
- weblog
location: Sydney
---
Friend of the site and [coffee blogger](https://jamesg.blog/) James Gallagher emailed a month ago (thanks spam filters!) with a new project he's launched: [IndieWeb Search](https://indieweb-search.jamesg.blog/).

> It's a search engine for independent websites (mainly listing people who participate in the IndieWeb community, but others are listed too).

For all my talk of independent and small-scale writing and syndication, I haven't got anywhere near as involved in the [IndieWeb](https://indieweb.org/) as I should. This looks to be another great resource; it's now in my [daily websites](https://rubenerd.com/omake/#daily-websites) list :).
