---
title: "Rhett and Link on boolean algebra"
date: "2021-06-09T22:13:37+10:00"
abstract: "I’m just starting to think everything is true."
thumb: "https://rubenerd.com/files/2021/rhett-link-kfc-01@1x.jpg"
year: "2021"
category: Media
tag:
- good-mythical-morning
- video
- youtube
location: Sydney
---
From one of their [recent Good Mythical Mores](https://www.youtube.com/watch?v=_loclvk2Kj4)\:

<p><img src="https://rubenerd.com/files/2021/rhett-link-kfc-01@1x.jpg" srcset="https://rubenerd.com/files/2021/rhett-link-kfc-01@1x.jpg 1x, https://rubenerd.com/files/2021/rhett-link-kfc-01@2x.jpg 2x" alt="Link: I'm just starting to think everything is true" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/rhett-link-kfc-02@1x.jpg" srcset="https://rubenerd.com/files/2021/rhett-link-kfc-02@1x.jpg 1x, https://rubenerd.com/files/2021/rhett-link-kfc-02@2x.jpg 2x" alt="except for the one that was false." style="width:500px; height:281px;" /></p>

