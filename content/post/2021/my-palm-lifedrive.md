---
title: "My Palm LiveDrive!"
date: "2021-04-25T16:18:13+10:00"
abstract: "She just arrived from eBay. My teenage inner-child is unreasonably excited."
thumb: "https://rubenerd.com/files/2021/lifedrive-yoko@1x.jpg"
year: "2021"
category: Hardware
tag:
- lifedrive
- nostalgia
- palm
- pdas
location: Sydney
---
*Gurren Lagann* was a seminal anime series that shook my world when I got around to watching it. Released in 2007 by Gainax, it set the state for Studio Trigger and is still highly regarded today for its themes, art style, comedy, and gripping story. 

Only two years prior, Palm released their LifeDrive device. Other Palms were produced after it was discontinued, but it was the technical pinnacle of a platform that had started with the original Pilot in the early 1990s. It ran PalmOS Garnet on a 416 MHz Intel XScale CPU, with a 320×480 display and a sleek case design that I think has aged especially well. It had built-in 802.11b Wi-Fi, a toggle switch for portrait or landscape operation, a dictation button, SD card slot, and a headphone jack.

<p><img src="https://rubenerd.com/files/2021/lifedrive-yoko@1x.jpg" srcset="https://rubenerd.com/files/2021/lifedrive-yoko@1x.jpg 1x, https://rubenerd.com/files/2021/lifedrive-yoko@2x.jpg 2x" alt="Photo of me holding the Palm LifeDrive next to a Pieces of Sweet Stars fig of Yoko from Gurren Lagann." style="width:500px; height:333px;" /></p>

But it was its target market and storage that set it apart from other Palm devices and PDAs. It was billed as including all the media one might need during the day, wedding the Palm's legendary PIM tools with office software, photos, and video. I used to think of it as the child of a classic iPod and a Palm TX.

It wasn't a market success. People were willing to excuse the slower performance of Microdrives on iPods for the huge jump in storage, but it made interactive devices like the LifeDrive feel sluggish and unresponsive. It was also quite a bit thicc'er (cough) to accomodate the Microdrive and the larger battery it required, making it less pocketable. Reviews from the time said it made too many compromises to be useful, which I can't fault. Emerging smartphones, including the ones Palm sold, probably didn't help.

<p><img src="https://rubenerd.com/files/2021/livedrive-back@1x.jpg" srcset="https://rubenerd.com/files/2021/livedrive-back@1x.jpg 1x, https://rubenerd.com/files/2021/livedrive-back@2x.jpg 2x" alt="The back of the LifeDrive, showing its thicc frame." style="width:500px; height:333px;" /></p>

This didn't stop me wanting one! I was in the Palm ecosystem for years by the time the LifeDrive came out, and the idea of having all that capacity with the familiar PalmOS environment was *hugely* appealing. I already had an iPod at the time though, and still clung to my Tungsten W as my Palm smartphone, so I couldn't justify saving my meagre income at the time to get one.

Today I suppose the closest we have is the iPod Touch, or the Android-based Sony Walkman. I miss when hardware manufacturers tried new and cool things, but that's for another post.

A decade and a half later, I posted this teaser about [Palm nostalgia](https://rubenerd.com/palm-pilot-nostalgia-on-its-way/):

> [..] I’ve had some saved eBay searches and price ceilings up for a few years, and have got some replacements for my long-lost and stolen original Palms in the post. I even managed to score a Grail that I always wanted growing up, but could never afford.

She arrived on Friday from the gentleman in Sydney who bought her at the Hong Kong airport new back in 2005! She's in immaculate condition, and comes with two stiluses (stilii?), the LifeDrive Palm Desktop software, the original USB HotSync cable, and the wall charger.

My next step is to take the Microdrive out, image it, and replace it with a higher-capacity CompactFlash card. This looks to be a common mod, and gives the device performance comperable to other Palms at the time. Only with more storage!

Now I just need my old Palm IIIx back, and I can do a family reunion.

