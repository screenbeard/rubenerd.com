---
title: "The most dangerous things in tech"
date: "2021-06-08T21:52:53+10:00"
abstract: "... is hubris!"
year: "2021"
category: Internet
tag:
- blogging
- people
- tech
location: Sydney
---
I saw this question floating around *both* on Mastodon and The Bird Site yesterday, so I took it as a sign. This was also written *before* the Fastly incident!

The most dangerous things in tech, for personal health and safety, are malformed process control system instructions without sufficient error handling. You don't want to tip  molten steel onto a worker below because you miscalculated where an inlet  was, or accidentally fire an ICBM, or hit someone in the face with a robotic arm. These have all happened, and I'll bet more often than we'd like to think.

But if we broaden our definition of dangerous, *is a sentence fragment with eight words!* I come back to my recurring theme here that **hubris is the most dangerous thing in tech**. Here are a but a few examples:

* Everyone from UI designers to system architects thinking they know better and can discard lessons from the past. Electron, the most recent versions of macOS, and systemd come to mind, but I'm sure we could all cite dozens more.

* Blockchain pyramid schemes, and those who spruik them. They're only useful for fleecing and extortion.

* Governments thinking that mandated cryptographic backdoors can be both mathematically feasible and impervious to exploitation.

* Sales teams that claim 100% uptime, or full fault-tolerance.

* Managers who claim the tech sector is a functional meritocracy. I'm surprised at how pervasive this still is, though I probably shouldn't be!

* Silly people still blogging in 2021, thinking that there's still a chance for independent media in an age of social networks and their desperate attempts to kill the likes of RSS. I'd prefer to see that as *optimism* ^^;.

