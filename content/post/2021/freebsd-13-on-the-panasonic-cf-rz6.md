---
title: "FreeBSD 13 on the Panasonic Let’s Note CF-RZ6"
date: "2021-06-02T14:49:16+10:00"
abstract: "My first page on the FreeBSD Wiki :)."
thumb: "https://rubenerd.com/files/2021/pct_intro03_l@1x.jpg"
year: "2021"
category: Hardware
tag:
- bsd
- freebsd
- panasonic
- panasonic-lets-note-cf-rz6
location: Sydney
---
I was about to launch into a guide about this cute little Japanese laptop, before finally deciding to [make a page on the FreeBSD Wiki](https://wiki.freebsd.org/Laptops/Panasonic_Lets_Note_CF-RZ6) about it. I also updated the [laptops](https://wiki.freebsd.org/Laptops) page, and submitted another [dmesg to the NYC\*BUG](https://dmesgd.nycbug.org/index.cgi?do=view&amp;id=6112).

The [FreeBSD Wiki](https://wiki.freebsd.org/) comes up in search results sometimes, but I still think it's an underutilised and underappreciated resource. Between that and the canonical [Handbook](https://docs.freebsd.org/en/books/handbook/), you can figure out most of how to build and maintain FreeBSD systems. Special thanks to Benedict Reuschling for accepting my application to contribute, and Mark Linimon for sorting out my account :).

<p><img src="https://rubenerd.com/files/2021/rz6-2017spring-series-main-02@1x.jpg" srcset="https://rubenerd.com/files/2021/rz6-2017spring-series-main-02@1x.jpg 1x, https://rubenerd.com/files/2021/rz6-2017spring-series-main-02@2x.jpg 2x" alt="Panasonic press photo of the Panasonic Let's Note CF-RZ6" style="width:500px; height:300px;" /></p>

As for this laptop, I [bought it in Akihabara](https://twitter.com/rubenerd/status/1109449223055896576) during a trip to AsiaBSDCon 2019. It already seems like such an age ago now. I miss the chats and beer 🍻.
