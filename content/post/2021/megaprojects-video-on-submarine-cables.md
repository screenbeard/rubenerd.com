---
title: "Megaprojects video on submarine cables"
date: "2021-05-12T14:28:07+10:00"
abstract: "Adding my own thoughts to an excellent YouTube video on modern telecommunications."
thumb: "https://rubenerd.com/files/2021/yt-T96P2d2UVsQ@1x.jpg"
year: "2021"
category: Media
tag:
- megaprojects
- networking
- sideprojects
- standards
- video
- youtube
location: Sydney
---
[Simon Whistler](https://twitter.com/SimonWhistler) and his team do such a great job distilling disparate topics into 15-20 minute segments on his various YouTube channels. [Megaprojects](https://www.youtube.com/channel/UC0woBco6Dgcxt0h8SwyyOmw) remains my favourite, though I've also been binging [Sideprojects](https://www.youtube.com/channel/UC3Wn3dABlgESm8Bzn8Vamgg) with Clara of an evening. Making topics interesting and approachable to a broad audience, especially with a bit of humour, is a skill I've yet to master after a decade and a half of blogging and speaking.

*(Clara also has a thing for dapper men with English accents, and I'm sure it still kills her inside that I sound like a weird Australian who grew up overseas. But I digress)!*

<p><a href="https://www.youtube.com/watch?v=T96P2d2UVsQ" title="Play Undersea Communication Cables"><img src="https://rubenerd.com/files/2021/yt-T96P2d2UVsQ@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-T96P2d2UVsQ@1x.jpg 1x, https://rubenerd.com/files/2021/yt-T96P2d2UVsQ@2x.jpg 2x" alt="Play Undersea Communication Cables" style="width:500px;height:281px;" /></a></p>

I just finished their [episode on submarine cables](https://www.youtube.com/watch?v=T96P2d2UVsQ), and wanted to add a few points:

* It's true that cost and speed are the principle reasons for using cables over satellites for most workloads, but just as important is latency. Satellites and comms have improved from those awkward exchanges between correspondents on TV, but it will always be more variable than a dedicated, direct path of light.

* Just as impressive as the physical effort to plan, lay, and maintain submarine cables are the standardised protocols that make them all work. I think it's an underappreciated&mdash;and frankly beautiful&mdash;triumph of humanity that we can all agree on specific implementation details, regardless of location, business, or worldview.

* Simon pontificated about a future where we may move on from cables. Satellite constellations may prove to get us part of the way there, but short of a breakthrough I think we'll be using cables for the bulk of comms for many more decades. The Trekkie in me wants to say *microwormholes* and teleportation.

