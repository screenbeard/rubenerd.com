---
title: "Trickle-down economics"
date: "2021-02-02T15:31:14+11:00"
abstract: "Surprising nobody, it doesn’t work."
year: "2021"
category: Thoughts
tag:
- economics
location: Sydney
---
Aimee Picchi [reviewed a paper for CBS](https://www.cbsnews.com/news/tax-cuts-rich-50-years-no-trickle-down/) about the effects of tricke-down economics, with unsurprising findings:

> The new paper, by David Hope of the London School of Economics and Julian Limberg of King's College London, examines 18 developed countries — from Australia to the United States — over a 50-year period from 1965 to 2015 [..] Per capita gross domestic product and unemployment rates were nearly identical after five years in countries that slashed taxes on the rich and in those that didn't.

But most of all:

> The incomes of the rich grew much faster in countries where tax rates were lowered. Instead of trickling down to the middle class, tax cuts for the rich may not accomplish much more than help the rich keep more of their riches and exacerbate income inequality, 

These sorts of studies are difficult, because we can't travel trough time retroactively and reverse or apply tax cuts to the same juristictions to witness the outcome. Having studied economics too, I place far more stock in scientific papers because they're reproducable.

But the broad trend here is clear. Trickle-down economics is, most charitably, ineffective. *Get it?* Because it's not charitible! Well, other than for people who don't need it.

