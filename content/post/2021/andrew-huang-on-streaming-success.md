---
title: "The @andrewhuang on streaming “success”"
date: "2021-05-26T08:02:10+10:00"
abstract: "He proposes the book industry emulate the tremendous success we musicians have seen with streaming."
year: "2021"
category: Media
tag:
- music
- streaming
location: Sydney
---
He [tweeted this morning](https://twitter.com/andrewhuang/status/1397300429927628801):

> I propose the book industry emulate the tremendous success we musicians have seen with streaming. For $10/mo users get access to every book in the world. When someone reads a book, the author receives a third of a cent.

Twitter then proceeded to do what it does best: asking him if he knew what libraries were. I can rattle off a few other obtuse non-sequiturs as well, like carrots, handbags, planets, hamsters, weddings, poets, and Kuala Lumpur. Have you heard of those, too? Actually, scratch that last one, they probably only know Malaysia from a Ben Stiller movie.

I've talked here a few times about how streaming services solved piracy for media companies, but not for individual artists, and *especially* not indie artists. A single CD sale represents tens of thousands of streaming plays, if not more. The power imbalance looks worse than it was in the Napster days, though the media are fine with this because *their* arses are covered.

These types of services devalue music, and in my experience they make music feel disposable. I think that's a shame, and not where we should settle.

Keep your streaming service if you want, but be sure to support acts you care about. I buy an album a month with the money I was spending on streaming services instead, and sync them to a dedicated music player that I carry to work like it's 2005. It's made music fun again.

