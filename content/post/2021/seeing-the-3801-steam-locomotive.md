---
title: "Seeing the 3801 steam locomotive"
date: "2021-03-14T10:37:12+11:00"
abstract: "Childhood dream unlocked!"
thumb: "https://rubenerd.com/files/2021/3801-front@1x.jpg"
year: "2021"
category: Travel
tag:
- 3801-locomotive
- australia
- trains
location: Sydney
---
It might not surprise some of you to know that I was *obsessed* with steam locomotives as a kid. Posters of the 3801, alongside the Victorian R-761 and the *Flying Scotsman*, adorned my walls before I even started primary school. I'm positive I watched one specific VHS tape about these machines on a loop on account of my parents realising I'd shut up and leave them be if I were sat in front of it for hours at a time!

The 3801 is a striking beast. She was built for the NSW Government Railways in 1943, and donated to the New South Wales Transport Museum in 1975. She famously steamed across the continent during the Austraian Bicentennial celebrations along with the other aforementioned locomotives. By 2007 she'd suffered sufficient mechanical and boiler issues that she was withdrawn from service. Fans like me were worried we'd never get to see her running again, but after 12 years of restoration work by volunteers she returned to service... yesterday!

<p><img src="https://rubenerd.com/files/2021/3801-front@1x.jpg" srcset="https://rubenerd.com/files/2021/3801-front@1x.jpg 1x, https://rubenerd.com/files/2021/3801-front@2x.jpg 2x" alt="" style="width:500px" /></p>

Clara booked us a private cabin in one of the restored carriages for one of her relaunch runs from Central to Hurstville. Not to put too fine a point on it, but it easily ranks as one of the hightlights of my life.

The restoration team did an incredible job. She was shinier and cleaner than I'd ever seen her in photos and videos before! Seeing her simmering on the platform, in real life... she didn't seem real. I wish my five-year old self could have been there. Oh wait, he was :).

