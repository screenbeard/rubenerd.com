---
title: "Other factors behind unhelpful comparison sites"
date: "2021-05-13T07:00:09+10:00"
abstract: "Josh from The Geekorium suggests it could also be people avoiding being targeted."
year: "2021"
category: Software
tag:
- communities
- feedback
- josh-nunn
location: Sydney
---
Josh Nunn of [The Geekorium](https://the.geekorium.com.au/) had some [feedback](https://aus.social/@screenbeard/106213344635063363) on my post about the [two kinds of comparison sites](https://rubenerd.com/two-types-of-comparison-sites/)\:

> I wonder if the reason there are fewer experts putting out their opinions on software and doing these comparisons is in part because there's no incentive. Comparing nginx to Varnish you're just opening yourself up to arguments, vitriol, and legions of fanboys descending on your writing to pick it apart for not choosing their favourite. And unless you're an expert, what you say is just going to be ridiculed if you're not 100% correct.

Rebecca Hales and Jonathan H. chimed in with similar thoughts. Chances are you've gone through the same thing if you're reading this by daring to have an opinion based on experience and facts.

I hadn't considered this angle at all. I had dismissed the second kind of unhelpful technical comparison site as "churnalism":

> These are written quickly and in bulk with barely a superficial understanding of the topic. They’ll mention software A, software B, then some form of *in conclusion, both are good options.* This isn’t helpful either.

I still think this applies to most mass-produced sites I read, but just as reasonable is the idea of people so jaded and exhausted from experience that they hedge their bets in an attempt to not be targeted. It's a grim indictment of Internet culture if true.

I didn't blog about BSD for years because I was burned out from Linux trolls; and before then, my Linux posts attracted Windows trolls. IT people are a fun bunch.
