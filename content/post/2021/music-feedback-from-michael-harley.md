---
title: "Music feedback from Michael Harley"
date: "2021-09-25T09:19:46+10:00"
abstract: "Not being able to buy digital music, and tools to play it offline in 2021."
year: "2021"
category: Media
tag:
- feedback
- music
location: Sydney
---
I wrote last Wednesday about my adventures with [making mix "tapes" in 2021](https://rubenerd.com/collecting-music-with-your-own-metadata/), which in this case are my own compilation albums with custom ID3 metadata and covers. I also spent a disproportionate amount of my [latest silly podcast](https://rubenerd.com/show419/) thinking about mobile music players. It's something that's been on my mind again a lot lately.

Michael of [Obsolete29](https://obsolete29.com/) (he has a [web feed](https://obsolete29.com/feed/feed.xml), and he's on the [blogroll](https://rubenerd.com/omake/#blogroll)!) had some interesting follow-up questions and thoughts.

> I enjoyed the mix tapes post as I'm going through this same process
myself. One of the most frustrating parts of the journey is how many
bands just simply do not offer ways to purchase digital music from
them. They only link to the streaming services. An example is Hippie
Sabotage. They literally do not even seem to consider that someone may
want to *buy* music from them. I'd like to also say I've purchased some
physical media and I do believe that every purchase of physical music
should come with a code to download the digital copy. IMO.

Unfortunately it's becoming all too common, and I see it as another casualty of streaming services. I've been *very* lucky that most of the acts I follow are happy to sell me digital copies, and even vinyl with codes to download; [Kris Delmhorst](https://www.krisdelmhorst.com/) and [Cory Wong](https://www.corywongmusic.com/digital-audio) come to mind as recent examples. For my sister and Clara it's another story.

One aspect I hadn't considered when I was chatting with Michael was international music. Clara loves J-pop, but finding anyone who'll even ship a CD internationally, let alone a digital download, is all but impossible unless you're very famous. My dad still struggles with world music, especially from Africa.

Michael wrote in a follow-up email that he worries that a generation have grown up unfamiliar with the idea of buying music. That coupled with the fact that modern acts see their only way to survive is to just post on streaming platforms is a one-two punch.

Michael continued:

> I'm curious about your music listening setup? What OS's and apps are
you using on desktop and mobile? Do you sync music and playlists
between the devices? How are you editing the metadata?

We talked back and forward about our setups, but I'll admit I'm still struggling to come up with a system that works for me. Michael uses Audacious on Linux, which lets him create and sync m3u playlists to his phone. It sounds compelling.

Right now my primary desktop machine is still a Mac, but I've transition to using [Musikcube](https://musikcube.com/) for much of my listening. It runs in the terminal, is cross-platform, and lets me just throw a folder of music at it. I edit metadata in bulk in the terminal with [eyeD3](https://pypi.org/project/eyed3/), and the [KDE Kid3 GUI](https://kid3.kde.org/) for more complicated stuff.

My next step is to get my Palm Lifedrive syncing somehow, now that it has a giant CF card in it. I think another big part of this is not wanting music on my phone, which also has work stuff and other commitments. A portable media player, in whatever form it takes, might be cool.
