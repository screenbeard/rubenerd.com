---
title: "Stores for Commodore 128 components"
date: "2021-02-24T07:30:46+11:00"
abstract: "An evolving list of indie merchants from everywhere!"
year: "2021"
category: Hardware
tag:
- commodore
- commodore-128
- shopping
location: Sydney
---
This is part four in my ongoing [Commodore 128 Series](https://rubenerd.com/tag/commodore-128-series/)! The C128 is a fantastic 8-bit computer because it can do so much with its built in mix of hardware, but that doesn't mean it can't be extended and introduced into the modern world in weird and wonderful ways. I also love how all of these are maintained by hobbyists from all over the world.

The components I've bought from these stores will eventually appear in their own post, along with my adventures installing and testing them. But for now I wanted a place to store this list.

* [8bitphoenix](https://www.etsy.com/au/shop/8bitphoenix?listing_id=775585490)
* [AmiBay Commodore Hardware](https://www.amibay.com/forumdisplay.php?27-Commodore)
* [Amigascene](https://www.etsy.com/au/shop/Amigascene)
* [Arananet RetroProducts](https://www.arananet.net/pedidos/product-category/c64/)
* [BackBit](http://store.backbit.io/)
* [eBay, Commodore Parts and Accessories](https://www.ebay.com.au/b/Commodore-Vintage-Computer-Parts-Accessories/175690/bn_7014658549)
* [The Future Was 8-bit](https://www.thefuturewas8bit.com/shop/commodore.html)
* [GGLabs](https://gglabs.us/node/451)
* [időrégész.hu](https://idoregesz.hu/product-category/commodore/)
* [Melbourne Console Reproductions](https://melbourneconsolerepros.com/index.php?cPath=22_32&osCsid=raeh8j57b8k8so7tsvnm1m5go7)
* [OldSoftware.com](https://www.oldsoftware.com/CBMchips.html)
* [Retro8BITShop](https://retro8bitshop.com/product-brand/commodore/commodore-128/)
* [Retro Gaming Cables](https://www.retrogamingcables.co.uk)
* [Retrofixes](https://store.retrofixes.com/products/commodore-c64-heatsink-upgrade-kit)
* [Retroleum.co.uk](https://www.retroleum.co.uk)
* [Retro Rewind](https://retrorewind.ca/commodore-128)
* [Shareware Plus Commodore 64 & 128](https://sharewareplus.blogspot.com)
* [TexElec](https://texelec.com/)
* [Ultimate64](https://ultimate64.com/Main_products)

I might add more here as I discover them. Let me know if there's anywhere else I should know about!

