---
title: "James’ Coffee Blog Advent of Bloggers"
date: "2021-12-19T10:53:35+10:00"
abstract: "This might be the nicest thing anyone’s written about my stuff."
year: "2021"
category: Internet
tag:
- james-coffee-blog
- weblog
location: Sydney
---
James's Coffee Blog [recently featured my blog](https://jamesg.blog/2021/12/06/advent-of-bloggers-6/) in his Advent of Blogs series. That sentence had the word blog three times, and now this paragraph has had it four times. Blog. Wait, damn it!

*(He even noted the dark theme, which reminds me that I need to finish it! The colours still don't match, and Rubi the mascot doesn't appear in the sidebar because she doesn't sport a transparent background).*

James is one of the most considerate, interesting people writing today, and I'm not just saying it on account of being featured among some other great writers in this series. Thank you :).

