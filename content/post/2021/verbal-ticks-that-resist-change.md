---
title: "Verbal ticks that never change"
date: "2021-07-03T11:46:07+10:00"
abstract: "Dishwashers will always be washing machines to me."
year: "2021"
category: Thoughts
tag:
- australia
- geography
- language
- new-south-wales
- personal
- united-states
location: Sydney
---
The more I learn about improving habits, the more I realise there are certain neural pathways in my brain that are so set in stone, invoking them feels like breathing. Not only are the following words completely wrong, but my identification and awareness of their inaccuracy doesn't shake my continued misuse of them:

* Referring to *shoulders* as *elbows*.

* Referring to *dishwashers* as *washing machines*.

* Pronouncing and reading *retime* as *re-a-time*.

* Saying *etc* as *etcetera* not *ex-cetra*, and referring to coffee as *espresso* not *expresso*. Wait, those are right, carry on!

* Reading *Chesapeake* as *cheapskate*.

In my defence of the last one, I only discovered recently that *Newport News* is a place, not a publication. Who calls a place *news*? Then again, this was named by the same people who brought us *Jersey City, New Jersey* and *Kansas City* not in *Kansas*, but *Missouri*. Just like *Texas City* in... *Texas*. The same Anglo colonists would later come to Australia and bestow the engima of *New South Wales* on us. Is it a newer version of *South Wales*, or is it a newer, southern version of *Wales*?

I really stuck my <del>shoulder</del> elbow in this one.

