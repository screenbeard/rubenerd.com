---
title: "The @ourokronii on laughter"
date: "2021-10-15T08:15:44+10:00"
abstract: "From her latest stream. I can relate… all too well!"
thumb: "https://rubenerd.com/files/2021/kronii-laugh-3@1x.jpg"
year: "2021"
category: Anime
tag:
- hololive
- hololive-council
- ouro-kronii
location: Sydney
---
From [Kronii's](https://rubenerd.com/ouro-kroniis-debut-stream-krotime/) latest [Minecraft stream](https://www.youtube.com/watch?v=_khEG6NomqU "Minecraft: Where do I even begin?"). I can relate... all too well!

<p><img src="https://rubenerd.com/files/2021/kronii-laugh-1@1x.jpg" srcset="https://rubenerd.com/files/2021/kronii-laugh-1@1x.jpg 1x, https://rubenerd.com/files/2021/kronii-laugh-1@2x.jpg 2x" alt="The laugh... laughing is how I cope." style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/kronii-laugh-2@1x.jpg" srcset="https://rubenerd.com/files/2021/kronii-laugh-2@1x.jpg 1x, https://rubenerd.com/files/2021/kronii-laugh-2@2x.jpg 2x" alt="Whether it's because I'm nervous, or... dealing with dread!" style="width:500px; height:281px;" /><br /><img src="https://rubenerd.com/files/2021/kronii-laugh-3@1x.jpg" srcset="https://rubenerd.com/files/2021/kronii-laugh-3@1x.jpg 1x, https://rubenerd.com/files/2021/kronii-laugh-3@2x.jpg 2x" alt="(laughs)" style="width:500px; height:281px;" /></p>
