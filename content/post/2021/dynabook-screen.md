---
title: "Dynabook screens"
date: "2021-01-11T09:18:00+11:00"
abstract: "No surprise, they're way lower PPI than any Mac :/"
year: "2021"
category: Hardware
tag: 
- ergonomics
- screens
location: Sydney
---
You know the drill for me now; whenever I see a cool new new PC laptop I head to the technical specs page to see how pedestrian the screen is. Today's is the premium [Portégé X30L-G](https://asia.dynabook.com/laptop/portege-x30l-g/specification.php):

> 1920×1080, 165 ppi

The 13-inch MacBook Pro has 2560×1600 with 227 ppi, almost 30% higher. And has for a decade.

I know I sound like an old record now, but why don't PC companies care about their screens? Why do they include gimmics like touch but not something that would actually make their computers more usable, especially for photographers or sysadmins that need lots of tiled windows?

All I can think of is the graphics are underpowered, or they think their customers don't care. They must be right, given how the tech press *never* mentions this elephant in the room. Once you use 2x HiDPI, 1.5 scaling is nasty.

Please, PC laptop makers, give Apple some meaningful competition! Dynabook were once Toshiba's legendary laptop line; if anyone could do it, it'd be them.
