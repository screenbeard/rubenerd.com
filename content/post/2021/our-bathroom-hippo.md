---
title: "Our bathroom hippo"
date: "2021-03-27T23:33:47+11:00"
abstract: "A different tack for my birthday post this year :)"
thumb: "https://rubenerd.com/files/2021/our-bathroom-hippo@1x.jpg"
year: "2021"
category: Hardware
tag:
- family
- personal
location: Sydney
---
I'd thought I take a different tack on my birthday today to talk about a little fella who's been with me for more than a few of them!

My family has had this little soapstone hippo for as long as I can remember. His familiar, warm face has always greeted us every time we needed dental floss, cotton buds, Tiger Balm, and other washroom paraphernalia. My dearly-departed mum bought him for one of the first houses she moved into with my dad, and he was ferried around all the family homes in Sydney, Melbourne, Brisbane, Singapore, Kuala Lumpur, and back to Sydney.

<p><img src="https://rubenerd.com/files/2021/our-bathroom-hippo@1x.jpg" srcset="https://rubenerd.com/files/2021/our-bathroom-hippo@1x.jpg 1x, https://rubenerd.com/files/2021/our-bathroom-hippo@2x.jpg 2x" alt="A small, pink soapstone hippo with a horizontal seam which opens up to a little dish for storing small bathroom stuff. He has a friendly face :)." style="width:500px" /></p>

He was there to see my brush my teeth before my first day of school, and my high school graduation. He saw my sister and I deal with acne and teenage breakdowns, practicing school speeches, learning how to shave without cutting off my nose. I spoke to him on more than a few occasions. I’ll bet he could tell stores.

I asked my dad if I could take Mr Hippo with me when I moved out with Clara, and he was only too happy to oblige. We don’t have much in the way of family heirlooms, but it's fun thinking he's been there for two families now.

He briefly spent time in my IKEA Billy bookcase holding USB keys and adaptors. But the bathroom is where we needed his grinning, reassuring face the most, when getting ready for work or taking on a day. *Morning Mr Hippo!*

His exterior has aged over time, and he developed a small chip on the corner of his lip from one of the Singapore moves. But then, we've both aged during that time :).

