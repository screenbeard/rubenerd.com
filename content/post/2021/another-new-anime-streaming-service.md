---
title: "Another new anime streaming service"
date: "2021-08-22T20:29:01+10:00"
abstract: "Why people don’t want yet another subscription."
year: "2021"
category: Anime
tag:
- business
- streaming
location: Sydney
---
I saw an ad recently for a new anime streaming service, and the first comment in the thread was "do not want". I couldn't have summarised it better myself.

I rambled about [media accessibility](https://rubenerd.com/i-wont-watch-it-if-its-inaccessible/) back in February:

> Whereas one could watch Netflix before (for example), now every media company is launching their own *me too* service. Even former computer companies... oh Apple, how far you've fallen. This dilutes the utility of any one platform each time, as more and more shows are splintered off into their own paid services that fewer people are likely to subscribe to in aggregate.

It's a bizarre, though perhaps not *entirely* unpredictable situation. Media companies would rather silo their content into another streaming platform people will need to pay for, both financially and with their time. You can't go to one place anymore, you have to remember which platform was hosting whichever series or show you're interested in. Even season, in some cases.

How is any of this better than the cable TV that cord-cutters cut? And how many woodchucks is that? Not to mention compatibility and accessibility concerns; I cross my fingers each time I try to access a new streaming service on FreeBSD or Linux, because we all know how much they love catering to people on alternative platforms. And you know that each one will launch with different and arbitrary geographic restrictions.
 
Think about the reaction of that commenter above. New streaming services today aren't just seen as competitors, they're *actively not wanted* by the people who use them. It's a classic web anti-pattern that puts the perceived and self-defeating needs of a business over people. There's a reason physical media *format wars* were never tenable long term.

My only hope is that these services see a drop-off in the number of people watching their shows. But even then, given the history of media company obstinacy, they'd probably blame that on piracy or geopolitical squabbles, and not on the real fact that they just made it more frustrating for the fans and viewers of the very content they publish to watch.

