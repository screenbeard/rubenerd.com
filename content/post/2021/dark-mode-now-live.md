---
title: "Dark mode theme now live"
date: "2021-10-21T08:02:55+10:00"
abstract: "Took surprisingly little effort, should have done ages ago."
year: "2021"
category: Internet
tag:
- colour
- dark-mode
- design
- weblog
location: Sydney
---
The devilishly-handsome proprietor of [The Geekorium](https://the.geekorium.com.au/) noted on [Mastodon](https://aus.social/@screenbeard/107132076632725577) that this site you're reading now has a dark mode theme. If your OS or window manager is set to a dark theme, and you're reading these words on the site instead of an RSS aggregator, you should be basking in darkness now.

I've long had a second, dark theme for the site listed in page headers called "Night Style", but I think only Opera surfaced the option to switch to it. I see almost no hits to it either, so it's likely nobody ever knew about it, let alone used it.

I didn't realise you only needed a [CSS media query](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme) to implement dark mode it in an existing theme, so I quietly added it a week ago to test. It's a work in progress and far from perfect yet, but better than nothing.

Curiously, I find dark modes *exacerbate* my headaches and eye strain, and makes words harder to read. Medical research seems to [bear this out](https://pubmed.ncbi.nlm.nih.gov/23654206/ "Positive display polarity is advantageous for both younger and older adults"), but I think personal preference is just as important. If you've indicated with your theme that you prefer dark modes, I'd like to be able to accommodate that.

