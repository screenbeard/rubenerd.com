---
title: "Holo Bass and Amelia Watson, Pop on Rocks"
date: "2021-03-29T10:22:49+11:00"
abstract: "I’m sorry, Mr Fox sir."
thumb: "https://rubenerd.com/files/2021/yt-42QhdIradBc@1x.jpg"
year: "2021"
category: Media
tag:
- amelia-watson
- hololive
- music
- music-monday
location: Sydney
---
Today's [Music Monday](https://www.youtube.com/watch?v=42QhdIradBc) is Holo Bass's fan song about Amelia Watson, Clara's and my favourite Hololive EN character. It already came out months ago, but we may have had it on repeat again recently.

<p><a href="https://www.youtube.com/watch?v=42QhdIradBc" title="Play Amelia Watson - Pop on Rocks: A Dr. Seuss Rap"><img src="https://rubenerd.com/files/2021/yt-42QhdIradBc@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-42QhdIradBc@1x.jpg 1x, https://rubenerd.com/files/2021/yt-42QhdIradBc@2x.jpg 2x" alt="Play Amelia Watson - Pop on Rocks: A Dr. Seuss Rap" style="width:500px;height:281px;" /></a></p>

Her [reaction in December](https://www.youtube.com/watch?v=3aLenRgqt-Y) was great, too.

