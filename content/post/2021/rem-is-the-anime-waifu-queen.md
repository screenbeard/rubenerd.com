---
title: "Rem is the anime waifu queen, according to stores"
date: "2021-10-09T08:59:38+10:00"
abstract: "Supply and demand still alive and well for this character who first appeared seven years ago. That’s impressive."
thumb: "https://rubenerd.com/files/2021/rem-s01e11@1x.jpg"
year: "2021"
category: Anime
tag:
- re-zero
- rem
- shopping
location: Sydney
---
When I can't sleep, or am otherwise mentally exhausted but still awake, I tend to read or window shop on eBay. Yesterday I was reading up on Estonian forests, and going down the rabbit hole of "anime light boards" for gaming PCs. I was unaware of the existance of these ornimental addons, nor do I even have space or use for them. The boards, not Estonian forests. We need more of those.

*I will go to the Baltic states one day, just you wait and see!*

What struck me was how predictable so many of the listings were. What sellers put up for auction on sites like eBay, Etsy, Red Bubble, and the like says a lot about the popularity of franchises and characters. Sellers wouldn't waste their time listing items that aren't in demand, and they have the purchase histories to inform these decisions. *Supply and demand*, in other words.

If you just go by what people list, there is no question that Rem is still the undisputed queen of anime waifus. The blue-haired, battle-hardened maid from the breakout hit *Re:Zero Starting Life in Another World* first featured in web and light novels seven years ago, and the anime five.

<p><img src="https://rubenerd.com/files/2021/rem-s01e11@1x.jpg" srcset="https://rubenerd.com/files/2021/rem-s01e11@1x.jpg 1x, https://rubenerd.com/files/2021/rem-s01e11@2x.jpg 2x" alt="Rem’s famous scene from episode 11 of the anime" style="width:500px; height:281px;" /></p>

Her enduring popularity is no small feat. Unlike Western comics and cartoons, most anime series have (relatively) brief shelf lives. There are notable exceptions to the rule (Naruto, One Piece, Sailor Moon, etc), but in my experience most people obsess over a character for a short while before the next one comes along to steal their attention with the attributes they admire.

There's a socioligical reason why blue and grey-haired characters like Rem, Rei, Ami, Yuki, and my site mascot Rubi are successful, which I'll admit I don't fully understand. They certainly feature heavily in my lists of favourite characters (though I was more of a pink-haired Ram fan in *Re:Zero*). It's a well-established trope that conveys calm intelligence with a dose of cuteness. Maybe because blue is a reassuring, unthreatening colour? Which is somewhat ironic, given how much you'd stand out in the real world with it as your dyed colour of choice!

Anyway, I just thought it was amazing that all these years later, she's *still* the default in so many listings. How long will her reign last?

*(The irony also isn't lost on me that I can't see blue right now. Maybe a light blue cast would be even more calming than a [greyscale display](https://rubenerd.com/greyscale-screens-for-anxiety/)).*
