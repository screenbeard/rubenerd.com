---
title: "macOS relocating my /private/etc/shells"
date: "2021-10-16T08:37:06+10:00"
abstract: "Hello, Relocated Items.nosync foler"
year: "2021"
category: Software
tag:
- oksh
- ksh
- macos
location: Sydney
---
I did a macOS software update recently and was greeted with a *Relocated Items.nosync* folder on my desktop.

Inside was a PDF explaining what it was:

> During the last macOS upgrade or file migration, some of your files couldn’t be moved to their new locations. This folder contains these files.
> 
> These configuration files were modified or customised by you, by another user or by an app. The modifications may be incompatible with the recent macOS upgrade. The modified files are in the Configuration folder, organised in subfolders named after their original locations.
>
> To restore any of the custom configurations, compare your modifications with the configuration changes made during the macOS upgrade and combine them when possible.

There was only one affected file: `/private/etc/shells`. I'd echo'd the path of the OpenBSD portable Kornshell [from pkgsrc](http://pkgsrc.se/shells/oksh) into it so I could use it as my daily driver, because I'm a gentleman.

The notice above had me believing they'd replaced my config, but instead they'd retained my original file and put their desired changes into the relocated folder. A quick diff, which sounds more like a band name, showed:

	12d11
	< /opt/pkg/bin/oksh

I think I'll be fine keeping that.
