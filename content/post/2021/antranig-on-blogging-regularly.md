---
title: "Antranig on blogging regularly"
date: "2021-04-12T09:26:28+10:00"
abstract: "His discussion about needing to write more, and his challenges with writing thoughts in English."
year: "2021"
category: Thoughts
tag:
- blogging
location: Sydney
---
I was catching up on RSS this morning and [saw this shoutout by Antranig](https://antranigv.am/weblog_en/posts/2021-04-05-13-32/) on his English blog:

> Ruben blogged recently about blogging regularly and it kind of hit me: Why don’t I blog regularly? I love blogging. I improved my Armenian by blogging for years, I wanted to be a blogger so bad that I asked my friends to rent me a domain and a hosting service since I didn’t have money when I moved to Armenia after the war. But yet again, it’s very hard for me to write my thoughts in English.

I keep having this discussion with my girlfriend Clara about her native Cantonese, and my dad about German. I'd selfishly love for these people to write in English, but being able to speak a second language *and* English is incredibly useful. If I lament the decline of English bloggers of late, the pool of Cantonese and Armenian writers who also understand English would be even smaller.

I know a lot of engineers for whom English is their second language, but they write in it for exposure. There's no question I benefit greatly from writing in a language that Americans can natively understand, for example. But by the same token, if I ever saw myself "competing" with other writers (I don't, to be clear!), the pool of English writers is massive. Clara is interested in anime and manga, my dad is into Italian motorbikes and travel, and Antranig has some of the most considered and well-written posts on BSD I've read in years... maybe writing in their native tongues would be even more engaging and useful for people.

I've also been told by people over the years that blogging has *helped* them write and communicate better in English. Antranig's English is excellent, but if anyone else wants to get into writing and blogging better, I'd be [happy to help](https://rubenerd.com/about/#contact) :).

