---
title: "Spam calls"
date: "2021-11-02T19:32:08+10:00"
abstract: "Never used to get them before, now they’re a daily occurance."
year: "2021"
category: Thoughts
tag:
- phones
- spam
location: Sydney
---
I managed to largely avoid getting spam calls my whole life, though that's just changed in the last couple of months. I get calls with Swiss, British, and American country codes telling me about Amazon orders I haven't made, tax I don't owe, and support for desktop operating systems I don't run.

*(And I'll bet even those caller ID numbers are faked).*

It's got so bad that I've been tempted to ditch my personal number entirely, and only use it for data (if that's even possible). There are people who make YouTube careers stringing along these scammers, but I just want to be left alone.

They're a nusiance, but they also do a number on people prone to anxiety. Friends and family know that we message each other first to make sure we're not in the middle of something important, so a call usually signals something is either urgent, or a delivery. Now my heart rushes only to realise its someone telling me "Microsofts need of the updates?"

My heart does go out to some of the people doing this. For each arsehole, I'm sure there's someone in a troubled part of the world who see oblivious people in the rich world as a meal ticket. I'm sure in their ethical model they assume you and I would be down a bit of money, but to them it might be a lifeline. Whether it be vaccine rollouts or general aid, we sure are good at turning a blind eye to the plight of our fellow human beings in other parts of the world.

But then I remember the kind of people who are taken in by these scams, many of whom are not that well off themselves, and that idealised view of a scammer begins to disintegrate. Just like my patience!

I'd continue this post, but I just got a call from someone in New South Wales claiming that I have overdue tax. The Internet would tell me to string him along and try and scam *him*, but I just hung up.
