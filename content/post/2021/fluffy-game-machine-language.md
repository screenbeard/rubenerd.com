---
title: "It started as a post about game machine language"
date: "2021-08-21T16:11:04+10:00"
abstract: "Don’t read this."
year: "2021"
category: Thoughts
tag:
- advertising
- pointless
- weekend-post
location: Sydney
---
This post is a bit weird. Even the title makes no sense. Is it machine language for games? A language for game machines? Is this post (excuse me, "piece") an example of the aforementioned language? How could it be, given it was written by me and not a machine. I do feel a bit like a blogging machine sometimes though, but one that targets games? Or game machines? Don't worry, the advertisement I'm about barely makes sense either.

I had my second shot of Covid vaccine yesterday. The side effects aren't nearly as bad as the first shot, though today I'm feeling noticeably listless and foggy in the head. As opposed to foggy in the hands, which sounds like I was summoning clouds with my outstretched palms. It's a surreal feeling; I'm not physically or mentally tired in the sense that I'll collapse onto my keyboard, but every mental task takes twice or three times longer. I'd be more accurate in my calculations if I weren't so foggy in the head. It's a surreal feeling; I'm not physically or mentally...

I saw this social media advertisement for a premium game machine brand on social media that was advertising their premium game machine:

> Be who you want. Go where you choose. $REDACTED is your portal to worlds where limits don't exist and you're free to play by your own rules.

It could be the vaccine I just had, and am relieved I just had, but what in the flame broiled burger are they talking about? I was never really a Whopper person, my favourite foods from Burger King were their excellent onion rings, and the BK chicken sandwich. Australia has Hungry Jacks which people here claim is the same thing, but it isn't. No really, it isn't.

Let's break this advertisement down into its constituent components. Let's go old school on them like fractional distillation, pulling apart each segment into individual sentences so we may figure out what in the actual ever loving sandwich this game advertisement is saying.

**"Be who you want".** Assuming you can afford a premium game machine? There's a barrier to entry to being who you want? And even if you could, how does a computer let you do that? It's a computer. I've spent my life in service of these ungrateful machines, and let me tell you, they have no emotional state or desire to have you be whomever you want. That's another thing, the grammar sounds lazy. I'm off my face right now and my sentences are more cohesive than this, if a bit on the verbose side of verbosity in which more words are said than what are necessary to convey an idea, thought process, or verbosity.

Maybe the next sentence makes more sense. **"Go where you choose"**. I can't, *Sydney is in lockdown*. Even if you were making a sweeping statement about venturing into virtual worlds, *read the damned room!*

Then they say they're **"your portal to worlds where limits don't exist"**. Except, you know, the physical limitations of the system. What if I want over 9000 FPS on a Retina-grade screen? Wait, I can't? Why not? Oh, *there's a limit* is there!? I kid, PC makers are only capable of [shipping rubbish screens](http://rubenerd.com/tag/pc-screen-syndrome/) even in 2021, let alone ones where limits don't exist.

*("Live without limits" has to be among the most grating, pointless, nonsensical platitudes advertisers ever got their hands on. It manages to take up three words in a sentence without saying anything that's grokkable or even factually true. I could pull three words off this pamphlet about Covid vaccines the nurse gave me and generate a more meaningful string of words. Here, let me try: "headache brings recycled". Genius)!*

Which leads us to the glorious last part of the last sentence in the advertisement, a form of media people regularly abbreviate as an "ad", though my use of it is entirely pointless given I've already used the full word "advertisement" at least ten times now. We've yet to hit a *synergy* or *dynamism* or *paradigm* yet, which I insist on pronouncing *para-didge-ee-um* because if they didn't want it pronounced that way they should tone down its use and substitute alternative words for it sometimes. If you don't know what a thesaurus is, maybe you need an alternative word for it first.

Here we go: **"you're free to play by your own rules"**. Oh yes, I'm sure jumping into a multiplayer game and using cheats to play the game by my own rules will go down *supremely* well, like the best pizza I've ever eaten, with pineapple, because I'm not a simpleton who can't mix flavours without disrupting my delicate sensibilities. Pineapple on pizza is *glorious*. And if you don't think a fruit belongs on pizza, what do you think the tomato sauce is made from, beetroot?! Is beetroot a vegetable? And if you think sweetness doesn't belong on pizza, you sure as hell better not read the ingredients for that tomato sauce either. I'll bet people who get angry about pineapple on pizza are the same ones who blanch at the idea of cheese foam at a bubble tea place, then go home to eat a cheesecake.

This has been a Weekend Post on the Rubenerd Blog. Thank you. I'm going to go lie down.

