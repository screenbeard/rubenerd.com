---
title: "grep returns (standard input) on FreeBSD"
date: "2021-03-24T22:07:52+11:00"
abstract: "Friends don't let friends accidentally use --files-with-matches in your GREP_OPTIONS"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- troubleshooting
location: Sydney
---
I was dealing with a *bizarre* error with **grep(1)** on FreeBSD, and it soon infected my macOS and NetBSD machines too. It was driving me crazy!

Negative results didn't print anything and returned a 1/false exit code, as expected:

    $ zfs list | grep illegalness
    $ echo $?
    ==> 1

If one or more lines matched however, it would only print a single line *in lieu* of those matching lines. "In lieu" still sounds like a decadent French take on a schnitzel. The exit code was still correct though:

    $ zfs list | grep log
    ==> (standard input)
    $ echo $?
    ==> 0

This was less than useful. You could say it was *useless*, though I felt the only thing I was having less of was hair growth on my scalp after ripping out sufficient quantities of it.

I went digging and realised I'd defined this in my [oksh(1)](https://github.com/ibara/oksh) `~/.kshrc` for reasons that confound me to this day:

    export GREP_OPTIONS='--colour=auto --files-with-matches --recursive'

I removed `--file-with-matches` and tried again:

    $ zfs list | grep log
    ==> zroot/var/log      544K  199G  544K  /var/log
    ==> zroot/srv/www/log  901K  199G  901K  /srv/www/log
    $ echo $?
    ==> 0

*Hot ziggidy!* I have my grep back! But what was happening here, and why did it spread?

grep was only returning matching *files* with that option set, instead of matching *lines*. It threw me by treating standard input as a file, which it dutify reported back to me when a line matched in that "file".

The reason it spread was another silly mistake. I had begun to merge my OS-specific `.kshrc` files into one, with a `case` statement to handle where OSs differ. The good news is FreeBSD, NetBSD, and macOS broadly use and respect each other's flags owing to the same or similar userlands, but those GNU ones though... I guess it "isn't UNIX" (cough). Fixing that offending `GREP_OPTIONS` variable solved it everywhere.
