---
title: "03:00 thoughts"
date: "2021-04-21T02:41:27+10:00"
abstract: "This reminded me of something"
year: "2021"
category: Anime
tag:
- anime-figs
- cp
- re-zero
- servers
- work
location: Sydney
---
A testy server woke me up from sleep this morning. Well, monitoring did specifically. As much as you don't want to see those alerts while you're asleep, it's better than *not* seeing alerts.

Once I'd sorted it out, the situation reminded me of something which has since escaped me. Here are some other thoughts:

* Pretzels?

* I counted Aloe Blacc saying he needs a dollar twenty times in his 2010 song *[I Need a Dollar](https://www.youtube.com/watch?v=Pqj119D9kDg)*. That means he needs $20.

* Why do detective shows always talk about "the body" and how "it" was found, rather than "his/her/their body" and that "he/she/they" were found?

* Alpha Omega have released a [cat-eared version of Rem](https://www.amiami.com/eng/detail?scode=FIGURE-055655&rank= "Details of her fig on AmiAmi") from Re:Zero, because of course. Why not Ram though? I can only assume she's pending.

* The first mention of Rem on this blog was [back in 2016](https://rubenerd.com/re-zeros-rem/), when I said the franchise name sounded like a forensic drive wiping tool that used blue bob cuts.

* Maybe it was all the *Mary Tyler Moore Show* reruns I used to watch with my parents, but bob hair is *so* my thing. As in, *really* my thing! Should I be saying that out loud?

<p><img src="https://rubenerd.com/files/2021/cat-ear-rem@1x.jpg" srcset="https://rubenerd.com/files/2021/cat-ear-rem@1x.jpg 1x, https://rubenerd.com/files/2021/cat-ear-rem@2x.jpg 2x" alt="Press photo of cat-eared Rem from Alpha Omega" style="width:500px; height:400px;" /></p>

Alright, back to bed.

