---
title: "A term for accurate, yet pointless responses"
date: "2021-05-05T08:46:32+10:00"
abstract: "Effective communication is about being understood."
year: "2021"
category: Thoughts
tag:
- interpersonal
- social-media
location: Sydney
---
I keep being reminded of that paraphrased John Siracusa quote that technical accuracy is necessary, but not sufficient. Effective communication is about being *understood* as much as is about being correct. A large cohort of the Interwebs seem utterly oblivious to this, whether it be in online debates, or even joke deconstruction. Worse, others wear it as a badge of honour.

Someone posting about how they're "so full they could never eat again!" will invariably garner a response from someone saying they need to eat throughout your life to survive. A person exasperated at a recursive dependency in a package manager will be told that life was tougher when we only had tarballs to compile. Make a joke about how renewing your passport was a bit pointless this year, and you'll have it explained to you that passports last a decade. Frustrated that you missed your train? Well, had you considered leaving earlier?

Those responses are factually accurate, and completely miss the point. Some don't see the forest for the trees, others are just obtuse or lack empathy. But the outcome is the same: metal exhaustion at having to deal with it!

I've been trying *so hard* to think of a term for this kind of rhetoric, anyone have an idea?

