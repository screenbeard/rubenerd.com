---
title: "The Nintendo Switch Lite is my new 2DS"
date: "2021-12-26T15:57:11+11:00"
abstract: "Thank you Clara!"
thumb: "https://rubenerd.com/files/2021/ds-lite@1x.jpg"
year: "2021"
category: Hardware
tag:
- games
- nintendo
- nostalgia
- pokemon
location: Sydney
---
*tl;dr: It's awesome, if you want a personal device!*

I'm a bit weird when it comes to Nintendo gear, and consoles in general. I don't play that many games, and the ones I do tend to be on PC. But *Pokémon* is such a nostalgia trip, even if I don't play it all that often.

Some of my fondest childhood memories were of sitting up late at school camp, trading *Pokémon* between my friends who had Red and Yellow. Then coming home and exploring those worlds with my sister with hers, too. The Pokémon world is really, *really* weird, but damn if it isn't engaging. The kids today don't know what they're missing with their backlights and wireless syncing and lithium ion batteries! But this old man digresses.

My last Nintendo device was the 2DS, a stripped-down version of the previous-generation 3DS. I didn't care for the 3D effect of the original, so I would have left it turned off. The flat, if ugly form-factor fit in my backpack with my laptop better than it's chunkier bretherin. It had decent battery life, was compatible with the same 3DS and DS games, and was much cheaper... even if at times I felt like the only person in the universe who liked and preferred his.

*(Unfortunately, I'll bet there were kids who were picked on for having "the wrong one", because some kids are special. Being a grownup on a train with the uncool console is a breeze by comparison)!*

The Nintendo Switch Lite follows in this same vein. It's everything I want in a portal game console, with none of the extras. Clara joked that when she was buying it for me for Christmas, the person serving her at the store said she'd "like to have a word" with someone who'd go out of their way to prefer a Lite. I'm sure it was said in jest, but it is surprising how hardwired we are to [always want the best of something](https://rubenerd.com/we-dont-always-need-to-extract-maximum-value/).

<p><img src="https://rubenerd.com/files/2021/ds-lite@1x.jpg" srcset="https://rubenerd.com/files/2021/ds-lite@1x.jpg 1x, https://rubenerd.com/files/2021/ds-lite@2x.jpg 2x" alt="Photo comparing my Switch Lite to the Nintendo 2DS" style="width:500px; height:333px;" /></p>

A *huge* part of the Switch's brand messaging (to the point where it's part of the console's logo) is its detachable JoyCons. The Switch Lite doesn't have these, nor can it be attached to a TV. I can see where more social gamers would feel like this loses a big part of the Switch's appeal, but I liken it to an introvert's console! It's to be played on the couch, or the train.

For me, these tradeoffs come with some big upsides. The Lite is *noticeably* lighter and thinner than its bigger cousins, yet feels more rigid and solid without those extra parts. It can play almost all the same games, and its [lack of OLED](https://rubenerd.com/toodles-and-michael-tsai-on-oled-iphones/) means photosensitive people can use it in mid-to-low light without migraine-induced nausea.

Colours are another important consideration. I don't know when companies decided that colour choices were to be given to budget options, and that premium users would want the Model T special, but having a teal console is fun. The 2DS's translucent blue case was equally cool. Why does everything always have to be boring?

Clara jokes that I've already inadvertedly called my Lite a DS a few times, but that speaks to its specific appeal. The Lite is my childhood Game Boy with a widescreen and modern takes on my classic games. Exactly what I was looking for :). Thank you Clara for the present!
