---
title: "Using NetBSD’s pkgsrc everywhere I can"
date: "2021-05-24T08:51:25+10:00"
abstract: "And why you should too!"
year: "2021"
category: Software
tag:
- bsd
- freebsd
- linux
- netbsd
- package-managers
- pkgsrc
location: Sydney
---
NetBSD's [pkgsrc package manager](https://www.pkgsrc.org/) is the best thing since sliced bread. Like everything the NetBSD maintainers touch, it's high quality, [well documented](https://www.netbsd.org/docs/pkgsrc/), predictable, and portable to a fault. I use it everywhere I can, from my macOS and FreeBSD laptops to remote Linux machines. This has lead people on social networks to ask me why, and to give examples.

The biggest reason comes down to what I call *digital hygiene*, best described by Merlin Mann as "not storing compost in your vegetable crisper". There's value in disambiguating personal tools and applications from what is required to run the system, because updating one set shouldn't impact the other.

*(I've had people ask me to quantify more about *why* I prefer using the BSDs over Linux where I can, and this is one of the reasons. Their architectural decision to include everything required to bootstrap a system in "base" and delegate other software to their respective package managers makes updates easier and less error prone. It comes with other benefits, but that's for another post).*

I use pkgsrc on systems for the same purpose. I use the system package manager to install software I need for my desktop environment on workstations, or services on servers, then let pkgsrc handle tooling that I've got more flexibility to break and tinker with. It maintains its own tree in `/opt`, which means in a pinch I can blow the whole thing away without affecting the rest of the system. This is especially useful on Debian given most Linux distros make no distinction between system and application packages. I even use it on FreeBSD where a dedicated jail for each project might be overkill, or when I want to maintain the exact same versions of tools across my FreeBSD and Mac machines. Did I mention how portable pkgsrc is!?

Speaking of macOS, what about Homebrew? I use it to install graphical applications, but by design it's no longer possible to easily modify build options. It's part of a broader trend on the Mac to protect users from themselves and to deliver *better experiences*, but it gets in the way of my work. MacPorts is far more useful, but I use pkgsrc everywhere else so it made sense to stick with the tools I know.

*(My first post about [pkgsrc on Mac OS X](https://rubenerd.com/p2595/ "Notes on using NetBSD’s pkgsrc on Mac OS X") in 2008 mostly still applies today, but you're best looking at the official docs).*

It's also simplified my life. I used to maintain multiple package and config managers for every tool I used, but thesedays I delegate it to pkgsrc. Just this morning I updated a new system with some of the essentials I need:

	# pkgin install perl ImageMagick p5-PerlMagick p5-DBD-SQLite \
        p5-XML-XPath tex-latex tex-latex-bin tex-standalone

pkgsrc is also a capable package manager for systems that either don't ship with one, or where you have limited access. I used to run it on Slackware and illumos, and maintained a pkgsrc tree in my home directory on my university's RHEL system (cough). I need to get back into illumos again; by putting this on the record here I hope it'll be more difficult for me to shirk this responsibility! It should go without saying that it works well on NetBSD too :).

The [pkgsrc website](https://www.pkgsrc.org/) lists the various OSs it supports, and how you can give it a try. Those on macOS, illumos, and CentOS can also use the [binary packages](https://pkgsrc.joyent.com/) generously supplied by Joyent.

