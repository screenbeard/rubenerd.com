---
title: "Proposed Coventry linear park"
date: "2021-12-15T10:24:12+10:00"
abstract: "Looks nice."
thumb: "https://rubenerd.com/files/2021/linear-park-coventry@1x.jpg"
year: "2021"
category: Thoughts
tag:
- environment
- nature
- transport
location: Sydney
---
I dislike freeways, and not just because [expanding them doesn't work](https://www.youtube.com/watch?v=NcUx5r_ksk8 "City Beautiful: Why are we still widening highways in US cities?"). But at least sometimes we can get some parkland after all the disruption that comes from their construction. Like getting a flower from someone after they've clubbed you with a wet fish. Some Monty Python references work better than others.

This [proposed park](https://www.coventrytelegraph.net/news/coventry-news/new-park-running-under-coventry-22410215) *underneath* a ring road in Coventry in the UK looks great. It'd turn this part of a walking and cycling commute from a dingy chore to something I'd look forward to.

<p><img src="https://rubenerd.com/files/2021/linear-park-coventry@1x.jpg" srcset="https://rubenerd.com/files/2021/linear-park-coventry@1x.jpg 1x, https://rubenerd.com/files/2021/linear-park-coventry@2x.jpg 2x" alt="Press photo showing the proposed park under the ring road overpass." style="width:405px; height:269px;" /></p>

My only question when I see such plans is: how will the greenery grow without direct sunlight? I'd long assumed the reason why land *under* so many freeways were barren wastelands was owing to plants not being able to grow there. Is there foliage that can thrive in indirect sunlight? If so, those barren wastelands are a choice by their respective councils or governments, which we need to tackle.

Singapore is famous for all its trees, vines, and gardens they [meticulously plant around freeways](https://commons.wikimedia.org/wiki/File:Pan_Island_Expressway._7_July_2017.jpg) and other road infrastructure. Now that I think about it, a lot of that would be out of direct sunlight, too. Any horticulturists out there?

*(It's perhaps a bit ironic that the closest I've ever come to finishing writing a light novel had horticulturists as the protagonists. You'd think I'd know this stuff).*
