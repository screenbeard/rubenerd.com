---
title: "Opting out of Google’s FLoC"
date: "2021-04-18T20:19:50+10:00"
abstract: "Adding a Permissions-Policy: interest-cohort=() header isn’t the be all, end all."
year: "2021"
category: Internet
tag:
- privacy
location: Sydney
---
News has been moving fast about [Google's FLoC system](https://www.eff.org/deeplinks/2021/03/googles-floc-terrible-idea). If you don't know what it is, look at where it's coming from and you can probably guess. Last Thursday [blog posts were recommending](https://paramdeo.com/blog/opting-your-website-out-of-googles-floc-network) the addition of this line to your web server software, which I did:

    Permissions-Policy: interest-cohort=()

[Rohan Kumar elaborated](https://seirdy.one/2021/04/16/permissions-policy-floc-misinfo.html) that while this is technically correct, it's not always necessary, nor the best way to do it:

> If your website does not include JS that calls document.interestCohort(), it will not leverage Google’s FLoC. Explicitly opting out will not change this.

He recommends not using third-party scripts, and implementing a [Content Security Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) header to block execution. I've been doing this for years; it's good advice in general.

