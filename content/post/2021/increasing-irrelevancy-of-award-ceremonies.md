---
title: "Increasing irrelevancy of award ceremonies"
date: "2021-04-28T17:49:36+10:00"
abstract: "“Boredom with ritzy galas” and a “deeper shift” in the entertainment world is good news for independent media."
year: "2021"
category: Media
tag:
- independent-media
- music
- quotes
- video
- youtube
location: Sydney
---
Hot on the heels of [my post about Practical Engineering](https://rubenerd.com/practical-engineering-on-cavitation/) which discussed independent media, The Economist ran an article on the [increasing irrelevancy](https://www.economist.com/books-and-arts/2021/04/24/and-the-winner-iswho-cares) of award ceremonies. It made a connection I hadn’t considered:

> ... the decline of interest in arts awards has been long in the making. It signifies growing boredom with ritzy galas, in an age when lots of stars broadcast directly to Instagram themselves.

And more broadly:

> ... it is evidence of a deeper shift in the entertainment world, in which the common popular culture that awards shows celebrate is itself being eroded.

They drew a line directly to streaming services, and the rise of algorithms that determine what people watch. There is no concentrated source of media like TV networks; we all watch our own curated mixes of Netflix, YouTube, et.al.

The temptation is there to think of this as a bad thing. I am *not* a fan of streaming platforms as they currently stand; they pay artists an absolute pittance, while patting themselves on the back for “solving” piracy. I've also written about the serious problems in [YouTube's recommendation algorithms](https://rubenerd.com/ann-reardon-on-viral-fake-food-videos/), including the promotion of <del>misinformation</del> lies. I don’t even think streaming is the most technically efficient way of spreading media either; distributed protocols like BitTorrent do a better job than a media silo and CDNs.

But there's good news. I like that I have more of a say in what I watch, read, and listen to now. Algorithms are an issue, but I can still click elsewhere. Cable or broadcast television wouldn’t carry videos about how to recap a Commodore 128, or Hololive EN characters that target an intersectional niche of Western anime and Minecraft fans! It's the same with with music charts, which already weren’t important to me, but now has far less impact on who’s successful or can make a living creating the art that makes our world a nice place to live.

Michael Franks [dedicated an entire song](https://rubenerd.com/mr-smooth/) to a smooth jazz network’s stranglehold of the medium in the 1980s. It’s come with problems we need to work through, but on balance I’d say there's room for optimism.

