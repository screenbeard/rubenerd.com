---
title: "Hales on Javascript-loaded assets"
date: "2021-10-20T08:00:21+10:00"
abstract: "Using dummy placeholder images to lazy load assets, and the issues this causes."
year: "2021"
category: Internet
tag:
- accessibility
- scripting
location: Sydney
---
Last Sunday I mentioned I was [back on the NoScript train](https://rubenerd.com/using-noscript-in-2021/) again, in part because so much dynamic content on the modern web is frustrating. Hales of [Halestrom.net](http://halestrom.net/darksleep/) alerted me to something else I wasn't aware of:

> Some sites do actually send you a page, but hide it visually unless JS is enabled.  I wrote a small extension that lets me get past this with a key shortcut.
> 
> Alas many websites don't load their images without javascript.  Often the img tags look like this:
> 
> <code>&lt;img src="placeholder.png" 
>	actual-resource="/bob/cavernousdogs.jpeg" /&gt;</code>
> 
>  The JS literally switches the tags around as you scroll (to lazy load).  I need to write an addon that looks for such cases and fixes them for you, so you don't have to deal with their giant javascript popups just to read their page.

I can see the point of lazy loading, but it sure flies in the face of accessibility.

While I'm talking about Hales, check out his [most recent ThinkPad repair job](http://halestrom.net/darksleep/blog/047_x131e_repair/) too. I'm in awe of engineers who can understand hardware at that level and bring something back to life like that. For the sake of the planet as much as anything else, we should be *encouraging* this.
