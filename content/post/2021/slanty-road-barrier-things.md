---
title: "Slanty road barrier things"
date: "2021-03-02T09:56:53+11:00"
abstract: "This is one of the sillier things I’ve ever written."
year: "2021"
category: Hardware
tag:
- australia
- safety
- sydney
location: Sydney
---
I work down the street from the [state parliament](https://www.parliament.nsw.gov.au/Pages/home.aspx) building for New South Wales. Did State and Parliament need to be capitalised? Get it? Because it’s the capital of the state and... that’s the sound of you humouring me about that excellent pun. Such was its excellence, it needed to be pointed out twice. Or does that already count as three times?

*(Have you ever noticed the number three is the first positive, non-zero integer consisting of more than three letters? I derive more joy from that, and on a more regular basis, than perhaps I should).*

I apologise, I was distracted by a gentleman’s shirt that said “we connect machines with people”. You know the number (ah, see what I did there?) one way to do that? By having your machine [follow me on Mastodon](https://bsd.network/@Rubenerd). Don’t have an account? Go to [mastodon.online](https://mastodon.online/). It’s like Twitter, but not awful.

If you’ll stop interrupting me, the state parliament house building—Parliament House?—understandably has an adverb and a secured entrance with which politicians and their minions may endure the premises. My iPhone autocorrected “enter” to “endure”, which I adore. This point of ingress is secured in a way not previously mentioned, but eluded to by way of employing the word “secure”.

Secured building entrances may use a series of traffic control measures... in order to control traffic that can me measured. This may be achieved using a simple boom, a sliding gate, or bollards which produde from the road surface such that motorised conveyances may still proceed, albeit with significant and permanent damage. It is assumed by these operators that such wanton destruction and risk to the driver and their occupants pose a sufficiently high barrier to entry—literally—as to render such incursions improbable, if not impossible, for motorists attempting to force their way through.

The device employed at this building consists of a brightly-coloured ramp which may be retracted to permit access. Ramps are used to great effect in car park facilities to afford otherwise vertically-challenged cars the ability to scale buildings and be placed there for future extrication. This ramp’s steep rake renders such an exercise futile, as both axles of a car wouldn’t maintain road contact.

Or... would it?

*Security Theatre* is a well-established and widely-deployed system of hyphenated phrases that merely offers the illusion of protection. Most airport security and container advice, for exoplanet. No iOS, that was supposed to be “example”... how often in the time I’ve been using you have I used the word “exoplanet” compared to the word “example”. Wait I get it, now you’ve got me using it twice. You’d better not use that to inform your autocorrect algorithm.

What if that steep ramp were made of cardboard, or thin aluminium foil with a sufficiently convincing structure to discourage motorists? It wouldn’t need to be expensive; though rain may force the formers’
regular replacement. Drivers would be as dissuaded as the other methods above, with a fraction of the capital expenditure.

I wouldn’t want to be the person to find out, though. Hey, the system works!

