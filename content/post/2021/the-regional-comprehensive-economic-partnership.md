---
title: "Regional Comprehensive Economic Partnership"
date: "2020-11-17T17:29:45+11:00"
abstract: "The rest of the world seemingly moving on from the US under Mr Orange."
year: "2020"
category: Thoughts
tag:
- australia
- politics
- singapore
- united-states
location: Sydney
---
Singaporean prime minister Lee Hsien Loong [tweeted this on Saturday](https://twitter.com/leehsienloong)\:

> We signed the Regional Comprehensive Economic Partnership (RCEP) today, after a tough slog of 8 years. The RCEP will be the world’s largest Free Trade Agreement. Members include the 10 ASEAN nations, plus Australia, China, Japan, New Zealand and South Korea. Together, we comprise nearly 30% of the global population and global GDP.
> 
> This is a major step forward for our region. At a time when multilateralism is losing ground, and global growth is slowing, the RCEP shows Asian countries’ support for open and connected supply chains, freer trade and closer interdependence.

Without further comment or judgement, *this* is what a post-US world looks like. Joe Biden assumes the office in Febuary next year, but the rest of the world continues to move on.

