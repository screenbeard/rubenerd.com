---
title: "The Men’s 50 Kilometre Race Walk"
date: "2021-08-06T09:04:26+10:00"
abstract: "And a silly record I set in primary school!"
year: "2021"
category: Thoughts
tag:
- health
- olympics
- sport
location: Sydney
---
Hey everyone. I'm dealing with a difficult situation right now, so my posts for the coming week will be brief or dusted off from my exhaustive drafts folder. There's some fun and interesting stuff in there, weirdly.

But I did need to jump on to correct the record before I semi-log off. I completely forgot about the Men's Race Walk in my [list of the best Olympic events](https://rubenerd.com/harukana-receive-and-olympic-sports/)! I've been following [The Guardian's Olympic page](https://www.theguardian.com/sport/live/2021/aug/04/tokyo-2020-olympics-110m-hurdles-golf-diving-cycling-and-more-live) about the event in progress. I love how diverse a group of people it attracts, from all walks (hah!) of life and age groups. I liken it to one of the great equaliser events.

It probably doesn't stand today, but I set a record during a charity walkathon at my primary school back in the day. Each lap around our school's oval was a dollar or two towards a charity; I volunteered mine to [MS Australia](https://www.ms.org.au/). I kept going until a teacher came to pat me on the shoulder and say the event was over, and that I was the only one left. I felt like my little legs could have gone for another dozen laps at least!

I was never an especially athletic person, but I have all the time in the world for walking and power walking. A brisk morning stroll is about the only thing that wakes me up in the morning; another reason lockdowns have been tough.

