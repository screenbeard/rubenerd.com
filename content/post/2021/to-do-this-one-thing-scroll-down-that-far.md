---
title: "To do this one thing, scroll down that far!"
date: "2021-12-17T15:59:29+10:00"
abstract: "Six presses of the spacebar, four banners, and 180 words later...!"
year: "2021"
category: Internet
tag:
- accessibility
- design
location: Sydney
---
I know this will sound *incredibly* rich coming from arguably one of the most verbose bloggers on the planet, but hypocricy isn't the only river in Egypt. Wait, that's not how that goes.

I needed a specific key command to break out of a console in a certain hypervisor. I couldn't rememeber what it was, so I searched online and found someone's site explaining it.

From the top of the page to where the key combination was, it was:

* six presses of the spacebar
* four banners
* 180 words
* one slideout chatbot
* one newsletter popup

Lesson learned: if I need to share a tip or remember something here, I'll put it at the very top of the post, then explain after.

