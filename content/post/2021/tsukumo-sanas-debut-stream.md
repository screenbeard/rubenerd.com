---
title: "Tsukumo Sana’s debut stream #sanallite"
date: "2021-08-26T08:30:34+10:00"
abstract: "m88888!"
thumb: "https://rubenerd.com/files/2021/yt-3Tv5GyebhQo@1x.jpg"
year: "2021"
category: Anime
tag:
- hololive
- tsukumo-sana
- video
- youtube
location: Sydney
---
Clara and I are *slowly* catching up with latest generation of [Hololive English](https://www.youtube.com/channel/UCotXwY6s8pWmuWd_snKYjhg) debuts, which we’re watching entirely out of order. I say both of us, though of course *she* has seen them all already. Last night was for the HoloCouncil’s Speaker of Space, Tsukumo Sana.

<p><a href="https://www.youtube.com/watch?v=3Tv5GyebhQo" title="Play 【DEBUT STREAM】BIG STREAM🪐 #holoCouncil #hololiveEnglish"><img src="https://rubenerd.com/files/2021/yt-3Tv5GyebhQo@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-3Tv5GyebhQo@1x.jpg 1x, https://rubenerd.com/files/2021/yt-3Tv5GyebhQo@2x.jpg 2x" alt="Play 【DEBUT STREAM】BIG STREAM🪐 #holoCouncil #hololiveEnglish" style="width:500px;height:281px;" /></a></p>

*Did you know that when you trip into a black hole you turn into spaghetti? EVERYTHING WAS A MISTAKE!*

I'd been spoiled a bit by being on social media for a few days before watching this stream, but seeing that meat pie floating above the celestial sky to the sound of her broad Australian accent, punctuated by that triumphant *m8* was nothing short of glorious! Not to be *one of those people*, but I could even hazard a guess to her background and where specifically in Australia she's from, based on having spent so much of my time living back here in that community... but let's leave it at that. *Shifty eyes*.

It's hard to explain to non-Aussies and Kiwis, but her deadpan joke delivery, timing, and her expectant expressions were *absolutely* spot on. I was worried that someone from our part of the world would feel compelled by Cover or her audience to conform to how other streamers speak, but I'm so happy she was able to be herself. 

Multi-diciplined creative people are among the worst human beings in the universe, but as the literal embodiment of the aforementioned time and space plane her graphics, production values, and introductiory video were such a joy. Her <del>NASA</del> SANA logo was my favourite touch.

Having had some time to digest (the meat pie) afterwards, I think I've finally figured out who and what her art reminds me of. Her eyes, facial expressions, and all the space-themed graphics harken back a bit to those late 1990s to early 2000s sci-fi series (think Diebuster). The other girls in the HoloCouncil are thorughly modern, but Sana has this retro vibe going that I *love*. 

Sana was bubbly, entertaining, and just the right amount of silly we need during *These Times*.

