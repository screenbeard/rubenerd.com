---
title: "Today I learned what ISCO-08 was"
date: "2021-06-24T14:07:46+10:00"
abstract: "The International Standard Classification of Occupations lists Technical Writers under the same section as poets. That feels like I’m in more creative company than I deserve."
year: "2021"
category: Media
tag:
- work
- writing
location: Sydney
---
I was helping someone with metadata on her website, when we realised that you can use <a href="https://schema.org/">Schema metadata</a> to define the occupation of a <a href="https://schema.org/Person">Person</a> with, surprising though it may seem, <code>hasOccupation</code>. The <a href="https://schema.org/Occupation">Occupation</a> type includes the following attribute:

<blockquote cite="https://schema.org/Occupation">
<p><strong>occupationalCategory</strong></p>

<p>A category describing the job, preferably using a term from a taxonomy such as BLS O&#8727;NET-SOC, ISCO-08 or similar, with the property repeated for each applicable value.</p>
</blockquote>

ISCO is the <cite>International Standard Classification of Occupations</cite>, as published by the International Labour Organization. I wonder if their title was a compromise between the correct spelling of Labour, and the incorrect spelling of Organization?

<a title="ISCO-08 Structure, index correspondence with ISCO-88" href="https://www.ilo.org/public/english/bureau/stat/isco/isco08/">ISCO-08</a> is the most recent iteration of the <cite>Index of Occupational Titles</cite>. Naturally I downloaded the PDF, like a gentleman, to see if and where they classify Technical Writers. On page 178:

<blockquote cite="http://www.ilo.org/public/english/bureau/stat/isco/docs/groupdefn08.pdf">
<p><strong>2641 Authors and Related Writers</strong>
 
<p>Authors and related writers plan, research and write books, scripts, storyboards, plays, essays, speeches, manuals, specifications and other non-journalistic articles (excluding material for newspapers, magazines and other periodicals) for publication or presentation.</p>
</blockquote>

Among the tasks listed:

<blockquote cite="http://www.ilo.org/public/english/bureau/stat/isco/docs/groupdefn08.pdf">
<p><strong>b)</strong> conducting research to establish factual content and to obtain other necessary information;</p>

<p><strong>d)</strong> analysing material, such as specifications, notes and drawings, and creating manuals, instructions for use, user guides and other documents to explain clearly and concisely the installation, operation and maintenance of software, electronic, mechanical and other equipment;</p>
</blockquote>

I'm in far more creative company than I deserve here: 

> Author, Book editor, Essayist, Indexer, Interactive media writer, Novelist, Playwright, Poet, Script writer, Speech writer,  Technical communicator, Technical writer, Writer

I'm also a "systems designer (IT)" in their parlance, which sits under <cite>2511 Systems Analysts</cite> on page 144. There are no poets in that classification, unfortunately.

