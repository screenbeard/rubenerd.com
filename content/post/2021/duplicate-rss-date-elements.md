---
title: "Duplicate RSS 2.0 date elements"
date: "2021-06-21T14:01:37+10:00"
abstract: "pubDate and dc:date are not semantic duplicates, despite what validators may say."
year: "2021"
category: Thoughts
tag:
- dublin-core
- metadata
- rss
- stadards
- web-feeds
location: Sydney
---
Here's some RSS minutea for your Monday. For the longest time I've been exporting feeds with Dublin Core **dc:date** elements on each item:

	<?xml version="1.0" encoding="UTF-8"?>
	<rss version="2.0" 
		xmlns:dc="http://purl.org/dc/elements/1.1/">
		
	<channel>

	<title>Example</title>
	<link>https://rubenerd.com/feed/</link>
	<description>An example RSS feed</description>
	<pubDate>Sat, 19 Jun 2021 19:07:53 +1000</pubDate>	
	<dc:date>2021-06-19T19:07:53+10:00</dc:date>
		
	<item>
	<title>Hello, world</title>
	<pubDate>Sat, 19 Jun 2021 19:07:53 +1000</pubDate>
	<dc:date>2021-06-19T19:07:53+10:00</dc:date>
	</item>
		
	</channel>
	</rss>

Validators recommend against [duplicating semantics](https://validator.w3.org/feed/docs/warning/DuplicateSemantics.html):

> Your channel contains two elements which mean the same thing. This can occur in RSS 2.0 when you mix core elements and namespace elements. This can confuse news aggregators and RSS parsers, since there are no universally accepted rules about which element takes precedence.
> 
> **Solution:** Remove one of the redundant elements.

I assert they're don't mean the same thing, and are therefore not redundant!

The [RSS 2.0 spec](https://cyber.harvard.edu/rss/rss.html#ltpubdategtSubelementOfLtitemgt) says this about **pubDate**:

> Its value is a date, indicating when the item was published. If it's a date in the future, aggregators may choose to not display the item until that date. 

This isn't analogous to [Dublin Core's date](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/terms/date/):

> A point or period of time associated with an event in the lifecycle of the resource.

In other words, **pubDate** is narrowly defined as publication date, whereas **dc:date** is broader in semantic scope. They may be used for the same thing, but one could imagine dc:date referring to an event the item describes, whereas **pubDate** is when that description was published. Think of an anime convention that has a cosplay content next Thursday, being announced in a post today.

*(The same thing applies to Atom's **updated** element, and Dublin Core's **dateSubmitted**).*

But what if they do mean the same thing? Calling them redundant ignores the question of precision. RSS 2.0's **pubDate** (unfortunately) permits 2-digit years, as per [RFC-822](https://www.ietf.org/rfc/rfc822.txt). **dc:date** mandates 4-digit years as per [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html). **pubDate** is expressed with 4-digit years by convention, but it's conceivable and entirely valid for **dc:date** to provide additional information than what **pubDate** has.

Which leads us to the justification for removing **dc:date**, so as not to "confuse news aggregators". As someone who maintains and builds aggregators, I don't buy this. I wouldn't think anything introduced with namespaces would take precedence over a mandatory element like **pubDate**.

Does any of this matter? I mean, does *anything*, right!?

