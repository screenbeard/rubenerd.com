---
title: "Greyscale screens for anxiety"
date: "2021-09-30T08:54:44+10:00"
abstract: "Passing on for any of you dealing with this too."
thumb: "https://rubenerd.com/files/2021/screenshot-macos-greyscale.png"
year: "2021"
category: Hardware
tag:
- accessibility
- anxiety
location: Sydney
---
I'm not sure if it's months of Covid lockdown amplifying what was already there, but my anxiety boiled over this week. Clutter affects me negatively, so I've been trying to make my workspaces cleaner and more deliberate. Anything to restore a sense of control, really.

One thing I didn't expect to work *so well* was having my computer screen set to greyscale mode the past few days. I'd enabled it to test the accessibility and contrast of a design I was working on and... I didn't turn it off.

<p><img src="https://rubenerd.com/files/2021/screenshot-macos-greyscale.png" alt="Screenshot of the macOS Accessibility preference pane, showing greyscale colour mode selected" style="width:500px; height:333px;" /></p>

Everything feels clearer. All my words, terminal windows, chat applications, email, and documents are still there, but with fewer distractions and less stimulation. It's like I'm writing on backlit paper, not interacting with a telescreen.

I've got the Mac Accessibility Shortcut pane in my menubar, so I can toggle colour back on for games like Minecraft, or YouTube videos. But then the work day begins again, and so does greyscale mode.

I have no scientific or medical basis for doing this, and I'm not sure how long I'll keep it up. All I can say, anecdotally, is that it's helped me. Give it a try if you think you could benefit too.

Bonus points: I wonder if KDE offers a similar mode?
