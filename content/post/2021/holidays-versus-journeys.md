---
title: "Holidays versus journeys"
date: "2021-04-09T12:00:21+10:00"
abstract: "“It's cheap to label anything that isn't work or study a holiday”."
year: "2021"
category: Travel
tag:
- quotes
location: Sydney
---
Leonie Doyle gave [food for thought](https://twitter.com/tomwconnell/status/1380333367405682691) this mornng:

> It's cheap to label anything that isn't work or study a "holiday". There are myriad valid reasons why people make journeys.

This was in response to Australian tourist minister saying that business trips and interational student travel would be prioritised over people taking holidays.

*(I've also been told by people who'd know that we use **holiday** for what Americans call **vacations**. But that doesn't sound the same to me somehow, let me know if I've been misinformed).*

