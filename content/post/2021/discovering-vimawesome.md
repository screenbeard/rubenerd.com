---
title: "Discovering Vim Awesome"
date: "2021-09-07T08:57:33+10:00"
abstract: "I was looking for a similar experience to browsing MELPA for Vim. This site is so cool."
year: "2021"
category: Software
tag:
- editors
- vim
location: Sydney
---
Today I found out about the [Vim Awesome](https://vimawesome.com) site:

> Vim Awesome is a directory of Vim plugins sourced from GitHub, Vim.org, and user submissions. Plugin usage data is extracted from dotfiles repos on GitHub.

My experiment with emacs wrapped up last month (a topic for another post), and this gets the closest to giving me a similar experience to the MELPA web interface for Vim. I love the breakdown of plugin types, and the ability to search and sort results.

I tend to keep pretty lean config and only run a few plugins, but it helped me a discover a few new gems like [Syntastic](https://vimawesome.com/plugin/syntastic) and the energetically-titled [TOAST!](https://vimawesome.com/plugin/toast-vim)
