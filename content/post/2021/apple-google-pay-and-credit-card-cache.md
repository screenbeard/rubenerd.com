---
title: "Credit card cachet in an era of phone payments"
date: "2021-05-31T11:33:41+10:00"
abstract: "Mobile phone payment processors are now the front for purchases, not a fancy credit card."
year: "2021"
category: Thoughts
tag:
- banks
- behaviour
- finance
- sociology
location: Sydney
---
Smartphones have up-ended so many industries and business models, and I'm now wondering if it'll do the same thing to credit cards.

Credit and charge cards were originally physical tokens of exclusivity. People flashing their Diners Club messaged to the world that they had connections to be invited or register, that they could service the fees, and that they frequented expensive places to make use of it. Receiving an itemised bill each month was far more convenient than paying with physical cash and bothering with coins, but I'm sure their *cachet* was just as important.

That veneer rubbed off slightly with the ubiquity of charge cards that ended up being the likes of JCB, Mastercard, and Visa. Even those without a line of credit could use them in place of Singapore's NETS or Australia's EFTPOS networks by the 2000s, with debit card options tied to bank accounts. We've even seen one-time gift cards, vouchers, and travel cards make use of these networks, to the point where most of us consider them utilitarian.

Certain payment processors and banks still go out of their way to cultivate that exclusive image. I've seen marketing material refer to credit cards with such euphemisms as *passports*, *tickets*, or *keys* to a better life. Companies like Amex went as far as to introduce their limitless Black card in response to urban legend.

But much of this is predicated on having that physical card. I'm sure half the reason people service gold or platinum card fees is to have the card to impress people; or at least, to feel like you're part of an exclusive club. It's not an accident that certain card companies call it your *membership* instead of an account.

Contactless payments were the first inkling I saw of that shifting. No longer did you need to present your card to someone, or even slot it into a card terminal yourself. You could merely wave your wallet in the direction of a PayPass-style card reader and have it register. Suddenly, you could have an ultra-exclusive, luxury card with free yacht trips in it, or a Discover. Who would know?

Phones take it one step further by abstracting the card away entirely behind their own payment systems. Apple Pay, Google Pay, or one of the dozens of new Chinese payment processors are what we use to pay for things now; the credit card itself is just the source of funds. This has lead to the rise of "pay later" services that eschew (gesundheit!) cards entirely.

Point hackers, businesses, or those who have practical reasons for having specific cards will continue to apply for and use them, but I wonder if this abstraction will eventually lead to these cards falling out of favour, like they have from so many wallets?

