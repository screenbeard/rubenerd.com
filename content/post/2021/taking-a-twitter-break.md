---
title: "Taking a Twitter break"
date: "2021-01-07T12:50:00+11:00"
abstract: "Insert wave emoji here"
year: "2021"
category: Internet
tag:
- social-media
- twitter
---
Everyone I follow on Twitter is awesome, and it's my primary form of contact for so many friends. Heck, I got to know my long-term partner Clara from using it. But the news is all a bit much.

Once more, if I need to ask whether social networks are good for me or not, and feel the need to announce my departure, I probably already know the answer.

Cheerio, feel free to reach out via [email](https://rubenerd.com/about/#contact).
