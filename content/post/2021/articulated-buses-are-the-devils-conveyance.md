---
title: "Articulated buses are the devil’s conveyance"
date: "2021-12-29T09:19:29+11:00"
abstract: "I refuse to be corrected on this!"
year: "2021"
category: Thoughts
tag:
- buses
- public-transport
location: Sydney
---
I'm not religious, but if the road to hell is paved with good intentions, articulated buses are the public transport that drives on them. This post explores the accurate reasons why I've made such an astute observation of accurate astuteness:

* They're **loud**. I'm not sure why they're exempt from implementing the same soundproofing as in almost every other modern bus, but you do *not* want to be on a street corner as one roars past. You know how you're not supposed to stare directly at an eclipse? You're not supposed to directly hear one of these six-wheeled monstrosities.

* They're **unpredictable**, or at least, drivers think they are. From my balcony I routinely see motorists who think they can get around the articulated bus that's making a corner. They almost never can, because basic laws of physics get in the way. And forget about it if you're a pedestrian or cyclist.

* They're **uncomfortable**. In a stroke of engineering genius, the designers managed to make bus ride quality *worse*. The entire back third of the bus pivots up and down, not just left and right when turning. Bob bob bob... hope you don't get motion sickness!

* Their **drivers don't care**. Some do, others are fine with blocking entire intersections in any direction by stopping right in the middle of them. Traffic is unavoidable, how you deal with it is what counts. These buses routinely can't.

* They're **inefficient**. You kinda sorta save a bus driver for every two or three of these, but that's it. They take up more road space than an equivalent double-decker (given sufficient clearance for their operation), and the long, flexible accordion section can't reasonably carry passengers. We lose a lot for this gimmick.

* You **can't be polite**. I always shout out thanks to drivers before alighting. Unless you can project like Shaggy, good luck being heard over the sound of the infernal engine from the back doors.

* They're **ugly**. This may be the biggest offence of all. Some of us were born this way! But buses are an artificial construct; there's no reason for them to look like tissue boxes stuck together with a half-used tube of toothpaste.

These deplorable contraptions work better when they have permanent right of way, such as in a bus-rapid transit system, or sections of Adelaide's pioneering O-Bahn. Otherwise, it's time to make some pretty coral reefs out of them; assuming ocean life would want to be seen attached and growing on such despicableness.

I'd say this post was tongue in cheek, but the back of this articulated bus just hit a pothole, and made me bite it. Okay not right now, I'm remembering back to an incident earlier this year. Grr articulated buses; bad enough that I can remember an experience riding in you all these months later.
