---
title: "Rediscovering one of our family computers"
date: "2021-04-04T07:59:17+10:00"
abstract: "Finding the case from a childhood computer on eBay... from Latvia!"
thumb: "https://rubenerd.com/files/2021/our-p1-case@1x.jpg"
year: "2021"
category: Hardware
tag:
- family-486
location: Sydney
---
We were an IBM-compatible family growing up. Our first machine was a 486SX tower, then we got a 90 MHz Pentium 1 when we moved to Singapore. My dad worked for a multinational that had him using PS/2s and XTs during the 1980s, then those legendary beige Toshiba laptops with the outboard trackballs. I vaguely remember the Apple IIs we used at school, and my grandfather had Commodores. But all my meaningful formative memories were formed staring at a glowing DOS prompt.

Over time these machines developed problems that I could probably easily fix now, but at the time were considered too far gone. In a decision I still kick myself for not pushing back on harder at the time, my parents threw both these machines away... though mercifully not before I could salvage a few parts.

Recently I also came across a treasure trove of documentation from cleaning my dad's house, which including technical specifications and sales orders for both these machines, along with my own first computer I built in primary school (that I still maintain to this day)! So I decided I'm going to make it a mission, among many others, to rebuild these machines as close as I can.

The cases are arguably the biggest challenge. For all the problems that restoring vintage Commodore computers introduce, you can at least be fairly sure that a 64C looks like a 64C. How do you do an eBay search for "old AT case?"

<p><img src="https://rubenerd.com/files/2021/our-p1-case@1x.jpg" srcset="https://rubenerd.com/files/2021/our-p1-case@1x.jpg 1x, https://rubenerd.com/files/2021/our-p1-case@2x.jpg 2x" alt="Photo from eBay showing a mini-AT beige tower bezel." style="width:420px" /></p>

Well, *turns out*, I did find one of them! Once again that mythical land of Latvia entered my life, with someone selling the otherwise-unremarkable face plate for that original Pentium 1 machine. It had *ten* watchers on eBay, presumably owing to more than a few of us having nostalgic memories for it.

It might need a bit of a clean based on the photo on the auction above, but I'm *so happy* I was able to snap this up. There were a few different case designs by this company, so my hope is to source another one locally and save on international shipping, and affix this face plate to it instead.
