---
title: "Speed-reading app advertisements"
date: "2021-05-23T09:11:20+10:00"
abstract: "That’s a different definition for reading."
year: "2021"
category: Thoughts
tag:
- memories
- reading
location: Sydney
---
I've been seeing advertisements on Twitter like this:

> Discover $SERVICE, an app that makes it possible to read and listen to the big ideas from the world's most influential nonfiction books in just 15 minutes.

They then present a table of books you'll be smashing through during a given month, with the implication that without their service you'd be reading far less.

This qualifies for a *you do you!* More power to their customers who's motivation is to read as many books as they can, in as short a time period as possible. But I couldn't imagine a worse way to read books.

It reminded me of one specific high school English class. Ms Gravina told us of a student of hers who claimed to have read War and Peace and a few Jane Austin books, but didn't understand half of it. It spawned a discussion on reading comprehension, and whether one is really *reading* something if they're only seeing words on a page. I've come to love Jane Austin, though I'll bet I'm still missing some context even now.

Ironically, it was that same class where I admitted to reading fewer than a dozen books a year, which invited surprise and ridicule. I like to lose myself completely in what I'm reading; it's not unusual for me to reread entire chapters, take notes, draw diagrams, and quote passages in my journal that made me think about something. Non-fiction books lead me to wanting to learn more about a topic, whether it be through podcasts or finding a subject expert who blogs about it. I'm especially fascinated by worldbuilding in fiction books too. This reading style falls afoul of a metric that prioritises breadth over depth.

I read even fewer books today; probably in the range of eight to ten a year. Part of this stems from competing uses of my time, and the volume of blogs I still read via RSS every day. I would like to read more, but for me that will come from adjusting priorities, not speed reading services.

