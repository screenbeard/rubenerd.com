---
title: "Drowning in popups, part two"
date: "2021-01-07T09:26:00+11:00"
abstract: "Didn't we deal with this the first time?"
year: "2021"
category: Internet
tag:
- accessibility
- design
- popups
---
Popup windows were one of the biggest scourges of the early web. They seemed to popup&mdash;heh&mdash;overnight, with even reputable sites throwing them in our faces, and even sneakily hiding them behind our active browser window so we'd see them later. It got so bad that an entire industry of popup blockers surfaced, and browsers like Phoenix and Opera touted their native popup blocking as a key feature when compared to IE.

We're living through the same thing now, only I'd argue it's even more pernicious. Popups are now done through CSS and JavaScript, making them much harder to detect and block. They screw up the flow and order of text on a page, making them bad for accessibility. And once again, web designers and executives are still under the misguided idea that they're not only tolerable this time around, they're desirable and reflect well on the sites that employ them. On certain narrow metrics that disregard users, they probably are. But they're a disaster as an industry trend.

*(It reminds me of the old adage about enterprise software not being bought by the people who have to use it. That obervation has haunted me since).*

I tweeted an especially bad example last year from CityLab, a blog I used to love reading. It started life as a category on the Atlantic's site, before getting its own site, then being spun off entirely. They were then bought by Bloomberg, and the UI has gone downhill since. Take a look:

<img src="https://rubenerd.com/files/2021/citilab-spam@1x.jpg" alt="Screenshot from Citylab, with multiple layers of popups and redundant navigation bars" srcset="https://rubenerd.com/files/2021/citilab-spam@2x.jpg" />

There is *so* much to unpack here. The UI isn't great to start; the irrelevent black navigation bar forces actually relevant links behind a meatstack icon, which research shows is a bad idea for discoverability and functional usability. The heading takes up too much space. It employs that antipattern of *Sign In* being less noticable than *Subscribe*. Links use the same colour as body text, making them difficult to distinguish. The links to the side of the Bloomberg CityLab text aren't centred correctly. I could go on.

But the most egregous design antipatterns are so bad, they overlap. The first is a banner ad that takes up almost half the screen, obscuring the entire article. Then a popup for a newsletter appears before you even get a chance to dismiss it. All it was missing was a chatbot icon in the corner with a notification and a red unread message bubble.

Once again, I have to take great pains here to say that this situation isn't necessarily the fault of web developers. Print publications have fared especially poorly in their transition to digital, and we've devalued journalism to the point where previously-reputable outfits have to take out half-page adverts to plead for money. These are symptoms of a broken system, and until that's resolved these shenanigans will only keep getting worse.

How's about signing up to our newsletter though? <span style="background: red; color:white; padding:0 1em;"><strong><em>!!!AGREE!!!</em></strong></span> <span style="color:lightgrey; font-size:smaller">disagree</span>.
