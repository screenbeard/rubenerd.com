---
title: "I’m now in @ninomaeinanis’s tentacult"
date: "2021-04-26T09:02:46+10:00"
abstract: "It made sense given how much joy she brings Clara and I every day :)."
thumb: "https://rubenerd.com/files/2021/tentacult@1x.jpg"
year: "2021"
category: Media
tag:
- hololive
- ninomae-inanis
location: Sydney
---
<p><a href="https://rubenerd.com/files/2021/tentacult@orig.jpg"><img src="https://rubenerd.com/files/2021/tentacult@1x.jpg" srcset="https://rubenerd.com/files/2021/tentacult@1x.jpg 1x, https://rubenerd.com/files/2021/tentacult@2x.jpg 2x" alt="Screenshot from YouTube showing Ina’s Takodachi Assemble Q&A stream" style="width:500px; height:248px;" /></a></p>

It made sense given [how much joy she brings](https://www.youtube.com/channel/UCMwGHR0BTZuLsmjY_NT5Pwg) Clara and I every day ^^. I should have done a lot sooner.

