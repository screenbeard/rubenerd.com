---
title: "Alice Di Micele on ageing"
date: "2021-02-04T15:51:02+11:00"
abstract: "I finally started getting it. I'm loving this ageing thing."
year: "2021"
category: Media
tag:
- music
- whole-wheat-radio
location: Sydney
---
Alice Di Micele (paraphrased), before singing Wise Old Woman in a Whole Wheat Radio house concert:

> I finally started *getting it*. I'm loving this ageing thing.
