---
title: "Retr0brighting my Commodore 128 keyboard"
date: "2021-03-02T21:59:51+11:00"
abstract: "It took a couple of attempts, but Oxy Vanish powder works wonders in Australia."
thumb: "https://rubenerd.com/files/2021/retrobright-keys-day3@1x.jpg"
year: "2021"
category: Hardware
tag:
- commodore
- commodore-128
- commodore-128-series
- retr0bright
location: Sydney
---
This is part five (already!?) in my [Commodore 128 series](https://rubenerd.com/tag/commodore-128-series/). I [wrote about Retr0bright](https://rubenerd.com/a-point-to-consider-before-retr0brighting/) back in early February. To recap, involves buying new capacitors. Ah, that was so good.

Retr0brigting is a process that was discovered to reverse the bromide-yellowing of plastics. The Commodore 128 that Screenbeard of [Mastodon](https://aus.social/@Screenbeard) and [Geekorium](https://www.the.geekorium.com.au) fame gave me was already in excellent condition, but the keyboard was a different shade to the case pieces. I'm not bothered by a bit of yellowing from age, but I thought it'd look nicer if the pieces match.

*(Hi, it's Ruben here, from The Future. I can't find my "before" photo of the 128 anywhere! I didn't want this to delay uploading this post, so for now imagine a Commodore 128 with a beige case and yellowed keycaps).*

The [original Retr0bright recipe](https://retr0bright.com/make.html) called for hydrogen peroxide from hair dye developer, some laundry booster as a catalyst, and xantham gum to create a paste that could be applied to the case. I bought some cream developer from Priceline in Sydney that seemed to do the trick for all three, but then I started reading reports of marbling and inconsistent results on smaller pieces like keycaps. Retr0brighters on YouTube now recommend submerging the pieces in warm water and liquid hair developer. Problem was, I could never find any non-cream developer in Australia, and ordering hydrogen peroxide through the post seemed like... a bad idea!

The solution&mdash;wow, I'm on fire with these puns today&mdash;came from a forum or blog post that I'm also blanking on right now unfortunately. But they pointed out that Oxy Vanish powder contains a sufficiently-high concentration of sodium percarbonate which, when mixed with water, forms hydrogen peroxide. The [chemistry checks out](https://en.wikipedia.org/wiki/Sodium_percarbonate#Chemistry).

**Hydrogen peroxide is nasty stuff. By attempting this yourself, you agree to have [read my site disclaimer](https://rubenerd.com/omake/terms/#general-disclaimer) and accept full resposibility for your safety and decision to do so. I suggest gloves at a minimum.**

I gave it a try, or at least attempted to. What proceeded was more than a week of overcast weather in Sydney. I posited that it was the universe conspiring against me! I could have got some UV lamps, but our balcony normally bakes in sunshine so it seemed like a waste. I also had plenty of [other things to fix](https://rubenerd.com/troubleshooting-a-commodore-1541-disk-drive/) and clean in the meatime.

<p><img src="https://rubenerd.com/files/2021/retrobright-keys-day1@1x.jpg" srcset="https://rubenerd.com/files/2021/retrobright-keys-day1@1x.jpg 1x, https://rubenerd.com/files/2021/retrobright-keys-day1@2x.jpg 2x" alt="" style="width:500px" /></p>

Eventually we had a clear day with plenty of sunshine, so I placed the keycaps in 1 L of warm water and 50 g of the Oxy Vanish, and let it sit for 6 hours. I couldn't tell any difference, which was disheartening.

On the next day of sunshine, I upped the concentration and the size of the container. I thought I might not have left enough surface area for each keycap, and the solution might not have had enough of the hydrogen peroxide. I left it out for the same amount of time, and Clara helped stir it while I went to a work appointment.

<p><img src="https://rubenerd.com/files/2021/retrobright-keys-day2@1x.jpg" srcset="https://rubenerd.com/files/2021/retrobright-keys-day2@1x.jpg 1x, https://rubenerd.com/files/2021/retrobright-keys-day2@2x.jpg 2x" alt="" style="width:500px" /></p>

The difference was *dramatic.* The "top deck" chocolate look of the caps was almost completely gone! The spacebar key was still a bit yellow, so I'm going to give it one last treatment. (With hindsight, I didn't do an especially good job with "before and after" shots here, but I couldn't be happier with the results).

In the meantime I added the keys back to the C128, and it all matches! Maybe if I did the whole case and keys a few more times it'd match my little second-hand VGA LCD I bought in Akihabara for my DOS and Commodore machines, but again my aim was just to have all the parts on the Commodore match.

<p><img src="https://rubenerd.com/files/2021/retrobright-keys-day3@1x.jpg" srcset="https://rubenerd.com/files/2021/retrobright-keys-day3@1x.jpg 1x, https://rubenerd.com/files/2021/retrobright-keys-day3@2x.jpg 2x" alt="" style="width:500px" /></p>

I have a replacement function key, spring, and plunger in the post from a friendly seller in Hungary, and the spacebar key will be added back soon. Now back to getting this stubborn 80-column VDC working!

