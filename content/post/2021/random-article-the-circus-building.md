---
title: "Random article: The Circus Building"
date: "2021-01-15T15:54:47+10:00"
abstract: "This may be my new work desktop background for this month"
thumb: "https://rubenerd.com/files/2020/circus-building@1x.jpg"
year: "2021"
category: Media
tag:
- nature
- photos
- random-article
- wikipedia
location: Sydney
---
<p><img src="https://rubenerd.com/files/2020/circus-building@1x.jpg" srcset="https://rubenerd.com/files/2020/circus-building@1x.jpg 1x, https://rubenerd.com/files/2020/circus-building@2x.jpg 2x" alt="" style="width:500px" /></p>

I love hitting the [Wikipedia random article](https://en.wikipedia.org/wiki/Special:Random) link and seeing what comes up. Today we got the [Circus Building](https://en.wikipedia.org/wiki/Circus_Building) which is:

> ... an exhibit building at Shelburne Museum in Shelburne, Vermont. It houses a collection of circus posters, Gustav A. Dentzel Carousel animals, and elaborately carved miniature circuses, including those by Roy Arnold and Edgar Kirk.

The [accompanying photo](https://commons.wikimedia.org/wiki/File:Circus_Building,_Shelburne_Museum,_Shelburne_VT.jpg) by John Phelan may be my new work desktop background for this month. The colours almost perfectly match my blog colour scheme too, which hasn't gone unnoticed.

