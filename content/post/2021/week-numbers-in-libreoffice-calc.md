---
title: "Week numbers in LibreOffic Calc"
date: "2021-10-15T08:38:48+10:00"
abstract: "=WEEKNUM(cell; 21)"
year: "2021"
category: Software
tag:
- guides
- libreoffice
- spreadsheets
- week-numbers
location: Sydney
---
I use week numbers for all sorts of things. It gives me more granularity than a month, and they're more accurate for certain use cases. For example, people assume a month has four weeks, but 48 leaves us four weeks short of the 52 weeks in a year. Renters and mortgage payers know all to well the fun of realising a specific month has three payments, not two.

*(As an aside, that's yet another reason why envelope budgeting is great. Before I required any tenancy agreement I signed to be paid monthly instead, I'd average out the cost of rent over twelve months and keep that extra in the "rent" envelope, so I'd never be surprised when those triple payment months appeared).*

That <span style="font-style:line-through">uncharacteristically</span> verbose introduction aside, this is how you can calculate the current week number from an epoch or existing date:

	=WEEKNUM(A1; 21)

I was confused why this wasn't returning what I expected when I didn't include the second parameter, but [WeekNumber.com explained](https://weeknumber.com/how-to/libreoffice)\:

> 21 is a code that identifies ISO week numbers among other week numbering schemes.

*Boom.*

