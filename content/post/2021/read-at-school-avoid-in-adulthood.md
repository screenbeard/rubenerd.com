---
title: "Read at school, avoid in adulthood"
date: "2021-07-30T16:21:04+10:00"
abstract: "I have never cared very much for Homer and have never read his works since I left school."
year: "2021"
category: Media
tag:
- education
location: Sydney
---
This passage from AJ Ayer's Autobiography, shared via Damon Young on The Bird Site, was *chef's kiss*:

> In addition to the work which we had to do in our own time to prepare for our lessons, we had to cope with what were known as Extra Books. This required us to master a book of Homer's Odyssey every half. The standard expected of us was such that we came to know the Greek text very nearly by heart. I do not think that any of us got the full 100 marks in the examination on it, but several of us came as close as 97 or 98, which I even then felt to be quite a creditable achievement for boys of thirteen or fourteen. Perhaps as a result, I have never cared very much for Homer and have never read his works since I left school.

One day I might get back into Shakespeare. But I'm not in a hurry, for this reason.

