---
title: "Practical Engineering on cavitation"
date: "2021-04-28T13:25:28+10:00"
abstract: "Another great video on something I’ve always been curious about."
thumb: "https://rubenerd.com/files/2021/yt-zCE26J0cYWA@1x.jpg"
year: "2021"
category: Media
tag:
- practical-engineering
- video
- youtube
location: Sydney
---
I love my [Hololive girls](https://rubenerd.com/im-in-inas-tentacult/), but most of the channels I'm obsessed with on The YouTubes talk about engineering, electronics, and IT. We're spoiled by the number of high quality, independently produced media channels, to such an extent that Clara and I haven't watched free to air TV or cable for years. *Why would you need to?* There's a wider discussion on the impact of independent media there for a future post.

<p><a href="https://www.youtube.com/watch?v=zCE26J0cYWA" title="Play What is Cavitation? (with AvE)"><img src="https://rubenerd.com/files/2021/yt-zCE26J0cYWA@1x.jpg" srcset="https://rubenerd.com/files/2021/yt-zCE26J0cYWA@1x.jpg 1x, https://rubenerd.com/files/2021/yt-zCE26J0cYWA@2x.jpg 2x" alt="Play What is Cavitation? (with AvE)" style="width:500px;height:281px;" /></a></p>

I've spruiked [Practical Engineering](https://www.youtube.com/channel/UCMOqf8ab-42UUQIdVoKwjlQ) before when he discussed the [Texas blackouts](https://rubenerd.com/the-texas-power-failure/), but I just finished watching his 2018 video about Cavitation. Grady's pacing, diagrams, and models do such a great job explaining concepts, and his enthusiasm for civil engineering is infectious. I knew a bit about the Bernoulli principle from early "superdiskettes", but now I know how it works! *So cool.*

> "I can't measure the pressure at the constriction of a venturi, which will be a VERY strong vacuum, but this gauge measures the total loss of pressure caused by the turbulence of cavitation. Just for reference... and because it looks cool!"

