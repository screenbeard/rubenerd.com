---
title: "The Heinzelmännchen unsaving your changes"
date: "2021-12-09T15:09:37+10:00"
abstract: "I’m sure there’s a German word for saving something and having stuff change in the interim."
year: "2021"
category: Software
tag:
- heinzelmannchen
location: Sydney
---
This happens to me *all* the time.

You'll be editing a document, decide it's finished, and you save it. You'll forget to close it, then go into another application and do something else. When you inevitably remember you still have it open, you'll try to close it.

> Do you want to close with unsaved changes?

Which unsaved changes? Where!?

As my German dad would say, the Heinzelmännchen did it! I'm [tagging it](https://rubenerd.com/tag/heinzelmannchen) as such until given sufficient evidence to the contrary.

