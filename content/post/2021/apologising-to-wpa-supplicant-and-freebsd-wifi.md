---
title: "Apologising to wpa_supplicant and FreeBSD Wi-Fi"
date: "2021-04-05T09:51:46+10:00"
abstract: "It wasn’t FreeBSD being flaky, it was my iTelephone. Go figure."
year: "2021"
category: Hardware
tag:
- apple
- bsd
- freebsd
- itelephone
- networking
- wi-fi
location: Sydney
---
Recently I was made aware of the *obvious* fact that tethering over Bluetooth is slower than Wi-Fi. I was an especially congested conference call recently, which aside from being a delightful bit of alliteration, cleared right up the moment I swapped over to Wi-Fi.

*(This was regrettable, as people could then see my face on the conference call, whereas before I was a pixillated blob. But the performance point stands. As opposed to me, because I was sitting).*

I always used Bluetooth for tethering on my MacBook Pro because it's always there. I choose my work iTelephone from the Bluetooth menu on the Mac menu bar after pairing it once, and it connects every time. *Just Works&trade;* experiences are becoming fewer and further between on modern Apple hardware, so I take it when I can.

My ThinkPad and Panasonic laptops running various FreeBSDs were another story. I didn't have Bluetooth enabled on those machines, so I used Wi-Fi. Every attempt to tether from iTelephones was a struggle, sometimes requiring me to turn on/off tethering in iOS multiple times before wpa_supplicant would detect it, connect, and DHCP. I just chalked it up to FreeBSD Wi-Fi being a bit flaky and lived with it. [I sense a theme](https://rubenerd.com/ethernet-papercuts/) with network hardware.

*Except, it wasn't FreeBSD!* On this new-ish 16-inch MacBook Pro, I had to do the same song and dance when using Wi-Fi instead of Bluetooth. It's rare for Bluetooth to outshine anything in reliability, but for whatever reason multiple iTelephones simply can't tether their Wi-Fi networks the first (or second, or third) time.

Now that I know it's the phone, wpa_supplicant on FreeBSD is no slower or clumsier than the equivalent commands on my Mac. All that internal grumbling and monologue was *entirely* misplaced. FreeBSD was doing exactly what I was telling it, and was working with the hardware as given.

Has anyone tethered with a recent FreeBSD over Bluetooth from an obstinate Apple phone? I won't be doing video calls on these machines, and Bluetooth is more than sufficient for terminal sessions and writing. Eventually I'd love to get an ultraportable with a SIM card, but for now I feel like this would work a treat.

