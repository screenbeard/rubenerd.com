---
title: "Rubenerd.com has a LiveJournal?"
date: "2021-03-05T10:50:08+11:00"
abstract: "When did LJ become a scraper?"
year: "2021"
category: Internet
tag:
- weblog
location: Sydney
---
Rebecca Hales emailed to let me know that this site <a rel="nofollow" href="https://rubenerd.livejournal.com/">has a LiveJournal</a>. It's news to me, I didn't make it, nor have I ever had an account.

It's basically an RSS scraper. They include the following disclaimer:

> LiveJournal.com makes no claim to the content supplied through this journal account. Articles are retrieved via a public feed supplied by the site for this purpose.

Yet the big blue SUBSCRIBE button links you to the signup page for LiveJournal, not the RSS feed itself. At best that's misleading. 

I'm glad I include a link directly to the site in the footer of each blog post in my RSS source. Fortunately most automated RSS scraping sites are too oblivious to remove these.

But it does make me think what kind of mischief I could get up to. Maybe I could write an nginx or Varnish rule to redirect requests from LJ to an RSS feed that just contains links from [this section](https://en.wikipedia.org/wiki/LiveJournal#Controversies_and_criticism) of their Wikipedia page.
