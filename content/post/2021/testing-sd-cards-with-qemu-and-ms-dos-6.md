---
title: "Testing SD cards with QEMU and MS-DOS 6"
date: "2021-03-24T08:25:00+11:00"
abstract: "Somebody forgot to zero out the card first."
thumb: "https://rubenerd.com/files/2021/dos6-error-2@1x.png"
year: "2021"
category: Thoughts
tag:
- dos
- qemu
- ms-dos
- mmx-machine
- troubleshooting
- virtualisation
location: Sydney
---
The [first computer](https://rubenerd.com/tag/mmx-machine/) I built as a kid still runs, thanks to generous bouts of nostalgia. She's a spectacular Pentium 1 minitower with a whopping 64 MiB of memory, a SCSI Zip drive, a Sound Blaster AWE32 PnP card, a crystal-clear Matrox Mystique GPU, and a 5.25-inch floppy drive from the long-departed family 486. She originally ran Windows 95, but now also runs BeOS, FreeBSD 6.3 (my first version) and MS-DOS 6.0 (what our family 486 ran).

Recently I decided to replace the internal drives with SD cards. I originally used CF cards with passive IDE adaptors, but SD cards are *significantly* cheaper and easier to source now, and their active converters still max out the ATA33 connections on this old Octec/Ocean Rhino 12+ board for reads and writes.

While I wait for the adaptor to arrive, I thought I'd prepare my two 16 GiB cards for DOS. I use bhyve and Xen, but for quick and dirty tests like this I just use QEMU with settings that mimic the target hardware:

    $ qemu-system-i386 \
        -machine isapc \
        -cpu pentium \
        -m 64M \
        -hda /dev/ada2 \
        -fda msdos6-disk1.img

The familiar MS-DOS 6 setup screen booted. Normally I strike `F3` at this stage and run `fdisk` to manually set up the partitions as I want, like a gentleman, but as a quick test I thought I'd let the installer do it for me.

<p><img src="https://rubenerd.com/files/2021/dos6-error-1@1x.png" srcset="https://rubenerd.com/files/2021/dos6-error-1@1x.png 1x, https://rubenerd.com/files/2021/dos6-error-1@2x.png 2x" alt="Error dialog box saying: Setup cannot install MS-DOS 6 on your computer. Press ENTER to exit Setup." style="width:500px; height:250px;" /></p>

In the words of Australian comedian Carl Barron, *[that dun‘ look good](https://www.youtube.com/watch?v=AsnE4-P-JBo).* So I pressed `ENTER` to exit the Setup, hoping I'd be able to use `fdisk`.

<p><img src="https://rubenerd.com/files/2021/dos6-error-2@1x.png" srcset="https://rubenerd.com/files/2021/dos6-error-2@1x.png 1x, https://rubenerd.com/files/2021/dos6-error-2@2x.png 2x" alt="New error: Memory allocation error. Cannot load COMMAND, system halted" style="width:500px; height:250px;" /></p>

MS-DOS 6.0 was a... fun release. Any rational person would use 6.20, which customers of 6.0 were eligible to "step-up" to, or even the widely-used final release of MS-DOS 6.22. Heck I'd even been told to stick with 5.x by people who would know, or switch to the vastly-superior DR-DOS. But what part of this entire setup is rational?

I got out my cheat MS-DOS 7 image made from Windows 95, and booted with that instead. This time I got to `fdisk`, and one of the more bizarre partition layouts I'd seen. The MS-DOS 6 installer really went to town:

<p><img src="https://rubenerd.com/files/2021/dos6-error-3@1x.png" srcset="https://rubenerd.com/files/2021/dos6-error-3@1x.png 1x, https://rubenerd.com/files/2021/dos6-error-3@2x.png 2x" alt="Display Partition Information screen on fdisk, showing an 'UNKNOWN' partition type and tab-shifted text that's all over the place." style="width:500px; height:250px;" /></p>

The MS-DOS 6 installer almost certainly got confused with the partition table of this new SD card, which I soon realised I hadn't zero'd out on my FreeBSD box first:

    # dcfldd if=/dev/zero of=/dev/${SD_CARD}

Then the installer worked. *Who'd have thunk it?*

