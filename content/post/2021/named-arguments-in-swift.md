---
title: "Named arguments in Swift"
date: "2021-04-28T18:20:23+10:00"
abstract: "I didn’t know its method definitions were defined this way."
year: "2021"
category: Software
tag:
- programming
- smalltalk
- swift
location: Sydney
---
Today I learned! Via [kornel on lobste.rs](https://lobste.rs/s/j7zv69/if_you_could_re_design_rust_from_scratch#c_ir0nnq):

> Yeah, I think Swift nailed it. Its overloading/default args aren’t even a special calling convention or kwargs object. They are merely part of function’s name cleverly split into pieces. `init(withFoo:f andBar:b)` is something like `init_withFoo_andBar_(f, b)` under the hood.

It's been a while since I was a full-time developer, but I loved the Objective-C/Smalltalk messaging style over the C/C++ style for parameters, even just for the sake of readability:

    Human age: 18 height : 180
    Human(18,180)

I might need to mess with Smalltalk again one day. Is there a modern implementation in regular use that isn't wed to C? *Objective Rust*, make it happen!

