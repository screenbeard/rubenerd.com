---
title: "Marie Kondo would say this kicks arse!"
date: "2021-02-04T08:23:09+11:00"
abstract: "She’s sparked a cottage industry of bad jokes that misunderstand her fairly simple point."
year: "2021"
category: Thoughts
tag:
- decluttering
- minimalism
location: Sydney
---
I take the responsibility of being *that guy* today seriously. Someone on Twitter posted this about the decluttering author:

> Showing Marie Kondo my apartment, which is filled with CD-Rs of Japanese Saturn games and a shoebox full of Sanwa buttons. "This kicks ass," she says. "Don't throw anything out"

... she would!

Marie Kondo has sparked a cottage industry of people intentionally misunderstanding her, or making jokes that backfire. It speaks to our society's priorities, and how we're conditioned to be good little passive consumers of stuff we don't need to keep the economy going.

This is well-trodden ground here; I've talked about growing up in [homes full of stuff](https://rubenerd.com/unpacking-singapore-day-one/) and the [anxiety I've lived with](https://rubenerd.com/goodbye-junk/) about being walled in and trapped. It's confronting to be told you don't need most of the rubbish you own, either under the misguided view that you might need it one day, or through misplaced guilt, or a hoarding complex at worst.

People have conflated her decluttering message with minimalism, which started gaining mainstream traction at around the same time. Minimalism postulates that you find meaning in people and experiences, not things. Marie asks whether something sparks joy, or is useful in a meaningful way. There's overlap, but my late mum's calligraphy or a beautiful 1980s graphic equaliser would definitely fit in the latter.

If you love this specific thing, and keeping it isn't debilitating or keeping you from living the life you want, hold onto it! Your zinger about Ms Kondo isn't as biting as you think.
