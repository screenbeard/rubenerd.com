---
title: "Bathroom design affordances"
date: "2021-03-14T09:09:01+11:00"
abstract: "Why is a switch... there?!"
thumb: "https://rubenerd.com/files/2021/lightswitch@1x.jpg"
year: "2021"
category: Thoughts
tag:
- accessibility
- design
location: Sydney
---
This is the power point in our bathroom, currently with a shaver and toothbrush plugged in. I was going to say *electric* shaver and toothbrush, before realising that naturally anything plugged into an electrical outlet is itself electric. Unless they're turned off, in which they cease being electrically powered *and* useful.

<p><img src="https://rubenerd.com/files/2021/lightswitch@1x.jpg" srcset="https://rubenerd.com/files/2021/lightswitch@1x.jpg 1x, https://rubenerd.com/files/2021/lightswitch@2x.jpg 2x" alt="Power point on our bathroom wall, showing two appliances plugged in, and an additional switch mounted lower on the bracket." style="width:500px" /></p>

Notice anything unusual about this arrangement, beyond my failed attempt to make the image look more interesting in monochrome?

Yes, the panel has *three switches*, not just two. The outer ones are obvious: they're for toggling power to the connected devices. But what's that middle one for?

This seems to confuse enough people who come to our apartment, but that's the primary bathroom light switch. It's position by the door&mdash;where a switch would otherwise be&mdash;is routinely overlooked because *it looks like a power point*. When this is is explained though, people are able to find and use it.

Once you know that panel includes the light switch, it's easier to find in the dark than a regular switch on account of having large protrusions that one can feel for. Though you don't want to rely too heavily on this, lest you slip and pull out a device while the switch is on.

But this leads to a far worse problem for accessibility than discoverability. Take a second look if you need to; notice anything with the positioning of that switch?

Normal light switch panels are easy to use. You move one of your digits in the vicinity of the switch and push. This panel doesn't let you do that. For reasons that confound me, they positioned the light switch *below* the others, which butts it against the connected plugs. You can't just reach for the switch from any orientation, you need to come at it from the top. I have skinny fingers, and even I struggle with toggling it from the front as I would a regular switch. The only way I can use it reliably is to rest my palm above the panel, and push it with my thumb. This is madness!

Why did they do this? Misguided aesthetics? Was there a conduit alignment or electrical reason why the button couldn't be positioned higher? Did they not want that middle switch being confused for the other ones? I doubt the latter, given we've been trained to expect those other power switches to be oriented in the same place on every wall panel we've ever used.

The only thing I can remotely think of is that it's to discourage you plugging in chunky power cables that would come from higher-powered devices. If it's hard to access that light switch with two-pin shaver and electric toothbrushes, it's *impossible* with a larger, rounded plug with a ground pin. In Singapore I got used to every bathroom having a power plug with a label saying "shavers only". I think it's the same in the UK. But if that were the case, why does that panel have a ground pin, and the ability to connect larger devices at all?

I would have loved to be a fly on the wall in these design meetings.

