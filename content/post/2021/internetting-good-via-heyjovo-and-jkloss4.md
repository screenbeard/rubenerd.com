---
title: "Internetting good, via @heyjovo and @jkloss4"
date: "2021-10-13T11:30:11+10:00"
abstract: "Design, manipulation, and PEBKAC pushback."
year: "2021"
category: Internet
tag:
- accessibility
- design
- ethics
- usability
location: Sydney
---
[@heyjovo posted](https://twitter.com/heyjovo/status/1448036929551798274) about a phenomena I still don't think is appreciated or well understood about the Internet:

> It wasn’t until I started doing user research that I realized just how many people can’t internet good. Designers, people are badder at internetting than you think.

Where do I even start with agreeing here? I feel as though my neck is about to snap from nodding!

This manifests in *so* many ways, but perhaps none as stark as the pervasive attitude of <abbr title="problem exists between keyboard and chair">PEBKAC</abbr>. Even without leaving the realm of design, so much of the modern web is left wanting when it comes to accessibility, ease of use, and respect. I've been working in the industry since primary school, and I struggle to understand certain websites and mobile applications sometimes.

The Internet isn't just an idle curiosity, place for research, or intellectual area of discussion for nerds, as much as people may wish for it to be the case. It's a mandatory component of modern society, and as such we have a duty of care as IT professionals. See my quote earlier in the week from [Martin Fowler about software](https://rubenerd.com/martin-fowler-on-the-impact-of-software/) as an example.

I've worked in support, and I know how tempting it is to dismiss people's skills on the basis of a lack of maturity, knowledge, or refusal to learn how something works. But being dismissive cuts both ways, and we have the advantage of expertise to assist.

@heyjovo also mentioned:

> Sometimes it’s because a system is too complex, poorly designed, or manipulative. 

*Manipulative* is a brilliant descriptor. I wouldn't even say Internet novices or the technically challenged have an uphill battle, I'd say the Internet is actively hostile to them.

I'm paraphrasing, but a few months ago Jim Kloss discussed on an web stream that the Internet you and I inhabit is different from that of other people. Turn off your dynamic content blockers like uBlock Origin or NoScript to see what the rest of the world has to go through while performing daily tasks. I made it half an hour last time before succumbing to despair and frustration. And these are systems *we've built*.

Manipulation also covers <span style="text-decoration:line-through">misinformation</span> lies, behavioural tracking, and the business models that foster the former and perpetuate the latter.
