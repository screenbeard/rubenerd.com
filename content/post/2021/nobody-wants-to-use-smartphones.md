---
title: "If nobody wants to use smartphones, what then?"
date: "2021-12-28T10:36:24+11:00"
abstract: "Hardware that people predict will replace smartphones sound even scarier."
year: "2021"
category: Hardware
tag:
- dan-benjamin
- design
- futurism
- phones
- sociology
location: Sydney
---
A few years ago on one of his shows, Dan Benjamin of [5by5](https://5by5.tv/) and [Fireside](https://fireside.fm/) fame posited that nobody *really* likes using their smartphones, they just happen to be the device we need to perform specific tasks. If and when a new way to communicate, play games, and navigate arrives, we'll move over to that and relegate smartphones to the same pile as PDAs, pagers, and the Chappe Semaphore Telegraph.

I'm not enough of a futurist to predict if and when such a device will be invented and made commercially available, or what form it will take. Maybe it's a smartwatch that can project a screen in front of us, or a pair of smart glasses, or something we read off our clothing. Science fiction has had fun with the idea of neural devices that interact directly with our brains. These are all equal parts intriguing and terrifying, but I also suspect the next generation device will be none of these.

People on the fringes like me will likely keep using smartphones in the future when they become a nostalgic relic, much as I do with my fleet of beautiful old Palm PDAs today. But it's hard to find fault in Dan's point for rational people, or society at large.

One thing all those hypothetical devices have in common though: they're closer to us than any other computer has been before. In the case of a neural interface, it's hard to think of anything closer that doesn't involve physically installing firmware into our cells.

This is worth thinking about, because the implications could be huge. Within a generation we've gone from ENIAC and mainframes, to minicomputers and workstations, to desktops and laptops, to smartphones and tablets. Each leap has come with added capabilities and performance, but more importantly they've come to us in smaller packages. Smaller means closer.

*(We're side-stepping cloud computing and remove servers here, and whether a smartphone or tablet today can really be thought of as a cohesive, atomic unit of computing if they require remote servers to operate. I suppose one could draw parallels between this and minicomputers with remote terminals, though they never achieved the amount of public use smartphones have. Either way, you get the gist).*

I'm sure people at the turn of last century would be equally fascinated and mortified at the prospect of everyone having tracking devices in their pockets, and would say they draw their comfort line at the ticker tape machine in their office, or their home radio. Maybe someone in the 1980s would say their luggable would be the closest they'd feel safe with, or maybe their pager. At least the former can be used as a weapon for self-defence, and the latter has plausible deniability built in (sorry, there wasn't a phone nearby)!

Smartphones straddle that edge for me. I either feel that they're *super* useful, or an unwanted house guest interrupting my sleep and downtime. Having a separate work phone has helped with this slightly, but the expectations of modern society remain: you must be connected and available at all times. I've come to accept and resent this.

I used a smartwatch, and it felt so invasive and anxiety-inducing having notifications physically on my body that I gave it up after a month. That's clearly a bridge too far for me, though I know people who love theirs.

Being in my thirties now, the World Wide Web, affordable laptops, and smartphones all happened within my lifetime. Kids today are being brought up in a world of ubiquitous connectivity and remote social interaction that I think would have terrified me as a kid! Gen Z will likely have an entirely different relationship and attitude when it comes to tech, and will have their own opinions and ideas about what constitutes acceptable distance. Maybe they won't find the idea of smart glasses creepy, or whatever else comes along. Maybe they'll be able to add distance where thus far I've failed.

As much as I love to hate on my modern smartphones, they seem to be the the closest I'm comfortable letting tech get to me... and even then, I wish they'd shut up sometimes! Maybe then I *do* want to use a smartphone; if only because the alternatives worry me more.
