---
title: "Minecraft, and mental health"
date: "2021-05-02T10:46:12+10:00"
abstract: "Minecraft Caves and Cliffs update delayed, and it’s reasonable."
year: "2021"
category: Software
tag:
- covid-19
- games
- health
- hololive
- minecraft
location: Sydney
---
Minecraft is one of those games you can absolutely lose yourself in for hours at a time. I was talking to one of my colleagues about how this one massive building complex has become repetitive and rote, yet Clara and I still love constructing it. Her comment was that it's almost a form of meditation, because you dedicate all your mental energy to it.

At the risk of putting too fine a point on it, Minecraft is the modern equivalent to me of the Lego I grew up with, and even SimCity to an extent. Those games got me through some tough family times, partly because there was creativity involved, but mostly due to it being a fun task that had *nothing* to do with the outside world, or circumstances outside my control. Minecraft was the perfect game for Clara and I to play to distract ourselves from COVID, especially during lockdowns.

It's in that context that I'm actually *relieved* to [read this press release](https://www.minecraft.net/en-us/article/a-caves---cliffs-announcement "A Caves & Cliffs announcement") by the Minecraft team, where they announced they're splitting their hotly-anticipated Caves and Cliffs expansion in two, and delaying their release:

> Technical considerations aside, we also prioritize our team’s health. We’ve come to realize that to ship all the features in the summer we would’ve had to work very long hours; and even then, there would’ve been no guarantee that everything would be finished on time. Lastly, working from home as we cope with the pandemic is still challenging – not just in terms of morale but also by hindering teamwork. Because our workflow is so complex and collaborative, not having the option to walk up to someone and ask for help makes everything take longer.

I don't think people in general appreciate the enormity of the effort involved in maintaining Internet infrastructure so others can do their work, chill with a streaming service, and keep in touch with family. The same applies to games; we see these as activities for fun, but there's serious work that goes into building, maintaining, updating, and fixing them.

It'd be a bit rich to attribute so many positive mental feelings to this software, and not afford the developers themselves any patience to improve their own.

