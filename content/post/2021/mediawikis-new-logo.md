---
title: "MediaWiki toasts the sunflower logo"
date: "2021-04-13T09:24:45+10:00"
abstract: "It’s... Inoffensive."
year: "2021"
category: Software
tag:
- colour
- design
- graphics
- logos
location: Sydney
---
The [MediaWiki](https://www.mediawiki.org/) software that powers Wikipedia among thousands of other sites has a new logo. It was announced on the [MediaWiki news page](https://www.mediawiki.org/wiki/News) on April Fools, so I initially thought it was a well-executed prank.

<p><img src="https://rubenerd.com/files/2021/logos-mediawiki@1x.jpg" srcset="https://rubenerd.com/files/2021/logos-mediawiki@1x.jpg 1x, https://rubenerd.com/files/2021/logos-mediawiki@2x.jpg 2x" alt="The old (left) and new (right) logo." style="width:500px; padding:1em 0; height:240px;" /></p>

Erik Möller's cheerful sunflower logo cleverly incorporated wiki syntax as a frame. Isarra Anthere even did a classy, [simplified version](https://www.mediawiki.org/wiki/File:MediaWiki_logo_2018.svg) in 2018 for smaller sizes and textile prints, while preserving the original pallete and shape.

By comparison, you know how they say the opposite of love isn't hate, it's indifference? This new logo is inoffensive, indistinct, bland, and forgettable. The worst I can say about its design is *meh*. For a branding exercise, it doesn't invoke anything about wiki software whatsoever; it looks like a [gas company](https://en.wikipedia.org/wiki/BP#/media/File:BP_Helios_logo.svg).

I'm glad the Wikimedia Foundation [updated](https://diff.wikimedia.org/2010/05/13/wikipedia-in-3d/) Paul Stansifer's whimsical spherical puzzle for Wikipedia in 2010, instead of replacing it with a dull mark. The rush to make the web look corporate is, to use a Marie Kondo analogy, not the stuff of joy. 🌻

