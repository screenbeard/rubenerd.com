---
title: "A report on exercise and sleep"
date: "2021-07-01T10:11:23+10:00"
abstract: "News outlets broadly got the message right, but not sure about some of the numbers."
year: "2021"
category: Thoughts
tag:
- health
- journalism
- news
location: Sydney
---
A [report by the British Journal of Sports Medicine](https://bjsm.bmj.com/content/54/20/1195) has been making the rounds on various news outlets. <a rel="nofollow" href="https://news.sky.com/story/brisk-walking-for-two-and-a-half-hours-a-week-could-prevent-early-death-caused-by-lack-of-sleep-study-12345259">Sky in the UK</a> was the first to break the story, as far as I can tell:

> The research suggested that two-and-a-half hours of walking or one hour and 15 minutes of running per week "eliminated most of the deleterious associations" of poor sleep and risk of an early death.

Other outlets summarised the report the same way.

It helps to confirm what I arrived at myself years ago. I've long taken to brisk evening walks not only to burn off excess energy, but to clear my head. I suffered insomnia for years, and this was the only thing outside drugs that helped. People lament that they don't have time for exercise; I say I don't have time *not to!* Especially with IT as an occupation.

Covid lockdowns have only made this more critical. I didn't realise how much I'd come to depend on powerwalking to train stations and the office for much of my exercise. Anxiety about the state of the world and family have played a big part in deteriorating sleep quality, but there's no question a more sedentary lifestyle (read: being stuck at home!) has negatively fed this. Getting back into late-night walks during lockdown saw an immediate improvement.

But here's where things get interesting. I searched around for the original study, and [Men's Health](https://www.menshealth.com/uk/health/a36885497/researchers-walking-benefits/) was the only outlet to link to it. A lifestyle magazine has more journalistic integrity than newspapers! I wonder why that didn't surprise me?

Anyway I [downloaded the report](https://bjsm.bmj.com/content/54/20/1195), and it makes no mention of walking or running whatsoever. I also couldn't find where "two-and-a-half hours of walking or one hour and 15 minutes of running" were quantified. The full conclusion, with the widely-quote section in bold, is below:

> Poor sleep was associated with a higher risk for all-cause and cause-specific mortality, and these risks were markedly exacerbated among participants with insufficient PA [physical activity]. Meeting the lower threshold of the current PA guidelines (600 MET-mins/week) **eliminated most of the deleterious associations of poor sleep** with mortality. Our results support the value of interventions to concurrently target PA and sleep to improve health. Future prospective studies with device-based sleep and PA assessments and trials concurrently targeting both behaviours are warranted.

MET refers to *metabolic equivalent tasks*. 10 hours or 600 minutes a week translates to about 85 minutes a day.

I checked the source for the PA guidelines they cited, which lead me to [this 2018 paper](https://jamanetwork.com/journals/jama/article-abstract/2712935) by the American Medical Association. It also doesn't mention walking or running, but quantifies activities and times:

> Adults should do at least 150 minutes to 300 minutes a week of moderate-intensity, or 75 minutes to 150 minutes a week of vigorous-intensity aerobic physical activity, or an equivalent combination of moderate- and vigorous-intensity aerobic activity.

The original report probably tried to humanise these by saying that walking is an example of moderate activity, and running is intensive. I wish they'd said that though.

I'm probably splitting hairs; the thrust of the research is that physical activity is correlated with reduced negative effects of poor sleep, which should encourage all of us to exercise more! My worry is when the popular press summarises and distills a study into a few bytes that aren't entirely accurate, which are prone to be disseminated without further context or references. We saw this with the "10,000 hours to be a professional" meme.

