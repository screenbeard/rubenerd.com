---
title: "John Roderick and Bean Dad"
date: "2021-01-13T08:42:01+11:00"
abstract: "It didn’t come as any surprise."
year: "2021"
category: Thoughts
tag:
- john-roderick
location: Sydney
---
I've flown in the face of the idea that you should never meet you heroes. Every developer, podcaster, and musician I've talked with and met has been great!

*Bean Dad* was the first to not be. I posted about an exchange of ours last year in which he described Millennials and Gen Z as insincere, and went off on a few of us who described the unique struggles facing young people in the 2020s; especially those younger than both of us.

I initially felt disillusioned and a touch sad when he slagged me off, but eventually attributed it to Twitter being a bad platform to discuss ideas, which it is, and some misunderstandings. 

Then I saw it happen again and, once more, my instinct was to defend him. I had listened to enough of his body of work that I thought I knew him, and that things I wouldn't excuse from other people were fine coming from him (this is almost certainly how some of his co-hosts will discuss him after the fallout from this has settled). That cognitive dissonance should have rung alarm bells, but I was desperate to preserve this image I had in my head.

The Bean Dad thing was strike three, and unfortunately for the rest of his fans, it was far more public. This time he was Internet Villain #1 for 2021, and everyone was uncovering and talking about things people like me had been subjected to before. Other musicians I follow and trust shared their own disturbing encounters. The most telling moment for me was not being surprised when I read who Bean Dad was. I suppose, deep down, I'd already come to terms with it.

If there's one good thing from this whole saga, it's that it gave people confidence to talk openly without fear.

