---
title: "The spotted wood owl"
date: "2021-05-13T06:45:40+10:00"
abstract: "When I see cute birds featured on Wikipedia, I share them!"
thumb: "https://rubenerd.com/files/2021/spotted-wood-owl@1x.jpg"
year: "2021"
category: Media
tag:
- birds
- photos
- wikipedia
location: Sydney
---
I have an unofficial rule here that I [share cute birds](https://rubenerd.com/tag/birds/) who makes it to Wikipedia's featured picture of the day. This spotted wood owl was photographed by JJ Harrison in Pasir Ris Park in Singapore, no less!

<p><img src="https://rubenerd.com/files/2021/spotted-wood-owl@1x.jpg" srcset="https://rubenerd.com/files/2021/spotted-wood-owl@1x.jpg 1x, https://rubenerd.com/files/2021/spotted-wood-owl@2x.jpg 2x" alt="Photo of a rather fetching Spotted Wood Owl in Pasir Ris Park in Singapore, by JJ Harrison." style="width:320px; height:480px;" /></p>

From the [Wikipedia article](https://en.wikipedia.org/wiki/Spotted_wood_owl)\:

> The spotted wood owl (Strix seloputo) is an owl of the earless owl genus, Strix. Its range is disjunct; it occurs in many regions surrounding Borneo, but not on that island itself.

