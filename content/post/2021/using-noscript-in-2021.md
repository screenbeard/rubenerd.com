---
title: "Using NoScript in 2021"
date: "2021-10-17T08:37:42+10:00"
abstract: "I mentioned it again recently and was asked if I still use it. I've gone back to it."
year: "2021"
category: Software
tag:
- accessibility
- firefox
- plugins
location: Sydney
---
Jim Kloss asked me a few years ago if I still used the [NoScript Security Suite](https://noscript.net/), the Firefox plugin that only permits sites you choose to load dynamic content. I sung its praises here for [many years](https://rubenerd.com/p3849/), but I felt the modern web's reliance on scripting made its use increasingly untenable. Graceful fallbacks are also a thing of the past, so sites will just refuse to load anything.

I'd emulated some of what I had in the past with the excellent uBlock Origin, but the protection still felt leaky. As I [mentioned last week](https://rubenerd.com/internetting-good-via-heyjovo-and-jkloss4/), so much of the modern web doesn't feel distasteful as much as it is user hostile, and it'll only get worse until there's a *Hail Mary* moment in the industry, the entire edifice collapses under its own bloated weight, or when the general population gets fed up. It's [happened before](https://en.wikipedia.org/wiki/Popup_blocker "Wikipedia article on popup blockers in the 1990s"). 

Maybe I was feeling nostalgic for the days when I had to go through a laundry list of domains until a site was functional again, but I reinstalled NoScript again over my recent break. It's... less frustrating than I remembered. Either that, or the benefits of using it outweigh whatever inconvenience it represents again. Maybe the modern web is so awful, selectively-enabling domains in a plugin is a *less* frustrating experience. That says something.

The best thing its done is disable third-party font downloads, so you don't get flashes of empty pages before text loads and reflows around the other page elements. You also get a feeling pretty quickly for sites that are carefully designed for accessibility, and those who think fallback is what happens when you lean too far in your chair. Maybe for tired web developers who are told by their managers that such concerns aren't important, that's exactly what they end up doing. For those who think fallbacks are a good idea, maybe we should saw off their chair legs.

Your mileage will definitely vary (YMWDV?), but give it a try again if you used it in the past but gave up. You might be as surprised as I was.
