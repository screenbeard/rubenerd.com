---
title: "Log4Shell"
date: "2021-12-17T10:38:52+10:00"
abstract: "Here comes the hindsighted wisdom!"
year: "2021"
category: Internet
tag:
- security
location: Sydney
---
I'm sure all of you have heard about what the press have dubbed Log4Shell, a growing family of vulnerabilities affecting the log4j package. Some of you may even be responsible for patching and auditing affected systems... assuming a nefarious actor hasn't done the former for you to get a permanent foothold. Fun times!

Much like other high-profile exploits like Heartbleed and Meltdown/Spectre, it didn't take long for the armchair experts to weigh in with the steamiest of hot takes. I don't even especially like hot cakes, but I think we'd all benefit from consuming those instead.

What's the difference between a hot cake, pancake, or pikelet?

These reactions have been as predictable as the silliness resulting from my various hat choices:

* Java itself is antiquated, pointless, shouldn't be used, not type safe, etc. One wonders how many of these people still use bash after Shellshock, or OpenSSL after Heartbleed, or a modern CPU after speculative exploits, or...! Heaven forbid when a data sanitisation issue affects a Rust package.

* More broadly, the developers and maintainers of the affected log4j package were stupid, made elementary mistakes these experts would never be caught making, and could have benefited from their hindsighted wisdom. 

I liken these reactions to the critics who point at an artwork and say "eh, I could have painted that". More to the point, where were they *before* the details were announced? Funny how that works.

At least this time it didn't take long for people to recognise another under-appreciated and under-resourced open source team who's efforts hold up so much infrastructure we depend on. Whether that translates into meaningful action within the industry we'll have to see. I donate to a bunch of projects and foundations, though I'm sure I could be doing more, too.

Hug a Java developer or sysadmin, I'm sure they need it. Maybe make them a nice pancake.

