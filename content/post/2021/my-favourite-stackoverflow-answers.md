---
title: "My favourite StackOverflow answers"
date: "2021-10-31T16:41:39+10:00"
abstract: "… are those preceding a comment claiming something can't be done."
year: "2021"
category: Internet
tag:
- troubleshooting
location: Sydney
---
... are those preceding a comment claiming something can't be done.

These more than make up for the *have you tried Googling it* or *why would you want to* responses that appear with such regularity, you'd think people are being paid for them.

