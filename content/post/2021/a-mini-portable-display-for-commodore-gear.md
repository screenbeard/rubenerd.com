---
title: "A mini, portable display for Commodore gear?"
date: "2021-04-03T08:41:50+11:00"
abstract: "Putting the LUGGABLE back into these machines! :D"
year: "2021"
category: Hardware
tag:
- commodore
- commodore-16
- commodore-128
- commodore-plus4
- displays
location: Sydney
---
[Speaking of displays](https://rubenerd.com/now-obsolete-macbooks-still-have-better-screens/)! I had a dream last night where I was using my Commodore Plus/4 out on our balcony with a little external display. I can't remember too many details, but it was in a 3D printed beige box with a power connector, signal cable, and some card slots. This lead me down the rabbit hole of figuring out how such a display would work.

I've talked here before about how our balcony saved us during lockdowns, thanks to trees and fresh air and not feeling cooped up. Aside from a weather-sealed power point, it's otherwise just like a coffee shop out here, so whatever we want to use we have to set up on the spot.

Ideally I'd want a 4:3 display that can accept composite video, which would work with the Plus/4, the C16, and the C128's 40-column mode out here. A smaller size would make it portable, maybe something a bit bigger than what the SX64 had.

But then my wishlist started growing! What about a built in:

* CGA2RGB converter and upscaler, for the C128's 80-column mode?

* SD2IEC card reader, or a Pi1541 for disk access? Maybe a datasette emulator as well?

* A battery? Either for the screen AND the computer? What about a switching power supply for the computer as well? Would that be a stretch?

At this stage the display enclosure would be taking on *everything* that that Commodore computer itself doesn't have. It'd be the *luggable* component, in 1980s parlance. It wouldn't matter if it's chunky; something I could put into a bag or stand on my balcony would be brilliant. I'd even see myself using this as a way to take my Plus/4 to my dad's place up the coast on holidays. Or even the C128 or C16 if I were able to source a bigger bag.

To do everything above, it'd need power, two display signals cables, and one or more data lines. Maybe I could braid or heat shrink them together, with sufficient shielding on the power cable. I could set up a port replicator-style connector for the other side so it's just one bracket being plugged into the Commodore itself, though that would limit it to just the Plus/4.

Even if nothing comes of this, I think it'll be a fun experiment to see just how these parts would fit together, if at all. Maybe I could get a small panel form eBay or Ali Express, and get a few 3D models printed to test.


