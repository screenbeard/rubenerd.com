---
title: "Organising posts by date"
date: "2021-10-27T09:02:59+10:00"
abstract: "Theme housekeeping I’ve always wanted to do."
year: "2021"
category: Internet
tag:
- weblog
location: Sydney
---
I always liked how the late, great [J-Walk Blog](https://rubenerd.com/goodbyej-walk-blog/) arranged posts with date headings on his home page. It made his site seem more like a periodical, rather than a stream. I finally got around to implementing this here.

I'm not sure if the style will remain the same, but it works for now.

I also took the opportunity over the weekend to remove a ton of legacy code and CSS, so let me know if it breaks anything for you. Thanks :).

