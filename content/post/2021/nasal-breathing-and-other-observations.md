---
title: "Nasal breathing, and other observations"
date: "2021-08-29T09:45:31+10:00"
abstract: "A bird swooped me, and I went down a Wikipedia rabbit hole."
year: "2021"
category: Thoughts
tag:
- health
location: Sydney
---
I was struck this morning by a thought, and a bird. The latter appeared out of nowhere on our little balcony and swiped at my hat, before being startled and flapping away. The former was borne of reading, an activity that's just as perilous depending on the material in question. Not in this case though, which leads me to the conclusion that this entire opening paragraph was pointless. Just like birds who think a flatcap is a threat to be neutralised, or a comestible to be eaten.

I'd been reading up on the Oxford-AstraZeneca vaccine that Clara and I have now had two shots of, and ended up on the Wikipedia article on [obligate nasal breathing](https://en.wikipedia.org/wiki/Obligate_nasal_breathing). *Obligate* sounds like a Beatles song, in which the singers opine that life goes on. *Obligate, obligate, life goes on, WAH! Ah na-na-na life goes on.* Those lyrics are apt given the aforementioned form of breathing's role in our continued survival.

I grew up saying wah in Singapore, but I [hear Ina whenever WAH comes up](https://www.youtube.com/watch?v=GxLQiefIVlA). Hololive has taken over my life over the last year, and I'm all the better for it. Wah.

If you'll stop interrupting me, I'm sure we've all realised that breathing though our mouth is less desirable than our nose. Having a blocked nose always results in a sore throat and loss of taste when we switch to our backup failsafe mode of breathing. It's not fun, though better than the alternative.

One of my silly sci-fi novellas in high school used an alien species' inability to breathe through their mouth as a plot device. They had a non-invasive contraption in their pockets at all times that would allow them to breathe if a nasal blockage or other complication was encountered. Humans had reverse-engineered them to allow us to breathe underwater, but ran into dependency issues where subjects couldn't wean themselves off using them after they'd started. My English teacher said it was a derivative and uninteresting idea, but it was at least believable based on how we humans react. That's my blog in a nutshell, now that I think about it.

Getting back to nasal breathing though, I didn't realise it [conferred all these benefits](https://en.wikipedia.org/wiki/Obligate_nasal_breathing#Humans)\:

> According to Jason Turowski, MD of the Cleveland Clinic, "we are designed to breathe through our noses from birth — it’s the way humans have evolved." This is because it is the job of the nose to filter out all of the particles that enter the body, as well as to humidify the air we breathe [..] and warm it to body temperature. In addition, nasal breathing produces nitric oxide within the body while mouth breathing does not.

The more you nose.

