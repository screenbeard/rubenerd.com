---
title: "Australian swimmer quits, and I’m not surprised"
date: "2021-07-19T07:05:18+10:00"
abstract: "Throw in some power imbalance and typical behaviour."
year: "2021"
category: Thoughts
tag:
- ethics
- news
location: Sydney
---
Last month to the day, [The Guardian Australia](https://www.theguardian.com/sport/2021/jun/15/swimming-australia-admits-failings-in-misconduct-complaints-procedures) and [ABC News](https://www.abc.net.au/news/2021-06-10/tokyo-olympics-maddie-groves-pulls-out-of-swimming-trials/100206470 "Australian swimmer Maddie Groves quits Tokyo Olympic trials, citing 'misogynistic perverts' in the sport") reported on a [tweet](https://twitter.com/MaddieGroves_/status/1402777387146768385) by Australian swimming medallist Maddie Groves:

> Let this be a lesson to all misogynistic perverts in sport and their boot lickers - You can no longer exploit young women and girls, body shame or medically gaslight them and then expect them to represent you so you can earn your annual bonus. Time’s UP.

It's maddening to see it come to this. The fact it doesn't come as a surprise to most of us is even more telling. Abuse in sport at this stage is akin to a religious organisation betraying the trust of their parishioners and burying sexual abuse reports. I'm not sure whether I'm angrier that it happened, or that it's become normalised.

I think I mentioned this years ago, but I briefly did some volunteer technical work in Singapore for an aquatic team and community centre one school holidays. Scorekeeping, web hosting, provisioning laptops, etc. It's why I know so much about FINA and diving despite having the aquatic abilities of a stone. Not pumice, one of those dense ones. Even there I remember being aware of a few people who routinely made inappropriate comments. It was against both genders, but suffice to say you and I know who did the most of it, and who most of their targets were.

Amp that dynamic up to eleven, and throw in the steep power imbalance at something national like Swimming Australia, and it sends chills up my spine.

But here's the thing, and the reason I keep posting stories like this now. *I wasn't an ally during those times*. I shouldn't have kept it to myself, it was on people like us to channel the awkwardness of those situations into calling the men out who did it. That's the only way we're going to affect change.

