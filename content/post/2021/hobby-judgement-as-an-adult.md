---
title: "Hobby judgement as an adult"
date: "2021-02-10T08:40:26+11:00"
abstract: "“Being grown-up lets you enjoy these things!”"
year: "2021"
category: Media
tag:
- card-captor-sakura
- hobbies
- personal
- sailor-moon
location: Sydney
---
I can't remember who tweeted this, and I'm paraphrasing, but they said it's great to revel in hobbies and fandoms as an adult, because there's so much less judgement than being a kid. I'm glad there's an upside to all the extra responsibilities and burdens we have while we pretend to be grown up!

*(I use the royal **we** above, because my sister and I had far more responsibilities as teenagers than we do now, for family reasons. And I'm sure we weren't alone. Wow, this got dark fast, back to liking things).*

Case in point, I tended to like shoujo anime series and manga over shounen, or even Western superhero comics. I felt like I could relate to the characters more than the latter's bombastic antics, and shoujo art is *gorgeous*. But admitting I liked Card Captor Sakura and Sailor Moon in school would have placed a sparkly flower-shaped target on my back.

On the other side, I liked collecting model cars, but most of the traditionally male toys targeted at me didn't interest me at all. Lego had their spectacular misstep with targeting specific sets at girls, but their toys were gender neutral when my sister and I were kids. 

*(Computers were great for similar reasons; I could tell people I was getting them in lieu of gaming consoles to play first-person shooters, but in reality I was driving trains through scenic landscapes and writing silly little programs. I openly talk about these now, but again, would have made my even more of a social outcast as a kid)!*

My teenage self would have scarcely believed that I'd now be fanning with my girlfriend over new *Hololive*, *Fate/Grand Order*, or *Atelier Ryza* costumes for our favourite characters. Granted their "summer" transformations are primarily targeted at guys (cough), but the general idea that it's permissible for boys to be interested in what's basically dress up of electronic dolls is a *monumental* step forward in breaking down these gender stereotypes. I don't think that gets enough credit.
 
Danny Choo's [Smart Dolls](https://info.smartdoll.jp/en) are another great example. Ball-jointed dolls (BJD's) have existed in Japan for years, but had yet to make meaningful inroads internationally. They're relatively expensive, and Clara and I definitely don't *need* them, but that hasn't stopped me figuring out [which pants](https://shop.smartdoll.jp/collections/filter-apparel-bottoms/products/cargo-pants-2019) and [top](https://shop.smartdoll.jp/collections/filter-apparel-tops/products/weathered-hooded-zip-cardigan) I want for my [hypothetical one](https://shop.smartdoll.jp/collections/filter-smart-doll-all/products/smart-doll-starlight-cinnamon), cough. I've sure been coughing a lot on this post.

I still feel like I self-censor, but those walls are coming down. I think it's still one of the great things about the Internet; someone out there has similar interests to you.

