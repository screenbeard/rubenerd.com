---
title: "Comprehensive Breville BKE395 kettle review"
date: "2021-02-21T21:47:42+11:00"
abstract: "It’s delightful."
thumb: "https://rubenerd.com/files/2021/breville-kettle.jpg"
year: "2021"
category: Hardware
tag:
- food
- reviews
location: Sydney
---
<p><img src="https://rubenerd.com/files/2021/breville-kettle.jpg" alt="Picture of the aforementioned kettle." style="width:397px" /></p>

It's small, simple to operate, boils water *fast*, and you get to watch it bubble away through its clear walls. We got one for our little kitchen to match our appliances, and it's been delightful.

