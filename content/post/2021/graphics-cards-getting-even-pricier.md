---
title: "Graphics cards getting even pricier"
date: "2021-01-06T14:31:00+10:00"
abstract: "Expensive hardware may motivate efficiency."
year: "2021"
category: Hardware
tag:
- gpu
- shopping
---
[Brad Chacos reported](https://www.pcworld.com/article/3602659/graphics-cards-are-about-to-get-a-lot-more-expensive-asus-warns.html) for PC World yesterday:

> Ever since Nvidia’s GeForce RTX 30-series and AMD’s Radeon RX 6000-series graphics cards launched last fall, the overwhelming demand and tight supply, exacerbated by a cryptocurrency boom, has caused prices for all graphics cards to go nuts. Brace yourself: It looks like it’s about to get even worse.

Reporters, I emplore you again, don't say *last fall*. It doesn't apply to half the planet. He's referring to Q3 last year, for those of you below the equator.

I lost the original tweeter, but the best response I saw was someone saying there may be a silver lining here: expensive hardware may motivate efficiency. 
