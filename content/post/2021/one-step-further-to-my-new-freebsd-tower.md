---
title: "One step closer to my new FreeBSD tower"
date: "2021-10-19T08:28:55+10:00"
abstract: "Getting a new Dell S2722QC 4K panel, and my GTX 970 is too long for the sleeper PC case!"
year: "2021"
category: Hardware
tag:
- bsd
- compaq-spaceship
- freebsd
- monitors
location: Sydney
---
In late July I blogged about buying a [Compaq spaceship](https://rubenerd.com/finally-buying-a-compaq-spaceship/) computer case. I had an unusual obsession with these towers as a kid, on account of them having a (very!) passing resemlence to rockets if looked at from the right angle. My plan was to build a new personal FreeBSD "sleeper PC" into it, which would also look great sitting next to my beige Pentium 1 tower, and my Commodore 128.

The last piece of the puzzle has been figuring out how to KVM it. I use a 16-inch MacBook Pro for work, which until last week was hooked up to an LG Ultrafine 4K display, a Microsoft split keyboard, a gigabit Ethernet dongle, and a Kensington trackball. Clara's and my apartment is tiny, so I figured I could use a KVM box to switch between the docked MacBook Pro, and the FreeBSD desktop.

*(My motivation was to split more personal stuff off this Mac, and onto my Panasonic ultraportable, and a personal tower. I think part of my anxiety comes from mixing work and home stuff, which I'm trying to disentangle).*

The biggest complication here was the LG Ultrafine display. It works great connected to this Mac (albeit with the dreaded light bleed issue on the edges), but it requires macOS to adjust brightness. There are aftermarket hacks to get this working on Windows, but I doubt I'd be using it on FreeBSD or Linux. No matter how I *looked* at it (hah), non-Macs connected to this thing would be sub-optimal.

<p><img src="https://rubenerd.com/files/2021/20_dell_s2722qc_i_s2722dc_27_calowe_monitory_z_matrycami_ips_o_rozdzielczosci_4k_oraz_wqhd_i_wsparciem_amd_freesync_0_b@1x.jpg" srcset="https://rubenerd.com/files/2021/20_dell_s2722qc_i_s2722dc_27_calowe_monitory_z_matrycami_ips_o_rozdzielczosci_4k_oraz_wqhd_i_wsparciem_amd_freesync_0_b@1x.jpg 1x, https://rubenerd.com/files/2021/20_dell_s2722qc_i_s2722dc_27_calowe_monitory_z_matrycami_ips_o_rozdzielczosci_4k_oraz_wqhd_i_wsparciem_amd_freesync_0_b@2x.jpg 2x" alt="" style="width:500px; height:316px;" /></p>

After months of indecision, I saw a special on a refurbished Dell S2722QC (gesundheit) 4K panel and pounced last week. It has a lower pixel density than the Ultrafine, but that's to be expected from the PC world. It's still surprisingly crisp even running at 2x Retina, and I'm loving that it's matte! The difference in glare is **ridiculous.** 
 
I've assembled most of the parts, so the next step is drilling the standoffs into the Compaq tower and installing the parts. I only noticed last week that my old GTX 970 is a few centimetres too long to fit into the case, so I might need to sell that for a few bucks and get something smaller. I've seen a few low-profile 1060s that don't go for silly money second-hand, despite the best efforts of crypto-"currency" miners.

I'm also keeping an eye on KVM boxes that work with DisplayPort, assuming they exist. Worst case I can plug the powered USB hub with my peripherals into the tower when I'm not using the laptop, but I'd rather have a nice switch to do it. [Let me know](https://rubenerd.com/about/#contact) if you've had experience with anything like that, I'd be keen to hear.

