---
title: "Thinking about movies"
date: "2021-03-20T11:00:32+11:00"
abstract: "I... don’t generally enjoy them."
year: "2021"
category: Media
tag:
- independent-creators
- movies
location: Sydney
---
I overheard this at a coffee shop this morning:

> My favourite thing about movies is not knowing what's going to happen!

I assumed that was the entire point of seeing movies until I met Clara, who enjoys reading spoilers before seeing them. That makes *no* sense to me, but then our relationship would be boring if we understood everything the other person did.

I've also got my own weird disposition when it comes to moves: I generally don't enjoy them. Sitting still for hours at a time, not being able to think about anything else, and leaving the cinema into the bright light of day feeling fatigued and lazy... the experience isn't something I'm in a hurry to repeat. 

I don't have ADD&mdash;and people joking they have it demean those who do&mdash;I just feel like I have a gigantic and ever-expanding list of personal projects and fun stuff I'd rather be doing.

A part of this was also realising I don't like being a passive consumer. One of the great things about indie YouTube video creators, podcasters, and bloggers is that I can financially support them directly, answer questions they ask, screenshot and share their ideas on my own blog, and so on. You can't do that with Hollywood. That feels a bit like a game changer!

