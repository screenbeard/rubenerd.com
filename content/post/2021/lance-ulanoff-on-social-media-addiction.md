---
title: "Lance Ulanoff on social media addiction"
date: "2021-12-21T10:22:26+11:00"
abstract: "I thought it was a balanced article on the pros and cons, and how we can approach a healtier relationship with this stuff."
year: "2021"
category: Internet
tag:
- social-media
- sociology
location: Sydney
---
I didn't realise Lance Ulanoff had a Medium account. I grew up reading his columns in PC Magazine, so this was quite a treat.

One of his most recent articles titled [Social Media’s Fine Line Between Addiction and Necessity](https://medium.com/@LanceUlanoff/social-medias-fine-line-between-addiction-and-necessity-8ec03b5ff3a1) hits the nail on the head in a few ways. He discusses the benefits of technology and how its lead to us feeling more connected, but concludes (emphasis added)\:

> ... it doesn’t take a degree in psychology or computer science to understand that these systems that are **programmed to feed you what you like** based on what you do on the platform and not necessarily who you are is potentially harmful, and because they feed us what we want, potentially addictive. Getting what you want 24/7 isn’t always a good thing.

This is exactly why I push back against insinuations that [social media algoritms are benign](https://rubenerd.com/its-not-always-the-algorithm/). These companies have it in their business model and survival strategy to keep your eyes glued to their platforms, and will use whatever data they can mine in whatever way to maintain this. Sometimes that works to our advantage, like discovering a new related channel on YouTube. Often though, it doesn't.

I hadn't considered Lance's mental health perspective too. Addiction comes from reward, wiring ourselves to be receptive to it over time, and delusions about what it's doing to us. I'll bet doomscrolling plays into this, too.

Lance concludes:

> Managing these addictions will always fall back to us: the adults who have their own issues, and their teens who need our guidance, rules, and help. It’s probably too late to get current teens off social media, though you might be able to curtail some of their time spent. Now, however, the time to engage with children under 10 to remind them that technology and social media are tools we use and not resources, like oxygen and water, we need to survive.

I'm not a parent, and feel at times that I even struggle to maintain a healthy relationship with it *myself*, let alone helping someone else cope with it. In some ways I'm relieved that ICQ, IRC, Friendster, and MySpace were the worst I had to deal with in high school, and those were nothing if tame by comparison.

That said, curtailing time spent on social media is probably best served by talking with young people candidly and honestly about its impacts. In other words, saying there are benefits to limiting your exposure, rather than blocking it wholesale.

I'm still on social media, but disabling all notifications, stopping myself idly going to it on my phone to fill small gaps of time, and allowing myself to ignore certain people and replies rather than satisfying the sugar hit of engaging, has made a huge impact both on my mood and attitute toward the platforms themselves.

Like all tools, they can be applied appropriately to a circumstance, or you can bash yourself silly. I know which one is less painful; and the other is to overuse social media.
