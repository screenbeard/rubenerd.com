---
title: "More cores are good"
date: "2021-05-25T15:17:57+10:00"
abstract: "Sometimes this is forgotten in press coverage."
year: "2021"
category: Hardware
tag:
- cpus
location: Sydney
---
*(Update: An earlier version of this post was titled "More cores is good", and it remains the permalink. It still makes grammatical sense in context, but it's awkward. Like me)!*

Discussions around Moore's Law thesedays invariably end with a resigned sigh, and an admission that we're "only" inking out more performance though pipeline consolidation&mdash;such as the Apple M1&mdash;and more cores.

*Moore cores!* Ah that's good. Still not as good as AMD's wasted opportunity to brand their binned 3-core CPUs as Tricorders or Tristars back in the day. But I digress.

Appreciate for a moment though that we've gone from single and dual core CPUs to ones with six, eight, and more becoming ubiquitous within a decade or so. Our workstations, desktops, and laptops now have more CPU cores than some servers or even supercomputers did in our lifetimes.

In the rush to opine the state of the silicon ceiling and lithographic constraints, the temptation is there to downplay how significant an improvement more cores provide. Humans don't think like CPUs. We're scatterbrained. We're capable of holding and processing multiple thoughts at the same time. This is true even when we attempt mindfulness or meditation, not that I'm bitter or anything. This manifests in how we work, and there's no question more cores has helped with this.

I love that my laptop here can have a few VMs running, a pkgin update being processed, a browser playing a video, and all the disparate chat apps one needs to conduct business in these times. Throw in my email client, maybe some background rendering, and a weekly deep ZFS scrub, and it starts to feel a bit like magic when you step back and admire all the asynchronous communication and moving parts working together without stepping on each others’ toes.

Each one of these tasks might be encountering the laws of diminishing returns as far as their compute times are concerned, but being able to command six separate little machines with the same input devices, storage, and display is neat. A hex-core CPU isn't just the sum of its parts; it's *more* useful than six separate machines.

