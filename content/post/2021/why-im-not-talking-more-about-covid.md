---
title: "Why I’m not talking more about Covid"
date: "2021-01-06T08:43:00+11:00"
abstract: "It's a coping mechanism, I'm not qualified, and need more anime reviews"
year: "2021"
category: Thoughts
tag:
- covid-19
- feedback
- konosuba
- personal
- weblog
---
I've still yet to make my way through all the comments people sent in the last couple of months; I wanted to thank you all again, and to let you know I'll get to them soon!

But I wanted to address one from Matt Chase in the UK who sent me a long, heartfelt message last night. Among his concerns was why I'm not discussing Covid here as much as I used to, especially given it's still ravaging parts of the world, and given it's arguably the biggest current news story of our time after climate change.

I did talk about it fairly often back in March last year from my own narrow perspective. Australia and New Zealand have had flareups, but we're still anomalies. The UK had more new cases yesterday than we've had since the pandemic started, and my friends in the US keep sharing heartbreaking stories and reports. Other parts of the world are doing even worse.

<p><img src="https://rubenerd.com/files/2021/konosuba@1x.jpg" alt="Key visual from Konosuba" style="width:192px; float:right; margin:0 0 1em 2em" srcset="https://rubenerd.com/files/2021/konosuba@2x.jpg" /></p>

I'm not sharing more stuff about it for three reasons. I'm not qualified to discuss it beyond lived details, and there's already enough <del>misinformation</del> lies out there. It's also a coping mechanism; this blog was first and foremost supposed to be something I like writing, and would enjoy reading if I came across it.

Is it a copout? Probably. But the final reason is I want this place to be a break from it. I hope this doesn't come across as insensitive.

Matt also asked why I don't do more anime reviews like I used to. If I want this to be a place where one can take a break, *challenge accepted!* I started watching *[Konosuba](https://sneakerbunko.jp/konosuba/)* on a recommendation [from a friend](https://jamiejakov.lv/), and it's just the right amount of silly energy we all need right now. And all that news coming out from Type Moon!? Stay tuned!
