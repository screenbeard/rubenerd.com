---
title: "The home storage crisis, via Doc Searls"
date: "2021-07-04T09:45:57+10:00"
abstract: "What are home users to do with camera phones that shoot 4K?"
thumb: "https://rubenerd.com/files/2009/4176626712_5e6671d575_h@2x.jpg"
year: "2021"
category: Hardware
tag:
- bsd
- freebsd
- hard-drives
- openzfs
- phones
- storage
- zfs
location: Sydney
---
No, not [that ridiculous Chia storage crisis](https://www.tomshardware.com/how-to/how-to-farm-chia-coin-the-new-storage-based-cryptocurrency). Doc [discusses something](https://blogs.harvard.edu/doc/2021/06/24/a-storage-crisis/ "A storage crisis") with which I'm sure we're all familiar:

> The best new phones come with the ability to shoot 108 megapixel photos, record 4K video with stereo sound, and pack the results into a terabyte of onboard storage. But what do you do when that storage fills up? 
>
> If you want to keep those files, you’ll need to offload them somewhere. Since your computer probably doesn’t have more than 2Tb of storage, you’ll need an external drive. Or two. Or three. Or more. Over time, a lot more.

His solution was to keep his local drives, and back them up to the cloud. That works if you have a fat pipe, and are willing to trade immediate access for convenience, and the knowledge that a team of storage experts are maintaining that remote copy. The former rules out most of Australia.

The options are:

* Use what I dubbed a [Stonehenge of External Drives](https://rubenerd.com/logitech-premium-powered-usb-hub/) back in 2009. Iomega had that excellent drive catalogue software in the 1990s that let you look up which Zip, Jaz, or Ditto cart had a file you were after; I wonder if there's something similar now?

* Take the eggs-in-one-basket approach and buy one very large external drive. Maybe you could use another drive as a duplicate that you keep off-site, assuming you remember to do this regularly.

* Get on board the NAS train or build a home server, assuming you want another appliance somewhere in your house you need to maintain.

* Accept that you'll never be able to keep anything or everything, and use the camera in your phone for ephemeral social networks like Instagram. I'm seeing more people do this, as terrifying as it sounds to me.

I went with the home server option, with a series of OpenZFS mirrored pools on a FreeBSD server. OpenZFS is the only file system I trust with anything important, but there's no doubt building that setup was expensive. There isn't a one size fits all solution here.

It's funny that it took Doc reminding me of the capabilities of our phones to get me thinking about this again.
