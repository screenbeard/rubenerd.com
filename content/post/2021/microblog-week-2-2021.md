---
title: "Microblog, week 2 of 2021"
date: "2021-01-08T08:54:00+11:00"
abstract: "My temporary substitute for Twitter."
year: "2021"
category: Internet
tag:
- food
- microblog
- microsoft
- music
---
I used to do this back in the day when Twitter was down. Now I'm posting my scatterbrain thoughts here instead while I'm on a [Twitter holiday](https://rubenerd.com/taking-a-twitter-break/).

* Uh oh, I went to [Gong Cha](https://gongchatea.com.au/) and Clara went to [Share Tea](https://sharetea.com.au/) for boba. There be disruption in the force!

* Made it through a bottle of Listerine Coconut and Lime. Nice for a change, but not my cup of tea. Because it'd be toxic drunk like that.

* Times I've instinctively reached for Twitter since I started this break: lost count after 12. Gulp.

* Think like a reductive cynic: Microsoft [released the Polaris source](https://www.neowin.net/news/a-canceled-windows-core-os-build-has-reportedly-leaked-online) on purpose to drum up interest in Windows.

* First three words iOS suggests I start my sentences with: Hey, Yeah, The.

* Pretty sure a CAPTCHA I just had to complete confused ducks for cars.
 
* *I count the hours that I lie awake. I count the minutes and the seconds too... ♫* 
