---
title: "Bypassing Windows 11 Internet account install"
date: "2023-03-02T17:18:17+11:00"
abstract: "SHIFT+F10 during network detection, then OOBE\\BYPASSNRO."
year: "2023"
category: Software
tag:
- windows
- windows-11
location: Sydney
---
If you have to install Windows 11, this is true at time of posting:

1. Install as normal
2. When prompted for a network connection, type **SHIFT+F10**
3. Type `OOBE\BYPASSNRO`
4. Restart, and choose **I don't have internet [sic]**
5. Create a local account

I found this by going to a site so loaded with ads, newsletter signup boxes, and autoplaying videos, I thought I was back in the Web 1.0 days without a popup blocker. I hope this saves someone else the trouble!
