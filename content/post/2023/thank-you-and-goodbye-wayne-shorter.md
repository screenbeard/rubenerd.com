---
title: "Thank you and goodbye, Wayne Shorter ♡"
date: "2023-03-03T13:12:43+11:00"
abstract: "Time to put Footprints on repeat."
year: "2023"
category: Media
tag:
- goodbye
- jazz
- music
location: Sydney
---
[Herbie Hancock](https://twitter.com/herbiehancock/status/1631379950660988928)\:

> Wayne Shorter, my best friend, left us with courage in his heart, love and compassion for all, and a seeking spirit for the eternal future. He was ready for his rebirth. As it is with every human being, he is irreplaceable.

I'm sorry to have never seen him perform, but I know [Footprints](https://www.youtube.com/watch?v=LgaIUqH0w6c) will be on heavy rotation for the next while. What an icon.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=LgaIUqH0w6c" title="Play Footprints (Remastered)"><img src="https://rubenerd.com/files/2023/yt-LgaIUqH0w6c@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-LgaIUqH0w6c@1x.jpg 1x, https://rubenerd.com/files/2023/yt-LgaIUqH0w6c@2x.jpg 2x" alt="Play Footprints (Remastered)" style="width:500px;height:281px;" /></a></p></figure>

