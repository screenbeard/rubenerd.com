---
title: "FreeBSD and NetBSD laptop feedback: the Framework"
date: "2023-03-14T09:37:47+11:00"
abstract: "The Framework Laptop was the overwhelming recommendation."
year: "2023"
category: Hardware
tag:
- bsd
- feedback
- freebsd
- framework-laptop
- netbsd
location: Sydney
---
About a dozen of you emailed and posted on social media in response to my [question about FreeBSD and NetBSD laptops](https://rubenerd.com/a-new-freebsd-laptop/), thanks!

Overwhelmingly the recommendation was the [Framework](https://frame.work), which I've looked at before but completely forgot. Their website doesn't make it easy to find the display resolution, but other sites report it as 2256×1504, which is excellent.

The [FreeBSD wiki](https://wiki.freebsd.org/Laptops/Framework_Laptop) has a page about it, and it'd be cool to do some testing on NetBSD to [add to their wiki](https://wiki.netbsd.org/laptops/) too.

It probably won't be in the budget for a while, but I'm keen to try. Ping me if you have any experience with running BSD on this laptop, I'd love to hear about your experience.

