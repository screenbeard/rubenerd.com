---
title: "The overjustification effect"
date: "2023-02-02T15:36:40+11:00"
abstract: "If people are doing something for an intrinsic reward and you start giving them extrinsic rewards for it, they lose their intrinsic motivation"
year: "2023"
category: Thoughts
tag:
- psychology
location: Sydney
---
Richard Bartle recently talked on a "Crypto Circle" panel in the UK, which to my disappointment wasn't about cryptography. [Nobody heeded his warnings](https://nwn.blogs.com/nwn/2023/02/richard-bartle-crypto-circle-blockchain-nft-virtual-worlds.html), which sounds about right for those drunk on blockchain KoolAid, or turpentine, or whatever they drink.

His comment about play-to-earn games introduced a new phenomena to me:

> [T]he psychological phenomenon known as the overjustification effect means that if people are doing something for an intrinsic reward and you start giving them extrinsic rewards for it, they lose their intrinsic motivation. Imagine if you were reading a book and were paid for every page you read: after a while, you'd be reading pages because you wanted paying, not because of the content.

This would also explain why turning a hobby into a career isn't always a winning move.

