---
title: "Towns that sound like toothpaste"
date: "2023-01-29T10:01:42+11:00"
abstract: "Crestview."
year: "2023"
category: Thoughts
tag:
- pointless
location: Sydney
---
From some quick searches:

* Crestview, Florida, USA
* Toothill, Essex, UK

And one that doesn't:

* Flekkefjord, Norway
