---
title: "Rudy Reyes on being a hero"
date: "2023-01-27T13:14:04+11:00"
abstract: "“A hero's light shines simply by living true to their authentic self in sharing that light with others.”"
year: "2023"
category: Thoughts
tag:
- quotes
location: Sydney
---
<a href="https://en.wikiquote.org/wiki/Rudy_Reyes_(actor)#Hero_Living_(2009)">From his 2009 book</a>:

> In moments when you may feel isolated or alone, it may not seem so, but what you do, what you think and say does make a difference simply because we are all connected- oftentimes in ways you may never know. But knowing every detail of the effect of your actions or your connection isn't the point of a hero's deeds.
>
> A hero's light shines simply by living true to their authentic self in sharing that light with others.
