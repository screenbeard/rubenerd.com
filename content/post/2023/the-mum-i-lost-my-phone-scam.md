---
title: "The “I lost my phone” scam"
date: "2023-01-19T08:21:37+11:00"
abstract: "A new generation of social engineering attacks targeting people’s phones."
year: "2023"
category: Internet
tag:
- scams
- spam
location: Sydney
---
I'm seeing an uptick in spam messages claiming the sender lost their phone, and that they're messaging from a friend's device. They impart a sense of urgency by claiming they're stranded, need money, and that their friend's phone is also running short of battery. Or long, depending on the form factor. Thank you.

It's an interesting form of social engineering for several reasons:

* The story being spun sounds plausible, if unlikely.

* It portrays the attacker as vulnerable, which activates the victim's good will, pity, and desire to help.

* The urgency of the alleged situation encourages victims to act quickly before thinking too critically about it.

* It doesn't rely on clicking a dubious link, at least not before trust has been established.

Here's one I got last week:

> Hi mum/dad I'm texting you off a friends phone I've smashed mine and their phones about to die can you message my new number on Whatsapp [redacted]

I'll admit, I came the closest I've ever come to falling for a scam like this. I was in the middle of a meeting, and with my divided attention I almost sent a message telling them they had a wrong number. This would have surely started a conversation to lure me in.

Here's another variation on the theme:

> Mum I've dropped my phone. Can you message me on whats'app heres my new number [redacted], it is urgent please x.

And they're already upping the ante:

> I'm at Woolworths and brought the wrong card with me. Can you please send me 240, I will pay you back when I get home. BSB [redacted] Acc [redacted]

It wouldn't surprise me if an AI is also being used to qualify and interact with people using canned or generated responses, at least initially. Then when you've hooked someone, you can refer them to a real person to finish the grift.

But there's good news. I dub these *novelty* attacks, because their impact is blunted by subsequent messages of a similar theme. Having a person get your number wrong once is plausible. Receiving ten wrong numbers in as many days? Not so much.

