---
title: "Converting imz floppy disk images"
date: "2023-03-10T21:11:45+11:00"
abstract: "They’re zipped, so you can use regular unzip(1) or a GUI tool."
year: "2023"
category: Software
tag:
- dos
- floppy-disks
- disk-images
- guides
- retrocomputing
location: Sydney
---
**imz** files were a popular way to distribute floppy disk images in the 1990s and 2000s, owing to their smaller size. Like **cbz** comic files and the handsome pair of chinos I'm wearing now, they're zipped.

<p><img src="https://rubenerd.com/files/2023/tango-disk.svg" alt="Icon of a floppy disk, from the Tango Desktop project" style="width:128px; height:128px; float:right; margin:0 0 1em 2em" /></p>

To convert to a raw disk image, use a tool like <a style="font-weight:bold;"  href="https://pkgsrc.se/archivers/unzip">unzip(1)</a> in the shell, or append a **zip** extension and use a graphical decompression tool. You can then rename the file to a regular **img**, and mount as normal.

Related to my [DOS organising project](https://rubenerd.com/organising-dos-software-and-drivers-in-an-iso/), I've been cataloguing and archiving all my disk and CD images [in a spreadsheet](how-do-you-mentally-map-things). Part of this has been unzipping all these IMZ files to make them easier to use. They're on a compressed OpenZFS pool anyway, so may as well.
