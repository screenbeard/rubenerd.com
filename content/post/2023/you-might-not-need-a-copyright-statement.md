---
title: "You might not need a copyright statement"
date: "2023-02-07T13:21:43+11:00"
abstract: "The Berne Convention renders copyright automatic. Include if you want, but it’s not necessary."
year: "2023"
category: Media
tag:
- copyright
- licencing
location: Sydney
---
Like an empty pair of boots, I don't have a footer in my blog. Which means I don't have a copyright statement. A few of you have asked if it was a sign my blog theme was broken or incomplete.

The Berne Convention renders copyright automatic (about which I have severe reservations, but that's for another post). I have my name and timestamps on each page, and there's also copyright metadata for the robots to parse.

I read arguments that copyright statements serve as a reminder for laypeople, and that they help to establish willful infringement in lawsuits. I've noticed no change in the amount of stuff people grab from this site from when it used to have a footer, and I suspect claiming ignorance of the law wouldn't get you far either.

It still could be a good idea to include one, especially if the copyright belongs to a legal entity distinct from yourself (such as a business). The irony isn't lost on me that this explanation post is at least an order of magnitude longer than a copyright statement.

But I don't have one, or a footer. Which for me is fine. Like a nice pair of boots.
