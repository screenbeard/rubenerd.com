---
title: "Pieces of 8: Alternative uses for a new hair brush"
date: "2023-01-26T14:44:52+11:00"
abstract: "Another pointless list of very useful things."
year: "2023"
category: Thoughts
tag:
- lists
- pieces-of-8
location: Sydney
---
* A doorstop
* A scalp massager
* Planks for a barber-themed float
* A tiny cricket bat
* Suboptimal noodle tongs
* A fly swat
* Table number marker for a hipster restaurant
* Fuel

