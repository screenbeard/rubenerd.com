---
title: "Libraries were full of tech inflexion points"
date: "2023-03-18T22:22:46+11:00"
abstract: "Microfiche, card files, DOS lookup terminals, audiobooks... and now laptops!"
year: "2023"
category: Hardware
tag:
- archiving
- childhood
- history
- libraries
- nostalgia
location: Sydney
---
Have you ever looked at a specific moment in your life and realised *this is where everything changed?* I've been thinking about tech inflection points again recently (while trying to distract myself from grim ones like AI), and I keep coming back to the beloved libraries in which I grew up.

When we lived in Melbourne in the early 1990s, our local council library had newspapers on microfiche, which my parents would show me. It seemed impossible for such a tiny slide to contain so much information, though it answered an early question in my head about where these libraries *keep* all their archives. Doesn't newspaper degrade, yellow, and fall apart!?

My parents were big into audiobooks, so we'd also be checking out dozens of cassette tapes. Books were out on CDs by the time we moved to Singapore, but aside from being more portable they weren't actually better. A tape would remember exactly where you left off; CDs required *you* to remember your last track.

But what happened when you actually needed a book from one of these library places? You'd look it up in the card file! You'd approach a wall of small drawers, in which were thousands of cards with details of each volume were contained. I already can't remember if these were sorted based on the Dewey Decimal number, or if it was purely alphabetical. It does make me wonder where all those chests ended up.

Our local library, and my school's library, were both already in the process of replacing their card file systems with DOS PCs with blue screens and white tables of text by the time I started using them seriously. Even that would be quaint by today's standards (though I knew one librarian in Singapore who hated their new Windows XP system and wished they could go back to the text-based one they had before).

Aside from outing myself as being in my thirties again with this rose-tinted retrospective, it's interesting walking into a library now and seeing the rows of tables covered in laptops and tablets. The books, periodicals, magazines, music, and encyclopædias are all still there, though I wonder how long they'll exist too?
