---
title: "A convincing(ish) phone scammer"
date: "2023-01-31T18:10:43+11:00"
abstract: "I’ll admit they had me at the start, which is scary."
year: "2023"
category: Thoughts
tag:
- scams
- security
- spam
location: Sydney
---
I had what might have been my first convincing scammer call me last night.

### The call

They called me after work purporting to be from Amazon. They had excellent colloquial Australian English, spoke calmly and clearly, had researched the weather in Sydney, and even responded positively when I asked how their day was going. I like to do some idle small talk before calls to built rapport (because being in a call centre can be a thankless job), but it's also a great way to sus people out.

Armed with just enough information from various high-profile leaks to sound mildly convincing, she began to weave a story that my account had been compromised. She asked if I knew anyone in Germany, which I said I did! This threw her a bit, but then she changed her mind and said the card associated with my account had been used by someone in Spain to purchase an iPhone 14.

It was clearly a scam at this stage, but curiosity got the better of me and I wanted to see where it went. **You should not do this,** and that includes me.

She talked a bit faster after this, presumably to bamboozle me and get me into a receptive state to start divulging information. She started asking pointed questions about my credit cards, which I replied by saying "you should have that on file, right?" I threw her a bone and gave her a fake bank and CVV number which she "verified", before finally handing me off to someone else when she gave up asking for a card number.

This second gentleman was the bad cop. He said someone was using my card, and that he was trying to help me, so to "stop fucking around". I chided him for his inappropriate language for which he apologised. I let him explain multiple times why they needed a card number to cancel a transaction, which I called out for being nonsense. "We don't have that information". "But I thought you said you worked at Amazon". Etc.

It should have been clear by then that I was jerking them around with obtuse questions and fake information, but they stuck around until the end. I even pretended to rememberer that in fact I did have a friend in Canada who used my card to buy a moose. *An iPhone you idiot, the scammers bought an iPhone in Germany! [sic]*

Eventually I got bored with the conversation going in circles, so I said I felt bad for them needing to scam people to make a living. I even took a stab at guessing his Punjabi accent (it's hard to explain, but I had Indian friends in Singapore and there are a few tells) and saying that I knew someone in Ludhiana. He maintained that he worked at Amazon, and I asked if he could get a better job, if he had a support network, whether his mental health was okay, etc. He remained obstinate, so I wished him and his female parter in crime the best in getting a legitimate career, but to stay out of cryptocurrency and tulip mania. Then hung up.


### Lessons

This was the first time I answered and interacted with a scammer on a phone for an extended period of time. I was a little shocked and disappointed in myself that they'd been able to hook me at the start, but I did get some insight into how these conversations are structured.

First, as I said above, **don't engage with scammers**. Even the most careful and hawkish of victims can divulge personal data they don't intend. These people are professionals trained in information extraction and high-pressure sales tactics, and your arrogance at *pwning* a scammer can backfire before you know it. Even if you think you're doing good by wasting their time instead of the time of a vulnerable person, it's still not worth the risk.

I was surprised at just how much effort the first person had put into sounding Australian. She was clearly northern Indian, but then Australia is a multicultural society and I speak to people with mixed Aussie accents every day. This level of preparation might be common knowledge, but I'd assumed these scammers were more cliché and obvious.

More worrying was how these scammers have been able to so effectively build profiles for people based on information leaks. You can't trust someone even if they divulge that they know a snippet of your address. If you're uncertain, my favourite trick is to give them knowingly false data and see if they ask you to confirm or not. A legitimate outfit will immediately be able to notice the discrepancy, but a scammer won't.

At the risk of reinforcing the perception that millenials and zoomers never answer their phones, this will only inform my behaviour of said. Numbers can be spoofed, and accents can be faked in a low-fidelity call. If people have something important to tell me, they'll message me or leave me a voicemail after.

I'm shaken, but a bit wiser.
