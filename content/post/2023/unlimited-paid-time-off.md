---
title: "Unlimited paid time off"
date: "2023-01-12T12:21:52+11:00"
abstract: "Another idea that sounds great in theory."
year: "2023"
category: Thoughts
tag:
- work
location: Sydney
---
I saw the news that another large American corporation implemented PTO. [This comment](https://www.theverge.com/2023/1/11/23550470/microsoft-employees-unlimited-time-off-2023) on the Verge tracks with the experience of three of my friends in the US living under such a scheme:

> "Unlimited PTO" is kind of like being on the hamster wheel with the cage door open.

Not to mention that removing liability on the company's books translates to not being paid out if you leave.

I admit it's easy for me to say from Australia, where we get above average amounts of guaranteed paid time off and personal/family leave. That's the real answer, because it sets the expectations for both parties. It also lets you take the time off without guilt, because that fixed period is anticipated, not asked for.
