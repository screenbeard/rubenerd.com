---
title: "Daniel Aleksandersen on split keyboard accessibility"
date: "2023-02-28T20:33:58+11:00"
abstract: "Accessibility means different things to everyone."
year: "2023"
category: Hardware
tag:
- ergonomics
- keyboards
location: Sydney
---
I've posted a few times about my quest for the perfect split keyboard. I'd become accustomed to a [2019 Microsoft Ergonomic Keyboard](https://rubenerd.com/the-microsoft-ergonomic-keyborad-2019/), but have since been spoiled by my [Topre board](https://rubenerd.com/quest-for-a-thocky-split-keyboard/) from a Japan trip. It's a hard life, I know.

I only just subscribed to Ctrl.blog, and Daniel has [some words of caution](https://www.ctrl.blog/entry/split-keyboard-accessibility.html) I hadn't considered:

> The design of split keyboards imposes an accessibility barrier that isn’t there with regular keyboards. The touch-typing method is meant as a best practice for reducing hand strain and not as a hard requirement. Split keyboards assume you’re using the touch-typing method and have a set of fully functional fingers on both hands.
>
> For something to be truly ergonomic, it needs to be adjustable and adaptable to fit different people and needs.

He suggests split keyboards duplicate certain keys that straddle the split, such as B and Y. Alternatively, they could be made detachable.

Like Daniel, I have an injury on one of my fingers that makes typing difficult. The damaged nerves and muscle in my right pinkie make it hard to strike Return or colon reliably, so over the years I adapted by using my ring finger to perform double duty. This has the effect of shifting my whole hand off the home row by one key on occasion, which to this day makes me write gibberish if I'm not careful. It's almost as though I need a split spacebar with one side working as a Return that my thumb can strike instead.

Point is, I agree that there's an untapped market for truly customisable split keyboards that have such accessibility considerations. What might work for a functional touch typist with all ten digits and two thumbs working correctly might pose a challenge to others.
