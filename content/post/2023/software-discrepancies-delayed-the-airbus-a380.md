---
title: "Software discrepancies delayed the Airbus A380"
date: "2023-02-05T10:15:38+11:00"
abstract: "Wires were designed too short. Whoops!"
year: "2023"
category: Software
tag:
- airbus
- aviation
- design
location: Sydney
---
I have to disambiguate A380 now, given Intel's foray into discrete GPUs. There's something entirely nonsensical but fun about buying an ARC GPU for my next build so I can say it has an Airbus inside. But I digress.

Next time you're feeling inadequate about a software incompatibility or miscalculation, spare a thought for the various A380 development teams. [Simple Flying](https://simpleflying.com/airbus-a380-program-software-discrepancies-delay-story/) quoted a final assembly mechanic as saying:

> The wiring wasn’t following the expected routing through the fuselage, so when we got to the end, they weren’t long enough to meet up with the connectors on the next section

It wasn't a small issue:

> Nearly 100,000 wires and 40,000 connectors were affected, and the entire work had to the redone from the design stage.

The cause was attributed to incompatible software used by a German contractor. Tolouse wanted them to use the same software, but their management team resisted. They quoted an Airbus Hamburg consultant:

> Changing over to new software is costly and time-consuming. Nobody in upper management wanted to get their hands dirty.

It's a story as old as software itself.
