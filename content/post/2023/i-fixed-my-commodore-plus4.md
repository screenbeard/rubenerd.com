---
title: "I fixed my beautiful little Commodore Plus/4!"
date: "2023-01-17T19:44:14+11:00"
abstract: "All it took was some contact cleaner, resocketing, and a new PLA!"
thumb: "https://rubenerd.com/files/2023/commodoreplus4-running@1x.jpg"
year: "2023"
category: Hardware
tag:
- best-posts
- commodore
- commodore-plus4
- retrocomputing
- troubleshooting
location: Sydney
---
I've been feeling a bit dejected lately when it comes to retrocomputers. It seems all my beloved machines have faults that are *just* beyond my abilities to diagnose and fix. It's a journey for sure, but it's still frustrating to not have anything to show for all my efforts. But that ended last weekend!

<figure><p><img src="https://rubenerd.com/files/2023/commodoreplus4-running@1x.jpg" alt="Photo of the Plus/4 running on my lab desk." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/commodoreplus4-running@2x.jpg 2x" /></p></figure>


### Diagnosing flaky video and the PLA

My 1984 Plus/4 (and C16) hasn't been the same since our last house move. Her screen output over S-Video or RF would initially look perfect, then flicker and rapidly accumulate garbage text around the borders; specifically the £ symbol. I was about to get a picture of this Brexit metaphor for this post, but then the screen went black and refused to show anything.

I suspected her TED chip, as it's responsible for the graphic output of the machine. But while probing around the voltages I brushed against the Programmable Logic Array chip in socket U19 and nearly burned my finger off! The PLA on Commodore machines generates chip select signals and handles memory banking, among other critical tasks. Commodore engineer [James Redfield](https://www.c64-wiki.com/wiki/PLA_(C64_chip)) said it was the "glue logic to tie everything together". I've watched enough repair videos to know it has a high failure rate among MOS chips, and especially so on the 264 line.


### Swapping with the C16

The good news was I also have a Commodore 16 from the same family. The [ubiquitous SAMS guides](https://archive.org/details/SAMS_Computerfacts_Commodore_C16_1984-12_Howard_W_Sams_Co_CC8) talk about fixing a machine by testing their chips "by substitution", which is certainly easier when you have multiple machines to work with. I gently de-socketed the PLA from the C16, removed the PLA from the Plus/4, applied some contact cleaner across the sockets, then tried the C16 PLA in the Plus/4.

*Huzzah*, the Plus/4 booted without problems, with full colour, sound, and detected the 1571!

Keen-eyed readers will note the aspect ratio gives this away as an NTSC machine. I'm using a transformer for the Plus/4 power supply to step down my 230 VAC mains to 110 VAC, but it's still running at 50 Hz instead of the 60 Hz it probably expects. I haven't encountered any weirdness doing it, but I still have it on my wish list to convert this to a PAL machine with a new crystal and KERNAL [sic] ROM. Fortunately, unlike the VIC chip on the VIC-20, C64, and C128, the TED can operate on either.

<figure><p><img src="https://rubenerd.com/files/2023/commodoreplus4-table@1x.jpg" alt="The Plus/4 showing a welcome greeting!" style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/commodoreplus4-table@2x.jpg 2x" /></p></figure>

*(As an aside, my project desk is in the worst possible position for taking photos! It's backlit against searing Australian sun, it's dark, and the monitor is too bright even at its lowest setting. Maybe I need some studio lights).*


### Saucing (mmm) a replacement

I could have kept the C16's PLA in the Plus/4, but then I wouldn't be able to use the C16 again. I found a few people selling replacement PLAs on eBay for less than I expected, but I also learned about Eslapion's Plus/4 *PLAnkton*, a delightfully-named modern FPGA replacement that runs cooler and uses a fraction of the power. [Gamedude](https://www.gamedude.com.au/products/shop/commodore-plus4-plankton-ic-replacement-pla-251641-02) sells them in Australia for a very reasonable price, so I decided to give it a shot.

The 264-series PLAnkton comes in two models with different pin lengths, to accommodate the sockets and clearances of the C16 and Plus/4. My Plus/4 has significantly nicer sockets than the C16, and with a bit of contact cleaner it installed into U19 with no problems. I powered her back on, and she worked! Next step, adding some proper heatsinks.

*(Note the keyboard cable is still attached. I'm nervous about damaging the delicate connector, so I always leave the top shell of the case connected and to the side when working on the motherboard).*

<figure><p><img src="https://rubenerd.com/files/2023/commodoreplus4-plankton@1x.jpg" alt="View of the open Plus/4 showing the PLAnkton chip replacement installed." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/commodoreplus4-plankton@2x.jpg 2x" /></p></figure>


### Retrospective

I'm glad these machines are getting more attention and interest today, to the point where people are even investing time on creating videos and even replacement components. These are special machines that have a unique place in 8-bit computer history, even if Commodore bungled their marketing and pricing at the time. If the C128 is my favourite second-hand 8-bit computer of all time, the Plus/4 and C16 aren't that far behind.
