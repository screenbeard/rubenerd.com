---
title: "The beauty of CGI and simple design, by Hales"
date: "2023-01-06T23:25:43+11:00"
abstract: "Finding someone still singing CGIs praises is surprising, and some alliteration."
year: "2023"
category: Internet
tag:
- cgi
- design
location: Sydney
---
I was reading through some of Hales' blog archives over Xmas, like a gentleman, and was reminded of his [post about CGI](https://halestrom.net/darksleep/blog/046_cgi/), the simple application/web interface from the 1990s. And here's the thing: he said it was *good*.

He invited us to compare CGI to contemporary frameworks:

* No dependencies or libraries to install, upgrade, maintain, test, and secure.

* Works with any programming language ever that can read text and print text.

* Zero external configuration, other than telling your webserver to enable CGI on your file.

He concludes:

> It’s too easy to get caught up in "professional syndrome", where you look up to the big players and trust in their opinions. But you also need to understand that their opinions are based on their current experiences, which are often a world away from what the rest of us should be worrying about.

I see people making the same mistake with tools like Kubernetes. Many, if not most stacks, simply don't need it... and those that do pay the price for all the overhead and brittleness. But I digress.

The [first version](https://rubenerd.com/omake/#used-to-power) of my site here was written as a Perl CGI script. I learned more about HTTP (GET, POST, etc), HTML, and templates from that one script than years of university. And while I was ultimately scared away from all the multiple spawned processes, the reality was I never hit any server limitations. I even had a crude form of Model-View-Controller (MVC) going without even realising it.

The reason I bring this back up now is that I feel like I've reached an impasse on so many things. Personal projects remain unfinished or abandoned because I've wanted to do things the "right" way. So I start building it the "right" way with complicated tools and processes, then raise my hands and think it's all too much for what I want to do. It puts all sorts of things out of reach.

Hales mentions premature optimisation. I feel like I'm doing that in my *life.* Maybe I need some mental CGI.

