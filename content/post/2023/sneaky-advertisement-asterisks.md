---
title: "Sneaky advertisement asterisks"
date: "2023-02-09T07:33:50+11:00"
abstract: "Why do companies advertise in such a way that they need to include a glyph that makes their claims look suspect?"
year: "2023"
category: Media
tag:
- advertising
- business
location: Sydney
---
A billboard near our local train station was recently erected (bit rude, if you ask me) proudly proclaiming their disposable water bottles to be 100% recycled. I was impressed, if one were to ignore the pointless and wasteful nature of bottled water in the first place! But even a cursory look revealed the claim to be qualified by a large asterisk.

What does that usually mean to you?

When I see a large asterisk on anything, let alone an advertisement, my brain immediately discounts anything about it. If one were being truthful, my implicit assumption is that one wouldn't need the asterisk in the first place. I see it as little more than a mechanism for the advertiser to fulfil their legal obligations, while still talking out the other side of their mouth.

If *Betteridge's Law of Headlines* says the answer is *no* to headlines coached with a question mark, I'd say any advertising claim with an asterisk is probably bunk.

But that raises an interesting question.

If you think about what advertisers are doing, they're paying for a temporary space to occupy and project their messaging from. This means, as with any business transaction, they'd want to do whatever is necessary to maximise their return on investment. My simplistic interpretation of asterisks would seem to run counter to this aim: why would you volunteer to make a claim that requires the inclusion of a giant asterisk, a glyph that people like me have been trained to interpret as a signal the ad is untrustworthy, or can be ignored?

Marketers must have the numbers to show that sceptical people like me are in the minority, or that even a specious or oversimplified claim is valuable enough for an advertisement to make. Perhaps they're counting on people forgetting the asterisk, but not the central claim. Anything I've ever read and watched about advertising drives home the point that *staying power* is important, which is probably why some in in the industry still cling to the idea that negative publicity is good.

Someone more qualified in the legal aspects of this could probably dive into *letter and spirit of the law* considerations here, and whether an asterisk really is sufficient to satisfy legal requirements for truth in advertising! It reminds me of those pharmacies in my local Sydney suburb that claim "<span style="font-size:smaller;">Are we</span> <strong>Australia's cheapest chemist</strong><span style="font-size:smaller;">?</span>", with the first two words and the question mark in such tiny lettering as to be invisible. They're not making a claim that a sugar pill will cure a horrible disease, or that their store is giving away free things they aren't. But even if it's more benign, I still consider the tactic dubious!

Since I noticed that billboard, I've been actively checking others with asterisks to see what disclaimers or qualifications they have. It's been a bit of a lesson for me; a minority of ads use asterisks to explain the specifics testing methods used, with what legislation they're compliant, or that people drinking alcohol or gambling should "do it responsibly". But most do exactly what I suspected: they soften or retract whatever bold claim the ad made.

The 100% recycled water bottle from the start of this post? Turns out it doesn't apply to the label or the bottle cap. I'd consider the latter to be an integral part of the bottle, and necessary for its function, so the claim its 100% recycled is nonsense! But I guess they're legally in the clear, which would also be true of their bottle if they removed the non-recyclable label from the 100% recyclable bottle.
