---
title: "Feedback on paper sizes"
date: "2023-03-06T08:36:31+11:00"
abstract: "Readers from the UK chime in about C5 envelopes, and Screenbeard suggestes why I might prefer the aspect ratio of US Letter."
year: "2023"
category: Media
tag:
- feedback
- paper
- screenbeard
location: Sydney
---
Last Friday I talked about [liking the ANSI/US Letter paper size](https://rubenerd.com/i-like-the-us-letter-paper-size/), despite living in a country that uses the far more sensible A4 for documents. I also mentioned that *C Series* paper also exists.

[Gaelan Steele](https://queer.party/@Gaelan/109964587685641941) commented on my remark about *C Series* paper:

> I’ve seen it in the UK used for envelope sizes - a C5 envelope fits a sheet of A4 folded in half

[And Hugh](https://chaos.social/@crablab/109964876227413498):

> @Gaelan @rubenerd Yeah this is super common for envelopes 🙂 We also have DL which fits American 'letter' when folded 3-way

I checked my stationary drawer, and sure enough, one of the sizes was a C5. I never noticed.

[Screenbeard suggested](https://aus.social/@screenbeard/109967742305910807) why I might prefer the aspect ratio:

> Gaelan's comment without context made me seek out the inevitable blog post. I have nothing to add to the discussion of C sizes, but your mention of Letter looking "right" got me thinking and I hypothesise it's because it's closer to (although not exactly) 4:3, and as someone who has invested so much time in retro computing, that aspect ratio is seared into your mind with fond memories and through practicality. What do you reckon?

I'd never considered this, but it makes sense. I miss 4:3 monitors more broadly, and having it at about that ratio for paper might be pleasing for a similar reason.

It also might me why I love my iPad mini for reading.
