---
title: "Printers and wasted tech potential"
date: "2023-01-21T13:47:51+11:00"
abstract: "Hostile technology that doesn’t help people."
year: "2023"
category: Hardware
tag:
- ethics
- printers
location: Sydney
---
Last year I mentioned [what a waste](https://rubenerd.com/cryptocurrency-waste/) cryptocurrency and blockchain guff was. I didn't just mean in terms of electricity and silicon, but also the wasted potential among thousands of engineers who could be directing their craft to solving real problems, helping their fellow human travellers, and making the world a more beautiful place.

Cory Doctorow was on fire last August [talking about printer companies](https://doctorow.medium.com/epson-boobytrapped-its-printers-7d0ef9b894d4), and this drove the point home:

> “Innovation” has become a curseword, thanks to…innovation. Some of the world’s most imaginative, best-funded sociopaths have spent decades innovating ways to fuck you over. While the whole tech sector likes to get in on this game, no one “innovates” like inkjet printer companies.
>
> If we could harness the creative energy put into turning printer users into ink-stained wretches, we could end the world’s reliance on Russian gas in an instant:

There's so much hostile tech being built today, so much energy being spent on non-existent, dubious, or entirely manufactured problems. The potential for good is limitless, but human ingenuity, creativity, and talent are finite.

That's what really shorts my IC.

What overflows my buffer.

What snaps my standoff.

What fragments my spinning rust.

What nulls my pointer.

What drops my packets.

What jams my printer.

What writes logic errors to my immutable smart contract.

Hey! We've come full circle.
