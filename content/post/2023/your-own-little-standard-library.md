---
title: "Your own little standard library"
date: "2023-01-07T11:54:25+10:00"
abstract: "Mine is called Mugboss, for the delightful desk organiser."
year: "2023"
category: Software
tag:
- personal
- scripts
location: Sydney
---
I'm sure every technical person and power user has a folder of scripts they use to automate repetitive tasks, or make their lives easier in specific use cases. I know this isn't strictly analogous to a "standard library" in a programming language or development environment, but I like to think of it as one we carry around for our lives. Why reinvent the wheel when our personal standard library has a function for us already?

My folder of Perl, Bourne shell, and tcsh scripts is named *Mugboss*, for the delightful little desk organiser I've had for years. It sits in my repos folder and is now version controlled with Git:

    ~/repos/mubgoss/

I refer to the folder in my oksh(1) `.kshrc` dotfile, so I can call up whichever script when needed:

    [ -d ~/repos/mugboss ] && export PATH ~/repos/mugboss/:$PATH

These really did start as rudimentary scripts. Some even got a promotion from one-liner functions or aliases in my dotfiles in the hopes of one day extending them. But they all have a rough structure:

1. A shebang, and a few comments about the date it was created, what problem it was attempting to solve, a list of parameters, and a TODO for features.

2. A command(1) check against every external tool the script uses, to make sure it's installed.

2. A basic parameter count, which returns an error if I don't supply the correct number.

3. The actual script, written as a function for easy importing.

It's also a bit fun for some digital archaeology. I can see how my mind has changed over the years just by sorting these scripts by modification date. For all my love of Perl, I've written more shell scripts over the last few years.

