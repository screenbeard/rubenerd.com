---
title: "FreeBSD pipes and redirection, via @klarainc"
date: "2023-01-18T07:38:34+11:00"
abstract: "These were among my first lightbulb moments when learning nix back in the day."
year: "2023"
category: Software
tag:
- bsd
- design
- freebsd
- history
- linux
- netbsd
location: Sydney
---
Today's installment of *[things you already know, unless you don't](https://rubenerd.com/tag/things-you-already-know-unless-you-dont)* comes from [Klara Systems](https://twitter.com/klarainc/status/1615454941379797005)\:

> Redirection in #FreeBSD allows you to save the output from a command to a file or pass it on as the input for another command. 
> 
> Basic forms of redirection with | for pipes and < and > for I/O of files date back to very early UNIX where the concept was first pioneered.

Pipes and redirection were one of those lightbulb moments I had with \*nix, albeit on Red Hat Linux at the time. Years later I accidentally realised I could even use them on DOS, albeit in a more limited capacity.

We take a lot of tooling for granted on these systems, because their use has become second nature. It's a testament to those [forward-thinking engineers](https://thenewstack.io/pipe-how-the-system-call-that-ties-unix-together-came-about/).

*(I'm sure some shortsighted Linux developer is already looking to extend systemd with redirectd(1) that only accepts JSON or something).*
