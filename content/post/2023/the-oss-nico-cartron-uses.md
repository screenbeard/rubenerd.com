---
title: "The OSs Nico Cartron Uses"
date: "2023-02-23T09:23:14+11:00"
abstract: "Anyone who mentions an Amstrad CPC gets an automatic share."
year: "2023"
category: Software
tag:
- lists
- retrocomputing
- sailfish-os
location: Sydney
---
Last month I blogged about [which OSs I use most frequently](https://rubenerd.com/the-oss-i-use-most-often/), which may be different from ones I'd *prefer* to use. The imitable Nico Cartron [posted his own list](https://www.ncartron.org/the-oss-i-use-most-often.html), which is similar to mine.

The biggest difference? Aside from not needing to run Windows (I'm jealous), he gives a notable mention to the Amstrad CPC running C/PM, which is too cool. I came so close to buying a later model Amstrad PPC on eBay with the flippy LCD, but it went for silly money. One day I'll save up enough to have an apartment big enough for a retrocomputer room, and shelving to fit all these machines.

Nico also runs [Sailfish OS](https://sailfishos.org/). iOS is fine, but it's looking like a dead end for people who can't use OLEDs. I'm half tempted to do what Nico did and keep an iPhone for work, and an indie handset for personal stuff.

