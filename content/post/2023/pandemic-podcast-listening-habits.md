---
title: "Pandemic podcast listening habits"
date: "2023-01-02T08:12:36+11:00"
abstract: "Having a reduced commute changed a bunch of things, including how I listen to shows."
year: "2023"
category: Media
tag:
- covid-19
- lockdowns
- podcasts
location: Sydney
---
Paolo of *Vintage is the New Old* (an excellent vintage tech blog and site) posted back in February about how the pandemic and remote work had [affected his podcast listening habits](https://www.vintageisthenewold.com/covid-has-killed-my-podcast-listening-habit)\:

> It seems silly because the podcasts kept being released, but I stopped listening to them. It seems that when I am home, I don’t have the capacity to focus on anything while I am listening to something I want to pay attention to. I either miss the podcast content completely or stop paying attention to whatever I was doing. Because of my 64K, mono-task brain, I can’t afford podcasts while I am working, hanging out with my family, or anything else that I do at home!
> 
> When I was thinking about this, I pictured those scenes from the 40s when the families would seat around the radio to listen to the news or a show. With our many modern distractions, that seems to be impossible to accomplish these days.

Ditto. I started listening to podcasts [before the term existed](http://newtimeradio.com), and got up to more than a dozen on regular rotation when I commuted. Now, my twice-weekly trips are just as likely to be filled with [music](https://rubenerd.com/my-new-sony-nw-a55-walkman/ "My new Sony NW-A55 Walkman"), books, and [reading feeds](https://rubenerd.com/why-i-live-out-of-feedland-now/ "Why I live out of FeedLand now") than podcasts. Even then, I've adjusted to listening when something interests me, rather than treating them as a season of episodes I have to tune into.

There's something to be said for high production values, regular schedules, and the business models that let people pursue what they love full time. But I also think that in the race to emulate radio, we missed an opportunity to approach things a bit differently. We don't have limited spectrum here, we can produce as little or as much as we want, and people can tune in whenever.

The good news is, the tech behind podcasting (aka, RSS) enclosures) is just as suited to that as well. Like a [river-of-news](https://rubenerd.com/why-i-live-out-of-feedland-now/) for text, sometimes you just want to listen to the latest interesting thing.

