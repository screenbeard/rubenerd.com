---
title: "Pet Shop Boys, Liberation"
date: "2023-01-23T14:58:10+11:00"
abstract: "Today’s Music Monday is a bit of a nostalgia trip."
thumb: "https://rubenerd.com/files/2023/yt-AmZhKlvuepI@1x.jpg"
year: "2023"
category: Media
tag:
- music
- music-monday
- pet-shop-boys
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* is a bit of a nostalgia trip. I have distinct memories of hearing the melody of this song in a shopping centre in Australia as a kid, around the time I started school. The fact it's the Pet Shop Boys is only a bonus.

As an aside, I love the reinterpretation of the covers on their remastered albums. I'm torn whether to keep all the originals in my music library, or switch them over to these.

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=AmZhKlvuepI" title="Play Liberation (2018 Remaster)"><img src="https://rubenerd.com/files/2023/yt-AmZhKlvuepI@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-AmZhKlvuepI@1x.jpg 1x, https://rubenerd.com/files/2023/yt-AmZhKlvuepI@2x.jpg 2x" alt="Play Liberation (2018 Remaster)" style="width:500px;height:281px;" /></a></p></figure>

