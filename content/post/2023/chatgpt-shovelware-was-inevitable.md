---
title: "ChatGPT shovelware was inevitable"
date: "2023-02-23T11:49:35+11:00"
abstract: "The rise of AI writing is a continuation of a trend Dan Olson discovered last year with spam books."
thumb: "https://rubenerd.com/files/2023/yt-biYciU1uiUw@1x.jpg"
year: "2023"
category: Software
tag:
- ai
- chatbots
location: Sydney
---
Last September, Dan Olson of Folding Ideas released a documentary about a pair of grifters who underpaid gig-econonmy ghost writers to [churn out audio book spam](https://www.youtube.com/watch?v=biYciU1uiUw). The books are designed to exploit trending topics in Audible in lieu of any expertise, and achieve "passive income" with as little effort (on your part) as possible:

> The arrangement is basically guaranteed to produce boring, inaccurate junk that leans *very* heavily on endlessly rewording shallow dives into widely-available information.
>
> Readers and actual creatives—the people who actually make stuff—are just gristle to be churned through.

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=biYciU1uiUw" title="Play Contrepreneurs: The Mikkelsen Twins"><img src="https://rubenerd.com/files/2023/yt-biYciU1uiUw@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-biYciU1uiUw@1x.jpg 1x, https://rubenerd.com/files/2023/yt-biYciU1uiUw@2x.jpg 2x" alt="Play Contrepreneurs: The Mikkelsen Twins" style="width:500px;height:281px;" /></a></p></figure>

Sound familiar? Four months on, and we now have AI that can peddle this faster than even a stressed human, [as reported by Reuters](https://www.reuters.com/technology/chatgpt-launches-boom-ai-written-e-books-amazon-2023-02-21/#main-content)\:

> There were over 200 e-books in Amazon’s Kindle store as of mid-February listing ChatGPT as an author or co-author ... and the number is rising daily. There is even a new sub-genre on Amazon: Books about using ChatGPT, written entirely by ChatGPT.
>
> But due to the nature of ChatGPT and many authors' failure to disclose they have used it, it is nearly impossible to get a full accounting of how many e-books may be written by AI.

This was completely unsurprising, like being told your ghost-written audio book about curing social anxiety with electrophoresis was as engaging, plausible, and factually accurate as something written by an AI. Or me!

I'm sure this is people testing limits and weaknesses, as Bruce Schneier [details in his latest book](https://www.schneier.com/books/a-hackers-mind/) which you definitely need to read (review post pending). But for each knowledge worker using these tools for fun, research, brainstorming, or supplementing their own work, we're about to see a wave of spammers I don't think we, and our systems, are prepared for.

It all seems less dystopian than disappointing. All the promise of cool new tech, and we're generating low-entropy noise.

