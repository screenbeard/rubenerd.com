---
title: "I feel for the NetBSD community"
date: "2023-01-06T07:07:06+10:00"
abstract: "Linux people ask why you’d run BSD, and BSD people wonder why you run NetBSD."
thumb: "https://rubenerd.com/files/uploads/anime.netbsd.mai.colour.420.jpg"
year: "2023"
category: Software
tag:
- bsd
- netbsd
location: Sydney
---
We share a common struggle within the BSD community, and more broadly among Unix-like OSs that aren't Linux. Think Minix, illumos, heck maybe even big iron UNIX. Linux is now perceived as the default, meaning anyone presenting an alternative has to justify their existence *on top of* its features and capabilities. Linux people with longer memories probably can recall what that used to be like.

That assumption might be a feature or a bug, depending on your perspective. It also doesn't have to be this way; with a bit of care you can write portable code and platforms.

<img src="https://rubenerd.com/files/2023/NetBSD.png" alt="The NetBSD logo" style="width:100px; float:right; margin:0 0 1em 2em" />

I mention it in passing though because I see similar sentiment within the BSD community. FreeBSD and OpenBSD get the lion's share of commentary, which isn't surprising given their install bases. But mention [NetBSD](https://www.netbsd.org/), and the same questions Linux people ask of BSD come up:

* Why do you run it?
* Why not `$OtherBSD`?
* What's the point?

Some of this comes from genuine curiosity, and I think there's just as much an opportunity to showcase that OS as all those "should I try FreeBSD" posts that irritate a vocal minority in the community. But I can see how this would grate after a while.

NetBSD was my first BSD operating system, and while I've since moved to FreeBSD for much of my work and personal use, I keep a few NetBSD machines around as well. It works *exceedingly* well, especially on older hardware. And even if I'm not running the OS, I'm probably using pkgsrc. It's hard to quantify, but whenever I try something new on NetBSD, I can grok the defaults and syntax, to the point where educated guesses work most of the time before even touching their excellent docs. That hasn't been true, especially on Linux, in a long time.

These routine questions tell me I need to write about NetBSD more too.
