---
title: "Zoe Williams on cryptocurrency and markets"
date: "2023-01-25T09:43:06+11:00"
abstract: "“Markets have never been free: they are social spaces.”"
year: "2023"
category: Thoughts
tag:
- blockchain
- cryptocurrency
- economics
- news
- scams
location: Sydney
---
I loved this paragraph in [her recent article about FTX](https://www.theguardian.com/commentisfree/2023/jan/17/whats-the-true-value-of-crypto-it-lays-bare-the-lies-of-libertarians-ftx), the ill-fated, fraudulent cryptocurrency exchange:

> Markets have never been free: they are social spaces and, as such, have always been governed by rules, which – since the first time a snake-eyed trader tried to cut flour with chalk – work because they are formally determined. Take away those rules and soon a greedy, clever person might take advantage. 

This will continue to happen as long as a quick buck can be fleeced from marks. Or Jeffs, or whatver their names are.
