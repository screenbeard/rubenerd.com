---
title: "Exciting new look, same great taste!"
date: "2023-03-12T14:57:53+11:00"
abstract: "I can’t say I’ve ever been excited for a new jar label."
year: "2023"
category: Media
tag:
- advertising
- pointless
location: Sydney
---
In the space of a month, our peanut butter, oolong tea, and tartare sauce have advertised "exciting" new rebrands, but promise that they'll have the same "great taste"!

I can't speak for you, but I'm not "excited" by food rebrands. I don't think I've ever woken up in the morning and exclaimed "fuck yes, my tea comes in a red box now!" Maybe that says more about my imagination than a jar of condiments. Perhaps I *should* be excited by a tin of peaches with floral accents. It's certainly more fiscally prudent (prunes?) than being excited by a new piece of Hi-Fi gear, or a holiday, or a personal hot air balloon fitted with one of those outdoor coffee carts.

The "same great taste" angle is also... odd to emphasise. Someone bursting into a room shouting "I didn't do it!" should immediately be regarded with suspicion. Like someone trying to hide shrinkflation with new packaging, for example. "Same great taste" also implies past rebrands came with a disgusting or unpalatable new flavour. "New look, and we've partnered with Oreo to...!"

This is probably why I'm not in comestible marketing. Maybe next time I change this blog theme, I should reassure people it'll have the same great posts, this one notwithstanding.
