---
title: "New design for old chocolate"
date: "2023-03-07T08:43:55+11:00"
abstract: "It’s being made overseas. But that didn’t stop their PR team giving another vague, nonsensical reason!"
year: "2023"
category: Media
tag:
- advertising
- food
- logos
location: Sydney
---
[Philop Oltermann quoted](https://www.theguardian.com/world/2023/mar/05/matterhorn-mountain-toblerone-packaging-design-switzerland "Matterhorn no more: Toblerone to change design under Swissness rules") a spokesperson from Toblerone's parent company:

> “The packaging redesign introduces a modernised and streamlined mountain logo that aligns with the geometric and triangular aesthetic”

The real reason? Laws that prevent Swiss iconography being used on products made outside the country, like those knock-off bags with Swiss flag badges you see in dollar stores. For one thing, they should be Franc stores, *amirite!?*

Today I also learned that [Liechtenstein has its own Franc](https://en.wikipedia.org/wiki/Liechtenstein_franc "Wikipedia article on the Liechtenstein Franc") it mints occasionally, though no such flavouring exists in Toblerone. Or [franks](https://en.wikipedia.org/wiki/Frankfurter_Sausage "Wikipedia article on frankfurter sausage"), of which I'm far more relieved. To be frank. Milking these jokes for all their worth. Worth... würst... I've got nothing. Nuts.

See also: is a phrase with two words. Also: [reblanding](https://justcreative.com/why-do-everyones-logo-fonts-look-the-same/).
