---
title: "Rediscovering books and libraries"
date: "2023-03-09T08:46:57+10:00"
abstract: "They exist, and they’re glorious!"
year: "2023"
category: Media
tag:
- books
- reading
location: Sydney
---
I've written a few times about my justification for buying and reading ebooks, but it boils down to these points:

* We live in a tiny apartment, but I still want to support the authors I read.

* I've come to *prefer* reading on my iPad Mini under many circumstances. The built-in backlight is easier to use than a booklight, I can dial font sizes to fit more on a page, and I find it easier holding and tapping a screen than turning pages, especially on crowded trains where I have only one hand free.

* I can easily copy quotes out for blogging or sharing with friends.

* It fulfils my childhood fantasy of carrying around a "portable library", even on holidays or while commuting.

But even my technocratic little mind knows these don't hold a candle to the other joys books bring. It's special knowing the book I'm holding was read by my late mum, or a complete stranger who's interests aligned with mine in that one specific circumstance. Reading is a personal activity, yet something we enjoy collectively.

Ergonomics are also a mixed bag. It's easier to dog-ear, bookmark, and highlight pages than it is on a screen, even with all our high-resolution, haptic inputs. And while an ebook reader remembers where we left off, its not as easy as flipping back through the pages to find something.

At a fundamental level, is a phrase with four words. There's something nostalgic and meaningful in the physical object, not just the text contained therein. There's a reason archaeologists, archivists, and historians hold onto old books, even if we have full transcriptions of their contents. And being truthful, I'd probably move back to them if I had a large room with shelves. Clara feels the same way about her manga.

That covers (hah!) the books themselves, but [Icona nailed](https://twitter.com/iconawrites/status/1466546612382146560) another part of the experience compared to ebooks last year:

> Internet shopping will never replace the feeling of walking into an old bookshop and browsing around until your gaze falls on a great book you were not looking for.

Clara and I have also learned recently that one can achieve this by going to... drumroll please... **libraries**! We live down the road from one of the largest ones in Sydney, yet we scarcely acknowledged its existence since we graduated from university.

You know what I'm reading right now? A borrowed copy of *Catch 22*. It's been on my mental `#TODO` list for years, but it took walking past a *featured books* shelf to remember it, borrow it, sit on the couch, and begin reading the damned thing.

Books are great! As is finding them! Sometimes you need to explore, and to stop rationalising.
