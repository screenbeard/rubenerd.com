---
title: "King CallMeKevin is a military genius"
date: "2023-01-19T13:30:51+11:00"
abstract: "If they’re stationary, we should be fine!"
thumb: "https://rubenerd.com/files/2023/callmekevin-surrounded@1x.jpg"
year: "2023"
category: Media
tag:
- callmekevin
- games
- quotes
- pointless
- youtube
location: Sydney
---
From one of his [most recent streams](https://www.youtube.com/watch?v=4JoHTjKKb24)\:

<figure><p><img src="https://rubenerd.com/files/2023/callmekevin-surrounded@1x.jpg" alt="Radovians have formed stationary posts around our castle, Your Grace." style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2023/callmekevin-surrounded@2x.jpg 2x" /><br /><img src="https://rubenerd.com/files/2023/callmekevin-shouldbefine@1x.jpg" alt="Kevin: Well if they're stationary, we should be fine!" style="width:500px; height:281px;" srcset="https://rubenerd.com/files/2023/callmekevin-shouldbefine@2x.jpg 2x" /></p></figure>
