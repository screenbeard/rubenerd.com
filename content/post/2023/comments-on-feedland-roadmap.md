---
title: "Comments on FeedLand roadmap"
date: "2023-01-16T17:33:13+11:00"
abstract: "I’d love to host a server."
year: "2023"
category: Internet
tag:
- dave-winer
- opml
- rss
- software
location: Sydney
---
Dave has [announced some changes](https://github.com/scripting/feedlandSupport/issues/166) coming to FeedLand, and is accepting comments. I feel like it's fitting to blog about them.

I've love to host a server. I'm not a JS developer, but I'd also happily contribute docs or finances if it'd help.

I see it as natural a thing to do as [hosting WordPress](https://rubenerd.com/dave-asks-about-wordpress-block-editor/), [MediaWiki](https://sasara.moe/wiki/Sasara.moe), and the static-generated site you're reading now. It achieves a few things:

* I like the security and control of my data that comes from self-hosting things. I trust Dave, but it always feels good being able to see the tables and code for yourself.

* There's the potential for integration with other stuff you host.

* It reduces the load on Dave's servers, as little as it may be.

I'll be interested to see what the technical requirements are. I briefly ran a Node.js and MariaDB stack on FreeBSD (my preferred server OS) for Ghost, so I'd expect the toolchain to be similar.
