---
title: "W-whoops, buying a Commodore VC-20"
date: "2023-02-11T19:04:46+11:00"
abstract: "The latest addition to my burgeoning 8-bit computer family!"
thumb: "https://rubenerd.com/files/2023/commodore-vc20-top@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-vc-20
- commodore-vic-20
location: Sydney 
---
I talked about [stuff I wanted to build](https://rubenerd.com/things-id-love-to-build-this-year/) last month, which included a modern take on the Commodore VIC-20. I since learned that while we can [recreate a Commodore 64](thoughts-on-an-entirely-new-commodore-64/), the VIC chip has no modern analogue. Ah, that was an excellent pun. I couldn't ethically bring myself to deprive someone's beloved childhood machine of a working replacement VIC chip, so I shelved the idea.

As though someone was listening, a gentleman in the UK was selling their 1983 Commodore VC-20 with the *exact* technical specifications I was after! And thanks to being halfway through a modern build myself, I had replacement parts for everything listed as broken.

Here she is, in all her forty-year old glory!

<figure><p><img src="https://rubenerd.com/files/2023/commodore-vc20-top@1x.jpg" alt="Photo of the VC-20 on our coffee table, showing its black and brown keyboard, and yellowed case with blotchy white patches." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/commodore-vc20-top@2x.jpg 2x" /></p></figure>

The VC-20 was the West German version of the VIC-20, owing to the amusing meaning of the latter in German. Unlike the Japanese VIC-1001 that included Hiragana in lieu of PETSCII graphics, the badges are all that distinguish this machine from other VIC-20s. I'm fascinated by rebrands; check out my [IBM WorkPad PDA post](https://rubenerd.com/my-new-ibm-workpad-pda/) for another example. I'm thrilled that I have another curiosity like this.

This particular VC-20 is the cost-reduced (CR) model Commodore sold alongside the newer C64 for a period in the early 1980s. It takes many design cues from the C64, including its keyboard font and colour scheme, updated badge with the gorgeous rainbow motif, lower case profile, and power connector. It otherwise retains the original VIC-20 case colour and ports.

The case exhibits noticeable yellowing, with blotchy marks around the vents and edges. This extends well into the case where sunlight wouldn't have been able to permeate, suggesting heat may have played a role in the discolouration. Otherwise, the plastic is in excellent physical condition, and I have a [retrobright process](https://rubenerd.com/retrobrighting-a-commodore-64c/) that works well that I'll be trying.

<figure><p><img src="https://rubenerd.com/files/2023/commodore-vc20-board@1x.jpg" alt="Photo inside the VC-20, showing the Revision B shortboard taking up about two thirds of the case." style="width:500px; height:333px;" srcset="https://rubenerd.com/files/2023/commodore-vc20-board@2x.jpg 2x" /></p></figure>

Taking a look inside, the empty space within the case shows it has the <span style="text-decoration:line-through">Revision B</span> board (I've since learned its a Revision D, I'll discuss in a follow-up). It has fewer chips (especially the SRAM in the lower-left), runs much cooler, and has compatibility with C64 power supplies. My aim is to eventually have one beefy C128 power supply permanently on my desk, which I can use on every machine with adaptors.

It has a [fascinating hodgepodge of MOS ICs](https://sasara.moe/wiki/Commodore_computers#IC_manifest). Almost all the chips have 1983 date codes, though the BASIC ROM is from 1981, and the CIA chips are from 1982 and 1986. The character ROM has several large scratches where a chip puller may have contacted with the plastic, suggesting it was replaced. The large ICs are also in high quality round sockets, not the cheap single-wipe units Commodore were infamous for shipping. Mounting holes around the VIC circuitry also have traces of solder, which suggests the shielding was removed. This machine has definitely been fixed before, which is heartening to see.

The biggest functional issue the seller warned about was a wonky keyboard, with keys that either stick or don't register. He suggested an error with the keyboard, but the mismatched and socketed CIA chips suggest to me that it might be a controller issue. I have a spare keyboard I intended to use in my scrapped modern VIC-20 build that I'll try, otherwise replacement CIA chips are easy to find.

In the next post, I'll talk about my teardown, cleaning, and rebuild :).
