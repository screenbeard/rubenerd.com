---
title: "Upgrading to macOS Ventura was a mistake"
date: "2023-01-19T10:51:33+11:00"
abstract: "Most modern Apple software is flaky, but this has been especially fun."
year: "2023"
category: Software
tag:
- apple
- errors
- macos
location: Sydney
---
Marcel Weiher had [such a great summary of Apple's software](https://blog.metaobject.com/2023/01/setting-bozo-bit-on-apple.html) of late:

> In other words, I had set the Bozo Bit on Apple. By default, when Apple does something new these days, I fully and quietly expect it to be broken. And I am surprised when they actually get something right, like Apple Silicon. And it wasn't an angry reaction to anything, in fact, it wasn't even much of conscious decision, more a gradual erosion of expectations.

macOS Ventura is the perfect example of this. [Michael Tsai has an exhaustive list](https://mjtsai.com/blog/2022/12/27/ventura-issues/) of broken things, but these are some that continue to make my life frustrating:

* **Music.app still sucks!** It's as buggy, feature incomplete, and unintuitive as it was when it first replaced iTunes. It's almost hostile to the idea that people would want local music libraries. I rsync music to a Walkman now, so I'm looking at decoupling my twenty-year old iTunes library over to something else. End of an era; not with a bang, but with a whimper.

* **System Settings is inscrutable**. I thought I'd get used to it, but it's still opaque and confusing a few months in. The categories on the side make no sense, controls are inconsistent with the rest of the system, and somehow they made an infinitely-scrollable window feel *more* cluttered than the previous System Preferences. It's bad enough that Windows 11 Settings is better, and that's saying something!

* **The bin is broken.** It never finishes emptying, so I have to use the Terminal. It's a common problem, but don't bother looking for a fix or you'll be hit with a wall of adware-laden crap sites that say *hurr durr permissions*. **SON OF A DIDDLY.**

* **Tethering has stopped working reliably.** It was fine before, now it requires enabling/disabling Wi-Fi a few times to work. This *could* be hardware, but the timing is awfully coincidental.

These show a lack of polish, but aren't show-stoppers:

* **Unmounted shares still show as mounted.** Doesn't matter what the underlying protocol was. Overstaying one's welcome is always intrusive.

* **Encrypted drives ask for passphrases, even if they're in the keychain**. I'm so relieved I don't rely on any Mac for archiving and bulk storage.

* **The Finder routinely can't gain focus.** Use Spotlight, ALT+TAB, or click the Finder icon in the Dock, and none of the windows surface. Not all the time, but often enough that I was beginning to question my perception of reality.

And some obligatory inb4:

* **Just use another OS/I don't have these problems.** The [FAQ addresses this](https://rubenerd.com/faqs/#comments).

* **Where can I buy chinos like yours?** Uniqlo. Or if you abbreviated *cappuccinos*, I adore [Apothecary Coffee](https://www.apothecarycoffee.com.au/) in Sydney. Don't spill cappuccinos on your chinos though, unless your next trip is to a laundry of some description.
