---
title: "Feedback about the “friend zone” post"
date: "2023-03-18T08:07:38+11:00"
abstract: "You deserve to give yourself a chance."
year: "2023"
category: Thoughts
tag:
- feedback
- relationships
location: Sydney
---
A gentleman emailed me last month (sorry, it's a big backlog!) regarding my [friend zone](https://rubenerd.com/the-independent-singapores-friend-zone-article/) post. If you didn't see it, I read an article republished on a Singapore news site offering advice for people stuck in the aforementioned zone. Instead, it offered a lesson in what *not* to do!

I didn't get permission to repost what he sent, and it was written in good faith, so I'll be mostly paraphrasing. In short, *is a phrase with two words!* And people say I'm too serious around here.

The crux of his email was that he was in college, had a crush on someone in their dorm, and that my comment that *love isn't a transaction* was a "splash of cold water [they] needed". He took exception to my characterisation in a few places, but otherwise agreed that trading favours for feelings wasn't a tenable strategy.

I take the L regarding tone, and apologise if I added to anyone's stress. My point in the post wasn't to denigrate people who think like this, but to offer them an alternative way of approaching relationships.

I empathise that the deck can seem stacked against shy, nervous, or quiet people with interests that deviate from the mainstream, regardless of our gender or orientation! But you deserve a fighting chance, and resenting someone for not having affections "despite you being nice" isn't a winning strategy. You should be a good person because you're a *good person*.

I had my first girlfriend at 27, and suffered most of my life from social anxiety *and* introversion (they're different things). Suffice to say, I know of what I speak!
