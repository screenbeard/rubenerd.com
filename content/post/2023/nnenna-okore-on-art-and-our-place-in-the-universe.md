---
title: "Nnenna Okore on art, and our place in the universe"
date: "2023-02-12T12:17:18+11:00"
abstract: "“The natural environment is a very fragile part of our existence”"
year: "2023"
category: Thoughts
tag:
- art
- environment
location: Sydney
---
I've spent the morning reading [this interview with Nnenna Okore](https://theartmomentum.com/nnenna-okore/
), the Australian-born Nigerian artist who works in the United States. 

This is how she introduced her body of work:

> It is our responsibility to treat and handle nature delicately, so that it can be sustained. A lot of my works are concerned with the material world and the fragility of mother nature.

And how she explained *Everything Good Shall Come to Pass*:

> &hellip; I am thinking of all the things that exist on the planet and how they will ‘fritter away’ and die someday. The natural environment is a very fragile part of our existence. It is a reminder, if we think about it, that we are only here for so long; all matter, all things, and our own existence will come to pass. Life is transient, so we need to make sure that we are watching out for our universe. This work embodies the hope that things will get better, especially in a country like Nigeria, where hardships and strife deeply affect a good portion of the masses. I am optimistic that things will get better.

I reorganised photos of her pieces on Wikimedia Commons [into a new category](https://commons.wikimedia.org/wiki/Category:Nnenna_Okore), which you can check out if you're interested.
