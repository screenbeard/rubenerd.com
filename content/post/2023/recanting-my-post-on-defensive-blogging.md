---
title: "Recanting my post on defensive blogging"
date: "2023-01-24T13:48:57+11:00"
abstract: "People acting in bad faith will always misinterpret what you say."
year: "2023"
category: Internet
tag:
- blogging
- comments
- feedback
location: Sydney
---
Back in 2021 I talked about what I called *[defensive blogging](https://rubenerd.com/defensive-blogging/)*. I'd been telling everyone to blog, and I felt it was my responsibility to warn people about the potential pitfalls.

My solutions were to raise the barrier to entry for accepting comments, and to tweak your writing style to head off bad comments from the start:

> I have had to modify how I write a bit too. Pointless references and silliness are natural ways to disarm people, but I also review what I write from the perspective of someone who’s reading in bad faith. Even if people in a real world conversation would understand what you’re saying in context, there are those who will only serve to nitpick what you say. Sometimes that takes an overt “inb4” at the end of a post, other times I make light of the situation or idea myself in advance. I appreciate people who don’t take themselves too seriously, and try to do it too.

The operative words are *bad faith*. People who write responses like this aren't swayed by disclaimers or footnotes, just as they weren't capable of thinking about you, your circumstances, and your constraints when reading your post in the first place!

I realise now my issue isn't that I get email like that, it's that I let them waste my time. What they need is a swift redirection to the nearest refuse receptacle, so you can go back to fun and productive things.
