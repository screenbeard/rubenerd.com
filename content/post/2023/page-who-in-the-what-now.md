---
title: "Page who in the what now?"
date: "2023-01-21T20:14:42+11:00"
abstract: "17801-21050 See all 76 results"
year: "2023"
category: Software
tag:
- design
- shopping
location: Sydney
---
I was browsing a famous online retailer, like a gentleman, and saw this below the page of results:

> 17801-21050 See all 76 results

76 total results, spread over 21,050 pages, of which I'm browsing page 17,801. That'd be a new individual item every 277 pages, assuming no duplication.

Did one of you slip something in my coffee... again? [James](https://jamesg.blog/)?
