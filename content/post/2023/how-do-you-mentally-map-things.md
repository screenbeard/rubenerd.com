---
title: "How do you mentally map things?"
date: "2023-02-03T13:46:17+11:00"
abstract: "Comparing mind maps, index cards, outlines, and the tables I seem to think in."
year: "2023"
category: Thoughts
tag:
- psychology
- work
location: Sydney
---
I love reading about people's thought processes when it comes to capturing and processing information, because it shows just how differently we're all wired.

A few examples:

* My late mum was big into mind maps. You start with a general idea, think of related concepts, and connect them with lines. Doing this can often surface other details, and can express relationships that would tedious or repetitive to in text.

* Merlin Mann of [43Folders](https://www.43folders.com/) and [Back to Work](https://www.backtowork.limo/) fame invented the Hipster PDA, and got me into tools like index cards and [nvALT](https://brettterpstra.com/projects/nvalt/). Storing information in a flat structure makes capturing and searching for things easier, because you spend time on the content and not the supporting structure.

* Dave Winer regularly discusses [outlines](http://outliners.com/default.html), which are tree structures that can be used to drill down into topics. A proper outlining tool can be used to collapse/expand sections as needed, and rearrange items or entire branches as your ideas evolve and grow over time. It's structured but flexible.

I've tried all manner of tools over the years, and I've come to the realisation that I always think of things in two axis, either in a list with one or more attributes, or two sets of attributes and their intersections. The former lends itself well to tracking things, and the latter expresses relationships so elegantly.

After years of reading that you shouldn't use a spreadsheet as a database, I absolutely use a LibreOffice Calc workbook as a personal database for everything. At its most basic I freeze a row of attributes, set an AutoFilter, and fill it out. I can add, delete, sort, collapse, filter, and move anything I need to, and expand to a new sheet if things need further clarification. It's fast, visual, and organised.

Spreadsheets are also more flexible and forgiving than a proper database when I need to break my own rules, or make quick changes to the way I structure or model something. An idea-capturing system shouldn't introduce friction, or you won't use it.

Thinking of things like this is also probably why I love using Perl arrays and hashes so much. As I've written here many times, they map so well to how my mind works compared to other languages, in a way I can't describe. I don't think it's a coincidence that Larry Wall majored in natural and artificial languages.

Embracing this idea, rather than fighting it, has been *huge*.

It's one of the more frustrating things about smartphones for me; there simply isn't a good mobile spreadsheet application that I can use to map out an idea on the go. Believe me, I've tried them all. Even within the limited horizontal space of a portrait display, I'd think you could still be clever and do something with it.

