---
title: "What happened to data sims for tablets?"
date: "2023-01-09T08:46:05+11:00"
abstract: "Surely affordable ones existed before?"
year: "2023"
category: Hardware
tag:
- australia
- ipad
- ipad-mini
- telcos
location: Sydney
---
Last year I [bought a iPad Mini 6](https://rubenerd.com/farewell-to-my-kindle-paperwhite/) to read manga, light novels, books, newspapers, and RSS at coffee shops and the sofa. I find iOS frustrating for productive work, but it's fine in this role.

*(Transferring books to it has become more frustrating since it decided not to mount or appear on macOS anymore, meaning tedious use of a third party server to upload/download from. But that's for another post).*

I opted for the version with mobile support, but ended up tethering off my phone instead. Weirdly though, my iPhone SE 3 has been much flakier than my iPhone 8 in this regard, with multiple attempts to tether often going nowhere. It's the same experience with the MacBook Air, and my FreeBSD Panasonic Let's Note.

I assumed I could go to our Australian telco and get an extra SIM to share Clara's and my shared mobile plan, or get a separate data SIM. The former was way too expensive to justify for a bit of convenience, and the latter doesn't exist anymore, unless you want to go the pre-paid route.

Australian telcos have always sucked, especially after coming back from Singapore. But was the situation always like this? I swear I could get an affordable data SIM a few years ago.

I miss services like AvantGo that would let me download news and books automatically before heading out. An iPad-sized Tungsten running classic PalmOS and AvantGo would be wonderful.
