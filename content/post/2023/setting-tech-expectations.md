---
title: "Setting tech expectations"
date: "2023-01-31T08:48:43+11:00"
abstract: "I expect that nothing involving printers and Bluetooth will ever work."
year: "2023"
category: Hardware
tag:
- bluetooth
- printers
- psychology
location: Sydney
---
It sounds obvious when spelled out like this, but one of the most valuable lessons I've ever learned was that our expectations can so often colour our views of a situation. It's not universally applicable: you can't force an expectation of feeling great if you haven't slept for three days. But having reasonable expectations, especially for tech, can help you cope with issues.

I have three examples that happened last night.

* Our home printer was needed to print (no, really!?) an invoice. Instead of doing that, it decided to chew through our entire paper supply printing garbage while I wasn't looking, then complained it was out of toner and paper.

* I wanted to use Bluetooth on my Mac to... I'll stop there, because we all know what happens.

* I needed to have macOS Ventura detect an SD card. When it didn't, I connected it to my FreeBSD tower and copied off photos instead.

I expect that nothing involving printers and Bluetooth will ever work, as well as specific functions in the most recent macOSs. I'll still try them on the off chance they delight me with a deviation from their expected behaviour, but I'll shrug my shoulders if my expectations are met. This reframes the outcome as a pleasant surprise, rather than disappointment.

I'll admit there's a bit of a defeatist streak of pessimism in this attitude, which also sounds like a great name for an emo band. But for certain tech, I think its the only healthy view one can take, especially if preserving your mental health is a concern. I might only be half joking.
