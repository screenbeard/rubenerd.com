---
title: "Dismissing criticism with workarounds"
date: "2023-01-08T10:10:47+11:00"
abstract: "Uhhhhhhh, you do know you that can disable it by opening the door marked BEWARE OF THE LEOPARD right?"
year: "2023"
category: Internet
tag:
- comments
- social-media
location: Sydney
---
With apologies to Douglas Adams:

> **OP:** This new feature is so irritating! Every time I press return, a glove slaps me in the face. WHY!?
> 
> **Reply Guy:** Errrr/uhhhh/ummmm, [you do know](https://www.urbandictionary.com/define.php?term=You+Do+Know) that you that can disable it by going to the planning office with a map and a flashlight, and opening the door marked "BEWARE OF THE LEOPARD"?
>
> **OP:** Thanks, but even if that always worked, I'm asking why it's the default. My face hurts.
>
> **Reply Guy:** You can [just](https://www.excitant.co.uk/just-is-a-dangerous-word/) disable it! I don't understand.
>
> **OP:** I think you do.

