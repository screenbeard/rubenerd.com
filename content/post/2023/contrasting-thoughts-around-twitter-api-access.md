---
title: "Contrasting thoughts around Twitter API access"
date: "2023-01-22T09:46:58+11:00"
abstract: "There’s whether they legally can, and whether it’s a dick move."
year: "2023"
category: Software
tag:
- twitter
location: Sydney
---
The recent Twitter API access issues are an illustrative case study in how people discuss issues online. One camp is horrified that longstanding third-party applications are being denied access. The other group says they're entirely in their right to do so.

They're talking past each other. One perspective doesn't negate the other. And the problem is, when confusion arises from this, it's human nature to dig a trench and shout louder. I'm as guilty of this as anyone, but I'm trying to be more observant of it.

Back to Twitter though: both parties are right:

* Everyone attached to the Twitter ecosystems was there at the indulgence of Twitter. The goalposts around what they accept and deny may be shifting faster under Muskian ownership, but they own the platform and can (mostly) do what they want. This is an American legal question; a prey of jurisdictions globally infamous for generally siding with the big guy.

* The other side is also right to feel scorned. Their applications ensured the success of the then-nascent Twitter site, and now they're being let go under questionable pretences. Just because someone can legally do something doesn't make it good, or ethical, or just. Conversely, something isn't morally defensible or appropriate just because it's legal... to say nothing of the significant power imbalance at play.

I feel bad for these software developers. I used the betas of their software back when they first launched in 2007, and the number of users on the platform was still measured in the thousands. But it also serves as a cautionary tale not to hitch your horse to one company that isn't beholden to humanly concerns, like ethical conduct. They, and their defenders, don't distinguish could from should, and think that it's reasonable and ethical to exercise any power you have.

I hope the Fediverse can avoid making the same mistakes, and offer a welcoming community for these developers. Their contributions make the ecosystem viable, and we forget at our peril.
