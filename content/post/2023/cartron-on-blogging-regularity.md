---
title: "The @cartron on blogging regularity"
date: "2023-01-14T09:32:40+11:00"
abstract: "Responding to his lovely comment on The Bird Site."
year: "2023"
category: Thoughts
tag:
- feedback
- personal
- writing
location: Sydney
---
[Nico commented via the Bird Site](https://twitter.com/cartron/status/1613821115457064961)\:

> I am still amazed that you manage to write almost daily 2 blog articles - I like the short ones like this one, I should probably do some of them as well from time to time - this one made me smile :-)

Thanks! Shorter/sillier posts have [got me into trouble](https://rubenerd.com/why-arent-you-more-serious/) before, but that only makes me want to do them more.

I love that we're the master of our own domain with blogging. If you want to post once every year, or a few smaller posts a day, or take a break for a few months, you absolutely can. You can write without titles, have a complicated or simple site design, include inline images or only post text, whatever you want. There aren't any rules, beyond writing syntax a browser and RSS aggregator can interpret.

[Nico's blog](https://www.ncartron.org/) is great too; he's tempted me with the siren song of an [even simpler blogging system](https://www.ncartron.org/failed-attempt-at-migrating-to-hugo.html) recently. My only qualm is his avatar always makes me want to get French ice cream, because nobody does it better! 🇫🇷
