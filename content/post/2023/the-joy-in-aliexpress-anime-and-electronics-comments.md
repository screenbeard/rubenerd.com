---
title: "AliExpress anime and electronics comments"
date: "2023-03-01T15:58:26+11:00"
abstract: "Missing head. Four stars."
year: "2023"
category: Hardware
tag:
- comments
- silly
- shopping
location: Sydney
---
AliExpress can useful for buying certain components I can't get from anywhere else, but the comments are the real gems. You'll see people complaining that heatsinks get too hot, that a function generator couldn't "detect" anything, and that a MiniPRO IC tester didn't come with any EPROMs.

I suspect if the site sold electric cars, people would complain that all the petrol spilled out when they tried to pour it into the charging port, or that a DIMM stuck to the side of their phone didn't offer any additional capacity. I suppose you self-select for such people when you compete so aggressively on price over anything else.

But this might be my new favourite. Under what was clearly a knock-off anime figure from either Kaguya-sama or Fate/Grand Order, I can't remember:

> Missing head. ★★★★☆

I don't know about you, but I'd rate something *significantly* lower if I left without my head, let alone one missing from a statue. It's not my most attractive feature, but I'm rather attached to it.

