---
title: "Escaping heat, and the luxury of Ceebs Money"
date: "2023-01-31T14:39:53+11:00"
abstract: "Entire classes of problems go away if you have it."
year: "2023"
category: Thoughts
tag:
- finances
- personal
- social-security
- weather
location: Sydney
---
For those outside the glorious (asterisk) Commonwealth, *[ceebs](https://en.wiktionary.org/wiki/ceebs)* is a plural evolution of the abbreviation *CBF*, or *couldn't be fsck'd*. It describes a decision devoid of enthusiasm, or made in the interest of getting something solved quickly without worrying about sweating the details. That's foreshadowing!

Having *Ceebs Money*, as I call it, can make all the difference in the world. If something comes out of the blue to make your life difficult or unpleasant, you can throw a bit of money at it in a way those struggling either don't have the capacity to do, or can't justify (it's one of the many reasons I'm for unconditional social security).

Case in point, Sydney has been going through a mini heatwave over the last week, with humidity levels more oppressive than I even felt like I experienced in Southeast Asia most of the year. Which, if you're a believer in Mr Murphy and his eponymous law, lead to our faltering air conditioning unit finally kicking the proverbial bucket. 

Our tiny apartment is situated just before a [large setback](https://en.wikipedia.org/wiki/Setback_(architecture)), meaning we get sun beating down from the roof as well as the windows. Without any mechanical ventilation and the still, humid air outside, our bedroom reached 37 degrees (99 F) and 90% humidity over Saturday night. It would have been hard enough to sleep had it not also been for a nausea migraine, and my incessant whinging as we lay in pools of sweat.

Enter *Ceebs Money!* A quick web search, and we'd booked a hotel down the street with a functional air conditioner and curtains I could use to block out sunlight and recover. When you're vomiting with vertigo and migraine head pain, switching to a quiet, dark, cool room made me feel euphoria I can't begin to articulate!

As I said above, not everyone has access to *Ceebs Money*. Everyone should. There should even be a tax category for it.
