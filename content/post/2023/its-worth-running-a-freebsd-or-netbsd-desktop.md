---
title: "It’s worth running a FreeBSD or NetBSD desktop"
date: "2023-02-07T07:29:32+11:00"
abstract: "Not detailing how to do it, but justifying why you'd want to."
year: "2023"
category: Software
tag:
- bsd
- feedback
- freebsd
- netbsd
location: Sydney
---
Every few months, someone asks me not how to install a FreeBSD or NetBSD desktop, but whether it's worth doing. Michał Sapka emailed with this most recent comment:

> Third: could I ask for some “FreeBSD as daily driver” article? You, a co-worker and Vermaden made me *very* bsd-curious but there’s a lot people in the webs saying that bsd on desktop is a bad idea. That it’s a server-only use case.

I'd say it's worth it! But it also depends on what you want to get out of it.

A desktop is a great introduction if you've never used BSD before. It teaches you about package management, how the base system is structured, building your first OpenZFS pool, how to perform updates, and in the case of FreeBSD, how to run tools like jails for process isolation, and bhyve for virtual machines. It's these use cases that happen to make the FreeBSD desktop *extremely* compelling for me. While understood to be "server" technologies, they also make upgrading foolproof, and the testing new software and architectures a cinch.

On the NetBSD side, I've seen the OS bring old hardware back to life, and make budget hardware *scream*. My Pentium III tower I built as a Windows 98 game machine is dual booted with the latest i386 release of NetBSD (because why not), and my FreeBSD Japanese Panasonic Let's Note is currently booting NetBSD too for some experiments. It's as though both machines have had free hardware upgrades, the difference is so stark.

Addressing the proverbial room elephant however, running a BSD desktop does come with its own challenges. FreeBSD is fortunate enough to have Nvidia binary drivers, but Wi-Fi and open-source AMD drivers tend to lag behind. NetBSD also gets even less attention from large vendors than the other BSDs, though in my experience, it sometimes edges out FreeBSD in laptop support. Most of your favourite Linux desktop software is probably available in FreeBSD ports or NetBSD pkgsrc, and those that aren't can probably be cross compiled. But while FreeBSD's [Linuxulator](https://wiki.freebsd.org/Linuxulator) can run some Linux-only binaries, there are some things you won't be able to run.

If you're familiar with installing Linux, I'd put the experience somewhere between a desktop-focused distribution like Fedora, and a LEGO set like Arch. You'll end up at a shell upon first boot, but you can speed run the installers and have a desktop from packages installed with only a few commands. (In fact, the base systems are literally tarballs, which comes with a whole set of other benefits that are beyond the scope of this post, but are waiting for you if you start using BSD seriously)!

But as I eluded to above, I also acknowledge the existence of the real world. I have colleagues and clients who expect documents written in specific software packages, and I want to run specific games and other proprietary software. For these, I delegate to my Mac, and my Fedora partition. I *could* run everything I need on those machines and do away with desktop BSD, but I wouldn't enjoy it.

And that's kind of the whole point. My FreeBSD desktop workstation is my favourite computer to use. Since I first took the plunge into the BSDs, it's clicked in a way other systems haven't. It was *fun* to tinker and try something new, especially an OS with that history and lineage going back to the original AT&T UNIX. And now I have something that works great for me. There's this pervasive attitude in IT and hustle culture nonsense that you should have to justify and monetise everything you do. Sometimes you need to give yourself permission to tinker.

If anyone is interested, the [FreeBSD Handbook](https://docs.freebsd.org/en/books/handbook/x11/) and [NetBSD Guide](https://www.netbsd.org/docs/guide/en/chap-x.html) are your best places to start. [Vermaden](https://vermaden.wordpress.com/freebsd-desktop/), [Tubsta](https://www.tubsta.com/2021/03/freebsd-13-0-full-desktop-experience/), [GaryH Tech](https://www.youtube.com/c/GaryHTech), and [RoboNuggie](https://www.youtube.com/channel/UCxwcmRAmBRzZMNS37dCgmHA) also create great BSD tutorials. Provided you're a bit careful with your [hardware choices](https://www.freebsd.org/releases/13.1R/hardware/), and have reasonable expectations about software compatibility, you might be surprised how polished an experience it can be. 

*(Update: I posted a [follow-up regarding OpenBSD too](https://rubenerd.com/derek-sivers-on-openbsd/)).*
