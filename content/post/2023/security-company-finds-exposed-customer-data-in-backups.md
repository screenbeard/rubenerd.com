---
title: "Security firm finds exposed customer data in backups"
date: "2023-02-08T13:47:06+11:00"
abstract: "Roughly 12% of online stores forget their backups in public folders due to human error or negligence."
year: "2023"
category: Internet
tag:
- news
- security
location: Sydney
---
Bill Toulas [reported the findings](https://www.bleepingcomputer.com/news/security/over-12-percent-of-analyzed-online-stores-expose-private-data-backups/) for Bleeping Computer:

> According to a study by website security company Sansec, roughly 12% of online stores forget their backups in public folders due to human error or negligence.
> 
> The study examined 2,037 stores of various sizes and found that 250 (12.3%) exposed ZIP, SQL, and TAR archives on public web folders that can be freely accessed without requiring authentication.

Is it bad that I'm almost, kinda, sorta, relieved? I would have expected *far* more than 12%. But that probably says more about me.
