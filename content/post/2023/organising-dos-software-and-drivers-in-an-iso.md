---
title: "Organising DOS software and drivers in an ISO"
date: "2023-03-04T21:40:46+11:00"
abstract: "No more nested folders of random archives and disk images!"
year: "2023"
category: Software
tag:
- buffet-problem
- dos
- retrocomputing
location: Sydney
---
8-bit Commodore computers aside, my first love is still probably DOS. It was the first OS I had experience with as a kid, even before the old Apple IIs my primary school inherited. We're spoiled for choice when it comes to virtualising it on our contemporary systems, and there's still (mostly) a wide availability of software and drivers available.

*(If I could tap into my inner Amelia Watson or Ouro Kronii, I'd travel back to the early 1990s and whisper **AMIGA** into my dad's ear before he bought home our first family PC clone. But then, he specifically bought a PC because he used XTs and ATs at work, so that would likely have not made a difference. One can dream).*

But in another form of what I dub the Buffet Problem, you end up downloading a ton of stuff into various folders, and it quickly becomes unwieldy. As an example, I'll buy a EGA graphics card for an old machine, and soon my current desktop is full of archives and folders of various drivers, utilities, and *versions* of those drivers and utilities to try. Being from different places and times, there's no consistency to their naming, or archive type. Even if something works, I soon forget which it was, and the whole exercise repeats.

I got around this by having an retro archive directory structure on my FreeBSD ZFS NAS that looks, a little something, like this:

* **BeOS**
* **CPM**
* **DOS**
    * Games
    * GEM
    * Drivers
        * Controllers
        * Iomega
        * Graphics
        * NICs
        * Sound
    * Installers
    * Utilities
    * Win16
* **SD2IEC**

But in reality, I was still shoving files with their original names and archive formats into these, without much regard for consistency and future use. If only there were a solution for easily accessing this stuff from the retrocomputers in question, and knowing which driver or utility was necessary to do something.

The answer came to me when I realised I was already doing things better with my curated, organised, and catalogued `SD2IEC` folder for my Commodore machines, and could apply the same logic to an ISO instead of an SD card.

I wrote last year about [using mkisofs(1) to create discs for DOS](https://rubenerd.com/dos-friendly-bootable-isos-with-mkisofs/). Provided you have an optical drive in your target machine, you could feasibly have every single driver and program on a single disc, with oodles of spare space (I cheat and add a CD-ROM to old PCs, even if it means using a sound card's IDE port or a SCSI daughter card. It might not be period correct, but they're too useful).

I created a new folder to act as the ISO root, then decompressed every archive, floppy disk image, and self-extracting EXE into it; quite an experience, especially for some of the more esoteric formats! A few `diff(1)` runs confirmed that much of what I stored were duplicates, they were just archived and named differently. This has been another great idle brain activity to do in the evenings; it must have taken a few weeks on and off at least.

Cooler still, the limitation of this approach ended up being a strength. The 8.3 filename limit forced me to rename every resulting directory. To make sense of it, I made a `README.TXT` on the root of the image listing clearly what each directory was, the version, and the year it was released. At a glance now I can see everything I've amassed, and it's easy enough to rebake the ISO when I find something new.

For bonus points, I'm now thinking whether I create a simple DOS boot menu using `CHOICE`, or a pseudo-GUI tool like PowerMenu, or even dusting off some old Borland Pascal to write a quick menu script. Then I could insert the disc, pick exactly what I need, and run.

Did I mention I'm in my thirties yet? Or was that painfully obvious?
