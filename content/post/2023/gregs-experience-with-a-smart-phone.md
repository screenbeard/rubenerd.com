---
title: "Greg’s experience with a smart phone"
date: "2023-01-27T07:20:15+11:00"
abstract: "“Why is answering an incoming call such a minefield?”"
year: "2023"
category: Hardware
tag:
- accessibility
- android
- design
- ios
- phones
location: Sydney
---
Greg Lehey wrote in his diary earlier in the month about his adventures [attempting to call someone](http://www.lemis.com/grog/diary-jan2023.php?&article=D-20230112-021600#D-20230112-021600) on a modern phone:

> So I picked up my phone, which was in landscape orientation and displaying one of its call indications: Dismiss/Accept? Turned it to portrait orientation. It dismissed without me coming anywhere near the screen.
> 
> Why does this keep happening? Is it my specific brand (Xiaomi)? Why is answering an incoming call such a minefield? 

The experience is barely better for us on iOS, as [Michael Tsai summarised](https://mjtsai.com/blog/2020/01/09/end-accept-decline-hold-accept/) back in 2020. It's perhaps the industry's worst kept secret that smart phones make absolutely *lousy* phones!

It floors me that modern UI/UX designers managed to bork one of the most robust, tested, simple, and well-understood interfaces in modern history. It's that same hubris that informed the industry's removal of the headphone jack too (after first mocking Apple for doing it).

I'd call them to tell them to fix their interfaces but... damn it!
