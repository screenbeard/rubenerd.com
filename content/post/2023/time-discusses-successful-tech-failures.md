---
title: "Time magazine discusses successful tech failures"
date: "2023-02-14T07:31:01+11:00"
abstract: "Palm, the Pebble, and the legendary Betamax get a mention."
year: "2023"
category: Hardware
tag:
- betamax
- palm
- retrospectives
- vhs
- video
location: Sydney
---
Most "bad tech" retrospectives are, at best, rehashes of the same dozen devices *everyone* posts. There's also little incentive to dig deeper into *why* such devices failed when all you're doing is filling space during a slow news day. But [this 2017 Time article](https://time.com/4704250/most-successful-technology-tech-failures-gadgets-flops-bombs-fails/) had some interesting thoughts among the cavalcade of regulars.

I shared the authors' dismay about Palm:

> That Palm was never able to convert the beachhead it established in mobile computing into a smartphone empire is one of the biggest tragedies in all of tech. Acquired by HP in 2010, Palm has devolved into a zombie brand, continually churning out handheld devices that neither sell well nor move the ball forward.

And Pebble, the early smartwatch:

> And the company cultivated a strong developer following, with hundreds of third-party apps and watch faces available &hellip; But the smartwatch market was too small for Pebble to survive for long, especially when the world’s biggest tech giants entered the space.

Unsurprisingly, Betamax also gets a mention:

> Stop me if you’ve heard this one: Betamax was better than VHS and only flopped because Sony fumbled its marketing. That’s about half right. In truth, Betamax’s technical bona fides were trifling (even to videophiles), and that, along with its higher price tag, made VHS the consumer no-brainer.

VHS also offered longer record time per tape, which contributed to its price and convenience. But that can't be the whole story either; Philips and Grundig had even longer recording times with their double-sided Video 2000 cassettes, but they sold even fewer than Betamax.

It just goes to show that technical features are often not at the forefront of purchasing decisions. Companies and pundits forget this at their peril.

