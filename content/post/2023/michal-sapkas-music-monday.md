---
title: "Michał Sapka’s Music Monday"
date: "2023-01-30T08:46:22+11:00"
abstract: "Waffles, pikelets, and Lost Control by Anathema"
thumb: "https://rubenerd.com/files/2023/yt-uoDhH5QEfX0@1x.jpg"
year: "2023"
category: Media
tag:
- feedback
- music
- music-monday
location: Sydney
---
Welcome to *[Music Monday](https://rubenerd.com/tag/music-monday/)*! Each and every Monday without fail, except when I fail, I share some music on a Monday. You'd think I'd stop clarifying what a self-evident name like *Music Monday* is, but then you wouldn't get your weekly dose of waffle.

A Canadian American friend in Singapore got me into waffles. I was firmly in the camp of pikelets for most of my childhood, and pancakes when those weren't available. I wonder if you could make a waffle from pikelet batter?

Today's entry [comes from Michał Sapka](https://michal.sapka.me/2023/anathema-lost-control/), who emailed to say this series had inspired him to start the same. He shared *[Lost Control](https://www.youtube.com/watch?v=uoDhH5QEfX0)* by Anathema:

> Anathema is one two most influential bands of my childhood. It singlehandedly took from the claws of hip-hop and made me interested in music. Unfortunately, today band members suffer psychological and monetary struggles - you can help them out via a fundraiser.
> 
> As a teen, I spent countless hours on trains with a portable audio player armed with Alternative 4. I still consider it their greatest album, and it is still one of my favorites.

I would *never* have heard of this unless someone I respect and read shared it. This is the blogosphere at its best :).

<figure><p><a target=_BLANK href="https://www.youtube.com/watch?v=uoDhH5QEfX0" title="Play Lost Control"><img src="https://rubenerd.com/files/2023/yt-uoDhH5QEfX0@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-uoDhH5QEfX0@1x.jpg 1x, https://rubenerd.com/files/2023/yt-uoDhH5QEfX0@2x.jpg 2x" alt="Play Lost Control" style="width:500px;height:281px;" /></a></p></figure>

I also wish I could claim credit for inventing this series, but I shamelessly [stole the concept](https://rubenerd.com/anime-music-monday/) from my old university anime club.
