---
title: "The economic and social effect of sealed devices"
date: "2023-02-25T10:08:00+11:00"
abstract: "We’re discoraging tinkerers, explorers, and potential future fixers."
year: "2023"
category: Hardware
tag:
- closed-systems
- design
- economics
- phones
location: Sydney
---
BBC technology editor Zoe Kleinman [posted a story](https://www.bbc.com/news/technology-64704104 "UK phone repair apprenticeship needed, says firm") about a British phone repair business, and their struggle to find qualified technicians. Noting a lack of industry training and standards, the spokesfolks proposed an apprenticeship programme, and raising awareness of the problem to get more people involved.

It's worth a read for its insight into this specific industry. But I thought this quote was broadly applicable to where we find ourselves:

> "You need some practical skills. But I think attitude is the most important because we can teach you how to repair devices. But you need to have the correct attitude," said Mr Ion.

A *phone repair* business, with a shortfall of *phone repairers*, needing to train more *phone repairers*, to perform *phone repairs*, using their *phone repair* skills, in their *phone repair* business, might not be that surprising or newsworthy by itself. 

Phone Repair. **Harrison Ford**. *GET ME OFF THIS PHONE*.

But I can't help but thinking the current generation of devices are partly to blame here. If you raise a generation of people on sealed phones and tablets, with components so tiny and inhospitably designed for repair, you discourage people from doing said repairs, which hampers growth in the skills and attitudes that Mr Ion explains above.

*(Sealed, integrated devices come with their own benefits. They tend to be more rigid, durable, and pack more components and power in a smaller space. But I have to think there's an inflexion point where the savings start to eat into the practicality of maintaining a device; to say nothing of the environmental impact of throwing away or recycling devices that otherwise could have been fixed and reused).*

Much (if not most) of the technical knowledge I have today didn't come from classrooms, it came from tinkering, trying, building, breaking, and fixing things. Opening up devices to see how they work wasn't a curiosity, it was a *compulsion!* I was incredibly lucky that I had supportive parents who gave me stuff to tinker with, were generous with their patience when I broke something, and shared in my joy when I got something fixed or built. It was never a question of *why* I wanted to do any of this; they saw a fascinated kid and wanted to do what they could to support him.

There are still avenues for kids to do this today, whether it be Arduino robotics or building a game PC. But we risk losing that in a broader sense with the tech we all use in our daily lives, and based on this BBC article, it's already having knock-on effects.
