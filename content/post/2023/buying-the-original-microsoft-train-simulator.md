---
title: "Buying the original Microsoft Train Simulator!"
date: "2023-02-04T13:10:45+11:00"
abstract: "This was probably the most important game from my childhood."
thumb: "https://rubenerd.com/files/2023/train-simulator-box@1x.jpg"
year: "2023"
category: Travel
tag:
- acela-express
- cd-roms
- games
- japan
- nostalgia
- odawara
- philadelphia
- shinjuku
- train-simulator
- united-states
- whitefish
location: Sydney
---
I went down to reception last night to pick up a parcel that was delivered while I was at work, and had become lost in the sorting process. The receptionist on that night look valiantly for it, before finally finding it in a different sorting box from what he expected.

While searching I showed him a screenshot from eBay to give him an idea of the dimensions. He took one look at the title and exclaimed *Train Simulator!? I used to play that! The Japanese routes were so cool!*

*Microsoft Train Simulator* steamed onto the scene in 2001, right at the tail end of the CD-ROM era. It received a few patches and add-ons, with Microsoft reportedly mulling a sequel. This fell through, but the developers took the game independent, and came out with a series of rail and train simulators that continue today.

<figure><p><img src="https://rubenerd.com/files/2023/train-sim-3801@1x.jpg" alt="Screenshot from Train Simulator taken back in 2004, showing the New South Wales 3801 in the Maria's Pass route" srcset="https://rubenerd.com/files/2023/train-sim-3801@1x.jpg 1x, https://rubenerd.com/files/2023/train-sim-3801@2x.jpg 2x" style="width:500px; height:281px;" /></p></figure>

I can't overemphasise just what an important game this was to me as a kid. I couldn't believe my eyes when I first noticed it for sale at Challenger in Funan Centre. Nobody at my school was interested in trains, so it was boon to my self esteem knowing that a company like Microsoft deemed us to be a large enough market to build a game for. It was (albeit capitalistic!) proof that this niche interest I harboured was uncommon, but not *unusual.* You almost couldn't give a better gift to a nerdy kid.

I spent years exploring the routes, doing the scenarios, building consists, downloading new engines, sharing my experiences on forums, and learning more about diesel, electric, and steam locomotives than I ever thought I could. Years after I moved on to other titles, I still kept it installed as a rudimentary graphics benchmarking tool (how many Dash 9s can I connect together before the game stutters?!), and even today I fire it up occasionally to relieve those childhood memories.

<figure><p><img src="https://rubenerd.com/files/2023/odakyu-observation-area@1x.jpg" alt="Photo inside the Odakyu Romancecar on an evening trip back to Shinjuku. The driver is in a cab upstairs." srcset="https://rubenerd.com/files/2023/odakyu-observation-area@1x.jpg 1x, https://rubenerd.com/files/2023/odakyu-observation-area@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

Years later, and my knowledge of Odawara and Hakone lead Clara and I to take a cozy, front-row seat trip on the [Odakyu Electric Railway *Romancecar*](https://rubenerd.com/show374/) for real, which by sheer luck was being retired the month we first travelled to Japan. When we first travelled to the US, we [took the Acela Express](https://rubenerd.com/annexe-trains-ruben-taketh-86/) from New York to Philadelphia to experience the Northeast Corridor I'd travelled electronically so many times. It also lead me to want to explore rural Montana, and to see that famous Whitefish station for real.

Recently though, I realised the copy of Train Simulator I'd long ripped to ISOs for convenience was almost certainly fake. I'm not sure if the person who gave it to me for Christmas that year realised when they bought it, but seeing them with fresh eyes again made me realise they were poor scan and print jobs. Even a closer look at the box revealed stretched graphics and colour shifting.

Naturally I went on eBay, and a gentleman in Western Australia was selling an original boxed copy for less than $20. Look at how pretty these discs are!

<figure><p><img src="https://rubenerd.com/files/2023/train-simulator-box@1x.jpg" alt="Photo of the Train Simulator box, with the two original CDs and some printed material." srcset="https://rubenerd.com/files/2023/train-simulator-box@1x.jpg 1x, https://rubenerd.com/files/2023/train-simulator-box@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

The box comes with a booklet detailing the route of each line, what the various signals mean, and what the controls were. The online documentation in the game was pretty good, but it sure would have made those first years easier having this stuff next to the keyboard!

I'd say I'm *chuffed* for this too, but railway-related puns are beneath me. Like ballast.
