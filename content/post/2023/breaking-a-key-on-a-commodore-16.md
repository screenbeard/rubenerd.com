---
title: "Breaking a Commodore 16 key, and retrocomputer storage"
date: "2023-03-02T17:21:51+11:00"
abstract: "How do you store these machines? I need to figure out a better system."
thumb: "https://rubenerd.com/files/2023/c16-spring@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-16
- retrocomputing
location: Sydney
---
Last weekend I was working on my 1984 Commodore 16, like a gentleman, trying to figure out why warm restarts don't work. The machine can be switched on after a few minutes of being off, but the reset button doesn't work, and flicking the power switch to simulate a warm reboot does nothing. The Commodore Plus/4 resets exactly as I expected when pressing its reset and power buttons, so I knew how the function was supposed to work on these 264-line of machines.

I'd just reassembled the machine having probed its reset lines with my adorable new budget oscilloscope (a subject for another post), when I stood up and bumped the machine clean off the table. On its journey to the floor, the 6 keycap somehow popped off, sending the spring flying and snapping off its post from the board. I couldn't believe it; it had literally just been sitting there.

I have a spare parts board from the VC-20 I can use, though it will require desoldering and reattaching the shift lock key to gain access. It shouldn't be too difficult, I hope. But it still sucks.

<figure><p><img src="https://rubenerd.com/files/2023/c16-spring@1x.jpg" style="width:500px; height:333px;" alt="The Commodore 16 showing its disconnected keycap and spring." srcset="https://rubenerd.com/files/2023/c16-spring@2x.jpg 2x" /></p></figure>

It was also a bit of a wakeup call. I thought I was being careful, but evidently not careful enough. I also need to review my work area; we have very little space in this apartment, but if I want to take this hobby seriously, I need to clear more space before doing it.

I'll admit, I also felt a bit down about breaking a piece of computer history like this. I posted on Mastodon that I thought it was a bit tragic that this machine lasted almost four decades before I got my hands on it, and deep down its the reason why I'm still so nervous making bigger changes to other machines to repair them. Signaleleven sent some [words of encouragement](https://mastodon.sdf.org/@signaleleven/109929537652194616) which legitimately helped!

> It can happen.  And that thing would not be even in service without your clumsy hands. Accidents are just a byproduct of doing things.
> 
> (I practice this attitude for when I kick myself for my mistakes. It's easier on others, think what would you say if this happened to a friend of yours)

Clara also suggested we get a proper cupboard or some clear Muji drawers specifically for storing these machines when I'm not using them, which I think is a great idea. I could even label each drawer with the machine it has, and keep the power supplies, SD2IEC peripherals, and video cables specific to each machine in its respective drawer. It would also negate the need for dust covers.

It makes me wonder, if you have a retrocomputer collection, how do you store them? Do you keep them in their original boxes (if you have them)? I like keeping them out because they're in semi-regular use, but clearly the current arrangement of having them all around the table isn't working.
