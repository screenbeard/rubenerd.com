---
title: "The mysterious textInput RSS field"
date: "2023-01-16T08:27:01+11:00"
abstract: "The RSS 2.0 spec includes a provison for it, though its purpose is “a mystery”"
year: "2023"
category: Internet
tag:
- bugs
- feedback
- feeds
- rss
location: Sydney
---
The RSS 2.0 specification includes a provision for a [channel-level textInput field](https://cyber.harvard.edu/rss/rss.html#optionalChannelElements)\:

> **textInput** Specifies a text input box that can be displayed with the channel.

It includes four elements: **title**, **description**, **name**, and **link**. It looks like any other simple HTML form, though the spec cautions:

> The purpose of the &lt;textInput&gt; element is something of a mystery. You can use it to specify a search engine box. Or to allow a reader to provide feedback. Most aggregators ignore it.

That last line has turned out not to be true, at least in practice. Perhaps owing to its obscurity, most parsers seem to choke or render incorrectly upon reaching it in a feed.

I would know, because I re-implemented it on [my own feed here](https://rubenerd.com/feed/) recently, and have had a few emails from people claiming it broke their readers! In the case of a few parsers, the **title** for the entire channel was overwritten with the **title** of that **textInput** block, suggesting they're incorrectly parsing any title in a **channel** header.

[Kyle Lanchman](https://blog.kylelanchman.com/), who has [excellent taste](https://anilist.co/user/klanchman/) (Lycrois Recoil *and* Bocchi!?), emailed to let me know that he's [issued a pull request](https://github.com/Ranchero-Software/RSParser/pull/76) to fix the parsing bug in RSParser, though he also uses another non-open source reader that likely won't be fixed by it. I'll leave the block there for now to use as an example, but remove it afterwards.

In a weird way, it's fun my RSS feed is leaving its mark on an established RSS parser!
