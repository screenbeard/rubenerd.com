---
title: "The writer of ahiru.pl also uses desktop email"
date: "2023-01-22T10:16:16+11:00"
abstract: "They can pry Thunderbird from their cold, dead hands! Also, read their blog."
year: "2023"
category: Internet
tag:
- email
- feedback
- thunderbird
location: Sydney
---
I've been having so many great conversations with many of you lately, which has helped me further break out of my introvert shell. My only concern is making sure I keep up to date with all of you, which I've already slipped up with a couple times owing to some errant email filters!

Which is a fitting segue into this comment by the writer of [ahiru.pl](https://ahiru.pl)\:

> They can pry Thunderbird from my cold, dead hands :D I personally am the other way round - sometimes am forced to use webmail interfaces for work (or can’t be bothered to set it up for something I’m not going to use), but always use a proper client at home. How else are you supposed to have a consistent interface for multiple different accounts? Or sort stuff? Or find stuff? Not to mention threading or storage (as you already have…).

This was the impetus I had for merging my personal email hosted in Alpine back into Thunderbird too. Having everything in one place makes life *much* easier, even if I still invoke some specific keybindings sometimes.

> I admit I use HTML email at times. My excuse being that it makes embedding pics and links easier.

This is... unfortunately true. I find I need to write HTML email when sending messages to suppliers, landlords, etc. More and more people don't understand direct URLs or image attachments, or are confused when their HTML email gets converted to plaintext when I reply. I could make a stand, or I could get our shower fixed.
