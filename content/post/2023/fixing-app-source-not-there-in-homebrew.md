---
title: "Fixing “app source not there” in Homebrew"
date: "2023-02-08T16:29:40+11:00"
abstract: "Issue a reinstall, and it’ll work."
year: "2023"
category: Software
tag:
- apple
- homebrew
- macos
- troubleshooting
location: Sydney
---
I've moved on to NetBSD's excellent pkgsrc tool for most of my macOS package needs, but I still use Homebrew to install graphical applications.

Today I attempted to upgrade a package, but in my half-asleep stupor I deleted the bundle from the Applications folder first. Derp. This lead to:

	$ brew upgrade --cask 86box   
	==> Error: 86box: It seems the App source '/Applications/86Box/86Box.app' is not there.

Installing again didn't work, for perhaps obvious reasons:

	$ brew install --cask 86box
	==> Warning: Cask '86box' is already installed.

Forcing a reinstall did the trick:

	$ brew reinstall --cask 86box
	==> Uninstalling Cask 86box
	==> Purging files for version 3.11,4311 of Cask 86box
	==> Installing Cask 86box
	==> Moving App '86Box.app' to '/Applications/86Box/86Box.app'
	🍺  86box was successfully installed!
