---
title: "Paul and Linda McCartney, Smile Away"
date: "2023-02-27T17:33:28+11:00"
abstract: "Smile Away! Smile Away! doo da da da doo da"
thumb: "https://rubenerd.com/files/2023/yt-0XjxQVCDepA@1x.jpg"
year: "2023"
category: Media
tag:
- music
- music-monday
- paul-mccartney
location: Sydney
---
It's time for another [Music Monday](https://rubenerd.com/tag/music-monday), that blog post series in which I write about a certain sensory experience on a specific day of the week, with a slight but delightful bit of alliteration for no reason whatsoever.

*Ram* is one of my favourite albums of all time, and this song never fails to cheer me up, especially if I've had a difficult conversation or a morning dealing with trolls.

Someone reacting badly to you? *Smile away!*

<figure><p><a target="_BLANK" href="https://www.youtube.com/watch?v=0XjxQVCDepA" title="Play Smile Away"><img src="https://rubenerd.com/files/2023/yt-0XjxQVCDepA@1x.jpg" srcset="https://rubenerd.com/files/2023/yt-0XjxQVCDepA@1x.jpg 1x, https://rubenerd.com/files/2023/yt-0XjxQVCDepA@2x.jpg 2x" alt="Play Smile Away" style="width:500px;height:281px;" /></a></p></figure>

