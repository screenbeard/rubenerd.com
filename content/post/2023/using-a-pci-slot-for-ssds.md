---
title: "Using PCI slots for SSD brackets"
date: "2023-01-10T11:28:12+11:00"
abstract: "Cases are getting bigger, but their supported storage is shrinking. This could be a workaround."
thumb: "https://rubenerd.com/files/2023/exb01-34right-top.jpg"
year: "2023"
category: Hardware
tag:
- servers
- ssds
- storage
location: Sydney
---
I'm spoiled in server land at work. The build quality of desktop cases has improved significantly over the last few years, but all the innovation is being poured into radiators, chintzy lighting, and vertical mount GPUs. Storage is relegated to awkward positions behind motherboards, in flimsy trays in the power supply shroud area, or eschewed (gesundheit) altogether. All together? English is weird.

In what I dub a reverse-Tardis, cases are getting bigger, but their internal storage is shrinking. Some of this can be attributed to the introduction of NVMe and eMMC that cleanly mount directly to the motherboard without data or power cables. But their price, and limited board slots, make them ill-suited for bulk storage, scratch space, and redundancy. People often say that about me.

What?

With even the multi-purpose 5.25-inch bay vanishing from all but a [select few cases](https://www.fractal-design.com/products/cases/pop/ "The Fractal Pop case"), what other options do we have? It seems silly that we'd use external drives when the cases are large enough to eat cases from a few years ago.

I've often wondered why we don't make better use of PCI slots, especially for storage. Desktop cases usually ship with far more slots than anyone will ever use, even with larger motherboards or graphics cards. And what if your use case is a NAS, or a home server?

Searching around last weekend, like a gentleman, I found an unusual [Silverstone EXB01](https://silverstonetek.com/en/product/info/storage/EXB01/) unit with a removable 9.5mm drive tray that mounts cleanly in a PCI slot.

<figure><p><img src="https://rubenerd.com/files/2023/exb01-34right-top.jpg" alt="Photo of the Silverstone EXB01, showing the PCI mointing bracket and frame for the 9.5 mm drive." style="width:280px; height:191px;" /></p></figure>

It uses standard SATA power and data connectors, meaning the PCI slot is only structural. This would be useful where the case extends beyond the motherboard, and you don't have any more PCI Express slots left for SATA, eMMC, or NVMe adaptors.

I'm tempted to buy a couple for the bottom of my Antec 300 FreeBSD bhyve box, if only to have a more solid place to mount drives instead of using double-sided tape or cable ties.
