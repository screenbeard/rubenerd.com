---
title: "Music player feedback and suggestions"
date: "2023-01-27T08:20:48+11:00"
abstract: "Navidrome, Plex Music, foobar2000, DeaDBeeF, Swinsian, and Exaile."
year: "2023"
category: Software
tag:
- audio
- music
location: Sydney
---
My post about [moving on from iTunes](https://rubenerd.com/moving-off-itunes-after-21-years/) generated a ton of interest and feedback. Thanks to all of you who sent in suggestions and comments in email and [Mastodon](https://bsd.network/@Rubenerd), you've given me a lot to think about.

These were the servers you all suggested:

* **[Navidrome](https://www.navidrome.org/)**, an open source, self-hosted media server that even has [FreeBSD install](https://www.navidrome.org/docs/installation/freebsd/) instructions. A few of you linked to [Wouter's post](https://brainbaking.com/post/2022/03/how-to-stream-your-own-music-reprise/) over on Brainbaking which goes into detail how it works.

* **[Plex Music](https://www.plex.tv/music/)**, which I was told would work well given I already use Plex as a server for shows and movies. I was unaware it even existed.

And local client software:

* **[foobar2000](https://www.foobar2000.org/)**, which is still being actively developed after all these years. The UI is a bit different to what I'm used to, but their Mac client is still pretty fast.

* **[DeaDBeeF](https://deadbeef.sourceforge.io/)**, a clever pun on hex and audio, and also a capable media player with a flexible UI.

* **[Swinsian](https://swinsian.com/)**, a paid macOS app that gets the closest to mimicking the design of classic iTunes. I'd ideally want something cross platform after being freed from Mac/Windows only players, but... it's also *really* nice.

And some other software I remembered:

* **[Exaile](https://exaile.org/)** is still around which is awesome. It was my first FreeBSD music player I used on Xfce back in the day when others seemed too complicated. The site claims it can be built with Homebrew on Mac as well.

* I forgot to mention in that previous post that I do still use **[Musikcube](https://musikcube.com/)** in the Terminal for some quick playback, but I miss seeing cover art.

I only need local playback and rsyncing with my [Walkman](https://rubenerd.com/my-new-sony-nw-a55-walkman/), so I'll probably end up running those local players through their paces.

