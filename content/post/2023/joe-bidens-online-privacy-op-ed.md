---
title: "Joe Biden’s online privacy op-ed"
date: "2023-01-14T09:01:13+11:00"
abstract: "Talking about data collection, disclosure, and targeting children. There are some interesting ideas."
year: "2023"
category: Internet
tag:
- privacy
- united-states
location: Sydney
---
[TechDirt shared](https://www.techdirt.com/2023/01/12/biden-wsj-tech-op-ed-more-of-the-same-confused-stuff-he-said-last-time/#comments) a Wall Street Journal tech op-ed written by US president Joe Biden. I'm not well-versed enough in American law to comment on most of it, but this specific passage stood out:

> [W]e need serious federal protections for Americans’ privacy. That means clear limits on how companies can collect, use and share highly personal data—your internet history, your personal communications, your location, and your health, genetic and biometric data. It’s not enough for companies to disclose what data they’re collecting. Much of that data shouldn’t be collected in the first place.

Agreed.

> These protections should be even stronger for young people, who are especially vulnerable online. We should limit targeted advertising and ban it altogether for children.

Mike Masnick asserts the devil is in the details. For example, he says it'd be infeasible and undesirable to to verify the age of web visitors. While true, it muddies the issue: it'd be easy to legislate against companies buying ads targeting children in the first place. We already do this with tobacco and gambling.

I think the thrust of Mr Biden's message in this section was on the money, and I'm glad it's getting attention. Thinking our industry could self-regulate was optimistic, but foolish.
