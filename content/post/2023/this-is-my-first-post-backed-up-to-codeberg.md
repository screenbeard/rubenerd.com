---
title: "This is my first post backed up to Codeberg"
date: "2023-03-15T19:06:49+11:00"
abstract: "If you’re reading this, my move to Codeberg was successful."
year: "2023"
category: Internet
tag:
- archiving
- git
location: Sydney
---
If you're reading this, it means my blog source (mmm, sauce) backup has successfully migrated to [Codeberg](https://codeberg.org/rubenerd). I'll update my source namespace references and other links in the coming days.

Thanks to [Screenbeard](https://the.geekorium.au/) for the idea. Let me know if you have an account and host some cool stuff that I can follow :)


### Comments


<div class="comment">
<header>
<time datetime="2023-03-21T00:45:00+10:30">Tuesday 21st March 2023</time>
<a class="homepage" rel="nofollow" href="//the.geekorium.au">screenbeard</a>
</header>
<div class="comment-content">
<p>Hey, it's really good to see you move. I hate that Microsoft and other big companies can make themselves these de-facto gatekeepers just by absorbing the latest technology. And just like your preference for mercurial, I don't like that our fellow nerds pour SO MUCH time into one technology that it even has a chance to become "the standard". I love seeing people push back.</p>
</div>
</div>