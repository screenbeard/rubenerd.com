---
title: "Booking a hotel in 2023"
date: "2023-02-06T13:18:26+11:00"
abstract: "“You have a message from Foo. Guest Survey Reminder. You have a message from Foo. This is your receipt. Guest Survey Reminder. You have...”"
year: "2023"
category: Travel
tag:
- email
- spam
location: Sydney
---
These are the emails I received from a hotel, and a booking site, after a two-night stay:

* Your new account on Foo
* Confirm your new account on Foo
* You have a message from Foo
* Stay Safe. Pre-check in now for your stay at Foo
* You have a message from Foo
* 🛄 Thanks! Your booking is confirmed at Foo
* This is your receipt
* You have a message from Foo
* You have a message from Foo
* Your recent stay at Foo
* Rate Foo
* Your review of Foo
* Guest Survey Reminder

Frequent business travellers, how do you cope with this? Is it background noise to you now, or do you have an ever-escalating set of filters?

**Update:** I just got another email reminding me of a Guest Survey. Good thing too, or I wouldn't have noticed.
