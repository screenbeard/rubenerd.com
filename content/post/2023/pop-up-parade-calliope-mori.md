---
title: "Pop Up Parade Calliope Mori"
date: "2023-03-16T20:10:24+11:00"
abstract: "Well deserved, and an awesome design!"
thumb: "https://rubenerd.com/files/2023/mori-thumb.jpg"
year: "2023"
category: Anime
tag:
- hololive
- music
location: Sydney
contributer:
- "https://www.goodsmile.info/en/product/14001/POP+UP+PARADE+Mori+Calliope.html"
---
Everyone's favourite [grim-reaping idol](https://www.youtube.com/@MoriCalliope/streams) from Hololive is getting a [Pop Up Parade fig rendition](https://www.goodsmile.info/en/product/14001/POP+UP+PARADE+Mori+Calliope.html), and I couldn't be happier for her. Not just because it's an awesome design that captures her style and personality so well, but for what it represents.

<figure><p><img src="https://rubenerd.com/files/2023/000947060_02@2x.jpg" alt="Press image of Pop Up Parade's Calliope Mori." style="width:306px; height:500px;" /></p></figure>

Hololive's English *Myth* branch burst onto the scene in 2020, back when the world felt like it was going to hell in a handbasket from Covid. Get it, because she's the grim... shaddup. Not to put too fine a point on it, but I'm sure their streams helped millions of people, including Clara and myself. They've put so much hard work, and I hope tokens like this help to demonstrate they're appreciated.

Clear files, acrylic stands, keychains, wall scrolls, and nendos (sorry fans!) are great and all, but there's no surer sign that you've *made it* in this slice of fandom than a scale fig that people can buy. Well okay, CDs and records too! But still.

As an aside, it's infectious feeling proud for someone's achievements. I should post about people *killing it* more often. Get it, because she's the grim... shaddup.
