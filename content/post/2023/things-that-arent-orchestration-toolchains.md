---
title: "Things that aren’t orchestration toolchains"
date: "2023-03-16T20:14:39+11:00"
abstract: "Cornflakes."
year: "2023"
category: Software
tag:
- lists
- in-no-particular-order
- pointless
location: Sydney
---
In no particular order:

* The Firth of Forth
* Anya Forger
* Fish and Finger Pie
* Tungsten
* This list
* Objective Phantasm&trade;
* ah... ah... AH... damn it, do you hate when *CHOO*
* Unplugged cyclonic vacuum cleaner handles
