

TechDirt shared a Wall Stret Joural tech oped written by Joe Biden. This was the part that stood out for me:

> [W]e need serious federal protections for Americans’ privacy. That means clear limits on how companies can collect, use and share highly personal data—your internet history, your personal communications, your location, and your health, genetic and biometric data. It’s not enough for companies to disclose what data they’re collecting. Much of that data shouldn’t be collected in the first place. These protections should be even stronger for young people, who are especially vulnerable online. We should limit targeted advertising and ban it altogether for children.

Mike Masnick points out the devil is in the details, which means its diffucult to take much of the article seriously. He discusses how technically infeasible it would be to block tracking children, for example.

I'm on the fence. I think Biden is broadly right, and not just about the US. Australia could use a Bill of Rights before we even get to federal privacy protections.

At the very least I'm glad this is getting more attention. The industry has coasted on people's lassis fair attitudes when it comes to this 
