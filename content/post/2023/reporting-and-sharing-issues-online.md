---
title: "Your issue isn’t real, because I don’t have it!"
date: "2023-02-26T09:48:26+11:00"
abstract: "Never underestimate the pedantry of someone convinced your issue isn’t real."
year: "2023"
category: Software
tag:
- psychology
- social-media
location: Sydney
---
Jeff Johnson [tried valiantly to explain in 2020](https://sigpipe.macromates.com/2020/macos-catalina-slow-by-design/#comment-25836)\:

> It's truly baffling how a few continue to vehemently deny the existence of an issue that has been independently verified by many. This verification includes packet traces and binary analysis of system processes, by the way. Moreover, an Apple Xcode engineer publicly confirmed it! There's no rational debate here anymore.
>
> There are a number of reasons why you personally might not be able to reproduce the issue.

Never underestimate the pedantry of someone convinced your issue isn't real, because they don't have it. It's as though they think "exceptions breaking the rule" only apply to them. It'd sure make life easier!

Empathy is in short supply in many technical forums, where the rush to be seen as correct is more important than [any attempt at understanding](https://rubenerd.com/if-you-want-to-understand-reality-dont-be-pedantic/). And we're all the poorer for it.
