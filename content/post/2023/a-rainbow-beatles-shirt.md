---
title: "A rainbow Beatles shirt"
date: "2023-01-09T18:15:12+11:00"
abstract: "I love it, but the colours seem wrong."
thumb: "https://rubenerd.com/files/2023/beatles-shirt@1x.jpg"
year: "2023"
category: Thoughts
tag:
- clothes
- music
- music-monday
- the-beatles
location: Sydney
---
Today's *[Music Monday](https://rubenerd.com/tag/music-monday/)* concerns a purchase I made over the weekend. I was walking through one of our local music stores, like a gentleman, when I chanced upon this fetching garment depicting our favourite Liverpudlians; Mat from [Techmoan](https://www.youtube.com/@Techmoan) notwithstanding:

<figure><p><img src="https://rubenerd.com/files/2023/beatles-shirt@1x.jpg" srcset="https://rubenerd.com/files/2023/beatles-shirt@2x.jpg" /></p></figure>

I'm wearing it now as I write this post; it's rather comfortable and has *The Beatles* on it. Not that specific shirt above, that's a press shot; and not the actual *Beatles*, they're not/weren't that flat.

I couldn't tell you why though, but the colours feel wrong. This is what I feel like they should be:

1. George Harrison should be <span style="font-weight:bold; color:#fc3">yellow</span> (here comes the sun...)
2. Paul McCartney should be <span style="font-weight:bold; color:#06f">blue</span>
3. Ringo Starr should be <span style="font-weight:bold; color:#c03">red</span>
4. George Martin should be <span style="font-weight:bold; color:#096">green</span>

I'm not sure about John Lennon. Maybe he's the black in the shirt.
