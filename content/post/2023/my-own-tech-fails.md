Last week I reviewed a Time magazine retrospective on tech fails. That got me thinking what *mine* would be.

I don't have too many to report beyond the regulars. Heck, I thought Microsoft Bob was fun, and even Windows Me ran fine on my then-new Sony laptop.

* DVD-RAM. I had one of those early DVD burners that supported this type of optical media. Unlike regular writeable media, DVD-RAM acted more like a floppy disk, which made them more flexible than even RW media. The idea was compelling, but its execution was slow, buggy, and had limited support outside my own machine. At least my Zip disks could be read on all the school machines.

* Iomega Clik. These tiny little disks were small enough that the drive could fit inside a PCMICA slot. It worked fine, but I tended to leave the one disk in there for days at a time, which the retractable disk cover *really* didn't like. It was later renamed the Pocket Zip after the infamous *Click of death* began to haunt Zip drives.

* iPhones after 11, and most Androids. I keep bringing this up, but their OLED screens render them unusable to me, unless I want a nausea headache. I've read reports of OLED sensitivity affecting anywhere from 10-20% of the population.

* IDE SSDs. Their lack of compatibility with any device I've tried with IDE makes them practically useless.
