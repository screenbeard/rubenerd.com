---
title: "The solution to centralisation"
date: "2023-01-10T15:07:03+11:00"
abstract: "The evidence doesn’t point to centralisation being inevitable."
year: "2023"
category: Internet
tag:
- om-malik
- silos
- social-media
location: Sydney
---
[Om Malik](https://om.co/2023/01/04/why-internet-silos-win/) quoted a [post by Manuel Moreale](https://manuelmoreale.com/on-internet-silos) about Twitter, Mastodon, and the broader question of Internet silos:

> I'm convinced that there's no solution to the centralisation issue we're currently facing. And that's because I think that fundamentally people are, when it comes to the internet, lazy. And gathering where everyone else is definitely seems easier. It's also easier to delegate the job of moderating and policing to someone else and so as a result people will inevitably cluster around a few big websites, no matter what infrastructure we build.

I empathise, and in my darker moments I may even agree. But podcasting is proof there's evidence to be optimistic. Its resistance to being silo'd isn't happenstance, it's a function of its design. Much as we expect to be able to load any webpage in our browser, we assume we can publish and listen to any podcast on any client we want. Platforms that have attempted to wrestle control away with proprietary extensions or exclusive content have failed for this reason.

Software may help with discovery, subscriptions, and sharing, but the underlying protocols are the building blocks of the web. They're open, distributed, and owned by nobody. Social networks can be built the same way, with the same inherent resiliency.

People don't cluster in silos because they're lazy, it's because its where everyone else is. Or more specifically, those they care about. People underestimate just how powerful the network effect really is. It's a roadblock right now, but it's also an opportunity.

Captured audiences in silos may offer an attractive business model in the short term, but they're by no means an inevitable structure, or the inescapable end-game of the Internet... as grim as our current situation may seem.
