---
title: "Comparing VIC-20 and C64 cart sizes"
date: "2023-02-06T17:07:12+11:00"
abstract: "The VIC-20 carts were ’uge!"
thumb: "https://rubenerd.com/files/2023/carts@1x.jpg"
year: "2023"
category: Hardware
tag:
- commodore
- commodore-16
- commodore-64
- commodore-plus4
- commodore-vic-20
- commodore-vc-20
- retrocomputing
location: Sydney
---
YouTube videos and blogs don't ever prepare me with a sense of scale. Wandering around an Osaka computer store with Clara made us realise just how gargantuan so many modern computer cases are, even ones billed as being smaller. The Fractal Torrent Mini and Nano are still bigger than the "mid-tower" I built my first Pentium 1 machine in, and Hyte cases would struggle fitting inside any reasonably-sized aircraft carrier.

Vintage computers are no different. I'm hooked on watching people lovingly restore their Commodore VIC-20 and Commodore 64 machines, but while their breadbox cases are the same, their cartridges certainly aren't. I knew they wouldn't be, based on the slot being much smaller on the C64 to accommodate the internal RF modulator.

But... I didn't expect it to be *this* much bigger!

<figure><p><img src="https://rubenerd.com/files/2023/carts@1x.jpg" alt="Photo on my work table showing the three carts described below." srcset="https://rubenerd.com/files/2023/carts@1x.jpg 1x, https://rubenerd.com/files/2023/carts@2x.jpg 2x" style="width:500px; height:333px;" /></p></figure>

From right to left we have:

* A **TED/264** cart from 1984 that came with my Commodore 16, that also works in the Plus/4. The board inside isn't that much different from the C64.

* A **Commodore 128** diagnostic cart, with the same dimensions as carts for the Commodore 64 from 1982.

* A **VIC-20** Penultimate+ from The Future Was 8-Bit, which I ordered last month when I thought I was going to build a VIC-20. These originally date from 1980.

The first two had prepared me to think it'd be... what, 20% bigger?

I now understand why people like Adrian Black joke about how difficult these carts would have been to insert and remove. Though it does make me wonder how many hijinks you get away with at this scale: a Raspberry Pi cart, for example?
