---
title: "The folly of haircut instructions"
date: "2019-01-19T11:35:16+11:00"
abstract: "Don’t cut that bit, you say! And they do it anyway."
year: "2019"
category: Thoughts
tag:
- dan-benjamin
- health
- john-roderick
- personal
- road-work
location: sydney
---
**Update 2021:** I’ve taken down fewer than twenty posts out of more than seven thousand in this blog’s history, and posts about this guy are some of them. [MBMBaM’s tweet put it best](https://twitter.com/MBMBaM/status/1345853685902036994) explaining why, though I also have personal reasons. Thanks for understanding.
