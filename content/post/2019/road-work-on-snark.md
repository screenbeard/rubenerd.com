---
title: "Road Work on snark"
date: "2019-01-04T11:44:05+11:00"
abstract: "And I don't drop little snide barbs into conversation that sound like they're friendly but are meant to undermine people."
year: "2019"
category: Media
tag:
- john-roderick
- podcasts
- road-work
location: sydney
---
**Update 2021:** I’ve taken down fewer than twenty posts out of more than seven thousand in this blog’s history, and posts about this guy are some of them. [MBMBaM’s tweet put it best](https://twitter.com/MBMBaM/status/1345853685902036994) explaining why, though I also have personal reasons. Thanks for understanding.
