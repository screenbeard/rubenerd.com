---
title: "Instapaper links for March 2013"
date: "2013-03-30"
year: 2013
category: Annexe
tag:
- from-instapaper
- listpost
location: Sydney
---
Links I saved onto [Instapaper](https://rubenerd.com/tag/from-instapaper/) this month:

* [Natick student computers fried after officials try uninstalling filter-circumventing application](http://www.boston.com/yourtown/news/natick/2013/01/natick_student_computers_fried.html)<br />Boston.com

* [North Korea Declares War Against United States ](http://www.addictinginfo.org/2013/03/31/north-korea-declares-war-against-united-states/)<br />Addicting Info

* ["Few credible people now claim that Scotland would do anything other than prosper as an independent nation"](http://www.scotsman.com/news/blair-jenkins-scotland-s-chance-for-equal-society-1-2868352)<br />Scotsman.com

* [Climate Change Is Making Canada Look More Like the United States](http://www.theatlanticcities.com/neighborhoods/2013/03/climate-change-making-canada-look-more-united-states/4941/)<br />The Atlantic Cities

* [Invention of the Day: A Bladeless Windmill](http://www.theatlanticcities.com/technology/2013/03/invention-day-bladeless-windmill/5142/)<br />The Atlantic Cities

* [Has Spain Just Slammed On The Brakes For Europe's Unitary Patent Plans?](http://www.techdirt.com/articles/20130327/11193822486/spain-slams-brakes-europes-unitary-patent-plans.shtml)<br />Techdirt

* [North Korea at “No.1 Combat Readiness Status”](http://thediplomat.com/flashpoints-blog/2013/03/26/reports-declares-north-korea-at-no-1-combat-readiness-status/)<br />The Diplomat

* [Karl Marx's Revenge: Class Struggle Grows Around the World](http://business.time.com/2013/03/25/marxs-revenge-how-class-struggle-is-shaping-the-world/)<br />TIME.com

* [Teritiary Education Minister Chris Bowen, a Kevin Rudd supporter, resigns from Julia Gillard's cabinet](http://www.sbs.com.au/news/article/1749202/PM-has-my-full-support)<br />SBS

* [Computer Science in Vietnam](http://neil.fraser.name/news/2013/03/16/)<br />Neil Fraser

* [More traffic is really the last thing we need: two sides to the debate on Badgerys Creek Airport](http://www.smh.com.au/comment/more-traffic-is-really-the-last-thing-we-need-20130320-2gfyg.html)<br />Sydney Morning Herald

* [Japan seizes nuclear-related materials from N Korea cargo](http://www.japantoday.com/category/crime/view/japan-seizes-nuclear-related-materials-from-n-korea-cargo)<br />Japan Today

* [China criticises US anti-missile N Korea plan](http://www.todayonline.com/chinaindia/china/china-criticises-us-anti-missile-n-korea-plan)<br />TODAYonline

* [An interview with Singapore Prime Minister Lee Hsien Loong](http://www.washingtonpost.com/opinions/an-interview-with-singapore-prime-minister-lee-hsien-loong/2013/03/15/5ce40cd4-8cae-11e2-9838-d62f083ba93f_story.html)<br />The Washington Post

* [Men drop rope from helicopter to help their friends escape from maximum-security prison in Quebec](http://cnews.canoe.ca/CNEWS/Crime/2013/03/17/20662446.html)<br />Canoe.ca

* [SimCity Hack Lets Users Destroy Anyone's Online City Thanks To Always-On DRM](http://www.cinemablend.com/games/SimCity-Hack-Lets-Users-Destroy-Anyone-Online-City-Thanks-Always-DRM-53685.html)<br />Cinemablend

* [Britain's Cameron says pope is wrong on the Falklands](https://www.reuters.com/article/2013/03/15/us-pope-britain-falklands-idUSBRE92E0MS20130315)<br />Reuters

* ["China will be the next Tunisia" - dissident Hu Jia tells @BBCNewshour](https://soundcloud.com/bbc-world-service/bbc-newshour-chinese-dissident)<br />SoundCloud 

* [What Bill Gates Got Wrong About Why Nations Fail - By Daron Acemoglu and James Robinson](http://www.foreignpolicy.com/articles/2013/03/12/what_bill_gates_got_wrong_about_why_nations_fail)<br />Foreign Policy

* [VIDEO: The Economic Impact of a War Between China and Japan](http://www.fairobserver.com/article/video-economic-impact-war-between-china-japan)<br />Fair Observer

* [WordPress 3.6 -- What To Look Forward To?](http://wpmu.org/wordpress-3-6-what-to-look-forward-to/)<br />WPMU

* [America's Dangerous Drift, "Could this be worse than decline?](http://thediplomat.com/2013/02/25/americas-dangerous-drift/)<br />The Diplomat

* [North Korea threatens 'pre-emptive' nuclear strike](http://www.abc.net.au/news/2013-03-07/north-korea-threatens-27pre-emptive27-nuclear-strike/4559532)<br />ABC News (Australian Broadcasting Corporation)

* [Toxic Sinkhole Threatens Southeastern Louisiana](http://www.forbes.com/sites/williampentland/2013/03/03/toxic-sinkhole-threatens-southeastern-louisiana/)<br />Forbes

* [The guilty truth: There's too much content](https://www.cnet.com/news/the-guilty-truth-theres-too-much-content/)<br />CNET

* [Our Work Here is Done](http://www.webstandards.org/2013/03/01/our-work-here-is-done/)<br />The Web Standards Project

* [Hypercritical: Fear of a WebKit Planet](http://hypercritical.co/2013/03/04/fear-of-a-webkit-planet)<br />Hypercritical

* [Washington State lawmaker defends bike tax, says bicycling is not good for the environment](http://seattlebikeblog.com/2013/03/02/state-lawmaker-says-bicycling-is-not-good-for-the-environment-should-be-taxed/)<br />Seattle Bike Blog

* [New Internet Porn Scam](http://www.schneier.com/blog/archives/2013/03/new_internet_po.html)<br />Schneier on Security

* [Beijing urges Hong Kong delegates to play positive role in city's development](http://www.scmp.com/news/china/article/1174088/beijing-urges-hong-kong-delegates-play-positive-role-citys-development)<br />South China Morning Post

* [Dennis Rodman says his new pal Kim Jong-un doesn't want war with the US, just a phone call from Barack Obama](http://www.abc.net.au/news/2013-03-04/an-rodman-says-kim-jong-un-wants-obama-phone-call/4550800)<br />ABC News (Australian Broadcasting Corporation)

* [Why Apple Needs to Fix Its Podcast Problem](https://www.cultofmac.com/218123/why-apple-needs-to-fix-its-podcast-problem/)<br />Cult of Mac

* [Australia 108 Skyscraper & the Melbourne economy](http://t.co/COdFTAFDOx)<br />Decoding the new economy

* [Closed Campgrounds Not Scare Tactic in No-Leeway Law](http://www.bloomberg.com/news/2013-02-27/closed-campgrounds-not-scare-tactic-in-no-leeway-law.html)<br />Bloomberg

* [Seth MacFarlane and the Oscars' Hostile, Ugly, Sexist Night](http://www.newyorker.com/online/blogs/closeread/2013/02/seth-macfarlane-and-the-oscars-hostile-ugly-sexist-night.html)<br />The New Yorker

* [NBN contracts reveal the state of Telstra’s ducts](http://www.theregister.co.uk/2013/01/17/pit_and_pipe_remediation_cost/)<br />The Register

